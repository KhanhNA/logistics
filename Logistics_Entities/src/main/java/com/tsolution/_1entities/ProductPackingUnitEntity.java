package com.tsolution._1entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "product_packing_unit")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductPackingUnitEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -592688129378699257L;

	@OneToMany(mappedBy = "productPackingUnit", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private List<ProductPackingUnitDescriptionEntity> productPackingUnitDescriptions;

	public List<ProductPackingUnitDescriptionEntity> getProductPackingUnitDescriptions() {
		if (this.productPackingUnitDescriptions == null) {
			this.productPackingUnitDescriptions = new ArrayList<>();
		}
		return this.productPackingUnitDescriptions;
	}

	public void setProductPackingUnitDescriptions(
			List<ProductPackingUnitDescriptionEntity> productPackingUnitDescriptions) {
		this.productPackingUnitDescriptions = productPackingUnitDescriptions;
	}

}