package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "product_type")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductTypeEntity extends SuperEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 4760621205810109514L;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "name", nullable = false)
	private String name;

	/**
	 * 0: không ho?t d?ng, 1: ho?t d?ng
	 */
	@Column(name = "status")
	private Boolean status;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}