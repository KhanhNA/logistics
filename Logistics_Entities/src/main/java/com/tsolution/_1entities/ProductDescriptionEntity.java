package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "product_description")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductDescriptionEntity extends SuperEntity implements Serializable {
	@Transient
	public static final String FIND = " FROM product_description pd join language l on pd.language_id = l.id "
			+ " join product p on pd.product_id = p.id "
			+ " WHERE p.available = 1 and pd.language_id = :language and ( :text is null "
			+ " or LOWER(p.code) like LOWER(CONCAT('%',:text,'%')) or LOWER(p.name) like LOWER(CONCAT('%',:text,'%')) "
			+ " or LOWER(pd.code) like LOWER(CONCAT('%',:text,'%')) or LOWER(pd.name) like LOWER(CONCAT('%',:text,'%')) ) "
			+ " and p.id not in (:exceptId) "
			+ " ORDER BY p.sort_order, p.is_hot, p.is_popular, p.quantity_ordered, p.code ";

	/**
	 *
	 */
	private static final long serialVersionUID = -4160771280596293872L;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private ProductEntity product;

	@ManyToOne
	@JoinColumn(name = "language_id")
	private LanguageEntity language;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	public ProductEntity getProduct() {
		return this.product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public LanguageEntity getLanguage() {
		return this.language;
	}

	public void setLanguage(LanguageEntity language) {
		this.language = language;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}