package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.tsolution._1entities.base.SuperEntity;

@Entity
@Table(name = "currency")
public class CurrencyEntity extends SuperEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1355210724519320952L;

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	/**
	 * 0: Không h? tr?, 1: Có h? tr?
	 */
	@Column(name = "is_support")
	private Boolean support;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getSupport() {
		return this.support;
	}

	public void setSupport(Boolean support) {
		this.support = support;
	}

}