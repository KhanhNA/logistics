package com.tsolution._1entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "manufacturer")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ManufacturerEntity extends SuperEntity implements Serializable {

	@Transient
	public static final String FIND = " FROM manufacturer m "
			+ " 	JOIN manufacturer_description md ON md.manufacturer_id = m.id "
			+ " 	JOIN language l ON md.language_id = l.id AND LOWER(l.code) = LOWER(:acceptLanguage) "
			+ " WHERE (:status is null or m.status = :status) and ( :text is null "
			+ " or LOWER(m.code) like LOWER(CONCAT('%',:text,'%')) or LOWER(md.name) like LOWER(CONCAT('%',:text,'%')) ) "
			+ " ORDER BY m.code, md.name, m.create_date ";

	/**
	 *
	 */
	private static final long serialVersionUID = -5250640559491023402L;

	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Transient
	private String name;

	@Column(name = "tel")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String tel;

	@Column(name = "email")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String email;

	/**
	 * 0: không ho?t d?ng, 1: ho?t d?ng
	 */
	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean status;

	@OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private List<ManufacturerDescriptionEntity> manufacturerDescriptions;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<ManufacturerDescriptionEntity> getManufacturerDescriptions() {
		return this.manufacturerDescriptions;
	}

	public void setManufacturerDescriptions(List<ManufacturerDescriptionEntity> manufacturerDescriptions) {
		this.manufacturerDescriptions = manufacturerDescriptions;
	}

}