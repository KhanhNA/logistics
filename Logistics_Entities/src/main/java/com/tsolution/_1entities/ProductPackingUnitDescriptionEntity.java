package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "product_packing_unit_description")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductPackingUnitDescriptionEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -653844766058346504L;

	@Column(name = "name", nullable = false)
	private String name;

	@ManyToOne
	@JoinColumn(name = "language_id", nullable = false)
	private LanguageEntity language;

	@ManyToOne
	@JoinColumn(name = "product_packing_unit_id", nullable = false)
	private ProductPackingUnitEntity productPackingUnit;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LanguageEntity getLanguage() {
		return this.language;
	}

	public void setLanguage(LanguageEntity language) {
		this.language = language;
	}

	public ProductPackingUnitEntity getProductPackingUnit() {
		return this.productPackingUnit;
	}

	public void setProductPackingUnit(ProductPackingUnitEntity productPackingUnit) {
		this.productPackingUnit = productPackingUnit;
	}

}