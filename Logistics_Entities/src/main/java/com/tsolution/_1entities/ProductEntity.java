package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.WhereJoinTable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DeserializeDateHandler;
import com.tsolution.utils.SerializeDateHandler;
import com.tsolution.utils.StringUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "product")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductEntity extends SuperEntity implements Serializable {

	@Transient
	public static final String FIND = " FROM product p left join product_description pd on pd.product_id = p.id "
			+ " WHERE p.available = 1 and p.id not in (:exceptId) and ( :text is null "
			+ " or LOWER(p.code) like LOWER(CONCAT('%',:text,'%')) or LOWER(p.name) like LOWER(CONCAT('%',:text,'%')) "
			+ " or LOWER(pd.code) like LOWER(CONCAT('%',:text,'%')) or LOWER(pd.name) like LOWER(CONCAT('%',:text,'%')) ) "
			+ " ORDER BY p.sort_order, p.is_hot, p.is_popular, p.quantity_ordered, p.code ";

	/**
	 *
	 */
	private static final long serialVersionUID = -5817625037072740444L;

	@ManyToOne
	@JoinColumn(name = "product_type_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductTypeEntity productType;

	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String name;

	@Column(name = "sku", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String sku;

	/**
	 * 0: không th? s? d?ng d? thao tác nghi?p v?, 1: Còn có th? s? d?ng d? thao tác
	 * nghi?p v?
	 */
	@Column(name = "available")
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private Boolean available;

	@Column(name = "available_date")
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonDeserialize(using = DeserializeDateHandler.class)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private LocalDateTime availableDate;

	/**
	 * S? lu?ng package dã du?c d?t hàng, tính v?i các don hàng hoàn thành vi?c xu?t
	 * hàng
	 */
	@Column(name = "quantity_ordered")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer quantityOrdered;

	@Column(name = "sort_order")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer sortOrder;

	@Column(name = "is_hot")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean hot;

	@Column(name = "is_popular")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean popular;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private List<ProductDescriptionEntity> productDescriptions;

	@ManyToOne
	@JoinColumn(name = "manufacturer_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ManufacturerEntity manufacturer;

	@ManyToOne
	@JoinColumn(name = "distributor_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private DistributorEntity distributor;

	@Column(name = "length")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double length;

	@Column(name = "width")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double width;

	@Column(name = "height")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double height;

	@Column(name = "weight")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double weight;

	@Column(name = "vat")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer vat;

	@Column(name = "warning_threshold")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer warningThreshold;

	@Column(name = "lifecycle")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer lifecycle;

	@Column(name = "barcode")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String barcode;

	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
	@JsonView({ JsonEntityViewer.Human.Detail.class,
			JsonEntityViewer.Human.Summary.ProductPackingGroupByExpireDate.class })
	private List<ProductPackingEntity> productPackings;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "product_packing_price", joinColumns = @JoinColumn(name = "product_id", insertable = false, updatable = false), inverseJoinColumns = @JoinColumn(name = "id"))
	@WhereJoinTable(clause = "from_date <= SYSDATE() AND ( to_date IS NULL OR to_date >= DATE(SYSDATE()) ) AND status = 1 AND sale_type = 'IN' AND packing_type_quantity = 1")
	@JsonView({ JsonEntityViewer.Human.Summary.InventoryIgnoreExpireDate.class,
			JsonEntityViewer.Human.Detail.ExportStatementDetail.class,
			JsonEntityViewer.Human.Summary.MerchantOrderDetails.class })
	private List<ProductPackingPriceEntity> productPackingPrices;

	public ProductEntity() {
	}

	public ProductEntity(Long id, String code) {
		this.setId(id);
		this.setCode(code);
	}

	public ProductTypeEntity getProductType() {
		return this.productType;
	}

	public void setProductType(ProductTypeEntity productType) {
		this.productType = productType;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSku() {
		return this.sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Boolean getAvailable() {
		return this.available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public LocalDateTime getAvailableDate() {
		return this.availableDate;
	}

	public void setAvailableDate(LocalDateTime availableDate) {
		this.availableDate = availableDate;
	}

	public Integer getQuantityOrdered() {
		return this.quantityOrdered;
	}

	public void setQuantityOrdered(Integer quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Boolean getHot() {
		return this.hot;
	}

	public void setHot(Boolean hot) {
		this.hot = hot;
	}

	public Boolean getPopular() {
		return this.popular;
	}

	public void setPopular(Boolean popular) {
		this.popular = popular;
	}

	public List<ProductDescriptionEntity> getProductDescriptions() {
		return this.productDescriptions;
	}

	public void setProductDescriptions(List<ProductDescriptionEntity> productDescriptions) {
		this.productDescriptions = productDescriptions;
	}

	public ManufacturerEntity getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(ManufacturerEntity manufacturer) {
		this.manufacturer = manufacturer;
	}

	public DistributorEntity getDistributor() {
		return this.distributor;
	}

	public void setDistributor(DistributorEntity distributor) {
		this.distributor = distributor;
	}

	public Double getLength() {
		return this.length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Double getWidth() {
		return this.width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return this.height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWeight() {
		return this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Integer getVat() {
		return this.vat;
	}

	public void setVat(Integer vat) {
		this.vat = vat;
	}

	public Integer getWarningThreshold() {
		return this.warningThreshold;
	}

	public void setWarningThreshold(Integer warningThreshold) {
		this.warningThreshold = warningThreshold;
	}

	public Integer getLifecycle() {
		return this.lifecycle;
	}

	public void setLifecycle(Integer lifecycle) {
		this.lifecycle = lifecycle;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public List<ProductPackingEntity> getProductPackings() {
		return this.productPackings;
	}

	public List<ProductPackingPriceEntity> getProductPackingPrices() {
		return this.productPackingPrices;
	}

	public void setProductPackingPrices(List<ProductPackingPriceEntity> productPackingPrices) {
		this.productPackingPrices = productPackingPrices;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProductEntity) {
			ProductEntity entity = (ProductEntity) obj;
			if ((entity.getId() != null) && !StringUtils.isNullOrEmpty(entity.getCode())
					&& (this.getId() == entity.getId()) && this.getCode().equalsIgnoreCase(entity.getCode())) {
				return true;
			}
			if ((entity.getId() != null) && (this.getId() == entity.getId())) {
				return true;
			}
			if (!StringUtils.isNullOrEmpty(entity.getCode()) && this.getCode().equalsIgnoreCase(entity.getCode())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return (super.getId().hashCode() * 17) + (this.getCode().hashCode() * 10);
	}

}