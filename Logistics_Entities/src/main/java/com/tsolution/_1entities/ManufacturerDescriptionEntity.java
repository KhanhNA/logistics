package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "manufacturer_description")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ManufacturerDescriptionEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1168812257398861913L;

	@ManyToOne
	@JoinColumn(name = "manufacturer_id")
	private ManufacturerEntity manufacturer;

	@ManyToOne
	@JoinColumn(name = "language_id")
	private LanguageEntity language;

	@Column(name = "name", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String name;

	@Column(name = "address")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String address;

	public ManufacturerEntity getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(ManufacturerEntity manufacturer) {
		this.manufacturer = manufacturer;
	}

	public LanguageEntity getLanguage() {
		return this.language;
	}

	public void setLanguage(LanguageEntity language) {
		this.language = language;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}