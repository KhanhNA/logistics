package com.tsolution._1entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Where;
import org.springframework.context.i18n.LocaleContextHolder;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.StringUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "product_packing")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductPackingEntity extends SuperEntity implements Serializable {

	@Transient
	public static final String FIND = " FROM product_packing pp JOIN product p ON pp.product_id = p.id "
			+ " JOIN manufacturer m ON p.manufacturer_id = m.id "
			+ " JOIN distributor d ON pp.distributor_id = d.id AND p.distributor_id = d.id AND d.id = :distributorId "
			+ " JOIN product_packing_price ppp ON ppp.product_packing_id = pp.id AND ppp.status = 1 AND ppp.sale_type = 'IN' "
			+ "		 AND ppp.from_date <= SYSDATE() AND ( ppp.to_date IS NULL OR ppp.to_date >= DATE(SYSDATE()) ) "
			+ " left join product_description pd on pd.product_id = p.id "
			+ " WHERE p.available = 1 and pp.id not in (:exceptId) and m.id = :manufacturerId and ( :text is null "
			+ " or LOWER(p.code) like LOWER(CONCAT('%',:text,'%')) or LOWER(p.name) like LOWER(CONCAT('%',:text,'%')) "
			+ " or LOWER(pp.code) like LOWER(CONCAT('%',:text,'%')) or LOWER(pp.name) like LOWER(CONCAT('%',:text,'%')) "
			+ " or LOWER(m.code) like LOWER(CONCAT('%',:text,'%')) "
//			+ " or LOWER(m.name) like LOWER(CONCAT('%',:text,'%')) "
			+ " or LOWER(pd.code) like LOWER(CONCAT('%',:text,'%')) or LOWER(pd.name) like LOWER(CONCAT('%',:text,'%')) ) ";

	/**
	 *
	 */
	private static final long serialVersionUID = 2308920535571354979L;

	@Column(name = "code")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String name;

	@Column(name = "vat")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer vat;

	@Column(name = "uom")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long uom;

	@OneToOne
	@JoinColumn(name = "uom", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.GOD.class)
	private ProductPackingUnitEntity productPackingUnit;

	@Column(name = "barcode")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String barcode;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private ProductEntity product;

	@ManyToOne
	@JoinColumn(name = "packing_type_id")
	private PackingTypeEntity packingType;

	@ManyToOne
	@JoinColumn(name = "distributor_id")
	private DistributorEntity distributor;

	/**
	 * 0: không ho?t d?ng, 1: ho?t d?ng
	 */
	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean status;

	@Column(name = "length")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double length;

	@Column(name = "width")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double width;

	@Column(name = "height")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double height;

	@Column(name = "weight")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double weight;

	@Column(name = "volume")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double volume;

	@Column(name = "warning_threshold")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer warningThreshold;

	@Column(name = "lifecycle")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer lifecycle;

	@OneToMany(mappedBy = "productPacking", fetch = FetchType.LAZY)
	@Where(clause = "from_date <= SYSDATE() AND ( to_date IS NULL OR to_date >= DATE(SYSDATE()) ) AND status = 1 AND sale_type = 'IN' ")
	@JsonView({ JsonEntityViewer.Human.CustomDetail.PoDetail.class, JsonEntityViewer.Human.Summary.ProductPacking.class,
			JsonEntityViewer.Human.Summary.StoreInventory.class })
	private List<ProductPackingPriceEntity> productPackingPrices;

	public ProductPackingEntity() {
	}

	public ProductPackingEntity(Long id) {
		this.setId(id);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVat() {
		return this.vat;
	}

	public void setVat(Integer vat) {
		this.vat = vat;
	}

	public String getUom() {
		if (this.productPackingUnit == null) {
			return null;
		}
		return this.productPackingUnit.getProductPackingUnitDescriptions().stream()
				.filter(x -> x.getLanguage().getCode().equalsIgnoreCase(LocaleContextHolder.getLocale().getLanguage()))
				.map(ProductPackingUnitDescriptionEntity::getName).findFirst().orElse(null);
	}

	public void setUom(Long uom) {
		this.uom = uom;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public ProductEntity getProduct() {
		return this.product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public PackingTypeEntity getPackingType() {
		return this.packingType;
	}

	public void setPackingType(PackingTypeEntity packingType) {
		this.packingType = packingType;
	}

	public DistributorEntity getDistributor() {
		return this.distributor;
	}

	public void setDistributor(DistributorEntity distributor) {
		this.distributor = distributor;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Double getLength() {
		return this.length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Double getWidth() {
		return this.width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return this.height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWeight() {
		return this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return this.volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Integer getWarningThreshold() {
		return this.warningThreshold;
	}

	public void setWarningThreshold(Integer warningThreshold) {
		this.warningThreshold = warningThreshold;
	}

	public Integer getLifecycle() {
		return this.lifecycle;
	}

	public void setLifecycle(Integer lifecycle) {
		this.lifecycle = lifecycle;
	}

	public List<ProductPackingPriceEntity> getProductPackingPrices() {
		if (this.productPackingPrices == null) {
			this.productPackingPrices = new ArrayList<>();
		}
		return this.productPackingPrices;
	}

	public void setProductPackingPrices(List<ProductPackingPriceEntity> productPackingPrices) {
		this.productPackingPrices = productPackingPrices;
	}

	@JsonView(JsonEntityViewer.GOD.class)
	public String getProductNameInEnglish() {
		List<ProductDescriptionEntity> productDescriptions = this.getProduct().getProductDescriptions();
		Optional<ProductDescriptionEntity> oProductDescription = productDescriptions.stream()
				.filter(productDescription -> LocaleContextHolder.getLocale().getLanguage()
						.equalsIgnoreCase(productDescription.getLanguage().getCode()))
				.findFirst();
		if (oProductDescription.isPresent()) {
			return oProductDescription.get().getName();
		}
		return "";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProductPackingEntity) {
			ProductPackingEntity entity = (ProductPackingEntity) obj;
			if ((entity.getId() != null) && !StringUtils.isNullOrEmpty(entity.getCode())
					&& (this.getId() == entity.getId()) && this.getCode().equalsIgnoreCase(entity.getCode())) {
				return true;
			}
			if ((entity.getId() != null) && (this.getId() == entity.getId())) {
				return true;
			}
			if (!StringUtils.isNullOrEmpty(entity.getCode()) && this.getCode().equalsIgnoreCase(entity.getCode())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return (super.getId().hashCode() * 17) + (this.getCode() == null ? 0 : this.getCode().hashCode() * 10);
	}

}
