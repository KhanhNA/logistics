package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "language")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class LanguageEntity extends SuperEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7347436598194628945L;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "SORT_ORDER")
	private Integer sortOrder;

	public LanguageEntity() {
	}

	public LanguageEntity(Long id) {
		this.setId(id);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

}