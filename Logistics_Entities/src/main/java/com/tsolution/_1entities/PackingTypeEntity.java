package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.utils.StringUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "packing_type")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class PackingTypeEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4327887776461927627L;

	public PackingTypeEntity() {
	}

	public PackingTypeEntity(Long id, String code) {
		this.setId(id);
		this.setCode(code);
	}

	@Column(name = "code", nullable = false)
	private String code;
	/**
	 * S? lu?ng theo quy cách dóng gói
	 */
	@Column(name = "quantity", nullable = false)
	private Integer quantity;

	/**
	 * 0: không ho?t d?ng, 1: ho?t d?ng
	 */
	@Column(name = "status")
	private Boolean status;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PackingTypeEntity) {
			PackingTypeEntity entity = (PackingTypeEntity) obj;
			if (entity.getId() != null) {
				if (entity.getId().equals(this.getId())) {
					if (StringUtils.isNullOrEmpty(entity.getCode())) {
						return true;
					} else {
						return entity.getCode().equalsIgnoreCase(this.getCode());
					}
				} else {
					return false;
				}
			} else {
				return entity.getCode().equalsIgnoreCase(this.getCode());
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return (super.getId().hashCode() * 17) + (this.getCode().hashCode() * 10);
	}
}