How to use:

LOGIN:
Method-Type: POST
URL: Logistics:XY7kmzoNzl100@localhost:8888/oauth/token
Body (x-www-form-urlencoded):
	grant_type: password
	username: user
	password: jwtpass

RESULT:
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjAxNTg0MjQsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiU1RBTkRBUkRfVVNFUiIsIkFETUlOX1VTRVIiXSwianRpIjoiNjE2Y2ZmNGUtMjdhYy00Yzg5LTgxM2ItZGJiYmRiYTMxZWFmIiwiY2xpZW50X2lkIjoiTG9naXN0aWNzIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl19.-Iee1HoKsJyQXUshmWGvaspkqlgUx8iXEGLEK1MLlpU",
    "token_type": "bearer",
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI2MTZjZmY0ZS0yN2FjLTRjODktODEzYi1kYmJiZGJhMzFlYWYiLCJleHAiOjE1NjI3NDk1MjQsImF1dGhvcml0aWVzIjpbIlNUQU5EQVJEX1VTRVIiLCJBRE1JTl9VU0VSIl0sImp0aSI6ImI0NzRlMzE5LThlMDktNDk4Zi04MGY5LTQ2MDEyOThiZmNlZiIsImNsaWVudF9pZCI6IkxvZ2lzdGljcyJ9.bUpYgZirlepbbaWY-mUkpRSEqzs9-KRyyxLQP5HX-dA",
    "expires_in": 899,
    "scope": "read write",
    "jti": "616cff4e-27ac-4c89-813b-dbbbdba31eaf"
}
--------------------------------------------------------------
GET ALL USERS:
Method-Type: GET
URL: http://localhost:8888/users
Headers: 
	Authorization: 'BEARER eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjAxNTk4MTksInVzZXJfbmFtZSI6InVzZXIiLCJhdXRob3JpdGllcyI6WyJTVEFOREFSRF9VU0VSIl0sImp0aSI6IjQ3ZjU3NzAyLTYxMmQtNDRjYS1iY2FjLWY3Y2EzZTE2NWI3NyIsImNsaWVudF9pZCI6IkxvZ2lzdGljcyIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdfQ.Z-FhMtS9UL1hzY1Xgd5H2MOL_tQ22uZwks7LuVCk61M'

RESULT: 
[
    {
        "id": 1,
        "username": "user",
        "firstName": "user",
        "lastName": "user",
        "roles": [
            {
                "id": 1,
                "roleName": "MERCHANT",
                "description": "Standard User - Has no admin rights"
            }
        ]
    },
    {
        "id": 2,
        "username": "admin",
        "firstName": "Admin",
        "lastName": "Admin",
        "roles": [
            {
                "id": 1,
                "roleName": "MERCHANT",
                "description": "Standard User - Has no admin rights"
            },
            {
                "id": 2,
                "roleName": "ADMIN",
                "description": "Admin User - Has permission to perform admin tasks"
            }
        ]
    }
]

--------------------------------------------------------------
GET ONE USER
Method-Type: GET
URL: http://localhost:8888/users/1
Headers: 
	Authorization: 'BEARER eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjAxNTk4MTksInVzZXJfbmFtZSI6InVzZXIiLCJhdXRob3JpdGllcyI6WyJTVEFOREFSRF9VU0VSIl0sImp0aSI6IjQ3ZjU3NzAyLTYxMmQtNDRjYS1iY2FjLWY3Y2EzZTE2NWI3NyIsImNsaWVudF9pZCI6IkxvZ2lzdGljcyIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdfQ.Z-FhMtS9UL1hzY1Xgd5H2MOL_tQ22uZwks7LuVCk61M'

RESULT: 
{
    "error": "access_denied",
    "error_description": "Access is denied"
}


--------------------------------------------------------------
https://www.zeonpad.com/app/index.html

JAVA_HOME: E:\jdk1.8.0_201
MAVEN_HOME: E:\apache-maven-3.6.3
PATH:
 - %JAVA_HOME%\bin
 - %MAVEN_HOME%\bin
 
COPY DLL to:
	- /usr/java/jdk1.8.0_231-amd64/jre/bin
	- /usr/java/jdk1.8.0_231-amd64/jre/lib/ext



mvn install:install-file -Dfile=E:\D-Commerce\Logistics\ext_LIBs\jacob.jar -DgroupId=zeonpad-jacob -DartifactId=jacob -Dversion=1.18 -Dpackaging=jar -DgeneratePom=true
mvn install:install-file -Dfile=E:\D-Commerce\Logistics\ext_LIBs\jacob-1.18.jar -DgroupId=zeonpad-jacob -DartifactId=jacob-1.18 -Dversion=1.18 -Dpackaging=jar -DgeneratePom=true
mvn install:install-file -Dfile=E:\D-Commerce\Logistics\ext_LIBs\zeonpadpdf.jar -DgroupId=zeonpad-jacob -DartifactId=zeonpadpdf -Dversion=1.18 -Dpackaging=jar -DgeneratePom=true

<dependency>
	<groupId>zeonpad-jacob</groupId>
	<artifactId>jacob</artifactId>
	<version>1.18</version>
</dependency>
<dependency>
	<groupId>zeonpad-jacob</groupId>
	<artifactId>jacob-1.18</artifactId>
	<version>1.18</version>
</dependency>
<dependency>
	<groupId>zeonpad-jacob</groupId>
	<artifactId>zeonpadpdf</artifactId>
	<version>1.18</version>
</dependency>



