2. User (CRUD - tạm thời chưa làm)
1. Language (CRUD - Tạm thời chưa làm)
2. Currency (CRUD - tạm thời chưa làm)
3. Country (Multilanguage when CRUD - Working)
4. Merchant (Check-in by GPS vs Network on mobile)
5. Import FC
	5.0: Chuẩn bị dữ liệu
		Product: Product_type -> product -> product_description 
		Store: store -> packing_type -> product_packing -> product_packing_price -> store_product_packing
	5.1: API Tìm kiếm product:
		product_type			status = 1 (ngầm định không care)
		product					available = 1 (ngầm định không care)
		product_description		language_id truyền vào theo language client đang chọn
		<or>					truyền vào từ client để query
			product_type			code
			product_type			name
			product					code
			product					name
			product_description		code
			product_description		code
		</or>
		<order>
			product					sort_order
			product					is_hot
			product					is_popular
			product					quantity_ordered
			product					code
		</order>
	5.2: API lấy ra kho FC (only One)
		status = 1
		ís_DC = 0
	5.3: API lấy ra packing_type
		status = 1
	5.4: API tạo lệnh nhập kho FC ???
		- Tạo import_statement
			+ id = 0
			+ from_store_id = null
			+ to_store_id = getFC() ở dưới client xong truyền lên
			+ code = ??? Xem lại nguyên tắc generate ( 20190620/PNK/-FC/140555 )
			+ QR_code = ???
			+ description = truyền lên
			+ import_date, create_date, create_user auto trên service
		- Tạo import_statement_detail
			+ id = null Không truyền lên cái này để nó tự tạo dữ liệu
			+ import_statement_id = null xử lý trên service
			+ store_product_packing_id: product.getId() + PackingType().getId() => ProductPackingPriceEntity + getExpireDate() => StoreProductPackingEntity
			+ quantity: truyền lên
			+ import_date, create_date, create_user auto trên service
		- Xử lý store_product_packing			
			{
				"toStoreId": 1,
				"toStoreCode": "FC",
				"description": "Đây là một cái mô tả nhé",
				"importStatementDetails":[
					{
						"productId": 1,
						"productCode": "DMS",
						"packingTypeId": 1,
						"packingTypeCode": "1",
						"quantity": 6789,
						"expireDate": "2019-12-12 00:00:00.000Z"
					},
					{
						"productId": 1,
						"productCode": "DMS",
						"packingTypeId": 1,
						"packingTypeCode": "1",
						"quantity": 6789,
						"expireDate": "2020-01-01 00:00:00.000Z"
					}
				]
			}
	
6. Lấy SYSDATE() từ DB mariaDB

7. Lệnh xuất kho từ kho đến Kho
	7.1: API: Tìm kiếm từ kho đến kho
		- FromStore: Query All
			status = 1
			is_DC = null
		- ToStore: Query Except FromStore
			status = 1
			is_DC = 1
			Long exceptId
	7.2: API: Tạo phiếu xuất kho:
		- Tạo export_statement
			+ id = 0
			+ from_store_id = truyền lên 
			+ to_store_id = truyền lên
			+ merchant_id = null với trường hợp này
			+ code = nguyên tắc generate ( <năm><tháng><ngày>/PXK/FC-DC_1/140555 )
			+ QR_code = ^^
			+ description = truyền lên
			+ order_id = null với trường hợp này
			+ total = null với trường hợp này
			+ status = 1 (Phải kiểm tra tồn kho trước rồi mới tạo; nếu không đủ báo lỗi)
			+ export_date, create_date, create_user auto trên service
		- Tạo export_statement_detail
			+ id = null Không truyền lên cái này để nó tự tạo dữ liệu
			+ export_statement_id = null xử lý trên service
			+ store_product_packing_id: product.getId() + PackingType().getId() => ProductPackingPriceEntity + getExpireDate() => StoreProductPackingEntity
			+ product_packing_price = null khi chuyển qua lại giữa các kho và có giá trị truyền vào khi chuyển cho merchant
			+ quantity: truyền lên
			+ export_date, create_date, create_user auto trên service
		- Xử lý store_product_packing
				{
					"fromStoreId": 1,
					"fromStoreCode": "FC",
					"toStoreId": 2,
					"toStoreCode": "DC_1",
					"description": "Đây là một cái mô tả nhé",
					"exportStatementDetails":[
						{
							"productId": 1,
							"productCode": "DMS",
							"packingTypeId": 1,
							"packingTypeCode": "1",
							"quantity": 9876,
							"price": 123
						}
					]
				}
8. Lệnh Import DC: tương tự như với FC chỉ thay mỗi ID của fromStore vs toStore
			{
				"fromStoreId": 1,
				"fromStoreCode": "FC",
				"toStoreId": 2,
				"toStoreCode": "DC_1",
				"description": "Đây là một cái mô tả nhé",
				"importStatementDetails":[
					{
						"productId": 1,
						"productCode": "DMS",
						"packingTypeId": 1,
						"packingTypeCode": "1",
						"quantity": 6789,
						"expireDate": "2019-12-12 00:00:00.000Z"
					},
					{
						"productId": 1,
						"productCode": "DMS",
						"packingTypeId": 1,
						"packingTypeCode": "1",
						"quantity": 3087,
						"expireDate": "2020-01-01 00:00:00.000Z"
					}
				]
			}

Tạo lệnh nhập kho từ lệnh xuất kho
	=> Trả toàn bộ thông tin lệnh xuất kho ExportStatementEntity về cho client xử lý quét thông tin
	=> Client quét thông tin xong thì sẽ chuyển đổi dữ liệu sang dạng ImportStatementEntity để tạo mới

9.  Tạo lệnh xuất kho từ thông tin order truyền lên: lưu vào bảng tạm merchant_order và check trạng thái hàng nào đủ hay thiếu
		=> Admin sẽ thấy đơn hàng nào thiếu để tạo đơn xuất từ FC/DC khác về
		=> Khi merchant lên lấy hàng sẽ quét QR Code để tạo Export Statement từ Merchant_order
		Input:
		{
			"orderId": 1,
			"merchantCode": "code",
			"fromStoreCode": "DC_1",
			"orderDetails": [
				{
					"productCode": "1",
					"packingTypeCode": "1",
					"quantity": 50,
					"price": 1000
				},
				{
					"productCode": "2",
					"packingTypeCode": "1",
					"quantity": 100,
					"price": 2000
				}
			]
		}
		Output:
		{
			"exportStatementCode": "...",
			"exportStatementQrCode": "...",
		}

10. Tạo danh sách lệnh xuất kho từ danh sách đơn hàng của đại lý
	=> Client sẽ chuyển đổi định dạng và gom hàng theo Store
11. Phân quyền quản lý kho cho storekeeper để thực hiện xử lý dữ liệu khi tạo lệnh nhập xuất
12. Xử lý phân trang cho tìm kiếm sản phẩm
13. Tạo role + permission theo client_id để sau còn phân quyền ^^
14. Tạo reference cho lệnh xuất vs lệnh nhập với đơn hàng của đại lý để khi nhập kho thì sẽ cập nhật lại trạng thái đơn hàng
15. Giảm lượng dữ liệu bằng việc thêm điều kiện lúc mapping dữ liệu
16. Sửa các cái kiểm tra quyền thành kiểm tra theo đường dẫn của permission ^^
17. Thêm phân quyền menu của android động trên DB và trả về ở /user/me
18. Thêm việc truyền language lên cho tất cả các API
19. Thêm i18N và sửa lại câu thông báo theo i18N
---------------------------------------------
20. Tạo lệnh Nhập, Thực nhập, Xuất và Thực Xuất
21. Nghiệp vụ QR Code import FC???
---------------------------------------------
END GAME. Nâng cấp version SpringBoot Y_Y




