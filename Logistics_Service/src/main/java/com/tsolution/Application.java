package com.tsolution;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import com.tsolution._1entities.PalletEntity;
import com.tsolution._2repositories.PalletRepository;
import com.tsolution._3services.PalletService;
import com.tsolution.config.DeliveryServiceConfig;
import com.tsolution.config.FileStorageProperties;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.QRCodeGenerator;
import com.tsolution.utils.StringUtils;

@SpringBootApplication
@ComponentScan("com.tsolution.*")
@EnableConfigurationProperties({ FileStorageProperties.class, DeliveryServiceConfig.class })
public class Application {

	private static final Logger log = LogManager.getLogger(Application.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
		try {
			context.getBean(PalletService.class).findAll();
			Application.log.info(System.getProperty("java.library.path"));
		} catch (BusinessException e) {
			Application.log.error(e.getMessage(), e);
		}
	}

	@Bean
	public CommandLineRunner demoData(PalletRepository palletRepository) {
		return args -> {
			List<PalletEntity> pallets = palletRepository.findAll();
			List<PalletEntity> palletsMissingQr = pallets.stream().filter(x -> StringUtils.isNullOrEmpty(x.getqRCode()))
					.collect(Collectors.toList());
			for (PalletEntity pallet : palletsMissingQr) {
				pallet.setqRCode(QRCodeGenerator.generate(pallet.getCode()));
				palletRepository.save(pallet);
			}
		};
	}

	@Bean
	public CommonsRequestLoggingFilter requestLoggingFilter() {
		CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
		loggingFilter.setIncludeClientInfo(true);
		loggingFilter.setIncludeQueryString(true);
		loggingFilter.setIncludePayload(true);
		return loggingFilter;
	}

//	private static String multiValueMapToUri()
//		MultiValueMap<String, String> xxx = new LinkedMultiValueMap<>()
//		xxx.add("7", "2019-09-29 00:00:00.000Z")
//		xxx.add("7", "2019-08-30 00:00:00.000Z")
//		UriComponents uriComponents = UriComponentsBuilder.newInstance().scheme("http").host("localhost").port(8888)
//				.queryParams(xxx).build()
//
//		return uriComponents.toUriString()
//
}
