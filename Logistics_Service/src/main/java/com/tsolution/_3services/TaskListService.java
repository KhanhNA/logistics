package com.tsolution._3services;

import org.springframework.http.ResponseEntity;

import com.tsolution.excetions.BusinessException;

public interface TaskListService {
	ResponseEntity<Object> getTaskListPo() throws BusinessException;

	ResponseEntity<Object> getStoreAppDashboard(String username, String acceptLanguage) throws BusinessException;
}
