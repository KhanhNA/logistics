package com.tsolution._3services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.dto.ImportStatementDto;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.export_statement.dto.ExportStatementDto;
import com.tsolution._1entities.export_statement.dto.ExportStatementFilterDto;
import com.tsolution.excetions.BusinessException;

public interface ExportStatementService extends IBaseService<ExportStatementDto> {
	ResponseEntity<Object> findOneByCode(String code) throws BusinessException;

	ResponseEntity<Object> find(ExportStatementFilterDto exportStatementFilterDto, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> createFromMerchantOrder(ExportStatementDto exportStatementDto) throws BusinessException;

	public ExportStatementEntity createByMerchantOrder(ExportStatementDto exportStatementDto) throws BusinessException;

	ResponseEntity<Object> print(Long id) throws BusinessException;

	ResponseEntity<Object> cancel(Long id) throws BusinessException;

	ResponseEntity<Object> cancelDetail(Long id, List<Long> detailIds) throws BusinessException;

	ResponseEntity<Object> actuallyExport(Long id, ImportStatementDto importStatementDto) throws BusinessException;

	ResponseEntity<Object> createByStoreDemand(ExportStatementDto exportStatementDto) throws BusinessException;

	ResponseEntity<Object> printPdf(Long id) throws BusinessException;

}
