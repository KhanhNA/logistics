package com.tsolution._3services;

import org.springframework.http.ResponseEntity;

import com.tsolution.excetions.BusinessException;

public interface ConfigService {
	ResponseEntity<Object> find(String code) throws BusinessException;
}
