package com.tsolution._3services;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.CountryEntity;
import com.tsolution.excetions.BusinessException;

public interface CountryService extends IBaseService<CountryEntity> {
	ResponseEntity<Object> find(Boolean support, Long language);

	ResponseEntity<Object> getLocationsTree() throws BusinessException;

	ResponseEntity<Object> getLocationsTownship() throws BusinessException;
}
