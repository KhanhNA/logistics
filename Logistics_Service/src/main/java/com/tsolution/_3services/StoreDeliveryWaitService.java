package com.tsolution._3services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.store.StoreDeliveryWaitEntity;
import com.tsolution.excetions.BusinessException;

public interface StoreDeliveryWaitService extends IBaseService<StoreDeliveryWaitEntity> {
	ResponseEntity<Object> getAllByFromStore(Long fromStoreId) throws BusinessException;

	ResponseEntity<Object> exportFromStore(Long fromStoreId) throws BusinessException;

	ResponseEntity<Object> importFromStore(Long fromStoreId, MultipartFile file) throws BusinessException;
}
