package com.tsolution._3services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.dto.PoDto;
import com.tsolution.excetions.BusinessException;

public interface PoService extends IBaseService<PoEntity> {
	ResponseEntity<Object> find(PoDto poDto, Integer pageNumber, Integer pageSize, String orderBy,
			String acceptLanguage) throws BusinessException;

	ResponseEntity<Object> print(Long id) throws BusinessException;

	ResponseEntity<Object> printPdf(Long id) throws BusinessException;

	ResponseEntity<Object> printMultiple(String acceptLanguage, List<Long> ids) throws BusinessException;

	ResponseEntity<Object> approve(Long id) throws BusinessException;

	ResponseEntity<Object> reject(Long id, PoEntity po) throws BusinessException;

	ResponseEntity<Object> updateArriveVietnamPort(Long id, PoEntity source) throws BusinessException;

	ResponseEntity<Object> updateArrivedVietnamPort(Long id, PoEntity poEntity) throws BusinessException;

	ResponseEntity<Object> updateArriveMyanmarPort(Long id, PoEntity poEntity) throws BusinessException;

	ResponseEntity<Object> updateArrivedMyanmarPort(Long id, PoEntity poEntity) throws BusinessException;

	ResponseEntity<Object> updateArriveFc(Long id, PoEntity poEntity) throws BusinessException;

	ResponseEntity<Object> downloadTemplate(Long manufacturerId, Long distributorId, List<Long> exceptId)
			throws BusinessException;

}
