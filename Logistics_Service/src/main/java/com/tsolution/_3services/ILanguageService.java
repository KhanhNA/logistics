package com.tsolution._3services;

import org.springframework.http.ResponseEntity;

public interface ILanguageService {
	public ResponseEntity<Object> getAll();
}
