package com.tsolution._3services;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.importpo.ImportPoDetailPalletEntity;
import com.tsolution._1entities.importpo.ImportPoEntity;
import com.tsolution.excetions.BusinessException;

public interface ImportPoService extends IBaseService<ImportPoEntity> {
	ResponseEntity<Object> find(Long storeId, Long distributorId, String text, Integer status, Integer pageNumber,
			Integer pageSize, String acceptLanguage) throws BusinessException;

	ResponseEntity<Object> updatePallet(Long id, List<ImportPoDetailPalletEntity> importPoDetailPallets)
			throws BusinessException;

	ResponseEntity<Object> create(ImportPoEntity importPoEntity, MultipartFile[] files) throws BusinessException;
}
