package com.tsolution._3services;

import java.time.LocalDateTime;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.CurrencyExchangeRateEntity;
import com.tsolution.excetions.BusinessException;

public interface CurrencyExchangeRateService extends IBaseService<CurrencyExchangeRateEntity> {
	ResponseEntity<Object> findCurrencyExchangeRate(Long fromCurrencyId, Long toCurrencyId, LocalDateTime fromDate,
			LocalDateTime toDate, Integer pageNumber, Integer pageSize) throws BusinessException;
}
