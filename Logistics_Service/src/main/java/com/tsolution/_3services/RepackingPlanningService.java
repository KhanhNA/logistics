package com.tsolution._3services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.RepackingPlanningDetailRepackedEntity;
import com.tsolution._1entities.RepackingPlanningEntity;
import com.tsolution._1entities.dto.RepackingPlanningDto;
import com.tsolution.excetions.BusinessException;

public interface RepackingPlanningService extends IBaseService<RepackingPlanningEntity> {

	ResponseEntity<Object> find(RepackingPlanningDto repackingPlanningDto, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> repack(Long id, List<RepackingPlanningDetailRepackedEntity> importPoDetailPallets)
			throws BusinessException;

	ResponseEntity<Object> confirmRepack(Long id) throws BusinessException;

	ResponseEntity<Object> cancelRepack(Long id) throws BusinessException;

	ResponseEntity<Object> cancelPlan(Long id) throws BusinessException;

	ResponseEntity<Object> findDetailRepackedById(Long id) throws BusinessException;

	ResponseEntity<Object> print(Long id) throws BusinessException;

	ResponseEntity<Object> printQrCode(Long id) throws BusinessException;

}
