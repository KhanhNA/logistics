package com.tsolution._3services;

import com.tsolution._1entities.DistributorEntity;

public interface DistributorService extends IBaseService<DistributorEntity> {
}
