package com.tsolution._3services.shipping_partner;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution.excetions.BusinessException;

public interface ShippingPartnerService {
	ResponseEntity<Object> find(ShippingPartnerEntity shippingPartner, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> findById(Long id) throws BusinessException;

	ResponseEntity<Object> create(ShippingPartnerEntity shippingPartner, MultipartFile[] files)
			throws BusinessException;

	ResponseEntity<Object> update(Long id, ShippingPartnerEntity source, MultipartFile[] files,
			List<Long> removeShippingPartnerFileIds) throws BusinessException;

	ResponseEntity<Object> accept(Long id) throws BusinessException;

	ResponseEntity<Object> reject(Long id, ShippingPartnerEntity shippingPartner) throws BusinessException;

}
