package com.tsolution._3services.shipping_partner.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution._1entities.shipping_partner.ShippingPartnerFileEntity;
import com.tsolution._1entities.shipping_partner.enums.ShippingPartnerStatus;
import com.tsolution._2repositories.shipping_partner.ShippingPartnerFileRepository;
import com.tsolution._2repositories.shipping_partner.ShippingPartnerRepository;
import com.tsolution._3services.impl.FileStorageService;
import com.tsolution._3services.impl.LogisticsBaseService;
import com.tsolution._3services.shipping_partner.ShippingPartnerService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;

@Service
public class ShippingPartnerServiceImpl extends LogisticsBaseService implements ShippingPartnerService {

	@Autowired
	private ShippingPartnerRepository shippingPartnerRepository;

	@Autowired
	private ShippingPartnerFileRepository shippingPartnerFileRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Override
	public ResponseEntity<Object> find(ShippingPartnerEntity shippingPartner, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		return new ResponseEntity<>(
				this.shippingPartnerRepository.find(shippingPartner, PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}

	private ShippingPartnerEntity getById(Long id) throws BusinessException {
		Optional<ShippingPartnerEntity> oShippingPartner = this.shippingPartnerRepository.findById(id == null ? 0 : id);
		if (!oShippingPartner.isPresent()) {
			throw new BusinessException(this.translator.toLocale("shipping.partner.not.exists"));
		}
		return oShippingPartner.get();
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		return new ResponseEntity<>(this.getById(id), HttpStatus.OK);
	}

	private void validEntity(ShippingPartnerEntity shippingPartner) throws BusinessException {
		if (StringUtils.isNullOrEmpty(shippingPartner.getName())) {
			throw new BusinessException(this.translator.toLocale("shipping.input.missing.name"));
		}
		if (StringUtils.isNullOrEmpty(shippingPartner.getPhone())) {
			throw new BusinessException(this.translator.toLocale("shipping.input.missing.phone"));
		}
		if (StringUtils.isNullOrEmpty(shippingPartner.getEmail())) {
			throw new BusinessException(this.translator.toLocale("shipping.input.missing.email"));
		}
		if (StringUtils.isNullOrEmpty(shippingPartner.getAddress())) {
			throw new BusinessException(this.translator.toLocale("shipping.input.missing.address"));
		}
		if (StringUtils.isNullOrEmpty(shippingPartner.getContractCode())) {
			throw new BusinessException(this.translator.toLocale("shipping.input.missing.contract.code"));
		}
		if (StringUtils.isNullOrEmpty(shippingPartner.getTaxCode())) {
			throw new BusinessException(this.translator.toLocale("shipping.input.missing.tax.code"));
		}
	}

	private List<ShippingPartnerFileEntity> updateFile(ShippingPartnerEntity shippingPartner, MultipartFile[] files,
			List<Long> removeFileIds) throws BusinessException {
		List<ShippingPartnerFileEntity> keepFiles = shippingPartner.getShippingPartnerFiles();
		if ((removeFileIds != null) && !removeFileIds.isEmpty()) {
			List<ShippingPartnerFileEntity> removeFiles = keepFiles.stream()
					.filter(file -> removeFileIds.contains(file.getId())).collect(Collectors.toList());
			if ((removeFiles != null) && !removeFiles.isEmpty()) {
				List<String> fileUrls = removeFiles.stream().map(ShippingPartnerFileEntity::getUrl)
						.collect(Collectors.toList());
				if (Boolean.TRUE.equals(this.fileStorageService.deleteFile(fileUrls))) {
					keepFiles = keepFiles.stream().filter(file -> !removeFileIds.contains(file.getId()))
							.collect(Collectors.toList());
					this.shippingPartnerFileRepository.deleteAll(removeFiles);
				} else {
//					throw new BusinessException("DELETE FILE FAILED!!!")
				}
			}
		}
		if ((files == null) || (files.length == 0)) {
			return keepFiles;
		}
		LocalDateTime sysdate = this.getSysdate();

		for (int i = 0; i < files.length; i++) {
			MultipartFile multipartFile = files[i];
			String fileName = this.fileStorageService.saveFile(
					String.format("%04d-%02d-shipping-partner-files", sysdate.getYear(), sysdate.getMonthValue()),
					multipartFile);
			ShippingPartnerFileEntity file = new ShippingPartnerFileEntity();
			file.setShippingPartner(shippingPartner);
			file.setUrl(fileName);
			this.shippingPartnerFileRepository.save(file);
			keepFiles.add(file);
		}
		return keepFiles;
	}

	@Override
	public ResponseEntity<Object> create(ShippingPartnerEntity shippingPartner, MultipartFile[] files)
			throws BusinessException {
		this.validEntity(shippingPartner);
		shippingPartner.setStatus(ShippingPartnerStatus.PENDING);
		shippingPartner = this.shippingPartnerRepository.save(shippingPartner);
		shippingPartner.setShippingPartnerFiles(this.updateFile(shippingPartner, files, new ArrayList<>()));
		return new ResponseEntity<>(shippingPartner, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long id, ShippingPartnerEntity source, MultipartFile[] files,
			List<Long> removeShippingPartnerFileIds) throws BusinessException {
		ShippingPartnerEntity shippingPartner = this.getById(id);
		if (ShippingPartnerStatus.APPROVED.equals(shippingPartner.getStatus())) {
			throw new BusinessException(this.translator.toLocale("shipping.cannot.update"));
		}
		this.validEntity(source);
		shippingPartner.setName(source.getName());
		shippingPartner.setPhone(source.getPhone());
		shippingPartner.setEmail(source.getEmail());
		shippingPartner.setAddress(source.getAddress());
		shippingPartner.setContractCode(source.getContractCode());
		shippingPartner.setTaxCode(source.getTaxCode());
		shippingPartner.setDescription(source.getDescription());
		shippingPartner = this.shippingPartnerRepository.save(shippingPartner);
		shippingPartner.setShippingPartnerFiles(this.updateFile(shippingPartner, files, removeShippingPartnerFileIds));
		return new ResponseEntity<>(shippingPartner, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> accept(Long id) throws BusinessException {
		ShippingPartnerEntity shippingPartner = this.getById(id);
		if (!ShippingPartnerStatus.PENDING.equals(shippingPartner.getStatus())) {
			throw new BusinessException(this.translator.toLocale("shipping.cannot.update"));
		}
		shippingPartner.setStatus(ShippingPartnerStatus.APPROVED);
		shippingPartner.setAcceptDate(this.getSysdate());
		shippingPartner.setAcceptUser(this.getCurrentUsername());
		return new ResponseEntity<>(this.shippingPartnerRepository.save(shippingPartner), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> reject(Long id, ShippingPartnerEntity source) throws BusinessException {
		if ((source == null) || StringUtils.isNullOrEmpty(source.getRejectReason())) {
			throw new BusinessException(this.translator.toLocale("shipping.input.missing.reject.reason"));
		}
		ShippingPartnerEntity shippingPartner = this.getById(id);
		shippingPartner.setStatus(ShippingPartnerStatus.REJECTED);
		shippingPartner.setRejectReason(source.getRejectReason());
		return new ResponseEntity<>(this.shippingPartnerRepository.save(shippingPartner), HttpStatus.OK);
	}

}
