package com.tsolution._3services;

import com.tsolution._1entities.CurrencyEntity;

public interface ICurrencyService extends IBaseService<CurrencyEntity> {
}
