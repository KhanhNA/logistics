package com.tsolution._3services;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution.excetions.BusinessException;

public interface IManufacturerService extends IBaseService<ManufacturerEntity> {
	ResponseEntity<Object> find(String text, Boolean status, Integer pageNumber, Integer pageSize,
			String acceptLanguage) throws BusinessException;
}
