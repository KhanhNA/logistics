package com.tsolution._3services;

import java.io.IOException;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.ImportStatementDetailPalletEntity;
import com.tsolution._1entities.ImportStatementEntity;
import com.tsolution._1entities.dto.ImportStatementDto;
import com.tsolution._1entities.enums.ImportStatementStatus;
import com.tsolution.excetions.BusinessException;

public interface ImportStatementService extends IBaseService<ImportStatementDto> {

	ResponseEntity<Object> find(String code, Integer status, String type, Integer pageNumber, Integer pageSize)
			throws BusinessException, IOException;

	ResponseEntity<Object> createFromExportStatement(ImportStatementDto importStatementDto) throws BusinessException;

	ResponseEntity<Object> createFromRepackingPlanning(ImportStatementDto importStatementDto) throws BusinessException;

	ResponseEntity<Object> findByRepackingPlanningId(Long repackingPlanningId) throws BusinessException;

	ResponseEntity<Object> print(Long id) throws BusinessException;

	ImportStatementEntity createImportStatementEntity(ImportStatementDto importStatementDto,
			ImportStatementStatus importStatementStatus) throws BusinessException;

	ResponseEntity<Object> actuallyImport(Long id, String description,
			List<ImportStatementDetailPalletEntity> importStatementDetailPallets) throws BusinessException;
}
