package com.tsolution._3services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution.excetions.BusinessException;

public interface ProductPackingService extends IBaseService<ProductPackingEntity> {
	ResponseEntity<Object> find(String text, List<Long> exceptId, Long manufacturerId, Long distributorId,
			Integer pageNumber, Integer pageSize) throws BusinessException;

}
