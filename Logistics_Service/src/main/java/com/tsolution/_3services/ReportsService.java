package com.tsolution._3services;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution._1entities.reportdto.InventoryAndSellOutReportFilterDto;
import com.tsolution.excetions.BusinessException;

public interface ReportsService {
	ResponseEntity<Object> getInventoryAndSellOutReportDto(
			InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> exportExcelInventoryAndSellOutReportDto(
			InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto) throws BusinessException;

	ResponseEntity<Object> exportPdfInventoryAndSellOutReportDto(
			InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto) throws BusinessException;

	ResponseEntity<Object> getInventoryDto(InventoryDto inventoryDto, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> exportExcelInventoryDto(InventoryDto inventoryDto) throws BusinessException;

	ResponseEntity<Object> exportPdfInventoryDto(InventoryDto inventoryDto) throws BusinessException;

	ResponseEntity<Object> exportExcelPalletInventoryDto(PalletDetailDto palletDetailDto) throws BusinessException;

	ResponseEntity<Object> exportPdfPalletInventoryDto(PalletDetailDto palletDetailDto) throws BusinessException;

}
