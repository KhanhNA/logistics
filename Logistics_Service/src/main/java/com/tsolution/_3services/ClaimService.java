package com.tsolution._3services;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.claim.Claim;
import com.tsolution._1entities.claim.ClaimDetail;
import com.tsolution._1entities.claim.ClaimImage;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution.excetions.BusinessException;

public interface ClaimService extends IBaseService<Claim> {
	ResponseEntity<Object> find(Claim claim, Integer pageNumber, Integer pageSize) throws BusinessException;

	ResponseEntity<Object> findByReference(ClaimType claimType, Long referenceId) throws BusinessException;

	ResponseEntity<Object> accept(Long id) throws BusinessException;

	ResponseEntity<Object> reject(Long id, Claim claim) throws BusinessException;

	List<ClaimDetail> getApprovedClaimDetailsByTypeAndReferenceId(ClaimType claimType, Long referenceId)
			throws BusinessException;

	ResponseEntity<Object> updateImage(Long id, List<ClaimImage> deletes, List<ClaimImage> adds, List<ClaimImage> edits,
			MultipartFile[] files) throws BusinessException;

	ResponseEntity<Object> printPdf(Long id) throws BusinessException;

	ResponseEntity<Object> getListDamagedGoods(Claim claim, String acceptLanguage, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> confirmCompleted(Long id, List<ClaimImage> adds, MultipartFile[] files)
			throws BusinessException;
}
