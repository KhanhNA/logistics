package com.tsolution._3services.impl;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.PackingTypeEntity;
import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution._1entities.enums.ExpireDateColors;
import com.tsolution._1entities.enums.PalletStep;
import com.tsolution._1entities.enums.SaleType;
import com.tsolution._1entities.reportdto.InventoryAndSellOutReportDto;
import com.tsolution._1entities.reportdto.InventoryAndSellOutReportFilterDto;
import com.tsolution._1entities.reportdto.InventoryReportDto;
import com.tsolution._1entities.reportdto.PalletInventoryReportDto;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution._2repositories.PalletDetailRepository;
import com.tsolution._2repositories.store.StoreUserRepository;
import com.tsolution._3services.ReportsService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.ExcelTo;
import com.tsolution.utils.Jxls2xTransformerUtils;

@Service
public class ReportsServiceImpl extends LogisticsBaseService implements ReportsService {

	private static final String EXPORT_EXCEL_OUTPUT_FILE_PATH_NOT_EXISTS = "export.excel.output.file.path.not.exists";

	private static final String DETAILS = "details";

	private static final String S_S_XLSX = "%s_%s.xlsx";

	private static final String EXPORT_PDF_OUTPUT_FILE_PATH_NOT_EXISTS = "export.pdf.output.file.path.not.exists";

	private static Logger log = LogManager.getLogger(ReportsServiceImpl.class);

	@Autowired
	private StoreUserRepository storeUserRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private PalletDetailRepository palletDetailRepository;

	@Override
	public ResponseEntity<Object> getInventoryAndSellOutReportDto(
			InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		return new ResponseEntity<>(this.storeProductPackingRepository.getInventoryAndSellOutReportDto(
				inventoryAndSellOutReportFilterDto, PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> exportExcelInventoryAndSellOutReportDto(
			InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto) throws BusinessException {
		try {
			String outputFilePath = this.exportExcelInventoryAndSellOut(inventoryAndSellOutReportFilterDto);
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(
						this.translator.toLocale(ReportsServiceImpl.EXPORT_EXCEL_OUTPUT_FILE_PATH_NOT_EXISTS));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ReportsServiceImpl.log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private String exportExcelInventoryAndSellOut(InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto)
			throws BusinessException {
		try {
			Page<InventoryAndSellOutReportDto> page = this.storeProductPackingRepository
					.getInventoryAndSellOutReportDto(inventoryAndSellOutReportFilterDto,
							PageRequest.of(0, Integer.MAX_VALUE));
			Map<String, Object> map = new HashMap<>();
			String fromDate = inventoryAndSellOutReportFilterDto.getFromDate()
					.format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN));
			map.put("fromDate", fromDate);
			String toDate = inventoryAndSellOutReportFilterDto.getToDate()
					.format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN));
			map.put("toDate", toDate);
			map.put(ReportsServiceImpl.DETAILS, page.getContent());

			return Jxls2xTransformerUtils.poiTransformer("templates/REPORT_INVENTORY_AND_SELL_OUT.xlsx", map,
					this.fileStorageService.getTempExportExcel(),
					String.format(ReportsServiceImpl.S_S_XLSX,
							this.getSysdate()
									.format(DateTimeFormatter.ofPattern(Constants.PREFIX_EXPORT_EXCEL_FILE_NAME)),
							"REPORT_INVENTORY_AND_SELL_OUT"));
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<Object> exportPdfInventoryAndSellOutReportDto(
			InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto) throws BusinessException {
		try {
			String excelFilePath = this.exportExcelInventoryAndSellOut(inventoryAndSellOutReportFilterDto);
			String pdfFilePath = ExcelTo.pdf(this.fileStorageService.getLibreOfficePath(), excelFilePath,
					this.fileStorageService.getTempExportExcel());
			File file = new File(pdfFilePath);
			if (!file.exists()) {
				throw new BusinessException(
						this.translator.toLocale(ReportsServiceImpl.EXPORT_PDF_OUTPUT_FILE_PATH_NOT_EXISTS));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(pdfFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ReportsServiceImpl.log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<Object> getInventoryDto(InventoryDto inventoryDto, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		return new ResponseEntity<>(
				this.getInventory(inventoryDto, this.getSysdate(), PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}

	private Page<InventoryReportDto> getInventory(InventoryDto inventoryDto, LocalDateTime sysdate, Pageable pageable)
			throws BusinessException {
		Page<StoreProductPackingEntity> page = null;
		if (Boolean.TRUE.equals(this.storeUserRepository.existsStoreUserByStoreIdAndUsername(inventoryDto.getStoreId(),
				this.getCurrentUsername()))) {
			page = this.storeProductPackingRepository.inventory(inventoryDto, this.getCurrentUsername(), pageable);
		} else {
			throw new BusinessException(this.translator.toLocale("common.unknown"));
		}
		List<InventoryReportDto> inventoryReportDtos = new ArrayList<>();
		for (StoreProductPackingEntity storeProductPacking : page.getContent()) {
			storeProductPacking.setClassName(ExpireDateColors.findClassName(sysdate,
					storeProductPacking.getExpireDate(), storeProductPacking.getProductPacking()));
			StoreEntity store = storeProductPacking.getStore();
			ProductPackingEntity productPacking = storeProductPacking.getProductPacking();
			PackingTypeEntity packingType = productPacking.getPackingType();
			InventoryReportDto inventoryReportDto = new InventoryReportDto();
			inventoryReportDto.setStoreCode(store.getCode());
			inventoryReportDto.setStoreName(store.getName());
			inventoryReportDto.setPackCode(productPacking.getCode());
			inventoryReportDto.setProductName(productPacking.getProductNameInEnglish());
			inventoryReportDto.setBarcode(productPacking.getBarcode());
			inventoryReportDto.setPackSize(packingType.getQuantity());
			inventoryReportDto.setUom(productPacking.getUom());
			inventoryReportDto.setQuantity(storeProductPacking.getTotalQuantity());
			List<ProductPackingPriceEntity> productPackingPrices = this.productPackingPriceRepository
					.getLastestPrices(productPacking.getId(), SaleType.IN);
			if (productPackingPrices.size() != 1) {
				throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_ERROR));
			}
			ProductPackingPriceEntity currentPrice = productPackingPrices.get(0);
			inventoryReportDto.setImportPrice(currentPrice.getPrice());
			inventoryReportDto.setCurrency(currentPrice.getCurrency().getCode());
			inventoryReportDto.setAmount(
					currentPrice.getPrice().multiply(new BigDecimal(storeProductPacking.getTotalQuantity())));
			inventoryReportDto.setExpiredDate(storeProductPacking.getExpireDate()
					.format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN)));
			ExpireDateColors expireDateColor = ExpireDateColors.findByValue(storeProductPacking.getClassName());
			inventoryReportDto.setStatus(this.translator
					.toLocale(expireDateColor == null ? BusinessException.COMMON_ERROR : expireDateColor.getStatus()));
			inventoryReportDtos.add(inventoryReportDto);
		}
		return new PageImpl<>(inventoryReportDtos, pageable, page.getTotalElements());
	}

	@Override
	public ResponseEntity<Object> exportExcelInventoryDto(InventoryDto inventoryDto) throws BusinessException {
		try {
			String outputFilePath = this.exportExcelInventory(inventoryDto);
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(
						this.translator.toLocale(ReportsServiceImpl.EXPORT_EXCEL_OUTPUT_FILE_PATH_NOT_EXISTS));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ReportsServiceImpl.log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private String exportExcelInventory(InventoryDto inventoryDto) throws BusinessException {
		try {
			LocalDateTime sysdate = this.getSysdate();
			List<InventoryReportDto> inventoryReportDtos = this
					.getInventory(inventoryDto, sysdate, PageRequest.of(0, Integer.MAX_VALUE)).getContent();
			Map<String, Object> map = new HashMap<>();
			map.put("exportTime", sysdate.format(DateTimeFormatter.ofPattern(Constants.REPORT_DATETIME_PATTERN)));
			map.put(ReportsServiceImpl.DETAILS, inventoryReportDtos);
			return Jxls2xTransformerUtils.poiTransformer("templates/REPORT_INVENTORY.xlsx", map,
					this.fileStorageService.getTempExportExcel(),
					String.format(ReportsServiceImpl.S_S_XLSX,
							sysdate.format(DateTimeFormatter.ofPattern(Constants.PREFIX_EXPORT_EXCEL_FILE_NAME)),
							"REPORT_INVENTORY"));
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<Object> exportPdfInventoryDto(InventoryDto inventoryDto) throws BusinessException {
		try {
			String excelFilePath = this.exportExcelInventory(inventoryDto);
			String pdfFilePath = ExcelTo.pdf(this.fileStorageService.getLibreOfficePath(), excelFilePath,
					this.fileStorageService.getTempExportExcel());
			File file = new File(pdfFilePath);
			if (!file.exists()) {
				throw new BusinessException(
						this.translator.toLocale(ReportsServiceImpl.EXPORT_PDF_OUTPUT_FILE_PATH_NOT_EXISTS));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(pdfFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ReportsServiceImpl.log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<Object> exportExcelPalletInventoryDto(PalletDetailDto palletDetailDto)
			throws BusinessException {
		try {
			String outputFilePath = this.exportExcelPalletInventory(palletDetailDto);
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(
						this.translator.toLocale(ReportsServiceImpl.EXPORT_EXCEL_OUTPUT_FILE_PATH_NOT_EXISTS));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ReportsServiceImpl.log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private String exportExcelPalletInventory(PalletDetailDto palletDetailDto) throws BusinessException {
		try {
			palletDetailDto.setUsername(this.getCurrentUsername());
			Page<PalletDetailEntity> page = this.palletDetailRepository
					.findGroupByPalletStepAndProductPackingAndExpireDate(palletDetailDto,
							this.getManagedStoreIncludeProvideStores(), PageRequest.of(0, Integer.MAX_VALUE));
			if (!page.hasContent()) {
				throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_NO_CONTENT));
			}
			List<PalletDetailEntity> palletDetails = page.getContent();
			LocalDateTime sysdate = this.getSysdate();
			List<PalletInventoryReportDto> palletInventoryReportDtos = new ArrayList<>();
			for (PalletDetailEntity palletDetail : page.getContent()) {
				palletDetail.setClassName(ExpireDateColors.findClassName(sysdate, palletDetail.getExpireDate(),
						palletDetail.getProductPacking()));
				PalletEntity pallet = palletDetail.getPallet();
				PalletStep palletStep = PalletStep.findByValue(pallet.getStep());
				ProductPackingEntity productPacking = palletDetail.getProductPacking();
				PackingTypeEntity packingType = productPacking.getPackingType();
				PalletInventoryReportDto palletInventoryReportDto = new PalletInventoryReportDto();
				palletInventoryReportDto.setZone(this.translator.toLocale(palletStep.getStepName()));
				palletInventoryReportDto.setPalletType(pallet.getName());
				palletInventoryReportDto.setPalletCode(pallet.getCode());
				palletInventoryReportDto.setPackCode(productPacking.getCode());
				palletInventoryReportDto.setProductName(productPacking.getProductNameInEnglish());
				palletInventoryReportDto.setBarcode(productPacking.getBarcode());
				palletInventoryReportDto.setPackSize(packingType.getQuantity());
				palletInventoryReportDto.setUom(productPacking.getUom());
				palletInventoryReportDto.setQuantity(palletDetail.getQuantity());
				palletInventoryReportDto.setExpiredDate(palletDetail.getExpireDate()
						.format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN)));
				ExpireDateColors expireDateColor = ExpireDateColors.findByValue(palletDetail.getClassName());
				palletInventoryReportDto.setStatus(this.translator.toLocale(
						expireDateColor == null ? BusinessException.COMMON_ERROR : expireDateColor.getStatus()));
				palletInventoryReportDtos.add(palletInventoryReportDto);
			}
			Map<String, Object> map = new HashMap<>();
			map.put("exportTime", sysdate.format(DateTimeFormatter.ofPattern(Constants.REPORT_DATETIME_PATTERN)));
			PalletDetailEntity palletDetail = palletDetails.get(0);
			StoreEntity store = palletDetail.getPallet().getStore();
			map.put("storeName", store.getName());
			map.put("storeCode", store.getCode());
			map.put("storeAddress", store.getAddress());
			map.put(ReportsServiceImpl.DETAILS, palletInventoryReportDtos);

			return Jxls2xTransformerUtils.poiTransformer("templates/REPORT_PALLET_INVENTORY.xlsx", map,
					this.fileStorageService.getTempExportExcel(),
					String.format(ReportsServiceImpl.S_S_XLSX,
							sysdate.format(DateTimeFormatter.ofPattern(Constants.PREFIX_EXPORT_EXCEL_FILE_NAME)),
							"REPORT_PALLET_INVENTORY"));
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<Object> exportPdfPalletInventoryDto(PalletDetailDto palletDetailDto)
			throws BusinessException {
		try {
			String excelFilePath = this.exportExcelPalletInventory(palletDetailDto);
			String pdfFilePath = ExcelTo.pdf(this.fileStorageService.getLibreOfficePath(), excelFilePath,
					this.fileStorageService.getTempExportExcel());
			File file = new File(pdfFilePath);
			if (!file.exists()) {
				throw new BusinessException(
						this.translator.toLocale(ReportsServiceImpl.EXPORT_PDF_OUTPUT_FILE_PATH_NOT_EXISTS));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(pdfFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ReportsServiceImpl.log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
