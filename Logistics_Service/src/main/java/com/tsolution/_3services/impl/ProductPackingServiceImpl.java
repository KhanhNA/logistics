package com.tsolution._3services.impl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._3services.ProductPackingService;
import com.tsolution.excetions.BusinessException;

@Service
public class ProductPackingServiceImpl extends LogisticsBaseService implements ProductPackingService {

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.productPackingRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		return new ResponseEntity<>(this.productPackingRepository.findById(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(List<ProductPackingEntity> entity) throws BusinessException {
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long id, ProductPackingEntity source) {
		return new ResponseEntity<>(source, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(String text, List<Long> exceptId, Long manufacturerId, Long distributorId,
			Integer pageNumber, Integer pageSize) throws BusinessException {
		Page<ProductPackingEntity> page = this.productPackingRepository.find(text, exceptId, manufacturerId,
				distributorId, PageRequest.of(pageNumber - 1, pageSize));
		return new ResponseEntity<>(page, HttpStatus.OK);
	}

}
