package com.tsolution._3services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsolution._1entities.Config;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.dcommerce.MerchantOrderInventoryDetailDto;
import com.tsolution._1entities.dcommerce.MerchantOrderInventoryDto;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.export_statement.dto.ExportStatementDto;
import com.tsolution._1entities.merchant_order.MerchantOrderDetailEntity;
import com.tsolution._1entities.merchant_order.MerchantOrderEntity;
import com.tsolution._1entities.merchant_order.dto.InputOrderDetailDto;
import com.tsolution._1entities.merchant_order.dto.InputOrderDto;
import com.tsolution._1entities.merchant_order.dto.OutputOrderDto;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderDetailStatus;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderStatus;
import com.tsolution._1entities.store.StoreDeliveryWaitEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution._1entities.store.dto.AvailableQuantityDto;
import com.tsolution._2repositories.ExportStatementRepository;
import com.tsolution._2repositories.IMerchantOrderDetailRepository;
import com.tsolution._2repositories.store.StoreDeliveryWaitRepository;
import com.tsolution._3services.ExportStatementService;
import com.tsolution._3services.MerchantOrderService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.QRCodeGenerator;
import com.tsolution.utils.StringUtils;

@Service
public class MerchantOrderServiceImpl extends LogisticsBaseService implements MerchantOrderService {

	private static final String MERCHANT_ORDER_NOT_ENOUGH_GOODS = "merchant.order.not.enough.goods";

	private static final Logger log = LogManager.getLogger(MerchantOrderServiceImpl.class);

	@Autowired
	private IMerchantOrderDetailRepository merchantOrderDetailRepository;

	@Autowired
	private ExportStatementService exportStatementService;

	@Autowired
	private StoreDeliveryWaitRepository storeDeliveryWaitRepository;

	@Autowired
	private ExportStatementRepository exportStatementRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.merchantOrderRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository.findById(id);
		if (!oMerchantOrder.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("merchant.order.id.not.exists", id));
		}
		return new ResponseEntity<>(oMerchantOrder.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findByCode(String code) throws BusinessException {
		Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository.findOneByCode(code);
		if (!oMerchantOrder.isPresent()) {
			throw new BusinessException(this.translator.toLocale("merchant.order.code.not.exists"));
		}
		return new ResponseEntity<>(new OutputOrderDto(oMerchantOrder.get()), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> search(String text, Date fromDate, Date toDate, Integer status, Long storeId,
			Integer pageNumber, Integer pageSize) throws BusinessException {
		List<Long> storeIds = this.getManagedStoreIncludeProvideStores();
		return new ResponseEntity<>(this.merchantOrderRepository.search(text, new java.sql.Date(fromDate.getTime()),
				new java.sql.Date(toDate.getTime()), storeIds, status, storeId,
				PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> details(List<Long> merchantOrderIds) throws BusinessException {
		List<MerchantOrderDetailEntity> merchantOrderDetails = new ArrayList<>();
		for (Long merchantOrderId : merchantOrderIds) {
			Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository.findById(merchantOrderId);
			if (!oMerchantOrder.isPresent()) {
				throw new BusinessException(
						this.translator.toLocaleByFormatString("merchant.order.id.not.exists", merchantOrderId));
			}
			merchantOrderDetails.addAll(oMerchantOrder.get().getMerchantOrderDetails());
		}
		return new ResponseEntity<>(merchantOrderDetails, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getStoresExistsNotEnoughQuantity(Integer pageNumber, Integer pageSize)
			throws BusinessException {
		return new ResponseEntity<>(
				this.storeRepository.getStoresExistsNotEnoughQuantity(this.getManagedStoreIncludeProvideStores(),
						MerchantOrderStatus.STATUS_STORE_NOT_ENOUGH_QUANTITY.getValue(),
						PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getInventoryByMerchantOrder(List<String> storeCodes, List<String> productPackingCodes)
			throws BusinessException {
		List<Long> storeIds = new ArrayList<>();
		for (String storeCode : storeCodes) {
			StoreEntity store = this.storeRepository.findOneByCode(storeCode);
			if (store == null) {
				throw new BusinessException(this.translator.toLocaleByFormatString("store.code.not.exists", storeCode));
			}
			storeIds.add(store.getId());
		}
		List<Long> productPackingIds = new ArrayList<>();
		for (String productPackingCode : productPackingCodes) {
			ProductPackingEntity productPacking = this.productPackingRepository.findOneByCode(productPackingCode);
			if (productPacking == null) {
				throw new BusinessException(
						this.translator.toLocaleByFormatString("product.packing.code.not.exists", productPackingCode));
			}
			productPackingIds.add(productPacking.getId());
		}
		return new ResponseEntity<>(
				this.storeProductPackingRepository.getInventoryByMerchantOrder(storeIds, productPackingIds),
				HttpStatus.OK);
	}

	private void validMerchantOrderInventoryDto(MerchantOrderInventoryDto merchantOrderInventory)
			throws BusinessException {
		if (StringUtils.isNullOrEmpty(merchantOrderInventory.getDestinationStore())
				|| (merchantOrderInventory.getStores() == null) || merchantOrderInventory.getStores().isEmpty()
				|| (merchantOrderInventory.getMerchantOrderInventoryDetails() == null)
				|| merchantOrderInventory.getMerchantOrderInventoryDetails().isEmpty()) {
			throw new BusinessException(this.translator.toLocale("merchant.order.inventory.input.missing"));
		}
		if (!merchantOrderInventory.getStores().contains(merchantOrderInventory.getDestinationStore())) {
			throw new BusinessException(
					this.translator.toLocale("merchant.order.inventory.input.store.not.in.provides.store"));
		}
	}

	@Override
	public ResponseEntity<Object> getInventoryByMerchantOrder(MerchantOrderInventoryDto merchantOrderInventory)
			throws BusinessException {
		this.validMerchantOrderInventoryDto(merchantOrderInventory);
		List<StoreEntity> stores = this.storeRepository.findByCodes(merchantOrderInventory.getStores());
		if ((stores == null) || stores.isEmpty()) {
			throw new BusinessException(this.translator.toLocale("merchant.order.inventory.input.missing"));
		}
		List<String> storeCodes = stores.stream().filter(x -> !merchantOrderInventory.getStores().contains(x.getCode()))
				.map(StoreEntity::getCode).collect(Collectors.toList());
		if ((storeCodes != null) && !storeCodes.isEmpty()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("store.code.not.exists", storeCodes.get(0)));
		}
		Optional<StoreEntity> oDestinationStore = stores.stream()
				.filter(x -> x.getCode().equals(merchantOrderInventory.getDestinationStore())).findFirst();
		if (!oDestinationStore.isPresent()) {
			throw new BusinessException(
					this.translator.toLocale("merchant.order.inventory.input.store.not.in.provides.store"));
		}
		StoreEntity destinationStore = oDestinationStore.get();
		List<Long> storeIds = stores.stream().map(StoreEntity::getId).collect(Collectors.toList());
		if (destinationStore.getProvideStore() != null) {
			storeIds.add(destinationStore.getProvideStore().getId());
		}
		// Lấy ra danh sách thời gian chờ khi chuyển hàng từ kho khác về
		// (ưu tiên nhanh nhất)
		List<StoreDeliveryWaitEntity> storeDeliveryWaits = this.storeDeliveryWaitRepository
				.getDeliveryWaitForMerchantOrderInventory(storeIds, destinationStore.getId());

		if ((storeDeliveryWaits == null) || storeDeliveryWaits.isEmpty()) {
			throw new BusinessException(this.translator.toLocale("merchant.order.store.delivery.wait.missing"));
//			merchantOrderInventory.getMerchantOrderInventoryDetails()
//					.forEach(storeDeliveryWait -> storeDeliveryWait.setMaxDayToWait(30))
//			return new ResponseEntity<>(merchantOrderInventory, HttpStatus.OK)
		}
		return new ResponseEntity<>(this.updateDayToWaitMerchantOrderInventoryDetailDto(merchantOrderInventory,
				storeIds, storeDeliveryWaits), HttpStatus.OK);
	}

	private MerchantOrderInventoryDto updateDayToWaitMerchantOrderInventoryDetailDto(
			MerchantOrderInventoryDto merchantOrderInventory, List<Long> storeIds,
			List<StoreDeliveryWaitEntity> storeDeliveryWaits) throws BusinessException {
		for (MerchantOrderInventoryDetailDto merchantOrderInventoryDetail : merchantOrderInventory
				.getMerchantOrderInventoryDetails()) {
			ProductPackingEntity productPacking = this.productPackingRepository
					.findOneByCode(merchantOrderInventoryDetail.getProductPackingCode());
			if (productPacking == null) {
				throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.code.not.exists",
						merchantOrderInventoryDetail.getProductPackingCode()));
			}
			// Lấy tồn kho của SP ở tất cả các kho
			List<StoreProductPackingEntity> storeProductPackings = this.storeProductPackingRepository
					.getInventoryByMerchantOrder(storeIds, Collections.singletonList(productPacking.getId()));

			// Kiểm tra nếu kho muốn lấy đã đủ hàng -> Next
			StoreProductPackingEntity destinationStoreProductPacking = this.pickDestinationStoreProductPacking(
					storeProductPackings, merchantOrderInventory.getDestinationStore());
			merchantOrderInventoryDetail.setInventory(destinationStoreProductPacking.getTotalQuantity());
			if (merchantOrderInventoryDetail.getQuantity()
					.compareTo(destinationStoreProductPacking.getTotalQuantity()) <= 0) {
				merchantOrderInventoryDetail.setMaxDayToWait(0);
				continue;
			}
			// Nếu kho muốn lấy không đủ hàng -> Kiểm tra lần lượt các kho gần
			Long quantity = merchantOrderInventoryDetail.getQuantity()
					- destinationStoreProductPacking.getTotalQuantity();
			for (StoreDeliveryWaitEntity storeDeliveryWait : storeDeliveryWaits) {
				StoreProductPackingEntity storeProductPacking = this.pickDestinationStoreProductPacking(
						storeProductPackings, storeDeliveryWait.getFromStore().getCode());
				merchantOrderInventoryDetail.setMaxDayToWait(storeDeliveryWait.getMaxDayToWait());
				if (quantity.compareTo(storeProductPacking.getTotalQuantity()) <= 0) {
					quantity = 0L;
					break;
				}
				quantity = quantity - storeProductPacking.getTotalQuantity();
			}
			// Nếu tất cả các kho đều ko đủ
			if (quantity > 0) {
				Optional<Config> oConfig = this.configRepository.findOneByCode("TOTAL_TIME_PURCHASE_ORDER");
				if (!oConfig.isPresent()) {
					MerchantOrderServiceImpl.log.error("Missing config 'TOTAL_TIME_PURCHASE_ORDER'");
					throw new BusinessException(
							this.translator.toLocale(MerchantOrderServiceImpl.MERCHANT_ORDER_NOT_ENOUGH_GOODS));
				}
				merchantOrderInventoryDetail.setMaxDayToWait(
						merchantOrderInventoryDetail.getMaxDayToWait() + Integer.valueOf(oConfig.get().getValue()));
			}
		}
		return merchantOrderInventory;
	}

	private StoreProductPackingEntity pickDestinationStoreProductPacking(
			List<StoreProductPackingEntity> storeProductPackings, String storeCode) {
		Optional<StoreProductPackingEntity> oStoreProductPacking = storeProductPackings.stream()
				.filter(x -> x.getStore().getCode().equals(storeCode)).findFirst();
		StoreProductPackingEntity storeProductPacking = new StoreProductPackingEntity();
		if (oStoreProductPacking.isPresent()) {
			storeProductPacking = oStoreProductPacking.get();
		} else {
			storeProductPacking.setTotalQuantity(0L);
		}
		return storeProductPacking;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<InputOrderDto> inputOrderDtos) throws BusinessException {
		List<OutputOrderDto> list = new ArrayList<>();
		for (InputOrderDto inputOrderDto : inputOrderDtos) {
			list.add(this.createMerchantOrder(inputOrderDto));
		}
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long id, InputOrderDto source) {
		return null;
	}

	private OutputOrderDto createMerchantOrder(InputOrderDto inputOrderDto) throws BusinessException {
		// Kiểm tra thông tin đầu vào có tồn tại không
		if ((inputOrderDto.getOrderId() == null) || (inputOrderDto.getOrderId() <= 0)
				|| StringUtils.isNullOrEmpty(inputOrderDto.getOrderCode())
				|| StringUtils.isNullOrEmpty(inputOrderDto.getTownshipCode())
				|| StringUtils.isNullOrEmpty(inputOrderDto.getMerchantCode())
				|| (inputOrderDto.getGoodsReceiveForm() == null) || (inputOrderDto.getOrderDetails() == null)
				|| (inputOrderDto.getOrderDetails().isEmpty())) {
			throw new BusinessException(this.translator.toLocale("merchant.order.input.info"));
		}
		Optional<MerchantOrderEntity> oMerchantOrderEntity = this.merchantOrderRepository
				.findOneByOrderId(inputOrderDto.getOrderId());
		if (oMerchantOrderEntity.isPresent()) {
			MerchantOrderEntity merchantOrderEntity = oMerchantOrderEntity.get();
			// Trả cái này cho ông nào gọi service ^^
			return new OutputOrderDto(merchantOrderEntity);
		}
		StoreEntity fromStore = null;
		if (StringUtils.isNullOrEmpty(inputOrderDto.getFromStoreCode())) {
			List<StoreEntity> stores = this.storeRepository
					.getStoreByLocationCharge(Collections.singletonList(inputOrderDto.getTownshipCode()));
			fromStore = stores.get(0);
		} else {
			fromStore = this.findStoreByIdOrCode(null, inputOrderDto.getFromStoreCode());
		}
		if (fromStore == null) {
			throw new BusinessException(this.translator.toLocale("common.from.store.info.not.exists"));
		}
		MerchantOrderEntity merchantOrderEntity = new MerchantOrderEntity(inputOrderDto);
		merchantOrderEntity.setFromStore(fromStore);
		String qrCode = QRCodeGenerator.generate(merchantOrderEntity.getCode());
		merchantOrderEntity.setqRCode(qrCode);
		// Lấy tổng số tồn ở DC vs FC cung cấp để kiểm tra tồn kho
		this.isEnoughGoods(inputOrderDto);
		// Trạng thái này có thể bị cập nhật lại khi quét chi tiết
		merchantOrderEntity.setStatus(MerchantOrderStatus.STATUS_STORE_ENOUGH_QUANTITY.getValue());
		List<MerchantOrderDetailEntity> merchantOrderDetails = new ArrayList<>();
		for (InputOrderDetailDto inputOrderDetailDto : inputOrderDto.getOrderDetails()) {
			MerchantOrderDetailEntity merchantOrderDetailEntity = new MerchantOrderDetailEntity();
			ProductPackingEntity productPackingEntity = this.findProductPackingByIdOrCode(null,
					inputOrderDetailDto.getProductPackingCode());
			if (productPackingEntity == null) {
				throw new BusinessException(this.translator.toLocaleByFormatString(
						"common.product.packing.code.not.exists", inputOrderDetailDto.getProductPackingCode()));
			}
			merchantOrderDetailEntity.setProductPacking(productPackingEntity);
			merchantOrderDetailEntity.setDistributor(productPackingEntity.getDistributor());
			merchantOrderDetailEntity.setQuantity(inputOrderDetailDto.getQuantity());
			merchantOrderDetailEntity.setPrice(inputOrderDetailDto.getPrice());
			merchantOrderDetailEntity.setType(inputOrderDetailDto.getType());
			// Lấy ra thông tin đóng gói hàng trong kho để kiểm tra lượng tồn
			Long totalAvailableQuantity = this.storeProductPackingRepository.getTotalAvaiable(fromStore.getId(),
					inputOrderDetailDto.getProductPackingCode());
			if (totalAvailableQuantity < inputOrderDetailDto.getQuantity()) {
				merchantOrderEntity.setStatus(MerchantOrderStatus.STATUS_STORE_NOT_ENOUGH_QUANTITY.getValue());
				merchantOrderDetailEntity.setStatus(MerchantOrderDetailStatus.STATUS_NOT_ENOUGH_QUANTITY.getValue());
			} else {
				merchantOrderDetailEntity.setStatus(MerchantOrderDetailStatus.STATUS_ENOUGH_QUANTITY.getValue());
			}
			merchantOrderDetails.add(merchantOrderDetailEntity);
		}
		merchantOrderEntity = this.merchantOrderRepository.save(merchantOrderEntity);
		for (MerchantOrderDetailEntity merchantOrderDetailEntity : merchantOrderDetails) {
			merchantOrderDetailEntity.setMerchantOrder(merchantOrderEntity);
		}
		this.merchantOrderDetailRepository.saveAll(merchantOrderDetails);
		if (MerchantOrderStatus.STATUS_STORE_ENOUGH_QUANTITY.getValue().equals(merchantOrderEntity.getStatus())) {
			ExportStatementDto exportStatementDto = new ExportStatementDto(merchantOrderEntity, merchantOrderDetails);
			this.exportStatementService.createByMerchantOrder(exportStatementDto);
		}
		// Trả cái này cho ông nào gọi service ^^
		return new OutputOrderDto(merchantOrderEntity);
	}

	private void isEnoughGoods(InputOrderDto inputOrderDto) throws BusinessException {
		List<String> productPackingCodes = inputOrderDto.getOrderDetails().stream()
				.map(InputOrderDetailDto::getProductPackingCode).collect(Collectors.toList());
		List<StoreEntity> stores = this.storeRepository
				.getStoreByLocationCharge(Collections.singletonList(inputOrderDto.getTownshipCode()));
		if (stores.isEmpty()) {
			throw new BusinessException(
					this.translator.toLocale(MerchantOrderServiceImpl.MERCHANT_ORDER_NOT_ENOUGH_GOODS));
		}
		List<String> storeCodes = new ArrayList<>();
		for (StoreEntity store : stores) {
			storeCodes.add(store.getCode());
			StoreEntity provideStore = store.getProvideStore();
			if (provideStore != null) {
				storeCodes.add(provideStore.getCode());
			}
		}
		List<AvailableQuantityDto> availableQuantityDtos = this.storeRepository.getAvailableQuantity(storeCodes,
				productPackingCodes);
		for (InputOrderDetailDto inputOrderDetailDto : inputOrderDto.getOrderDetails()) {
			Long availableQuantity = availableQuantityDtos.stream()
					.filter(x -> x.getProductPackingCode()
							.equalsIgnoreCase(inputOrderDetailDto.getProductPackingCode()))
					.map(AvailableQuantityDto::getQuantity).findFirst().orElse(0L);
			if (availableQuantity < inputOrderDetailDto.getQuantity()) {
				throw new BusinessException(
						this.translator.toLocale(MerchantOrderServiceImpl.MERCHANT_ORDER_NOT_ENOUGH_GOODS));
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> cancel(String code) throws BusinessException {
		Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository.findOneByCode(code);
		if (!oMerchantOrder.isPresent()) {
			throw new BusinessException(this.translator.toLocale("merchant.order.code.not.exists"));
		}
		MerchantOrderEntity merchantOrder = oMerchantOrder.get();
		if (!MerchantOrderStatus.STATUS_STORE_NOT_ENOUGH_QUANTITY.getValue().equals(merchantOrder.getStatus())
				&& !MerchantOrderStatus.STATUS_STORE_ENOUGH_QUANTITY.getValue().equals(merchantOrder.getStatus())) {
			throw new BusinessException(this.translator.toLocale("merchant.order.cancel.conditions"));
		}
		Optional<ExportStatementEntity> oExportStatement = this.exportStatementRepository
				.findOneByMerchantOrderId(merchantOrder.getId());
		if (oExportStatement.isPresent()) {
			this.exportStatementService.cancel(oExportStatement.get().getId());
		}
		merchantOrder.setStatus(MerchantOrderStatus.STATUS_CANCELED.getValue());
		merchantOrder = this.merchantOrderRepository.save(merchantOrder);
		return new ResponseEntity<>(merchantOrder, HttpStatus.OK);
	}

}
