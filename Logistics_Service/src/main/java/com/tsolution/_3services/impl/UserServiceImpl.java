package com.tsolution._3services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;

import com.tsolution._1entities.Config;
import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.UserEntity;
import com.tsolution._1entities.dto.RoleDto;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.StoreUserEntity;
import com.tsolution._2repositories.DistributorRepository;
import com.tsolution._2repositories.store.StoreUserRepository;
import com.tsolution._3services.EmailService;
import com.tsolution._3services.UserService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.SingleSignOnUtils;
import com.tsolution.utils.StringUtils;

@Service
public class UserServiceImpl extends LogisticsBaseService implements UserService {

	private static final Logger log = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	private StoreUserRepository storeUserRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private SingleSignOnUtils singleSignOnUtils;

	@Autowired
	private EmailService emailService;

	@Value("${sso.default.password}")
	private String defaultPassword;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.userRepository.findAll(), HttpStatus.OK);
	}

	private UserEntity findByUsername(String username) throws BusinessException {
		Optional<UserEntity> oUser = this.userRepository.findOneByUsername(username);
		if (!oUser.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("user.id.not.exists", username));
		}
		return oUser.get();
	}

	@Override
	public ResponseEntity<Object> findById(String authorization, String acceptLanguage, String username)
			throws BusinessException {
		UserEntity user = this.findByUsername(username);
		try {
			ResponseEntity<Object> result = this.singleSignOnUtils.get(authorization, acceptLanguage,
					"/user/" + username + "/role");
			if (!HttpStatus.OK.equals(result.getStatusCode())) {
				return new ResponseEntity<>(user, HttpStatus.OK);
			}
			Object oListRole = result.getBody();
			List<RoleDto> listRole = this.getListValueFromResponse(oListRole, RoleDto.class);
			user.setRole(listRole.isEmpty() ? null : listRole.get(0).getId());
		} catch (HttpStatusCodeException e) {
			UserServiceImpl.log.error(StringUtils.getExceptionMessage(e), e);
			user.setStatus(null);
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(Boolean status, String text, List<Long> storeIds, Integer pageNumber,
			Integer pageSize) throws BusinessException {
		return new ResponseEntity<>(
				this.userRepository.find(status, text, storeIds, PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(String authorization, String acceptLanguage, UserEntity userEntityInput)
			throws BusinessException {
		Optional<UserEntity> oUser = this.userRepository.findOneByUsername(userEntityInput.getUsername());
		if (oUser.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("user.username.is.exists", userEntityInput.getUsername()));
		}
		if (userEntityInput.getDistributorId() != null) {
			Optional<DistributorEntity> oDistributor = this.distributorRepository
					.findById(userEntityInput.getDistributorId());
			if (!oDistributor.isPresent()) {
				throw new BusinessException(this.translator.toLocaleByFormatString("distributor.id.not.exists",
						userEntityInput.getDistributorId()));
			}
		}
		userEntityInput.setStatus(true);
		UserEntity user = this.userRepository.save(userEntityInput);

		if (userEntityInput.getStores() != null) {
			this.handleStoreUser(authorization, acceptLanguage, userEntityInput.getUsername(),
					userEntityInput.getStores(), userEntityInput.getRole());
		}

		if (userEntityInput.getRole() != null) {
			userEntityInput.setRoles(Collections.singletonList(new RoleDto(userEntityInput.getRole())));
		}
		userEntityInput.setEnabled(true);
		ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage, "/user",
				userEntityInput);

		if (!HttpStatus.OK.equals(result.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("user.add.failed"));
		}
		if (!StringUtils.isNullOrEmpty(user.getEmail())) {
			this.emailService.sendSimpleMessage(user.getEmail(),
					this.translator.toLocaleByFormatString("user.notification.email.create", user.getUsername()),
					String.format("Username: %s %nPassword: %s %nBEST,", user.getUsername(), this.defaultPassword));
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	private void handleStoreUser(String authorization, String acceptLanguage, String username, List<StoreEntity> stores,
			Long roleId) throws BusinessException {
		boolean isSetHeadOfWarehouse = false;
		if (roleId != null) {
			ResponseEntity<Object> result = this.singleSignOnUtils.get(authorization, acceptLanguage,
					"/role/" + roleId);
			if (!HttpStatus.OK.equals(result.getStatusCode())) {
				throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_ERROR));
			}
			List<RoleDto> roles = this.getListValueFromResponse(result.getBody(), RoleDto.class);
			Optional<Config> oConfig = this.configRepository.findOneByCode("ROLE_HEAD_OF_WAREHOUSE");
			if (!roles.isEmpty() && oConfig.isPresent()) {
				List<String> roleCodes = Arrays.asList(oConfig.get().getValue().split("\\|")).stream().map(String::trim)
						.map(String::toUpperCase).collect(Collectors.toList());
				isSetHeadOfWarehouse = roleCodes.contains(roles.get(0).getRoleName().trim().toUpperCase());
			}
		}
		List<StoreUserEntity> storeUsers = new ArrayList<>();
		for (StoreEntity store : stores) {
			Optional<StoreEntity> oStore = this.storeRepository
					.findById((store == null) || (store.getId() == null) ? 0 : store.getId());
			if (!oStore.isPresent()) {
				throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
			}
			store = oStore.get();
			if (isSetHeadOfWarehouse) {
				store.setUsername(username);
				store = this.storeRepository.save(store);
			}
			StoreUserEntity storeUser = new StoreUserEntity();
			storeUser.setUsername(username);
			storeUser.setStore(store);
			storeUsers.add(storeUser);
		}
		this.storeUserRepository.resetStoreUser(username);
		this.storeUserRepository.saveAll(storeUsers);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(String authorization, String acceptLanguage, String username,
			UserEntity source) throws BusinessException {
		UserEntity user = this.findByUsername(username);
		if (source.getDistributorId() != null) {
			Optional<DistributorEntity> oDistributor = this.distributorRepository.findById(source.getDistributorId());
			if (!oDistributor.isPresent()) {
				throw new BusinessException(
						this.translator.toLocaleByFormatString("distributor.id.not.exists", source.getDistributorId()));
			}
		}

		user.setFirstName(source.getFirstName());
		user.setLastName(source.getLastName());
		user.setTel(source.getTel());
		user.setEmail(source.getEmail());
		user.setDistributorId(source.getDistributorId());
		user = this.userRepository.save(user);

		if (source.getStores() != null) {
			this.handleStoreUser(authorization, acceptLanguage, username, source.getStores(), source.getRole());
		}

		if (source.getRole() != null) {
			source.setRoles(Collections.singletonList(new RoleDto(source.getRole())));
		}

		Map<String, Long> pathVariables = new HashMap<>();
		pathVariables.put("id", 0L);

		ResponseEntity<Object> result = this.singleSignOnUtils.patch(authorization, acceptLanguage, "/user/{id}",
				pathVariables, source);

		if (!HttpStatus.OK.equals(result.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("user.update.failed"));
		}

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> active(String authorization, String acceptLanguage, UserEntity user)
			throws BusinessException {
		this.findByUsername(user.getUsername());
		this.userRepository.activate(user.getUsername());
		ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage, "/user/active",
				user.getUsername());
		if (!HttpStatus.OK.equals(result.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("user.active.failed"));
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> deactive(String authorization, String acceptLanguage, UserEntity user)
			throws BusinessException {
		this.findByUsername(user.getUsername());
		this.userRepository.deactive(user.getUsername());
		ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage, "/user/deactive",
				user.getUsername());
		if (!HttpStatus.OK.equals(result.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("user.deactive.failed"));
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> resetPassword(String authorization, String acceptLanguage, UserEntity user)
			throws BusinessException {
		user = this.findByUsername(user.getUsername());
		ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage,
				"/user/reset-password", user.getUsername());

		if (!HttpStatus.OK.equals(result.getStatusCode())) {
			throw new BusinessException(this.translator.toLocale("user.reset.password.failed"));
		}
		if (!StringUtils.isNullOrEmpty(user.getEmail())) {
			this.emailService.sendSimpleMessage(user.getEmail(),
					this.translator.toLocale("user.notification.reset.password"),
					String.format("Username: %s %nPassword: %s %nBEST,", user.getUsername(), this.defaultPassword));
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
