package com.tsolution._3services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution._2repositories.ManufacturerRepository;
import com.tsolution._3services.IManufacturerService;
import com.tsolution.excetions.BusinessException;

@Service
public class ManufacturerServiceImpl extends LogisticsBaseService implements IManufacturerService {

	@Autowired
	private ManufacturerRepository manufacturerRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(List<ManufacturerEntity> entities) throws BusinessException {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long id, ManufacturerEntity source) throws BusinessException {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(String text, Boolean status, Integer pageNumber, Integer pageSize,
			String acceptLanguage) throws BusinessException {
		return new ResponseEntity<>(this.manufacturerRepository.find(text, status,
				PageRequest.of(pageNumber - 1, pageSize), acceptLanguage), HttpStatus.OK);
	}

}
