package com.tsolution._3services.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.tsolution._1entities.PackingTypeEntity;
import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.ProductEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.UserEntity;
import com.tsolution._1entities.enums.PalletStep;
import com.tsolution._1entities.oauth2.OAuth2AuthenticationDto;
import com.tsolution._1entities.oauth2.OAuth2AuthenticationDto.Authorities;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._2repositories.ConfigRepository;
import com.tsolution._2repositories.CountryRepository;
import com.tsolution._2repositories.IMerchantOrderDetailRepository;
import com.tsolution._2repositories.IMerchantRepository;
import com.tsolution._2repositories.IPackingTypeRepository;
import com.tsolution._2repositories.IProductRepository;
import com.tsolution._2repositories.MerchantOrderRepository;
import com.tsolution._2repositories.PalletRepository;
import com.tsolution._2repositories.ProductPackingPriceRepository;
import com.tsolution._2repositories.ProductPackingRepository;
import com.tsolution._2repositories.UserRepository;
import com.tsolution._2repositories.store.StoreProductPackingDetailRepository;
import com.tsolution._2repositories.store.StoreProductPackingRepository;
import com.tsolution._2repositories.store.StoreRepository;
import com.tsolution._3services.ClaimService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;

public abstract class LogisticsBaseService extends BaseService {

	@Autowired
	protected CountryRepository countryRepository;

	@Autowired
	protected StoreRepository storeRepository;

	@Autowired
	protected IMerchantRepository merchantRepository;

	@Autowired
	protected IProductRepository productRepository;

	@Autowired
	protected ProductPackingRepository productPackingRepository;

	@Autowired
	protected ProductPackingPriceRepository productPackingPriceRepository;

	@Autowired
	protected IPackingTypeRepository packingTypeRepository;

	@Autowired
	protected StoreProductPackingRepository storeProductPackingRepository;

	@Autowired
	protected StoreProductPackingDetailRepository storeProductPackingDetailRepository;

	@Autowired
	protected MerchantOrderRepository merchantOrderRepository;

	@Autowired
	protected IMerchantOrderDetailRepository mechantOrderDetailRepository;

	@Autowired
	private PalletRepository palletRepository;

	@Autowired
	protected UserRepository userRepository;

	@Autowired
	protected ClaimService claimService;

	@Autowired
	protected ConfigRepository configRepository;

	protected boolean hasAuthority(String... permissions) throws BusinessException {
		OAuth2AuthenticationDto auth2AuthenticationDto = this.getCurrentOAuth2Details();
		if ((auth2AuthenticationDto == null) || (permissions == null) || (permissions.length <= 0)) {
			return false;
		}
		List<Authorities> authorities = auth2AuthenticationDto.getAuthorities();
		if ((authorities == null) || authorities.isEmpty()) {
			return false;
		}
		for (Authorities authority : authorities) {
			for (String permission : permissions) {
				if ((authority != null) && permission.equalsIgnoreCase(authority.getAuthority())) {
					return true;
				}
			}
		}
		return false;
	}

	protected UserEntity getCurrentUser() throws BusinessException {
		String username = this.getCurrentUsername();
		Optional<UserEntity> oUser = this.userRepository.findOneByUsername(username);
		if (!oUser.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("user.username.not.exists", username));
		}
		return oUser.get();
	}

	protected List<Long> getManagedStoreIncludeProvideStores() throws BusinessException {
		UserEntity user = this.getCurrentUser();
		List<StoreEntity> stores = user.getStores();
		List<Long> storeIds = stores.stream().map(StoreEntity::getId).collect(Collectors.toList());
		stores.addAll(this.storeRepository.getStoresByProvideStore(storeIds));
		return stores.stream().map(StoreEntity::getId).collect(Collectors.toList());
	}

	protected StoreEntity findStoreByIdOrCode(Long id, String code) {
		StoreEntity storeEntity = this.storeRepository.findById(id == null ? -1 : id)
				.orElse(this.storeRepository.findOneByCode(code));
		if ((storeEntity != null) && !storeEntity.getCode().equalsIgnoreCase(code)
				&& !StringUtils.isNullOrEmpty(code)) {
			return null;
		}
		return storeEntity;
	}

	protected ProductPackingEntity findProductPackingByIdOrCode(Long id, String code) {
		ProductPackingEntity productPackingEntity = this.productPackingRepository.findById(id == null ? -1 : id)
				.orElse(this.productPackingRepository.findOneByCode(code));
		if ((productPackingEntity != null) && !productPackingEntity.getCode().equalsIgnoreCase(code)
				&& !StringUtils.isNullOrEmpty(code)) {
			return null;
		}
		return productPackingEntity;
	}

	protected ProductEntity findProductByIdOrCode(Long id, String code) {
		ProductEntity productEntity = this.productRepository.findById(id == null ? -1 : id)
				.orElse(this.productRepository.findOneByCode(code));
		if ((productEntity != null) && !productEntity.getCode().equalsIgnoreCase(code)
				&& !StringUtils.isNullOrEmpty(code)) {
			return null;
		}
		return productEntity;
	}

	protected PackingTypeEntity findPackingTypeByIdOrCode(Long id, String code) {
		PackingTypeEntity packingTypeEntity = this.packingTypeRepository.findById(id == null ? -1 : id)
				.orElse(this.packingTypeRepository.findOneByCode(code));
		if ((packingTypeEntity != null) && !packingTypeEntity.getCode().equalsIgnoreCase(code)
				&& !StringUtils.isNullOrEmpty(code)) {
			return null;
		}
		return packingTypeEntity;
	}

	protected PalletEntity getPallet(Long id, PalletStep palletStep) throws BusinessException {
		Optional<PalletEntity> oPallet = this.palletRepository.findById(id == null ? -1 : id);
		if (!oPallet.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("pallet.id.not.exists", id == null ? -1 : id));
		}
		if (!palletStep.getValue().equals(oPallet.get().getStep())) {
			throw new BusinessException(this.translator.toLocale("pallet.check.step.invalid"));
		}
		return oPallet.get();
	}

	public String generateImportStatementCode(ProductPackingEntity productPacking, LocalDateTime expireDate)
			throws BusinessException {
		if ((productPacking == null) || (productPacking.getProduct() == null)
				|| StringUtils.isNullOrEmpty(productPacking.getProduct().getCode())
				|| (productPacking.getPackingType() == null)
				|| StringUtils.isNullOrEmpty(productPacking.getPackingType().getCode()) || (expireDate == null)) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_ERROR));
		}
		return String.format("%s %n %s %n %s", productPacking.getProduct().getCode(),
				productPacking.getPackingType().getCode(), expireDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
	}
}
