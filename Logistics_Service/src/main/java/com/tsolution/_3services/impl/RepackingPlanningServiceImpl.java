package com.tsolution._3services.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.RepackingPlanningDetailEntity;
import com.tsolution._1entities.RepackingPlanningDetailPalletEntity;
import com.tsolution._1entities.RepackingPlanningDetailRepackedEntity;
import com.tsolution._1entities.RepackingPlanningEntity;
import com.tsolution._1entities.claim.ClaimDetail;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution._1entities.dto.RepackingPlanningDto;
import com.tsolution._1entities.dto.RepackingPlanningPrintQrCodeDto;
import com.tsolution._1entities.enums.ImportPoStatus;
import com.tsolution._1entities.enums.PalletStep;
import com.tsolution._1entities.enums.RepackingPlanningStatus;
import com.tsolution._1entities.importpo.ImportPoEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._2repositories.DistributorRepository;
import com.tsolution._2repositories.IRepackingPlanningDetailPalletRepository;
import com.tsolution._2repositories.IRepackingPlanningDetailRepackedRepository;
import com.tsolution._2repositories.IRepackingPlanningDetailRepository;
import com.tsolution._2repositories.PalletDetailRepository;
import com.tsolution._2repositories.RepackingPlanningRepository;
import com.tsolution._2repositories.importpo.ImportPoRepository;
import com.tsolution._3services.PalletService;
import com.tsolution._3services.RepackingPlanningService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DatetimeUtils;
import com.tsolution.utils.Jxls2xTransformerUtils;
import com.tsolution.utils.JxlsTransformerUtils;
import com.tsolution.utils.PartitionListUtils;
import com.tsolution.utils.QRCodeGenerator;

@Service
public class RepackingPlanningServiceImpl extends LogisticsBaseService implements RepackingPlanningService {

	private static final String YYYY_M_MDD = "yyyyMMdd";

	private static final String PRODUCT_PACKING_ID_NOT_EXISTS2 = "product.packing.id.not.exists";

	private static final String PRODUCT_PACKING_ID_NOT_EXISTS = RepackingPlanningServiceImpl.PRODUCT_PACKING_ID_NOT_EXISTS2;

	private static final String REPACKINGPLANNING_ID_NOT_EXISTS = "repackingplanning.id.not.exists";

	private static final Logger logger = LogManager.getLogger(RepackingPlanningServiceImpl.class);

	@Autowired
	RepackingPlanningRepository repackingPlanningRepository;

	@Autowired
	IRepackingPlanningDetailRepository repackingPlanningDetailRepository;

	@Autowired
	IRepackingPlanningDetailPalletRepository repackingPlanningDetailPalletRepository;

	@Autowired
	IRepackingPlanningDetailRepackedRepository repackingPlanningDetailRepackedRepository;

	@Autowired
	PalletService palletService;

	@Autowired
	PalletDetailRepository palletDetailRepository;

	@Autowired
	ImportPoRepository importPoRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private DistributorRepository distributorRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.repackingPlanningRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<RepackingPlanningEntity> oRepackingPlanning = this.repackingPlanningRepository.findById(id);
		if (!oRepackingPlanning.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		return new ResponseEntity<>(oRepackingPlanning.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findDetailRepackedById(Long id) throws BusinessException {
		Optional<RepackingPlanningEntity> oRepackingPlanning = this.repackingPlanningRepository.findById(id);
		if (!oRepackingPlanning.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		if (!RepackingPlanningStatus.REPACKED.getValue().equals(oRepackingPlanning.get().getStatus())) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.cancel.check.status.is.repacked"));
		}
		List<RepackingPlanningDetailRepackedEntity> repackingPlanningDetailRepackeds = new ArrayList<>();
		for (RepackingPlanningDetailEntity repackingPlanningDetail : oRepackingPlanning.get()
				.getRepackingPlanningDetails()) {
			repackingPlanningDetailRepackeds.addAll(repackingPlanningDetail.getRepackingPlanningDetailRepackeds());
		}
		return new ResponseEntity<>(repackingPlanningDetailRepackeds, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(RepackingPlanningDto repackingPlanningDto, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		return new ResponseEntity<>(this.repackingPlanningRepository.find(repackingPlanningDto,
				this.getCurrentUsername(), PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	private List<RepackingPlanningDetailPalletEntity> specifyPalletDetailForRepackingPlanningDetail(
			Long repackingPlanningDetailId, Long inputQuantity, Long storeId, Long productPackingId,
			LocalDateTime expireDate) throws BusinessException {
		// Thực hiện xác định pallet_detail lấy hàng và trừ tồn
		List<RepackingPlanningDetailPalletEntity> repackingPlanningDetailPallets = new ArrayList<>();
		Optional<RepackingPlanningDetailEntity> oRepackingPlanningDetail = this.repackingPlanningDetailRepository
				.findById(repackingPlanningDetailId);
		if (!oRepackingPlanningDetail.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("repackingplanning.detail.id.not.exists",
					repackingPlanningDetailId));
		}
		long quantity = inputQuantity.longValue();
		List<PalletDetailEntity> palletDetails = this.palletDetailRepository.findByProductPackingAndExpireDate(storeId,
				productPackingId, DatetimeUtils.localDateTime2SqlDate(expireDate),
				PalletStep.IMPORT_PO_UPDATE_PALLET.getValue());
		if (palletDetails.stream().mapToLong(PalletDetailEntity::getQuantity).sum() < quantity) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("repackingplanning.total.quantity.product.not.enough",
							productPackingId, expireDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
		}
		for (PalletDetailEntity palletDetailEntity : palletDetails) {
			if (quantity == 0) {
				break;
			}
			RepackingPlanningDetailPalletEntity repackingPlanningDetailPallet = new RepackingPlanningDetailPalletEntity();
			repackingPlanningDetailPallet.setRepackingPlanningDetail(oRepackingPlanningDetail.get());
			repackingPlanningDetailPallet.setPalletDetail(palletDetailEntity);
			if (quantity < palletDetailEntity.getQuantity()) {
				repackingPlanningDetailPallet.setQuantity(quantity);
				this.palletService.pullDownPalletDetail(palletDetailEntity.getId(), quantity);
				quantity = 0;
			} else {
				quantity = quantity - palletDetailEntity.getQuantity();
				repackingPlanningDetailPallet.setQuantity(palletDetailEntity.getQuantity());
				this.palletService.pullDownPalletDetail(palletDetailEntity.getId(), palletDetailEntity.getQuantity());
			}
			repackingPlanningDetailPallets.add(repackingPlanningDetailPallet);
			// Xác định Import_PO để cập nhập lại trạng thái
			if (palletDetailEntity.getImportPoDetailPallet() == null) {
				throw new BusinessException(
						this.translator.toLocale("pallet.detail.can.not.find.import.po.detail.pallet"));
			}
			ImportPoEntity importPo = palletDetailEntity.getImportPoDetailPallet().getImportPo();
			importPo.setStatus(ImportPoStatus.OUT_OF_PALLET.getValue());
			this.importPoRepository.save(importPo);
		}
		return repackingPlanningDetailPallets;
	}

	private void validRepackingPlanning(RepackingPlanningEntity repackingPlanning, LocalDateTime sysdate)
			throws BusinessException {
		if ((repackingPlanning.getStore() == null) || (repackingPlanning.getStore().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.missing.store"));
		}
		Optional<StoreEntity> oStore = this.storeRepository.findById(repackingPlanning.getStore().getId());
		if (!oStore.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("store.id.not.exists",
					repackingPlanning.getStore().getId()));
		}
		if ((repackingPlanning.getDistributor() == null) || (repackingPlanning.getDistributor().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.missing.distributor"));
		}
		Optional<DistributorEntity> oDistributor = this.distributorRepository
				.findById(repackingPlanning.getDistributor().getId());
		if (!oDistributor.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("distributor.id.not.exists",
					repackingPlanning.getDistributor().getId()));
		}
		this.validRepackingPlanningDetails(repackingPlanning, sysdate);
	}

	private void validRepackingPlanningDetails(RepackingPlanningEntity repackingPlanning, LocalDateTime sysdate)
			throws BusinessException {
		if ((repackingPlanning.getRepackingPlanningDetails() == null)
				|| repackingPlanning.getRepackingPlanningDetails().isEmpty()) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.missing.details"));
		}
		List<RepackingPlanningDetailEntity> repackingPlanningDetails = repackingPlanning.getRepackingPlanningDetails();
		for (RepackingPlanningDetailEntity repackingPlanningDetail : repackingPlanningDetails) {
			this.validRepackingPlanningDetail(repackingPlanningDetail, sysdate);
		}
		// Nếu đã có claim thì cấm sửa
		if (repackingPlanning.getId() != null) {
			List<ClaimDetail> claimDetails = this.claimService.getApprovedClaimDetailsByTypeAndReferenceId(
					ClaimType.REPACKING_PLANNING, repackingPlanning.getId());
			if ((claimDetails != null) && !claimDetails.isEmpty()) {
				throw new BusinessException(this.translator.toLocale("repackingplanning.is.in.claim"));
			}
		}
	}

	private void validRepackingPlanningDetail(RepackingPlanningDetailEntity sourceDetail, LocalDateTime sysdate)
			throws BusinessException {
		if ((sourceDetail.getProductPacking() == null) || (sourceDetail.getProductPacking().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.detail.missing.product.packing"));
		}
		Optional<ProductPackingEntity> oProductPacking = this.productPackingRepository
				.findById(sourceDetail.getProductPacking().getId());
		if (!oProductPacking.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(RepackingPlanningServiceImpl.PRODUCT_PACKING_ID_NOT_EXISTS,
							sourceDetail.getProductPacking().getId()));
		}
		if (sourceDetail.getQuantity() <= 0) {
			throw new BusinessException(
					this.translator.toLocale("repacking.planning.detail.quantity.must.be.greater.zero"));
		}
		if (sourceDetail.getExpireDate().toLocalDate().isBefore(sysdate.toLocalDate())) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.out.of.date",
					oProductPacking.get().getCode()));
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<RepackingPlanningEntity> entities) throws BusinessException {
		LocalDateTime sysdate = this.getSysdate();
		List<RepackingPlanningEntity> list = new ArrayList<>();
		for (RepackingPlanningEntity repackingPlanning : entities) {
			if (repackingPlanning.getId() != null) {
				throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
			}
			this.validRepackingPlanning(repackingPlanning, sysdate);
			String code = String.format("%s-REPACKING-%s",
					sysdate.format(DateTimeFormatter.ofPattern(RepackingPlanningServiceImpl.YYYY_M_MDD)),
					sysdate.format(DateTimeFormatter.ofPattern("HHmmssSSS")));
			repackingPlanning.setPlanningDate(sysdate);
			repackingPlanning.setCode(code);
			repackingPlanning.setStatus(RepackingPlanningStatus.NEW.getValue());
			RepackingPlanningEntity tempRepackingPlanning = this.repackingPlanningRepository.save(repackingPlanning);
			List<RepackingPlanningDetailEntity> tempRepackingPlanningDetails = new ArrayList<>();
			for (RepackingPlanningDetailEntity repackingPlanningDetail : repackingPlanning
					.getRepackingPlanningDetails()) {
				tempRepackingPlanningDetails
						.add(this.createRepackingPlanningDetail(tempRepackingPlanning, repackingPlanningDetail));
			}
			tempRepackingPlanning.setRepackingPlanningDetails(tempRepackingPlanningDetails);
			list.add(tempRepackingPlanning);

		}
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	private RepackingPlanningDetailEntity createRepackingPlanningDetail(RepackingPlanningEntity repackingPlanning,
			RepackingPlanningDetailEntity repackingPlanningDetail) throws BusinessException {
		repackingPlanningDetail.setRepackingPlanning(repackingPlanning);
		RepackingPlanningDetailEntity tempRepackingPlanningDetail = this.repackingPlanningDetailRepository
				.save(repackingPlanningDetail);
		List<RepackingPlanningDetailPalletEntity> repackingPlanningDetailPallets = this
				.specifyPalletDetailForRepackingPlanningDetail(tempRepackingPlanningDetail.getId(),
						tempRepackingPlanningDetail.getQuantity(), repackingPlanning.getStore().getId(),
						tempRepackingPlanningDetail.getProductPacking().getId(),
						tempRepackingPlanningDetail.getExpireDate());
		tempRepackingPlanningDetail.setRepackingPlanningDetailPallets(
				this.repackingPlanningDetailPalletRepository.saveAll(repackingPlanningDetailPallets));
		return tempRepackingPlanningDetail;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, RepackingPlanningEntity source) throws BusinessException {
		if ((id == null) || (source.getId() == null) || !id.equals(source.getId())) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.missing.id"));
		}
		Optional<RepackingPlanningEntity> oTarget = this.repackingPlanningRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		if (!RepackingPlanningStatus.NEW.getValue().equals(oTarget.get().getStatus())) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.cancel.check.status.is.new"));
		}
		RepackingPlanningEntity target = oTarget.get();
		List<ClaimDetail> claimDetails = null;
		try {
			claimDetails = this.claimService.getApprovedClaimDetailsByTypeAndReferenceId(ClaimType.REPACKING_PLANNING,
					target.getId());
		} catch (Exception e) {
			RepackingPlanningServiceImpl.logger.info(e.getMessage(), e);
		}
		if (claimDetails != null) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.claimed.update.planning"));
		}
		target.setDescription(source.getDescription());
		target = this.repackingPlanningRepository.save(target);
		// Trả tất cả hàng về các pallet_detail rồi muốn làm gì thì làm @@
		List<RepackingPlanningDetailEntity> targetDetails = target.getRepackingPlanningDetails();
		this.backToPallet(targetDetails);
		// Lọc ra IDs không nằm trong list detail sau khi sửa => xóa
		List<RepackingPlanningDetailEntity> listDelete = targetDetails.stream().filter(
				ipod -> source.getRepackingPlanningDetails().stream().noneMatch(x -> ipod.getId().equals(x.getId())))
				.collect(Collectors.toList());
		if (targetDetails.removeAll(listDelete)) {
			this.repackingPlanningDetailRepository.deleteAll(listDelete);
		}
		LocalDateTime sysdate = this.getSysdate();
		// Add Or Edit Details
		for (RepackingPlanningDetailEntity sourceDetail : source.getRepackingPlanningDetails()) {
			this.validRepackingPlanningDetail(sourceDetail, sysdate);
			if (sourceDetail.getId() == null) {
				targetDetails.add(this.createRepackingPlanningDetail(target, sourceDetail));
			} else {
				int index = targetDetails.stream().map(RepackingPlanningDetailEntity::getId)
						.collect(Collectors.toList()).indexOf(sourceDetail.getId());
				if (index < 0) {
					throw new BusinessException(this.translator.toLocale("repackingplanning.cannot.find.detail"));
				}
				RepackingPlanningDetailEntity temp = targetDetails.get(index);
				temp.setQuantity(sourceDetail.getQuantity());
				temp.setRepackingPlanningDetailPallets(new ArrayList<>());
				temp = this.repackingPlanningDetailRepository.save(temp);
				List<RepackingPlanningDetailPalletEntity> repackingPlanningDetailPallets = this
						.specifyPalletDetailForRepackingPlanningDetail(temp.getId(), temp.getQuantity(),
								target.getStore().getId(), temp.getProductPacking().getId(), temp.getExpireDate());
				temp.setRepackingPlanningDetailPallets(
						this.repackingPlanningDetailPalletRepository.saveAll(repackingPlanningDetailPallets));
				targetDetails.set(index, temp);
			}
		}
		target.setRepackingPlanningDetails(targetDetails);
		return new ResponseEntity<>(target, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> repack(Long id, List<RepackingPlanningDetailRepackedEntity> sourceDetailRepackeds)
			throws BusinessException {
		Optional<RepackingPlanningEntity> oTarget = this.repackingPlanningRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		RepackingPlanningEntity target = oTarget.get();
		LocalDateTime sysdate = this.getSysdate();
		this.validRepackingPlanningDetailRepackeds(target, sourceDetailRepackeds, sysdate);
		List<RepackingPlanningDetailEntity> targetDetails = target.getRepackingPlanningDetails();
		target.setRepackedDate(sysdate);
		target.setStatus(RepackingPlanningStatus.PLANNED.getValue());
		this.repackingPlanningRepository.save(target);
		for (int i = 0; i < sourceDetailRepackeds.size(); i++) {
			RepackingPlanningDetailRepackedEntity sourceDetailRepacked = sourceDetailRepackeds.get(i);
			ProductPackingEntity productPacking = sourceDetailRepacked.getProductPacking();
			if ((productPacking == null) || (productPacking.getId() == null)) {
				throw new BusinessException(this.translator
						.toLocaleByFormatString(RepackingPlanningServiceImpl.PRODUCT_PACKING_ID_NOT_EXISTS, -1));
			}
			if ((productPacking.getPackingType() == null) || (productPacking.getPackingType().getId() == null)) {
				Optional<ProductPackingEntity> pp = this.productPackingRepository.findById(productPacking.getId());
				if (!pp.isPresent()) {
					throw new BusinessException(this.translator.toLocaleByFormatString(
							RepackingPlanningServiceImpl.PRODUCT_PACKING_ID_NOT_EXISTS, productPacking.getId()));
				}
				sourceDetailRepacked.setProductPacking(pp.get());
				sourceDetailRepackeds.set(i, sourceDetailRepacked);
			}
		}
		List<RepackingPlanningDetailRepackedEntity> targetDetailRepackeds = new ArrayList<>();
		for (RepackingPlanningDetailEntity targetDetail : targetDetails) {
			this.validRepackingPlanningDetail(targetDetail, sysdate);
			List<RepackingPlanningDetailRepackedEntity> sourceRepackingPlanningDetailRepackeds = this
					.getRepackingPlanningDetailRepackeds(targetDetail, sourceDetailRepackeds);
			this.repackingPlanningDetailRepackedRepository
					.deleteAll(targetDetail.getRepackingPlanningDetailRepackeds());
			for (RepackingPlanningDetailRepackedEntity sourceDetailRepacked : sourceRepackingPlanningDetailRepackeds) {
				sourceDetailRepacked.setRepackingPlanningDetail(targetDetail);
				// Thông tin chi tiết trên QR Code:
				// - Mã sản phẩm
				// - Mã quy cách đóng gói
				// - Ngày hết hạn (yyyyMMdd)
				String code = this.generateImportStatementCode(sourceDetailRepacked.getProductPacking(),
						targetDetail.getExpireDate());
				sourceDetailRepacked.setCode(code);
				sourceDetailRepacked.setqRCode(QRCodeGenerator.generate(code));
				sourceDetailRepacked = this.repackingPlanningDetailRepackedRepository.save(sourceDetailRepacked);
				targetDetailRepackeds.add(sourceDetailRepacked);
			}
		}
		return new ResponseEntity<>(targetDetailRepackeds, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> confirmRepack(Long id) throws BusinessException {
		Optional<RepackingPlanningEntity> oTarget = this.repackingPlanningRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		RepackingPlanningEntity target = oTarget.get();
		if (!RepackingPlanningStatus.PLANNED.getValue().equals(target.getStatus())) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.confirm.repack.check.status"));
		}
		target.setStatus(RepackingPlanningStatus.REPACKED.getValue());
		return new ResponseEntity<>(this.repackingPlanningRepository.save(target), HttpStatus.OK);
	}

	private void validRepackingPlanningDetailRepackeds(RepackingPlanningEntity target,
			List<RepackingPlanningDetailRepackedEntity> sourceDetailRepackeds, LocalDateTime sysdate)
			throws BusinessException {
		if (!RepackingPlanningStatus.NEW.getValue().equals(target.getStatus())
				&& !RepackingPlanningStatus.PLANNED.getValue().equals(target.getStatus())) {
			throw new BusinessException(
					this.translator.toLocale("repackingplanning.cancel.check.status.is.new.repacked"));
		}
		List<RepackingPlanningDetailEntity> targetDetails = target.getRepackingPlanningDetails();
		// Bất kì bản ghi nào thiếu ID Repacking_planning_detail -> next
		if (sourceDetailRepackeds.stream().anyMatch(input -> (input.getRepackingPlanningDetail() == null)
				|| (input.getRepackingPlanningDetail().getId() == null))) {
			throw new BusinessException(
					this.translator.toLocale("repackingplanning.detail.repacked.missing.detail.id"));
		}
		// source vs target không khớp nhau về ID Repacking_planning_detail -> next
		List<Long> targetDetailIds = targetDetails.stream().map(RepackingPlanningDetailEntity::getId)
				.collect(Collectors.toList());
		List<Long> sourceDetailIds = sourceDetailRepackeds.stream().map(d -> d.getRepackingPlanningDetail().getId())
				.collect(Collectors.toList());
		if (targetDetailIds.stream().anyMatch(x -> !sourceDetailIds.contains(x))
				|| sourceDetailIds.stream().anyMatch(x -> !targetDetailIds.contains(x))) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.detail.invalid"));
		}
		for (RepackingPlanningDetailRepackedEntity repackingPlanningDetailRepacked : sourceDetailRepackeds) {
			this.validRepackingPlanningDetailRepacked(repackingPlanningDetailRepacked, sysdate);
		}
		this.validClaimRepackingPlanningDetailRepacked(target, sourceDetailRepackeds);
	}

	private void validRepackingPlanningDetailRepacked(
			RepackingPlanningDetailRepackedEntity repackingPlanningDetailRepacked, LocalDateTime sysdate)
			throws BusinessException {
		if ((repackingPlanningDetailRepacked.getProductPacking() == null)
				|| (repackingPlanningDetailRepacked.getProductPacking().getId() == null)) {
			throw new BusinessException(
					this.translator.toLocale("repackingplanning.detail.repacked.missing.product.packing"));
		}
		Optional<ProductPackingEntity> oProductPacking = this.productPackingRepository
				.findById(repackingPlanningDetailRepacked.getProductPacking().getId());
		if (!oProductPacking.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(RepackingPlanningServiceImpl.PRODUCT_PACKING_ID_NOT_EXISTS2,
							repackingPlanningDetailRepacked.getProductPacking().getId()));
		}
		repackingPlanningDetailRepacked.setProductPacking(oProductPacking.get());
		if (repackingPlanningDetailRepacked.getExpireDate() == null) {
			throw new BusinessException(
					this.translator.toLocale("repackingplanning.detail.repacked.missing.expire.date"));
		}
		if (repackingPlanningDetailRepacked.getExpireDate().toLocalDate().isBefore(sysdate.toLocalDate())) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.out.of.date",
					oProductPacking.get().getCode()));
		}
		if ((repackingPlanningDetailRepacked.getQuantity() == null)
				|| (repackingPlanningDetailRepacked.getQuantity() < 0)) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.detail.missing.quantity"));
		}
	}

	private void validClaimRepackingPlanningDetailRepacked(RepackingPlanningEntity target,
			List<RepackingPlanningDetailRepackedEntity> sourceDetailRepackeds) throws BusinessException {
		List<ClaimDetail> claimDetails = this.claimService
				.getApprovedClaimDetailsByTypeAndReferenceId(ClaimType.REPACKING_PLANNING, target.getId());
		claimDetails = claimDetails == null ? this.claimService.getApprovedClaimDetailsByTypeAndReferenceId(
				ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING, target.getId()) : claimDetails;
		if (target.getStatus().equals(RepackingPlanningStatus.REPACKED.getValue()) && (claimDetails != null)) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.claimed.repack"));
		}
		List<RepackingPlanningDetailEntity> repackingPlanningDetails = target.getRepackingPlanningDetails();
		for (RepackingPlanningDetailEntity repackingPlanningDetail : repackingPlanningDetails) {
			Long repackingPlanningDetailRepackedQuantity = sourceDetailRepackeds.stream()
					.filter(x -> x.getRepackingPlanningDetail().getId().equals(repackingPlanningDetail.getId()))
					.mapToLong(rpdr -> rpdr.getQuantity() * rpdr.getProductPacking().getPackingType().getQuantity())
					.sum();
			if (repackingPlanningDetailRepackedQuantity < 0) {
				throw new BusinessException(this.translator.toLocale("repackingplanning.detail.missing.quantity"));
			}
			Long repackingPlanningDetailQuantity = repackingPlanningDetail.getQuantity()
					* repackingPlanningDetail.getProductPacking().getPackingType().getQuantity();
			Long claimDetailQuantity = claimDetails == null ? 0L
					: claimDetails.stream()
							.filter(x -> x.getProductPacking().getId()
									.equals(repackingPlanningDetail.getProductPacking().getId())
									&& x.getExpireDate().toLocalDate()
											.equals(repackingPlanningDetail.getExpireDate().toLocalDate()))
							.mapToLong(cd -> cd.getQuantity() * cd.getProductPacking().getPackingType().getQuantity())
							.reduce(0, Long::sum);

			if (!repackingPlanningDetailQuantity
					.equals(repackingPlanningDetailRepackedQuantity + claimDetailQuantity)) {
				throw new BusinessException(
						this.translator.toLocale("repackingplanning.detail.repacked.quantity.invalid"));
			}
		}
	}

	private List<RepackingPlanningDetailRepackedEntity> getRepackingPlanningDetailRepackeds(
			RepackingPlanningDetailEntity targetDetail,
			List<RepackingPlanningDetailRepackedEntity> sourceDetailRepackeds) throws BusinessException {
		List<RepackingPlanningDetailRepackedEntity> sourceRepackingPlanningDetailRepackeds = new ArrayList<>();
		for (RepackingPlanningDetailRepackedEntity sourceDetailRepacked : sourceDetailRepackeds) {
			if (!targetDetail.getId().equals(sourceDetailRepacked.getRepackingPlanningDetail().getId())) {
				continue;
			}
			sourceRepackingPlanningDetailRepackeds.add(sourceDetailRepacked);
		}
		if (sourceRepackingPlanningDetailRepackeds.isEmpty()) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.detail.invalid"));
		}
		return sourceRepackingPlanningDetailRepackeds;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> cancelRepack(Long id) throws BusinessException {
		Optional<RepackingPlanningEntity> oTarget = this.repackingPlanningRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		RepackingPlanningEntity target = oTarget.get();
		if (!RepackingPlanningStatus.PLANNED.getValue().equals(target.getStatus())) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("repackingplanning.cancel.check.status.is.repacked"));
		}
		List<ClaimDetail> claimDetails = this.claimService
				.getApprovedClaimDetailsByTypeAndReferenceId(ClaimType.REPACKING_PLANNING, target.getId());
		claimDetails = claimDetails == null ? this.claimService.getApprovedClaimDetailsByTypeAndReferenceId(
				ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING, target.getId()) : claimDetails;
		if (claimDetails != null) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.exists.approved.claim"));
		}
		target.setStatus(RepackingPlanningStatus.NEW.getValue());
		target = this.repackingPlanningRepository.save(target);
		target.getRepackingPlanningDetails().forEach(x -> {
			this.repackingPlanningDetailRepackedRepository.deleteAll(x.getRepackingPlanningDetailRepackeds());
			x.setRepackingPlanningDetailRepackeds(null);
		});
		return new ResponseEntity<>(target, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> cancelPlan(Long id) throws BusinessException {
		Optional<RepackingPlanningEntity> oTarget = this.repackingPlanningRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		RepackingPlanningEntity target = oTarget.get();
		List<RepackingPlanningDetailEntity> repackingPlanningDetails = target.getRepackingPlanningDetails();
		if (!RepackingPlanningStatus.NEW.getValue().equals(target.getStatus()) || repackingPlanningDetails.stream()
				.anyMatch(x -> !x.getRepackingPlanningDetailRepackeds().isEmpty())) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.cancel.check.status.is.new"));
		}
		List<ClaimDetail> claimDetails = this.claimService
				.getApprovedClaimDetailsByTypeAndReferenceId(ClaimType.REPACKING_PLANNING, target.getId());
		if (claimDetails != null) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.exists.approved.claim"));
		}
		this.backToPallet(repackingPlanningDetails);
		target.setStatus(RepackingPlanningStatus.CANCEL.getValue());
		target = this.repackingPlanningRepository.save(target);
		return new ResponseEntity<>(target, HttpStatus.OK);
	}

	private void backToPallet(List<RepackingPlanningDetailEntity> repackingPlanningDetails) throws BusinessException {
		for (RepackingPlanningDetailEntity repackingPlanningDetail : repackingPlanningDetails) {
			for (RepackingPlanningDetailPalletEntity repackingPlanningDetailPallet : repackingPlanningDetail
					.getRepackingPlanningDetailPallets()) {
				// Cộng lại pallet_detail
				this.palletService.pushUpPalletDetail(repackingPlanningDetailPallet.getPalletDetail().getId(),
						repackingPlanningDetailPallet.getQuantity());
			}
			this.repackingPlanningDetailPalletRepository
					.deleteAll(repackingPlanningDetail.getRepackingPlanningDetailPallets());
		}
	}

	@Override
	public ResponseEntity<Object> print(Long id) throws BusinessException {

		Optional<RepackingPlanningEntity> oRepackingPlanningEntity = this.repackingPlanningRepository.findById(id);
		if (!oRepackingPlanningEntity.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		try {
			Map<String, Object> map = new HashMap<>();
			map.put("objData", oRepackingPlanningEntity.get());
			String outputFilePath = JxlsTransformerUtils.transformer("templates/PRINT_REPACKING_PLANNING.xlsx", map,
					this.fileStorageService.getTempExportExcel(), null);
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.excel.output.file.path.not.exists"));
			}
			Resource resource = new FileSystemResource(file.getAbsolutePath());
			HttpHeaders headers = new HttpHeaders();
			headers.add("filename", file.getName());
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			RepackingPlanningServiceImpl.logger.error(e.getMessage(), e);
		}
		return null;
	}

	@Override
	public ResponseEntity<Object> printQrCode(Long id) throws BusinessException {

		Optional<RepackingPlanningEntity> oRepackingPlanningEntity = this.repackingPlanningRepository.findById(id);
		if (!oRepackingPlanningEntity.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(RepackingPlanningServiceImpl.REPACKINGPLANNING_ID_NOT_EXISTS, id));
		}
		try {
			List<RepackingPlanningDetailRepackedEntity> repackingPlanningDetailRepackeds = oRepackingPlanningEntity
					.get().getRepackingPlanningDetails().stream()
					.map(RepackingPlanningDetailEntity::getRepackingPlanningDetailRepackeds)
					.flatMap(List<RepackingPlanningDetailRepackedEntity>::stream).filter(rpdr -> rpdr.getQuantity() > 0)
					.collect(Collectors.toList());

			List<RepackingPlanningPrintQrCodeDto> repackingPlanningPrintQrCodeDtos = repackingPlanningDetailRepackeds
					.stream().map(RepackingPlanningPrintQrCodeDto::new).collect(Collectors.toList());

			List<List<RepackingPlanningPrintQrCodeDto>> partitioned = PartitionListUtils.ofSize(
					repackingPlanningPrintQrCodeDtos,
					JxlsTransformerUtils.getChunkSize("templates/PRINT_REPACKING_PLANNING_QRCODE.xlsx"));

			Map<String, Object> map = new HashMap<>();
			map.put("objData", partitioned);
			String outputFilePath = Jxls2xTransformerUtils.poiTransformer(
					"templates/PRINT_REPACKING_PLANNING_QRCODE.xlsx", map, this.fileStorageService.getTempExportExcel(),
					String.format("%s-%s.%s", "PRINT_REPACKING_PLANNING_QRCODE",
							this.getSysdate().format(
									DateTimeFormatter.ofPattern(Constants.PREFIX_EXPORT_EXCEL_FILE_NAME)),
							"xlsx"),
					"Template", "Result");
			RepackingPlanningServiceImpl.logger.debug(outputFilePath);
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			RepackingPlanningServiceImpl.logger.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.OK);

	}

}
