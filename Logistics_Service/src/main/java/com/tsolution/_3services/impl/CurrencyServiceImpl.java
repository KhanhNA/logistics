package com.tsolution._3services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._2repositories.CurrencyRepository;
import com.tsolution._3services.ICurrencyService;
import com.tsolution.excetions.BusinessException;

@Service
public class CurrencyServiceImpl implements ICurrencyService {

	@Autowired
	private CurrencyRepository currencyRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.currencyRepository.findAll().stream()
				.filter(x -> Boolean.TRUE.equals(x.getSupport())).collect(Collectors.toList()), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		return null;
	}

	@Override
	public ResponseEntity<Object> create(List<CurrencyEntity> entities) throws BusinessException {
		return null;
	}

	@Override
	public ResponseEntity<Object> update(Long id, CurrencyEntity source) throws BusinessException {
		return null;
	}
}
