package com.tsolution._3services.impl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsolution._1entities.ImportStatementDetailEntity;
import com.tsolution._1entities.ImportStatementDetailPalletEntity;
import com.tsolution._1entities.ImportStatementEntity;
import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.RepackingPlanningDetailEntity;
import com.tsolution._1entities.RepackingPlanningDetailRepackedEntity;
import com.tsolution._1entities.RepackingPlanningEntity;
import com.tsolution._1entities.claim.ClaimDetail;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution._1entities.dto.ImportStatementDetailDto;
import com.tsolution._1entities.dto.ImportStatementDto;
import com.tsolution._1entities.enums.ExportStatementStatus;
import com.tsolution._1entities.enums.ImportStatementStatus;
import com.tsolution._1entities.enums.RepackingPlanningStatus;
import com.tsolution._1entities.export_statement.ExportStatementDetailEntity;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.export_statement.dto.ExportStatementDto;
import com.tsolution._1entities.merchant_order.MerchantOrderDetailEntity;
import com.tsolution._1entities.merchant_order.MerchantOrderEntity;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderDetailStatus;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderStatus;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.StoreProductPackingDetailEntity;
import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution._2repositories.ExportStatementRepository;
import com.tsolution._2repositories.ImportStatementDetailPalletRepository;
import com.tsolution._2repositories.ImportStatementDetailRepository;
import com.tsolution._2repositories.ImportStatementRepository;
import com.tsolution._2repositories.PalletDetailRepository;
import com.tsolution._2repositories.PalletRepository;
import com.tsolution._2repositories.RepackingPlanningRepository;
import com.tsolution._3services.ExportStatementService;
import com.tsolution._3services.ImportStatementService;
import com.tsolution._3services.PalletService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.JxlsTransformerUtils;
import com.tsolution.utils.QRCodeGenerator;
import com.tsolution.utils.StringUtils;

@Service
public class ImportStatementServiceImpl extends LogisticsBaseService implements ImportStatementService {

	private static final String IMPORT_STATEMENT_DETAIL_PALLET_NOT_MATCH_DETAIL = "import.statement.detail.pallet.not.match.detail";

	private static final String IMPORT_STATEMENT_DETAIL_QUANTITY_INVALID = "import.statement.detail.quantity.invalid";

	private static final String IMPORT_STATEMENT_ID_NOT_EXISTS = "import.statement.id.not.exists";

	@Autowired
	private ImportStatementRepository importStatementRepository;

	@Autowired
	private ImportStatementDetailRepository importStatementDetailRepository;

	@Autowired
	private ImportStatementDetailPalletRepository importStatementDetailPalletRepository;

	@Autowired
	private ExportStatementRepository exportStatementRepository;

	@Autowired
	private RepackingPlanningRepository repackingPlanningRepository;

	@Autowired
	private PalletRepository palletRepository;

	@Autowired
	private PalletDetailRepository palletDetailRepository;

	@Autowired
	private PalletService palletService;

	@Autowired
	private ExportStatementService exportStatementService;

	@Autowired
	private FileStorageService fileStorageService;

	private static Logger log = LogManager.getLogger(ImportStatementServiceImpl.class);

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.importStatementRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<ImportStatementEntity> oImportStatement = this.importStatementRepository.findById(id);
		if (!oImportStatement.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(ImportStatementServiceImpl.IMPORT_STATEMENT_ID_NOT_EXISTS, id));
		}
		return new ResponseEntity<>(oImportStatement.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findByRepackingPlanningId(Long repackingPlanningId) throws BusinessException {
		Optional<ImportStatementEntity> oImportStatement = this.importStatementRepository
				.findOneByRepackingPlanningId(repackingPlanningId);
		if (!oImportStatement.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString("import.statement.repacking.planning.id.not.exists", repackingPlanningId));
		}
		return new ResponseEntity<>(oImportStatement.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(String code, Integer status, String type, Integer pageNumber, Integer pageSize)
			throws BusinessException, IOException {
		Page<ImportStatementEntity> pageImportStatement = this.importStatementRepository.find(code, status, type,
				this.getCurrentUsername(), PageRequest.of(pageNumber - 1, pageSize));
		pageImportStatement.getContent().forEach(importStatement -> {
			List<ImportStatementDetailEntity> importStatementDetails = importStatement.getImportStatementDetails();
			importStatement.setTotalQuantity(
					importStatementDetails.stream().mapToLong(ImportStatementDetailEntity::getQuantity).sum());
			importStatement.setTotalVolume(importStatementDetails.stream()
					.mapToDouble(importStatementDetail -> importStatementDetail.getQuantity()
							* importStatementDetail.getProductPacking().getVolume())
					.sum());
		});
		return new ResponseEntity<>(pageImportStatement, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> print(Long id) throws BusinessException {
		Optional<ImportStatementEntity> oImportStatementEntity = this.importStatementRepository.findById(id);
		if (!oImportStatementEntity.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(ImportStatementServiceImpl.IMPORT_STATEMENT_ID_NOT_EXISTS, id));
		}
		try {
			ImportStatementEntity importStatement = oImportStatementEntity.get();
			Map<String, Object> map = new HashMap<>();
			map.put("qrCode", importStatement.getqRCode());
			StoreEntity fromStore = importStatement.getFromStore();
			map.put("fromStoreCode", fromStore == null ? "" : fromStore.getCode());
			map.put("fromStoreName", fromStore == null ? "" : fromStore.getName());
			map.put("fromStoreAddress", fromStore == null ? "" : fromStore.getAddress());
			StoreEntity toStore = importStatement.getToStore();
			map.put("toStoreCode", toStore == null ? "" : toStore.getCode());
			map.put("toStoreName", toStore == null ? "" : toStore.getName());
			map.put("toStoreAddress", toStore == null ? "" : toStore.getAddress());
			map.put("code", importStatement.getCode());
			map.put("createDate", importStatement.getCreateDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			map.put("createBy", importStatement.getCreateUser());
			ExportStatementEntity exportStatement = importStatement.getExportStatement();
			map.put("exportStatementCode", exportStatement == null ? "" : exportStatement.getCode());
			map.put("details", importStatement.getImportStatementDetails());

			Map<Integer, List<Integer>> mapBase64 = new HashMap<>();
			mapBase64.put(2, Collections.singletonList(8));

			String outputFilePath = JxlsTransformerUtils.transformer("templates/PRINT_IMPORTSTATEMENT.xlsx", map,
					this.fileStorageService.getTempExportExcel(), oImportStatementEntity.get().getCode() + ".xlsx",
					mapBase64);
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.excel.output.file.path.not.exists"));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ImportStatementServiceImpl.log.error(e.getMessage(), e);
		}
		return null;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<ImportStatementDto> importStatementDtos) throws BusinessException {
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_NOT_SUPPORT));
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> actuallyImport(Long id, String description,
			List<ImportStatementDetailPalletEntity> importStatementDetailPallets) throws BusinessException {
		if (id == null) {
			throw new BusinessException(this.translator.toLocale("import.statement.input.missing.id"));
		}
		Optional<ImportStatementEntity> oImportStatement = this.importStatementRepository.findById(id);
		if (!oImportStatement.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(ImportStatementServiceImpl.IMPORT_STATEMENT_ID_NOT_EXISTS, id));
		}
		ImportStatementEntity target = oImportStatement.get();
		if (!ImportStatementStatus.NOT_UPDATE_INVENTORY.getValue().equals(target.getStatus())) {
			throw new BusinessException(this.translator.toLocale("import.statement.actually.export.status"));
		}
		// Thực nhập luôn ^^
		importStatementDetailPallets = this.actuallyImport(target, importStatementDetailPallets);
		if (importStatementDetailPallets.isEmpty()) {
			throw new BusinessException(
					this.translator.toLocale("import.statement.update.store.product.packing.failed"));
		}
		this.importStatementDetailPalletRepository.saveAll(importStatementDetailPallets);
		// Cập nhật trạng thái lệnh nhập
		target.setDescription(description);
		target.setStatus(ImportStatementStatus.UPDATED_INVENTORY.getValue());
		target = this.importStatementRepository.save(target);
		// Cập nhật lại lệnh xuất (nếu có)
		ExportStatementEntity exportStatement = target.getExportStatement();
		if (exportStatement != null) {
			List<Long> productPackingIds = importStatementDetailPallets.stream()
					.map(ImportStatementDetailPalletEntity::getProductPackingId).collect(Collectors.toList());
			if ((productPackingIds == null) || productPackingIds.isEmpty()) {
				productPackingIds = Arrays.asList(-1L);
			}
			this.updateExportStatementCompleted(exportStatement, productPackingIds);
		}
		return new ResponseEntity<>(target, HttpStatus.OK);
	}

	private void updateExportStatementCompleted(ExportStatementEntity exportStatementEntity,
			List<Long> productPackingIds) throws BusinessException {
		exportStatementEntity.setStatus(ExportStatementStatus.COMPLETED.getValue());
		exportStatementEntity = this.exportStatementRepository.save(exportStatementEntity);
		// Thêm phần cập nhật Merchant_Order_Detail với các sản phẩm mới nhập
		// - Ưu tiên những đơn được gắn theo lệnh xuất
		List<Long> merchantOrderIds = exportStatementEntity.getMerchantOrderFromDC();
		if ((merchantOrderIds != null) && !merchantOrderIds.isEmpty()) {
			for (Long id : merchantOrderIds) {
				Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository.findById(id);
				if (!oMerchantOrder.isPresent()) {
					throw new BusinessException(
							this.translator.toLocaleByFormatString("merchant.order.id.not.exists", id));
				}
				MerchantOrderEntity merchantOrder = oMerchantOrder.get();
				this.updateMerchantOrderEnough(merchantOrder);
			}
		}
		// - Quét lại các đơn có trạng thái không đủ số lượng
		if (StringUtils.isNullOrEmpty(exportStatementEntity.getMerchantCode())) {
			List<MerchantOrderEntity> merchantOrders = this.merchantOrderRepository
					.findMerchantOrderNotEnoughtQuantity(exportStatementEntity.getToStore().getId(), productPackingIds);
			for (MerchantOrderEntity merchantOrder : merchantOrders) {
				this.updateMerchantOrderEnough(merchantOrder);
			}
		}
	}

	private void updateMerchantOrderEnough(MerchantOrderEntity merchantOrder) throws BusinessException {
		List<MerchantOrderDetailEntity> merchantOrderDetails = merchantOrder.getMerchantOrderDetails();
		for (int i = 0; i < merchantOrderDetails.size(); i++) {
			MerchantOrderDetailEntity merchantOrderDetailEntity = merchantOrderDetails.get(i);
			Long totalAvaiableQuantity = this.storeProductPackingRepository.getTotalAvaiable(
					merchantOrder.getFromStore().getId(), merchantOrderDetailEntity.getProductPacking().getId());
			if (totalAvaiableQuantity >= merchantOrderDetailEntity.getQuantity()) {
				merchantOrderDetailEntity.setStatus(MerchantOrderDetailStatus.STATUS_ENOUGH_QUANTITY.getValue());
			} else {
				merchantOrderDetailEntity.setStatus(MerchantOrderDetailStatus.STATUS_NOT_ENOUGH_QUANTITY.getValue());
			}
			merchantOrder.getMerchantOrderDetails().set(i,
					this.mechantOrderDetailRepository.save(merchantOrderDetailEntity));
		}
		if (merchantOrderDetails.stream().noneMatch(moed -> (MerchantOrderDetailStatus.STATUS_NOT_ENOUGH_QUANTITY
				.getValue().equals(moed.getStatus()))
				|| (MerchantOrderDetailStatus.STATUS_WAITING_FOR_STATEMENT.getValue().equals(moed.getStatus())))) {
			merchantOrder.setStatus(MerchantOrderStatus.STATUS_STORE_ENOUGH_QUANTITY.getValue());
		} else {
			merchantOrder.setStatus(MerchantOrderStatus.STATUS_STORE_NOT_ENOUGH_QUANTITY.getValue());
		}
		this.merchantOrderRepository.save(merchantOrder);
		if (MerchantOrderStatus.STATUS_STORE_ENOUGH_QUANTITY.getValue().equals(merchantOrder.getStatus())) {
			ExportStatementDto exportStatementDto = new ExportStatementDto(merchantOrder, merchantOrderDetails);
			this.exportStatementService.createByMerchantOrder(exportStatementDto);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, ImportStatementDto source) throws BusinessException {
		Optional<ImportStatementEntity> oTarget = this.importStatementRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(ImportStatementServiceImpl.IMPORT_STATEMENT_ID_NOT_EXISTS, id));
		}
		ImportStatementEntity target = oTarget.get();
		if (!ImportStatementStatus.NOT_UPDATE_INVENTORY.getValue().equals(target.getStatus())) {
			throw new BusinessException(this.translator.toLocale("import.statement.update.status"));
		}
		List<ImportStatementDetailEntity> importStatementDetails = target.getImportStatementDetails();
		this.importStatementDetailRepository.deleteAll(importStatementDetails);
		LocalDateTime sysdate = this.getSysdate();
		this.validImportStatementDto(source, sysdate);
		importStatementDetails = this.createImportStatementDetails(target, source.getImportStatementDetails(), sysdate);
		this.importStatementDetailRepository.saveAll(importStatementDetails);
		target.setImportStatementDetails(importStatementDetails);
		return new ResponseEntity<>(target, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> createFromExportStatement(ImportStatementDto importStatementDto)
			throws BusinessException {
		Optional<ExportStatementEntity> oExportStatementEntity = this.exportStatementRepository.findById(
				importStatementDto.getExportStatementId() == null ? 0 : importStatementDto.getExportStatementId());
		if (!oExportStatementEntity.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("export.statement.id.not.exists",
					importStatementDto.getExportStatementId()));
		}
		ExportStatementEntity exportStatement = oExportStatementEntity.get();
		if (!exportStatement.getStatus().equals(ExportStatementStatus.RELEASED.getValue())) {
			throw new BusinessException(this.translator.toLocale("export.statement.status.check.released"));
		}
		ImportStatementEntity importStatementEntity = this.createImportStatementEntity(importStatementDto,
				ImportStatementStatus.NOT_UPDATE_INVENTORY);

		// Đổi lại trạng thái của reference
		exportStatement.setStatus(ExportStatementStatus.PROCESSING.getValue());
		this.exportStatementRepository.save(exportStatement);

		return new ResponseEntity<>(importStatementEntity, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> createFromRepackingPlanning(ImportStatementDto importStatementDto)
			throws BusinessException {
		Optional<RepackingPlanningEntity> oRepackingPlanning = this.repackingPlanningRepository.findById(
				importStatementDto.getRepackingPlanningId() == null ? 0 : importStatementDto.getRepackingPlanningId());
		if (!oRepackingPlanning.isPresent()) {
			throw new BusinessException(this.translator.toLocale("import.statement.missing.repacking.planning"));
		}
		RepackingPlanningEntity repackingPlanningEntity = oRepackingPlanning.get();
		if (!RepackingPlanningStatus.REPACKED.getValue().equals(repackingPlanningEntity.getStatus())) {
			throw new BusinessException(this.translator.toLocale("repackingplanning.status.check.repacked"));
		}

		ImportStatementEntity importStatementEntity = this.createImportStatementEntity(importStatementDto,
				ImportStatementStatus.UPDATED_INVENTORY);

		// Gom thông tin id liên kết theo product_packing_id và expire_date
		List<ImportStatementDetailPalletEntity> importStatementDetailPallets = importStatementDto
				.getImportStatementDetailPallets();
		if ((importStatementDetailPallets == null) || importStatementDetailPallets.isEmpty()) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		for (ImportStatementDetailPalletEntity importStatementDetailPalletEntity : importStatementDetailPallets) {
			importStatementDetailPalletEntity.setImportStatementId(importStatementEntity.getId());
			Optional<ImportStatementDetailEntity> oImportStatementDetail = importStatementEntity
					.getImportStatementDetails().stream()
					.filter(isd -> isd.getProductPacking().getId()
							.equals(importStatementDetailPalletEntity.getProductPackingId())
							&& isd.getExpireDate().toLocalDate()
									.equals(importStatementDetailPalletEntity.getExpireDate().toLocalDate()))
					.findFirst();
			if (oImportStatementDetail.isPresent()) {
				importStatementDetailPalletEntity.setImportStatementDetailId(oImportStatementDetail.get().getId());
			}
		}

		// Thực nhập luôn ^^
		importStatementDetailPallets = this.actuallyImport(importStatementEntity, importStatementDetailPallets);
		if (importStatementDetailPallets.isEmpty()) {
			throw new BusinessException(
					this.translator.toLocale("import.statement.update.store.product.packing.failed"));
		}
		this.importStatementDetailPalletRepository.saveAll(importStatementDetailPallets);

		// Đổi lại trạng thái của reference
		repackingPlanningEntity.setImportedDate(this.getSysdate());
		repackingPlanningEntity.setStatus(RepackingPlanningStatus.IMPORTED.getValue());
		this.repackingPlanningRepository.save(repackingPlanningEntity);

		return new ResponseEntity<>(importStatementEntity, HttpStatus.OK);
	}

	private void validImportStatementDto(ImportStatementDto importStatementDto, LocalDateTime sysdate)
			throws BusinessException {
		if (importStatementDto.getFromStoreId() == null) {
			throw new BusinessException(this.translator.toLocale("import.statement.missing.from.store"));
		}
		Optional<StoreEntity> oFromStore = this.storeRepository.findById(importStatementDto.getFromStoreId());
		if (!oFromStore.isPresent()) {
			throw new BusinessException(this.translator.toLocale("common.from.store.info.not.exists"));
		}
		importStatementDto.setFromStoreCode(oFromStore.get().getCode());
		if (importStatementDto.getToStoreId() == null) {
			throw new BusinessException(this.translator.toLocale("import.statement.missing.to.store"));
		}
		Optional<StoreEntity> oToStore = this.storeRepository.findById(importStatementDto.getToStoreId());
		if (!oToStore.isPresent()) {
			throw new BusinessException(this.translator.toLocale("common.to.store.info.not.exists"));
		}
		StoreEntity toStore = oToStore.get();
		importStatementDto.setToStoreCode(toStore.getCode());
		Optional<ExportStatementEntity> oExportStatement = this.exportStatementRepository.findById(
				importStatementDto.getExportStatementId() == null ? 0 : importStatementDto.getExportStatementId());
		Optional<RepackingPlanningEntity> oRepackingPlanning = this.repackingPlanningRepository.findById(
				importStatementDto.getRepackingPlanningId() == null ? 0 : importStatementDto.getRepackingPlanningId());
		if (!oExportStatement.isPresent() && !oRepackingPlanning.isPresent()) {
			throw new BusinessException(
					this.translator.toLocale("import.statement.missing.repacking.planning.export.statement"));
		}
		this.validImportStatementDetailDtos(importStatementDto, sysdate);
		if (oRepackingPlanning.isPresent()) {
			importStatementDto.setEstimatedTimeOfArrival(sysdate);
			this.validClaimRepackingPlanning(oRepackingPlanning.get(), importStatementDto);
		}
		if (oExportStatement.isPresent()) {
			if (importStatementDto.getEstimatedTimeOfArrival() == null) {
				throw new BusinessException(this.translator.toLocale("import.statement.missing.delivery.time"));
			}
			this.validClaimExportStatement(oExportStatement.get(), importStatementDto);
		}
	}

	private void validImportStatementDetailDtos(ImportStatementDto importStatementDto, LocalDateTime sysdate)
			throws BusinessException {
		if ((importStatementDto.getImportStatementDetails() == null)
				|| (importStatementDto.getImportStatementDetails().isEmpty())) {
			throw new BusinessException(this.translator.toLocale("import.statement.input.missing.details"));
		}
		for (ImportStatementDetailDto importStatementDetailDto : importStatementDto.getImportStatementDetails()) {
			this.validImportStatementDetailDto(importStatementDetailDto, sysdate);
		}
	}

	private void validImportStatementDetailDto(ImportStatementDetailDto importStatementDetailDto, LocalDateTime sysdate)
			throws BusinessException {
		if (importStatementDetailDto.getProductPackingId() == null) {
			throw new BusinessException(this.translator.toLocale("import.statement.detail.missing.product.packing"));
		}
		Optional<ProductPackingEntity> oProductPacking = this.productPackingRepository
				.findById(importStatementDetailDto.getProductPackingId());
		if (!oProductPacking.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.id.not.exists",
					importStatementDetailDto.getProductPackingId()));
		}
		importStatementDetailDto.setProductPacking(oProductPacking.get());
		if (importStatementDetailDto.getExpireDate() == null) {
			throw new BusinessException(this.translator.toLocale("import.statement.detail.missing.expire.date"));
		}
		if (importStatementDetailDto.getExpireDate().toLocalDate().isBefore(sysdate.toLocalDate())) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.out.of.date",
					oProductPacking.get().getCode()));
		}
		if ((importStatementDetailDto.getQuantity() == null) || (importStatementDetailDto.getQuantity() < 0)) {
			throw new BusinessException(
					this.translator.toLocale(ImportStatementServiceImpl.IMPORT_STATEMENT_DETAIL_QUANTITY_INVALID));
		}
	}

	private void validClaimRepackingPlanning(RepackingPlanningEntity repackingPlanningEntity,
			ImportStatementDto importStatement) throws BusinessException {
		List<RepackingPlanningDetailRepackedEntity> repackingPlanningDetailRepackeds = repackingPlanningEntity
				.getRepackingPlanningDetails().stream()
				.map(RepackingPlanningDetailEntity::getRepackingPlanningDetailRepackeds)
				.flatMap(List<RepackingPlanningDetailRepackedEntity>::stream).collect(Collectors.toList());
		List<ClaimDetail> claimDetails = this.claimService.getApprovedClaimDetailsByTypeAndReferenceId(
				ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING, repackingPlanningEntity.getId());
		for (RepackingPlanningDetailRepackedEntity repackingPlanningDetailRepacked : repackingPlanningDetailRepackeds) {
			Long quantityClaimDetail = claimDetails == null ? 0L
					: claimDetails.stream()
							.filter(x -> x.getProductPacking().getId()
									.equals(repackingPlanningDetailRepacked.getProductPacking().getId())
									&& x.getExpireDate().toLocalDate()
											.equals(repackingPlanningDetailRepacked.getExpireDate().toLocalDate()))
							.mapToLong(ClaimDetail::getQuantity).reduce(0, Long::sum);
			Long quantityImportStatementDetail = importStatement.getImportStatementDetails().stream().filter(
					x -> x.getProductPackingId().equals(repackingPlanningDetailRepacked.getProductPacking().getId())
							&& x.getExpireDate().toLocalDate()
									.equals(repackingPlanningDetailRepacked.getExpireDate().toLocalDate()))
					.mapToLong(ImportStatementDetailDto::getQuantity).reduce(0, Long::sum);
			if (!repackingPlanningDetailRepacked.getQuantity()
					.equals(quantityImportStatementDetail + quantityClaimDetail)) {
				throw new BusinessException(this.translator.toLocale(
						"import.statement.detail.quantity.must.be.less.than.repacking.planning.detail.repacked"));
			}
		}
	}

	private void validClaimExportStatement(ExportStatementEntity exportStatement, ImportStatementDto importStatement)
			throws BusinessException {
		Map<Long, Long> totalQuantityExportStatementDetail = exportStatement.getExportStatementDetails().stream()
				.collect(Collectors.groupingBy(esd -> esd.getProductPacking().getId(),
						Collectors.summingLong(ExportStatementDetailEntity::getQuantity)));
		Map<Long, Long> totalQuantityImportStatementDetail = importStatement.getImportStatementDetails().stream()
				.collect(Collectors.groupingBy(ImportStatementDetailDto::getProductPackingId,
						Collectors.summingLong(ImportStatementDetailDto::getQuantity)));
		for (Map.Entry<Long, Long> entry : totalQuantityExportStatementDetail.entrySet()) {
			Long productPackingId = entry.getKey();
			Long isdq = 0L;
			if (totalQuantityImportStatementDetail.containsKey(productPackingId)) {
				if (totalQuantityImportStatementDetail.get(productPackingId) < 0) {
					throw new BusinessException(this.translator
							.toLocale(ImportStatementServiceImpl.IMPORT_STATEMENT_DETAIL_QUANTITY_INVALID));
				}
				isdq = totalQuantityImportStatementDetail.get(productPackingId);
			}
			Long esdq = 0L;
			if (totalQuantityExportStatementDetail.containsKey(productPackingId)) {
				esdq = totalQuantityExportStatementDetail.get(productPackingId);
			}
			if (!esdq.equals(isdq)) {
				throw new BusinessException(this.translator
						.toLocale("import.statement.detail.quantity.must.be.less.than.export.statement.detail"));
			}
		}
	}

	@Override
	public ImportStatementEntity createImportStatementEntity(ImportStatementDto importStatementDto,
			ImportStatementStatus importStatementStatus) throws BusinessException {
		LocalDateTime sysdate = this.getSysdate();
		this.validImportStatementDto(importStatementDto, sysdate);
		if (StringUtils.isNullOrEmpty(importStatementDto.getCode())) {
			// Tạo QRCode để scan ^^
			String uniqueCode = String.format("%s-PNK-%s-%s-%s",
					sysdate.format(DateTimeFormatter.ofPattern("yyyyMMdd")), importStatementDto.getFromStoreCode(),
					importStatementDto.getToStoreCode(), sysdate.format(DateTimeFormatter.ofPattern("HHmmssSSS")));
			importStatementDto.setCode(uniqueCode);
		}
		ImportStatementEntity importStatementEntity = new ImportStatementEntity();
		importStatementEntity.setFromStore(new StoreEntity(importStatementDto.getFromStoreId()));
		importStatementEntity.setToStore(new StoreEntity(importStatementDto.getToStoreId()));
		importStatementEntity.setCode(importStatementDto.getCode());
		importStatementEntity.setqRCode(QRCodeGenerator.generate(importStatementDto.getCode()));
		importStatementEntity.setDescription(importStatementDto.getDescription());
		importStatementEntity.setImportDate(sysdate);
		importStatementEntity.setEstimatedTimeOfArrival(importStatementDto.getEstimatedTimeOfArrival());
		importStatementEntity.setRepackingPlanning(importStatementDto.getRepackingPlanningId() == null ? null
				: new RepackingPlanningEntity(importStatementDto.getRepackingPlanningId()));
		importStatementEntity.setExportStatement(importStatementDto.getExportStatementId() == null ? null
				: new ExportStatementEntity(importStatementDto.getExportStatementId()));
		importStatementEntity.setStatus(importStatementStatus.getValue());
		// Lưu lệnh nhập và chi tiết lệnh nhập hàng
		importStatementEntity = this.importStatementRepository.save(importStatementEntity);
		// Tạo chi tiết cho lệnh nhập
		List<ImportStatementDetailEntity> importStatementDetailEntities = this.createImportStatementDetails(
				importStatementEntity, importStatementDto.getImportStatementDetails(), sysdate);
		importStatementDetailEntities = this.importStatementDetailRepository.saveAll(importStatementDetailEntities);
		importStatementEntity.setImportStatementDetails(importStatementDetailEntities);
		return importStatementEntity;
	}

	private List<ImportStatementDetailEntity> createImportStatementDetails(ImportStatementEntity importStatement,
			List<ImportStatementDetailDto> importStatementDetailDtos, LocalDateTime sysdate) {
		List<ImportStatementDetailEntity> importStatementDetailEntities = new ArrayList<>();
		for (ImportStatementDetailDto importStatementDetailDto : importStatementDetailDtos) {
			ImportStatementDetailEntity importStatementDetailEntity = new ImportStatementDetailEntity();
			importStatementDetailEntity.setImportStatement(importStatement);
			importStatementDetailEntity.setProductPacking(importStatementDetailDto.getProductPacking());
			importStatementDetailEntity.setExpireDate(importStatementDetailDto.getExpireDate());
			importStatementDetailEntity.setQuantity(importStatementDetailDto.getQuantity());
			importStatementDetailEntity.setImportDate(sysdate);
			importStatementDetailEntities.add(importStatementDetailEntity);
		}
		return importStatementDetailEntities;
	}

	private void validImportStatementDetailPallets(ImportStatementEntity importStatement,
			List<ImportStatementDetailPalletEntity> importStatementDetailPallets) throws BusinessException {
		if ((importStatementDetailPallets == null) || importStatementDetailPallets.isEmpty()) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		for (ImportStatementDetailPalletEntity importStatementDetailPallet : importStatementDetailPallets) {
			this.validImportStatementDetailPallet(importStatement, importStatementDetailPallet);
		}
		if (importStatement.getRepackingPlanning() != null) {
			this.validImportStatementDetailAndDetailPallets(importStatement.getImportStatementDetails(),
					importStatementDetailPallets);
		} else if (importStatement.getExportStatement() != null) {
			this.validClaimImportStatementExportStatement(importStatement, importStatementDetailPallets);
		}
	}

	private void validClaimImportStatementExportStatement(ImportStatementEntity importStatement,
			List<ImportStatementDetailPalletEntity> importStatementDetailPallets) throws BusinessException {
		Map<ImportStatementDetailDto, Long> totalQuantityImportStatementDetail = importStatement
				.getImportStatementDetails().stream()
				.collect(Collectors.groupingBy(
						esd -> new ImportStatementDetailDto(esd.getProductPacking().getId(), esd.getExpireDate()),
						Collectors.summingLong(ImportStatementDetailEntity::getQuantity)));
		Map<ImportStatementDetailDto, Long> totalQuantityImportStatementDetailPallet = importStatementDetailPallets
				.stream()
				.collect(Collectors.groupingBy(
						isd -> new ImportStatementDetailDto(isd.getProductPackingId(), isd.getExpireDate()),
						Collectors.summingLong(ImportStatementDetailPalletEntity::getQuantity)));
		List<ClaimDetail> claimDetailsDelivery = this.claimService
				.getApprovedClaimDetailsByTypeAndReferenceId(ClaimType.DELIVERY, importStatement.getId());
		List<ClaimDetail> claimDetailsImportStatement = this.claimService.getApprovedClaimDetailsByTypeAndReferenceId(
				ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT, importStatement.getId());
		for (Map.Entry<ImportStatementDetailDto, Long> entry : totalQuantityImportStatementDetail.entrySet()) {
			ImportStatementDetailDto importStatementDetail = entry.getKey();
			Long quantityImportStatementDetail = entry.getValue();
			if (quantityImportStatementDetail < 0) {
				throw new BusinessException(
						this.translator.toLocale(ImportStatementServiceImpl.IMPORT_STATEMENT_DETAIL_QUANTITY_INVALID));
			}
			Long quantityImportStatementDetailPallet = totalQuantityImportStatementDetailPallet
					.get(importStatementDetail);

			Long claimDetailDeliveryQuantity = claimDetailsDelivery == null ? 0L
					: claimDetailsDelivery.stream().filter(
							x -> x.getProductPacking().getId().equals(importStatementDetail.getProductPackingId())
									&& x.getExpireDate().toLocalDate()
											.equals(importStatementDetail.getExpireDate().toLocalDate()))
							.mapToLong(ClaimDetail::getQuantity).reduce(0, Long::sum);
			Long claimDetailImportStatementQuantity = claimDetailsImportStatement == null ? 0L
					: claimDetailsImportStatement.stream().filter(
							x -> x.getProductPacking().getId().equals(importStatementDetail.getProductPackingId())
									&& x.getExpireDate().toLocalDate()
											.equals(importStatementDetail.getExpireDate().toLocalDate()))
							.mapToLong(ClaimDetail::getQuantity).reduce(0, Long::sum);

			if (!quantityImportStatementDetail.equals(quantityImportStatementDetailPallet + claimDetailDeliveryQuantity
					+ claimDetailImportStatementQuantity)) {
				throw new BusinessException(this.translator
						.toLocale(ImportStatementServiceImpl.IMPORT_STATEMENT_DETAIL_PALLET_NOT_MATCH_DETAIL));
			}
		}
	}

	private void validImportStatementDetailAndDetailPallets(List<ImportStatementDetailEntity> importStatementDetails,
			List<ImportStatementDetailPalletEntity> importStatementDetailPallets) throws BusinessException {
		Map<ImportStatementDetailDto, Long> totalQuantityImportStatementDetail = importStatementDetails.stream()
				.collect(Collectors.groupingBy(
						isd -> new ImportStatementDetailDto(isd.getProductPacking().getId(), isd.getExpireDate()),
						Collectors.summingLong(ImportStatementDetailEntity::getQuantity)));
		Map<ImportStatementDetailDto, Long> totalQuantityImportStatementDetailPallet = importStatementDetailPallets
				.stream()
				.collect(Collectors.groupingBy(
						isd -> new ImportStatementDetailDto(isd.getProductPackingId(), isd.getExpireDate()),
						Collectors.summingLong(ImportStatementDetailPalletEntity::getQuantity)));
		if (totalQuantityImportStatementDetail.keySet().size() != totalQuantityImportStatementDetailPallet.keySet()
				.size()) {
			throw new BusinessException(this.translator
					.toLocale(ImportStatementServiceImpl.IMPORT_STATEMENT_DETAIL_PALLET_NOT_MATCH_DETAIL));
		}
		for (Map.Entry<ImportStatementDetailDto, Long> entry : totalQuantityImportStatementDetail.entrySet()) {
			ImportStatementDetailDto importStatementDetail = entry.getKey();
			Long quantityImportStatementDetail = entry.getValue();
			Long quantityImportStatementDetailPallet = totalQuantityImportStatementDetailPallet
					.get(importStatementDetail);
			if (!quantityImportStatementDetail.equals(quantityImportStatementDetailPallet)) {
				throw new BusinessException(this.translator
						.toLocale(ImportStatementServiceImpl.IMPORT_STATEMENT_DETAIL_PALLET_NOT_MATCH_DETAIL));
			}
		}
	}

	private void validImportStatementDetailPallet(ImportStatementEntity importStatementEntity,
			ImportStatementDetailPalletEntity importStatementDetailPallet) throws BusinessException {
		if ((importStatementDetailPallet.getImportStatementId() == null)
				|| !importStatementEntity.getId().equals(importStatementDetailPallet.getImportStatementId())) {
			throw new BusinessException(
					this.translator.toLocale("import.statement.detail.pallet.missing.import.statement"));
		}
		if ((importStatementDetailPallet.getImportStatementDetailId() == null)
				|| importStatementEntity.getImportStatementDetails().stream().noneMatch(
						isd -> isd.getId().equals(importStatementDetailPallet.getImportStatementDetailId()))) {
			throw new BusinessException(
					this.translator.toLocale("import.statement.detail.pallet.missing.import.statement.detail"));
		}
		if (importStatementDetailPallet.getProductPackingId() == null) {
			throw new BusinessException(
					this.translator.toLocale("import.statement.detail.pallet.missing.product.packing"));
		}
		if (importStatementDetailPallet.getExpireDate() == null) {
			throw new BusinessException(this.translator.toLocale("import.statement.detail.pallet.missing.expire.date"));
		}
		if (importStatementEntity.getImportStatementDetails().stream().noneMatch(
				isd -> isd.getProductPacking().getId().equals(importStatementDetailPallet.getProductPackingId())
						&& isd.getExpireDate().toLocalDate()
								.equals(importStatementDetailPallet.getExpireDate().toLocalDate()))) {
			throw new BusinessException(this.translator
					.toLocale(ImportStatementServiceImpl.IMPORT_STATEMENT_DETAIL_PALLET_NOT_MATCH_DETAIL));
		}
		if ((importStatementDetailPallet.getQuantity() == null) || (importStatementDetailPallet.getQuantity() < 0)) {
			throw new BusinessException(this.translator.toLocale("import.statement.detail.pallet.missing.quantity"));
		}
		if (!importStatementEntity.getToStore().isDc()) {
			this.validImportStatementDetailPalletFC(importStatementDetailPallet);
		}
	}

	private void validImportStatementDetailPalletFC(ImportStatementDetailPalletEntity importStatementDetailPallet)
			throws BusinessException {
		if ((importStatementDetailPallet.getPallet() == null)
				|| (importStatementDetailPallet.getPallet().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.statement.detail.pallet.missing.pallet"));
		} else {
			Optional<PalletEntity> oPallet = this.palletRepository
					.findById(importStatementDetailPallet.getPallet().getId());
			if (!oPallet.isPresent()) {
				throw new BusinessException(this.translator.toLocaleByFormatString("pallet.id.not.exists",
						importStatementDetailPallet.getPallet().getId()));
			}
			importStatementDetailPallet.setPallet(oPallet.get());
		}
	}

	private List<ImportStatementDetailPalletEntity> actuallyImport(ImportStatementEntity importStatementEntity,
			List<ImportStatementDetailPalletEntity> importStatementDetailPallets) throws BusinessException {
		this.validImportStatementDetailPallets(importStatementEntity, importStatementDetailPallets);
		StoreEntity toStore = importStatementEntity.getToStore();
		List<ImportStatementDetailPalletEntity> results = new ArrayList<>();
		for (ImportStatementDetailPalletEntity importStatementDetailPallet : importStatementDetailPallets) {
			Optional<ImportStatementDetailEntity> oImportStatementDetail = importStatementEntity
					.getImportStatementDetails().stream()
					.filter(isd -> isd.getId().equals(importStatementDetailPallet.getImportStatementDetailId()))
					.findFirst();
			if (!oImportStatementDetail.isPresent()) {
				throw new BusinessException(
						this.translator.toLocale("import.statement.detail.pallet.missing.import.statement.detail"));
			}
			ImportStatementDetailEntity importStatementDetailx = oImportStatementDetail.get();
			ProductPackingEntity productPacking = importStatementDetailx.getProductPacking();
			Optional<StoreProductPackingEntity> oStoreProductPacking = this.storeProductPackingRepository
					.findByStoreIdAndProductPackingIdAndExpireDate(toStore.getId(),
							importStatementDetailPallet.getProductPackingId(),
							importStatementDetailPallet.getExpireDate());
			StoreProductPackingEntity storeProductPacking;
			if (!oStoreProductPacking.isPresent()) {
				storeProductPacking = new StoreProductPackingEntity();
				storeProductPacking.setStore(toStore);
				storeProductPacking.setProductPacking(productPacking);
				storeProductPacking.setProduct(productPacking.getProduct());
				storeProductPacking.setTotalQuantity(0L);
				storeProductPacking.setExpireDate(importStatementDetailPallet.getExpireDate());
				// Thông tin chi tiết trên QR Code:
				// - Mã sản phẩm
				// - Mã quy cách đóng gói
				// - Ngày hết hạn (yyyyMMdd)
				String storeProductPackingCode = this.generateImportStatementCode(productPacking,
						importStatementDetailPallet.getExpireDate());
				storeProductPacking.setCode(storeProductPackingCode);
				storeProductPacking.setqRCode(QRCodeGenerator.generate(storeProductPackingCode));
			} else {
				storeProductPacking = oStoreProductPacking.get();
			}
			storeProductPacking.setTotalQuantity(
					storeProductPacking.getTotalQuantity() + importStatementDetailPallet.getQuantity());
			storeProductPacking = this.storeProductPackingRepository.save(storeProductPacking);

			StoreProductPackingDetailEntity storeProductPackingDetail = new StoreProductPackingDetailEntity();
			storeProductPackingDetail.setStoreProductPacking(storeProductPacking);
			storeProductPackingDetail.setQuantity(importStatementDetailPallet.getQuantity());
			storeProductPackingDetail.setOriginalQuantity(importStatementDetailPallet.getQuantity());
			storeProductPackingDetail.setCode(storeProductPacking.getCode());
			storeProductPackingDetail.setqRCode(storeProductPacking.getqRCode());
			storeProductPackingDetail = this.storeProductPackingDetailRepository.save(storeProductPackingDetail);

			if (!toStore.isDc()) {
				PalletDetailEntity palletDetail = this.palletDetailRepository.save(new PalletDetailEntity(
						importStatementDetailPallet.getPallet(), productPacking.getProduct(), productPacking, 0L,
						importStatementDetailPallet.getExpireDate(), storeProductPackingDetail));
				this.palletService.pushUpPalletDetail(palletDetail.getId(), importStatementDetailPallet.getQuantity());
				importStatementDetailPallet.setPalletDetail(palletDetail);
			}
			importStatementDetailPallet.setStoreProductPackingDetailId(storeProductPackingDetail.getId());
			results.add(importStatementDetailPallet);
		}
		return results;
	}

}
