package com.tsolution._3services.impl;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.MerchantEntity;
import com.tsolution._3services.IMerchantService;

@Service
public class MerchantServiceImpl extends LogisticsBaseService implements IMerchantService {

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.merchantRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) {
		return new ResponseEntity<>(this.merchantRepository.findById(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(List<MerchantEntity> entity) {
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long id, MerchantEntity source) {
		return new ResponseEntity<>(source, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(Boolean status) {
		return new ResponseEntity<>(this.merchantRepository.find(status), HttpStatus.OK);
	}

}
