package com.tsolution._3services.impl;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.UserEntity;
import com.tsolution._1entities.claim.ClaimDetail;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution._1entities.dto.ImportStatementDetailDto;
import com.tsolution._1entities.dto.ImportStatementDto;
import com.tsolution._1entities.enums.ExportStatementStatus;
import com.tsolution._1entities.enums.ImportStatementStatus;
import com.tsolution._1entities.export_statement.ExportStatementDetailEntity;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.export_statement.dto.ExportStatementDetailDto;
import com.tsolution._1entities.export_statement.dto.ExportStatementDto;
import com.tsolution._1entities.export_statement.dto.ExportStatementFilterDto;
import com.tsolution._1entities.merchant_order.MerchantOrderDetailEntity;
import com.tsolution._1entities.merchant_order.MerchantOrderEntity;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderDetailStatus;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderGoodsReceiveForm;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderStatus;
import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution._1entities.shipping_partner.enums.ShippingPartnerStatus;
import com.tsolution._1entities.store.StoreDeliveryWaitEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.StoreProductPackingDetailEntity;
import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution._2repositories.ExportStatementRepository;
import com.tsolution._2repositories.IExportStatementDetailRepository;
import com.tsolution._2repositories.PalletDetailRepository;
import com.tsolution._2repositories.shipping_partner.ShippingPartnerRepository;
import com.tsolution._2repositories.store.StoreDeliveryWaitRepository;
import com.tsolution._3services.ExportStatementService;
import com.tsolution._3services.ImportStatementService;
import com.tsolution._3services.PalletService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CallApiUtils;
import com.tsolution.utils.ExcelTo;
import com.tsolution.utils.Jxls2xTransformerUtils;
import com.tsolution.utils.Jxls2xTransformerUtils.DrawBase64Dto;
import com.tsolution.utils.QRCodeGenerator;
import com.tsolution.utils.StringUtils;

@Service
public class ExportStatementServiceImpl extends LogisticsBaseService implements ExportStatementService {

	private static final String EXPORT_STATEMENT_ID_NOT_EXISTS = "export.statement.id.not.exists";

	private static final String MERCHANT_ORDER_ID_NOT_EXISTS = "merchant.order.id.not.exists";

	private static final Logger log = LogManager.getLogger(ExportStatementServiceImpl.class);

	@Value("${dcommerce.url}")
	protected String host;

	@Value("${security.oauth2.client.accessTokenUri}")
	private String accessTokenUri;

	@Value("${security.oauth2.client.clientId}")
	private String clientId;

	@Value("${security.oauth2.client.clientSecret}")
	private String clientSecret;

	@Value("${dcommerce.username}")
	private String username;

	@Value("${dcommerce.password}")
	private String password;

	@Autowired
	private ExportStatementRepository exportStatementRepository;

	@Autowired
	private IExportStatementDetailRepository exportStatementDetailRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private ImportStatementService importStatementService;

	@Autowired
	private PalletDetailRepository palletDetailRepository;

	@Autowired
	private PalletService palletService;

	@Autowired
	private StoreDeliveryWaitRepository storeDeliveryWaitRepository;

	@Autowired
	private ShippingPartnerRepository shippingPartnerRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.exportStatementRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<ExportStatementEntity> oExportStatementEntity = this.exportStatementRepository.findById(id);
		if (!oExportStatementEntity.isPresent()) {
			throw new BusinessException(this.translator.toLocale("export.statement.not.exists"));
		}
		ExportStatementEntity exportStatement = oExportStatementEntity.get();
		return new ResponseEntity<>(exportStatement, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findOneByCode(String code) throws BusinessException {
		ExportStatementEntity exportStatementEntity = this.exportStatementRepository.findOneByCode(code);
		if (exportStatementEntity == null) {
			throw new BusinessException(this.translator.toLocale("export.statement.code.not.exists"));
		}
		return new ResponseEntity<>(exportStatementEntity, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(ExportStatementFilterDto exportStatementFilterDto, Integer pageNumber,
			Integer pageSize) throws BusinessException {
		return new ResponseEntity<>(this.exportStatementRepository.find(exportStatementFilterDto,
				this.getCurrentUsername(), PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	private Map<String, Object> putParams(ExportStatementEntity exportStatement) throws BusinessException {
		Map<String, Object> map = new HashMap<>();
		map.put("qrCode", exportStatement.getqRCode());
		StoreEntity fromStore = exportStatement.getFromStore();
		map.put("fromStoreCode", fromStore == null ? "" : fromStore.getCode());
		map.put("fromStoreName", fromStore == null ? "" : fromStore.getName());
		String fromStorePhone = fromStore == null ? "" : fromStore.getPhone();
		String fromStoreUserPhone = (fromStore == null) || (fromStore.getUser() == null) ? ""
				: fromStore.getUser().getTel();
		String fromStoreTel = String.join(" / ", fromStorePhone, fromStoreUserPhone);
		map.put("fromStoreTel", fromStoreTel);
		map.put("fromStoreAddress", fromStore == null ? "" : fromStore.getAddress());
		StoreEntity toStore = exportStatement.getToStore();
		MerchantOrderEntity merchantOrder = exportStatement.getMerchantOrder();
		map.put("toStoreCode", toStore == null ? merchantOrder.getMerchantCode() : toStore.getCode());
		map.put("toStoreName", toStore == null ? merchantOrder.getMerchantName() : toStore.getName());
		String toStorePhone = toStore == null ? merchantOrder.getPhone() : toStore.getPhone();
		String toStoreUserPhong = (toStore == null) || (toStore.getUser() == null) ? "" : toStore.getUser().getTel();
		String toStoreTel = String.join(" / ", toStorePhone, toStoreUserPhong);
		if (toStoreTel.trim().startsWith("/") || toStoreTel.trim().endsWith("/") || "/".equals(toStoreTel.trim())) {
			toStoreTel = toStoreTel.replace("/", "");
		}
		map.put("toStoreTel", toStoreTel);
		map.put("toStoreAddress", toStore == null ? "" : toStore.getAddress());

		map.put("code", exportStatement.getCode());
		map.put("createDate", exportStatement.getCreateDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		Optional<UserEntity> oUser = this.userRepository.findOneByUsername(exportStatement.getCreateUser());
		if (!oUser.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("user.id.not.exists", exportStatement.getCreateUser()));
		}
		UserEntity user = oUser.get();
		map.put("createBy", user.getFirstName().trim() + " " + user.getLastName().trim());
		map.put("tel", user.getTel());
		map.put("email", user.getEmail());
		map.put("note", exportStatement.getDescription());

		Map<ProductPackingEntity, Long> mapDetails = exportStatement.getExportStatementDetails().stream()
				.collect(Collectors.groupingBy(ExportStatementDetailEntity::getProductPacking,
						Collectors.summingLong(ExportStatementDetailEntity::getQuantity)));
		map.put("details", mapDetails);
		return map;
	}

	private String exportExcel(Long id) throws BusinessException {
		Optional<ExportStatementEntity> oExportStatementEntity = this.exportStatementRepository.findById(id);
		if (!oExportStatementEntity.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(ExportStatementServiceImpl.EXPORT_STATEMENT_ID_NOT_EXISTS, id));
		}
		try {
			List<DrawBase64Dto> drawBase64Dtos = new ArrayList<>();
			drawBase64Dtos.add(new DrawBase64Dto("", 1, 1, 3, 3));
			drawBase64Dtos.add(new DrawBase64Dto("", 9, 1, 10, 5));

			ExportStatementEntity exportStatement = oExportStatementEntity.get();
			Map<String, Object> map = this.putParams(exportStatement);
			map.put("specialImagePosition", drawBase64Dtos);

			return Jxls2xTransformerUtils.poiTransformer("templates/PRINT_EXPORTSTATEMENT.xlsx", map,
					this.fileStorageService.getTempExportExcel(), oExportStatementEntity.get().getCode() + ".xlsx");
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<Object> print(Long id) throws BusinessException {
		try {
			String outputFilePath = this.exportExcel(id);
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.excel.output.file.path.not.exists"));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ExportStatementServiceImpl.log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> printPdf(Long id) throws BusinessException {
		try {
			String excelFilePath = this.exportExcel(id);
			String pdfFilePath = ExcelTo.pdf(this.fileStorageService.getLibreOfficePath(), excelFilePath,
					this.fileStorageService.getTempExportExcel());
			File file = new File(pdfFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.pdf.output.file.path.not.exists"));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(pdfFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ExportStatementServiceImpl.log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, ExportStatementDto source) throws BusinessException {
		Optional<ExportStatementEntity> oTarget = this.exportStatementRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator.toLocale("export.statement.not.exists"));
		}
		ExportStatementEntity target = oTarget.get();
		if (!ExportStatementStatus.REQUESTED.getValue().equals(target.getStatus())
				|| ((target.getMerchantOrder() != null) && (target.getMerchantOrder().getId() != null))) {
			throw new BusinessException(this.translator.toLocale("export.statement.update.status"));
		}
		// Lấy chi tiết ra trả hết về kho và xóa
		List<ExportStatementDetailEntity> exportStatementDetails = target.getExportStatementDetails();
		this.returnProductPackingQuantity(exportStatementDetails);
		this.exportStatementDetailRepository.deleteAll(exportStatementDetails);
		// Tạo thông tin chi tiết mới
		LocalDateTime sysdate = this.getSysdate();
		this.validExportStatementDto(source, sysdate);
		exportStatementDetails = this.createExportStatementDetails(source, sysdate);
		exportStatementDetails.forEach(detail -> detail.setExportStatement(target));
		this.exportStatementDetailRepository.saveAll(exportStatementDetails);

		target.setShippingPartner(source.getShippingPartner());
		target.setDescription(source.getDescription());
		target.setExportStatementDetails(exportStatementDetails);
		return new ResponseEntity<>(target, HttpStatus.OK);
	}

	/*
	 * Tạo phiếu xuất từ mẫu phiếu xuất ---------------------------------------- -
	 * FC/DC -> DC: Tạo phiếu thực xuất trừ kho luôn ------------------------- -
	 * Merchant tạo đơn: tạo order để lưu ------------------------------------ =>
	 * Khi merchant lên lấy hàng quét QR thì sẽ tạo phiếu thực xuất từ mẫu phiếu
	 * xuất giống như FC/DC -> DC thay ToStore bằng Merchant
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<ExportStatementDto> exportStatementDtos) throws BusinessException {
		List<ExportStatementEntity> list = new ArrayList<>();
		for (ExportStatementDto exportStatementDto : exportStatementDtos) {
			ExportStatementEntity exportStatement = this.createExportStatementEntity(exportStatementDto);
			list.add(exportStatement);
			// TODO: Cái này để dành khi nào có sharevan thì tìm cách để đút vào sau :(
//			try {
//				this.createExportStatementDelivery(exportStatement);
//			} catch (Exception e) {
//				ExportStatementServiceImpl.log.error(e.getMessage(), e);
//			}
		}
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> createFromMerchantOrder(ExportStatementDto exportStatementDto)
			throws BusinessException {
		return new ResponseEntity<>(this.createByMerchantOrder(exportStatementDto), HttpStatus.OK);
	}

	@Override
	public ExportStatementEntity createByMerchantOrder(ExportStatementDto exportStatementDto) throws BusinessException {
		// Kiểm tra code truyền vào
		if (exportStatementDto.getMerchantOrderId() == null) {
			throw new BusinessException(this.translator.toLocale("merchant.order.input.info"));
		}
		Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository
				.findById(exportStatementDto.getMerchantOrderId());
		if (!oMerchantOrder.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString(
					ExportStatementServiceImpl.MERCHANT_ORDER_ID_NOT_EXISTS, exportStatementDto.getMerchantOrderId()));
		}
		MerchantOrderEntity merchantOrderEntity = oMerchantOrder.get();
		if (!MerchantOrderStatus.STATUS_STORE_ENOUGH_QUANTITY.getValue().equals(merchantOrderEntity.getStatus())) {
			throw new BusinessException(this.translator.toLocale("merchant.order.status.invalid"));
		}
		exportStatementDto.setEstimatedTimeOfArrival(this.getSysdate());
		return this.createExportStatementEntity(exportStatementDto);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> createByStoreDemand(ExportStatementDto exportStatementDto) throws BusinessException {
		// Kiểm tra code truyền vào
		if ((exportStatementDto.getMerchantOrderIds() == null) || exportStatementDto.getMerchantOrderIds().isEmpty()) {
			throw new BusinessException(
					this.translator.toLocale("export.statement.input.missing.list.merchant.order.ids"));
		}
		for (Long id : exportStatementDto.getMerchantOrderIds()) {
			Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository.findById(id);
			if (!oMerchantOrder.isPresent()) {
				throw new BusinessException(
						this.translator.toLocaleByFormatString(ExportStatementServiceImpl.MERCHANT_ORDER_ID_NOT_EXISTS,
								exportStatementDto.getMerchantOrderId()));
			}
			MerchantOrderEntity merchantOrder = oMerchantOrder.get();
			if (!MerchantOrderStatus.STATUS_STORE_NOT_ENOUGH_QUANTITY.getValue().equals(merchantOrder.getStatus())) {
				throw new BusinessException(this.translator.toLocale("merchant.order.status.invalid"));
			}
			List<MerchantOrderDetailEntity> merchantOrderDetails = merchantOrder.getMerchantOrderDetails();
			for (int i = 0; i < merchantOrderDetails.size(); i++) {
				MerchantOrderDetailEntity merchantOrderDetailEntity = merchantOrderDetails.get(i);
				// Nếu bản ghi đã đủ kho/đang chờ lệnh -> Bỏ qua
				// Nếu chi tiết lệnh xuất không có sản phẩm này cũng bỏ qua
				if ((MerchantOrderDetailStatus.STATUS_ENOUGH_QUANTITY.getValue()
						.equals(merchantOrderDetailEntity.getStatus()))
						|| (MerchantOrderDetailStatus.STATUS_WAITING_FOR_STATEMENT.getValue()
								.equals(merchantOrderDetailEntity.getStatus()))
						|| exportStatementDto.getExportStatementDetails().stream().noneMatch(detail -> detail
								.getProductPackingId().equals(merchantOrderDetailEntity.getProductPacking().getId()))) {
					continue;
				}
				merchantOrderDetailEntity.setStatus(MerchantOrderDetailStatus.STATUS_WAITING_FOR_STATEMENT.getValue());
				this.mechantOrderDetailRepository.save(merchantOrderDetailEntity);
			}
			merchantOrder.setStatus(MerchantOrderStatus.STATUS_STORE_WAITING_FOR_IMPORT.getValue());
			this.merchantOrderRepository.save(merchantOrder);
		}
		ExportStatementEntity exportStatement = this.createExportStatementEntity(exportStatementDto);
		return new ResponseEntity<>(exportStatement, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> actuallyExport(Long id, ImportStatementDto importStatementDto)
			throws BusinessException {
		if (id == null) {
			throw new BusinessException(this.translator.toLocale("export.statement.input.missing.id"));
		}
		Optional<ExportStatementEntity> oExportStatement = this.exportStatementRepository.findById(id);
		if (!oExportStatement.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(ExportStatementServiceImpl.EXPORT_STATEMENT_ID_NOT_EXISTS, id));
		}
		ExportStatementEntity result = oExportStatement.get();
		if (!ExportStatementStatus.REQUESTED.getValue().equals(result.getStatus())) {
			throw new BusinessException(this.translator.toLocale("export.statement.actually.export.status"));
		}
		// Nếu là thực xuất cho đơn hàng Đại lý cấp 1 thì COMPLETED luôn
		// Nếu là thực xuất dạng điều chuyển hàng thì phải chờ thực nhập đầu kia
		MerchantOrderEntity merchantOrder = result.getMerchantOrder();
		if (merchantOrder != null) {
			this.validActuallyExportMerchantOrderDetailQuantity(merchantOrder, importStatementDto);
			if (MerchantOrderGoodsReceiveForm.SHIPPING.equals(merchantOrder.getGoodsReceiveForm())) {
				result.setShippingPartner(this.getShippingPartner(importStatementDto.getShippingPartner()));
			}
			result.setStatus(ExportStatementStatus.COMPLETED.getValue());
			merchantOrder.setStatus(MerchantOrderStatus.STATUS_STORE_EXPORTED.getValue());
			this.merchantOrderRepository.save(merchantOrder);
			OAuth2RestOperations oAuth2RestOperations = CallApiUtils.login(this.accessTokenUri, this.clientId,
					this.clientSecret, this.username, this.password);
			if (oAuth2RestOperations == null) {
				throw new BusinessException(this.translator.toLocale("common.authen.faild"));
			}
			ResponseEntity<Object> apiResult = CallApiUtils.get(this.host,
					oAuth2RestOperations.getAccessToken().getValue(),
					String.format("/api/v1/orderV2/warehouseDeliveredConfirm?orderCode=%s", merchantOrder.getCode()),
					Object.class);
			if (!HttpStatus.OK.equals(apiResult.getStatusCode())) {
				throw new BusinessException(this.translator.toLocale("merchant.order.status.update.dcm.failed"));
			}
		} else {
			result.setStatus(ExportStatementStatus.PROCESSING.getValue());
			importStatementDto.setCode(result.getCode());
			this.importStatementService.createImportStatementEntity(importStatementDto,
					ImportStatementStatus.NOT_UPDATE_INVENTORY);
		}
		result = this.exportStatementRepository.save(result);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	private ShippingPartnerEntity getShippingPartner(ShippingPartnerEntity shippingPartner) throws BusinessException {
		if ((shippingPartner == null) || (shippingPartner.getId() == null)) {
			throw new BusinessException("export.statement.actual.export.missing.shipping.partner");
		}
		Optional<ShippingPartnerEntity> oShippingPartner = this.shippingPartnerRepository
				.findById(shippingPartner.getId());
		if (!oShippingPartner.isPresent()
				|| !ShippingPartnerStatus.APPROVED.equals(oShippingPartner.get().getStatus())) {
			throw new BusinessException("export.statement.input.missing.shipping.partner");
		}
		return oShippingPartner.get();
	}

	private void validActuallyExportMerchantOrderDetailQuantity(MerchantOrderEntity merchantOrder,
			ImportStatementDto importStatementDto) throws BusinessException {
		Map<Long, Long> totalQuantityMerchantOrderDetail = merchantOrder.getMerchantOrderDetails().stream()
				.collect(Collectors.groupingBy(mod -> mod.getProductPacking().getId(),
						Collectors.summingLong(MerchantOrderDetailEntity::getQuantity)));
		Map<Long, Long> totalQuantityImportStatementDetail = ((importStatementDto == null)
				|| (importStatementDto.getImportStatementDetails() == null))
						? new HashMap<>()
						: importStatementDto.getImportStatementDetails().stream()
								.filter(is -> (is.getProductPackingId() != null) && (is.getQuantity() != null))
								.collect(Collectors.groupingBy(ImportStatementDetailDto::getProductPackingId,
										Collectors.summingLong(ImportStatementDetailDto::getQuantity)));
		for (Map.Entry<Long, Long> entry : totalQuantityMerchantOrderDetail.entrySet()) {
			Long productPackingId = entry.getKey();
			Long isdq = 0L;
			if (totalQuantityImportStatementDetail.containsKey(productPackingId)) {
				if (totalQuantityImportStatementDetail.get(productPackingId) < 0) {
					throw new BusinessException(this.translator.toLocale("import.statement.detail.quantity.invalid"));
				}
				isdq = totalQuantityImportStatementDetail.get(productPackingId);
			}
			Long modq = 0L;
			if (totalQuantityMerchantOrderDetail.containsKey(productPackingId)) {
				modq = totalQuantityMerchantOrderDetail.get(productPackingId);
			}
			if (!modq.equals(isdq)) {
				throw new BusinessException(this.translator
						.toLocale("import.statement.detail.quantity.must.be.less.than.export.statement.detail"));
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> cancel(Long id) throws BusinessException {
		Optional<ExportStatementEntity> oExportStatementEntity = this.exportStatementRepository.findById(id);
		if (!oExportStatementEntity.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(ExportStatementServiceImpl.EXPORT_STATEMENT_ID_NOT_EXISTS, id));
		}
		ExportStatementEntity exportStatement = oExportStatementEntity.get();
		if (!exportStatement.getStatus().equals(ExportStatementStatus.REQUESTED.getValue())) {
			throw new BusinessException(this.translator.toLocale("export.statement.cannot.cancel"));
		}
		List<ClaimDetail> claimDetails = this.claimService
				.getApprovedClaimDetailsByTypeAndReferenceId(ClaimType.EXPORT_STATEMENT, exportStatement.getId());
		if (claimDetails != null) {
			throw new BusinessException(this.translator.toLocale("export.statement.exists.approved.claim"));
		}
		exportStatement.setStatus(ExportStatementStatus.CANCELED.getValue());
		exportStatement = this.exportStatementRepository.save(exportStatement);
		List<ExportStatementDetailEntity> exportStatementDetails = exportStatement.getExportStatementDetails();
		this.returnProductPackingQuantity(exportStatementDetails);
		if (exportStatement.getMerchantOrderFromDC() != null) {
			this.rollBackMerchantOrderStatus(exportStatement);
		}
		return new ResponseEntity<>(exportStatement, HttpStatus.OK);
	}

	private void rollBackMerchantOrderStatus(ExportStatementEntity exportStatement) throws BusinessException {
		for (Long merchantOrderId : exportStatement.getMerchantOrderFromDC()) {
			Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository.findById(merchantOrderId);
			if (!oMerchantOrder.isPresent()) {
				throw new BusinessException(this.translator.toLocaleByFormatString(
						ExportStatementServiceImpl.MERCHANT_ORDER_ID_NOT_EXISTS, merchantOrderId));
			}
			MerchantOrderEntity merchantOrder = oMerchantOrder.get();
			for (MerchantOrderDetailEntity merchantOrderDetail : merchantOrder.getMerchantOrderDetails()) {
				if (MerchantOrderDetailStatus.STATUS_WAITING_FOR_STATEMENT.getValue()
						.equals(merchantOrderDetail.getStatus())) {
					merchantOrderDetail.setStatus(MerchantOrderDetailStatus.STATUS_NOT_ENOUGH_QUANTITY.getValue());
					this.mechantOrderDetailRepository.save(merchantOrderDetail);
				}
			}
			if (MerchantOrderStatus.STATUS_STORE_WAITING_FOR_IMPORT.getValue().equals(merchantOrder.getStatus())) {
				merchantOrder.setStatus(MerchantOrderStatus.STATUS_STORE_NOT_ENOUGH_QUANTITY.getValue());
				this.merchantOrderRepository.save(merchantOrder);
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> cancelDetail(Long id, List<Long> detailIds) throws BusinessException {
		Optional<ExportStatementEntity> oExportStatementEntity = this.exportStatementRepository.findById(id);
		if (!oExportStatementEntity.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(ExportStatementServiceImpl.EXPORT_STATEMENT_ID_NOT_EXISTS, id));
		}
		ExportStatementEntity exportStatement = oExportStatementEntity.get();
		if (!exportStatement.getStatus().equals(ExportStatementStatus.REQUESTED.getValue())) {
			throw new BusinessException(this.translator.toLocale("export.statement.cannot.cancel"));
		}
		List<ExportStatementDetailEntity> exportStatementDetailEntities = exportStatement.getExportStatementDetails();
		List<Long> exportStatementIds = exportStatementDetailEntities.stream().map(ExportStatementDetailEntity::getId)
				.collect(Collectors.toList());
		if ((detailIds == null) || detailIds.isEmpty() || detailIds.stream().noneMatch(exportStatementIds::contains)) {
			throw new BusinessException(this.translator.toLocale("export.statement.detail.id.not.exists"));
		}
		List<ExportStatementDetailEntity> details = exportStatementDetailEntities.stream()
				.filter(detail -> detailIds.contains(detail.getId())).collect(Collectors.toList());
		this.returnProductPackingQuantity(details);
		this.exportStatementDetailRepository.deleteAll(details);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private void returnProductPackingQuantity(List<ExportStatementDetailEntity> exportStatementDetails)
			throws BusinessException {
		for (ExportStatementDetailEntity exportStatementDetail : exportStatementDetails) {
			StoreProductPackingDetailEntity storeProductPackingDetail = exportStatementDetail
					.getStoreProductPackingDetail();
			StoreProductPackingEntity storeProductPacking = storeProductPackingDetail.getStoreProductPacking();
			storeProductPackingDetail
					.setQuantity(storeProductPackingDetail.getQuantity() + exportStatementDetail.getQuantity());
			storeProductPacking
					.setTotalQuantity(storeProductPacking.getTotalQuantity() + exportStatementDetail.getQuantity());
			this.storeProductPackingDetailRepository.save(storeProductPackingDetail);
			this.storeProductPackingRepository.save(storeProductPacking);
			if ((exportStatementDetail.getPalletDetail() != null)
					&& (exportStatementDetail.getPalletDetail().getId() != null)) {
				this.palletService.pushUpPalletDetail(exportStatementDetail.getPalletDetail().getId(),
						exportStatementDetail.getQuantity());
			}
		}
	}

	private void validExportStatementDto(ExportStatementDto exportStatementDto, LocalDateTime sysdate)
			throws BusinessException {
		Optional<StoreEntity> oFromStore = this.storeRepository
				.findById(exportStatementDto.getFromStoreId() == null ? 0L : exportStatementDto.getFromStoreId());
		if (!oFromStore.isPresent()) {
			throw new BusinessException(this.translator.toLocale("export.statement.input.missing.from.store"));
		}
		StoreEntity fromStore = oFromStore.get();
		exportStatementDto.setFromStore(fromStore);

		Optional<StoreEntity> oToStore = this.storeRepository
				.findById(exportStatementDto.getToStoreId() == null ? 0L : exportStatementDto.getToStoreId());
		if (oToStore.isPresent()) {
			StoreEntity toStore = oToStore.get();
			exportStatementDto.setToStore(toStore);
			this.validDeliveryTime(exportStatementDto, fromStore, toStore, sysdate);
		} else if (StringUtils.isNullOrEmpty(exportStatementDto.getMerchantCode())) {
			throw new BusinessException(this.translator.toLocale("export.statement.missing.to.store.merchant"));
		}

		if (exportStatementDto.getMerchantOrderId() == null) {
			exportStatementDto.setShippingPartner(this.getShippingPartner(exportStatementDto.getShippingPartner()));
		} else {
			Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository
					.findById(exportStatementDto.getMerchantOrderId());
			if (!oMerchantOrder.isPresent()) {
				throw new BusinessException(
						this.translator.toLocaleByFormatString(ExportStatementServiceImpl.MERCHANT_ORDER_ID_NOT_EXISTS,
								exportStatementDto.getMerchantOrderId()));
			}
		}

		if (!exportStatementDto.getMerchantOrderIds().isEmpty()) {
			for (Long merchantOrderId : exportStatementDto.getMerchantOrderIds()) {
				Optional<MerchantOrderEntity> oMerchantOrder = this.merchantOrderRepository.findById(merchantOrderId);
				if (!oMerchantOrder.isPresent()) {
					throw new BusinessException(this.translator.toLocaleByFormatString(
							ExportStatementServiceImpl.MERCHANT_ORDER_ID_NOT_EXISTS, merchantOrderId));
				}
			}
		}

		this.validExportStatementDetailDtos(exportStatementDto);
	}

	private void validExportStatementDetailDtos(ExportStatementDto exportStatementDto) throws BusinessException {
		if ((exportStatementDto.getExportStatementDetails() == null)
				|| (exportStatementDto.getExportStatementDetails().isEmpty())) {
			throw new BusinessException(this.translator.toLocale("export.statement.can.not.missing.from.store.info"));
		}
		for (ExportStatementDetailDto exportStatementDetailDto : exportStatementDto.getExportStatementDetails()) {
			this.validExportStatementDetailDto(exportStatementDetailDto);
		}
	}

	private void validExportStatementDetailDto(ExportStatementDetailDto exportStatementDetailDto)
			throws BusinessException {
		if (exportStatementDetailDto.getProductPackingId() == null) {
			throw new BusinessException(this.translator.toLocale("export.statement.detail.missing.product.packing"));
		}
		Optional<ProductPackingEntity> oProductPacking = this.productPackingRepository
				.findById(exportStatementDetailDto.getProductPackingId());
		if (!oProductPacking.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.id.not.exists",
					exportStatementDetailDto.getProductPackingId()));
		}
		if ((exportStatementDetailDto.getQuantity() == null) || (exportStatementDetailDto.getQuantity() <= 0)) {
			throw new BusinessException(this.translator.toLocale("export.statement.detail.quantity.invalid"));
		}
	}

	private void validDeliveryTime(ExportStatementDto exportStatementDto, StoreEntity fromStore, StoreEntity toStore,
			LocalDateTime sysdate) throws BusinessException {
		List<StoreDeliveryWaitEntity> storeDeliveryWaits = this.storeDeliveryWaitRepository
				.getDeliveryWaitForMerchantOrderInventory(Collections.singletonList(fromStore.getId()),
						toStore.getId());
		if ((storeDeliveryWaits == null) || storeDeliveryWaits.isEmpty()) {
			throw new BusinessException(this.translator.toLocale("store.delivery.wait.missing"));
		}
		exportStatementDto
				.setEstimatedTimeOfArrival(sysdate.plusDays(storeDeliveryWaits.get(0).getMaxDayToWait() + 1L));
	}

	private ExportStatementEntity createExportStatementEntity(ExportStatementDto exportStatementDto)
			throws BusinessException {
		// Thông tin đầu vào thiếu từ kho vs chi tiết -> Next
		LocalDateTime sysdate = this.getSysdate();
		this.validExportStatementDto(exportStatementDto, sysdate);
		// Bắt đầu tạo lệnh xuất
		ExportStatementEntity exportStatementEntity = new ExportStatementEntity();
		exportStatementEntity.setFromStore(exportStatementDto.getFromStore());
		exportStatementEntity
				.setToStore(exportStatementDto.getToStoreId() == null ? null : exportStatementDto.getToStore());
		exportStatementEntity.setMerchantCode(exportStatementDto.getMerchantCode());
		if (StringUtils.isNullOrEmpty(exportStatementDto.getCode())) {
			// Tạo QRCode để scan ^^
			String uniqueCode = String.format("%s-STO-%09d", sysdate.format(DateTimeFormatter.ofPattern("yyMMdd")),
					Long.valueOf(this.getGeneratorValue("EXPORT_STATEMENT_CODE")));
			exportStatementDto.setCode(uniqueCode);
		}
		exportStatementEntity.setCode(exportStatementDto.getCode());
		exportStatementEntity.setqRCode(QRCodeGenerator.generate(exportStatementDto.getCode()));
		exportStatementEntity.setDescription(exportStatementDto.getDescription());
		exportStatementEntity.setExportDate(sysdate);
		exportStatementEntity.setEstimatedTimeOfArrival(exportStatementDto.getEstimatedTimeOfArrival());
		exportStatementEntity.setMerchantOrder(exportStatementDto.getMerchantOrderId() == null ? null
				: new MerchantOrderEntity(exportStatementDto.getMerchantOrderId()));
		exportStatementEntity.setMerchantOrderFromDC(
				(exportStatementDto.getMerchantOrderIds() == null) || exportStatementDto.getMerchantOrderIds().isEmpty()
						? null
						: exportStatementDto.getMerchantOrderIds());
		exportStatementEntity.setShippingPartner(exportStatementDto.getShippingPartner());
		exportStatementEntity.setStatus(ExportStatementStatus.REQUESTED.getValue());
		// Tạo chi tiết cho lệnh xuất
		List<ExportStatementDetailEntity> exportStatementDetailEntities = this
				.createExportStatementDetails(exportStatementDto, sysdate);
		if (exportStatementDto.getMerchantOrderId() != null) {
			// Chỉ SUM price khi lập lệnh xuất kho từ đơn hàng của merchant
			Double sum = exportStatementDetailEntities.stream()
					.mapToDouble(
							o -> o.getProductPackingPrice().multiply(BigDecimal.valueOf(o.getQuantity())).doubleValue())
					.sum();
			exportStatementEntity.setTotal(BigDecimal.valueOf(sum));
		}
		// Lưu lệnh xuất và chi tiết lệnh xuất hàng
		exportStatementEntity = this.exportStatementRepository.save(exportStatementEntity);
		for (ExportStatementDetailEntity exportStatementDetailEntity : exportStatementDetailEntities) {
			exportStatementDetailEntity.setExportStatement(exportStatementEntity);
		}
		exportStatementDetailEntities = this.exportStatementDetailRepository.saveAll(exportStatementDetailEntities);
		exportStatementEntity.setExportStatementDetails(exportStatementDetailEntities);
		return exportStatementEntity;
	}

	private List<ExportStatementDetailEntity> createExportStatementDetails(ExportStatementDto exportStatementDto,
			LocalDateTime sysdate) throws BusinessException {
		List<ExportStatementDetailEntity> exportStatementDetailEntities = new ArrayList<>();
		List<Long> productPackingIds = exportStatementDto.getExportStatementDetails().stream()
				.map(ExportStatementDetailDto::getProductPackingId).collect(Collectors.toList());
		List<StoreProductPackingEntity> storeProductPackings = this.storeProductPackingRepository
				.getAvaiableForExport(exportStatementDto.getFromStoreId(), productPackingIds);
		for (ExportStatementDetailDto exportStatementDetailDto : exportStatementDto.getExportStatementDetails()) {
			// Lấy thông tin hàng ở kho
			// - Nếu hàng không có trong kho -> Next
			List<StoreProductPackingEntity> storeProductPackingsByProductPackingId = storeProductPackings.stream()
					.filter(storeProductPacking -> storeProductPacking.getProductPackingId()
							.equals(exportStatementDetailDto.getProductPackingId()))
					.collect(Collectors.toList());

			if (storeProductPackingsByProductPackingId.isEmpty()) {
				throw new BusinessException(this.translator.toLocaleByFormatString(
						"store.can.not.done.export.statement", exportStatementDto.getFromStore().getCode()));
			}
			// - Nếu hàng có trong kho nhưng số lượng không đủ -> Next
			Long tempQuantity = exportStatementDetailDto.getQuantity();
			if (tempQuantity <= 0) {
				throw new BusinessException(this.translator.toLocale("export.statement.detail.quantity.invalid"));
			}
			if (storeProductPackingsByProductPackingId.stream().mapToLong(StoreProductPackingEntity::getTotalQuantity)
					.sum() < tempQuantity) {
				throw new BusinessException(this.translator.toLocaleByFormatString(
						"store.not.enough.product.for.done.export.statement",
						exportStatementDto.getFromStore().getCode(), exportStatementDetailDto.getProductPackingCode()));
			}
			// - Nếu hàng có trong kho thì ưu tiên theo expire_date gần nhất
			storeProductPackingsByProductPackingId.sort(Comparator.comparing(StoreProductPackingEntity::getExpireDate));

			exportStatementDetailEntities
					.addAll(this.createExportStatementDetails(storeProductPackingsByProductPackingId, tempQuantity,
							exportStatementDetailDto.getPrice(), sysdate));
		}
		return exportStatementDetailEntities;
	}

	private List<ExportStatementDetailEntity> createExportStatementDetails(
			List<StoreProductPackingEntity> storeProductPackingsByProductPackingId, Long tempQuantity, BigDecimal price,
			LocalDateTime sysdate) throws BusinessException {
		List<ExportStatementDetailEntity> exportStatementDetailEntities = new ArrayList<>();
		// Xác định lô hàng chi tiết để xuất kho
		// - Ưu tiên lô hàng của expire_date nhập vào trước (create_date)
		// thì cho đi trước
		for (StoreProductPackingEntity storeProductPacking : storeProductPackingsByProductPackingId) {
			List<StoreProductPackingDetailEntity> storeProductPackingDetails = this.storeProductPackingDetailRepository
					.getDetailByStoreProductPackingId(storeProductPacking.getId());
			if (tempQuantity <= 0) {
				break;
			}
			for (StoreProductPackingDetailEntity storeProductPackingDetail : storeProductPackingDetails) {
				if (tempQuantity <= 0) {
					break;
				}
				ExportStatementDetailEntity exportStatementDetailEntity = new ExportStatementDetailEntity();
				exportStatementDetailEntity.setProductPackingPrice(price);
				exportStatementDetailEntity.setExportDate(sysdate);
				exportStatementDetailEntity.setStoreProductPackingDetail(storeProductPackingDetail);
				exportStatementDetailEntity.setProductPacking(storeProductPacking.getProductPacking());
				exportStatementDetailEntity.setExpireDate(storeProductPacking.getExpireDate());
				long minusQuantity;
				if (tempQuantity <= storeProductPackingDetail.getQuantity()) {
					// Tạo chi tiết lệnh xuất với chi tiết kho
					exportStatementDetailEntity.setQuantity(tempQuantity);
					// --
					minusQuantity = tempQuantity;
					storeProductPacking.setTotalQuantity(storeProductPacking.getTotalQuantity() - tempQuantity);
					storeProductPackingDetail.setQuantity(storeProductPackingDetail.getQuantity() - tempQuantity);
					tempQuantity = 0L;
				} else {
					// Tạo chi tiết lệnh xuất với chi tiết kho
					exportStatementDetailEntity.setQuantity(storeProductPackingDetail.getQuantity());
					// --
					minusQuantity = storeProductPackingDetail.getQuantity();
					storeProductPacking.setTotalQuantity(
							storeProductPacking.getTotalQuantity() - storeProductPackingDetail.getQuantity());
					tempQuantity = tempQuantity - storeProductPackingDetail.getQuantity();
					storeProductPackingDetail.setQuantity(0L);
				}
				this.storeProductPackingDetailRepository.save(storeProductPackingDetail);
				Optional<PalletDetailEntity> oPalletDetail = this.palletDetailRepository
						.findByStoreProductPackingDetailId(storeProductPackingDetail.getId());
				if (oPalletDetail.isPresent()) {
					this.palletService.pullDownPalletDetail(oPalletDetail.get().getId(), minusQuantity);
					exportStatementDetailEntity.setPalletDetail(oPalletDetail.get());
				}
				exportStatementDetailEntities.add(exportStatementDetailEntity);
			}
			this.storeProductPackingRepository.save(storeProductPacking);
		}
		return exportStatementDetailEntities;
	}

}
