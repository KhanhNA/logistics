package com.tsolution._3services.impl;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.UserEntity;
import com.tsolution._1entities.dto.AppDashboardDto;
import com.tsolution._1entities.dto.DashboardTaskListPoDto;
import com.tsolution._1entities.dto.DashboardTaskListPoDto.DashboardTaskListPoDatasetDto;
import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.enums.ExpireDateColors;
import com.tsolution._1entities.enums.ExportStatementStatus;
import com.tsolution._1entities.enums.PoStatus;
import com.tsolution._1entities.export_statement.dto.ExportStatementFilterDto;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._2repositories.ExportStatementRepository;
import com.tsolution._2repositories.PoRepository;
import com.tsolution._3services.TaskListService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;

@Service
public class TaskListServiceImpl extends LogisticsBaseService implements TaskListService {

	@Autowired
	private PoRepository poRepository;

	@Autowired
	private ExportStatementRepository exportStatementRepositor;

	@Override
	public ResponseEntity<Object> getTaskListPo() throws BusinessException {
		List<PoEntity> pos = this.poRepository.getTaskListPo(this.getCurrentUsername(),
				Arrays.asList(PoStatus.APPROVED.getValue(), PoStatus.ARRIVED_VIETNAM_PORT.getValue(),
						PoStatus.ARRIVED_MYANMAR_PORT.getValue()));
		LocalDateTime sysdate = this.getSysdate();
		List<StoreEntity> stores = pos.stream().map(PoEntity::getStore).collect(Collectors.toList());
		List<Long> storeIds = stores.stream().map(StoreEntity::getId).distinct().collect(Collectors.toList());

		List<String> lables = new ArrayList<>();
		for (Long storeId : storeIds) {
			lables.add(stores.stream().filter(store -> store.getId().equals(storeId))
					.map(store -> String.format("%s - %s", store.getCode(), store.getName())).findFirst().orElse(""));
		}

		List<DashboardTaskListPoDatasetDto> datasets = new ArrayList<>();
		if (Boolean.TRUE.equals(this.hasAuthority(Arrays.asList("patch/pos/{id}/arrive-vietnam-port")))) {
			List<PoEntity> poMissingArriveVietnamPortDate = pos.stream().filter(po -> po.getStatus()
					.equals(PoStatus.APPROVED) && (po.getArriveVietnamPortDate() == null)
					&& (Period.between(po.getApproveDate().toLocalDate(), sysdate.toLocalDate()).getDays() >= 1))
					.collect(Collectors.toList());
			List<Long> countMissingArriveVietnamPortDate = new ArrayList<>();
			for (Long storeId : storeIds) {
				countMissingArriveVietnamPortDate.add(poMissingArriveVietnamPortDate.stream()
						.filter(x -> x.getStore().getId().equals(storeId)).count());
			}
			DashboardTaskListPoDatasetDto missingArriveVietnamPortDate = new DashboardTaskListPoDatasetDto();
			missingArriveVietnamPortDate
					.setLabel(this.translator.toLocale("dashboard.task.list.po.missing.arrive.vietnam.port.date"));
			missingArriveVietnamPortDate.setBackgroundColor("#f53794");
			missingArriveVietnamPortDate.setData(countMissingArriveVietnamPortDate);
			datasets.add(missingArriveVietnamPortDate);
		}

		if (Boolean.TRUE.equals(this.hasAuthority(Arrays.asList("patch/pos/{id}/arrive-myanmar-port")))) {
			List<PoEntity> poMissingArriveMyanmarPortDate = pos.stream()
					.filter(po -> po.getStatus().equals(PoStatus.ARRIVED_VIETNAM_PORT)
							&& (po.getArriveMyanmarPortDate() == null)
							&& (Period.between(po.getArrivedVietnamPortDate().toLocalDate(), sysdate.toLocalDate())
									.getDays() >= 1))
					.collect(Collectors.toList());
			List<Long> countMissingArriveMyanmarPortDate = new ArrayList<>();
			for (Long storeId : storeIds) {
				countMissingArriveMyanmarPortDate.add(poMissingArriveMyanmarPortDate.stream()
						.filter(x -> x.getStore().getId().equals(storeId)).count());
			}
			DashboardTaskListPoDatasetDto missingArriveMyanmarPortDate = new DashboardTaskListPoDatasetDto();
			missingArriveMyanmarPortDate
					.setLabel(this.translator.toLocale("dashboard.task.list.po.missing.arrive.myanmar.port.date"));
			missingArriveMyanmarPortDate.setBackgroundColor("#4dc9f6");
			missingArriveMyanmarPortDate.setData(countMissingArriveMyanmarPortDate);
			datasets.add(missingArriveMyanmarPortDate);
		}

		if (Boolean.TRUE.equals(this.hasAuthority(Arrays.asList("patch/pos/{id}/arrive-fc")))) {
			List<PoEntity> poMissingArriveFcDate = pos.stream()
					.filter(po -> po.getStatus().equals(PoStatus.ARRIVED_MYANMAR_PORT) && (po.getArriveFcDate() == null)
							&& (Period.between(po.getArrivedMyanmarPortDate().toLocalDate(), sysdate.toLocalDate())
									.getDays() >= 1))
					.collect(Collectors.toList());
			List<Long> countMissingArriveFcDate = new ArrayList<>();
			for (Long storeId : storeIds) {
				countMissingArriveFcDate
						.add(poMissingArriveFcDate.stream().filter(x -> x.getStore().getId().equals(storeId)).count());
			}
			DashboardTaskListPoDatasetDto missingArriveFcDate = new DashboardTaskListPoDatasetDto();
			missingArriveFcDate.setLabel(this.translator.toLocale("dashboard.task.list.po.missing.arrive.fc.date"));
			missingArriveFcDate.setBackgroundColor("#f67019");
			missingArriveFcDate.setData(countMissingArriveFcDate);
			datasets.add(missingArriveFcDate);
		}

		DashboardTaskListPoDto dashboardTaskListPoDto = new DashboardTaskListPoDto();
		dashboardTaskListPoDto.setTitleText(this.translator.toLocale("dashboard.task.list.po"));
		dashboardTaskListPoDto.setLabels(lables);
		dashboardTaskListPoDto.setDatasets(datasets);

		return new ResponseEntity<>(dashboardTaskListPoDto, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getStoreAppDashboard(String username, String acceptLanguage)
			throws BusinessException {
		Optional<UserEntity> oUser = this.userRepository
				.findOneByUsername(StringUtils.isNullOrEmpty(username) ? "" : username);
		if (!oUser.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("user.username.not.exists", username));
		}
		List<StoreEntity> stores = oUser.get().getStores();
		if ((stores == null) || stores.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		StoreEntity store = stores.get(0);
		AppDashboardDto appDashboardDto = new AppDashboardDto();
		appDashboardDto.setStoreId(store.getId());
		if (store.isDc()) {
			// SO chờ xuất
			ExportStatementFilterDto exportStatementFilterDto = new ExportStatementFilterDto();
			exportStatementFilterDto.setType("from");
			exportStatementFilterDto.setFromStoreId(store.getId());
			exportStatementFilterDto.setStatus(ExportStatementStatus.REQUESTED);
			exportStatementFilterDto.setIsExportSo(true);
			appDashboardDto.setSoWaitForExport(
					this.exportStatementRepositor.count(exportStatementFilterDto, this.getCurrentUsername()));
		} else {
			// PO Chờ nhận
			List<PoEntity> pos = this.poRepository.getTaskListPo(this.getCurrentUsername(),
					Arrays.asList(PoStatus.ARRIVED_MYANMAR_PORT.getValue()));
			appDashboardDto.setPoWaitForImport(pos.stream().filter(po -> po.getArriveFcDate() != null).count());
		}

		// BOTH FC & DC
		ExportStatementFilterDto exportStatementFilterDto;
		// STO chờ xuất (Tạo theo kế hoạch + Tạo theo bổ sung hàng)
		exportStatementFilterDto = new ExportStatementFilterDto();
		exportStatementFilterDto.setType("from");
		exportStatementFilterDto.setFromStoreId(store.getId());
		exportStatementFilterDto.setStatus(ExportStatementStatus.REQUESTED);
		exportStatementFilterDto.setIsExportSo(false);
		appDashboardDto.setStoWaitForExport(
				this.exportStatementRepositor.count(exportStatementFilterDto, this.getCurrentUsername()));
		// STO đang giao
		exportStatementFilterDto = new ExportStatementFilterDto();
		exportStatementFilterDto.setType("from");
		exportStatementFilterDto.setFromStoreId(store.getId());
		exportStatementFilterDto.setStatus(ExportStatementStatus.PROCESSING);
		exportStatementFilterDto.setIsExportSo(false);
		appDashboardDto.setStoDelivery(
				this.exportStatementRepositor.count(exportStatementFilterDto, this.getCurrentUsername()));
		// STO chờ nhập
		exportStatementFilterDto = new ExportStatementFilterDto();
		exportStatementFilterDto.setType("to");
		exportStatementFilterDto.setToStoreId(store.getId());
		exportStatementFilterDto.setStatus(ExportStatementStatus.PROCESSING);
		exportStatementFilterDto.setIsExportSo(false);
		appDashboardDto.setStoWaitForImport(
				this.exportStatementRepositor.count(exportStatementFilterDto, this.getCurrentUsername()));

		// INVENTORY
		InventoryDto inventoryDto = new InventoryDto();
		inventoryDto.setStoreId(store.getId());
		inventoryDto.setAcceptLanguage(acceptLanguage);
		inventoryDto.setExceptProductPackingIds(Arrays.asList(-1L));
		// INVENTORY_WARNING_OUT_OF_DATE: SP cảnh báo hết hạn
		inventoryDto.setExpireDateColor(ExpireDateColors.WARNING);
		appDashboardDto.setInventoryWarningOutOfDate(
				this.storeProductPackingRepository.inventory(inventoryDto, this.getCurrentUsername()));
		// INVENTORY_OUT_OF_DATE: SP hết hạn
		inventoryDto.setExpireDateColor(ExpireDateColors.OUT_OF_DATE);
		appDashboardDto.setInventoryOutOfDate(
				this.storeProductPackingRepository.inventory(inventoryDto, this.getCurrentUsername()));

		return new ResponseEntity<>(appDashboardDto, HttpStatus.OK);
	}

}
