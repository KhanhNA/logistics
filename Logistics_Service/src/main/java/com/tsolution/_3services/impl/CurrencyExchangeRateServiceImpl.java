package com.tsolution._3services.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._1entities.CurrencyExchangeRateEntity;
import com.tsolution._2repositories.CurrencyExchangeRateRepository;
import com.tsolution._2repositories.CurrencyRepository;
import com.tsolution._3services.CurrencyExchangeRateService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.DatetimeUtils;

@Service
public class CurrencyExchangeRateServiceImpl extends LogisticsBaseService implements CurrencyExchangeRateService {

	@Autowired
	private CurrencyExchangeRateRepository currencyExchangeRateRepository;

	@Autowired
	private CurrencyRepository currencyRepository;

	@Override
	public ResponseEntity<Object> findAll() throws BusinessException {
		return new ResponseEntity<>(this.translator.toLocale(BusinessException.COMMON_NOT_SUPPORT), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<CurrencyExchangeRateEntity> oCurrencyExchangeRate = this.currencyExchangeRateRepository
				.findById(id == null ? 0 : id);
		if (!oCurrencyExchangeRate.isPresent()) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		return new ResponseEntity<>(oCurrencyExchangeRate.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(List<CurrencyExchangeRateEntity> entities) throws BusinessException {
		List<CurrencyExchangeRateEntity> currencyExchangeRates = new ArrayList<>();
		LocalDateTime sysdate = this.getSysdate();
		for (CurrencyExchangeRateEntity currencyExchangeRateEntity : entities) {
			if (currencyExchangeRateEntity.getId() != null) {
				throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
			}
			this.validCurrencyExchangeRateEntity(currencyExchangeRateEntity, sysdate);
			currencyExchangeRates.add(this.currencyExchangeRateRepository.save(currencyExchangeRateEntity));
		}
		return new ResponseEntity<>(currencyExchangeRates, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long id, CurrencyExchangeRateEntity source) throws BusinessException {
		Optional<CurrencyExchangeRateEntity> oCurrencyExchangeRate = this.currencyExchangeRateRepository
				.findById(id == null ? 0 : id);
		if (!oCurrencyExchangeRate.isPresent()) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		LocalDateTime sysdate = this.getSysdate();
		this.validCurrencyExchangeRateEntity(source, sysdate);
		CurrencyExchangeRateEntity currencyExchangeRate = oCurrencyExchangeRate.get();
		if (currencyExchangeRate.getFromDate().toLocalDate().isAfter(sysdate.toLocalDate())
				&& source.getFromDate().toLocalDate().isAfter(sysdate.toLocalDate())) {
			currencyExchangeRate.setFromCurrency(source.getFromCurrency());
			currencyExchangeRate.setToCurrency(source.getToCurrency());
			currencyExchangeRate.setExchangeRate(source.getExchangeRate());
			currencyExchangeRate.setFromDate(source.getFromDate());
		}
		currencyExchangeRate.setToDate(source.getToDate());
		return new ResponseEntity<>(this.currencyExchangeRateRepository.save(currencyExchangeRate), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findCurrencyExchangeRate(Long fromCurrencyId, Long toCurrencyId,
			LocalDateTime fromDate, LocalDateTime toDate, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		return new ResponseEntity<>(this.currencyExchangeRateRepository.findCurrencyExchangeRate(fromCurrencyId,
				toCurrencyId, DatetimeUtils.localDateTime2SqlDate(fromDate),
				DatetimeUtils.localDateTime2SqlDate(toDate), PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	private void validCurrencyExchangeRateEntity(CurrencyExchangeRateEntity currencyExchangeRate, LocalDateTime sysdate)
			throws BusinessException {
		if ((currencyExchangeRate.getFromCurrency() == null)
				|| (currencyExchangeRate.getFromCurrency().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("currency.exchange.rate.input.missing.from.currency"));
		}
		if ((currencyExchangeRate.getToCurrency() == null) || (currencyExchangeRate.getToCurrency().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("currency.exchange.rate.input.missing.to.currency"));
		}
		if (currencyExchangeRate.getFromCurrency().getId().equals(currencyExchangeRate.getToCurrency().getId())) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		Optional<CurrencyEntity> oFromCurrency = this.currencyRepository
				.findById(currencyExchangeRate.getFromCurrency().getId());
		if (!oFromCurrency.isPresent()) {
			throw new BusinessException(this.translator.toLocale("currency.exchange.rate.input.missing.from.currency"));
		}
		Optional<CurrencyEntity> oToCurrency = this.currencyRepository
				.findById(currencyExchangeRate.getToCurrency().getId());
		if (!oToCurrency.isPresent()) {
			throw new BusinessException(this.translator.toLocale("currency.exchange.rate.input.missing.to.currency"));
		}
		if ((currencyExchangeRate.getExchangeRate() == null)
				|| ((new BigDecimal(0)).compareTo(currencyExchangeRate.getExchangeRate()) >= 0)) {
			throw new BusinessException(this.translator.toLocale("currency.exchange.rate.input.error.exchange.rate"));
		}

		this.validCurrencyExchangeRateEntityFromDateToDate(currencyExchangeRate, sysdate);
	}

	private void validCurrencyExchangeRateEntityFromDateToDate(CurrencyExchangeRateEntity currencyExchangeRate,
			LocalDateTime sysdate) throws BusinessException {
		LocalDateTime fromDate = currencyExchangeRate.getFromDate();
		if (fromDate == null) {
			throw new BusinessException(this.translator.toLocale("currency.exchange.rate.input.missing.from.date"));
		}
		if ((currencyExchangeRate.getId() == null) && fromDate.toLocalDate().isBefore(sysdate.toLocalDate())) {
			throw new BusinessException(
					this.translator.toLocale("currency.exchange.rate.input.from.date.can.not.less.than.sysdate"));
		}
		LocalDateTime toDate = currencyExchangeRate.getToDate();
		if ((toDate != null) && toDate.toLocalDate().isBefore(sysdate.toLocalDate())) {
			throw new BusinessException(
					this.translator.toLocale("currency.exchange.rate.input.to.date.can.not.less.than.sysdate"));
		}
		if ((toDate != null) && toDate.toLocalDate().isBefore(fromDate.toLocalDate())) {
			throw new BusinessException(this.translator
					.toLocale("currency.exchange.rate.input.to.date.must.be.greater.or.equal.from.date"));
		}

		if (Boolean.TRUE
				.equals(this.currencyExchangeRateRepository.isExistsCurrencyExchangeRate(currencyExchangeRate.getId(),
						currencyExchangeRate.getFromCurrency().getId(), currencyExchangeRate.getToCurrency().getId(),
						DatetimeUtils.localDateTime2SqlDate(currencyExchangeRate.getFromDate()),
						currencyExchangeRate.getToDate() == null ? null
								: DatetimeUtils.localDateTime2SqlDate(currencyExchangeRate.getToDate())))) {
			throw new BusinessException(
					this.translator.toLocale("currency.exchange.rate.input.exists.exchange.rate.in.range"));
		}
	}
}
