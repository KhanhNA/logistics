package com.tsolution._3services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.CountryEntity;
import com.tsolution._1entities.CountryLocationEntity;
import com.tsolution._1entities.dto.CountryLocationDto;
import com.tsolution._1entities.dto.FlatTreeNode;
import com.tsolution._2repositories.CountryLocationRepository;
import com.tsolution._2repositories.ICountryDescriptionRepository;
import com.tsolution._3services.CountryService;
import com.tsolution.excetions.BusinessException;

@Service
public class CountryServiceImpl extends LogisticsBaseService implements CountryService {

	@Autowired
	private ICountryDescriptionRepository countryDescriptionRepository;

	@Autowired
	private CountryLocationRepository countryLocationRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.countryRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) {
		return new ResponseEntity<>(this.countryRepository.findById(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(List<CountryEntity> entity) {
		return null;
	}

	@Override
	public ResponseEntity<Object> update(Long id, CountryEntity source) {
		return new ResponseEntity<>(source, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(Boolean support, Long language) {
		return new ResponseEntity<>(this.countryDescriptionRepository.find(support, language), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getLocationsTree() throws BusinessException {
		List<CountryLocationDto> countryLocationDtos = this.countryLocationRepository.getLocationsTree();
		// Lấy ra distinct Country

		List<CountryLocationEntity> countries = countryLocationDtos.stream()
				.filter(this.distinctByKeys(
						Arrays.asList(CountryLocationDto::getCountryId, CountryLocationDto::getCountryCode)))
				.map(cl -> new CountryLocationEntity(cl.getCountryId(), cl.getCountryCode(), "", null))
				.collect(Collectors.toList());

		for (CountryLocationEntity country : countries) {
			List<CountryLocationEntity> regions = countryLocationDtos.stream()
					.filter(cl -> (cl.getRegionCountryId() != null) && cl.getRegionCountryId().equals(country.getId()))
					.filter(this.distinctByKeys(
							Arrays.asList(CountryLocationDto::getRegionId, CountryLocationDto::getRegionCode)))
					.map(cl -> new CountryLocationEntity(cl.getRegionId(), cl.getRegionCode(), cl.getRegionName(),
							country))
					.collect(Collectors.toList());
			country.setChildren(regions);
			for (CountryLocationEntity region : regions) {
				List<CountryLocationEntity> districts = countryLocationDtos.stream()
						.filter(cl -> (cl.getDistrictParentId() != null)
								&& cl.getDistrictParentId().equals(region.getId()))
						.filter(this.distinctByKeys(
								Arrays.asList(CountryLocationDto::getDistrictId, CountryLocationDto::getDistrictCode)))
						.map(cl -> new CountryLocationEntity(cl.getDistrictId(), cl.getDistrictCode(),
								cl.getDistrictName(), region))
						.collect(Collectors.toList());
				region.setChildren(districts);
				for (CountryLocationEntity district : districts) {
					List<CountryLocationEntity> townships = countryLocationDtos.stream()
							.filter(cl -> (cl.getTownshipParentId() != null)
									&& cl.getTownshipParentId().equals(district.getId()))
							.filter(this.distinctByKeys(Arrays.asList(CountryLocationDto::getTownshipId,
									CountryLocationDto::getTownshipCode)))
							.map(cl -> new CountryLocationEntity(cl.getTownshipId(), cl.getTownshipCode(),
									cl.getTownshipName(), district))
							.collect(Collectors.toList());
					district.setChildren(townships);
				}
			}
		}
		return new ResponseEntity<>(countries, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getLocationsTownship() throws BusinessException {
		List<CountryLocationDto> countryLocationDtos = this.countryLocationRepository.getLocationsTree();

		List<FlatTreeNode> countries = countryLocationDtos.stream()
				.filter(x -> (x.getCountryId() != null) && (x.getRegionId() != null) && (x.getDistrictId() != null)
						&& (x.getTownshipId() != null))
				.filter(this.distinctByKeys(
						Arrays.asList(CountryLocationDto::getCountryId, CountryLocationDto::getCountryCode)))
				.map(cl -> new FlatTreeNode(cl.getCountryCode(), cl.getCountryCode(), new ArrayList<>()))
				.collect(Collectors.toList());

		for (FlatTreeNode country : countries) {
			List<FlatTreeNode> regions = countryLocationDtos.stream()
					.filter(cl -> (cl.getRegionCountryId() != null) && cl.getCountryCode().equals(country.getValue()))
					.filter(this.distinctByKeys(
							Arrays.asList(CountryLocationDto::getRegionId, CountryLocationDto::getRegionCode)))
					.map(cl -> new FlatTreeNode(cl.getRegionCode(), cl.getRegionCode() + " - " + cl.getRegionName(),
							new ArrayList<>()))
					.collect(Collectors.toList());
			country.setChildren(regions);
			for (FlatTreeNode region : regions) {
				List<FlatTreeNode> districts = countryLocationDtos.stream()
						.filter(cl -> (cl.getDistrictParentId() != null)
								&& cl.getRegionCode().equals(region.getValue()))
						.filter(this.distinctByKeys(
								Arrays.asList(CountryLocationDto::getDistrictId, CountryLocationDto::getDistrictCode)))
						.map(cl -> new FlatTreeNode(cl.getDistrictCode(),
								cl.getDistrictCode() + " - " + cl.getDistrictName(), new ArrayList<>()))
						.collect(Collectors.toList());
				region.setChildren(districts);
				for (FlatTreeNode district : districts) {
					List<FlatTreeNode> townships = countryLocationDtos.stream()
							.filter(cl -> (cl.getTownshipParentId() != null)
									&& cl.getDistrictCode().equals(district.getValue()))
							.filter(this.distinctByKeys(Arrays.asList(CountryLocationDto::getTownshipId,
									CountryLocationDto::getTownshipCode)))
							.map(cl -> new FlatTreeNode(cl.getTownshipCode(),
									cl.getTownshipCode() + " - " + cl.getTownshipName(), new ArrayList<>()))
							.collect(Collectors.toList());
					district.setChildren(townships);
				}
			}
		}
		return new ResponseEntity<>(countries, HttpStatus.OK);
	}

}
