package com.tsolution._3services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.ProductEntity;
import com.tsolution._3services.IProductService;
import com.tsolution.excetions.BusinessException;

@Service
public class ProductServiceImpl extends LogisticsBaseService implements IProductService {

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.productRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) {
		return new ResponseEntity<>(this.productRepository.findById(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(List<ProductEntity> productEntities) {
		List<ProductEntity> list = new ArrayList<>();
		for (ProductEntity productEntity : productEntities) {
			list.add(this.productRepository.save(productEntity));
		}
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long id, ProductEntity source) {
		return new ResponseEntity<>(source, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(String text, List<Long> exceptId, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		return new ResponseEntity<>(
				this.productRepository.find(text, exceptId, PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getPackingType(Boolean status) {
		return new ResponseEntity<>(this.packingTypeRepository.findByStatus(status), HttpStatus.OK);
	}

}
