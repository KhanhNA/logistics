package com.tsolution._3services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.DistributorEntity;
import com.tsolution._2repositories.DistributorRepository;
import com.tsolution._3services.DistributorService;
import com.tsolution.excetions.BusinessException;

@Service
public class DistributorServiceImpl extends LogisticsBaseService implements DistributorService {

	@Autowired
	DistributorRepository distributorRepository;

	@Override
	public ResponseEntity<Object> findAll() throws BusinessException {
		return new ResponseEntity<>(this.distributorRepository
				.findAll(Boolean.TRUE.equals(this.userRepository.existsUserByUsername(this.getCurrentUsername()))
						? this.getCurrentUsername()
						: null),
				HttpStatus.OK);

	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<DistributorEntity> oDistributor = this.distributorRepository.findById(id);
		if (!oDistributor.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("distributor.id.not.exists", id));
		}
		return new ResponseEntity<>(oDistributor.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(List<DistributorEntity> entities) throws BusinessException {
		return new ResponseEntity<>(this.translator.toLocale("common.unknown"), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(Long id, DistributorEntity source) throws BusinessException {
		return new ResponseEntity<>(this.translator.toLocale("common.unknown"), HttpStatus.OK);
	}

}
