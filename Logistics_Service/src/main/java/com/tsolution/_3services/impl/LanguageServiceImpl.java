package com.tsolution._3services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._2repositories.ILanguageRepository;
import com.tsolution._3services.ILanguageService;

@Service
public class LanguageServiceImpl implements ILanguageService {

	@Autowired
	private ILanguageRepository languageRepository;

	@Override
	public ResponseEntity<Object> getAll() {
		return new ResponseEntity<>(this.languageRepository.findAll(), HttpStatus.OK);
	}

}
