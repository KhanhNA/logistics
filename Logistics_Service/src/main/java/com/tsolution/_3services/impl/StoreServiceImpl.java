package com.tsolution._3services.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.CountryEntity;
import com.tsolution._1entities.CountryLocationEntity;
import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._1entities.LanguageEntity;
import com.tsolution._1entities.dto.AvailableQuantityFilterDto;
import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.enums.ExpireDateColors;
import com.tsolution._1entities.enums.LocationType;
import com.tsolution._1entities.enums.PalletStep;
import com.tsolution._1entities.store.StoreDeliveryWaitEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.StoreFileEntity;
import com.tsolution._1entities.store.StoreLocationChargeEntity;
import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution._1entities.store.dto.StoreDto;
import com.tsolution._2repositories.CountryLocationRepository;
import com.tsolution._2repositories.CurrencyRepository;
import com.tsolution._2repositories.ExportStatementRepository;
import com.tsolution._2repositories.ILanguageRepository;
import com.tsolution._2repositories.ImportStatementRepository;
import com.tsolution._2repositories.PalletDetailRepository;
import com.tsolution._2repositories.PoRepository;
import com.tsolution._2repositories.RepackingPlanningRepository;
import com.tsolution._2repositories.importpo.ImportPoRepository;
import com.tsolution._2repositories.store.StoreDeliveryWaitRepository;
import com.tsolution._2repositories.store.StoreFileRepository;
import com.tsolution._2repositories.store.StoreLocationChargeRepository;
import com.tsolution._2repositories.store.StoreUserRepository;
import com.tsolution._3services.StoreService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;

@Service
public class StoreServiceImpl extends LogisticsBaseService implements StoreService {

	@Autowired
	private PalletDetailRepository palletDetailRepository;

	@Autowired
	private StoreUserRepository storeUserRepository;

	@Autowired
	private StoreLocationChargeRepository storeLocationChargeRepository;

	@Autowired
	private StoreFileRepository storeFileRepository;

	@Autowired
	private CurrencyRepository currencyRepository;

	@Autowired
	private ILanguageRepository languageRepository;

	@Autowired
	private CountryLocationRepository countryLocationRepository;

	@Autowired
	private StoreDeliveryWaitRepository storeDeliveryWaitRepository;

	@Autowired
	private PoRepository poRepository;
	@Autowired
	private ImportPoRepository importPoRepository;
	@Autowired
	private RepackingPlanningRepository repackingPlanningRepository;
	@Autowired
	private ExportStatementRepository exportStatementRepository;
	@Autowired
	private ImportStatementRepository importStatementRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.storeRepository.findAll(), HttpStatus.OK);
	}

	private StoreEntity getById(Long id) throws BusinessException {
		Optional<StoreEntity> oStore = this.storeRepository.findById(id == null ? 0 : id);
		if (!oStore.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("store.id.not.exists", id));
		}
		return oStore.get();
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		return new ResponseEntity<>(this.getById(id), HttpStatus.OK);
	}

	private void validStore(StoreEntity store) throws BusinessException {
		StoreEntity tempStore = this.storeRepository.findOneByCode(store.getCode());
		if ((tempStore != null) && ((store.getId() == null) || !tempStore.getId().equals(store.getId()))) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("store.code.is.exists", store.getCode()));
		}
		if (StringUtils.isNullOrEmpty(store.getName())) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.name"));
		}
		if ((store.getCurrency() == null) || (store.getCurrency().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.currency"));
		}
		Optional<CurrencyEntity> oCurrency = this.currencyRepository.findById(store.getCurrency().getId());
		if (!oCurrency.isPresent()) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.currency"));
		}
		if ((store.getLanguage() == null) || (store.getLanguage().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.language"));
		}
		Optional<LanguageEntity> oLanguage = this.languageRepository.findById(store.getLanguage().getId());
		if (!oLanguage.isPresent()) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.language"));
		}
		if (store.getLat() == null) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.latitude"));
		}
		if (store.getLng() == null) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.longitude"));
		}
		this.validStoreLocation(store);
		this.validStoreDC(store);
		this.validStoreArea(store);
		this.validStoreUser(store);
	}

	private void validStoreDC(StoreEntity store) throws BusinessException {
		if (store.isDc()) {
			if (((store.getProvideStore() == null) || (store.getProvideStore().getId() == null))) {
				throw new BusinessException(this.translator.toLocale("store.input.missing.provide.store"));
			}
			if ((store.getDeliveryFrequency() == null) || (store.getDeliveryFrequency() <= 0)) {
				throw new BusinessException(this.translator.toLocale("store.input.missing.delivery.frequency"));
			}
			Optional<StoreEntity> oStore = this.storeRepository.findById(store.getProvideStore().getId());
			if (!oStore.isPresent()) {
				throw new BusinessException(this.translator.toLocale("store.input.missing.provide.store"));
			}
		}
	}

	private void validStoreLocation(StoreEntity store) throws BusinessException {
		if ((store.getCountry() == null) || (store.getCountry().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.country"));
		}
		if ((store.getRegion() == null) || (store.getRegion().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.region"));
		}
		if ((store.getDistrict() == null) || (store.getDistrict().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.district"));
		}
		if ((store.getTownship() == null) || (store.getTownship().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.township"));
		}
		Optional<CountryEntity> oCountry = this.countryRepository.findById(store.getCountry().getId());
		if (!oCountry.isPresent()) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.country"));
		}
		Optional<CountryLocationEntity> oTownship = this.countryLocationRepository
				.findById(store.getTownship().getId());
		if (!oTownship.isPresent()) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.township"));
		}
		CountryLocationEntity township = oTownship.get();
		store.setTownship(township);
		CountryLocationEntity district = township.getParent();
		if ((district == null) || (district.getId() == null) || !district.getId().equals(store.getDistrict().getId())) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.district"));
		}
		store.setDistrict(district);
		CountryLocationEntity region = district.getParent();
		if ((region == null) || (region.getId() == null) || !region.getId().equals(store.getRegion().getId())) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.region"));
		}
		store.setRegion(region);
	}

	private void validStoreUser(StoreEntity store) throws BusinessException {
		if ((store.getUser() != null) && !StringUtils.isNullOrEmpty(store.getUser().getUsername())) {
			if (Boolean.FALSE.equals(this.userRepository.existsUserByUsername(store.getUser().getUsername()))) {
				throw new BusinessException(this.translator.toLocaleByFormatString("user.username.not.exists",
						store.getUser().getUsername()));
			}
			if ((store.getId() != null) && Boolean.FALSE.equals(this.storeUserRepository
					.existsStoreUserByStoreIdAndUsername(store.getId(), store.getUser().getUsername()))) {
				throw new BusinessException(this.translator.toLocale("store.input.store.user.not.exists"));
			}
		}
	}

	private void validStoreArea(StoreEntity store) throws BusinessException {
		if (store.getLength() == null) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.length"));
		}
		if (store.getWidth() == null) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.width"));
		}
		if (store.getHeight() == null) {
			throw new BusinessException(this.translator.toLocale("store.input.missing.height"));
		}
		store.setArea(store.getLength().multiply(store.getWidth()));
	}

	private List<StoreLocationChargeEntity> updateLocationCharge(StoreEntity store,
			List<StoreLocationChargeEntity> storeLocationCharges, boolean isEdit) throws BusinessException {
		List<StoreLocationChargeEntity> results = new ArrayList<>();
		if (!store.isDc() || (storeLocationCharges == null) || storeLocationCharges.isEmpty()) {
			this.storeLocationChargeRepository.deleteAll(store.getStoreLocationCharges());
			return results;
		}
		if (Boolean.TRUE.equals(isEdit)) {
			// Xóa cái đám không có trong danh sách mới đẩy lên
			List<StoreLocationChargeEntity> listDelete = store.getStoreLocationCharges().stream()
					.filter(old -> storeLocationCharges.stream()
							.noneMatch(newz -> (old.getId() != null)
									&& old.getStore().getId().equals(newz.getStore().getId())
									&& old.getCountryLocation().getId().equals(newz.getCountryLocation().getId())))
					.collect(Collectors.toList());
			if (store.getStoreLocationCharges().removeAll(listDelete)) {
				this.storeLocationChargeRepository.deleteAll(listDelete);
			}
		} else {
			storeLocationCharges.forEach(x -> x.setStore(store));
		}

		for (StoreLocationChargeEntity storeLocationChargeEntity : storeLocationCharges) {
			Optional<StoreLocationChargeEntity> oStoreLocationCharge = store
					.getStoreLocationCharges().stream().filter(
							old -> (old.getId() != null)
									&& old.getStore().getId().equals(storeLocationChargeEntity.getStore().getId())
									&& old.getCountryLocation().getId()
											.equals(storeLocationChargeEntity.getCountryLocation().getId()))
					.findFirst();
			if (oStoreLocationCharge.isPresent()) {
				results.add(oStoreLocationCharge.get());
				continue;
			}
			Optional<CountryLocationEntity> oCountryLocation = this.countryLocationRepository
					.findById(storeLocationChargeEntity.getCountryLocation().getId());
			if (!oCountryLocation.isPresent()) {
				throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
			}
			CountryLocationEntity countryLocation = oCountryLocation.get();
			if (!LocationType.TOWNSHIP.equals(countryLocation.getLocationType())) {
				throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
			}
			this.storeRepository.getStoreByLocationCharge(Collections.singletonList(countryLocation.getCode()));
			storeLocationChargeEntity.setStore(store);
			results.add(this.storeLocationChargeRepository.save(storeLocationChargeEntity));
		}
		return results;
	}

	private List<StoreFileEntity> updateFile(StoreEntity store, MultipartFile[] files, List<Long> removeFileIds)
			throws BusinessException {
		List<StoreFileEntity> keepFiles = store.getStoreFiles();
		if ((removeFileIds != null) && !removeFileIds.isEmpty()) {
			List<StoreFileEntity> removeFiles = keepFiles.stream()
					.filter(kpiFile -> removeFileIds.contains(kpiFile.getId())).collect(Collectors.toList());
			if ((removeFiles != null) && !removeFiles.isEmpty()) {
				List<String> fileUrls = removeFiles.stream().map(StoreFileEntity::getUrl).collect(Collectors.toList());
				if (Boolean.TRUE.equals(this.fileStorageService.deleteFile(fileUrls))) {
					keepFiles = keepFiles.stream().filter(kpiFile -> !removeFileIds.contains(kpiFile.getId()))
							.collect(Collectors.toList());
					this.storeFileRepository.deleteAll(removeFiles);
				} else {
					throw new BusinessException("DELETE FILE FAILED!!!");
				}
			}
		}

		if ((files == null) || (files.length == 0)) {
			return keepFiles;
		}
		LocalDateTime sysdate = this.getSysdate();

		for (int i = 0; i < files.length; i++) {
			MultipartFile multipartFile = files[i];
			String fileName = this.fileStorageService.saveFile(
					String.format("%04d/%02d/store-files", sysdate.getYear(), sysdate.getMonthValue()), multipartFile);
			StoreFileEntity file = new StoreFileEntity();
			file.setStore(store);
			file.setUrl(fileName);
			this.storeFileRepository.save(file);
			keepFiles.add(file);
		}
		return keepFiles;
	}

	private StoreEntity createOne(StoreEntity store, MultipartFile[] files) throws BusinessException {
		this.validStore(store);
		store.setCode(String.format("%s_%s%05d", store.isDc() ? "DC" : "FC",
				store.getRegion().getName().substring(0, 2).toUpperCase(),
				Long.valueOf(this.getGeneratorValue(store.isDc() ? "STORE_CODE_DC" : "STORE_CODE_FC"))));
		store = this.storeRepository.save(store);
		store.setStoreLocationCharges(this.updateLocationCharge(store, store.getStoreLocationCharges(), false));
		store.setStoreFiles(this.updateFile(store, files, new ArrayList<>()));
		return store;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(StoreEntity store, MultipartFile[] files) throws BusinessException {
		return new ResponseEntity<>(this.createOne(store, files), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<StoreEntity> entities) throws BusinessException {
		List<StoreEntity> stores = new ArrayList<>();
		for (StoreEntity store : entities) {
			store = this.createOne(store, null);
			stores.add(store);
		}
		return new ResponseEntity<>(stores, HttpStatus.OK);
	}

	private StoreEntity updateOne(Long id, StoreEntity source, MultipartFile[] files, List<Long> removeFileIds)
			throws BusinessException {
		StoreEntity store = this.getById(id);
		if (!id.equals(source.getId())) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		this.validStore(source);
		store.setStoreFiles(this.updateFile(store, files, removeFileIds));
		store.setStoreLocationCharges(this.updateLocationCharge(store, source.getStoreLocationCharges(), true));
		if (Boolean.FALSE.equals(source.getStatus()) && Boolean.TRUE.equals(store.getStatus())) {
			source.setOutBusinessSince(this.getSysdate());
		}
		source.setStatus(Boolean.FALSE);
		source = this.storeRepository.save(source);
		return source;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, StoreEntity source) throws BusinessException {
		return new ResponseEntity<>(this.updateOne(id, source, null, null), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, StoreEntity source, MultipartFile[] files,
			List<Long> removeKpiFileIds) throws BusinessException {
		return new ResponseEntity<>(this.updateOne(id, source, files, removeKpiFileIds), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> activate(Long id) throws BusinessException {
		this.getById(id);
		StoreDto storeDto = new StoreDto();
		storeDto.setStatus(true);
		storeDto.setExceptIds(Collections.singletonList(id));
		Page<StoreEntity> pStore = this.storeRepository.find(storeDto, PageRequest.of(0, 9999));
		List<StoreDeliveryWaitEntity> storeDeliveryWaits = this.storeDeliveryWaitRepository.getAllByFromStore(id);
		if (storeDeliveryWaits == null) {
			storeDeliveryWaits = new ArrayList<>();
		}
		if ((pStore != null) && !pStore.getContent().isEmpty()) {
			List<Long> storeIds = storeDeliveryWaits.stream().map(sdw -> sdw.getToStore().getId())
					.collect(Collectors.toList());
			List<StoreEntity> stores = pStore.getContent().stream().filter(s -> !storeIds.contains(s.getId()))
					.collect(Collectors.toList());
			if ((stores != null) && !stores.isEmpty()) {
				throw new BusinessException(this.translator.toLocale("store.input.missing.delivery.wait"));
			}
			this.storeRepository.activate(id);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> deactivate(Long id) throws BusinessException {
		this.getById(id);
		Long poHang = this.poRepository.countPoHang(id);
		Long importPoHang = this.importPoRepository.countImportPoHang(id);
		Long repackingPlanningHang = this.repackingPlanningRepository.countRepackingPlanningHang(id);
		Long exportStatementHang = this.exportStatementRepository.countExportStatementHang(id);
		Long importStatementHang = this.importStatementRepository.countImportStatementHang(id);
		Long merchantOrderHang = this.merchantOrderRepository.countMerchantOrderHang(id);
		Long storeProductPackingHang = this.storeProductPackingRepository.countStoreProductPackingHang(id);
		if (((poHang != null) && (poHang > 0)) || ((importPoHang != null) && (importPoHang > 0))
				|| ((repackingPlanningHang != null) && (repackingPlanningHang > 0))
				|| ((exportStatementHang != null) && (exportStatementHang > 0))
				|| ((importStatementHang != null) && (importStatementHang > 0))
				|| ((merchantOrderHang != null) && (merchantOrderHang > 0))
				|| ((storeProductPackingHang != null) && (storeProductPackingHang > 0))) {
			throw new BusinessException(this.translator.toLocaleByFormatString("store.deactivate.nofication", poHang,
					importPoHang, repackingPlanningHang, exportStatementHang, importStatementHang, merchantOrderHang,
					storeProductPackingHang));
		}
		this.storeRepository.deactivate(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(StoreDto storeDto, Boolean ignoreCheckPermission, Integer pageNumber,
			Integer pageSize) throws IOException, BusinessException {
		String currentUser;
		if (this.getCurrentOAuth2Details().getAuthorities().stream()
				.anyMatch(authority -> "post/stores".equalsIgnoreCase(authority.getAuthority())
						|| "patch/stores/{id}".equalsIgnoreCase(authority.getAuthority()))) {
			currentUser = null;
		} else {
			currentUser = this.getCurrentOAuth2Details().getName();
		}
		storeDto.setCurrentUser(Boolean.TRUE.equals(ignoreCheckPermission) ? null : currentUser);
		return new ResponseEntity<>(this.storeRepository.find(storeDto, PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getStoresByProvideStore(List<Long> storeIds) {
		return new ResponseEntity<>(this.storeRepository.getStoresByProvideStore(storeIds), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> nearHere(Float currentLat, Float currentLng, Integer maxDistance,
			String productPackingCode, Boolean isDC, Integer pageNumber, Integer pageSize) throws BusinessException {
		try {
			if (StringUtils.isNullOrEmpty(productPackingCode)) {
				return new ResponseEntity<>(this.storeRepository.nearHere(currentLat, currentLng, maxDistance, isDC,
						PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(this.storeProductPackingRepository.nearHere(currentLat, currentLng,
						maxDistance, productPackingCode, isDC, PageRequest.of(pageNumber - 1, pageSize)),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<Object> inventory(InventoryDto inventoryDto, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		Page<StoreProductPackingEntity> page = null;
		if (Boolean.TRUE.equals(this.storeUserRepository.existsStoreUserByStoreIdAndUsername(inventoryDto.getStoreId(),
				this.getCurrentUsername()))) {
			page = this.storeProductPackingRepository.inventory(inventoryDto, this.getCurrentUsername(),
					PageRequest.of(pageNumber - 1, pageSize));
		} else {
			throw new BusinessException(this.translator.toLocale("common.unknown"));
		}
		LocalDateTime sysdate = this.getSysdate();
		page.forEach(storeProductPacking -> storeProductPacking.setClassName(ExpireDateColors.findClassName(sysdate,
				storeProductPacking.getExpireDate(), storeProductPacking.getProductPacking())));
		return new ResponseEntity<>(page, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> inventoryIgnoreExpireDate(InventoryDto inventoryDto, Long exportStatementId,
			Integer pageNumber, Integer pageSize) throws BusinessException {
		return new ResponseEntity<>(this.storeProductPackingRepository.inventoryIgnoreExpireDate(inventoryDto,
				exportStatementId, this.getCurrentUsername(), PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> inventoryPalletDetail(Long storeId, Long productPackingId, java.sql.Date expireDate)
			throws BusinessException {
		return new ResponseEntity<>(this.palletDetailRepository.findGroupByProductPackingAndExpireDate(storeId,
				productPackingId, expireDate, PalletStep.IMPORT_STATEMENT_UPDATE_PALLET.getValue()), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getStoreUsers(Long id) throws BusinessException {
		this.getById(id);
		return new ResponseEntity<>(this.userRepository.findByStore(id), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getAvailableQuantity(String townshipCode, List<String> productPackingCodes)
			throws BusinessException {
		List<StoreEntity> stores = this.storeRepository
				.getStoreByLocationCharge(Collections.singletonList(townshipCode));
		if (stores.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		List<String> storeCodes = new ArrayList<>();
		for (StoreEntity store : stores) {
			storeCodes.add(store.getCode());
			StoreEntity provideStore = store.getProvideStore();
			if (provideStore != null) {
				storeCodes.add(provideStore.getCode());
			}
		}
		return new ResponseEntity<>(this.storeRepository.getAvailableQuantity(storeCodes, productPackingCodes),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getStoreByLocationCharge(String townshipCode) throws BusinessException {
		List<StoreEntity> stores = this.storeRepository
				.getStoreByLocationCharge(Collections.singletonList(townshipCode));
		if (stores.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(stores.get(0), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getInventory(AvailableQuantityFilterDto availableQuantityFilterDto)
			throws BusinessException {
		List<StoreEntity> stores = this.storeRepository
				.getStoreByLocationCharge(availableQuantityFilterDto.getTownshipCodes());
		if (stores.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		List<String> storeCodes = new ArrayList<>();
		for (StoreEntity store : stores) {
			storeCodes.add(store.getCode());
			StoreEntity provideStore = store.getProvideStore();
			if (provideStore != null) {
				storeCodes.add(provideStore.getCode());
			}
		}
		return new ResponseEntity<>(this.storeRepository.getAvailableQuantity(storeCodes,
				availableQuantityFilterDto.getProductPackingCodes()), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findTownship(List<String> townshipCodes, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		return new ResponseEntity<>(
				this.countryLocationRepository.findTownship(townshipCodes, PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}

}
