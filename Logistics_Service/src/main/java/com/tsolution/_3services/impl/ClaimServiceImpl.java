package com.tsolution._3services.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.ImportStatementEntity;
import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.RepackingPlanningEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.claim.Claim;
import com.tsolution._1entities.claim.ClaimDetail;
import com.tsolution._1entities.claim.ClaimImage;
import com.tsolution._1entities.claim.enums.ClaimStatus;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.importpo.ImportPoEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.StoreProductPackingDetailEntity;
import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution._2repositories.ClaimDetailRepository;
import com.tsolution._2repositories.ClaimImageRepository;
import com.tsolution._2repositories.ClaimRepository;
import com.tsolution._2repositories.ExportStatementRepository;
import com.tsolution._2repositories.ImportStatementRepository;
import com.tsolution._2repositories.PalletDetailRepository;
import com.tsolution._2repositories.PalletRepository;
import com.tsolution._2repositories.PoRepository;
import com.tsolution._2repositories.RepackingPlanningRepository;
import com.tsolution._2repositories.importpo.ImportPoRepository;
import com.tsolution._3services.ClaimService;
import com.tsolution._3services.PalletService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.DatetimeUtils;
import com.tsolution.utils.ExcelTo;
import com.tsolution.utils.Jxls2xTransformerUtils;
import com.tsolution.utils.Jxls2xTransformerUtils.DrawBase64Dto;
import com.tsolution.utils.StringUtils;
import com.tsolution.utils.Utils;

@Service
public class ClaimServiceImpl extends LogisticsBaseService implements ClaimService {
	private static final String CLAIM_IMAGE_ID_NOT_EXISTS = "claim.image.id.not.exists";
	private static final Logger log = LogManager.getLogger(ClaimServiceImpl.class);
	private static final String STORE_NOT_ENOUGH_PRODUCT_FOR_DONE_CLAIM = "store.not.enough.product.for.done.claim";
	private static final String CLAIM_ID_NOT_EXISTS = "claim.id.not.exists";
	private static final String CLAIM_INPUT_STATUS_CAN_NOT_ACTION = "claim.input.status.can.not.action";
	@Autowired
	private ClaimRepository claimRepository;
	@Autowired
	private ClaimDetailRepository claimDetailRepository;
	@Autowired
	private ClaimImageRepository claimImageRepository;

	@Autowired
	private PoRepository poRepository;
	@Autowired
	private ImportPoRepository importPoRepository;
	@Autowired
	private RepackingPlanningRepository repackingPlanningRepository;
	@Autowired
	private ImportStatementRepository importStatementRepository;
	@Autowired
	private ExportStatementRepository exportStatementRepository;
	@Autowired
	private PalletService palletService;
	@Autowired
	private PalletRepository palletRepository;
	@Autowired
	private PalletDetailRepository palletDetailRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Override
	public ResponseEntity<Object> find(Claim claim, Integer pageNumber, Integer pageSize) throws BusinessException {
		List<Long> managedStoreIncludeProvideStores = this.getManagedStoreIncludeProvideStores();
		return new ResponseEntity<>(this.claimRepository.find(claim, managedStoreIncludeProvideStores,
				PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findAll() throws BusinessException {
		return new ResponseEntity<>(
				new BusinessException(this.translator.toLocale(BusinessException.COMMON_NOT_SUPPORT)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<Claim> oClaim = this.claimRepository.findById(id);
		if (!oClaim.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(ClaimServiceImpl.CLAIM_ID_NOT_EXISTS, id));
		}
		Claim claim = oClaim.get();
		if (!this.getManagedStoreIncludeProvideStores().contains(claim.getStore().getId())
				|| !this.validTypePermission(claim.getType())) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_ACCESS_DENIED));
		}
		return new ResponseEntity<>(oClaim.get(), HttpStatus.OK);
	}

	private boolean validTypePermission(ClaimType claimType) {
		return claimType != null;
//		return this.hasAuthority("patch/claims/accept/{id}", "patch/claims/reject/{id}")
//				|| (ClaimType.IMPORT_PO.getValue().equalsIgnoreCase(claimType.getValue())
//						&& this.hasAuthority("post/import-pos"))
//				|| (ClaimType.IMPORT_PO_UPDATE_PALLET.getValue().equalsIgnoreCase(claimType.getValue())
//						&& this.hasAuthority("post/import-pos/update-pallet/{id}"))
//				|| (ClaimType.REPACKING_PLANNING.getValue().equalsIgnoreCase(claimType.getValue())
//						&& this.hasAuthority("post/repacking-plannings"))
//				|| (ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING.getValue().equalsIgnoreCase(claimType.getValue())
//						&& this.hasAuthority("post/import-statements/from-repacking-planning"))
//				|| (ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT.getValue().equalsIgnoreCase(claimType.getValue())
//						&& this.hasAuthority("patch/import-statements/{id}/import"))
//				|| (ClaimType.EXPORT_STATEMENT.getValue().equalsIgnoreCase(claimType.getValue())
//						&& this.hasAuthority("patch/export-statements/{id}/export"))
//				|| (ClaimType.DELIVERY.getValue().equalsIgnoreCase(claimType.getValue())
//						&& this.hasAuthority("post/import-statements/from-export-statement"))
//				|| ClaimType.OUT_OF_DATE_PALLET_STEP_1.getValue().equalsIgnoreCase(claimType.getValue())
//				|| ClaimType.OUT_OF_DATE_PALLET_STEP_2.getValue().equalsIgnoreCase(claimType.getValue())
//				|| ClaimType.OTHER.getValue().equalsIgnoreCase(claimType.getValue())
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<Claim> entities) throws BusinessException {
		List<Claim> claims = new ArrayList<>();
		for (Claim source : entities) {
			if (source.getId() != null) {
				throw new BusinessException(this.translator.toLocale("claim.input.redundant.id"));
			}
			this.validClaim(source);
			if ((source.getStatus() == null) || !ClaimStatus.WAIT_APPROVE.getValue().equals(source.getStatus())) {
				throw new BusinessException(
						this.translator.toLocale(ClaimServiceImpl.CLAIM_INPUT_STATUS_CAN_NOT_ACTION));
			}
			LocalDateTime sysdate = this.getSysdate();
			if (StringUtils.isNullOrEmpty(source.getCode())) {
				source.setCode(String.format("%s-claim-%s", sysdate.format(DateTimeFormatter.ofPattern("yyyyMMdd")),
						sysdate.format(DateTimeFormatter.ofPattern("HHmmssSSS"))));
			}
			Claim claim = this.claimRepository.save(source);
			List<ClaimDetail> claimDetails = new ArrayList<>();
			for (ClaimDetail claimDetail : source.getClaimDetails()) {
				claimDetails.addAll(this.createClaimDetail(claim, claimDetail));
			}
			claim.setClaimDetails(claimDetails);
			claims.add(claim);
		}
		return new ResponseEntity<>(claims, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, Claim source) throws BusinessException {
		if ((id == null) || (source.getId() == null) || !id.equals(source.getId())) {
			throw new BusinessException(this.translator.toLocale("claim.input.missing.id"));
		}
		Optional<Claim> oClaim = this.claimRepository.findById(id);
		if (!oClaim.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(ClaimServiceImpl.CLAIM_ID_NOT_EXISTS, id));
		}
		Claim claim = oClaim.get();
		if (ClaimStatus.APPROVED.getValue().equals(claim.getStatus())) {
			throw new BusinessException(this.translator.toLocale("claim.input.status.approved"));
		}
		this.validClaim(source);
		claim.setStore(source.getStore());
		claim.setAmenable(source.getAmenable());
		claim.setDescription(source.getDescription());
		List<ClaimDetail> claimDetails = claim.getClaimDetails();
		if (ClaimStatus.WAIT_APPROVE.getValue().equals(claim.getStatus())) {
			// Trả hết hàng về và xóa chi tiết đi rồi tạo lại
			this.returnClaimDetailToPallet(claimDetails);
		}
		claim.setStatus(ClaimStatus.WAIT_APPROVE.getValue());
		claim = this.claimRepository.save(claim);

		this.claimDetailRepository.deleteAll(claimDetails);
		for (ClaimDetail claimDetail : source.getClaimDetails()) {
			claimDetails.addAll(this.createClaimDetail(claim, claimDetail));
		}
		claim.setClaimDetails(claimDetails);
		return new ResponseEntity<>(claim, HttpStatus.OK);
	}

	private void validClaim(Claim claim) throws BusinessException {
		if ((claim.getStore() == null) || (claim.getStore().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("claim.input.missing.store"));
		}
		if ((claim.getType() == null) || StringUtils.isNullOrEmpty(claim.getType().getValue())) {
			throw new BusinessException(this.translator.toLocale("claim.input.missing.type"));
		}
		if (!ClaimType.contains(claim.getType().getValue())) {
			throw new BusinessException(this.translator.toLocale("claim.input.type.invalid"));
		}
		Optional<StoreEntity> oStore = this.storeRepository.findById(claim.getStore().getId());
		if (!oStore.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("store.id.not.exists", claim.getStore().getId()));
		}
		if (!this.getManagedStoreIncludeProvideStores().contains(claim.getStore().getId())
				|| !this.validTypePermission(claim.getType())) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_ACCESS_DENIED));
		}
		this.validClaimDetails(claim);
		if ((claim.getId() != null) || (claim.getReferenceId() == null)) {
			return;
		}
		Optional<Claim> oClaim = this.claimRepository.getClaimByTypeAndReferenceId(claim.getType(),
				claim.getReferenceId());
		if (oClaim.isPresent()) {
			Claim temp = oClaim.get();
			if (ClaimStatus.REJECTED.getValue().equals(temp.getStatus())) {
				claim.setId(temp.getId());
				claim.setCode(temp.getCode());
				this.claimDetailRepository.deleteAll(temp.getClaimDetails());
			} else {
				throw new BusinessException(
						this.translator.toLocaleByFormatString("claim.exists.with.code", oClaim.get().getCode()));
			}
		}
	}

	private void validClaimDetails(Claim claim) throws BusinessException {
		if ((claim.getClaimDetails() == null) || claim.getClaimDetails().isEmpty()) {
			throw new BusinessException(this.translator.toLocale("claim.input.missing.details"));
		}
		for (ClaimDetail claimDetail : claim.getClaimDetails()) {
			this.validClaimDetail(claimDetail, claim.getType());
		}
		SuperEntity superEntity = this.getReferenceEntity(claim.getType(), claim.getReferenceId());
		String messageValid = claim.getType().validReferenceWithClaimDetails(superEntity,
				this.getManagedStoreIncludeProvideStores(), claim.getClaimDetails());
		if (!StringUtils.isNullOrEmpty(messageValid)) {
			throw new BusinessException(this.translator.toLocale(messageValid));
		}
	}

	private void validClaimDetail(ClaimDetail claimDetail, ClaimType claimType) throws BusinessException {
		if (claimDetail.getId() != null) {
			throw new BusinessException(this.translator.toLocale("claim.detail.input.invalid.id"));
		}
		if ((claimDetail.getProductPacking() == null) || (claimDetail.getProductPacking().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("claim.detail.input.missing.product.packing"));
		}
		if (claimDetail.getExpireDate() == null) {
			throw new BusinessException(this.translator.toLocale("claim.detail.input.missing.expire.date"));
		}
		if (claimDetail.getQuantity() == null) {
			throw new BusinessException(this.translator.toLocale("claim.detail.input.missing.quantity"));
		}
		if (claimDetail.getQuantity() <= 0) {
			throw new BusinessException(this.translator.toLocale("claim.detail.input.quantity.range"));
		}
		Optional<ProductPackingEntity> oProductPacking = this.productPackingRepository
				.findById(claimDetail.getProductPacking().getId());
		if (!oProductPacking.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.id.not.exists",
					claimDetail.getProductPacking().getId()));
		}
		claimDetail.setProductPacking(oProductPacking.get());
		if ((claimDetail.getPalletId() == null) && ClaimType.OUT_OF_DATE_PALLET_STEP_1.equals(claimType)) {
			throw new BusinessException(this.translator.toLocale("claim.detail.input.missing.pallet"));
		}
		if (claimDetail.getPalletId() != null) {
			Optional<PalletEntity> oPallet = this.palletRepository.findById(claimDetail.getPalletId());
			if (!oPallet.isPresent()) {
				throw new BusinessException(
						this.translator.toLocaleByFormatString("pallet.id.not.exists", claimDetail.getPalletId()));
			}
		}
	}

	private List<ClaimDetail> createClaimDetail(Claim claim, ClaimDetail claimDetail) throws BusinessException {
		claimDetail.setClaim(claim);
		Optional<ProductPackingPriceEntity> oProductPackingPrice = this.claimRepository
				.getProductPackingPrice(claim.getType().getQueryForProductPackingPrice(claim.getReferenceId(),
						claimDetail.getProductPacking().getId()));
		if (!oProductPackingPrice.isPresent()) {
			throw new BusinessException(this.translator.toLocale("claim.detail.input.missing.product.packing.price"));
		}
		claimDetail.setProductPackingPrice(oProductPackingPrice.get());
		List<ClaimDetail> claimDetailPallets = this.createClaimDetailWithStoreProductPacking(claim.getStore().getId(),
				claim.getType(), claimDetail);
		claimDetailPallets = this.claimDetailRepository.saveAll(claimDetailPallets);
		return claimDetailPallets;
	}

	private List<ClaimDetail> createClaimDetailWithStoreProductPacking(Long storeId, ClaimType claimType,
			ClaimDetail claimDetail) throws BusinessException {
		switch (claimType) {
		case EXPORT_STATEMENT:
		case OUT_OF_DATE_PALLET_STEP_2:
		case OTHER:
			ProductPackingEntity productPacking = claimDetail.getProductPacking();
			// Nếu là lấy bù hàng cho thực xuất thì phải chọn SP còn hạn
			// Nếu là claim hết hạn hay hàng hỏng hủy thì phải lấy theo expire_date đầu vào
			List<StoreProductPackingEntity> storeProductPackings = null;
			if (ClaimType.EXPORT_STATEMENT.equals(claimType)) {
				storeProductPackings = this.storeProductPackingRepository.getAvaiableForExport(storeId,
						Collections.singletonList(productPacking.getId()));
			} else {
				Optional<StoreProductPackingEntity> oStoreProductPacking = this.storeProductPackingRepository
						.findByStoreIdAndProductPackingIdAndExpireDate(storeId, productPacking.getId(),
								claimDetail.getExpireDate());
				if (!oStoreProductPacking.isPresent()) {
					throw new BusinessException(this.translator.toLocaleByFormatString(
							ClaimServiceImpl.STORE_NOT_ENOUGH_PRODUCT_FOR_DONE_CLAIM, productPacking.getCode()));
				}
				storeProductPackings = Collections.singletonList(oStoreProductPacking.get());
			}
			if ((storeProductPackings == null) || storeProductPackings.isEmpty() || (storeProductPackings.stream()
					.mapToLong(StoreProductPackingEntity::getTotalQuantity).sum() < claimDetail.getQuantity())) {
				throw new BusinessException(this.translator.toLocaleByFormatString(
						ClaimServiceImpl.STORE_NOT_ENOUGH_PRODUCT_FOR_DONE_CLAIM, productPacking.getCode()));
			}
			// - Nếu hàng có trong kho thì ưu tiên theo expire_date gần nhất
			storeProductPackings.sort(Comparator.comparing(StoreProductPackingEntity::getExpireDate));
			return this.createClaimDetailWithStoreProductPackingDetail(claimDetail, storeProductPackings);
		case OUT_OF_DATE_PALLET_STEP_1:
			return this.createClaimDetailWithPalletDetail(claimDetail);
		default:
			return Collections.singletonList(claimDetail);
		}
	}

	private List<ClaimDetail> createClaimDetailWithStoreProductPackingDetail(ClaimDetail claimDetail,
			List<StoreProductPackingEntity> storeProductPackings) throws BusinessException {
		List<ClaimDetail> claimDetails = new ArrayList<>();
		Long tempQuantity = claimDetail.getQuantity();
		for (StoreProductPackingEntity storeProductPacking : storeProductPackings) {
			if (tempQuantity <= 0) {
				break;
			}
			List<StoreProductPackingDetailEntity> storeProductPackingDetails = this.storeProductPackingDetailRepository
					.getDetailByStoreProductPackingId(storeProductPacking.getId());
			for (StoreProductPackingDetailEntity storeProductPackingDetail : storeProductPackingDetails) {
				if (tempQuantity <= 0) {
					break;
				}
				ClaimDetail cd = new ClaimDetail();
				cd.setClaim(claimDetail.getClaim());
				cd.setProductPacking(claimDetail.getProductPacking());
				cd.setProductPackingPrice(claimDetail.getProductPackingPrice());
				cd.setExpireDate(storeProductPacking.getExpireDate());
				cd.setStoreProductPackingDetail(storeProductPackingDetail);
				long minusQuantity;
				if (tempQuantity <= storeProductPackingDetail.getQuantity()) {
					cd.setQuantity(tempQuantity);
					minusQuantity = tempQuantity;
					storeProductPacking.setTotalQuantity(storeProductPacking.getTotalQuantity() - minusQuantity);
					storeProductPackingDetail.setQuantity(storeProductPackingDetail.getQuantity() - minusQuantity);
					tempQuantity = 0L;
				} else {
					cd.setQuantity(storeProductPackingDetail.getQuantity());
					minusQuantity = storeProductPackingDetail.getQuantity();
					storeProductPacking.setTotalQuantity(storeProductPacking.getTotalQuantity() - minusQuantity);
					tempQuantity = tempQuantity - minusQuantity;
					storeProductPackingDetail.setQuantity(0L);
				}
				this.storeProductPackingDetailRepository.save(storeProductPackingDetail);
				Optional<PalletDetailEntity> oPalletDetail = this.palletDetailRepository
						.findByStoreProductPackingDetailId(storeProductPackingDetail.getId());
				if (oPalletDetail.isPresent()) {
					PalletDetailEntity palletDetail = oPalletDetail.get();
					this.palletService.pullDownPalletDetail(palletDetail.getId(), minusQuantity);
					cd.setPalletDetail(palletDetail);
				}
				claimDetails.add(cd);
			}
			this.storeProductPackingRepository.save(storeProductPacking);
		}
		return claimDetails;
	}

	private List<ClaimDetail> createClaimDetailWithPalletDetail(ClaimDetail claimDetail) throws BusinessException {
		List<PalletDetailEntity> palletDetails = this.palletDetailRepository.findByPalletAndProductPackingAndExpireDate(
				claimDetail.getPalletId(), claimDetail.getProductPacking().getId(),
				DatetimeUtils.localDateTime2SqlDate(claimDetail.getExpireDate()));
		if (palletDetails.stream().mapToLong(PalletDetailEntity::getQuantity).sum() < claimDetail.getQuantity()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(ClaimServiceImpl.STORE_NOT_ENOUGH_PRODUCT_FOR_DONE_CLAIM,
							claimDetail.getProductPacking().getCode()));
		}
		List<ClaimDetail> claimDetails = new ArrayList<>();
		Long tempQuantity = claimDetail.getQuantity();
		for (PalletDetailEntity palletDetail : palletDetails) {
			if (tempQuantity <= 0) {
				break;
			}
			ClaimDetail cd = new ClaimDetail();
			cd.setClaim(claimDetail.getClaim());
			cd.setProductPacking(claimDetail.getProductPacking());
			cd.setProductPackingPrice(claimDetail.getProductPackingPrice());
			cd.setExpireDate(claimDetail.getExpireDate());
			cd.setPalletDetail(palletDetail);
			if (tempQuantity <= palletDetail.getQuantity()) {
				cd.setQuantity(tempQuantity);
				this.palletService.pullDownPalletDetail(palletDetail.getId(), tempQuantity);
				tempQuantity = 0L;
			} else {
				cd.setQuantity(palletDetail.getQuantity());
				this.palletService.pullDownPalletDetail(palletDetail.getId(), palletDetail.getQuantity());
				tempQuantity -= palletDetail.getQuantity();
			}
			claimDetails.add(cd);
		}
		return claimDetails;
	}

	private SuperEntity getReferenceEntity(ClaimType claimType, Long referenceId) throws BusinessException {
		switch (claimType) {
		case IMPORT_PO:
			Optional<PoEntity> oPo = this.getReferencePo(referenceId);
			return oPo.get();
		case IMPORT_PO_UPDATE_PALLET:
			Optional<ImportPoEntity> oImportPo = this.getReferenceImportPo(referenceId);
			return oImportPo.get();
		case REPACKING_PLANNING:
		case IMPORT_STATEMENT_REPACKING_PLANNING:
			Optional<RepackingPlanningEntity> oRepackingPlanning = this.getReferenceRepackingPlanning(referenceId);
			return oRepackingPlanning.get();
		case IMPORT_STATEMENT_EXPORT_STATEMENT:
		case DELIVERY:
			Optional<ImportStatementEntity> oImportStatement = this.getReferenceImportStatement(referenceId);
			return oImportStatement.get();
		case EXPORT_STATEMENT:
			Optional<ExportStatementEntity> oExportStatement = this.getReferenceExportStatement(referenceId);
			return oExportStatement.get();
		default:
			return null;
		}
	}

	private Optional<ExportStatementEntity> getReferenceExportStatement(Long referenceId) throws BusinessException {
		if (referenceId == null) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_MISSING_REFERENCE);
		}
		Optional<ExportStatementEntity> oExportStatement = this.exportStatementRepository.findById(referenceId);
		if (!oExportStatement.isPresent()) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_REFERENCE_NOT_EXISTS);
		}
		return oExportStatement;
	}

	private Optional<ImportStatementEntity> getReferenceImportStatement(Long referenceId) throws BusinessException {
		if (referenceId == null) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_MISSING_REFERENCE);
		}
		Optional<ImportStatementEntity> oImportStatement = this.importStatementRepository.findById(referenceId);
		if (!oImportStatement.isPresent()) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_REFERENCE_NOT_EXISTS);
		}
		return oImportStatement;
	}

	private Optional<RepackingPlanningEntity> getReferenceRepackingPlanning(Long referenceId) throws BusinessException {
		if (referenceId == null) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_MISSING_REFERENCE);
		}
		Optional<RepackingPlanningEntity> oRepackingPlanning = this.repackingPlanningRepository.findById(referenceId);
		if (!oRepackingPlanning.isPresent()) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_REFERENCE_NOT_EXISTS);
		}
		return oRepackingPlanning;
	}

	private Optional<ImportPoEntity> getReferenceImportPo(Long referenceId) throws BusinessException {
		if (referenceId == null) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_MISSING_REFERENCE);
		}
		Optional<ImportPoEntity> oImportPo = this.importPoRepository.findById(referenceId);
		if (!oImportPo.isPresent()) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_REFERENCE_NOT_EXISTS);
		}
		return oImportPo;
	}

	private Optional<PoEntity> getReferencePo(Long referenceId) throws BusinessException {
		if (referenceId == null) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_MISSING_REFERENCE);
		}
		Optional<PoEntity> oPo = this.poRepository.findById(referenceId);
		if (!oPo.isPresent()) {
			throw new BusinessException(ClaimType.CLAIM_INPUT_REFERENCE_NOT_EXISTS);
		}
		return oPo;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> accept(Long id) throws BusinessException {
		Optional<Claim> oClaim = this.claimRepository.findById(id);
		if (!oClaim.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(ClaimServiceImpl.CLAIM_ID_NOT_EXISTS, id));
		}
		Claim claim = oClaim.get();
		if (!ClaimStatus.WAIT_APPROVE.getValue().equals(claim.getStatus())) {
			throw new BusinessException(this.translator.toLocale(ClaimServiceImpl.CLAIM_INPUT_STATUS_CAN_NOT_ACTION));
		}
		SuperEntity superEntity = this.getReferenceEntity(claim.getType(), claim.getReferenceId());
		String messageValid = claim.getType().validReferenceWithClaimDetails(superEntity,
				this.getManagedStoreIncludeProvideStores(), claim.getClaimDetails());
		if (!StringUtils.isNullOrEmpty(messageValid)) {
			throw new BusinessException(this.translator.toLocale(messageValid));
		}
		claim.setStatus(ClaimStatus.APPROVED.getValue());
		claim = this.claimRepository.save(claim);
		return new ResponseEntity<>(claim, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> reject(Long id, Claim claimRejectReason) throws BusinessException {
		if (StringUtils.isNullOrEmpty(claimRejectReason.getRejectReason())) {
			throw new BusinessException(this.translator.toLocale("claim.input.missing.reject.reason"));
		}
		Optional<Claim> oClaim = this.claimRepository.findById(id);
		if (!oClaim.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(ClaimServiceImpl.CLAIM_ID_NOT_EXISTS, id));
		}
		Claim claim = oClaim.get();
		if (!ClaimStatus.WAIT_APPROVE.getValue().equals(claim.getStatus())) {
			throw new BusinessException(this.translator.toLocale(ClaimServiceImpl.CLAIM_INPUT_STATUS_CAN_NOT_ACTION));
		}
		claim.setRejectReason(claimRejectReason.getRejectReason());
		claim.setStatus(ClaimStatus.REJECTED.getValue());
		claim = this.claimRepository.save(claim);
		this.returnClaimDetailToPallet(claim.getClaimDetails());
		return new ResponseEntity<>(claim, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> confirmCompleted(Long id, List<ClaimImage> adds, MultipartFile[] files)
			throws BusinessException {
		Optional<Claim> oClaim = this.claimRepository.findById(id);
		if (!oClaim.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(ClaimServiceImpl.CLAIM_ID_NOT_EXISTS, id));
		}
		Claim claim = oClaim.get();
		if (!ClaimStatus.APPROVED.getValue().equals(claim.getStatus())) {
			throw new BusinessException(this.translator.toLocale("claim.input.status.can.not.confirm.completed"));
		}
		if ((adds == null) || adds.isEmpty() || (files == null) || (files.length != adds.size())) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		this.addImages(claim, adds, files);
		claim.setStatus(ClaimStatus.COMPLETED.getValue());
		claim = this.claimRepository.save(claim);
		return new ResponseEntity<>(claim, HttpStatus.OK);
	}

	private void returnClaimDetailToPallet(List<ClaimDetail> claimDetails) throws BusinessException {
		if ((claimDetails == null) || claimDetails.isEmpty()) {
			return;
		}
		for (ClaimDetail claimDetail : claimDetails) {
			if ((claimDetail.getPalletDetail() != null) && (claimDetail.getPalletDetail().getId() != null)) {
				this.palletService.pushUpPalletDetail(claimDetail.getPalletDetail().getId(), claimDetail.getQuantity());
			}
			if (claimDetail.getStoreProductPackingDetail() != null) {
				StoreProductPackingDetailEntity storeProductPackingDetail = claimDetail.getStoreProductPackingDetail();
				StoreProductPackingEntity storeProductPacking = storeProductPackingDetail.getStoreProductPacking();
				storeProductPackingDetail
						.setQuantity(storeProductPackingDetail.getQuantity() + claimDetail.getQuantity());
				storeProductPacking
						.setTotalQuantity(storeProductPacking.getTotalQuantity() + claimDetail.getQuantity());
				this.storeProductPackingDetailRepository.save(storeProductPackingDetail);
				this.storeProductPackingRepository.save(storeProductPacking);
			}
		}
	}

	@Override
	public List<ClaimDetail> getApprovedClaimDetailsByTypeAndReferenceId(ClaimType claimType, Long referenceId)
			throws BusinessException {
		Optional<Claim> oClaim = this.claimRepository.getClaimByTypeAndReferenceId(claimType, referenceId);
		List<ClaimDetail> claimDetails = null;
		if (oClaim.isPresent()) {
			if (ClaimStatus.WAIT_APPROVE.getValue().equals(oClaim.get().getStatus())) {
				throw new BusinessException(this.translator.toLocale("claim.status.please.wait.for.accept"));
			} else if (ClaimStatus.APPROVED.getValue().equals(oClaim.get().getStatus())) {
				claimDetails = oClaim.get().getClaimDetails();
			}
		}
		return claimDetails;
	}

	@Override
	public ResponseEntity<Object> findByReference(ClaimType claimType, Long referenceId) throws BusinessException {
		List<ClaimDetail> claimDetails = this.getApprovedClaimDetailsByTypeAndReferenceId(claimType, referenceId);
		if (claimDetails == null) {
			claimDetails = new ArrayList<>();
		}
		return new ResponseEntity<>(claimDetails, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> updateImage(Long id, List<ClaimImage> deletes, List<ClaimImage> adds,
			List<ClaimImage> edits, MultipartFile[] files) throws BusinessException {
		Optional<Claim> oClaim = this.claimRepository.findById(id);
		if (!oClaim.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(ClaimServiceImpl.CLAIM_ID_NOT_EXISTS, id));
		}
		if ((deletes != null) && !deletes.isEmpty()) {
			this.deleteOldImages(deletes);
		}
		if ((edits != null) && !edits.isEmpty()) {
			this.editImages(oClaim.get(), edits);
		}
		if ((adds != null) && !adds.isEmpty() && (files != null) && (files.length == adds.size())) {
			this.addImages(oClaim.get(), adds, files);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private void addImages(Claim claim, List<ClaimImage> adds, MultipartFile[] files) throws BusinessException {
		for (MultipartFile multipartFile : files) {
			for (ClaimImage claimImage : adds) {
				String originalFileName = multipartFile.getOriginalFilename();
				if ((originalFileName != null) && originalFileName.equals(claimImage.getUrl())) {
					String fileName = this.fileStorageService.saveFile("claim-images", multipartFile);
					ClaimServiceImpl.log.info(fileName);
					ClaimImage image = new ClaimImage();
					image.setClaim(claim);
					image.setUrl(fileName);
					image.setDescription(claimImage.getDescription());
					this.validClaimImage(image);
					this.claimImageRepository.save(image);
				}
			}
		}
	}

	private void editImages(Claim claim, List<ClaimImage> edits) throws BusinessException {
		for (ClaimImage claimImage : edits) {
			Optional<ClaimImage> oClaimImage = this.claimImageRepository
					.findById(claimImage.getId() == null ? 0 : claimImage.getId());
			if (!oClaimImage.isPresent()) {
				throw new BusinessException(this.translator
						.toLocaleByFormatString(ClaimServiceImpl.CLAIM_IMAGE_ID_NOT_EXISTS, claimImage.getId()));
			}
			ClaimImage image = oClaimImage.get();
			if (!image.getClaim().getId().equals(claim.getId())) {
				throw new BusinessException(this.translator
						.toLocaleByFormatString(ClaimServiceImpl.CLAIM_IMAGE_ID_NOT_EXISTS, claimImage.getId()));
			}
			image.setDescription(claimImage.getDescription());
			this.validClaimImage(image);
			this.claimImageRepository.save(image);
		}
	}

	private void validClaimImage(ClaimImage claimImage) throws BusinessException {
		if (StringUtils.isNullOrEmpty(claimImage.getDescription())) {
			throw new BusinessException(this.translator.toLocale("claim.image.missing.description"));
		}
	}

	private void deleteOldImages(List<ClaimImage> deletes) throws BusinessException {
		for (int i = 0; i < deletes.size(); i++) {
			ClaimImage claimImage = deletes.get(i);
			if (claimImage.getId() == null) {
				throw new BusinessException(this.translator.toLocale("warehouse.image.input.invalid.data.struct"));
			}
			Optional<ClaimImage> oClaimImage = this.claimImageRepository.findById(claimImage.getId());
			if (!oClaimImage.isPresent()) {
				throw new BusinessException(this.translator
						.toLocaleByFormatString(ClaimServiceImpl.CLAIM_IMAGE_ID_NOT_EXISTS, claimImage.getId()));
			}
			ClaimImage image = oClaimImage.get();
			deletes.set(i, image);
			this.claimImageRepository.delete(image);
		}
		List<String> fileUrls = deletes.stream().map(ClaimImage::getUrl).collect(Collectors.toList());
		if (Boolean.FALSE.equals(this.fileStorageService.deleteFile(fileUrls))) {
			throw new BusinessException(this.translator.toLocale("claim.image.delete.failed"));
		}
	}

	@Override
	public ResponseEntity<Object> printPdf(Long id) throws BusinessException {
		Optional<Claim> oClaim = this.claimRepository.findById(id);
		if (!oClaim.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(ClaimServiceImpl.CLAIM_ID_NOT_EXISTS, id));
		}
		Claim claim = oClaim.get();
		if (!ClaimStatus.APPROVED.getValue().equals(claim.getStatus())) {
			throw new BusinessException(this.translator.toLocale("claim.print.status"));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("createDate", Utils.toDateString(claim.getCreateDate(), Utils.DATE_FORMAT_REPORT));
		map.put("code", claim.getCode());
		map.put("store", claim.getStore().getCode());
		map.put("details", claim.getClaimDetails());

		List<DrawBase64Dto> drawBase64Dtos = new ArrayList<>();
		drawBase64Dtos.add(new DrawBase64Dto("", 1, 1, 2, 4));
		map.put("specialImagePosition", drawBase64Dtos);

		try {
			String excelFilePath = Jxls2xTransformerUtils.poiTransformer("templates/PRINT_CLAIM.xlsx", map,
					this.fileStorageService.getTempExportExcel(), claim.getCode() + ".xlsx");
			String pdfFilePath = ExcelTo.pdf(this.fileStorageService.getLibreOfficePath(), excelFilePath,
					this.fileStorageService.getTempExportExcel());
			File file = new File(pdfFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.pdf.output.file.path.not.exists"));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(pdfFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			ClaimServiceImpl.log.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<Object> getListDamagedGoods(Claim claim, String acceptLanguage, Integer pageNumber,
			Integer pageSize) throws BusinessException {
		List<Long> managedStoreIncludeProvideStores = this.getManagedStoreIncludeProvideStores();
		return new ResponseEntity<>(this.claimRepository.getListDamagedGoods(claim, managedStoreIncludeProvideStores,
				acceptLanguage, PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

}
