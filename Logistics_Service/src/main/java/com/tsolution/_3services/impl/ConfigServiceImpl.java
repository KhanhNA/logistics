package com.tsolution._3services.impl;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.Config;
import com.tsolution._3services.ConfigService;
import com.tsolution.excetions.BusinessException;

@Service
public class ConfigServiceImpl extends LogisticsBaseService implements ConfigService {

	@Override
	public ResponseEntity<Object> find(String code) throws BusinessException {
		Optional<Config> oConfig = this.configRepository.findOneByCode(code);
		if (!oConfig.isPresent()) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		return new ResponseEntity<>(oConfig.get(), HttpStatus.OK);
	}

}
