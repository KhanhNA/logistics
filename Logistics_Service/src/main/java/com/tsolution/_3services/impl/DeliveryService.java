package com.tsolution._3services.impl;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tsolution._1entities.export_statement.ExportStatementDelivery;
import com.tsolution.config.DeliveryServiceConfig;

@Service
public class DeliveryService {
	private RestTemplate restTemplate;

	private String url;

	@Autowired
	public DeliveryService(DeliveryServiceConfig deliveryConfig) {
		this.restTemplate = new RestTemplate();
		this.url = deliveryConfig.getUrl();
	}

	public ResponseEntity<ExportStatementDelivery> postForObject(String relativeUrl,
			ExportStatementDelivery exportStatementDelivery) throws MalformedURLException, URISyntaxException {
		HttpEntity<ExportStatementDelivery> request = new HttpEntity<>(exportStatementDelivery);
		return this.restTemplate.postForEntity(new URL(new URL(this.url), relativeUrl).toURI(), request,
				ExportStatementDelivery.class);
	}
}
