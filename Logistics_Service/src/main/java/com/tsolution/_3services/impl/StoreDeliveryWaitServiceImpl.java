package com.tsolution._3services.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.store.StoreDeliveryWaitEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.dto.StoreDto;
import com.tsolution._2repositories.store.StoreDeliveryWaitRepository;
import com.tsolution._3services.StoreDeliveryWaitService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Jxls2xTransformerUtils;

@Service
public class StoreDeliveryWaitServiceImpl extends LogisticsBaseService implements StoreDeliveryWaitService {

	private static Logger log = LogManager.getLogger(StoreDeliveryWaitServiceImpl.class);

	private static final String STORE_DELIVERY_WAIT_ID_NOT_EXISTS = "store.delivery.wait.id.not.exists";
	@Autowired
	private StoreDeliveryWaitRepository storeDeliveryWaitRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.storeDeliveryWaitRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<StoreDeliveryWaitEntity> oStoreDeliveryWait = this.storeDeliveryWaitRepository.findById(id);
		if (!oStoreDeliveryWait.isPresent()) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(StoreDeliveryWaitServiceImpl.STORE_DELIVERY_WAIT_ID_NOT_EXISTS, id));
		}
		return new ResponseEntity<>(oStoreDeliveryWait.get(), HttpStatus.OK);
	}

	private StoreEntity findStoreById(Long id) throws BusinessException {
		Optional<StoreEntity> oStore = this.storeRepository.findById(id == null ? 0 : id);
		if (!oStore.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("store.id.not.exists", id));
		}
		return oStore.get();
	}

	private void validStoreDeliveryWait(StoreDeliveryWaitEntity storeDeliveryWait) throws BusinessException {
		if ((storeDeliveryWait.getFromStore() == null) || (storeDeliveryWait.getFromStore().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("store.delivery.wait.input.missing.from.store"));
		}
		if ((storeDeliveryWait.getToStore() == null) || (storeDeliveryWait.getToStore().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("store.delivery.wait.input.missing.to.store"));
		}
		if ((storeDeliveryWait.getMaxDayToWait() == null) || (storeDeliveryWait.getMaxDayToWait() <= 0)) {
			throw new BusinessException(
					this.translator.toLocale("store.delivery.wait.input.max.day.wait.must.be.greater.than.zero"));
		}
		this.findStoreById(storeDeliveryWait.getFromStore().getId());
		this.findStoreById(storeDeliveryWait.getToStore().getId());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<StoreDeliveryWaitEntity> entities) throws BusinessException {
		List<StoreDeliveryWaitEntity> storeDeliveryWaits = new ArrayList<>();
		for (StoreDeliveryWaitEntity storeDeliveryWait : entities) {
			if (storeDeliveryWait.getId() != null) {
				throw new BusinessException(this.translator.toLocale("store.delivery.wait.input.cannot.have.id"));
			}
			this.validStoreDeliveryWait(storeDeliveryWait);
			storeDeliveryWaits.add(this.storeDeliveryWaitRepository.save(storeDeliveryWait));
		}
		return new ResponseEntity<>(storeDeliveryWaits, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, StoreDeliveryWaitEntity source) throws BusinessException {
		Optional<StoreDeliveryWaitEntity> oStoreDeliveryWaitEntity = this.storeDeliveryWaitRepository.findById(id);
		if (!oStoreDeliveryWaitEntity.isPresent() || !id.equals(source.getId())) {
			throw new BusinessException(this.translator
					.toLocaleByFormatString(StoreDeliveryWaitServiceImpl.STORE_DELIVERY_WAIT_ID_NOT_EXISTS, id));
		}
		this.validStoreDeliveryWait(source);
		return new ResponseEntity<>(this.storeDeliveryWaitRepository.save(source), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getAllByFromStore(Long fromStoreId) throws BusinessException {
		this.findStoreById(fromStoreId);
		return new ResponseEntity<>(this.storeDeliveryWaitRepository.getAllByFromStore(fromStoreId), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> exportFromStore(Long fromStoreId) throws BusinessException {
		// Kiểm tra id truyền vào
		StoreEntity fromStore = this.findStoreById(fromStoreId);
		// Lấy dữ liệu cũ
		List<StoreDeliveryWaitEntity> storeDeliveryWaits = this.storeDeliveryWaitRepository
				.getAllByFromStore(fromStoreId);
		storeDeliveryWaits = storeDeliveryWaits.stream()
				.filter(sdw -> Boolean.TRUE.equals(sdw.getToStore().getStatus())).collect(Collectors.toList());
		// Lấy các Kho chưa có thông tin thời gian
		List<Long> exceptIds = storeDeliveryWaits.stream().map(sdw -> sdw.getToStore().getId())
				.collect(Collectors.toList());
		exceptIds.add(fromStoreId);
		StoreDto storeDto = new StoreDto();
		storeDto.setStatus(true);
		storeDto.setExceptIds(exceptIds);
		List<StoreEntity> toStores = this.storeRepository.find(storeDto, PageRequest.of(0, Integer.MAX_VALUE))
				.getContent();
		for (StoreEntity storeEntity : toStores) {
			StoreDeliveryWaitEntity storeDeliveryWait = new StoreDeliveryWaitEntity();
			storeDeliveryWait.setFromStore(fromStore);
			storeDeliveryWait.setToStore(storeEntity);
			storeDeliveryWaits.add(0, storeDeliveryWait);
		}
		// Export
		Map<String, Object> map = new HashMap<>();
		map.put("fromStore", fromStore);
		map.put("details", storeDeliveryWaits);
		String outputFilePath = Jxls2xTransformerUtils.poiTransformer("templates/IMPORT_TRANSFER_WAREHOUSE_TIME.xlsx",
				map, this.fileStorageService.getTempExportExcel(),
				String.format("IMPORT_TRANSFER_WAREHOUSE_TIME_%d_%s.xlsx", fromStoreId, fromStore.getCode()));
		try {
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.excel.output.file.path.not.exists"));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (IOException e) {
			StoreDeliveryWaitServiceImpl.log.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> importFromStore(Long fromStoreId, MultipartFile file) throws BusinessException {
		// Kiểm tra id truyền vào
		StoreEntity fromStore = this.findStoreById(fromStoreId);
		// Lấy dữ liệu cũ
		List<StoreDeliveryWaitEntity> storeDeliveryWaits = this.storeDeliveryWaitRepository
				.getAllByFromStore(fromStoreId);
		List<StoreDeliveryWaitEntity> results = new ArrayList<>();
		try (XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());) {
			XSSFSheet sheet = workbook.getSheet("Result");
			for (int i = 8; i <= sheet.getLastRowNum(); i++) {
				XSSFRow row = sheet.getRow(i);
				String toStoreCode = row.getCell(1).getStringCellValue();
				double time = row.getCell(4).getNumericCellValue();
				Optional<StoreDeliveryWaitEntity> oStoreDeliveryWait = storeDeliveryWaits.stream()
						.filter(sdw -> sdw.getToStore().getCode().equalsIgnoreCase(toStoreCode)).findFirst();
				StoreDeliveryWaitEntity storeDeliveryWait;
				if (oStoreDeliveryWait.isPresent()) {
					storeDeliveryWait = oStoreDeliveryWait.get();
				} else {
					StoreEntity toStore = this.storeRepository.findOneByCode(toStoreCode);
					if (toStore == null) {
						throw new BusinessException(
								this.translator.toLocaleByFormatString("store.code.not.exists", toStoreCode));
					}
					storeDeliveryWait = new StoreDeliveryWaitEntity();
					storeDeliveryWait.setFromStore(fromStore);
					storeDeliveryWait.setToStore(toStore);
				}
				storeDeliveryWait.setMaxDayToWait(BigDecimal.valueOf(time).intValue());
				results.add(this.storeDeliveryWaitRepository.save(storeDeliveryWait));
			}
		} catch (IOException e) {
			StoreDeliveryWaitServiceImpl.log.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

}
