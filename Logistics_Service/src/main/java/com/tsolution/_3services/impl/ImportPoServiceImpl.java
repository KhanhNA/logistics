package com.tsolution._3services.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.PoDetailEntity;
import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.ProductEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.claim.ClaimDetail;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution._1entities.enums.ImportPoStatus;
import com.tsolution._1entities.enums.PalletStep;
import com.tsolution._1entities.enums.PoStatus;
import com.tsolution._1entities.importpo.ImportPoDetailEntity;
import com.tsolution._1entities.importpo.ImportPoDetailPalletEntity;
import com.tsolution._1entities.importpo.ImportPoEntity;
import com.tsolution._1entities.importpo.ImportPoQcEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._2repositories.DistributorRepository;
import com.tsolution._2repositories.ManufacturerRepository;
import com.tsolution._2repositories.PalletDetailRepository;
import com.tsolution._2repositories.PoDetailRepository;
import com.tsolution._2repositories.PoRepository;
import com.tsolution._2repositories.importpo.ImportPoDetailPalletRepository;
import com.tsolution._2repositories.importpo.ImportPoDetailRepository;
import com.tsolution._2repositories.importpo.ImportPoQcRepository;
import com.tsolution._2repositories.importpo.ImportPoRepository;
import com.tsolution._3services.ImportPoService;
import com.tsolution._3services.PalletService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.QRCodeGenerator;

@Service
public class ImportPoServiceImpl extends LogisticsBaseService implements ImportPoService {

	private static final String IMPORT_PO_DETAIL_QUANTITY_INVALID = "import.po.detail.quantity.invalid";

	@Autowired
	private PoRepository poRepository;

	@Autowired
	private ImportPoRepository importPoRepository;

	@Autowired
	private ImportPoDetailRepository importPoDetailRepository;

	@Autowired
	private ImportPoDetailPalletRepository importPoDetailPalletRepository;

	@Autowired
	private ImportPoQcRepository importPoQcRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private PalletService palletService;

	@Autowired
	private PalletDetailRepository palletDetailRepository;

	@Autowired
	private PoDetailRepository poDetailRepository;

	@Autowired
	private ManufacturerRepository manufacturerRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.importPoRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<ImportPoEntity> importPo = this.importPoRepository.findById(id);
		if (!importPo.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("import.po.id.not.exists", id));
		}
		return new ResponseEntity<>(importPo.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(Long storeId, Long distributorId, String text, Integer status,
			Integer pageNumber, Integer pageSize, String acceptLanguage) throws BusinessException {
		return new ResponseEntity<>(this.importPoRepository.find(storeId, distributorId, text, status,
				this.getCurrentUsername(), PageRequest.of(pageNumber - 1, pageSize), acceptLanguage), HttpStatus.OK);
	}

	private void validImportPo(ImportPoEntity importPo, LocalDateTime sysdate) throws BusinessException {
		if ((importPo.getPo() == null) || (importPo.getPo().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.missing.po"));
		}
		if ((importPo.getManufacturer() == null) || (importPo.getManufacturer().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.missing.manufacturer"));
		}
		if ((importPo.getStore() == null) || (importPo.getStore().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.missing.store"));
		}
		if ((importPo.getDistributor() == null) || (importPo.getDistributor().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.missing.distributor"));
		}
		Optional<PoEntity> oPo = this.poRepository.findById(importPo.getPo().getId());
		if (!oPo.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("po.id.not.exists", importPo.getPo().getId()));
		}
		importPo.setPo(oPo.get());
		Optional<ManufacturerEntity> oManufacturer = this.manufacturerRepository
				.findById(importPo.getManufacturer().getId());
		if (!oManufacturer.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("manufacturer.id.not.exists",
					importPo.getManufacturer().getId()));
		}
		Optional<StoreEntity> oStore = this.storeRepository.findById(importPo.getStore().getId());
		if (!oStore.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("store.id.not.exists", importPo.getStore().getId()));
		}
		Optional<DistributorEntity> oDistributor = this.distributorRepository
				.findById(importPo.getDistributor().getId());
		if (!oDistributor.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("distributor.id.not.exists",
					importPo.getDistributor().getId()));
		}
		if (!ImportPoStatus.NEW.getValue().equals(importPo.getStatus())) {
			throw new BusinessException(this.translator.toLocale("import.po.status.check.new"));
		}
		PoEntity po = oPo.get();
		if (!PoStatus.ARRIVED_MYANMAR_PORT.equals(po.getStatus())) {
			throw new BusinessException(this.translator.toLocale("po.status.check.arrived.myanmar.port"));
		}
		this.validImportPoDetails(importPo, po, sysdate);
	}

	private void validImportPoDetails(ImportPoEntity importPo, PoEntity po, LocalDateTime sysdate)
			throws BusinessException {
		if ((importPo.getImportPoDetails() == null) || importPo.getImportPoDetails().isEmpty()) {
			throw new BusinessException(this.translator.toLocale("import.po.missing.details"));
		}
		List<ImportPoDetailEntity> importPoDetails = importPo.getImportPoDetails();
		for (ImportPoDetailEntity importPoDetail : importPoDetails) {
			this.validImportPoDetail(importPoDetail, sysdate);
		}
		List<PoDetailEntity> poDetails = po.getPoDetails();
		List<ClaimDetail> claimDetails = this.claimService
				.getApprovedClaimDetailsByTypeAndReferenceId(ClaimType.IMPORT_PO, po.getId());
		for (PoDetailEntity poDetail : poDetails) {
			Long importPoDetailQuantity = importPoDetails.stream()
					.filter(x -> poDetail.getId().equals(x.getPoDetail().getId()))
					.mapToLong(ImportPoDetailEntity::getQuantity).sum();
			if (importPoDetailQuantity < 0) {
				throw new BusinessException(
						this.translator.toLocale(ImportPoServiceImpl.IMPORT_PO_DETAIL_QUANTITY_INVALID));
			} else if (claimDetails == null) {
				if (!poDetail.getQuantity().equals(importPoDetailQuantity)) {
					throw new BusinessException(
							this.translator.toLocale(ImportPoServiceImpl.IMPORT_PO_DETAIL_QUANTITY_INVALID));
				}
			} else {
				Long claimDetailQuantity = claimDetails.stream()
						.filter(x -> x.getProductPacking().getId().equals(poDetail.getProductPacking().getId()))
						.mapToLong(ClaimDetail::getQuantity).sum();
				if (!poDetail.getQuantity().equals(importPoDetailQuantity + claimDetailQuantity)) {
					throw new BusinessException(
							this.translator.toLocale(ImportPoServiceImpl.IMPORT_PO_DETAIL_QUANTITY_INVALID));
				}

			}
		}
	}

	private void validImportPoDetail(ImportPoDetailEntity importPoDetail, LocalDateTime sysdate)
			throws BusinessException {
		this.validImportPoDetailNonQuery(importPoDetail, sysdate);
		Optional<ProductEntity> oProduct = this.productRepository.findById(importPoDetail.getProduct().getId());
		if (!oProduct.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.id.not.exists",
					importPoDetail.getProduct().getId()));
		}
		Optional<ProductPackingEntity> oProductPacking = this.productPackingRepository
				.findById(importPoDetail.getProductPacking().getId());
		if (!oProductPacking.isPresent() || !oProduct.get().equals(oProductPacking.get().getProduct())) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.id.not.exists",
					importPoDetail.getProductPacking().getId()));
		}
		Optional<ProductPackingPriceEntity> oProductPackingPrice = this.productPackingPriceRepository
				.findById(importPoDetail.getProductPackingPrice().getId());
		if (!oProductPackingPrice.isPresent()
				|| !oProductPacking.get().equals(oProductPackingPrice.get().getProductPacking())) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.price.id.not.exists",
					importPoDetail.getProductPackingPrice().getId()));
		}
		Optional<PoDetailEntity> oPoDetail = this.poDetailRepository.findById(importPoDetail.getPoDetail().getId());
		if (!oPoDetail.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("po.detail.id.not.exists",
					importPoDetail.getPoDetail().getId()));
		}
	}

	private void validImportPoDetailNonQuery(ImportPoDetailEntity importPoDetail, LocalDateTime sysdate)
			throws BusinessException {
		if ((importPoDetail.getProduct() == null) || (importPoDetail.getProduct().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.missing.product"));
		}
		if ((importPoDetail.getProductPacking() == null) || (importPoDetail.getProductPacking().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.missing.product.packing"));
		}
		if ((importPoDetail.getProductPackingPrice() == null)
				|| (importPoDetail.getProductPackingPrice().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.missing.product.packing.price"));
		}
		if ((importPoDetail.getPoDetail() == null) || (importPoDetail.getPoDetail().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.missing.po.detail"));
		}
		if (importPoDetail.getExpireDate() == null) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.missing.expire.date"));
		}
		if ((importPoDetail.getQuantity() > 0) && importPoDetail.getExpireDate().isBefore(sysdate)) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.expire.date.invalid"));
		}
	}

	private List<ImportPoQcEntity> updateQcFile(ImportPoEntity importPo, MultipartFile[] files,
			List<Long> removeFileIds) throws BusinessException {
		List<ImportPoQcEntity> keepFiles = importPo.getImportPoQcs();
		if ((removeFileIds != null) && !removeFileIds.isEmpty()) {
			List<ImportPoQcEntity> removeFiles = keepFiles.stream()
					.filter(kpiFile -> removeFileIds.contains(kpiFile.getId())).collect(Collectors.toList());
			if ((removeFiles != null) && !removeFiles.isEmpty()) {
				List<String> fileUrls = removeFiles.stream().map(ImportPoQcEntity::getUrl).collect(Collectors.toList());
				if (Boolean.TRUE.equals(this.fileStorageService.deleteFile(fileUrls))) {
					keepFiles = keepFiles.stream().filter(kpiFile -> !removeFileIds.contains(kpiFile.getId()))
							.collect(Collectors.toList());
					this.importPoQcRepository.deleteAll(removeFiles);
				} else {
					throw new BusinessException("DELETE FILE FAILED!!!");
				}
			}
		}

		if ((files == null) || (files.length == 0)) {
			return keepFiles;
		}
		LocalDateTime sysdate = this.getSysdate();

		for (int i = 0; i < files.length; i++) {
			MultipartFile multipartFile = files[i];
			String fileName = this.fileStorageService.saveFile(
					String.format("%04d/%02d/QC-files", sysdate.getYear(), sysdate.getMonthValue()), multipartFile);
			ImportPoQcEntity file = new ImportPoQcEntity();
			file.setPo(importPo.getPo());
			file.setImportPo(importPo);
			file.setUrl(fileName);
			this.importPoQcRepository.save(file);
			keepFiles.add(file);
		}
		return keepFiles;
	}

	private ImportPoEntity createOne(ImportPoEntity importPo, MultipartFile[] files, LocalDateTime sysdate, int index)
			throws BusinessException {
		if ((importPo.getPo() == null) || (importPo.getPo().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.missing.po"));
		}
		this.validImportPo(importPo, sysdate);
		String code = String.format("%s-IMPORTPO-%s/%s", sysdate.format(DateTimeFormatter.ofPattern("yyyyMMdd")),
				sysdate.format(DateTimeFormatter.ofPattern("HHmmssSSS")), index);
		importPo.setCode(code);
		importPo.setqRCode(QRCodeGenerator.generate(code));
		importPo.setStatus(ImportPoStatus.NEW.getValue());
		importPo = this.importPoRepository.save(importPo);

		PoEntity po = importPo.getPo();
		po.setStatus(PoStatus.IMPORTED);
		po.setImportDate(sysdate);
		po.setImportUser(this.getCurrentUsername());
		this.poRepository.save(po);

		List<ImportPoDetailEntity> importPoDetails = new ArrayList<>();
		for (ImportPoDetailEntity importPoDetail : importPo.getImportPoDetails()) {
			importPoDetail.setImportPo(importPo);
			importPoDetail = this.importPoDetailRepository.save(importPoDetail);
			importPoDetails.add(importPoDetail);
		}
		importPo.setImportPoDetails(importPoDetails);
		importPo.setImportPoQcs(this.updateQcFile(importPo, files, new ArrayList<>()));
		return importPo;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(ImportPoEntity importPoEntity, MultipartFile[] files)
			throws BusinessException {
		return new ResponseEntity<>(this.createOne(importPoEntity, files, this.getSysdate(), 0), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<ImportPoEntity> entities) throws BusinessException {
		LocalDateTime sysdate = this.getSysdate();
		List<ImportPoEntity> list = new ArrayList<>();
		for (ImportPoEntity importPo : entities) {
			list.add(this.createOne(importPo, null, sysdate, entities.indexOf(importPo)));
		}
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, ImportPoEntity source) throws BusinessException {
		Optional<ImportPoEntity> oTarget = this.importPoRepository.findById(id);
		if (!oTarget.isPresent() || !ImportPoStatus.NEW.getValue().equals(oTarget.get().getStatus())) {
			throw new BusinessException(this.translator.toLocale("import.po.status.check.new"));
		}
		ImportPoEntity target = oTarget.get();
		LocalDateTime sysdate = this.getSysdate();
		this.validImportPo(source, sysdate);
		target.setDescription(source.getDescription());
		target = this.importPoRepository.save(target);
		List<ImportPoDetailEntity> targetDetails = target.getImportPoDetails();
		// Lọc ra IDs không nằm trong list detail sau khi sửa => xóa
		List<ImportPoDetailEntity> listDelete = targetDetails.stream()
				.filter(ipod -> source.getImportPoDetails().stream().noneMatch(x -> ipod.getId().equals(x.getId())))
				.collect(Collectors.toList());
		if (targetDetails.removeAll(listDelete)) {
			this.importPoDetailRepository.deleteAll(listDelete);
		}
		// Add Or Edit Details
		for (ImportPoDetailEntity sourceDetail : source.getImportPoDetails()) {
			if (sourceDetail.getId() == null) {
				sourceDetail.setImportPo(target);
				sourceDetail = this.importPoDetailRepository.save(sourceDetail);
				targetDetails.add(sourceDetail);
			} else {
				int index = targetDetails.stream().map(ImportPoDetailEntity::getId).collect(Collectors.toList())
						.indexOf(sourceDetail.getId());
				if (index >= 0) {
					ImportPoDetailEntity temp = targetDetails.get(index);
					temp.setQuantity(sourceDetail.getQuantity());
					temp.setExpireDate(sourceDetail.getExpireDate());
					temp = this.importPoDetailRepository.save(temp);
					targetDetails.set(index, temp);
				}
			}
		}
		target.setImportPoDetails(targetDetails);
		return new ResponseEntity<>(target, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public ResponseEntity<Object> updatePallet(Long id, List<ImportPoDetailPalletEntity> sourceDetailsPallets)
			throws BusinessException {
		Optional<ImportPoEntity> oTarget = this.importPoRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("import.po.id.not.exists", id));
		}
		ImportPoEntity target = oTarget.get();
		List<ImportPoDetailEntity> importPoDetails = target.getImportPoDetails();
		// Validate trước khi update
		this.validateUpdatePalletInput(target, sourceDetailsPallets);
		target.setStatus(ImportPoStatus.IN_PALLET.getValue());
		target = this.importPoRepository.save(target);
		List<ImportPoDetailPalletEntity> targetDetailPallets = new ArrayList<>();
		for (ImportPoDetailEntity targetDetail : importPoDetails) {
			List<ImportPoDetailPalletEntity> sourceImportPoDetailPallets = this
					.getImportPoDetailPallets(sourceDetailsPallets, this.removeImportPoDetailPallets(targetDetail));
			// Thêm mới ImportPoDetailPallet + PalletDetail
			for (ImportPoDetailPalletEntity sourceDetailPallet : sourceImportPoDetailPallets) {
				if ((sourceDetailPallet.getPallet() == null) || (sourceDetailPallet.getPallet().getId() == null)) {
					throw new BusinessException(
							this.translator.toLocale("import.po.detail.pallet.can.not.find.pallet"));
				}
				PalletEntity palletEntity = this.getPallet(sourceDetailPallet.getPallet().getId(),
						PalletStep.IMPORT_PO_UPDATE_PALLET);
				PalletDetailEntity palletDetailEntity = this.palletDetailRepository
						.save(new PalletDetailEntity(palletEntity, targetDetail, 0L));
				this.palletService.pushUpPalletDetail(palletDetailEntity.getId(), sourceDetailPallet.getQuantity());
				ImportPoDetailPalletEntity importPoDetailPallet = this.importPoDetailPalletRepository
						.save(new ImportPoDetailPalletEntity(target, targetDetail, sourceDetailPallet.getPallet(),
								palletDetailEntity, sourceDetailPallet.getQuantity()));
				targetDetailPallets.add(importPoDetailPallet);
			}
		}
		return new ResponseEntity<>(targetDetailPallets, HttpStatus.OK);
	}

	private void validClaimImportPoDetailPallet(ImportPoEntity importPo,
			List<ImportPoDetailPalletEntity> importPoDetailPallets) throws BusinessException {
		List<ClaimDetail> claimDetails = this.claimService
				.getApprovedClaimDetailsByTypeAndReferenceId(ClaimType.IMPORT_PO_UPDATE_PALLET, importPo.getId());
		List<ImportPoDetailEntity> importPoDetails = importPo.getImportPoDetails();
		for (ImportPoDetailEntity importPoDetail : importPoDetails) {
			Long importPoDetailPalletQuantity = importPoDetailPallets.stream()
					.filter(x -> x.getImportPoDetail().getId().equals(importPoDetail.getId()))
					.mapToLong(ImportPoDetailPalletEntity::getQuantity).sum();
			if (importPoDetailPalletQuantity < 0) {
				throw new BusinessException(
						this.translator.toLocale(ImportPoServiceImpl.IMPORT_PO_DETAIL_QUANTITY_INVALID));
			} else if (claimDetails == null) {
				if (!importPoDetail.getQuantity().equals(importPoDetailPalletQuantity)) {
					throw new BusinessException(this.translator.toLocale("import.po.detail.pallet.quantity.invalid"));
				}
			} else {
				Long claimDetailQuantity = claimDetails.stream()
						.filter(x -> x.getProductPacking().getId().equals(importPoDetail.getProductPacking().getId())
								&& x.getExpireDate().toLocalDate().equals(importPoDetail.getExpireDate().toLocalDate()))
						.mapToLong(ClaimDetail::getQuantity).sum();
				if (!importPoDetail.getQuantity().equals(importPoDetailPalletQuantity + claimDetailQuantity)) {
					throw new BusinessException(this.translator.toLocale("import.po.detail.pallet.quantity.invalid"));
				}

			}
		}
	}

	private void validateUpdatePalletInput(ImportPoEntity importPo,
			List<ImportPoDetailPalletEntity> importPoDetailPallets) throws BusinessException {
		// ImportPo: Status != IN_PALLET && Status != NEW => next
		if (!ImportPoStatus.IN_PALLET.getValue().equals(importPo.getStatus())
				&& !ImportPoStatus.NEW.getValue().equals(importPo.getStatus())) {
			throw new BusinessException(this.translator.toLocale("import.po.status.check.new.inpallet"));
		}
		List<ImportPoDetailEntity> importPoDetails = importPo.getImportPoDetails();
		// Bất kì bản ghi nào sai ID Import PO -> next
		if (importPoDetailPallets.stream().anyMatch(
				input -> (input.getImportPo() == null) || !input.getImportPo().getId().equals(importPo.getId()))) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.pallet.missing.import.po"));
		}
		// Bất kì bản ghi nào sai ID Import PO Detail -> next
		if (importPoDetailPallets.stream().anyMatch(input -> input.getImportPoDetail() == null)) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.pallet.missing.import.po.detail"));
		}
		// Bất kì bản ghi nào Thiếu thông tin pallet -> next
		if (importPoDetailPallets.stream()
				.anyMatch(input -> (input.getPallet() == null) || (input.getPallet().getId() == null))) {
			throw new BusinessException(this.translator.toLocale("import.po.detail.pallet.missing.pallet"));
		}
		// 2 List source vs target không khớp nhau về ID ImportPoDetail -> next
		List<Long> targetDetailIds = importPoDetails.stream().map(ImportPoDetailEntity::getId)
				.collect(Collectors.toList());
		List<Long> sourceDetailIds = importPoDetailPallets.stream().map(d -> d.getImportPoDetail().getId())
				.collect(Collectors.toList());
		if (targetDetailIds.stream().anyMatch(x -> !sourceDetailIds.contains(x))
				|| sourceDetailIds.stream().anyMatch(x -> !targetDetailIds.contains(x))) {
			throw new BusinessException(
					this.translator.toLocale("import.po.detail.pallet.input.invalid.update.pallet"));
		}
		this.validClaimImportPoDetailPallet(importPo, importPoDetailPallets);
	}

	private ImportPoDetailEntity removeImportPoDetailPallets(ImportPoDetailEntity targetDetail)
			throws BusinessException {
		// Xóa các ImportPoDetailPallet + PalletDetail cũ đi với điều kiện:
		// 1. ImportPoDetailPallet: isEmpty() => Bỏ qua điều kiện 2
		// 2. ImportPoDetailPallet: PalletDetail (Quantity != OrgQuantity) => next
		if (targetDetail.getImportPoDetailPallets() != null) {
			if (targetDetail.getImportPoDetailPallets().stream()
					.anyMatch(x -> !x.getPalletDetail().getQuantity().equals(x.getPalletDetail().getOrgQuantity()))) {
				throw new BusinessException(this.translator.toLocale("import.po.detail.pallet.can.not.update.pallet"));
			}
			this.importPoDetailPalletRepository.deleteAll(targetDetail.getImportPoDetailPallets());
			List<PalletDetailEntity> palletDetails = targetDetail.getImportPoDetailPallets().stream()
					.map(ImportPoDetailPalletEntity::getPalletDetail).collect(Collectors.toList());
			for (PalletDetailEntity palletDetail : palletDetails) {
				this.palletService.pullDownPalletDetail(palletDetail.getId(), palletDetail.getQuantity());
			}
			this.palletDetailRepository.deleteAll(palletDetails);
			targetDetail.setImportPoDetailPallets(new ArrayList<>());
		}
		return targetDetail;
	}

	private List<ImportPoDetailPalletEntity> getImportPoDetailPallets(
			List<ImportPoDetailPalletEntity> sourceDetailsPallets, ImportPoDetailEntity targetDetail)
			throws BusinessException {
		List<ImportPoDetailPalletEntity> sourceImportPoDetailPallets = new ArrayList<>();
		for (ImportPoDetailPalletEntity sourceDetailPallet : sourceDetailsPallets) {
			if (targetDetail.getId().equals(sourceDetailPallet.getImportPoDetail().getId())) {
				sourceImportPoDetailPallets.add(sourceDetailPallet);
			}
		}
		if (sourceImportPoDetailPallets.isEmpty()) {
			throw new BusinessException(
					this.translator.toLocale("import.po.detail.pallet.total.quantity.update.pallet.not.valid"));
		}
		return sourceImportPoDetailPallets;
	}
}
