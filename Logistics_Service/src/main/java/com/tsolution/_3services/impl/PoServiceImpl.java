package com.tsolution._3services.impl;

import java.io.File;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._1entities.CurrencyExchangeRateEntity;
import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.ManufacturerDescriptionEntity;
import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution._1entities.PoDetailEntity;
import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.ProductEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.dto.PoDto;
import com.tsolution._1entities.dto.PoPrintDto;
import com.tsolution._1entities.enums.PoStatus;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._2repositories.CurrencyExchangeRateRepository;
import com.tsolution._2repositories.CurrencyRepository;
import com.tsolution._2repositories.DistributorRepository;
import com.tsolution._2repositories.ManufacturerRepository;
import com.tsolution._2repositories.PoDetailRepository;
import com.tsolution._2repositories.PoRepository;
import com.tsolution._3services.PoService;
import com.tsolution._3services.report.ReportService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DatetimeUtils;
import com.tsolution.utils.ExcelTo;
import com.tsolution.utils.Jxls2xTransformerUtils;
import com.tsolution.utils.QRCodeGenerator;
import com.tsolution.utils.StringUtils;
import com.tsolution.utils.Utils;

@Service
public class PoServiceImpl extends LogisticsBaseService implements PoService {

	private static final String PO_ID_NOT_EXISTS = "po.id.not.exists";

	private static final Logger logger = LogManager.getLogger(PoServiceImpl.class);

	@Autowired
	PoRepository poRepository;

	@Autowired
	PoDetailRepository poDetailRepository;

	@Autowired
	private ReportService reportService;

	@Autowired
	private ManufacturerRepository manufacturerRepository;

	@Autowired
	private DistributorRepository distributerRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private CurrencyRepository currencyRepository;

	@Autowired
	private CurrencyExchangeRateRepository currencyExchangeRateRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.poRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<PoEntity> poEntity = this.poRepository.findById(id);
		if (!poEntity.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString(PoServiceImpl.PO_ID_NOT_EXISTS, id));
		}
		return new ResponseEntity<>(poEntity.get(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(PoDto poDto, Integer pageNumber, Integer pageSize, String orderBy,
			String acceptLanguage) throws BusinessException {
		Sort sort;
		if (StringUtils.isNullOrEmpty(orderBy)) {
			sort = Sort.by(Direction.ASC, "create_date").and(Sort.by(Direction.ASC, "approve_date"))
					.and(Sort.by(Direction.ASC, "import_date")).and(Sort.by(Direction.ASC, "code"));
		} else {
			String[] order = orderBy.split(",");
			sort = Sort.by("DESC".equals(order[1]) ? Direction.DESC : Direction.ASC, order[0]);
		}
		PageRequest pageRequest = PageRequest.of(pageNumber - 1, pageSize, sort);
		return new ResponseEntity<>(
				this.poRepository.find(poDto, this.getCurrentUsername(), pageRequest, acceptLanguage), HttpStatus.OK);
	}

	private void validPoEntity(PoEntity po) throws BusinessException {
		if ((po.getManufacturer() == null) || (po.getManufacturer().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("po.missing.manufacturer"));
		}
		if ((po.getStore() == null) || (po.getStore().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("po.missing.store"));
		}
		if ((po.getDistributor() == null) || (po.getDistributor().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("po.missing.distributor"));
		}

		Optional<ManufacturerEntity> oManufacturer = this.manufacturerRepository.findById(po.getManufacturer().getId());
		if (!oManufacturer.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("manufacturer.id.not.exists", po.getManufacturer().getId()));
		}
		Optional<StoreEntity> oStore = this.storeRepository.findById(po.getStore().getId());
		if (!oStore.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("store.id.not.exists", po.getStore().getId()));
		}
		Optional<DistributorEntity> oDistributor = this.distributerRepository.findById(po.getDistributor().getId());
		if (!oDistributor.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("distributor.id.not.exists", po.getDistributor().getId()));
		}

		this.validPoCurrencyExchangeRate(po);
	}

	private void validPoCurrencyExchangeRate(PoEntity po) throws BusinessException {
		if ((po.getFromCurrency() == null) || (po.getFromCurrency().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("po.missing.from.currency"));
		}
		if ((po.getToCurrency() == null) || (po.getToCurrency().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("po.missing.to.currency"));
		}
		if (po.getExchangeRate() == null) {
			throw new BusinessException(this.translator.toLocale("po.missing.exchange.rate"));
		}

		Optional<CurrencyEntity> oFromCurrency = this.currencyRepository.findById(po.getFromCurrency().getId());
		if (!oFromCurrency.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("currency.id.not.exists", po.getFromCurrency().getId()));
		}
		Optional<CurrencyEntity> oToCurrency = this.currencyRepository.findById(po.getToCurrency().getId());
		if (!oToCurrency.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("currency.id.not.exists", po.getToCurrency().getId()));
		}
		LocalDateTime sysdate = this.getSysdate();
		Page<CurrencyExchangeRateEntity> page = this.currencyExchangeRateRepository.findCurrencyExchangeRate(
				po.getFromCurrency().getId(), po.getToCurrency().getId(), DatetimeUtils.localDateTime2SqlDate(sysdate),
				null, PageRequest.of(0, 1));
		if ((page == null) || page.getContent().isEmpty()) {
			throw new BusinessException(this.translator.toLocale("po.missing.exchange.rate"));
		}
		po.setExchangeRate(page.getContent().get(0).getExchangeRate());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<PoEntity> poEntities) throws BusinessException {
		LocalDateTime sysdate = this.getSysdate();
		List<PoEntity> list = new ArrayList<>();
		for (PoEntity poEntity : poEntities) {
			this.validPoEntity(poEntity);
			String code = String.format("%s-PO-%09d", sysdate.format(DateTimeFormatter.ofPattern("yyMMdd")),
					Long.valueOf(this.getGeneratorValue("PO_CODE")));
			poEntity.setCode(code);
			poEntity.setqRCode(QRCodeGenerator.generate(code));
			poEntity.setStatus(PoStatus.NEW);
			PoEntity temp = this.poRepository.save(poEntity);
			List<PoDetailEntity> poDetails = new ArrayList<>();
			for (PoDetailEntity poDetailEntity : poEntity.getPoDetails()) {
				poDetailEntity = this.addOrEditPoDetail(temp, poDetailEntity, poDetailEntity);
				poDetails.add(poDetailEntity);
			}
			temp.setPoDetails(poDetails);
			list.add(temp);
		}
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, PoEntity source) throws BusinessException {
		Optional<PoEntity> oTarget = this.poRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator.toLocale(PoServiceImpl.PO_ID_NOT_EXISTS));
		}
		if (!PoStatus.NEW.equals(oTarget.get().getStatus())) {
			throw new BusinessException(this.translator.toLocale("po.status.check.new"));
		}
		PoEntity target = oTarget.get();
		this.validPoEntity(source);
		target.setManufacturer(source.getManufacturer());
		target.setStore(source.getStore());
		target.setDistributor(source.getDistributor());
		target.setDescription(source.getDescription());
		target.setDeliveryAddress(source.getDeliveryAddress());
		target.setFromCurrency(source.getFromCurrency());
		target.setExchangeRate(source.getExchangeRate());
		target.setToCurrency(source.getToCurrency());
		target = this.poRepository.save(target);
		List<PoDetailEntity> targetDetails = target.getPoDetails();
		// Lọc ra IDs không nằm trong list detail sau khi sửa => xóa
		List<PoDetailEntity> listDelete = targetDetails.stream()
				.filter(pod -> source.getPoDetails().stream().noneMatch(x -> pod.getId().equals(x.getId())))
				.collect(Collectors.toList());
		if (targetDetails.removeAll(listDelete)) {
			this.poDetailRepository.deleteAll(listDelete);
		}
		// Add Or Edit Details
		for (PoDetailEntity sourceDetail : source.getPoDetails()) {
			if (sourceDetail.getId() == null) {
				sourceDetail = this.addOrEditPoDetail(target, sourceDetail, sourceDetail);
				targetDetails.add(sourceDetail);
			} else {
				int index = targetDetails.stream().map(PoDetailEntity::getId).collect(Collectors.toList())
						.indexOf(sourceDetail.getId());
				if (index >= 0) {
					PoDetailEntity temp = targetDetails.get(index);
					temp = this.addOrEditPoDetail(target, temp, sourceDetail);
					targetDetails.set(index, temp);
				}
			}
		}
		target.setPoDetails(targetDetails);
		return new ResponseEntity<>(target, HttpStatus.OK);
	}

	private PoDetailEntity addOrEditPoDetail(PoEntity poEntity, PoDetailEntity poDetailEntity,
			PoDetailEntity editedPoDetail) throws BusinessException {
		poDetailEntity.setPo(poEntity);
		Optional<ProductEntity> oProduct = this.productRepository.findById(editedPoDetail.getProduct().getId());
		if (!oProduct.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.id.not.exists",
					editedPoDetail.getProduct().getId()));
		}
		poDetailEntity.setProduct(oProduct.get());
		Optional<ProductPackingEntity> oProductPacking = this.productPackingRepository
				.findById(editedPoDetail.getProductPacking().getId());
		if (!oProductPacking.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.id.not.exists",
					editedPoDetail.getProductPacking().getId()));
		}
		poDetailEntity.setProductPacking(oProductPacking.get());
		Optional<ProductPackingPriceEntity> oProductPackingPrice = this.productPackingPriceRepository
				.findById(editedPoDetail.getProductPackingPrice().getId());
		if (!oProductPackingPrice.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("product.packing.price.id.not.exists",
					editedPoDetail.getProductPackingPrice().getId()));
		}
		poDetailEntity.setProductPackingPrice(oProductPackingPrice.get());
		poDetailEntity.setVat(editedPoDetail.getVat());
		if (editedPoDetail.getQuantity() <= 0) {
			throw new BusinessException(this.translator.toLocale("po.detail.quantity.invalid"));
		}
		poDetailEntity.setQuantity(editedPoDetail.getQuantity());
		return this.poDetailRepository.save(poDetailEntity);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> approve(Long id) throws BusinessException {
		Optional<PoEntity> oPoEntity = this.poRepository.findById(id);
		try {
			if (!oPoEntity.isPresent()) {
				throw new BusinessException(this.translator.toLocaleByFormatString(PoServiceImpl.PO_ID_NOT_EXISTS, id));
			}
			if (!PoStatus.NEW.equals(oPoEntity.get().getStatus())) {
				throw new BusinessException(this.translator.toLocale("po.approve.check.status.is.new"));
			}
			PoEntity poEntity = oPoEntity.get();
			poEntity.setStatus(PoStatus.APPROVED);
			poEntity.setApproveDate(this.getSysdate());
			poEntity.setApproveUser(this.getCurrentOAuth2Details().getPrincipal().getUsername());
			poEntity = this.poRepository.save(poEntity);
			return new ResponseEntity<>(poEntity, HttpStatus.OK);
		} catch (Exception ex) {
			throw new BusinessException(ex.getMessage());
		}
	}

	private String exportExcel(Long id) throws BusinessException {
		Optional<PoEntity> oPoEntity = this.poRepository.findById(id);
		if (!oPoEntity.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString(PoServiceImpl.PO_ID_NOT_EXISTS, id));
		}
		try {
			PoEntity po = oPoEntity.get();
			Map<String, Object> map = new HashMap<>();
			map.put("code", po.getCode());
			map.put("createDate", Utils.toDateString(po.getCreateDate(), Utils.DATE_FORMAT_REPORT));
			if (po.getCreateUserObj() != null) {
				map.put("purchaser", String.format("%s %s", po.getCreateUserObj().getFirstName(),
						po.getCreateUserObj().getLastName()));
				map.put("tel", po.getCreateUserObj().getTel());
				map.put("email", po.getCreateUserObj().getEmail());
			}
			map.put("deliveryAddress", po.getDeliveryAddress());
			String approveUser = "Not approved (အတည်ပြုခြင်း )";
			if (PoStatus.APPROVED.equals(po.getStatus()) && (po.getApproveUserObj() != null)) {
				approveUser = String.format("Approved by (အတည်ပြုခြင်းမရှိ -): %s %s",
						po.getApproveUserObj().getFirstName(), po.getApproveUserObj().getLastName());
			}
			map.put("approveUser", approveUser);
			ManufacturerEntity manufacturer = po.getManufacturer();
			map.put("manufacturerCode", manufacturer.getCode());
			Optional<ManufacturerDescriptionEntity> oManufacturerDescription = manufacturer
					.getManufacturerDescriptions().stream()
					.filter(x -> "EN".equalsIgnoreCase(x.getLanguage().getCode())).findFirst();
			map.put("manufacturerName",
					oManufacturerDescription.isPresent() ? oManufacturerDescription.get().getName() : "");
			map.put("manufacturerAddress",
					oManufacturerDescription.isPresent() ? oManufacturerDescription.get().getAddress() : "");
			DistributorEntity distributor = po.getDistributor();
			map.put("distributorName", distributor.getName());
			map.put("distributorAddress", distributor.getAddress());
			map.put("distributorTin", distributor.getTin());
			map.put("currency", po.getPoDetails().get(0).getProductPackingPrice().getCurrency().getCode());

			ResultSet rsData = this.reportService.getResultSet("call printPO(" + po.getId() + ") ", null, null);
			List<PoPrintDto> details = this.reportService.resultSetToList(rsData, PoPrintDto.class);
			map.put("details", details);
			return Jxls2xTransformerUtils.poiTransformer("templates/PRINT_PO.xlsx", map,
					this.fileStorageService.getTempExportExcel(), po.getCode() + ".xlsx");
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ResponseEntity<Object> print(Long id) throws BusinessException {
		try {
			String outputFilePath = this.exportExcel(id);
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.excel.output.file.path.not.exists"));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			PoServiceImpl.logger.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<Object> printPdf(Long id) throws BusinessException {
		try {
			String excelFilePath = this.exportExcel(id);
			String pdfFilePath = ExcelTo.pdf(this.fileStorageService.getLibreOfficePath(), excelFilePath,
					this.fileStorageService.getTempExportExcel());
			File file = new File(pdfFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.pdf.output.file.path.not.exists"));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(pdfFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			PoServiceImpl.logger.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<Object> printMultiple(String acceptLanguage, List<Long> ids) throws BusinessException {
		try {
			List<PoEntity> poEntities = this.poRepository.findAllById(ids);
			if (poEntities.isEmpty() || (poEntities.size() < ids.size())) {
				throw new BusinessException(this.translator.toLocaleByFormatString(PoServiceImpl.PO_ID_NOT_EXISTS,
						org.apache.commons.lang3.StringUtils.join(ids, ',')));
			}
			poEntities.forEach(po -> {
				po.setStatusString(this.translator.toLocale(po.getStatusString()));
				ManufacturerEntity manufacturer = po.getManufacturer();
				Optional<ManufacturerDescriptionEntity> oManufacturerDescription = manufacturer
						.getManufacturerDescriptions().stream()
						.filter(md -> md.getLanguage().getCode().equalsIgnoreCase(acceptLanguage)).findFirst();
				if (oManufacturerDescription.isPresent()) {
					manufacturer.setName(oManufacturerDescription.get().getName());
				}
			});
			Map<String, Object> map = new HashMap<>();
			map.put("objData", poEntities);
			String outputFilePath = Jxls2xTransformerUtils.poiTransformer("templates/POs_DATAs.xlsx", map,
					this.fileStorageService.getTempExportExcel(), String.format("%s-%s.%s", "POs_DATAs",
							this.getSysdate().format(DateTimeFormatter.ofPattern("yyMMdd")), "xlsx"));
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			PoServiceImpl.logger.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> reject(Long id, PoEntity po) throws BusinessException {
		if ((po == null) || StringUtils.isNullOrEmpty(po.getRejectReason())) {
			throw new BusinessException(this.translator.toLocale("po.missing.reason"));
		}
		Optional<PoEntity> oPoEntity = this.poRepository.findById(id == null ? 0 : id);
		try {
			if (!oPoEntity.isPresent()) {
				throw new BusinessException(this.translator.toLocaleByFormatString(PoServiceImpl.PO_ID_NOT_EXISTS, id));
			}
			PoEntity poEntity = oPoEntity.get();
			String currentStatus = poEntity.getStatus().getValue();
			if (Arrays.asList(PoStatus.IMPORTED, PoStatus.REJECT).stream()
					.anyMatch(x -> x.getValue().equalsIgnoreCase(currentStatus))) {
				throw new BusinessException(this.translator.toLocale("po.reject.check.status"));
			}
			poEntity.setRejectReason(po.getRejectReason());
			poEntity.setStatus(PoStatus.REJECT);
			poEntity = this.poRepository.save(poEntity);
			return new ResponseEntity<>(poEntity, HttpStatus.OK);
		} catch (Exception ex) {
			throw new BusinessException(ex.getMessage());
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> updateArriveVietnamPort(Long id, PoEntity source) throws BusinessException {
		if (source.getArriveVietnamPortDate() == null) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		LocalDateTime sysdate = this.getSysdate();
		if (source.getArriveVietnamPortDate().toLocalDate().isBefore(sysdate.toLocalDate())
				|| source.getArriveVietnamPortDate().toLocalDate().isEqual(sysdate.toLocalDate())) {
			throw new BusinessException(
					this.translator.toLocale("po.input.arrive.vietnam.port.date.must.be.greater.than.sysdate"));
		}
		Optional<PoEntity> oTarget = this.poRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator.toLocale(PoServiceImpl.PO_ID_NOT_EXISTS));
		}
		PoEntity target = oTarget.get();
		if (!PoStatus.APPROVED.equals(target.getStatus())) {
			throw new BusinessException(this.translator.toLocale("po.status.check.approved"));
		}
		target.setArriveVietnamPortDate(source.getArriveVietnamPortDate());
		target.setArriveVietnamPortUser(this.getCurrentUsername());
		return new ResponseEntity<>(this.poRepository.save(target), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> updateArrivedVietnamPort(Long id, PoEntity source) throws BusinessException {
		if (source.getArrivedVietnamPortDate() == null) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		LocalDateTime sysdate = this.getSysdate();
		if (source.getArrivedVietnamPortDate().toLocalDate().isAfter(sysdate.toLocalDate())) {
			throw new BusinessException(
					this.translator.toLocale("po.input.arrived.vietnam.port.date.must.be.less.than.or.equal.sysdate"));
		}
		Optional<PoEntity> oTarget = this.poRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator.toLocale(PoServiceImpl.PO_ID_NOT_EXISTS));
		}
		PoEntity target = oTarget.get();
		if (!PoStatus.APPROVED.equals(target.getStatus())) {
			throw new BusinessException(this.translator.toLocale("po.status.check.approved"));
		}
		if (target.getArriveVietnamPortDate() == null) {
			throw new BusinessException(this.translator.toLocale("po.input.missing.arrive.vietnam.port.date"));
		}
		if (!source.getArrivedVietnamPortDate().toLocalDate().isAfter(target.getApproveDate().toLocalDate())) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		target.setArrivedVietnamPortDate(source.getArrivedVietnamPortDate());
		target.setArrivedVietnamPortUser(this.getCurrentUsername());
		target.setStatus(PoStatus.ARRIVED_VIETNAM_PORT);
		return new ResponseEntity<>(this.poRepository.save(target), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> updateArriveMyanmarPort(Long id, PoEntity source) throws BusinessException {
		if (source.getArriveMyanmarPortDate() == null) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		LocalDateTime sysdate = this.getSysdate();
		if (source.getArriveMyanmarPortDate().toLocalDate().isBefore(sysdate.toLocalDate())
				|| source.getArriveMyanmarPortDate().toLocalDate().isEqual(sysdate.toLocalDate())) {
			throw new BusinessException(
					this.translator.toLocale("po.input.arrive.myanmar.port.date.must.be.greater.than.sysdate"));
		}
		Optional<PoEntity> oTarget = this.poRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator.toLocale(PoServiceImpl.PO_ID_NOT_EXISTS));
		}
		PoEntity target = oTarget.get();
		if (!PoStatus.APPROVED.equals(target.getStatus())
				&& !PoStatus.ARRIVED_VIETNAM_PORT.equals(target.getStatus())) {
			throw new BusinessException(this.translator.toLocale("po.status.check.approved.arrived.vietnam.port"));
		}
		target.setArriveMyanmarPortDate(source.getArriveMyanmarPortDate());
		target.setArriveMyanmarPortUser(this.getCurrentUsername());
		return new ResponseEntity<>(this.poRepository.save(target), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> updateArrivedMyanmarPort(Long id, PoEntity source) throws BusinessException {
		if (source.getArrivedMyanmarPortDate() == null) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		LocalDateTime sysdate = this.getSysdate();
		if (source.getArrivedMyanmarPortDate().toLocalDate().isAfter(sysdate.toLocalDate())) {
			throw new BusinessException(
					this.translator.toLocale("po.input.arrived.myanmar.port.date.must.be.less.than.or.equal.sysdate"));
		}
		Optional<PoEntity> oTarget = this.poRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator.toLocale(PoServiceImpl.PO_ID_NOT_EXISTS));
		}
		PoEntity target = oTarget.get();
		if (!PoStatus.APPROVED.equals(target.getStatus())
				&& !PoStatus.ARRIVED_VIETNAM_PORT.equals(target.getStatus())) {
			throw new BusinessException(this.translator.toLocale("po.status.check.approved.arrived.vietnam.port"));
		}
		if (!source.getArrivedMyanmarPortDate().toLocalDate()
				.isAfter(target.getArrivedVietnamPortDate().toLocalDate())) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		target.setArrivedMyanmarPortDate(source.getArrivedMyanmarPortDate());
		target.setArrivedMyanmarPortUser(this.getCurrentUsername());
		target.setStatus(PoStatus.ARRIVED_MYANMAR_PORT);
		return new ResponseEntity<>(this.poRepository.save(target), HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> updateArriveFc(Long id, PoEntity source) throws BusinessException {
		if (source.getArriveFcDate() == null) {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
		LocalDateTime sysdate = this.getSysdate();
		if (source.getArriveFcDate().toLocalDate().isBefore(sysdate.toLocalDate())
				|| source.getArriveFcDate().toLocalDate().isEqual(sysdate.toLocalDate())) {
			throw new BusinessException(
					this.translator.toLocale("po.input.arrive.fc.date.must.be.greater.than.sysdate"));
		}
		Optional<PoEntity> oTarget = this.poRepository.findById(id);
		if (!oTarget.isPresent()) {
			throw new BusinessException(this.translator.toLocale(PoServiceImpl.PO_ID_NOT_EXISTS));
		}
		PoEntity target = oTarget.get();
		if (!PoStatus.ARRIVED_VIETNAM_PORT.equals(target.getStatus())
				&& !PoStatus.ARRIVED_MYANMAR_PORT.equals(target.getStatus())) {
			throw new BusinessException(
					this.translator.toLocale("po.status.check.arrived.vietnam.port.arrived.myanmar.port"));
		}
		target.setArriveFcDate(source.getArriveFcDate());
		target.setArriveFcUser(this.getCurrentUsername());
		return new ResponseEntity<>(this.poRepository.save(target), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> downloadTemplate(Long manufacturerId, Long distributorId, List<Long> exceptId)
			throws BusinessException {
		try {
			Page<ProductPackingEntity> page = this.productPackingRepository.find(null, exceptId, manufacturerId,
					distributorId, PageRequest.of(0, Integer.MAX_VALUE));
			Map<String, List<ProductPackingEntity>> mapDetail = new HashMap<>();
			mapDetail.put("Product", page.getContent());

			Map<String, Object> map = new HashMap<>();
			map.put("mapDetail", mapDetail);
			map.put("sheetNames", new ArrayList<>(mapDetail.keySet()));
			String outputFilePath = Jxls2xTransformerUtils.poiTransformer("templates/PREPARE_PO_DETAIL.xlsx", map,
					this.fileStorageService.getTempExportExcel(),
					String.format("%s-%s.%s", "PREPARE_PO_DETAIL",
							this.getSysdate().format(
									DateTimeFormatter.ofPattern(Constants.PREFIX_EXPORT_EXCEL_FILE_NAME)),
							"xlsx"),
					"Template", "Data");
			File file = new File(outputFilePath);
			if (!file.exists()) {
				throw new BusinessException(this.translator.toLocale("export.excel.output.file.path.not.exists"));
			}
			Resource resource = this.fileStorageService.loadFileAsResource(outputFilePath);
			HttpHeaders headers = this.fileStorageService.loadHttpHeaders(resource);
			return new ResponseEntity<>(resource, headers, HttpStatus.OK);
		} catch (Exception e) {
			PoServiceImpl.logger.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
