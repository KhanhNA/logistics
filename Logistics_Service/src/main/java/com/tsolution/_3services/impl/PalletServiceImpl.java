package com.tsolution._3services.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution._1entities.enums.ExpireDateColors;
import com.tsolution._1entities.enums.PalletStep;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._2repositories.PalletDetailRepository;
import com.tsolution._2repositories.PalletRepository;
import com.tsolution._3services.PalletService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.QRCodeGenerator;
import com.tsolution.utils.StringUtils;

@Service
public class PalletServiceImpl extends LogisticsBaseService implements PalletService {

	@Autowired
	PalletRepository palletRepository;

	@Autowired
	PalletDetailRepository palletDetailRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.palletRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<PalletEntity> palletEntity = this.palletRepository.findById(id);
		if (!palletEntity.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("pallet.id.not.exists", id));
		}
		return new ResponseEntity<>(palletEntity, HttpStatus.OK);
	}

	private void validPalletInfo(PalletEntity pallet) throws BusinessException {
		if ((pallet.getStore() == null) || (pallet.getStore().getId() == null)) {
			throw new BusinessException(this.translator.toLocale("pallet.input.missing.store"));
		}
		Optional<StoreEntity> oStore = this.storeRepository.findById(pallet.getStore().getId());
		if (!oStore.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("store.id.not.exists", pallet.getStore().getId()));
		}
		if (oStore.get().isDc()) {
			throw new BusinessException(this.translator.toLocale("pallet.input.store.invalid"));
		}
		if (StringUtils.isNullOrEmpty(pallet.getCode())) {
			throw new BusinessException(this.translator.toLocale("pallet.input.missing.code"));
		}
		if (StringUtils.isNullOrEmpty(pallet.getName())) {
			throw new BusinessException(this.translator.toLocale("pallet.input.missing.name"));
		}
		if ((pallet.getStep() == null) || !PalletStep.isContain(pallet.getStep())) {
			throw new BusinessException(this.translator.toLocale("pallet.input.missing.step"));
		}
		if ((pallet.getLength() == null) || (pallet.getLength() <= 0)) {
			throw new BusinessException(this.translator.toLocale("pallet.input.missing.length"));
		}
		if ((pallet.getWidth() == null) || (pallet.getWidth() <= 0)) {
			throw new BusinessException(this.translator.toLocale("pallet.input.missing.width"));
		}
		if ((pallet.getHeight() == null) || (pallet.getHeight() <= 0)) {
			throw new BusinessException(this.translator.toLocale("pallet.input.missing.height"));
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<PalletEntity> entities) throws BusinessException {
		List<PalletEntity> pallets = new ArrayList<>();
		for (PalletEntity pallet : entities) {
			if (pallet.getId() != null) {
				throw new BusinessException(this.translator.toLocale("pallet.input.cannot.contain.id"));
			}
			do {
				String code = String.format("Pallet%05d", Long.valueOf(this.getGeneratorValue("PALLET_CODE")));
				Optional<PalletEntity> oPallet = this.palletRepository.findByCode(code);
				if (!oPallet.isPresent()) {
					pallet.setCode(code);
					pallet.setqRCode(QRCodeGenerator.generate(code));
				}
			} while (StringUtils.isNullOrEmpty(pallet.getCode()));
			this.validPalletInfo(pallet);
			Double volume = pallet.getLength() * pallet.getWidth() * pallet.getHeight();
			pallet.setFreeVolume(volume);
			pallet.setTotalVolume(volume);
			pallets.add(this.palletRepository.save(pallet));
		}
		return new ResponseEntity<>(pallets, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, PalletEntity source) throws BusinessException {
		Optional<PalletEntity> palletEntity = this.palletRepository.findById(id);
		if (!palletEntity.isPresent() || !id.equals(source.getId())) {
			throw new BusinessException(this.translator.toLocaleByFormatString("pallet.id.not.exists", id));
		}
		this.validPalletInfo(source);
		Optional<PalletEntity> oPallet = this.palletRepository.findByCode(source.getCode());
		if (oPallet.isPresent() && !id.equals(oPallet.get().getId())) {
			throw new BusinessException(this.translator.toLocaleByFormatString("pallet.code.exists", source.getCode()));
		}
		List<PalletDetailEntity> palletDetails = palletEntity.get().getPalletDetails();
		if (Boolean.FALSE.equals(source.getStatus()) && (palletDetails != null) && !palletDetails.isEmpty()) {
			throw new BusinessException(this.translator.toLocale("pallet.cannot.deactive"));
		}
		Double volume = source.getLength() * source.getWidth() * source.getHeight();
		Double subVolume = palletEntity.get().getTotalVolume() - volume;
		if ((palletEntity.get().getFreeVolume() - subVolume) < 0) {
			throw new BusinessException(this.translator.toLocale("pallet.input.volume.invalid"));
		}
		source.setFreeVolume(palletEntity.get().getFreeVolume() - subVolume);
		source.setTotalVolume(palletEntity.get().getTotalVolume() - subVolume);
		return new ResponseEntity<>(this.palletRepository.save(source), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(PalletDetailDto palletDetailDto, String acceptLanguage, Integer pageNumber,
			Integer pageSize) throws BusinessException {
		return new ResponseEntity<>(
				this.palletRepository.find(palletDetailDto, acceptLanguage, PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}

	@Override
	public void pushUpPalletDetail(Long palletDetailId, Long quantity) throws BusinessException {
		Optional<PalletDetailEntity> oPalletDetail = this.palletDetailRepository.findById(palletDetailId);
		if (!oPalletDetail.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("pallet.detail.id.not.exists", palletDetailId));
		}
		PalletDetailEntity palletDetail = oPalletDetail.get();
		if (palletDetail.getOrgQuantity() == 0L) {
			palletDetail.setOrgQuantity(quantity);
		} else if ((quantity + palletDetail.getQuantity()) > palletDetail.getOrgQuantity()) {
			throw new BusinessException(this.translator.toLocale("pallet.detail.quantity.invalid"));
		}
		palletDetail.setQuantity(quantity + palletDetail.getQuantity());
		palletDetail = this.palletDetailRepository.save(palletDetail);

		PalletEntity pallet = palletDetail.getPallet();
		pallet.setFreeVolume(pallet.getFreeVolume() - (palletDetail.getProductPacking().getVolume() * quantity));
		if (pallet.getFreeVolume() < 0) {
			throw new BusinessException(this.translator.toLocaleByFormatString("pallet.over.volume", pallet.getCode()));
		}
		this.palletRepository.save(pallet);
	}

	@Override
	public void pullDownPalletDetail(Long palletDetailId, Long quantity) throws BusinessException {
		Optional<PalletDetailEntity> oPalletDetail = this.palletDetailRepository.findById(palletDetailId);
		if (!oPalletDetail.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString("pallet.detail.id.not.exists", palletDetailId));
		}
		PalletDetailEntity palletDetail = oPalletDetail.get();
		if (quantity > palletDetail.getQuantity()) {
			throw new BusinessException(this.translator.toLocale("pallet.detail.quantity.invalid"));
		}
		palletDetail.setQuantity(palletDetail.getQuantity() - quantity);
		this.palletDetailRepository.save(palletDetail);

		PalletEntity pallet = palletDetail.getPallet();
		pallet.setFreeVolume(pallet.getFreeVolume() + (palletDetail.getProductPacking().getVolume() * quantity));
		this.palletRepository.save(pallet);
	}

	@Override
	public ResponseEntity<Object> findProductPackingGroupByExpireDate(PalletDetailDto palletDetailDto,
			MultiValueMap<String, String> productPackingIdExpireDate, Long repackingPlanningId, Integer pageNumber,
			Integer pageSize) throws BusinessException {
		palletDetailDto.setUsername(this.getCurrentUsername());
		Page<PalletDetailEntity> palletDetails = this.palletDetailRepository.findGroupByProductPackingAndExpireDate(
				palletDetailDto, repackingPlanningId, PageRequest.of(pageNumber - 1, pageSize));
		LocalDateTime sysdate = this.getSysdate();
		palletDetails.forEach(palletDetail -> palletDetail.setClassName(ExpireDateColors.findClassName(sysdate,
				palletDetail.getExpireDate(), palletDetail.getProductPacking())));
		return new ResponseEntity<>(palletDetails, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findGroupByPalletStepAndProductPackingAndExpireDate(PalletDetailDto palletDetailDto,
			Integer pageNumber, Integer pageSize) throws BusinessException {
		palletDetailDto.setUsername(this.getCurrentUsername());
		return new ResponseEntity<>(
				this.palletDetailRepository.findGroupByPalletStepAndProductPackingAndExpireDate(palletDetailDto,
						this.getManagedStoreIncludeProvideStores(), PageRequest.of(pageNumber - 1, pageSize)),
				HttpStatus.OK);
	}
}
