package com.tsolution._3services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.ProductEntity;
import com.tsolution.excetions.BusinessException;

public interface IProductService extends IBaseService<ProductEntity> {
	ResponseEntity<Object> find(String text, List<Long> exceptId, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> getPackingType(Boolean status);
}
