package com.tsolution._3services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.UserEntity;
import com.tsolution.excetions.BusinessException;

public interface UserService {

	ResponseEntity<Object> findAll();

	ResponseEntity<Object> findById(String authorization, String acceptLanguage, String username)
			throws BusinessException;

	ResponseEntity<Object> find(Boolean status, String text, List<Long> storeIds, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> create(String authorization, String acceptLanguage, UserEntity entities)
			throws BusinessException;

	ResponseEntity<Object> update(String authorization, String acceptLanguage, String username, UserEntity source)
			throws BusinessException;

	ResponseEntity<Object> active(String authorization, String acceptLanguage, UserEntity user)
			throws BusinessException;

	ResponseEntity<Object> deactive(String authorization, String acceptLanguage, UserEntity user)
			throws BusinessException;

	ResponseEntity<Object> resetPassword(String authorization, String acceptLanguage, UserEntity user)
			throws BusinessException;
}
