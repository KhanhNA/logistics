package com.tsolution._3services;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.MerchantEntity;

public interface IMerchantService extends IBaseService<MerchantEntity> {
	ResponseEntity<Object> find(Boolean status);
}
