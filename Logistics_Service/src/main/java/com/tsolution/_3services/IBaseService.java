package com.tsolution._3services;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution.excetions.BusinessException;

public interface IBaseService<T extends Serializable> {
	public ResponseEntity<Object> findAll() throws BusinessException;

	public ResponseEntity<Object> findById(Long id) throws BusinessException;

	public ResponseEntity<Object> create(List<T> entities) throws BusinessException;

	public ResponseEntity<Object> update(Long id, T source) throws BusinessException;

}
