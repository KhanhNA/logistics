package com.tsolution._3services;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution.excetions.BusinessException;

public interface PalletService extends IBaseService<PalletEntity> {
	ResponseEntity<Object> find(PalletDetailDto palletDetailDto, String acceptLanguage, Integer pageNumber,
			Integer pageSize) throws BusinessException;

	void pushUpPalletDetail(Long palletDetailId, Long quantity) throws BusinessException;

	void pullDownPalletDetail(Long palletDetailId, Long quantity) throws BusinessException;

	ResponseEntity<Object> findProductPackingGroupByExpireDate(PalletDetailDto palletDetailDto,
			MultiValueMap<String, String> productPackingIdExpireDate, Long repackingPlanningId, Integer pageNumber,
			Integer pageSize) throws BusinessException;

	ResponseEntity<Object> findGroupByPalletStepAndProductPackingAndExpireDate(PalletDetailDto palletDetailDto,
			Integer pageNumber, Integer pageSize) throws BusinessException;

}
