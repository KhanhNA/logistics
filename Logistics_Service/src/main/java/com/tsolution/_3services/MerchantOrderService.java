package com.tsolution._3services;

import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.tsolution._1entities.dcommerce.MerchantOrderInventoryDto;
import com.tsolution._1entities.merchant_order.dto.InputOrderDto;
import com.tsolution.excetions.BusinessException;

public interface MerchantOrderService extends IBaseService<InputOrderDto> {

	ResponseEntity<Object> findByCode(String code) throws BusinessException;

	ResponseEntity<Object> search(String text, Date fromDate, Date toDate, Integer status, Long storeId,
			Integer pageNumber, Integer pageSize) throws BusinessException;

	ResponseEntity<Object> getInventoryByMerchantOrder(List<String> storeCodes, List<String> productPackingCodes)
			throws BusinessException;

	ResponseEntity<Object> getInventoryByMerchantOrder(@RequestBody MerchantOrderInventoryDto merchantOrderInventory)
			throws BusinessException;

	ResponseEntity<Object> cancel(String code) throws BusinessException;

	ResponseEntity<Object> getStoresExistsNotEnoughQuantity(Integer pageNumber, Integer pageSize)
			throws BusinessException;

	ResponseEntity<Object> details(List<Long> merchantOrderIds) throws BusinessException;
}
