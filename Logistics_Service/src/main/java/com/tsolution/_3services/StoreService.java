package com.tsolution._3services;

import java.io.IOException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.dto.AvailableQuantityFilterDto;
import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.dto.StoreDto;
import com.tsolution.excetions.BusinessException;

public interface StoreService extends IBaseService<StoreEntity> {

	ResponseEntity<Object> create(StoreEntity store, MultipartFile[] files) throws BusinessException;

	ResponseEntity<Object> update(Long id, StoreEntity storeEntity, MultipartFile[] files, List<Long> removeFileIds)
			throws BusinessException;

	ResponseEntity<Object> activate(Long id) throws BusinessException;

	ResponseEntity<Object> deactivate(Long id) throws BusinessException;

	ResponseEntity<Object> find(StoreDto storeDto, Boolean ignoreCheckPermission, Integer pageNumber, Integer pageSize)
			throws IOException, BusinessException;

	ResponseEntity<Object> getStoresByProvideStore(List<Long> storeIds);

	ResponseEntity<Object> nearHere(Float currentLat, Float currentLng, Integer maxDistance, String productPackingCode,
			Boolean isDC, Integer pageNumber, Integer pageSize) throws BusinessException;

	ResponseEntity<Object> inventory(InventoryDto inventoryDto, Integer pageNumber, Integer pageSize)
			throws BusinessException;

	public ResponseEntity<Object> inventoryIgnoreExpireDate(InventoryDto inventoryDto, Long exportStatementId,
			Integer pageNumber, Integer pageSize) throws BusinessException;

	ResponseEntity<Object> inventoryPalletDetail(Long storeId, Long productPackingId, java.sql.Date expireDate)
			throws BusinessException;

	ResponseEntity<Object> getStoreUsers(Long id) throws BusinessException;

	ResponseEntity<Object> getAvailableQuantity(String townshipCode, List<String> productPackingCodes)
			throws BusinessException;

	ResponseEntity<Object> getStoreByLocationCharge(String townshipCode) throws BusinessException;

	ResponseEntity<Object> getInventory(AvailableQuantityFilterDto availableQuantityFilterDto) throws BusinessException;

	ResponseEntity<Object> findTownship(List<String> townshipCodes, Integer pageNumber, Integer pageSize)
			throws BusinessException;

}
