package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@Entity
@Table(name = "repacking_planning_detail")
public class RepackingPlanningDetailEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1385625909304384446L;

	@ManyToOne
	@JoinColumn(name = "repacking_planning_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private RepackingPlanningEntity repackingPlanning;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingEntity productPacking;

	@Column(name = "expire_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime expireDate;

	@Column(name = "quantity", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long quantity;

	@OneToMany(mappedBy = "repackingPlanningDetail", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<RepackingPlanningDetailPalletEntity> repackingPlanningDetailPallets;

	@OneToMany(mappedBy = "repackingPlanningDetail", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<RepackingPlanningDetailRepackedEntity> repackingPlanningDetailRepackeds;

	public RepackingPlanningDetailEntity() {
	}

	public RepackingPlanningDetailEntity(Long productPackingId, LocalDateTime expireDate) {
		this.productPacking = new ProductPackingEntity(productPackingId);
		this.expireDate = expireDate;
	}

	public RepackingPlanningEntity getRepackingPlanning() {
		return this.repackingPlanning;
	}

	public void setRepackingPlanning(RepackingPlanningEntity repackingPlanning) {
		this.repackingPlanning = repackingPlanning;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public List<RepackingPlanningDetailPalletEntity> getRepackingPlanningDetailPallets() {
		return this.repackingPlanningDetailPallets;
	}

	public void setRepackingPlanningDetailPallets(
			List<RepackingPlanningDetailPalletEntity> repackingPlanningDetailPallets) {
		this.repackingPlanningDetailPallets = repackingPlanningDetailPallets;
	}

	public List<RepackingPlanningDetailRepackedEntity> getRepackingPlanningDetailRepackeds() {
		return this.repackingPlanningDetailRepackeds;
	}

	public void setRepackingPlanningDetailRepackeds(
			List<RepackingPlanningDetailRepackedEntity> repackingPlanningDetailRepackeds) {
		this.repackingPlanningDetailRepackeds = repackingPlanningDetailRepackeds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.expireDate == null) ? 0 : this.expireDate.hashCode());
		result = (prime * result) + ((this.productPacking == null) ? 0 : this.productPacking.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		RepackingPlanningDetailEntity other = (RepackingPlanningDetailEntity) obj;
		if (this.expireDate == null) {
			if (other.expireDate != null) {
				return false;
			}
		} else if (!this.expireDate.equals(other.expireDate)) {
			return false;
		}
		if (this.productPacking == null) {
			if (other.productPacking != null) {
				return false;
			}
		} else if (!this.productPacking.equals(other.productPacking)) {
			return false;
		}
		return true;
	}

}