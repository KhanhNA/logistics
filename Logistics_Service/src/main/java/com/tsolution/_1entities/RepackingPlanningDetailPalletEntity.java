package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@Entity
@Table(name = "repacking_planning_detail_pallet")
public class RepackingPlanningDetailPalletEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1023191699122496210L;

	@ManyToOne
	@JoinColumn(name = "repacking_planning_detail_id")
	private RepackingPlanningDetailEntity repackingPlanningDetail;

	@ManyToOne
	@JoinColumn(name = "pallet_detail_id")
	private PalletDetailEntity palletDetail;

	@Column(name = "quantity", nullable = false)
	private Long quantity;

	public RepackingPlanningDetailEntity getRepackingPlanningDetail() {
		return this.repackingPlanningDetail;
	}

	public void setRepackingPlanningDetail(RepackingPlanningDetailEntity repackingPlanningDetail) {
		this.repackingPlanningDetail = repackingPlanningDetail;
	}

	public PalletDetailEntity getPalletDetail() {
		return this.palletDetail;
	}

	public void setPalletDetail(PalletDetailEntity palletDetail) {
		this.palletDetail = palletDetail;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

}