package com.tsolution._1entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.enums.PoStatus;
import com.tsolution._1entities.importpo.ImportPoEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.tsolution.utils.StringUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "po")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class PoEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2217347785913030055L;

	@ManyToOne
	@JoinColumn(name = "manufacturer_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ManufacturerEntity manufacturer;

	@ManyToOne
	@JoinColumn(name = "distributor_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private DistributorEntity distributor;

	@Column(name = "code")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "QR_code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private String qRCode;

	@ManyToOne
	@JoinColumn(name = "store_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity store;

	@Column(name = "description")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String description;

	@Column(name = "delivery_address")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String deliveryAddress;

	/**
	 * 0: Mới lập PO, 1: Đã lập phiếu nhập kho FC từ PO
	 */
	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private PoStatus status;

	@ManyToOne
	@JoinColumn(name = "from_currency_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CurrencyEntity fromCurrency;

	@Column(name = "exchange_rate", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal exchangeRate;

	@ManyToOne
	@JoinColumn(name = "to_currency_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CurrencyEntity toCurrency;

	@Column(name = "reject_reason")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String rejectReason;

	@Column(name = "approve_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime approveDate;

	@Column(name = "approve_user")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String approveUser;

	@Column(name = "arrive_vietnam_port_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime arriveVietnamPortDate;

	@Column(name = "arrive_vietnam_port_user")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String arriveVietnamPortUser;

	@Column(name = "arrived_vietnam_port_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime arrivedVietnamPortDate;

	@Column(name = "arrived_vietnam_port_user")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String arrivedVietnamPortUser;

	@Column(name = "arrive_myanmar_port_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime arriveMyanmarPortDate;

	@Column(name = "arrive_myanmar_port_user")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String arriveMyanmarPortUser;

	@Column(name = "arrived_myanmar_port_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime arrivedMyanmarPortDate;

	@Column(name = "arrived_myanmar_port_user")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String arrivedMyanmarPortUser;

	@Column(name = "arrive_fc_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime arriveFcDate;

	@Column(name = "arrive_fc_user")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String arriveFcUser;

	@Column(name = "import_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime importDate;

	@Column(name = "import_user")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String importUser;

	@OneToMany(mappedBy = "po", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<PoDetailEntity> poDetails;

	@OneToOne(mappedBy = "po")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ImportPoEntity importPo;

	@Transient
	@JsonView(JsonEntityViewer.Human.Custom.ListPo.class)
	private BigDecimal total;

	@Transient
	private String statusString;

	@Transient
	private String createDateString;

	@Transient
	private String arrivedVietnamPortDateString;

	@Transient
	private String arrivedMyanmarPortDateString;

	@Transient
	private String arrivedFcDateString;

	@ManyToOne
	@JoinColumn(name = "create_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity createUserObj;

	@ManyToOne
	@JoinColumn(name = "approve_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity approveUserObj;

	@ManyToOne
	@JoinColumn(name = "arrive_vietnam_port_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity arriveVietnamPortUserObj;

	@ManyToOne
	@JoinColumn(name = "arrived_vietnam_port_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity arrivedVietnamPortUserObj;

	@ManyToOne
	@JoinColumn(name = "arrive_myanmar_port_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity arriveMyanmarPortUserObj;

	@ManyToOne
	@JoinColumn(name = "arrived_myanmar_port_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity arrivedMyanmarPortUserObj;

	@ManyToOne
	@JoinColumn(name = "arrive_fc_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity arriveFcUserObj;

	@ManyToOne
	@JoinColumn(name = "import_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity importUserObj;

	public ManufacturerEntity getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(ManufacturerEntity manufacturer) {
		this.manufacturer = manufacturer;
	}

	public DistributorEntity getDistributor() {
		return this.distributor;
	}

	public void setDistributor(DistributorEntity distributor) {
		this.distributor = distributor;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public StoreEntity getStore() {
		return this.store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeliveryAddress() {
		return this.deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public PoStatus getStatus() {
		return this.status;
	}

	public void setStatus(PoStatus status) {
		this.status = status;
	}

	public CurrencyEntity getFromCurrency() {
		return this.fromCurrency;
	}

	public void setFromCurrency(CurrencyEntity fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public CurrencyEntity getToCurrency() {
		return this.toCurrency;
	}

	public void setToCurrency(CurrencyEntity toCurrency) {
		this.toCurrency = toCurrency;
	}

	public String getRejectReason() {
		return this.rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public LocalDateTime getApproveDate() {
		return this.approveDate;
	}

	public void setApproveDate(LocalDateTime approveDate) {
		this.approveDate = approveDate;
	}

	public String getApproveUser() {
		return this.approveUser;
	}

	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}

	public LocalDateTime getArriveVietnamPortDate() {
		return this.arriveVietnamPortDate;
	}

	public void setArriveVietnamPortDate(LocalDateTime arriveVietnamPortDate) {
		this.arriveVietnamPortDate = arriveVietnamPortDate;
	}

	public String getArriveVietnamPortUser() {
		return this.arriveVietnamPortUser;
	}

	public void setArriveVietnamPortUser(String arriveVietnamPortUser) {
		this.arriveVietnamPortUser = arriveVietnamPortUser;
	}

	public LocalDateTime getArrivedVietnamPortDate() {
		return this.arrivedVietnamPortDate;
	}

	public void setArrivedVietnamPortDate(LocalDateTime arrivedVietnamPortDate) {
		this.arrivedVietnamPortDate = arrivedVietnamPortDate;
	}

	public String getArrivedVietnamPortUser() {
		return this.arrivedVietnamPortUser;
	}

	public void setArrivedVietnamPortUser(String arrivedVietnamPortUser) {
		this.arrivedVietnamPortUser = arrivedVietnamPortUser;
	}

	public LocalDateTime getArriveMyanmarPortDate() {
		return this.arriveMyanmarPortDate;
	}

	public void setArriveMyanmarPortDate(LocalDateTime arriveMyanmarPortDate) {
		this.arriveMyanmarPortDate = arriveMyanmarPortDate;
	}

	public String getArriveMyanmarPortUser() {
		return this.arriveMyanmarPortUser;
	}

	public void setArriveMyanmarPortUser(String arriveMyanmarPortUser) {
		this.arriveMyanmarPortUser = arriveMyanmarPortUser;
	}

	public LocalDateTime getArrivedMyanmarPortDate() {
		return this.arrivedMyanmarPortDate;
	}

	public void setArrivedMyanmarPortDate(LocalDateTime arrivedMyanmarPortDate) {
		this.arrivedMyanmarPortDate = arrivedMyanmarPortDate;
	}

	public String getArrivedMyanmarPortUser() {
		return this.arrivedMyanmarPortUser;
	}

	public void setArrivedMyanmarPortUser(String arrivedMyanmarPortUser) {
		this.arrivedMyanmarPortUser = arrivedMyanmarPortUser;
	}

	public LocalDateTime getArriveFcDate() {
		return this.arriveFcDate;
	}

	public void setArriveFcDate(LocalDateTime arriveFcDate) {
		this.arriveFcDate = arriveFcDate;
	}

	public String getArriveFcUser() {
		return this.arriveFcUser;
	}

	public void setArriveFcUser(String arriveFcUser) {
		this.arriveFcUser = arriveFcUser;
	}

	public LocalDateTime getImportDate() {
		return this.importDate;
	}

	public void setImportDate(LocalDateTime importDate) {
		this.importDate = importDate;
	}

	public String getImportUser() {
		return this.importUser;
	}

	public void setImportUser(String importUser) {
		this.importUser = importUser;
	}

	public List<PoDetailEntity> getPoDetails() {
		return this.poDetails;
	}

	public void setPoDetails(List<PoDetailEntity> poDetails) {
		this.poDetails = poDetails;
	}

	public ImportPoEntity getImportPo() {
		return this.importPo;
	}

	public void setImportPo(ImportPoEntity importPo) {
		this.importPo = importPo;
	}

	public BigDecimal getTotal() {
		this.total = new BigDecimal(0);
		for (PoDetailEntity poDetailEntity : this.poDetails) {
			this.total = this.total.add(new BigDecimal(poDetailEntity.getQuantity())
					.multiply(poDetailEntity.getProductPackingPrice().getPrice())
					.multiply(new BigDecimal(100 + poDetailEntity.getVat())).divide(new BigDecimal(100)));
		}
		return this.total;
	}

	public String getStatusString() {
		if (!StringUtils.isNullOrEmpty(this.statusString)) {
			return this.statusString;
		}
		switch (this.status) {
		case REJECT:
			return "po.status.reject";
		case NEW:
			return "po.status.new";
		case APPROVED:
			return "po.status.approved";
		case IMPORTED:
			return "po.status.imported";
		case ARRIVED_VIETNAM_PORT:
			return "po.status.arrived.vietnam.port";
		case ARRIVED_MYANMAR_PORT:
			return "po.status.arrived.myanmar.port";
		default:
			return "common.unknown";
		}
	}

	public void setStatusString(String statusString) {
		this.statusString = statusString;
	}

	public UserEntity getCreateUserObj() {
		return this.createUserObj;
	}

	public void setCreateUserObj(UserEntity createUserObj) {
		this.createUserObj = createUserObj;
	}

	public UserEntity getApproveUserObj() {
		return this.approveUserObj;
	}

	public void setApproveUserObj(UserEntity approveUserObj) {
		this.approveUserObj = approveUserObj;
	}

	public UserEntity getArriveVietnamPortUserObj() {
		return this.arriveVietnamPortUserObj;
	}

	public void setArriveVietnamPortUserObj(UserEntity arriveVietnamPortUserObj) {
		this.arriveVietnamPortUserObj = arriveVietnamPortUserObj;
	}

	public UserEntity getArrivedVietnamPortUserObj() {
		return this.arrivedVietnamPortUserObj;
	}

	public void setArrivedVietnamPortUserObj(UserEntity arrivedVietnamPortUserObj) {
		this.arrivedVietnamPortUserObj = arrivedVietnamPortUserObj;
	}

	public UserEntity getArriveMyanmarPortUserObj() {
		return this.arriveMyanmarPortUserObj;
	}

	public void setArriveMyanmarPortUserObj(UserEntity arriveMyanmarPortUserObj) {
		this.arriveMyanmarPortUserObj = arriveMyanmarPortUserObj;
	}

	public UserEntity getArrivedMyanmarPortUserObj() {
		return this.arrivedMyanmarPortUserObj;
	}

	public void setArrivedMyanmarPortUserObj(UserEntity arrivedMyanmarPortUserObj) {
		this.arrivedMyanmarPortUserObj = arrivedMyanmarPortUserObj;
	}

	public UserEntity getArriveFcUserObj() {
		return this.arriveFcUserObj;
	}

	public void setArriveFcUserObj(UserEntity arriveFcUserObj) {
		this.arriveFcUserObj = arriveFcUserObj;
	}

	public UserEntity getImportUserObj() {
		return this.importUserObj;
	}

	public void setImportUserObj(UserEntity importUserObj) {
		this.importUserObj = importUserObj;
	}

	public String getCreateDateString() {
		return this.getCreateDate() == null ? ""
				: this.getCreateDate().format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN));
	}

	public String getArriveVietnamPortDateString() {
		return this.getArriveVietnamPortDate() == null ? ""
				: this.getArriveVietnamPortDate().format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN));
	}

	public String getArrivedVietnamPortDateString() {
		return this.getArrivedVietnamPortDate() == null ? ""
				: this.getArrivedVietnamPortDate().format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN));
	}

	public String getArriveMyanmarPortDateString() {
		return this.getArriveMyanmarPortDate() == null ? ""
				: this.getArriveMyanmarPortDate().format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN));
	}

	public String getArrivedMyanmarPortDateString() {
		return this.getArrivedMyanmarPortDate() == null ? ""
				: this.getArrivedMyanmarPortDate().format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN));
	}

	public String getArriveFcDateString() {
		return this.getArriveFcDate() == null ? ""
				: this.getArriveFcDate().format(DateTimeFormatter.ofPattern(Constants.REPORT_DATE_PATTERN));
	}

}