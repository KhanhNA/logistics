package com.tsolution._1entities.reportdto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InventoryAndSellOutReportDto {

	@Id
	private Long id;

	@Column(name = "storeName")
	private String storeName;

	@Column(name = "storeCode")
	private String storeCode;

	@Column(name = "sku")
	private String sku;

	@Column(name = "barcode")
	private String barcode;

	@Column(name = "productName")
	private String productName;

	@Column(name = "packSize")
	private Integer packSize;

	@Column(name = "packCode")
	private String packCode;

	@Column(name = "uom")
	private String uom;

	@Column(name = "inventory")
	private Long inventory;

	@Column(name = "sellQuantity")
	private Long sellQuantity;

	@Column(name = "deliveryFrequency")
	private Long deliveryFrequency;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStoreName() {
		return this.storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreCode() {
		return this.storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getSku() {
		return this.sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getPackSize() {
		return this.packSize;
	}

	public void setPackSize(Integer packSize) {
		this.packSize = packSize;
	}

	public String getPackCode() {
		return this.packCode;
	}

	public void setPackCode(String packCode) {
		this.packCode = packCode;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Long getInventory() {
		return this.inventory;
	}

	public void setInventory(Long inventory) {
		this.inventory = inventory;
	}

	public Long getSellQuantity() {
		return this.sellQuantity;
	}

	public void setSellQuantity(Long sellQuantity) {
		this.sellQuantity = sellQuantity;
	}

	public Long getDeliveryFrequency() {
		return this.deliveryFrequency;
	}

	public void setDeliveryFrequency(Long deliveryFrequency) {
		this.deliveryFrequency = deliveryFrequency;
	}

}
