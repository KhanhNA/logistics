package com.tsolution._1entities.reportdto;

public class PalletInventoryReportDto {
	private String zone;
	private String palletType;
	private String palletCode;
	private String packCode;
	private String productName;
	private String barcode;
	private Integer packSize;
	private String uom;
	private Long quantity;
	private String expiredDate;
	private String status;

	public String getZone() {
		return this.zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getPalletType() {
		return this.palletType;
	}

	public void setPalletType(String palletType) {
		this.palletType = palletType;
	}

	public String getPalletCode() {
		return this.palletCode;
	}

	public void setPalletCode(String palletCode) {
		this.palletCode = palletCode;
	}

	public String getPackCode() {
		return this.packCode;
	}

	public void setPackCode(String packCode) {
		this.packCode = packCode;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Integer getPackSize() {
		return this.packSize;
	}

	public void setPackSize(Integer packSize) {
		this.packSize = packSize;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getExpiredDate() {
		return this.expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
