package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "country_description")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class CountryDescriptionEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5736160990450631676L;

	@ManyToOne
	@JoinColumn(name = "language_id")
	private LanguageEntity language;

	@ManyToOne
	@JoinColumn(name = "country_id")
	private CountryEntity country;

	@Column(name = "name")
	private String name;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	public LanguageEntity getLanguage() {
		return this.language;
	}

	public void setLanguage(LanguageEntity language) {
		this.language = language;
	}

	public CountryEntity getCountry() {
		return this.country;
	}

	public void setCountry(CountryEntity country) {
		this.country = country;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}