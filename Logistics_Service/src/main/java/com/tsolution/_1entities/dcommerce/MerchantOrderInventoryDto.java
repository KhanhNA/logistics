package com.tsolution._1entities.dcommerce;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class MerchantOrderInventoryDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7445008595881954178L;

	private String destinationStore;
	private List<String> stores;

	private List<MerchantOrderInventoryDetailDto> merchantOrderInventoryDetails;

	public String getDestinationStore() {
		return this.destinationStore;
	}

	public void setDestinationStore(String destinationStore) {
		this.destinationStore = destinationStore;
	}

	public List<String> getStores() {
		return this.stores;
	}

	public void setStores(List<String> stores) {
		this.stores = stores;
	}

	public List<MerchantOrderInventoryDetailDto> getMerchantOrderInventoryDetails() {
		return this.merchantOrderInventoryDetails;
	}

	public void setMerchantOrderInventoryDetails(List<MerchantOrderInventoryDetailDto> merchantOrderInventoryDetails) {
		this.merchantOrderInventoryDetails = merchantOrderInventoryDetails;
	}

}
