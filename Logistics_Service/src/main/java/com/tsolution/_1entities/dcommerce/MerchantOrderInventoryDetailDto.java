package com.tsolution._1entities.dcommerce;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class MerchantOrderInventoryDetailDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6368858447386401289L;
	private String productPackingCode;
	private Long quantity;
	private Long inventory;
	private Integer maxDayToWait;

	public String getProductPackingCode() {
		return this.productPackingCode;
	}

	public void setProductPackingCode(String productPackingCode) {
		this.productPackingCode = productPackingCode;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Long getInventory() {
		return this.inventory;
	}

	public void setInventory(Long inventory) {
		this.inventory = inventory;
	}

	public Integer getMaxDayToWait() {
		return this.maxDayToWait;
	}

	public void setMaxDayToWait(Integer maxDayToWait) {
		this.maxDayToWait = maxDayToWait;
	}

}
