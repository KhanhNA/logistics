package com.tsolution._1entities.dcommerce;

public class MerchantOrderDto {
	private String logisticCode;
	private Integer orderStatus;

	public MerchantOrderDto(String logisticCode, Integer orderStatus) {
		super();
		this.logisticCode = logisticCode;
		this.orderStatus = orderStatus;
	}

	public String getLogisticCode() {
		return this.logisticCode;
	}

	public void setLogisticCode(String logisticCode) {
		this.logisticCode = logisticCode;
	}

	public Integer getOrderStatus() {
		return this.orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

}
