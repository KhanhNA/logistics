package com.tsolution._1entities.enums;

public enum RepackingPlanningStatus {
	NEW(0), PLANNED(3), REPACKED(1), IMPORTED(2), CANCEL(-1);

	private Integer value;

	RepackingPlanningStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
