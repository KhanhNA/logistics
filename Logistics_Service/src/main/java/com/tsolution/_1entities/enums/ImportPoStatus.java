package com.tsolution._1entities.enums;

public enum ImportPoStatus {
	NEW(0), IN_PALLET(1), OUT_OF_PALLET(2);

	private Integer value;

	ImportPoStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
