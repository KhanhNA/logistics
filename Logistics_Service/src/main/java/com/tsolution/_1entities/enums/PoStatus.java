package com.tsolution._1entities.enums;

public enum PoStatus {
	REJECT("REJECT"), NEW("NEW"), APPROVED("APPROVED"), IMPORTED("IMPORTED"),
	ARRIVED_VIETNAM_PORT("ARRIVED_VIETNAM_PORT"), ARRIVED_MYANMAR_PORT("ARRIVED_MYANMAR_PORT");

	private String value;

	PoStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
