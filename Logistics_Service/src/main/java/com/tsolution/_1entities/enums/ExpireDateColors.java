package com.tsolution._1entities.enums;

import java.time.LocalDateTime;

import com.tsolution._1entities.ProductPackingEntity;

public enum ExpireDateColors {
	OUT_OF_DATE("mat-row-red") {
		@Override
		public String generateQuery(String expireDate, String lifeCycle, String warningThreshold) {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append(String.format(" AND DATEDIFF(DATE(%s), DATE(SYSDATE())) <= 0 ", expireDate));
			return strQuery.toString();
		}

		@Override
		public String getStatus() {
			return "inventory.status.mat-row-red";
		}
	},
	WARNING("mat-row-yellow") {
		@Override
		public String generateQuery(String expireDate, String lifeCycle, String warningThreshold) {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append(String.format(" AND DATEDIFF(DATE(%s), DATE(SYSDATE())) > 0 ", expireDate));
			strQuery.append(" AND DATEDIFF( DATE_ADD( ");
			strQuery.append(String.format(" DATE_SUB(%s, INTERVAL %s DAY), ", expireDate, lifeCycle));
			strQuery.append(String.format(" INTERVAL %s DAY ", warningThreshold));
			strQuery.append(" ), DATE(SYSDATE())) <= 0 ");
			return strQuery.toString();
		}

		@Override
		public String getStatus() {
			return "inventory.status.mat-row-yellow";
		}
	},
	STILL_EXPIRY_DATE("") {
		@Override
		public String generateQuery(String expireDate, String lifeCycle, String warningThreshold) {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append(String.format(" AND DATEDIFF(DATE(%s), DATE(SYSDATE())) > 0 ", expireDate));
			strQuery.append(" AND DATEDIFF( DATE_ADD( ");
			strQuery.append(String.format(" DATE_SUB(%s, INTERVAL %s DAY), ", expireDate, lifeCycle));
			strQuery.append(String.format(" INTERVAL %s DAY ", warningThreshold));
			strQuery.append(" ), DATE(SYSDATE())) > 0 ");
			return strQuery.toString();
		}

		@Override
		public String getStatus() {
			return "inventory.status.still-expiry-date";
		}
	};

	private String value;

	ExpireDateColors(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public abstract String generateQuery(String expireDate, String lifeCycle, String warningThreshold);

	public abstract String getStatus();

	public static String findClassName(LocalDateTime sysdate, LocalDateTime expireDate,
			ProductPackingEntity productPacking) {
		if ((expireDate == null) || (sysdate == null)) {
			return null;
		}
		if (!expireDate.toLocalDate().isAfter(sysdate.toLocalDate())) {
			return ExpireDateColors.OUT_OF_DATE.getValue();
		} else {
			LocalDateTime nsx = expireDate.minusDays(productPacking.getLifecycle());
			LocalDateTime warningDay = nsx.plusDays(productPacking.getWarningThreshold());
			if (!warningDay.toLocalDate().isAfter(sysdate.toLocalDate())) {
				return ExpireDateColors.WARNING.getValue();
			}
			return ExpireDateColors.STILL_EXPIRY_DATE.getValue();
		}
	}

	public static ExpireDateColors findByValue(String value) {
		for (ExpireDateColors expireDateColor : ExpireDateColors.values()) {
			if (expireDateColor.getValue().equalsIgnoreCase(value)) {
				return expireDateColor;
			}
		}
		return null;
	}
}
