package com.tsolution._1entities.enums;

public enum ExportStatementStatus {
	// Mới tạo lệnh xuất chưa thực xuất
	REQUESTED(0),
	// Đã tạo lệnh thực xuất chưa có lệnh nhập tương ứng
	RELEASED(1),
	// Đang được thực thi
	// 2.1: Đã tạo lệnh nhập nhưng chưa thực nhập,
	// 2.2: Tạo lệnh từ Merchant_order
	PROCESSING(2),
	// Được hoàn thành thực nhập/thực xuất cho merchant
	COMPLETED(3),
	// Lệnh bị hủy
	CANCELED(4),
	// Lệnh bị trả lại từ phía nhận
	RETURNED(5);

	private Integer value;

	ExportStatementStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

	public static ExportStatementStatus findByValue(Integer value) {
		for (ExportStatementStatus exportStatementStatus : ExportStatementStatus.values()) {
			if (exportStatementStatus.getValue().equals(value)) {
				return exportStatementStatus;
			}
		}
		return null;
	}
}
