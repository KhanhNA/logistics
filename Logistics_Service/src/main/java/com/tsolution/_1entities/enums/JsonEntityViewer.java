package com.tsolution._1entities.enums;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

public class JsonEntityViewer {
	public interface Human {
		public interface Summary {
			public interface ProductPacking extends Summary {
			}

			public interface ProductPackingGroupByExpireDate extends Summary {
			}

			public interface InventoryIgnoreExpireDate extends Summary {
			}

			public interface MerchantOrderDetails extends Summary {
			}

			public interface CountryLocationTree extends Summary {
			}

			public interface StoreInventory extends Summary {
			}
		}

		public interface Custom extends Human.Summary {
			public interface ListRepackingPlanning extends Custom {
			}

			public interface ListPo extends Summary {
			}
		}

		public interface Detail extends Summary {
			public interface ExportStatementDetail extends Detail {
			}
		}

		public interface CustomDetail extends Human.Detail {
			public interface PoDetail extends Detail {
			}
		}

	}

	public interface GOD extends Human.Detail {
	}

	public static final Map<String, Class<? extends Human.Summary>> MapRequest = ImmutableMap.<String, Class<? extends Human.Summary>>builder()
			.put("/product-packings", Human.Summary.ProductPacking.class)
			.put("/product-packings/group-by-expire-date", Human.Summary.ProductPackingGroupByExpireDate.class)
			.put("/repacking-plannings", Human.Custom.ListRepackingPlanning.class)
			.put("/pos", Human.Custom.ListPo.class).put("/pos/{id}", Human.CustomDetail.PoDetail.class)
			.put("/stores/inventory-ignore-expire-date", Human.Summary.InventoryIgnoreExpireDate.class)
			.put("/export-statements/{id}", Human.Detail.ExportStatementDetail.class)
			.put("/merchant-orders/details", Human.Summary.MerchantOrderDetails.class)
			.put("/countries/locations/tree", Human.Summary.CountryLocationTree.class)
			.put("/stores/inventory", Human.Summary.StoreInventory.class).build();

}
