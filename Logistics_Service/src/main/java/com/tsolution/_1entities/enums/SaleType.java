package com.tsolution._1entities.enums;

public enum SaleType {
	IN("IN"), OUT("OUT");
	String value;

	SaleType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
