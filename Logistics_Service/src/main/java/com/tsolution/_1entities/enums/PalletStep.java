package com.tsolution._1entities.enums;

public enum PalletStep {
	IMPORT_PO_UPDATE_PALLET(1) {
		@Override
		public String getStepName() {
			return "pallet.step.import.po.update.pallet";
		}
	},
	IMPORT_STATEMENT_UPDATE_PALLET(2) {
		@Override
		public String getStepName() {
			return "pallet.step.import.statement.update.pallet";
		}
	};

	Integer value;

	PalletStep(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

	public abstract String getStepName();

	public static boolean isContain(Integer input) {
		for (PalletStep palletStep : PalletStep.values()) {
			if (palletStep.getValue().equals(input)) {
				return true;
			}
		}
		return false;
	}

	public static PalletStep findByValue(Integer value) {
		for (PalletStep palletStep : PalletStep.values()) {
			if (palletStep.getValue().equals(value)) {
				return palletStep;
			}
		}
		return null;
	}
}
