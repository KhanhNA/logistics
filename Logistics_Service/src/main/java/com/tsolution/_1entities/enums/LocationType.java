package com.tsolution._1entities.enums;

public enum LocationType {
	REGION("REGION"), DISTRICT("DISTRICT"), TOWNSHIP("TOWNSHIP");

	String value;

	LocationType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
