package com.tsolution._1entities.enums;

public enum ImportStatementStatus {
	// Mới tạo lệnh nhập chưa thực nhập và chưa cộng kho
	NOT_UPDATE_INVENTORY(0),
	// Đã tạo lệnh thực nhập và cộng kho
	UPDATED_INVENTORY(1);

	private Integer value;

	ImportStatementStatus(int value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
