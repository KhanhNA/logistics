package com.tsolution._1entities.claim.enums;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tsolution._1entities.ImportStatementDetailEntity;
import com.tsolution._1entities.ImportStatementEntity;
import com.tsolution._1entities.PoDetailEntity;
import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.RepackingPlanningDetailEntity;
import com.tsolution._1entities.RepackingPlanningDetailRepackedEntity;
import com.tsolution._1entities.RepackingPlanningEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.claim.ClaimDetail;
import com.tsolution._1entities.enums.ExportStatementStatus;
import com.tsolution._1entities.enums.ImportPoStatus;
import com.tsolution._1entities.enums.ImportStatementStatus;
import com.tsolution._1entities.enums.PoStatus;
import com.tsolution._1entities.enums.RepackingPlanningStatus;
import com.tsolution._1entities.export_statement.ExportStatementDetailEntity;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.importpo.ImportPoDetailEntity;
import com.tsolution._1entities.importpo.ImportPoEntity;
import com.tsolution.excetions.BusinessException;

public enum ClaimType {
	IMPORT_PO("IMPORT_PO") {

		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append(" SELECT ppp.* ");
			strQuery.append(" FROM po p ");
			strQuery.append("	   JOIN po_detail pd ON pd.po_id = p.id AND p.id = %d ");
			strQuery.append(" 					     AND pd.product_packing_id = %d ");
			strQuery.append("	   JOIN product_packing_price ppp ON ppp.id = pd.product_packing_price_id ");
			strQuery.append(" LIMIT 1 ");
			return String.format(strQuery.toString(), referenceId, productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			if (!(superEntity instanceof PoEntity)) {
				return ClaimType.CLAIM_INPUT_MISSING_REFERENCE;
			}
			PoEntity po = (PoEntity) superEntity;
			if (!managedStores.contains(po.getStore().getId())) {
				return BusinessException.COMMON_ACCESS_DENIED;
			}
			if ((po.getStatus() == null) || !PoStatus.ARRIVED_MYANMAR_PORT.equals(po.getStatus())) {
				return "po.status.check.arrived.myanmar.port";
			}
			List<PoDetailEntity> poDetails = po.getPoDetails();
			// Gom số lượng theo product_packing_id
			Map<Long, Long> totalQuantityByProductPackingId = claimDetails.stream().collect(Collectors.groupingBy(
					cd -> cd.getProductPacking().getId(), Collectors.summingLong(ClaimDetail::getQuantity)));
			for (Map.Entry<Long, Long> entry : totalQuantityByProductPackingId.entrySet()) {
				Long productPackingId = entry.getKey();
				Long quantity = entry.getValue();
				boolean isExists = false;
				for (PoDetailEntity poDetail : poDetails) {
					if (productPackingId.equals(poDetail.getProductPacking().getId())) {
						isExists = true;
						if (quantity > poDetail.getQuantity()) {
							return ClaimType.CLAIM_DETAIL_INPUT_QUANTITY_RANGE;
						}
					}
				}
				if (!isExists) {
					return "claim.detail.input.product.packing.not.exists.reference";
				}
			}
			return null;
		}
	},
	IMPORT_PO_UPDATE_PALLET("IMPORT_PO_UPDATE_PALLET") {
		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			if (!(superEntity instanceof ImportPoEntity)) {
				return ClaimType.CLAIM_INPUT_MISSING_REFERENCE;
			}
			ImportPoEntity importPo = (ImportPoEntity) superEntity;
			if (!managedStores.contains(importPo.getStore().getId())) {
				return BusinessException.COMMON_ACCESS_DENIED;
			}
			if ((importPo.getStatus() == null) || !ImportPoStatus.NEW.getValue().equals(importPo.getStatus())) {
				return "import.po.status.check.new";
			}
			List<ImportPoDetailEntity> importPoDetails = importPo.getImportPoDetails();
			// Gom số lượng theo product_packing_id, expire_date
			Map<ClaimDetail, Long> totalQuantityByProductPackingIdExpireDate = claimDetails.stream()
					.collect(Collectors.groupingBy(
							cd -> new ClaimDetail(cd.getProductPacking().getId(), cd.getExpireDate()),
							Collectors.summingLong(ClaimDetail::getQuantity)));
			for (Map.Entry<ClaimDetail, Long> entry : totalQuantityByProductPackingIdExpireDate.entrySet()) {
				ClaimDetail claimDetail = entry.getKey();
				Long quantity = entry.getValue();
				boolean isExists = false;
				for (ImportPoDetailEntity importPoDetail : importPoDetails) {
					if (claimDetail.getProductPacking().getId().equals(importPoDetail.getProductPacking().getId())
							&& claimDetail.getExpireDate().toLocalDate()
									.isEqual(importPoDetail.getExpireDate().toLocalDate())) {
						isExists = true;
						if (quantity > importPoDetail.getQuantity()) {
							return ClaimType.CLAIM_DETAIL_INPUT_QUANTITY_RANGE;
						}
					}
				}
				if (!isExists) {
					return ClaimType.CLAIM_DETAIL_INPUT_NOT_MATCH_REFERENCE_DETAIL;
				}
			}
			return null;
		}
	},
	REPACKING_PLANNING("REPACKING_PLANNING") {
		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			if (!(superEntity instanceof RepackingPlanningEntity)) {
				return ClaimType.CLAIM_INPUT_MISSING_REFERENCE;
			}
			RepackingPlanningEntity repackingPlanning = (RepackingPlanningEntity) superEntity;
			if (!managedStores.contains(repackingPlanning.getStore().getId())) {
				return BusinessException.COMMON_ACCESS_DENIED;
			}
			if ((repackingPlanning.getStatus() == null)
					|| !(RepackingPlanningStatus.NEW.getValue().equals(repackingPlanning.getStatus())
							|| RepackingPlanningStatus.PLANNED.getValue().equals(repackingPlanning.getStatus()))) {
				return "repackingplanning.status.check.new";
			}
			List<RepackingPlanningDetailEntity> repackingPlanningDetails = repackingPlanning
					.getRepackingPlanningDetails();
			// Gom số lượng theo product_packing_id, expire_date
			Map<ClaimDetail, Long> totalQuantityByProductPackingIdExpireDate = claimDetails.stream()
					.collect(Collectors.groupingBy(
							cd -> new ClaimDetail(cd.getProductPacking().getId(), cd.getExpireDate()),
							Collectors.summingLong(ClaimDetail::getQuantity)));
			for (Map.Entry<ClaimDetail, Long> entry : totalQuantityByProductPackingIdExpireDate.entrySet()) {
				ClaimDetail claimDetail = entry.getKey();
				Long quantity = entry.getValue();
				boolean isExists = false;
				for (RepackingPlanningDetailEntity repackingPlanningDetail : repackingPlanningDetails) {
					if (claimDetail.getProductPacking().getId()
							.equals(repackingPlanningDetail.getProductPacking().getId())
							&& claimDetail.getExpireDate().toLocalDate()
									.isEqual(repackingPlanningDetail.getExpireDate().toLocalDate())) {
						isExists = true;
						if (quantity > repackingPlanningDetail.getQuantity()) {
							return ClaimType.CLAIM_DETAIL_INPUT_QUANTITY_RANGE;
						}
					}
				}
				if (!isExists) {
					return ClaimType.CLAIM_DETAIL_INPUT_NOT_MATCH_REFERENCE_DETAIL;
				}
			}
			return null;
		}
	},
	IMPORT_STATEMENT_REPACKING_PLANNING("IMPORT_STATEMENT_REPACKING_PLANNING") {
		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			if (!(superEntity instanceof RepackingPlanningEntity)) {
				return ClaimType.CLAIM_INPUT_MISSING_REFERENCE;
			}
			// Gom số lượng theo product_packing_id, expire_date
			Map<ClaimDetail, Long> totalQuantityByProductPackingIdExpireDate = claimDetails.stream()
					.collect(Collectors.groupingBy(
							cd -> new ClaimDetail(cd.getProductPacking().getId(), cd.getExpireDate()),
							Collectors.summingLong(ClaimDetail::getQuantity)));
			RepackingPlanningEntity repackingPlanning = (RepackingPlanningEntity) superEntity;
			if (!managedStores.contains(repackingPlanning.getStore().getId())) {
				return BusinessException.COMMON_ACCESS_DENIED;
			}
			if ((repackingPlanning.getStatus() == null)
					|| !RepackingPlanningStatus.REPACKED.getValue().equals(repackingPlanning.getStatus())) {
				return "repackingplanning.status.check.repacked";
			}
			Map<RepackingPlanningDetailRepackedEntity, Long> totalQuantityRepackingPlanningDetailRepacked = repackingPlanning
					.getRepackingPlanningDetails().stream()
					.map(RepackingPlanningDetailEntity::getRepackingPlanningDetailRepackeds)
					.flatMap(List<RepackingPlanningDetailRepackedEntity>::stream)
					.collect(Collectors.groupingBy(
							rpdr -> new RepackingPlanningDetailRepackedEntity(rpdr.getProductPacking().getId(),
									rpdr.getExpireDate()),
							Collectors.summingLong(RepackingPlanningDetailRepackedEntity::getQuantity)));
			for (Map.Entry<ClaimDetail, Long> entry : totalQuantityByProductPackingIdExpireDate.entrySet()) {
				ClaimDetail claimDetail = entry.getKey();
				Long quantity = entry.getValue();
				Long quantityRepackingPlanningDetailRepacked = totalQuantityRepackingPlanningDetailRepacked
						.get(new RepackingPlanningDetailRepackedEntity(claimDetail.getProductPacking().getId(),
								claimDetail.getExpireDate()));
				if (quantityRepackingPlanningDetailRepacked == null) {
					return ClaimType.CLAIM_DETAIL_INPUT_NOT_MATCH_REFERENCE_DETAIL;
				}
				if (quantity > quantityRepackingPlanningDetailRepacked) {
					return ClaimType.CLAIM_DETAIL_INPUT_QUANTITY_RANGE;
				}
			}
			return null;
		}
	},
	IMPORT_STATEMENT_EXPORT_STATEMENT("IMPORT_STATEMENT_EXPORT_STATEMENT") {
		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			if (!(superEntity instanceof ImportStatementEntity)) {
				return ClaimType.CLAIM_INPUT_MISSING_REFERENCE;
			}
			// Gom số lượng theo product_packing_id, expire_date
			Map<ClaimDetail, Long> totalQuantityByProductPackingIdExpireDate = claimDetails.stream()
					.collect(Collectors.groupingBy(
							cd -> new ClaimDetail(cd.getProductPacking().getId(), cd.getExpireDate()),
							Collectors.summingLong(ClaimDetail::getQuantity)));
			ImportStatementEntity importStatement = (ImportStatementEntity) superEntity;
			if (!managedStores.contains(importStatement.getToStore().getId())) {
				return BusinessException.COMMON_ACCESS_DENIED;
			}
			if ((importStatement.getStatus() == null)
					|| !ImportStatementStatus.NOT_UPDATE_INVENTORY.getValue().equals(importStatement.getStatus())) {
				return "import.statement.claim.status";
			}
			List<ImportStatementDetailEntity> importStatementDetails = importStatement.getImportStatementDetails();
			for (Map.Entry<ClaimDetail, Long> entry : totalQuantityByProductPackingIdExpireDate.entrySet()) {
				ClaimDetail claimDetail = entry.getKey();
				Long quantity = entry.getValue();
				boolean isExists = false;
				for (ImportStatementDetailEntity importStatementDetail : importStatementDetails) {
					if (claimDetail.getProductPacking().getId()
							.equals(importStatementDetail.getProductPacking().getId())
							&& claimDetail.getExpireDate().toLocalDate()
									.isEqual(importStatementDetail.getExpireDate().toLocalDate())) {
						isExists = true;
						if (quantity > importStatementDetail.getQuantity()) {
							return ClaimType.CLAIM_DETAIL_INPUT_QUANTITY_RANGE;
						}
					}
				}
				if (!isExists) {
					return ClaimType.CLAIM_DETAIL_INPUT_NOT_MATCH_REFERENCE_DETAIL;
				}
			}
			return null;
		}
	},
	EXPORT_STATEMENT("EXPORT_STATEMENT") {

		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			if (!(superEntity instanceof ExportStatementEntity)) {
				return ClaimType.CLAIM_INPUT_MISSING_REFERENCE;
			}
			ExportStatementEntity exportStatement = (ExportStatementEntity) superEntity;
			if (!managedStores.contains(exportStatement.getFromStore().getId())) {
				return BusinessException.COMMON_ACCESS_DENIED;
			}
			if ((exportStatement.getStatus() == null)
					|| !ExportStatementStatus.REQUESTED.getValue().equals(exportStatement.getStatus())) {
				return "export.statement.claim.status";
			}
			// Xem xét lại khi hàng hỏng thì phải vào kho mà lấy bù để thực xuất và chỉ quan
			// tâm là hàng này có thuộc chi tiết lệnh xuất không
			List<ExportStatementDetailEntity> exportStatementDetails = exportStatement.getExportStatementDetails();
			for (ClaimDetail claimDetail : claimDetails) {
				if (exportStatementDetails.stream()
						.noneMatch(esd -> esd.getProductPacking().equals(claimDetail.getProductPacking()))) {
					return ClaimType.CLAIM_DETAIL_INPUT_NOT_MATCH_REFERENCE_DETAIL;
				}
			}
			return null;
		}
	},
	DELIVERY("DELIVERY") {

		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			if (!(superEntity instanceof ImportStatementEntity)) {
				return ClaimType.CLAIM_INPUT_MISSING_REFERENCE;
			}
			ImportStatementEntity importStatement = (ImportStatementEntity) superEntity;
			if ((importStatement.getToStore() == null)
					|| !managedStores.contains(importStatement.getToStore().getId())) {
				return BusinessException.COMMON_ACCESS_DENIED;
			}
			if ((importStatement.getStatus() == null)
					|| !ImportStatementStatus.NOT_UPDATE_INVENTORY.getValue().equals(importStatement.getStatus())) {
				return "import.statement.claim.status";
			}
			List<ImportStatementDetailEntity> importStatementDetails = importStatement.getImportStatementDetails();
			for (ClaimDetail claimDetail : claimDetails) {
				boolean isExists = false;
				for (ImportStatementDetailEntity importStatementDetail : importStatementDetails) {
					if (claimDetail.getProductPacking().getId()
							.equals(importStatementDetail.getProductPacking().getId())
							&& claimDetail.getExpireDate().toLocalDate()
									.isEqual(importStatementDetail.getExpireDate().toLocalDate())) {
						isExists = true;
						if (claimDetail.getQuantity() > importStatementDetail.getQuantity()) {
							return ClaimType.CLAIM_DETAIL_INPUT_QUANTITY_RANGE;
						}
					}
				}
				if (!isExists) {
					return ClaimType.CLAIM_DETAIL_INPUT_NOT_MATCH_REFERENCE_DETAIL;
				}
			}
			return null;
		}
	},
	OUT_OF_DATE_PALLET_STEP_1("OUT_OF_DATE_PALLET_STEP_1") {

		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			return null;
		}
	},
	OUT_OF_DATE_PALLET_STEP_2("OUT_OF_DATE_PALLET_STEP_2") {

		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			return null;
		}
	},
	OTHER("OTHER") {

		@Override
		public String getQueryForProductPackingPrice(Long referenceId, Long productPackingId) {
			return String.format(ClaimType.getCurrentProductPackingPrice(), productPackingId);
		}

		@Override
		public String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
				List<ClaimDetail> claimDetails) {
			return null;
		}
	};

	private static final String CLAIM_DETAIL_INPUT_NOT_MATCH_REFERENCE_DETAIL = "claim.detail.input.not.match.reference.detail";
	private static final String CLAIM_DETAIL_INPUT_QUANTITY_RANGE = "claim.detail.input.quantity.range";
	public static final String CLAIM_INPUT_MISSING_REFERENCE = "claim.input.missing.reference";
	public static final String CLAIM_INPUT_REFERENCE_NOT_EXISTS = "claim.input.reference.not.exists";
	String value;

	ClaimType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static ClaimType parse(String input) {
		ClaimType claimType = null;
		for (ClaimType type : ClaimType.values()) {
			if (type.getValue().equalsIgnoreCase(input)) {
				claimType = type;
				break;
			}
		}
		return claimType;
	}

	public static boolean contains(String value) {
		for (ClaimType c : ClaimType.values()) {
			if (c.getValue().equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	public static String getCurrentProductPackingPrice() {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT ppp.* ");
		strQuery.append(" FROM product_packing_price ppp ");
		strQuery.append(" WHERE ppp.product_packing_id = %d ");
		strQuery.append(" 		AND ppp.from_date <= SYSDATE() ");
		strQuery.append(" 		AND ( ppp.to_date IS NULL OR ppp.to_date >= DATE(SYSDATE()) ) ");
		strQuery.append("		AND ppp.status = 1 ");
		strQuery.append(" LIMIT 1 ");
		return strQuery.toString();
	}

	public abstract String validReferenceWithClaimDetails(SuperEntity superEntity, List<Long> managedStores,
			List<ClaimDetail> claimDetails);

	public abstract String getQueryForProductPackingPrice(Long referenceId, Long productPackingId);

}
