package com.tsolution._1entities.claim.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DeserializeDateHandler;
import com.tsolution.utils.SerializeDateHandler;

@Entity
public class ListDamagedGoodsDto implements Serializable {
	private static final long serialVersionUID = 380805269410455920L;

	@Id
	private Long id;
	@Column(name = "store_code", nullable = false)
	private String storeCode;
	@Column(name = "store_name", nullable = false)
	private String storeName;
	@Column(name = "claim_type")
	private String claimType;
	@Column(name = "amenable")
	private String amenable;
	@Column(name = "product_packing_code", nullable = false)
	private String productPackingCode;
	@Column(name = "product_packing_name", nullable = false)
	private String productPackingName;
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonDeserialize(using = DeserializeDateHandler.class)
	@Column(name = "expire_date", nullable = false)
	private LocalDateTime expireDate;
	@Column(name = "quantity", nullable = false)
	private Long quantity;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStoreCode() {
		return this.storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getStoreName() {
		return this.storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getClaimType() {
		return this.claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public String getAmenable() {
		return this.amenable;
	}

	public void setAmenable(String amenable) {
		this.amenable = amenable;
	}

	public String getProductPackingCode() {
		return this.productPackingCode;
	}

	public void setProductPackingCode(String productPackingCode) {
		this.productPackingCode = productPackingCode;
	}

	public String getProductPackingName() {
		return this.productPackingName;
	}

	public void setProductPackingName(String productPackingName) {
		this.productPackingName = productPackingName;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

}
