package com.tsolution._1entities.claim.enums;

import java.util.Arrays;
import java.util.Optional;

public enum ClaimStatus {
	WAIT_APPROVE(0), APPROVED(1), REJECTED(2), COMPLETED(3);

	Integer value;

	ClaimStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}

	public static ClaimStatus valueOf(Integer value) {
		Optional<ClaimStatus> oClaimStatus = Arrays.stream(ClaimStatus.values())
				.filter(status -> status.value.equals(value)).findFirst();
		return oClaimStatus.isPresent() ? oClaimStatus.get() : null;
	}
}
