package com.tsolution._1entities.claim;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.store.StoreEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "claim")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Claim extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1550629098240099214L;

	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@ManyToOne
	@JoinColumn(name = "store_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity store;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ClaimType type;

	@Column(name = "reference_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long referenceId;

	@Column(name = "amenable")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String amenable;

	@Column(name = "description")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String description;

	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer status;

	@Column(name = "reject_reason")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String rejectReason;

	@OneToMany(mappedBy = "claim", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ClaimDetail> claimDetails;

	@OneToMany(mappedBy = "claim", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ClaimImage> claimImages;

	@Transient
	private java.sql.Date fromCreateDate;
	@Transient
	private java.sql.Date toCreateDate;
	@Transient
	private String productPackingCode;
	@Transient
	private String productName;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public StoreEntity getStore() {
		return this.store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public ClaimType getType() {
		return this.type;
	}

	public void setType(ClaimType type) {
		this.type = type;
	}

	public Long getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(Long referenceId) {
		this.referenceId = referenceId;
	}

	public String getAmenable() {
		return this.amenable;
	}

	public void setAmenable(String amenable) {
		this.amenable = amenable;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRejectReason() {
		return this.rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public List<ClaimDetail> getClaimDetails() {
		return this.claimDetails;
	}

	public void setClaimDetails(List<ClaimDetail> claimDetails) {
		this.claimDetails = claimDetails;
	}

	public List<ClaimImage> getClaimImages() {
		return this.claimImages;
	}

	public void setClaimImages(List<ClaimImage> claimImages) {
		this.claimImages = claimImages;
	}

	public java.sql.Date getFromCreateDate() {
		return this.fromCreateDate;
	}

	public void setFromCreateDate(java.sql.Date fromCreateDate) {
		this.fromCreateDate = fromCreateDate;
	}

	public java.sql.Date getToCreateDate() {
		return this.toCreateDate;
	}

	public void setToCreateDate(java.sql.Date toCreateDate) {
		this.toCreateDate = toCreateDate;
	}

	public String getProductPackingCode() {
		return this.productPackingCode;
	}

	public void setProductPackingCode(String productPackingCode) {
		this.productPackingCode = productPackingCode;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

}