package com.tsolution._1entities.claim;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.store.StoreProductPackingDetailEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "claim_detail")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ClaimDetail extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5291825172149051829L;

	@ManyToOne
	@JoinColumn(name = "claim_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Claim claim;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingEntity productPacking;

	@Column(name = "expire_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime expireDate;

	@Column(name = "quantity")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long quantity;

	@ManyToOne
	@JoinColumn(name = "product_packing_price_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingPriceEntity productPackingPrice;

	@Transient
	private Long palletId;

	@ManyToOne
	@JoinColumn(name = "pallet_detail_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private PalletDetailEntity palletDetail;

	@ManyToOne
	@JoinColumn(name = "store_product_packing_detail_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreProductPackingDetailEntity storeProductPackingDetail;

	public ClaimDetail() {
	}

	public ClaimDetail(Long productPackingId, LocalDateTime expireDate) {
		this.productPacking = new ProductPackingEntity(productPackingId);
		this.expireDate = expireDate;
	}

	public Claim getClaim() {
		return this.claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public ProductPackingPriceEntity getProductPackingPrice() {
		return this.productPackingPrice;
	}

	public void setProductPackingPrice(ProductPackingPriceEntity productPackingPrice) {
		this.productPackingPrice = productPackingPrice;
	}

	public Long getPalletId() {
		return this.palletId;
	}

	public void setPalletId(Long palletId) {
		this.palletId = palletId;
	}

	public PalletDetailEntity getPalletDetail() {
		return this.palletDetail;
	}

	public void setPalletDetail(PalletDetailEntity palletDetail) {
		this.palletDetail = palletDetail;
	}

	public StoreProductPackingDetailEntity getStoreProductPackingDetail() {
		return this.storeProductPackingDetail;
	}

	public void setStoreProductPackingDetail(StoreProductPackingDetailEntity storeProductPackingDetail) {
		this.storeProductPackingDetail = storeProductPackingDetail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.expireDate == null) ? 0 : this.expireDate.hashCode());
		result = (prime * result) + ((this.productPacking == null) ? 0 : this.productPacking.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ClaimDetail other = (ClaimDetail) obj;
		if (this.expireDate == null) {
			if (other.expireDate != null) {
				return false;
			}
		} else if (!this.expireDate.equals(other.expireDate)) {
			return false;
		}
		if (this.productPacking == null) {
			if (other.productPacking != null) {
				return false;
			}
		} else if (!this.productPacking.equals(other.productPacking)) {
			return false;
		}
		return true;
	}

}