package com.tsolution._1entities.claim;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "claim_image")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ClaimImage extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4779499484634162952L;

	@ManyToOne
	@JoinColumn(name = "claim_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Claim claim;

	@Column(name = "url", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String url;

	@Column(name = "description", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String description;

	public Claim getClaim() {
		return this.claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}