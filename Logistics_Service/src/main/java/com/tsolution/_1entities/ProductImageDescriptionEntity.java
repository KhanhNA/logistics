package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "product_image_description")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductImageDescriptionEntity extends SuperEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "product_image_id", nullable = false)
	private Long productImageId;

	@Column(name = "language_id", nullable = false)
	private Long languageId;

	@Column(name = "name")
	private String name;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "alt_tag")
	private String altTag;

}