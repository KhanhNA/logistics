package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "repacking_planning")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class RepackingPlanningEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6412102715157927014L;

	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "description")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String description;

	@ManyToOne
	@JoinColumn(name = "store_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity store;

	@ManyToOne
	@JoinColumn(name = "distributor_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private DistributorEntity distributor;

	/**
	 * 0: Mới lên plan theo product_packing + quantity(nhỏ hơn tổng quantity của tất
	 * cả các pallet), 1: Repacked, 2: Đã tạo lệnh thực nhập
	 */
	@Column(name = "status", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer status;

	@Column(name = "planning_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime planningDate;

	@Column(name = "repacked_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime repackedDate;

	@Column(name = "imported_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime importedDate;

	@OneToMany(mappedBy = "repackingPlanning", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<RepackingPlanningDetailEntity> repackingPlanningDetails;

	@OneToOne(mappedBy = "repackingPlanning", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private ImportStatementEntity importStatement;

	@ManyToOne
	@JoinColumn(name = "create_user", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity createUserObj;

	public RepackingPlanningEntity() {
	}

	public RepackingPlanningEntity(Long id) {
		this.setId(id);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StoreEntity getStore() {
		return this.store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public DistributorEntity getDistributor() {
		return this.distributor;
	}

	public void setDistributor(DistributorEntity distributor) {
		this.distributor = distributor;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public LocalDateTime getPlanningDate() {
		return this.planningDate;
	}

	public void setPlanningDate(LocalDateTime planningDate) {
		this.planningDate = planningDate;
	}

	public LocalDateTime getRepackedDate() {
		return this.repackedDate;
	}

	public void setRepackedDate(LocalDateTime repackedDate) {
		this.repackedDate = repackedDate;
	}

	public LocalDateTime getImportedDate() {
		return this.importedDate;
	}

	public void setImportedDate(LocalDateTime importedDate) {
		this.importedDate = importedDate;
	}

	public List<RepackingPlanningDetailEntity> getRepackingPlanningDetails() {
		return this.repackingPlanningDetails;
	}

	public void setRepackingPlanningDetails(List<RepackingPlanningDetailEntity> repackingPlanningDetails) {
		this.repackingPlanningDetails = repackingPlanningDetails;
	}

	public ImportStatementEntity getImportStatement() {
		return this.importStatement;
	}

	public void setImportStatement(ImportStatementEntity importStatement) {
		this.importStatement = importStatement;
	}

}