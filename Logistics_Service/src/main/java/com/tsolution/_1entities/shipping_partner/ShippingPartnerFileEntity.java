package com.tsolution._1entities.shipping_partner;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "shipping_partner_file")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ShippingPartnerFileEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1865697452290448300L;

	@ManyToOne
	@JoinColumn(name = "shipping_partner_id", nullable = false)
	private ShippingPartnerEntity shippingPartner;

	@Column(name = "url", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String url;

	@Column(name = "description")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String description;

	public ShippingPartnerEntity getShippingPartner() {
		return this.shippingPartner;
	}

	public void setShippingPartner(ShippingPartnerEntity shippingPartnerEntity) {
		this.shippingPartner = shippingPartnerEntity;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
