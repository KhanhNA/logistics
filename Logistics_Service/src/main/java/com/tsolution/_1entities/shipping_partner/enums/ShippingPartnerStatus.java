package com.tsolution._1entities.shipping_partner.enums;

public enum ShippingPartnerStatus {
	PENDING("PENDING"), APPROVED("APPROVED"), REJECTED("REJECTED");

	String value;

	ShippingPartnerStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
