package com.tsolution._1entities.shipping_partner;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.shipping_partner.enums.ShippingPartnerStatus;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "shipping_partner")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ShippingPartnerEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5562733465375098599L;

	@Column(name = "name", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String name;

	@Column(name = "phone", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String phone;

	@Column(name = "email", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String email;

	@Column(name = "address", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String address;

	@Column(name = "contract_code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String contractCode;

	@Column(name = "tax_code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String taxCode;

	@Column(name = "description", nullable = true)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String description;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ShippingPartnerStatus status;

	@Column(name = "accept_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime acceptDate;

	@Column(name = "accept_user")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String acceptUser;

	@Column(name = "reject_reason")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String rejectReason;

	@OneToMany(mappedBy = "shippingPartner", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ShippingPartnerFileEntity> shippingPartnerFiles;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public String getTaxCode() {
		return this.taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ShippingPartnerStatus getStatus() {
		return this.status;
	}

	public void setStatus(ShippingPartnerStatus status) {
		this.status = status;
	}

	public LocalDateTime getAcceptDate() {
		return this.acceptDate;
	}

	public void setAcceptDate(LocalDateTime acceptDate) {
		this.acceptDate = acceptDate;
	}

	public String getAcceptUser() {
		return this.acceptUser;
	}

	public void setAcceptUser(String acceptUser) {
		this.acceptUser = acceptUser;
	}

	public String getRejectReason() {
		return this.rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public List<ShippingPartnerFileEntity> getShippingPartnerFiles() {
		if (this.shippingPartnerFiles == null) {
			this.shippingPartnerFiles = new ArrayList<>();
		}
		return this.shippingPartnerFiles;
	}

	public void setShippingPartnerFiles(List<ShippingPartnerFileEntity> shippingPartnerFileEntities) {
		this.shippingPartnerFiles = shippingPartnerFileEntities;
	}

}
