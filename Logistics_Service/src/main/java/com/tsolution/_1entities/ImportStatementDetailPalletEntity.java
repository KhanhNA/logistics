package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.store.StoreProductPackingDetailEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "import_statement_detail_pallet")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ImportStatementDetailPalletEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 477082231137602705L;

	@Column(name = "import_statement_id")
	private Long importStatementId;

	@Column(name = "import_statement_detail_id")
	private Long importStatementDetailId;

	@Column(name = "product_packing_id")
	private Long productPackingId;

	@Column(name = "expire_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime expireDate;

	@Column(name = "quantity", nullable = false)
	private Long quantity;

	@Column(name = "store_product_packing_detail_id")
	private Long storeProductPackingDetailId;

	@OneToOne
	@JoinColumn(name = "store_product_packing_detail_id", insertable = false, updatable = false)
	private StoreProductPackingDetailEntity storeProductPackingDetail;

	@ManyToOne
	@JoinColumn(name = "pallet_id")
	private PalletEntity pallet;

	@OneToOne
	@JoinColumn(name = "pallet_detail_id")
	private PalletDetailEntity palletDetail;

	public Long getImportStatementId() {
		return this.importStatementId;
	}

	public void setImportStatementId(Long importStatementId) {
		this.importStatementId = importStatementId;
	}

	public Long getImportStatementDetailId() {
		return this.importStatementDetailId;
	}

	public void setImportStatementDetailId(Long importStatementDetailId) {
		this.importStatementDetailId = importStatementDetailId;
	}

	public Long getProductPackingId() {
		return this.productPackingId;
	}

	public void setProductPackingId(Long productPackingId) {
		this.productPackingId = productPackingId;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Long getStoreProductPackingDetailId() {
		return this.storeProductPackingDetailId;
	}

	public void setStoreProductPackingDetailId(Long storeProductPackingDetailId) {
		this.storeProductPackingDetailId = storeProductPackingDetailId;
	}

	public StoreProductPackingDetailEntity getStoreProductPackingDetail() {
		return this.storeProductPackingDetail;
	}

	public void setStoreProductPackingDetail(StoreProductPackingDetailEntity storeProductPackingDetail) {
		this.storeProductPackingDetail = storeProductPackingDetail;
	}

	public PalletEntity getPallet() {
		return this.pallet;
	}

	public void setPallet(PalletEntity pallet) {
		this.pallet = pallet;
	}

	public PalletDetailEntity getPalletDetail() {
		return this.palletDetail;
	}

	public void setPalletDetail(PalletDetailEntity palletDetail) {
		this.palletDetail = palletDetail;
	}

}