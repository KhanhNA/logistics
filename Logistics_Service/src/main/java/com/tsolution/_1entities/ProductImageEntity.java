package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "product_image")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductImageEntity extends SuperEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "product_id", nullable = false)
	private Long productId;

	@Column(name = "default_image")
	private Boolean defaultImage;

	@Column(name = "image_crop")
	private Boolean imageCrop;

	@Column(name = "image_type")
	private Integer imageType;

	@Column(name = "image")
	private String image;

	@Column(name = "image_url")
	private String imageUrl;

}