package com.tsolution._1entities.export_statement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.store.StoreProductPackingDetailEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "export_statement_detail")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ExportStatementDetailEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1855261938868935577L;

	@ManyToOne
	@JoinColumn(name = "export_statement_id")
	private ExportStatementEntity exportStatement;

	@ManyToOne
	@JoinColumn(name = "store_product_packing_detail_id")
	private StoreProductPackingDetailEntity storeProductPackingDetail;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	private ProductPackingEntity productPacking;

	@Column(name = "expire_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime expireDate;

	@Column(name = "product_packing_price")
	private BigDecimal productPackingPrice;

	@Column(name = "quantity", nullable = false)
	private Long quantity;

	@Column(name = "export_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime exportDate;

	@ManyToOne
	@JoinColumn(name = "pallet_detail_id")
	private PalletDetailEntity palletDetail;

	public ExportStatementDetailEntity() {
	}

	public ExportStatementDetailEntity(Long productPackingId, LocalDateTime expireDate) {
		this.productPacking = new ProductPackingEntity(productPackingId);
		this.expireDate = expireDate;
	}

	public ExportStatementEntity getExportStatement() {
		return this.exportStatement;
	}

	public void setExportStatement(ExportStatementEntity exportStatement) {
		this.exportStatement = exportStatement;
	}

	public StoreProductPackingDetailEntity getStoreProductPackingDetail() {
		return this.storeProductPackingDetail;
	}

	public void setStoreProductPackingDetail(StoreProductPackingDetailEntity storeProductPackingDetail) {
		this.storeProductPackingDetail = storeProductPackingDetail;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public BigDecimal getProductPackingPrice() {
		return this.productPackingPrice;
	}

	public void setProductPackingPrice(BigDecimal productPackingPrice) {
		this.productPackingPrice = productPackingPrice;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public LocalDateTime getExportDate() {
		return this.exportDate;
	}

	public void setExportDate(LocalDateTime exportDate) {
		this.exportDate = exportDate;
	}

	public PalletDetailEntity getPalletDetail() {
		return this.palletDetail;
	}

	public void setPalletDetail(PalletDetailEntity palletDetail) {
		this.palletDetail = palletDetail;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.expireDate, this.productPacking);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ExportStatementDetailEntity)) {
			return false;
		}
		ExportStatementDetailEntity other = (ExportStatementDetailEntity) obj;
		return Objects.equals(this.expireDate, other.expireDate)
				&& Objects.equals(this.productPacking, other.productPacking);
	}

}