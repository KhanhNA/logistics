package com.tsolution._1entities.export_statement.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.enums.ExportStatementStatus;
import com.tsolution._1entities.merchant_order.MerchantOrderDetailEntity;
import com.tsolution._1entities.merchant_order.MerchantOrderEntity;
import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ExportStatementDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5019820540525299828L;
	private Long id;
	private Long fromStoreId;
	private StoreEntity fromStore;
	private Long toStoreId;
	private StoreEntity toStore;
	private String code;
	private String merchantCode;
	private Long merchantOrderId;
	private List<Long> merchantOrderIds;
	private String description;
	private ExportStatementStatus status;
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime estimatedTimeOfArrival;
	private List<ExportStatementDetailDto> exportStatementDetails;
	private ShippingPartnerEntity shippingPartner;

	public ExportStatementDto() {
	}

	public ExportStatementDto(MerchantOrderEntity merchantOrderEntity,
			List<MerchantOrderDetailEntity> merchantOrderDetails) {
		this.code = merchantOrderEntity.getCode();
		this.fromStoreId = merchantOrderEntity.getFromStore().getId();
		this.fromStore = merchantOrderEntity.getFromStore();
		this.merchantCode = merchantOrderEntity.getMerchantCode();
		this.merchantOrderId = merchantOrderEntity.getId();
		this.exportStatementDetails = new ArrayList<>();
		for (MerchantOrderDetailEntity merchantOrderDetailEntity : merchantOrderDetails) {
			ExportStatementDetailDto exportStatementDetailDto = new ExportStatementDetailDto();
			exportStatementDetailDto.setProductPackingId(merchantOrderDetailEntity.getProductPacking().getId());
			exportStatementDetailDto.setProductPackingCode(merchantOrderDetailEntity.getProductPacking().getCode());
			exportStatementDetailDto.setQuantity(merchantOrderDetailEntity.getQuantity());
			exportStatementDetailDto.setPrice(merchantOrderDetailEntity.getPrice());
			this.exportStatementDetails.add(exportStatementDetailDto);
		}
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFromStoreId() {
		return this.fromStoreId;
	}

	public void setFromStoreId(Long fromStoreId) {
		this.fromStoreId = fromStoreId;
	}

	public StoreEntity getFromStore() {
		return this.fromStore;
	}

	public void setFromStore(StoreEntity fromStore) {
		this.fromStore = fromStore;
	}

	public Long getToStoreId() {
		return this.toStoreId;
	}

	public void setToStoreId(Long toStoreId) {
		this.toStoreId = toStoreId;
	}

	public StoreEntity getToStore() {
		return this.toStore;
	}

	public void setToStore(StoreEntity toStore) {
		this.toStore = toStore;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMerchantCode() {
		return this.merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public Long getMerchantOrderId() {
		return this.merchantOrderId;
	}

	public void setMerchantOrderId(Long merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	public List<Long> getMerchantOrderIds() {
		if (this.merchantOrderIds == null) {
			this.merchantOrderIds = new ArrayList<>();
		}
		return this.merchantOrderIds;
	}

	public void setMerchantOrderIds(List<Long> merchantOrderIds) {
		this.merchantOrderIds = merchantOrderIds;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ExportStatementStatus getStatus() {
		return this.status;
	}

	public void setStatus(ExportStatementStatus status) {
		this.status = status;
	}

	public LocalDateTime getEstimatedTimeOfArrival() {
		return this.estimatedTimeOfArrival;
	}

	public void setEstimatedTimeOfArrival(LocalDateTime estimatedTimeOfArrival) {
		this.estimatedTimeOfArrival = estimatedTimeOfArrival;
	}

	public List<ExportStatementDetailDto> getExportStatementDetails() {
		return this.exportStatementDetails;
	}

	public void setExportStatementDetails(List<ExportStatementDetailDto> exportStatementDetails) {
		this.exportStatementDetails = exportStatementDetails;
	}

	public ShippingPartnerEntity getShippingPartner() {
		return this.shippingPartner;
	}

	public void setShippingPartner(ShippingPartnerEntity shippingPartner) {
		this.shippingPartner = shippingPartner;
	}

}
