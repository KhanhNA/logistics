package com.tsolution._1entities.export_statement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "export_statement_delivery")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ExportStatementDelivery extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1134917049816991376L;

	@Column(name = "export_statement_id", nullable = false)
	private Long exportStatementId;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "QR_code", nullable = false)
	private String qRCode;

	@Column(name = "description")
	private String description = "NULL";

	@Column(name = "from_address", nullable = false)
	private String fromAddress;

	@Column(name = "from_username", nullable = false)
	private String fromUsername;

	@Column(name = "from_user_contact", nullable = false)
	private String fromUserContact;

	@Column(name = "from_latitude", nullable = false)
	private BigDecimal fromLatitude;

	@Column(name = "from_longitude", nullable = false)
	private BigDecimal fromLongitude;

	@Column(name = "to_address", nullable = false)
	private String toAddress;

	@Column(name = "to_username", nullable = false)
	private String toUsername;

	@Column(name = "to_user_contact", nullable = false)
	private String toUserContact;

	@Column(name = "to_latitude", nullable = false)
	private BigDecimal toLatitude;

	@Column(name = "to_longitude", nullable = false)
	private BigDecimal toLongitude;

	@Transient
	private List<ExportStatementDeliveryDetail> exportStatementDeliveryDetails;

	public Long getExportStatementId() {
		return this.exportStatementId;
	}

	public void setExportStatementId(Long exportStatementId) {
		this.exportStatementId = exportStatementId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getQRCode() {
		return this.qRCode;
	}

	public void setQRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFromAddress() {
		return this.fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getFromUsername() {
		return this.fromUsername;
	}

	public void setFromUsername(String fromUsername) {
		this.fromUsername = fromUsername;
	}

	public String getFromUserContact() {
		return this.fromUserContact;
	}

	public void setFromUserContact(String fromUserContact) {
		this.fromUserContact = fromUserContact;
	}

	public BigDecimal getFromLatitude() {
		return this.fromLatitude;
	}

	public void setFromLatitude(BigDecimal fromLatitude) {
		this.fromLatitude = fromLatitude;
	}

	public BigDecimal getFromLongitude() {
		return this.fromLongitude;
	}

	public void setFromLongitude(BigDecimal fromLongitude) {
		this.fromLongitude = fromLongitude;
	}

	public String getToAddress() {
		return this.toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getToUsername() {
		return this.toUsername;
	}

	public void setToUsername(String toUsername) {
		this.toUsername = toUsername;
	}

	public String getToUserContact() {
		return this.toUserContact;
	}

	public void setToUserContact(String toUserContact) {
		this.toUserContact = toUserContact;
	}

	public BigDecimal getToLatitude() {
		return this.toLatitude;
	}

	public void setToLatitude(BigDecimal toLatitude) {
		this.toLatitude = toLatitude;
	}

	public BigDecimal getToLongitude() {
		return this.toLongitude;
	}

	public void setToLongitude(BigDecimal toLongitude) {
		this.toLongitude = toLongitude;
	}

	public List<ExportStatementDeliveryDetail> getExportStatementDeliveryDetails() {
		return this.exportStatementDeliveryDetails;
	}

	public void setExportStatementDeliveryDetails(List<ExportStatementDeliveryDetail> exportStatementDeliveryDetails) {
		this.exportStatementDeliveryDetails = exportStatementDeliveryDetails;
	}

	@Override
	public String toString() {
		return "ExportStatementDelivery{exportStatementId=" + this.exportStatementId + ", code=" + this.code
				+ ", qRCode=" + this.qRCode + ", description=" + this.description + ", fromAddress=" + this.fromAddress
				+ ", fromUsername=" + this.fromUsername + ", fromUserContact=" + this.fromUserContact
				+ ", fromLatitude=" + this.fromLatitude + ", fromLongitude=" + this.fromLongitude + ", toAddress="
				+ this.toAddress + ", toUsername=" + this.toUsername + ", toUserContact=" + this.toUserContact
				+ ", toLatitude=" + this.toLatitude + ", toLongitude=" + this.toLongitude + "}";
	}
}