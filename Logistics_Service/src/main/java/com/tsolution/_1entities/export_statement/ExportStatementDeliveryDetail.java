package com.tsolution._1entities.export_statement;

import java.io.Serializable;

import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.store.StoreProductPackingDetailEntity;

public class ExportStatementDeliveryDetail implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2601084074544318489L;

	private String code;
	private String qRCode;
	private String productCode;
	private String productName;
	private Long quantity;

	public ExportStatementDeliveryDetail() {
	}

	public ExportStatementDeliveryDetail(ExportStatementDetailEntity exportStatementDetail) {
		StoreProductPackingDetailEntity storeProductPackingDetail = exportStatementDetail
				.getStoreProductPackingDetail();
		this.code = storeProductPackingDetail.getCode();
		this.qRCode = storeProductPackingDetail.getqRCode();
		this.quantity = exportStatementDetail.getQuantity();
		ProductPackingEntity productPacking = exportStatementDetail.getProductPacking();
		this.productCode = productPacking.getCode();
		this.productName = productPacking.getName();
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public String getProductCode() {
		return this.productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

}