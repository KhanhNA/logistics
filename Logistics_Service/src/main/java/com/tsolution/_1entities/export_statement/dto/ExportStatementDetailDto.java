package com.tsolution._1entities.export_statement.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ExportStatementDetailDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5771991989205228776L;
	private Long id;
	private Long productPackingId;
	private String productPackingCode;
	private Long quantity;
	private BigDecimal price;

	public Long getId() {
		return this.id;
	}

	public Long getProductPackingId() {
		return this.productPackingId;
	}

	public void setProductPackingId(Long productPackingId) {
		this.productPackingId = productPackingId;
	}

	public String getProductPackingCode() {
		return this.productPackingCode;
	}

	public void setProductPackingCode(String productPackingCode) {
		this.productPackingCode = productPackingCode;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
