package com.tsolution._1entities.export_statement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.merchant_order.MerchantOrderEntity;
import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.tsolution.utils.StringUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "export_statement")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ExportStatementEntity extends SuperEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -8763262403215442607L;

	/**
	 * Không th? NULL v?i m?i tru?ng h?p
	 */

	@ManyToOne
	@JoinColumn(name = "from_store_id")
	private StoreEntity fromStore;

	/**
	 * Có th? NULL v?i tru?ng h?p là DC xu?t hàng tr? don cho Merchant
	 */

	@ManyToOne
	@JoinColumn(name = "to_store_id")
	private StoreEntity toStore;

	/**
	 * Có th? NULL v?i tru?ng h?p là FC/DC xu?t hàng cho DC
	 */

	@Column(name = "merchant_code")
	private String merchantCode;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "QR_code", nullable = false)
	private String qRCode;

	@Column(name = "description")
	private String description;

	@ManyToOne
	@JoinColumn(name = "merchant_order_id")
	private MerchantOrderEntity merchantOrder;

	@Column(name = "merchant_order_from_dc")
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private String merchantOrderFromDC;

	/**
	 * T?ng ti?n c?a don xu?t t? DC cho merchant
	 */
	@Column(name = "total")
	private BigDecimal total;

	/**
	 * 0: phi?u m?i t?o chua th?c thi, 1: phi?u dang du?c th?c thi, 2: phi?u du?c
	 * hoàn thành, 3: phi?u b? h?y
	 */
	@Column(name = "status")
	private Integer status;

	@Column(name = "export_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime exportDate;

	@Column(name = "estimated_time_of_arrival", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime estimatedTimeOfArrival;

	@OneToMany(mappedBy = "exportStatement", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ExportStatementDetailEntity> exportStatementDetails;

	@ManyToOne
	@JoinColumn(name = "shipping_partner_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ShippingPartnerEntity shippingPartner;

	public ExportStatementEntity() {
	}

	public ExportStatementEntity(Long id) {
		this.setId(id);
	}

	public StoreEntity getFromStore() {
		return this.fromStore;
	}

	public void setFromStore(StoreEntity fromStore) {
		this.fromStore = fromStore;
	}

	public StoreEntity getToStore() {
		return this.toStore;
	}

	public void setToStore(StoreEntity toStore) {
		this.toStore = toStore;
	}

	public String getMerchantCode() {
		return this.merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MerchantOrderEntity getMerchantOrder() {
		return this.merchantOrder;
	}

	public void setMerchantOrder(MerchantOrderEntity merchantOrder) {
		this.merchantOrder = merchantOrder;
	}

	public List<Long> getMerchantOrderFromDC() {
		return StringUtils.isNullOrEmpty(this.merchantOrderFromDC) ? null
				: Arrays.asList(this.merchantOrderFromDC.split("\\s*,\\s*")).stream()
						.filter(x -> !StringUtils.isNullOrEmpty(x)).mapToLong(num -> Long.parseLong(num.trim())).boxed()
						.collect(Collectors.toList());
	}

	public void setMerchantOrderFromDC(List<Long> merchantOrderIds) {
		this.merchantOrderFromDC = merchantOrderIds == null ? null
				: merchantOrderIds.stream().map(Object::toString).collect(Collectors.joining(","));
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public LocalDateTime getExportDate() {
		return this.exportDate;
	}

	public void setExportDate(LocalDateTime exportDate) {
		this.exportDate = exportDate;
	}

	public LocalDateTime getEstimatedTimeOfArrival() {
		return this.estimatedTimeOfArrival;
	}

	public void setEstimatedTimeOfArrival(LocalDateTime estimatedTimeOfArrival) {
		this.estimatedTimeOfArrival = estimatedTimeOfArrival;
	}

	public List<ExportStatementDetailEntity> getExportStatementDetails() {
		return this.exportStatementDetails;
	}

	public void setExportStatementDetails(List<ExportStatementDetailEntity> exportStatementDetails) {
		this.exportStatementDetails = exportStatementDetails;
	}

	public ShippingPartnerEntity getShippingPartner() {
		return this.shippingPartner;
	}

	public void setShippingPartner(ShippingPartnerEntity shippingPartner) {
		this.shippingPartner = shippingPartner;
	}

}