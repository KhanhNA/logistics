package com.tsolution._1entities.export_statement.dto;

import java.time.LocalDateTime;

import com.tsolution._1entities.enums.ExportStatementStatus;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderGoodsReceiveForm;

public class ExportStatementFilterDto {

	private String code;
	private Long fromStoreId;
	private Long toStoreId;
	private LocalDateTime createDate;
	private ExportStatementStatus status;
	private String type;
	private Boolean isExportSo;
	private MerchantOrderGoodsReceiveForm merchantOrderGoodsReceiveForm;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getFromStoreId() {
		return this.fromStoreId;
	}

	public void setFromStoreId(Long fromStoreId) {
		this.fromStoreId = fromStoreId;
	}

	public Long getToStoreId() {
		return this.toStoreId;
	}

	public void setToStoreId(Long toStoreId) {
		this.toStoreId = toStoreId;
	}

	public LocalDateTime getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public ExportStatementStatus getStatus() {
		return this.status;
	}

	public void setStatus(ExportStatementStatus status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getIsExportSo() {
		return this.isExportSo;
	}

	public void setIsExportSo(Boolean isExportSo) {
		this.isExportSo = isExportSo;
	}

	public MerchantOrderGoodsReceiveForm getMerchantOrderGoodsReceiveForm() {
		return this.merchantOrderGoodsReceiveForm;
	}

	public void setMerchantOrderGoodsReceiveForm(MerchantOrderGoodsReceiveForm merchantOrderGoodsReceiveForm) {
		this.merchantOrderGoodsReceiveForm = merchantOrderGoodsReceiveForm;
	}

}
