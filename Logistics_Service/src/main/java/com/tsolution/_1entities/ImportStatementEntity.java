package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "import_statement")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ImportStatementEntity extends SuperEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -6291169993564799646L;

	/**
	 * Có th? NULL v?i tru?ng h?p là FC nh?p hàng
	 */

	@ManyToOne
	@JoinColumn(name = "from_store_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity fromStore;

	/**
	 * Không th? NULL v?i m?i tru?ng h?p
	 */

	@ManyToOne
	@JoinColumn(name = "to_store_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity toStore;

	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "QR_code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String qRCode;

	@Column(name = "description")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String description;

	@Column(name = "import_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime importDate;

	@OneToOne
	@JoinColumn(name = "export_statement_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ExportStatementEntity exportStatement;

	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer status;

	@Column(name = "estimated_time_of_arrival", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime estimatedTimeOfArrival;

	@OneToMany(mappedBy = "importStatement", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ImportStatementDetailEntity> importStatementDetails;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "import_statement_id", referencedColumnName = "id")
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ImportStatementDetailPalletEntity> importStatementDetailPallets;

	@OneToOne
	@JoinColumn(name = "repacking_planning_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private RepackingPlanningEntity repackingPlanning;

	@Transient
	private Long totalQuantity;

	@Transient
	private Double totalVolume;

	public StoreEntity getFromStore() {
		return this.fromStore;
	}

	public void setFromStore(StoreEntity fromStore) {
		this.fromStore = fromStore;
	}

	public StoreEntity getToStore() {
		return this.toStore;
	}

	public void setToStore(StoreEntity toStore) {
		this.toStore = toStore;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getImportDate() {
		return this.importDate;
	}

	public void setImportDate(LocalDateTime importDate) {
		this.importDate = importDate;
	}

	public ExportStatementEntity getExportStatement() {
		return this.exportStatement;
	}

	public void setExportStatement(ExportStatementEntity exportStatement) {
		this.exportStatement = exportStatement;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public LocalDateTime getEstimatedTimeOfArrival() {
		return this.estimatedTimeOfArrival;
	}

	public void setEstimatedTimeOfArrival(LocalDateTime estimatedTimeOfArrival) {
		this.estimatedTimeOfArrival = estimatedTimeOfArrival;
	}

	public List<ImportStatementDetailEntity> getImportStatementDetails() {
		return this.importStatementDetails;
	}

	public void setImportStatementDetails(List<ImportStatementDetailEntity> importStatementDetails) {
		this.importStatementDetails = importStatementDetails;
	}

	public List<ImportStatementDetailPalletEntity> getImportStatementDetailPallets() {
		return this.importStatementDetailPallets;
	}

	public void setImportStatementDetailPallets(List<ImportStatementDetailPalletEntity> importStatementDetailPallets) {
		this.importStatementDetailPallets = importStatementDetailPallets;
	}

	public RepackingPlanningEntity getRepackingPlanning() {
		return this.repackingPlanning;
	}

	public void setRepackingPlanning(RepackingPlanningEntity repackingPlanning) {
		this.repackingPlanning = repackingPlanning;
	}

	public Long getTotalQuantity() {
		return this.totalQuantity;
	}

	public void setTotalQuantity(Long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Double getTotalVolume() {
		return this.totalVolume;
	}

	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}

}