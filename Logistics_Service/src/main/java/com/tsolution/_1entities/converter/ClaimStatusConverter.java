package com.tsolution._1entities.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.tsolution._1entities.claim.enums.ClaimStatus;

@Converter(autoApply = true)
public class ClaimStatusConverter implements AttributeConverter<ClaimStatus, Integer> {

	@Override
	public Integer convertToDatabaseColumn(ClaimStatus attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getValue();
	}

	@Override
	public ClaimStatus convertToEntityAttribute(Integer dbData) {
		if (dbData == null) {
			return null;
		}

		return Stream.of(ClaimStatus.values()).filter(c -> c.getValue().equals(dbData)).findFirst()
				.orElseThrow(IllegalArgumentException::new);
	}

}
