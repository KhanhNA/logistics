package com.tsolution._1entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.enums.LocationType;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "country_location")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryLocationEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5216454439475611096L;

	@ManyToOne
	@JoinColumn(name = "country_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CountryEntity country;

	@Column(name = "location_type")
	@Enumerated(EnumType.STRING)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocationType locationType;

	@Column(name = "code")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String name;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CountryLocationEntity parent;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Summary.CountryLocationTree.class)
	@Where(clause = "status = 1")
	private List<CountryLocationEntity> children;

	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer status;

	public CountryLocationEntity() {
	}

	public CountryLocationEntity(Long id, String code, String name, CountryLocationEntity parent) {
		this.setId(id);
		this.code = code;
		this.name = name;
		this.parent = parent;
	}

	public CountryEntity getCountry() {
		return this.country;
	}

	public void setCountry(CountryEntity country) {
		this.country = country;
	}

	public LocationType getLocationType() {
		return this.locationType;
	}

	public void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CountryLocationEntity getParent() {
		return this.parent;
	}

	public void setParent(CountryLocationEntity parent) {
		this.parent = parent;
	}

	public List<CountryLocationEntity> getChildren() {
		return this.children;
	}

	public void setChildren(List<CountryLocationEntity> children) {
		this.children = children;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}