package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "repacking_planning_detail_repacked")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class RepackingPlanningDetailRepackedEntity extends SuperEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 8596291518283741779L;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "QR_code", nullable = false)
	private String qRCode;

	@ManyToOne
	@JoinColumn(name = "repacking_planning_detail_id")
	private RepackingPlanningDetailEntity repackingPlanningDetail;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	private ProductPackingEntity productPacking;

	@Column(name = "expire_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime expireDate;

	@Column(name = "quantity", nullable = false)
	private Long quantity;

	public RepackingPlanningDetailRepackedEntity() {
	}

	public RepackingPlanningDetailRepackedEntity(Long productPackingId, LocalDateTime expireDate) {
		this.productPacking = new ProductPackingEntity(productPackingId);
		this.expireDate = expireDate;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public RepackingPlanningDetailEntity getRepackingPlanningDetail() {
		return this.repackingPlanningDetail;
	}

	public void setRepackingPlanningDetail(RepackingPlanningDetailEntity repackingPlanningDetail) {
		this.repackingPlanningDetail = repackingPlanningDetail;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.expireDate == null) ? 0 : this.expireDate.hashCode());
		result = (prime * result) + ((this.productPacking == null) ? 0 : this.productPacking.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		RepackingPlanningDetailRepackedEntity other = (RepackingPlanningDetailRepackedEntity) obj;
		if (this.expireDate == null) {
			if (other.expireDate != null) {
				return false;
			}
		} else if (!this.expireDate.equals(other.expireDate)) {
			return false;
		}
		if (this.productPacking == null) {
			if (other.productPacking != null) {
				return false;
			}
		} else if (!this.productPacking.equals(other.productPacking)) {
			return false;
		}
		return true;
	}

}