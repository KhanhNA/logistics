package com.tsolution._1entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.store.StoreEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@Entity
@Table(name = "pallet")
public class PalletEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1451581564350580014L;

	@ManyToOne
	@JoinColumn(name = "store_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity store;

	@Column(name = "code")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "QR_code")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String qRCode;

	@Column(name = "name", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String name;

	/**
	 * Nếu là dạng location của store thì phải điền step; Nếu là dạng pallet trong
	 * location thì không cần step
	 */
	@Column(name = "step")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer step;

	@ManyToOne
	@JoinColumn(name = "parent_pallet_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private PalletEntity parentPallet;

	@OneToMany(mappedBy = "parentPallet", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<PalletEntity> childPallets;

	@OneToMany(mappedBy = "pallet", fetch = FetchType.LAZY)
	@Where(clause = "quantity > 0")
	@JsonView(JsonEntityViewer.GOD.class)
	private List<PalletDetailEntity> palletDetails;

	@Column(name = "length")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double length;

	@Column(name = "width")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double width;

	@Column(name = "height")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double height;

	@Column(name = "free_volume")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double freeVolume;

	@Column(name = "total_volume")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Double totalVolume;

	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean status;

	public StoreEntity getStore() {
		return this.store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return this.code + " - " + this.name;
	}

	public Integer getStep() {
		return this.step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public PalletEntity getParentPallet() {
		return this.parentPallet;
	}

	public void setParentPallet(PalletEntity parentPallet) {
		this.parentPallet = parentPallet;
	}

	public List<PalletEntity> getChildPallets() {
		return this.childPallets;
	}

	public void setChildPallets(List<PalletEntity> childPallets) {
		this.childPallets = childPallets;
	}

	public List<PalletDetailEntity> getPalletDetails() {
		return this.palletDetails;
	}

	public void setPalletDetails(List<PalletDetailEntity> palletDetails) {
		this.palletDetails = palletDetails;
	}

	public Double getLength() {
		return this.length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Double getWidth() {
		return this.width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return this.height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getFreeVolume() {
		return this.freeVolume;
	}

	public void setFreeVolume(Double freeVolume) {
		this.freeVolume = freeVolume;
	}

	public Double getTotalVolume() {
		return this.totalVolume;
	}

	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}