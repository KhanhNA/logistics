package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.dto.RoleDto;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "users")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class UserEntity implements Serializable {

	private static final long serialVersionUID = -6533431224377312877L;

	@Id
	@Column(name = "username")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String username;

	@Column(name = "first_name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String firstName;

	@Column(name = "last_name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String lastName;

	@Column(name = "tel")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String tel;

	@Column(name = "email")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String email;

	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean status;

	@Column(name = "distributor_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long distributorId;

	@ManyToOne
	@JoinColumn(name = "distributor_id", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private DistributorEntity distributor;

	@CreatedDate
	@Column(name = "create_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView({ JsonEntityViewer.GOD.class })
	private LocalDateTime createDate;

	@CreatedBy
	@Column(name = "create_user")
	@JsonView({ JsonEntityViewer.GOD.class })
	private String createUser;

	@LastModifiedDate
	@Column(name = "update_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.GOD.class)
	private LocalDateTime updateDate;

	@LastModifiedBy
	@Column(name = "update_user")
	@JsonView(JsonEntityViewer.GOD.class)
	private String updateUser;

	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "store_user", joinColumns = @JoinColumn(name = "username", referencedColumnName = "username"), inverseJoinColumns = @JoinColumn(name = "store_id", referencedColumnName = "id"))
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<StoreEntity> stores;

	@Transient
	private List<Long> storeIds;

	@Transient
	private Long role;

	@Transient
	private List<RoleDto> roles;

	@Transient
	private Boolean enabled;

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Long getDistributorId() {
		return this.distributorId;
	}

	public void setDistributorId(Long distributorId) {
		this.distributorId = distributorId;
	}

	public DistributorEntity getDistributor() {
		return this.distributor;
	}

	public Long getRole() {
		return this.role;
	}

	public void setRole(Long role) {
		this.role = role;
	}

	public List<RoleDto> getRoles() {
		return this.roles;
	}

	public void setRoles(List<RoleDto> roles) {
		this.roles = roles;
	}

	public List<Long> getStoreIds() {
		if ((this.stores == null) || this.stores.isEmpty()) {
			return new ArrayList<>();
		}
		return this.stores.stream().map(StoreEntity::getId).collect(Collectors.toList());
	}

	public void setStoreIds(List<Long> storeIds) {
		this.storeIds = storeIds;
	}

	public List<StoreEntity> getStores() {
		return this.stores;
	}

	public void setStores(List<StoreEntity> stores) {
		this.stores = stores;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

}
