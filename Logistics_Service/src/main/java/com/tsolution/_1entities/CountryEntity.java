package com.tsolution._1entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "country")
@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 941008566295519257L;

	@Column(name = "isocode", nullable = false)
	private String isocode;

	/**
	 * 0: Không h? tr?, 1: Có h? tr?
	 */
	@Column(name = "is_support")
	private Boolean support;

	@OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
	private List<CountryDescriptionEntity> countryDescriptions;

	@OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Summary.CountryLocationTree.class)
	@Where(clause = "status = 1 AND parent_id IS NULL")
	private List<CountryLocationEntity> children;

	public CountryEntity() {
	}

	public CountryEntity(Long id, String isocode) {
		this.setId(id);
		this.isocode = isocode;
	}

	public String getIsocode() {
		return this.isocode;
	}

	public void setIsocode(String isocode) {
		this.isocode = isocode;
	}

	public Boolean getSupport() {
		return this.support;
	}

	public void setSupport(Boolean support) {
		this.support = support;
	}

	public List<CountryDescriptionEntity> getCountryDescriptions() {
		return this.countryDescriptions;
	}

	public void setCountryDescriptions(List<CountryDescriptionEntity> countryDescriptions) {
		this.countryDescriptions = countryDescriptions;
	}

	public List<CountryLocationEntity> getChildren() {
		return this.children;
	}

	public void setChildren(List<CountryLocationEntity> children) {
		this.children = children;
	}

}