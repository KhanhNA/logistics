package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "import_statement_detail")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ImportStatementDetailEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4124249101601724499L;

	@ManyToOne
	@JoinColumn(name = "import_statement_id")
	private ImportStatementEntity importStatement;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	private ProductPackingEntity productPacking;

	@Column(name = "expire_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime expireDate;

	@Column(name = "quantity", nullable = false)
	private Long quantity;

	@Column(name = "import_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime importDate;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "import_statement_detail_id", referencedColumnName = "id")
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ImportStatementDetailPalletEntity> importStatementDetailPallets;

	public ImportStatementEntity getImportStatement() {
		return this.importStatement;
	}

	public void setImportStatement(ImportStatementEntity importStatement) {
		this.importStatement = importStatement;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPackingEntity) {
		this.productPacking = productPackingEntity;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public LocalDateTime getImportDate() {
		return this.importDate;
	}

	public void setImportDate(LocalDateTime importDate) {
		this.importDate = importDate;
	}

	public List<ImportStatementDetailPalletEntity> getImportStatementDetailPallets() {
		return this.importStatementDetailPallets;
	}

	public void setImportStatementDetailPallets(List<ImportStatementDetailPalletEntity> importStatementDetailPallets) {
		this.importStatementDetailPallets = importStatementDetailPallets;
	}

}