package com.tsolution._1entities.dto;

import java.util.List;

public class DashboardTaskListPoDto {
	private String titleText;
	private List<String> labels;
	private List<DashboardTaskListPoDatasetDto> datasets;

	public String getTitleText() {
		return this.titleText;
	}

	public void setTitleText(String titleText) {
		this.titleText = titleText;
	}

	public List<String> getLabels() {
		return this.labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public List<DashboardTaskListPoDatasetDto> getDatasets() {
		return this.datasets;
	}

	public void setDatasets(List<DashboardTaskListPoDatasetDto> datasets) {
		this.datasets = datasets;
	}

	public static class DashboardTaskListPoDatasetDto {
		private String label;
		private String backgroundColor;
		private List<Long> data;

		public String getLabel() {
			return this.label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getBackgroundColor() {
			return this.backgroundColor;
		}

		public void setBackgroundColor(String backgroundColor) {
			this.backgroundColor = backgroundColor;
		}

		public List<Long> getData() {
			return this.data;
		}

		public void setData(List<Long> data) {
			this.data = data;
		}

	}
}
