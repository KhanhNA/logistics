package com.tsolution._1entities.dto;

import java.io.Serializable;

public class PoPrintDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 5754422868444314950L;
	private String article;
	private String barcode;
	private String description;
	private Double quantity;
	private Double packsize;
	private Double quantityorder;
	private String uom;
	private Double unitprice;
	private Double amount;
	private Double vat;
	private Double vatamount;
	private Double totaliclvat;

	public String getArticle() {
		return this.article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getPacksize() {
		return this.packsize;
	}

	public void setPacksize(Double packsize) {
		this.packsize = packsize;
	}

	public Double getQuantityorder() {
		return this.quantityorder;
	}

	public void setQuantityorder(Double quantityorder) {
		this.quantityorder = quantityorder;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getUnitprice() {
		return this.unitprice;
	}

	public void setUnitprice(Double unitprice) {
		this.unitprice = unitprice;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getVat() {
		return this.vat;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}

	public Double getVatamount() {
		return this.vatamount;
	}

	public void setVatamount(Double vatamount) {
		this.vatamount = vatamount;
	}

	public Double getTotaliclvat() {
		return this.totaliclvat;
	}

	public void setTotaliclvat(Double totaliclvat) {
		this.totaliclvat = totaliclvat;
	}

}
