package com.tsolution._1entities.dto;

public class AppDashboardDto {
	private Long storeId;
	private Long poWaitForImport;
	private Long soWaitForExport;
	private Long stoWaitForExport;
	private Long stoDelivery;
	private Long stoWaitForImport;
	private Long inventoryWarningOutOfDate;
	private Long inventoryOutOfDate;

	public Long getStoreId() {
		return this.storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Long getPoWaitForImport() {
		return this.poWaitForImport;
	}

	public void setPoWaitForImport(Long poWaitForImport) {
		this.poWaitForImport = poWaitForImport;
	}

	public Long getSoWaitForExport() {
		return this.soWaitForExport;
	}

	public void setSoWaitForExport(Long soWaitForExport) {
		this.soWaitForExport = soWaitForExport;
	}

	public Long getStoWaitForExport() {
		return this.stoWaitForExport;
	}

	public void setStoWaitForExport(Long stoWaitForExport) {
		this.stoWaitForExport = stoWaitForExport;
	}

	public Long getStoDelivery() {
		return this.stoDelivery;
	}

	public void setStoDelivery(Long stoDelivery) {
		this.stoDelivery = stoDelivery;
	}

	public Long getStoWaitForImport() {
		return this.stoWaitForImport;
	}

	public void setStoWaitForImport(Long stoWaitForImport) {
		this.stoWaitForImport = stoWaitForImport;
	}

	public Long getInventoryWarningOutOfDate() {
		return this.inventoryWarningOutOfDate;
	}

	public void setInventoryWarningOutOfDate(Long inventoryWarningOutOfDate) {
		this.inventoryWarningOutOfDate = inventoryWarningOutOfDate;
	}

	public Long getInventoryOutOfDate() {
		return this.inventoryOutOfDate;
	}

	public void setInventoryOutOfDate(Long inventoryOutOfDate) {
		this.inventoryOutOfDate = inventoryOutOfDate;
	}

}
