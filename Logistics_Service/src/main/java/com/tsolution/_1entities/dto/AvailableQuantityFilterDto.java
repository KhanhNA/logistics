package com.tsolution._1entities.dto;

import java.util.List;

public class AvailableQuantityFilterDto {
	private List<String> townshipCodes;
	private List<String> productPackingCodes;

	public List<String> getTownshipCodes() {
		return this.townshipCodes;
	}

	public void setTownshipCodes(List<String> townshipCodes) {
		this.townshipCodes = townshipCodes;
	}

	public List<String> getProductPackingCodes() {
		return this.productPackingCodes;
	}

	public void setProductPackingCodes(List<String> productPackingCodes) {
		this.productPackingCodes = productPackingCodes;
	}

}
