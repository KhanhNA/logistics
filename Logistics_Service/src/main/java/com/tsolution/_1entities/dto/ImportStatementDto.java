package com.tsolution._1entities.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.ImportStatementDetailPalletEntity;
import com.tsolution._1entities.enums.ImportStatementStatus;
import com.tsolution._1entities.export_statement.ExportStatementDetailEntity;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ImportStatementDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4287325072512788783L;
	private Long id;
	private String code;
	private Long fromStoreId;
	private String fromStoreCode;
	private Long toStoreId;
	private String toStoreCode;
	private Long exportStatementId;
	private Long repackingPlanningId;
	private String description;
	private ImportStatementStatus status;
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime estimatedTimeOfArrival;
	private List<ImportStatementDetailDto> importStatementDetails;
	private List<ImportStatementDetailPalletEntity> importStatementDetailPallets;
	private ShippingPartnerEntity shippingPartner;

	public ImportStatementDto() {
	}

	public ImportStatementDto(ExportStatementEntity exportStatementEntity) {
		if (exportStatementEntity != null) {
			this.fromStoreId = exportStatementEntity.getFromStore() == null ? null
					: exportStatementEntity.getFromStore().getId();
			this.toStoreId = exportStatementEntity.getToStore() == null ? null
					: exportStatementEntity.getToStore().getId();
			this.exportStatementId = exportStatementEntity.getId();
			this.estimatedTimeOfArrival = exportStatementEntity.getEstimatedTimeOfArrival();
			this.description = exportStatementEntity.getDescription();
			List<ExportStatementDetailEntity> exportStatementDetails = exportStatementEntity
					.getExportStatementDetails();
			Map<Pair<Long, LocalDateTime>, Long> map = exportStatementDetails.stream()
					.collect(Collectors.groupingBy(esd -> Pair.of(esd.getProductPacking().getId(), esd.getExpireDate()),
							Collectors.summingLong(ExportStatementDetailEntity::getQuantity)));
			this.importStatementDetails = map.entrySet().stream()
					.map(e -> new ImportStatementDetailDto(e.getKey().getLeft(), e.getKey().getRight(), e.getValue()))
					.collect(Collectors.toList());
		}
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getFromStoreId() {
		return this.fromStoreId;
	}

	public void setFromStoreId(Long fromStoreId) {
		this.fromStoreId = fromStoreId;
	}

	public String getFromStoreCode() {
		return this.fromStoreCode;
	}

	public void setFromStoreCode(String fromStoreCode) {
		this.fromStoreCode = fromStoreCode;
	}

	public Long getToStoreId() {
		return this.toStoreId;
	}

	public void setToStoreId(Long toStoreId) {
		this.toStoreId = toStoreId;
	}

	public String getToStoreCode() {
		return this.toStoreCode;
	}

	public void setToStoreCode(String toStoreCode) {
		this.toStoreCode = toStoreCode;
	}

	public Long getExportStatementId() {
		return this.exportStatementId;
	}

	public void setExportStatementId(Long exportStatementId) {
		this.exportStatementId = exportStatementId;
	}

	public Long getRepackingPlanningId() {
		return this.repackingPlanningId;
	}

	public void setRepackingPlanningId(Long repackingPlanningId) {
		this.repackingPlanningId = repackingPlanningId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ImportStatementStatus getStatus() {
		return this.status;
	}

	public void setStatus(ImportStatementStatus status) {
		this.status = status;
	}

	public LocalDateTime getEstimatedTimeOfArrival() {
		return this.estimatedTimeOfArrival;
	}

	public void setEstimatedTimeOfArrival(LocalDateTime estimatedTimeOfArrival) {
		this.estimatedTimeOfArrival = estimatedTimeOfArrival;
	}

	public List<ImportStatementDetailDto> getImportStatementDetails() {
		return this.importStatementDetails;
	}

	public void setImportStatementDetails(List<ImportStatementDetailDto> importStatementDetails) {
		this.importStatementDetails = importStatementDetails;
	}

	public List<ImportStatementDetailPalletEntity> getImportStatementDetailPallets() {
		return this.importStatementDetailPallets;
	}

	public void setImportStatementDetailPallets(List<ImportStatementDetailPalletEntity> importStatementDetailPallets) {
		this.importStatementDetailPallets = importStatementDetailPallets;
	}

	public ShippingPartnerEntity getShippingPartner() {
		return this.shippingPartner;
	}

	public void setShippingPartner(ShippingPartnerEntity shippingPartner) {
		this.shippingPartner = shippingPartner;
	}

}
