package com.tsolution._1entities.dto;

import java.util.Date;

import com.tsolution._1entities.enums.PoStatus;

public class PoDto {
	private Long storeId;
	private Long manufacturerId;
	private Long distributorId;
	private String text;
	private PoStatus dateType;
	private Date fromDate;
	private Date toDate;
	private PoStatus status;

	public Long getStoreId() {
		return this.storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Long getManufacturerId() {
		return this.manufacturerId;
	}

	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public Long getDistributorId() {
		return this.distributorId;
	}

	public void setDistributorId(Long distributorId) {
		this.distributorId = distributorId;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public PoStatus getDateType() {
		return this.dateType;
	}

	public void setDateType(PoStatus dateType) {
		this.dateType = dateType;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public PoStatus getStatus() {
		return this.status;
	}

	public void setStatus(PoStatus status) {
		this.status = status;
	}

}
