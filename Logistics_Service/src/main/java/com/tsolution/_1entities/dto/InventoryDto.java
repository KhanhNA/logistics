package com.tsolution._1entities.dto;

import java.io.Serializable;
import java.util.List;

import com.tsolution._1entities.enums.ExpireDateColors;

public class InventoryDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7545032381082141721L;
	private String acceptLanguage;
	private Long storeId;
	private Long distributorId;
	private Long manufacturerId;
	private List<Long> exceptProductPackingIds;
	private String productPackingCode;
	private String productPackingName;
	private ExpireDateColors expireDateColor;

	public String getAcceptLanguage() {
		return this.acceptLanguage;
	}

	public void setAcceptLanguage(String acceptLanguage) {
		this.acceptLanguage = acceptLanguage;
	}

	public Long getStoreId() {
		return this.storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Long getDistributorId() {
		return this.distributorId;
	}

	public void setDistributorId(Long distributorId) {
		this.distributorId = distributorId;
	}

	public Long getManufacturerId() {
		return this.manufacturerId;
	}

	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public List<Long> getExceptProductPackingIds() {
		return this.exceptProductPackingIds;
	}

	public void setExceptProductPackingIds(List<Long> exceptProductPackingIds) {
		this.exceptProductPackingIds = exceptProductPackingIds;
	}

	public String getProductPackingCode() {
		return this.productPackingCode;
	}

	public void setProductPackingCode(String productPackingCode) {
		this.productPackingCode = productPackingCode;
	}

	public String getProductPackingName() {
		return this.productPackingName;
	}

	public void setProductPackingName(String productPackingName) {
		this.productPackingName = productPackingName;
	}

	public ExpireDateColors getExpireDateColor() {
		return this.expireDateColor;
	}

	public void setExpireDateColor(ExpireDateColors expireDateColor) {
		this.expireDateColor = expireDateColor;
	}

}
