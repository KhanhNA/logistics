package com.tsolution._1entities.dto;

import java.util.Date;

public class ObjectTwoColumn extends Object {
	private Long id;
	private Date expireDate;

	public ObjectTwoColumn(Long id, Date date) {
		super();
		this.id = id;
		this.expireDate = date;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

}
