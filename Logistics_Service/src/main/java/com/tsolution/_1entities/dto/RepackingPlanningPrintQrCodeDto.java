package com.tsolution._1entities.dto;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;

import com.google.common.xml.XmlEscapers;
import com.tsolution._1entities.RepackingPlanningDetailRepackedEntity;

public class RepackingPlanningPrintQrCodeDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -585368242202289135L;
	private String productPackingName;
	private String qRCode;
	private String expireDate;

	public RepackingPlanningPrintQrCodeDto() {
	}

	public RepackingPlanningPrintQrCodeDto(RepackingPlanningDetailRepackedEntity repackingPlanningDetailRepacked) {
		this.productPackingName = repackingPlanningDetailRepacked.getProductPacking().getName();
		this.qRCode = XmlEscapers.xmlAttributeEscaper().escape(repackingPlanningDetailRepacked.getqRCode());
		this.expireDate = repackingPlanningDetailRepacked.getRepackingPlanningDetail().getExpireDate()
				.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	public String getProductPackingName() {
		return this.productPackingName;
	}

	public void setProductPackingName(String productPackingName) {
		this.productPackingName = productPackingName;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public String getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

}
