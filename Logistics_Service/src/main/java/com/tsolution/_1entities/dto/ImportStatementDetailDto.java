package com.tsolution._1entities.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.ImportStatementDetailEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ImportStatementDetailDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 2632686201787177650L;
	private Long productPackingId;
	private ProductPackingEntity productPacking;
	private Long quantity;

	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime expireDate;

	public ImportStatementDetailDto() {
	}

	public ImportStatementDetailDto(Long productPackingId, LocalDateTime expireDate) {
		this.productPackingId = productPackingId;
		this.expireDate = expireDate;
	}

	public ImportStatementDetailDto(Long productPackingId, LocalDateTime expireDate, Long quantity) {
		this.productPackingId = productPackingId;
		this.expireDate = expireDate;
		this.quantity = quantity;
	}

	public ImportStatementDetailDto(ImportStatementDetailEntity importStatementDetail) {
		if (importStatementDetail != null) {
			this.productPackingId = importStatementDetail.getProductPacking() == null ? null
					: importStatementDetail.getProductPacking().getId();
			this.expireDate = importStatementDetail.getExpireDate();
			this.quantity = importStatementDetail.getQuantity();
		}
	}

	public Long getProductPackingId() {
		return this.productPackingId;
	}

	public void setProductPackingId(Long productPackingId) {
		this.productPackingId = productPackingId;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.expireDate == null) ? 0 : this.expireDate.hashCode());
		result = (prime * result) + ((this.productPackingId == null) ? 0 : this.productPackingId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ImportStatementDetailDto other = (ImportStatementDetailDto) obj;
		if (this.expireDate == null) {
			if (other.expireDate != null) {
				return false;
			}
		} else if (!this.expireDate.equals(other.expireDate)) {
			return false;
		}
		if (this.productPackingId == null) {
			if (other.productPackingId != null) {
				return false;
			}
		} else if (!this.productPackingId.equals(other.productPackingId)) {
			return false;
		}
		return true;
	}

}
