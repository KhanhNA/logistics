package com.tsolution._1entities.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CountryLocationDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 9200860706167022114L;

	@Id
	private Long id;
	private Long countryId;
	private String countryCode;
	private Long regionId;
	private String regionCode;
	private String regionName;
	private Long regionCountryId;
	private Long districtId;
	private String districtCode;
	private String districtName;
	private Long districtParentId;
	private Long townshipId;
	private String townshipCode;
	private String townshipName;
	private Long townshipParentId;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public String getRegionCode() {
		return this.regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public Long getRegionCountryId() {
		return this.regionCountryId;
	}

	public void setRegionCountryId(Long regionCountryId) {
		this.regionCountryId = regionCountryId;
	}

	public Long getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public String getDistrictCode() {
		return this.districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getDistrictName() {
		return this.districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Long getDistrictParentId() {
		return this.districtParentId;
	}

	public void setDistrictParentId(Long districtParentId) {
		this.districtParentId = districtParentId;
	}

	public Long getTownshipId() {
		return this.townshipId;
	}

	public void setTownshipId(Long townshipId) {
		this.townshipId = townshipId;
	}

	public String getTownshipCode() {
		return this.townshipCode;
	}

	public void setTownshipCode(String townshipCode) {
		this.townshipCode = townshipCode;
	}

	public String getTownshipName() {
		return this.townshipName;
	}

	public void setTownshipName(String townshipName) {
		this.townshipName = townshipName;
	}

	public Long getTownshipParentId() {
		return this.townshipParentId;
	}

	public void setTownshipParentId(Long townshipParentId) {
		this.townshipParentId = townshipParentId;
	}

}
