package com.tsolution._1entities.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.tsolution._1entities.enums.ExpireDateColors;

public class PalletDetailDto implements Serializable {
	private static final long serialVersionUID = 8793535820695819665L;

	private Long storeId;
	private Long distributorId;
	private String text;
	private String productPackingCode;
	private String productPackingName;
	private Long palletId;
	private Boolean isIncludeProductPackingIdExpireDate;
	private List<Long> productPackingIds;
	private List<String> productPackingExpireDates;
	private List<Integer> palletSteps;
	private String username;
	private Boolean status;
	private Integer step;
	private ExpireDateColors expireDateColor;

	public Long getStoreId() {
		return this.storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Long getDistributorId() {
		return this.distributorId;
	}

	public void setDistributorId(Long distributorId) {
		this.distributorId = distributorId;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getProductPackingCode() {
		return this.productPackingCode;
	}

	public void setProductPackingCode(String productPackingCode) {
		this.productPackingCode = productPackingCode;
	}

	public String getProductPackingName() {
		return this.productPackingName;
	}

	public void setProductPackingName(String productPackingName) {
		this.productPackingName = productPackingName;
	}

	public Long getPalletId() {
		return this.palletId;
	}

	public void setPalletId(Long palletId) {
		this.palletId = palletId;
	}

	public Boolean getIsIncludeProductPackingIdExpireDate() {
		return this.isIncludeProductPackingIdExpireDate;
	}

	public void setIsIncludeProductPackingIdExpireDate(Boolean isIncludeProductPackingIdExpireDate) {
		this.isIncludeProductPackingIdExpireDate = isIncludeProductPackingIdExpireDate;
	}

	public List<Long> getProductPackingIds() {
		return this.productPackingIds;
	}

	public void setProductPackingIds(List<Long> productPackingIds) {
		this.productPackingIds = productPackingIds;
	}

	public List<String> getProductPackingExpireDates() {
		if (this.productPackingExpireDates == null) {
			this.productPackingExpireDates = new ArrayList<>();
		}
		return this.productPackingExpireDates;
	}

	public void setProductPackingExpireDates(List<String> productPackingExpireDates) {
		this.productPackingExpireDates = productPackingExpireDates;
	}

	public List<Integer> getPalletSteps() {
		return this.palletSteps;
	}

	public void setPalletSteps(List<Integer> palletSteps) {
		this.palletSteps = palletSteps;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getStep() {
		return this.step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public ExpireDateColors getExpireDateColor() {
		return this.expireDateColor;
	}

	public void setExpireDateColor(ExpireDateColors expireDateColor) {
		this.expireDateColor = expireDateColor;
	}

}
