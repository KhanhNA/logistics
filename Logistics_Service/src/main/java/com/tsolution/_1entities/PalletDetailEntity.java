package com.tsolution._1entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.importpo.ImportPoDetailEntity;
import com.tsolution._1entities.importpo.ImportPoDetailPalletEntity;
import com.tsolution._1entities.store.StoreProductPackingDetailEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "pallet_detail")
@JsonIdentityInfo(generator = JSOGGenerator.class)
@Entity
@JsonInclude(Include.NON_NULL)
public class PalletDetailEntity extends SuperEntity implements Serializable {

	@Transient
	public static final String FIND_GROUP_BY_PRODUCT_PACKING_AND_EXPIRE_DATE = " SELECT max(pd.id) id, pd.pallet_id, pd.product_id, "
			+ "			 pd.product_packing_id, SUM(quantity) quantity, SUM(org_quantity) org_quantity, "
			+ "			 date(expire_date) expire_date, null store_product_packing_detail_id, "
			+ " 		 null create_date, null create_user, null update_date, null update_user "
			+ " FROM pallet_detail pd "
			+ " 	 JOIN pallet p ON pd.pallet_id = p.id AND p.store_id = :storeId AND (:step IS NULL OR p.step = :step) "
			+ " WHERE product_packing_id = :productPackingId and date(expire_date) = date(:expireDate) and quantity > 0 "
			+ " GROUP BY pd.pallet_id, pd.product_id, pd.product_packing_id, date(expire_date) ";

	@Transient
	public static final String FIND_BY_PRODUCT_PACKING_AND_EXPIRE_DATE = " SELECT pd.id id, pd.pallet_id, pd.product_id, "
			+ "			 pd.product_packing_id, quantity, org_quantity, date(expire_date) expire_date, "
			+ "			 pd.store_product_packing_detail_id, pd.create_date, pd.create_user, pd.update_date, pd.update_user "
			+ " 	 FROM pallet_detail pd "
			+ " 	 JOIN pallet p ON pd.pallet_id = p.id AND p.store_id = :storeId AND (:step IS NULL OR p.step = :step) "
			+ " WHERE product_packing_id = :productPackingId and date(expire_date) = date(:expireDate) and quantity > 0 ";

	/**
	 *
	 */
	private static final long serialVersionUID = 4839917617249442171L;

	@ManyToOne
	@JoinColumn(name = "pallet_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private PalletEntity pallet;

	@ManyToOne
	@JoinColumn(name = "product_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductEntity product;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingEntity productPacking;

	@Column(name = "quantity", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long quantity;

	@Column(name = "org_quantity", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long orgQuantity;

	@Column(name = "expire_date", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime expireDate;

	@OneToOne(mappedBy = "palletDetail")
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private ImportPoDetailPalletEntity importPoDetailPallet;

	@OneToOne
	@JoinColumn(name = "store_product_packing_detail_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreProductPackingDetailEntity storeProductPackingDetail;

	@Transient
	private Long inventory;

	@Transient
	private String className;

	public PalletDetailEntity() {
		this.inventory = this.quantity;
	}

	public PalletDetailEntity(PalletEntity pallet, ImportPoDetailEntity importPoDetailEntity, Long quantity) {
		super();
		this.setPallet(pallet);
		this.setProduct(importPoDetailEntity.getProduct());
		this.setProductPacking(importPoDetailEntity.getProductPacking());
		this.setQuantity(quantity);
		this.setOrgQuantity(quantity);
		this.setExpireDate(importPoDetailEntity.getExpireDate());
		this.inventory = quantity;
	}

	public PalletDetailEntity(PalletEntity pallet, ProductEntity product, ProductPackingEntity productPacking,
			Long quantity, LocalDateTime expireDate, StoreProductPackingDetailEntity storeProductPackingDetail) {
		super();
		this.setPallet(pallet);
		this.setProduct(product);
		this.setProductPacking(productPacking);
		this.setQuantity(quantity);
		this.setOrgQuantity(quantity);
		this.setExpireDate(expireDate);
		this.setStoreProductPackingDetail(storeProductPackingDetail);
		this.inventory = quantity;
	}

	public PalletEntity getPallet() {
		return this.pallet;
	}

	public void setPallet(PalletEntity pallet) {
		this.pallet = pallet;
	}

	public ProductEntity getProduct() {
		return this.product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Long getOrgQuantity() {
		return this.orgQuantity;
	}

	public void setOrgQuantity(Long orgQuantity) {
		this.orgQuantity = orgQuantity;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public ImportPoDetailPalletEntity getImportPoDetailPallet() {
		return this.importPoDetailPallet;
	}

	public void setImportPoDetailPallet(ImportPoDetailPalletEntity importPoDetailPallet) {
		this.importPoDetailPallet = importPoDetailPallet;
	}

	public StoreProductPackingDetailEntity getStoreProductPackingDetail() {
		return this.storeProductPackingDetail;
	}

	public void setStoreProductPackingDetail(StoreProductPackingDetailEntity storeProductPackingDetail) {
		this.storeProductPackingDetail = storeProductPackingDetail;
	}

	public Long getInventory() {
		return this.inventory;
	}

	public String getClassName() {
		return this.className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

}