package com.tsolution._1entities.importpo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.store.StoreEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "import_po")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ImportPoEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4124305577591390544L;

	@ManyToOne
	@JoinColumn(name = "manufacturer_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ManufacturerEntity manufacturer;

	@ManyToOne
	@JoinColumn(name = "distributor_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private DistributorEntity distributor;

	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "QR_code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String qRCode;

	@ManyToOne
	@JoinColumn(name = "store_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity store;

	@Column(name = "description")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String description = "NULL";

	@OneToOne
	@JoinColumn(name = "po_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private PoEntity po;

	@OneToMany(mappedBy = "importPo", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ImportPoDetailEntity> importPoDetails;

	@OneToMany(mappedBy = "importPo", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ImportPoQcEntity> importPoQcs;

	/**
	 * 0: Mới nhập, 1: Đã lập Repacking, 2: Đã thực nhập
	 */
	@Column(name = "status", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer status;

	public ManufacturerEntity getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(ManufacturerEntity manufacturer) {
		this.manufacturer = manufacturer;
	}

	public DistributorEntity getDistributor() {
		return this.distributor;
	}

	public void setDistributor(DistributorEntity distributor) {
		this.distributor = distributor;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public StoreEntity getStore() {
		return this.store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PoEntity getPo() {
		return this.po;
	}

	public void setPo(PoEntity po) {
		this.po = po;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<ImportPoDetailEntity> getImportPoDetails() {
		return this.importPoDetails;
	}

	public void setImportPoDetails(List<ImportPoDetailEntity> importPoDetails) {
		this.importPoDetails = importPoDetails;
	}

	public List<ImportPoQcEntity> getImportPoQcs() {
		if (this.importPoQcs == null) {
			this.importPoQcs = new ArrayList<>();
		}
		return this.importPoQcs;
	}

	public void setImportPoQcs(List<ImportPoQcEntity> importPoQcs) {
		this.importPoQcs = importPoQcs;
	}

}