package com.tsolution._1entities.importpo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "import_po_qc")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ImportPoQcEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8776481386043704797L;

	@ManyToOne
	@JoinColumn(name = "po_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private PoEntity po;

	@ManyToOne
	@JoinColumn(name = "import_po_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ImportPoEntity importPo;

	@Column(name = "url")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String url;

	public PoEntity getPo() {
		return this.po;
	}

	public void setPo(PoEntity po) {
		this.po = po;
	}

	public ImportPoEntity getImportPo() {
		return this.importPo;
	}

	public void setImportPo(ImportPoEntity importPo) {
		this.importPo = importPo;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}