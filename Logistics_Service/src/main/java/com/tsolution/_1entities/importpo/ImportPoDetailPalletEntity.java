package com.tsolution._1entities.importpo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "import_po_detail_pallet")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ImportPoDetailPalletEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1405716629112396020L;

	@ManyToOne
	@JoinColumn(name = "import_po_id")
	private ImportPoEntity importPo;

	@ManyToOne
	@JoinColumn(name = "import_po_detail_id")
	private ImportPoDetailEntity importPoDetail;

	@ManyToOne
	@JoinColumn(name = "pallet_id")
	private PalletEntity pallet;

	@OneToOne
	@JoinColumn(name = "pallet_detail_id")
	private PalletDetailEntity palletDetail;

	@Column(name = "quantity", nullable = false)
	private Long quantity;

	public ImportPoDetailPalletEntity() {
		super();
	}

	public ImportPoDetailPalletEntity(ImportPoEntity importPo, ImportPoDetailEntity importPoDetail, PalletEntity pallet,
			PalletDetailEntity palletDetail, Long quantity) {
		super();
		this.importPo = importPo;
		this.importPoDetail = importPoDetail;
		this.pallet = pallet;
		this.palletDetail = palletDetail;
		this.quantity = quantity;
	}

	public ImportPoEntity getImportPo() {
		return this.importPo;
	}

	public void setImportPo(ImportPoEntity importPo) {
		this.importPo = importPo;
	}

	public ImportPoDetailEntity getImportPoDetail() {
		return this.importPoDetail;
	}

	public void setImportPoDetail(ImportPoDetailEntity importPoDetail) {
		this.importPoDetail = importPoDetail;
	}

	public PalletEntity getPallet() {
		return this.pallet;
	}

	public void setPallet(PalletEntity pallet) {
		this.pallet = pallet;
	}

	public PalletDetailEntity getPalletDetail() {
		return this.palletDetail;
	}

	public void setPalletDetail(PalletDetailEntity palletDetail) {
		this.palletDetail = palletDetail;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

}