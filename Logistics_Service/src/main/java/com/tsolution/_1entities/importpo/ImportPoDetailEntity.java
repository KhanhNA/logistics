package com.tsolution._1entities.importpo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.PoDetailEntity;
import com.tsolution._1entities.ProductEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "import_po_detail")
@JsonIdentityInfo(generator = JSOGGenerator.class)

public class ImportPoDetailEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3962209816583699816L;

	@ManyToOne
	@JoinColumn(name = "import_po_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ImportPoEntity importPo;

	@ManyToOne
	@JoinColumn(name = "product_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductEntity product;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingEntity productPacking;

	@Column(name = "quantity", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long quantity;

	@ManyToOne
	@JoinColumn(name = "product_packing_price_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingPriceEntity productPackingPrice;

	@ManyToOne
	@JoinColumn(name = "po_detail_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private PoDetailEntity poDetail;

	@Column(name = "expire_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime expireDate;

	@OneToMany(mappedBy = "importPoDetail", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<ImportPoDetailPalletEntity> importPoDetailPallets;

	public ImportPoEntity getImportPo() {
		return this.importPo;
	}

	public void setImportPo(ImportPoEntity importPo) {
		this.importPo = importPo;
	}

	public ProductEntity getProduct() {
		return this.product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public ProductPackingPriceEntity getProductPackingPrice() {
		return this.productPackingPrice;
	}

	public void setProductPackingPrice(ProductPackingPriceEntity productPackingPrice) {
		this.productPackingPrice = productPackingPrice;
	}

	public PoDetailEntity getPoDetail() {
		return this.poDetail;
	}

	public void setPoDetail(PoDetailEntity poDetail) {
		this.poDetail = poDetail;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public List<ImportPoDetailPalletEntity> getImportPoDetailPallets() {
		return this.importPoDetailPallets;
	}

	public void setImportPoDetailPallets(List<ImportPoDetailPalletEntity> importPoDetailPallets) {
		this.importPoDetailPallets = importPoDetailPallets;
	}

}