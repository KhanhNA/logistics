package com.tsolution._1entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;

@Entity
@Table(name = "currency_exchange_rate")
public class CurrencyExchangeRateEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8232695282712680284L;

	@ManyToOne
	@JoinColumn(name = "from_currency_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CurrencyEntity fromCurrency;

	@ManyToOne
	@JoinColumn(name = "to_currency_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CurrencyEntity toCurrency;

	@Column(name = "exchange_rate", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal exchangeRate;

	@Column(name = "from_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime fromDate;

	@Column(name = "to_date")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime toDate;

	public CurrencyEntity getFromCurrency() {
		return this.fromCurrency;
	}

	public void setFromCurrency(CurrencyEntity fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public CurrencyEntity getToCurrency() {
		return this.toCurrency;
	}

	public void setToCurrency(CurrencyEntity toCurrency) {
		this.toCurrency = toCurrency;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public LocalDateTime getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDateTime getToDate() {
		return this.toDate;
	}

	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}

}
