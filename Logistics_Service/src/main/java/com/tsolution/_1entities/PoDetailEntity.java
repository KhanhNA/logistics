package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "po_detail")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class PoDetailEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6908341398160984781L;

	@ManyToOne
	@JoinColumn(name = "po_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private PoEntity po;

	@ManyToOne
	@JoinColumn(name = "product_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductEntity product;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingEntity productPacking;

	@ManyToOne
	@JoinColumn(name = "product_packing_price_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingPriceEntity productPackingPrice;

	@Column(name = "quantity", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long quantity;

	@Column(name = "vat")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer vat;

	public PoEntity getPo() {
		return this.po;
	}

	public void setPo(PoEntity po) {
		this.po = po;
	}

	public ProductEntity getProduct() {
		return this.product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public ProductPackingPriceEntity getProductPackingPrice() {
		return this.productPackingPrice;
	}

	public void setProductPackingPrice(ProductPackingPriceEntity productPackingPrice) {
		this.productPackingPrice = productPackingPrice;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Integer getVat() {
		return this.vat;
	}

	public void setVat(Integer vat) {
		this.vat = vat;
	}

}