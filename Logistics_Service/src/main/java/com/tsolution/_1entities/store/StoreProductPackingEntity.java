package com.tsolution._1entities.store;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.ProductEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DeserializeDateHandler;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "store_product_packing")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class StoreProductPackingEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5605080636443054104L;

	@ManyToOne
	@JoinColumn(name = "store_id")
	private StoreEntity store;

	@Column(name = "product_packing_id", insertable = false, updatable = false)
	private Long productPackingId;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	private ProductPackingEntity productPacking;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private ProductEntity product;

	@Column(name = "total_quantity", nullable = false)
	private Long totalQuantity;

	@Column(name = "expire_date", nullable = false)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonDeserialize(using = DeserializeDateHandler.class)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	private LocalDateTime expireDate;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "QR_code", nullable = false)
	private String qRCode;

	@Transient
	private String className;

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public ProductEntity getProduct() {
		return this.product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public LocalDateTime getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(LocalDateTime expireDate) {
		this.expireDate = expireDate;
	}

	public String getClassName() {
		return this.className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public StoreEntity getStore() {
		return this.store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public Long getProductPackingId() {
		if (this.productPackingId == null) {
			return this.productPacking == null ? 0 : this.productPacking.getId();
		}
		return this.productPackingId;
	}

	public void setProductPackingId(Long productPackingId) {
		this.productPackingId = productPackingId;
	}

	public Long getTotalQuantity() {
		return this.totalQuantity;
	}

	public void setTotalQuantity(Long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

}