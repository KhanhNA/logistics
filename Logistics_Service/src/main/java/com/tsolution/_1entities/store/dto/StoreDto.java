package com.tsolution._1entities.store.dto;

import java.io.Serializable;
import java.util.List;

public class StoreDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7147064487758279924L;

	private Boolean status;
	private String text;
	private Long countryId;
	private Long regionId;
	private Long districtId;
	private Long townshipId;
	private Boolean isDc;
	private List<Long> exceptIds;
	private String currentUser;

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public Long getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public Long getTownshipId() {
		return this.townshipId;
	}

	public void setTownshipId(Long townshipId) {
		this.townshipId = townshipId;
	}

	public Boolean getIsDc() {
		return this.isDc;
	}

	public void setIsDc(Boolean isDc) {
		this.isDc = isDc;
	}

	public List<Long> getExceptIds() {
		return this.exceptIds;
	}

	public void setExceptIds(List<Long> exceptIds) {
		this.exceptIds = exceptIds;
	}

	public String getCurrentUser() {
		return this.currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

}
