package com.tsolution._1entities.store;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.CountryLocationEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "store_location_charge")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class StoreLocationChargeEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8776481386043704797L;

	@ManyToOne
	@JoinColumn(name = "store_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity store;

	@ManyToOne
	@JoinColumn(name = "country_location_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CountryLocationEntity countryLocation;

	public StoreEntity getStore() {
		return this.store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public CountryLocationEntity getCountryLocation() {
		return this.countryLocation;
	}

	public void setCountryLocation(CountryLocationEntity countryLocation) {
		this.countryLocation = countryLocation;
	}

}