package com.tsolution._1entities.store.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AvailableQuantityDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4464998660664567443L;

	@Id
	private String productPackingCode;
	private Long quantity;

	public String getProductPackingCode() {
		return this.productPackingCode;
	}

	public void setProductPackingCode(String productPackingCode) {
		this.productPackingCode = productPackingCode;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

}
