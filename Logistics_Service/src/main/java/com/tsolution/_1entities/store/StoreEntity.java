package com.tsolution._1entities.store;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.CountryEntity;
import com.tsolution._1entities.CountryLocationEntity;
import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._1entities.LanguageEntity;
import com.tsolution._1entities.UserEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "store")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class StoreEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7537020479587805827L;

	@Transient
	public static final String GET_STORES_EXISTS_NOT_ENOUGH_QUANTITY = " FROM store s WHERE s.id in (:storeIds) "
			+ " AND EXISTS (SELECT 1 FROM merchant_order mo WHERE mo.from_store_id = s.id AND mo.status = :merchantOrderStatus) ";

	@Transient
	public static final String NEAR_HERE = " from store s where (:isDC IS NULL OR s.is_DC = :isDC) AND calcDistanceByLatLng(:currentLat, :currentLng, s.lat, s.lng) < :maxDistance ";

	@ManyToOne
	@JoinColumn(name = "country_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CountryEntity country;

	@ManyToOne
	@JoinColumn(name = "region_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CountryLocationEntity region;

	@ManyToOne
	@JoinColumn(name = "district_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CountryLocationEntity district;

	@ManyToOne
	@JoinColumn(name = "township_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CountryLocationEntity township;

	@ManyToOne
	@JoinColumn(name = "language_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LanguageEntity language;

	@ManyToOne
	@JoinColumn(name = "currency_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private CurrencyEntity currency;

	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "name", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String name;

	@Column(name = "domain_name")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String domainName;

	@Column(name = "in_business_since", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime inBusinessSince;

	@Column(name = "out_business_since")
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime outBusinessSince;

	@Column(name = "email")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String email;

	@Column(name = "logo")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String logo;

	@Column(name = "floor_no")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer floorNo;

	@Column(name = "address")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String address;

	@Column(name = "phone")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String phone;

	@Column(name = "postal_code")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String postalCode;

	@ManyToOne
	@JoinColumn(name = "provide_store_id")
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private StoreEntity provideStore;

	/**
	 * 0: Là kho t?ng FC, 1: Là kho DC
	 */
	@Column(name = "is_DC")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private boolean dc;

	/**
	 * 0: không ho?t d?ng, 1: ho?t d?ng
	 */
	@Column(name = "status", columnDefinition = "boolean default false")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Boolean status;

	@Column(name = "lat", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal lat;

	@Column(name = "lng", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal lng;

	@OneToMany(mappedBy = "store", fetch = FetchType.LAZY)
	@Where(clause = "total_quantity > 0")
	@JsonView(JsonEntityViewer.GOD.class)
	private List<StoreProductPackingEntity> storeProductPackings;

	@OneToMany(mappedBy = "store", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<StoreLocationChargeEntity> storeLocationCharges;

	@OneToMany(mappedBy = "store", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<StoreFileEntity> storeFiles;

	@OneToMany(mappedBy = "store", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.GOD.class)
	private List<StoreUserEntity> storeUsers;

	@Column(name = "delivery_frequency", nullable = true)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer deliveryFrequency;

	@Column(name = "length", nullable = true)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal length;

	@Column(name = "width", nullable = true)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal width;

	@Column(name = "height", nullable = true)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal height;

	@Column(name = "area", nullable = true)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal area;

	@Column(name = "username")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String username;

	@OneToOne
	@JoinColumn(name = "username", insertable = false, updatable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private UserEntity user;

	public StoreEntity() {
	}

	public StoreEntity(Long id) {
		this.setId(id);
	}

	public CountryEntity getCountry() {
		return this.country;
	}

	public void setCountry(CountryEntity country) {
		this.country = country;
	}

	public CountryLocationEntity getRegion() {
		return this.region;
	}

	public void setRegion(CountryLocationEntity region) {
		this.region = region;
	}

	public CountryLocationEntity getDistrict() {
		return this.district;
	}

	public void setDistrict(CountryLocationEntity district) {
		this.district = district;
	}

	public CountryLocationEntity getTownship() {
		return this.township;
	}

	public void setTownship(CountryLocationEntity township) {
		this.township = township;
	}

	public LanguageEntity getLanguage() {
		return this.language;
	}

	public void setLanguage(LanguageEntity language) {
		this.language = language;
	}

	public CurrencyEntity getCurrency() {
		return this.currency;
	}

	public void setCurrency(CurrencyEntity currency) {
		this.currency = currency;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomainName() {
		return this.domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public LocalDateTime getInBusinessSince() {
		return this.inBusinessSince;
	}

	public void setInBusinessSince(LocalDateTime inBusinessSince) {
		this.inBusinessSince = inBusinessSince;
	}

	public LocalDateTime getOutBusinessSince() {
		return this.outBusinessSince;
	}

	public void setOutBusinessSince(LocalDateTime outBusinessSince) {
		this.outBusinessSince = outBusinessSince;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Integer getFloorNo() {
		return this.floorNo;
	}

	public void setFloorNo(Integer floorNo) {
		this.floorNo = floorNo;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public StoreEntity getProvideStore() {
		return this.provideStore;
	}

	public void setProvideStore(StoreEntity provideStore) {
		this.provideStore = provideStore;
	}

	public boolean isDc() {
		return this.dc;
	}

	public void setDc(boolean dc) {
		this.dc = dc;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public BigDecimal getLat() {
		return this.lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return this.lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public Integer getDeliveryFrequency() {
		return this.deliveryFrequency;
	}

	public void setDeliveryFrequency(Integer deliveryFrequency) {
		this.deliveryFrequency = deliveryFrequency;
	}

	public BigDecimal getLength() {
		return this.length;
	}

	public void setLength(BigDecimal length) {
		this.length = length;
	}

	public BigDecimal getWidth() {
		return this.width;
	}

	public void setWidth(BigDecimal width) {
		this.width = width;
	}

	public BigDecimal getHeight() {
		return this.height;
	}

	public void setHeight(BigDecimal height) {
		this.height = height;
	}

	public BigDecimal getArea() {
		return this.area;
	}

	public void setArea(BigDecimal area) {
		this.area = area;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserEntity getUser() {
		return this.user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public List<StoreLocationChargeEntity> getStoreLocationCharges() {
		return this.storeLocationCharges;
	}

	public void setStoreLocationCharges(List<StoreLocationChargeEntity> storeLocationCharges) {
		this.storeLocationCharges = storeLocationCharges;
	}

	public List<StoreFileEntity> getStoreFiles() {
		if (this.storeFiles == null) {
			this.storeFiles = new ArrayList<>();
		}
		return this.storeFiles;
	}

	public void setStoreFiles(List<StoreFileEntity> storeFiles) {
		this.storeFiles = storeFiles;
	}

}
