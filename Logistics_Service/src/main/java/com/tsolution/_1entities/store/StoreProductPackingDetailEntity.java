package com.tsolution._1entities.store;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "store_product_packing_detail")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class StoreProductPackingDetailEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4920653704585427443L;

	@ManyToOne
	@JoinColumn(name = "store_product_packing_id")
	private StoreProductPackingEntity storeProductPacking;

	@Column(name = "quantity", nullable = false)
	private Long quantity;

	@Column(name = "original_quantity", nullable = false)
	private Long originalQuantity;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "QR_code", nullable = false)
	private String qRCode;

	public StoreProductPackingEntity getStoreProductPacking() {
		return this.storeProductPacking;
	}

	public void setStoreProductPacking(StoreProductPackingEntity storeProductPacking) {
		this.storeProductPacking = storeProductPacking;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Long getOriginalQuantity() {
		return this.originalQuantity;
	}

	public void setOriginalQuantity(Long originalQuantity) {
		this.originalQuantity = originalQuantity;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

}
