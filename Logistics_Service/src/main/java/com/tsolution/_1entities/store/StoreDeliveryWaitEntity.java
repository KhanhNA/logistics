package com.tsolution._1entities.store;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "store_delivery_wait")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class StoreDeliveryWaitEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6456511211511090547L;

	@ManyToOne
	@JoinColumn(name = "from_store_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity fromStore;

	@ManyToOne
	@JoinColumn(name = "to_store_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity toStore;

	@Column(name = "max_day_to_wait", nullable = false)
	private Integer maxDayToWait;

	public StoreEntity getFromStore() {
		return this.fromStore;
	}

	public void setFromStore(StoreEntity fromStore) {
		this.fromStore = fromStore;
	}

	public StoreEntity getToStore() {
		return this.toStore;
	}

	public void setToStore(StoreEntity toStore) {
		this.toStore = toStore;
	}

	public Integer getMaxDayToWait() {
		return this.maxDayToWait;
	}

	public void setMaxDayToWait(Integer maxDayToWait) {
		this.maxDayToWait = maxDayToWait;
	}

}