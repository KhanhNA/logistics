package com.tsolution._1entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "merchant")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class MerchantEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4572272641714394555L;

	@ManyToOne
	@JoinColumn(name = "country_id")
	private CountryEntity country;

	@ManyToOne
	@JoinColumn(name = "language_id")
	private LanguageEntity language;

	@ManyToOne
	@JoinColumn(name = "currency_id")
	private CurrencyEntity currency;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "domain_name")
	private String domainName;

	@Column(name = "in_business_since", nullable = false)
	@JsonSerialize(using = SerializeDateHandler.class)
	private LocalDateTime inBusinessSince;

	@Column(name = "email")
	private String email;

	@Column(name = "logo")
	private String logo;

	@Column(name = "address")
	private String address;

	@Column(name = "city")
	private String city;

	@Column(name = "phone")
	private String phone;

	@Column(name = "postal_code")
	private String postalCode;

	@Column(name = "lat", nullable = false)
	private BigDecimal lat;

	@Column(name = "lng", nullable = false)
	private BigDecimal lng;

	/**
	 * 0: không hoạt động, 1: hoạt động
	 */
	@Column(name = "status")
	private Boolean status;

	public CountryEntity getCountry() {
		return this.country;
	}

	public void setCountry(CountryEntity country) {
		this.country = country;
	}

	public LanguageEntity getLanguage() {
		return this.language;
	}

	public void setLanguage(LanguageEntity language) {
		this.language = language;
	}

	public CurrencyEntity getCurrency() {
		return this.currency;
	}

	public void setCurrency(CurrencyEntity currency) {
		this.currency = currency;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomainName() {
		return this.domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public LocalDateTime getInBusinessSince() {
		return this.inBusinessSince;
	}

	public void setInBusinessSince(LocalDateTime inBusinessSince) {
		this.inBusinessSince = inBusinessSince;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public BigDecimal getLat() {
		return this.lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return this.lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
}