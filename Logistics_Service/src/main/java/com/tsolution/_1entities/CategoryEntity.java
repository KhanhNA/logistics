package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "category")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class CategoryEntity extends SuperEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "image", nullable = false)
	private String image;

	@Column(name = "sort_order")
	private Integer sortOrder;

	@Column(name = "parent_id")
	private Long parentId;

	/**
	 * 0: không ho?t d?ng, 1: ho?t d?ng
	 */
	@Column(name = "status")
	private Boolean status;

}