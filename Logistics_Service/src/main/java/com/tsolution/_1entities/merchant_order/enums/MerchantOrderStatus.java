package com.tsolution._1entities.merchant_order.enums;

public enum MerchantOrderStatus {
	STATUS_STORE_NOT_ENOUGH_QUANTITY(0), STATUS_STORE_ENOUGH_QUANTITY(1), STATUS_STORE_WAITING_FOR_IMPORT(5),
	STATUS_STORE_EXPORTED(2), STATUS_CANCELED(3), STATUS_RETURNED(4);

	private Integer value;

	MerchantOrderStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
