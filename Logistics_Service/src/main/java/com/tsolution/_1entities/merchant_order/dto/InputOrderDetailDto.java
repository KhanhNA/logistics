package com.tsolution._1entities.merchant_order.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class InputOrderDetailDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1769380618237422880L;
	private String productPackingCode;
	private Long quantity;
	private BigDecimal price;
	private Integer type;

	public String getProductPackingCode() {
		return this.productPackingCode;
	}

	public void setProductPackingCode(String productPackingCode) {
		this.productPackingCode = productPackingCode;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
