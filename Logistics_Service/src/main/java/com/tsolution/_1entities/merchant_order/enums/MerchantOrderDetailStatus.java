package com.tsolution._1entities.merchant_order.enums;

public enum MerchantOrderDetailStatus {
	STATUS_NOT_ENOUGH_QUANTITY(0), STATUS_WAITING_FOR_STATEMENT(1), STATUS_ENOUGH_QUANTITY(2);

	private Integer value;

	MerchantOrderDetailStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
