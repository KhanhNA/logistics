package com.tsolution._1entities.merchant_order.enums;

public enum MerchantOrderDetailType {
	HANG_BAN(0), HANG_KHUYEN_MAI(1);

	private Integer value;

	MerchantOrderDetailType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
