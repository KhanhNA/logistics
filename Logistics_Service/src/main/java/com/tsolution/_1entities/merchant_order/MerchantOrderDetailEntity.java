package com.tsolution._1entities.merchant_order;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "merchant_order_detail")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class MerchantOrderDetailEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7080433788939135359L;

	@ManyToOne
	@JoinColumn(name = "merchant_order_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private MerchantOrderEntity merchantOrder;

	@ManyToOne
	@JoinColumn(name = "product_packing_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private ProductPackingEntity productPacking;

	@ManyToOne
	@JoinColumn(name = "distributor_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private DistributorEntity distributor;

	@Column(name = "quantity", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long quantity;

	@Column(name = "price", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal price;

	/**
	 * 0: Hàng bán, 1: Hàng khuyến mại
	 */
	@Column(name = "type", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer type;

	/**
	 * 0: from_store không đủ hàng, 1: from_store đủ hàng
	 */
	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer status;

	public MerchantOrderEntity getMerchantOrder() {
		return this.merchantOrder;
	}

	public void setMerchantOrder(MerchantOrderEntity merchantOrder) {
		this.merchantOrder = merchantOrder;
	}

	public ProductPackingEntity getProductPacking() {
		return this.productPacking;
	}

	public void setProductPacking(ProductPackingEntity productPacking) {
		this.productPacking = productPacking;
	}

	public DistributorEntity getDistributor() {
		return this.distributor;
	}

	public void setDistributor(DistributorEntity distributor) {
		this.distributor = distributor;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}