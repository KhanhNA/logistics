package com.tsolution._1entities.merchant_order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.merchant_order.dto.InputOrderDto;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderGoodsReceiveForm;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "merchant_order")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class MerchantOrderEntity extends SuperEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6858507565094911086L;

	@Column(name = "code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String code;

	@Column(name = "QR_code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String qRCode;

	@Column(name = "merchant_code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String merchantCode;

	@Column(name = "merchant_name", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String merchantName;

	@Column(name = "phone", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String phone;

	@Column(name = "amount", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private BigDecimal amount;

	@Column(name = "order_id", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long orderId;

	@Column(name = "order_code", nullable = false)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String orderCode;

	@Column(name = "order_date", nullable = false)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime orderDate;

	@ManyToOne
	@JoinColumn(name = "from_store_id")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private StoreEntity fromStore;

	/**
	 * 0: Đơn hàng Store thiếu hàng, 1: Đơn hàng Store đủ hàng, 2: Đơn hàng trả hàng
	 * thành công, 3: Bị hủy, 4: Bị trả
	 */
	@Column(name = "status")
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Integer status;

	@Column(name = "goods_receive_form", nullable = false)
	@Enumerated(EnumType.STRING)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private MerchantOrderGoodsReceiveForm goodsReceiveForm;

	@OneToMany(mappedBy = "merchantOrder", fetch = FetchType.LAZY)
	@JsonView(JsonEntityViewer.Human.Detail.class)
	private List<MerchantOrderDetailEntity> merchantOrderDetails;

	public MerchantOrderEntity() {
	}

	public MerchantOrderEntity(Long id) {
		this.setId(id);
	}

	public MerchantOrderEntity(InputOrderDto inputOrderDto) {
		this.setCode(inputOrderDto.getOrderCode());
		this.setOrderId(inputOrderDto.getOrderId());
		this.setOrderCode(inputOrderDto.getOrderCode());
		this.setOrderDate(inputOrderDto.getOrderDate());
		this.setMerchantCode(inputOrderDto.getMerchantCode());
		this.setMerchantName(inputOrderDto.getMerchantName());
		this.setPhone(inputOrderDto.getPhone());
		this.setAmount(inputOrderDto.getAmount());
		this.setGoodsReceiveForm(inputOrderDto.getGoodsReceiveForm());
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getqRCode() {
		return this.qRCode;
	}

	public void setqRCode(String qRCode) {
		this.qRCode = qRCode;
	}

	public String getMerchantCode() {
		return this.merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantName() {
		return this.merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderCode() {
		return this.orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public LocalDateTime getOrderDate() {
		return this.orderDate;
	}

	public void setOrderDate(LocalDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public StoreEntity getFromStore() {
		return this.fromStore;
	}

	public void setFromStore(StoreEntity fromStore) {
		this.fromStore = fromStore;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public MerchantOrderGoodsReceiveForm getGoodsReceiveForm() {
		return this.goodsReceiveForm;
	}

	public void setGoodsReceiveForm(MerchantOrderGoodsReceiveForm goodsReceiveForm) {
		this.goodsReceiveForm = goodsReceiveForm;
	}

	public List<MerchantOrderDetailEntity> getMerchantOrderDetails() {
		return this.merchantOrderDetails;
	}

	public void setMerchantOrderDetails(List<MerchantOrderDetailEntity> merchantOrderDetails) {
		this.merchantOrderDetails = merchantOrderDetails;
	}

}