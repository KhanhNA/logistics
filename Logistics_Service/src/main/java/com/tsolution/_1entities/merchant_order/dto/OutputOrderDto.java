package com.tsolution._1entities.merchant_order.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.merchant_order.MerchantOrderEntity;
import com.tsolution._1entities.store.StoreEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class OutputOrderDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -897620561635021452L;

	private String code;
	private String qrCode;
	private Integer status;
	private StoreEntity store;

	public OutputOrderDto() {
	}

	public OutputOrderDto(MerchantOrderEntity merchantOrderEntity) {
		this.setCode(merchantOrderEntity.getCode());
		this.setQrCode(merchantOrderEntity.getqRCode());
		this.setStatus(merchantOrderEntity.getStatus());
		this.setStore(merchantOrderEntity.getFromStore());
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getQrCode() {
		return this.qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public StoreEntity getStore() {
		return this.store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

}
