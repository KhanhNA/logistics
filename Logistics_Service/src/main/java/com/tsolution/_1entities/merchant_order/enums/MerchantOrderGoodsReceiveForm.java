package com.tsolution._1entities.merchant_order.enums;

public enum MerchantOrderGoodsReceiveForm {
	SHIPPING("SHIPPING"), PICK_UP("PICK_UP");

	private String value;

	MerchantOrderGoodsReceiveForm(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
