package com.tsolution._1entities.merchant_order.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderGoodsReceiveForm;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DeserializeDateHandler;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class InputOrderDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4395487876927122079L;

	private Long orderId;
	private String orderCode;
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonDeserialize(using = DeserializeDateHandler.class)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	private LocalDateTime orderDate;
	private String merchantCode;
	private String merchantName;
	private String phone;
	private BigDecimal amount;
	private String fromStoreCode;
	private String townshipCode;
	private MerchantOrderGoodsReceiveForm goodsReceiveForm;
	private List<InputOrderDetailDto> orderDetails;

	public Long getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderCode() {
		return this.orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public LocalDateTime getOrderDate() {
		return this.orderDate;
	}

	public void setOrderDate(LocalDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public String getMerchantCode() {
		return this.merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantName() {
		return this.merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getFromStoreCode() {
		return this.fromStoreCode;
	}

	public void setFromStoreCode(String fromStoreCode) {
		this.fromStoreCode = fromStoreCode;
	}

	public String getTownshipCode() {
		return this.townshipCode;
	}

	public void setTownshipCode(String townshipCode) {
		this.townshipCode = townshipCode;
	}

	public MerchantOrderGoodsReceiveForm getGoodsReceiveForm() {
		return this.goodsReceiveForm;
	}

	public void setGoodsReceiveForm(MerchantOrderGoodsReceiveForm goodsReceiveForm) {
		this.goodsReceiveForm = goodsReceiveForm;
	}

	public List<InputOrderDetailDto> getOrderDetails() {
		return this.orderDetails;
	}

	public void setOrderDetails(List<InputOrderDetailDto> orderDetails) {
		this.orderDetails = orderDetails;
	}

}
