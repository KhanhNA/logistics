package com.tsolution._1entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Table(name = "product_digital")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ProductDigitalEntity extends SuperEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "product_id", nullable = false)
	private Long productId;

	@Column(name = "file_name")
	private String fileName;

}