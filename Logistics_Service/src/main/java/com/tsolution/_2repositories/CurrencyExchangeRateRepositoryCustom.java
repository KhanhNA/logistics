package com.tsolution._2repositories;

import java.sql.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.CurrencyExchangeRateEntity;
import com.tsolution.excetions.BusinessException;

public interface CurrencyExchangeRateRepositoryCustom {
	Page<CurrencyExchangeRateEntity> findCurrencyExchangeRate(Long fromCurrencyId, Long toCurrencyId, Date fromDate,
			Date toDate, Pageable pageable) throws BusinessException;

	Boolean isExistsCurrencyExchangeRate(Long id, Long fromCurrencyId, Long toCurrencyId, Date fromDate, Date toDate)
			throws BusinessException;
}