package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.PoEntity;

public interface PoRepository
		extends JpaRepository<PoEntity, Long>, JpaSpecificationExecutor<PoEntity>, PoRepositoryCustom {
}