package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.ProductImageDescriptionEntity;

public interface ProductImageDescriptionEntityRepository extends JpaRepository<ProductImageDescriptionEntity, Long>, JpaSpecificationExecutor<ProductImageDescriptionEntity> {

}