package com.tsolution._2repositories;

import com.tsolution.excetions.BusinessException;

public interface MerchantOrderRepositoryCustom {
	Long countMerchantOrderHang(Long storeId) throws BusinessException;
}
