package com.tsolution._2repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.ManufacturerEntity;

public interface ManufacturerRepository
		extends JpaRepository<ManufacturerEntity, Long>, JpaSpecificationExecutor<ManufacturerEntity> {
	@Query(value = " SELECT DISTINCT m.* " + ManufacturerEntity.FIND, countQuery = " SELECT COUNT(DISTINCT m.id) "
			+ ManufacturerEntity.FIND, nativeQuery = true)
	Page<ManufacturerEntity> find(@Param("text") String text, @Param("status") Boolean status, Pageable pageable,
			String acceptLanguage);
}