package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.ProductDigitalEntity;

public interface ProductDigitalEntityRepository extends JpaRepository<ProductDigitalEntity, Long>, JpaSpecificationExecutor<ProductDigitalEntity> {

}