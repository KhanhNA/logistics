package com.tsolution._2repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.dto.PoDto;
import com.tsolution.excetions.BusinessException;

public interface PoRepositoryCustom {
	Page<PoEntity> find(PoDto poDto, String username, Pageable pageable, String acceptLanguage)
			throws BusinessException;

	Long countPoHang(Long storeId) throws BusinessException;

	List<PoEntity> getTaskListPo(String username, List<String> statuses) throws BusinessException;
}
