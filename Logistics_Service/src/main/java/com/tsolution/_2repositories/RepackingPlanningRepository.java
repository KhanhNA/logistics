package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.RepackingPlanningEntity;

public interface RepackingPlanningRepository extends JpaRepository<RepackingPlanningEntity, Long>,
		JpaSpecificationExecutor<RepackingPlanningEntity>, RepackingPlanningRepositoryCustom {
}