package com.tsolution._2repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.CountryDescriptionEntity;

public interface ICountryDescriptionRepository
		extends JpaRepository<CountryDescriptionEntity, Long>, JpaSpecificationExecutor<CountryDescriptionEntity> {
	@Query(value = "SELECT cd.* from country c join country_description cd on c.id = cd.country_id where (:support is null or c.is_support = :support) and (:language is null or cd.language_id = :language)", nativeQuery = true)
	List<CountryDescriptionEntity> find(@Param("support") Boolean support, @Param("language") Long language);
}