package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.ProductPackingUnitEntity;

public interface ProductPackingUnitRepository
		extends JpaRepository<ProductPackingUnitEntity, Long>, JpaSpecificationExecutor<ProductPackingUnitEntity> {
}