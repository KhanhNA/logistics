package com.tsolution._2repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution.excetions.BusinessException;

public interface PalletDetailRepositoryCustom {
	Page<PalletDetailEntity> findGroupByProductPackingAndExpireDate(PalletDetailDto palletDetailDto,
			Long repackingPlanningId, Pageable pageable) throws BusinessException;

	Page<PalletDetailEntity> findGroupByPalletStepAndProductPackingAndExpireDate(PalletDetailDto palletDetailDto,
			List<Long> managedStoreIds, Pageable pageable) throws BusinessException;
}
