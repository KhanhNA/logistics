package com.tsolution._2repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.PackingTypeEntity;

public interface IPackingTypeRepository
		extends JpaRepository<PackingTypeEntity, Long>, JpaSpecificationExecutor<PackingTypeEntity> {
	List<PackingTypeEntity> findByStatus(Boolean status);

	PackingTypeEntity findOneByCode(String code);
}