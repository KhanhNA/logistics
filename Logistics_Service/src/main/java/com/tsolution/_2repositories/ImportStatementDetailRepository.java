package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.ImportStatementDetailEntity;

public interface ImportStatementDetailRepository extends JpaRepository<ImportStatementDetailEntity, Long>,
		JpaSpecificationExecutor<ImportStatementDetailEntity> {

}