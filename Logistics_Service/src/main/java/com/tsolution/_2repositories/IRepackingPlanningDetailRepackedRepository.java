package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.RepackingPlanningDetailRepackedEntity;

public interface IRepackingPlanningDetailRepackedRepository extends JpaRepository<RepackingPlanningDetailRepackedEntity, Long>, JpaSpecificationExecutor<RepackingPlanningDetailRepackedEntity> {

}