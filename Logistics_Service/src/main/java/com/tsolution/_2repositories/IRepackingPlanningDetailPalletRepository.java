package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.RepackingPlanningDetailPalletEntity;

public interface IRepackingPlanningDetailPalletRepository extends JpaRepository<RepackingPlanningDetailPalletEntity, Long>, JpaSpecificationExecutor<RepackingPlanningDetailPalletEntity> {

}