package com.tsolution._2repositories;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.PalletDetailEntity;

public interface PalletDetailRepository extends JpaRepository<PalletDetailEntity, Long>,
		JpaSpecificationExecutor<PalletDetailEntity>, PalletDetailRepositoryCustom {

	@Query(value = PalletDetailEntity.FIND_GROUP_BY_PRODUCT_PACKING_AND_EXPIRE_DATE, nativeQuery = true)
	List<PalletDetailEntity> findGroupByProductPackingAndExpireDate(Long storeId, Long productPackingId,
			Date expireDate, Integer step);

	@Query(value = PalletDetailEntity.FIND_BY_PRODUCT_PACKING_AND_EXPIRE_DATE, nativeQuery = true)
	List<PalletDetailEntity> findByProductPackingAndExpireDate(Long storeId, Long productPackingId, Date expireDate,
			Integer step);

	@Query(value = "SELECT pld.* FROM pallet_detail pld WHERE pld.pallet_id = :palletId AND pld.product_packing_id = :productPackingId AND pld.expire_date = :expireDate", nativeQuery = true)
	List<PalletDetailEntity> findByPalletAndProductPackingAndExpireDate(Long palletId, Long productPackingId,
			Date expireDate);

	Optional<PalletDetailEntity> findByStoreProductPackingDetailId(Long storeProductPackingDetailId);
}