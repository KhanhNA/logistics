package com.tsolution._2repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.PalletEntity;

public interface PalletRepository
		extends JpaRepository<PalletEntity, Long>, JpaSpecificationExecutor<PalletEntity>, PalletRepositoryCustom {
	@Query(value = "SELECT p.* FROM pallet p WHERE p.code = :code LIMIT 1", nativeQuery = true)
	Optional<PalletEntity> findByCode(String code);
}