package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.claim.ClaimImage;

public interface ClaimImageRepository extends JpaRepository<ClaimImage, Long>, JpaSpecificationExecutor<ClaimImage> {
}