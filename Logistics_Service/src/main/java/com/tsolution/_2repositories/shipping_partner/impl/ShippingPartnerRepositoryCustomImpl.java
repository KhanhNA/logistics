package com.tsolution._2repositories.shipping_partner.impl;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution._2repositories.shipping_partner.ShippingPartnerRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class ShippingPartnerRepositoryCustomImpl implements ShippingPartnerRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<ShippingPartnerEntity> find(ShippingPartnerEntity shippingPartner, Pageable pageable) throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		Map<String, Object> params = new HashMap<>();

		strQuery.append(" FROM shipping_partner sp ");
		strQuery.append(" WHERE (:name IS NULL OR LOWER(sp.name) LIKE LOWER(CONCAT('%',:name,'%'))) ");
		strQuery.append("   AND (:phone IS NULL OR LOWER(sp.phone) LIKE LOWER(CONCAT('%',:phone,'%'))) ");
		strQuery.append("   AND (:email IS NULL OR LOWER(sp.email) LIKE LOWER(CONCAT('%',:email,'%'))) ");
		strQuery.append("   AND (:address IS NULL OR LOWER(sp.address) LIKE LOWER(CONCAT('%',:address,'%'))) ");
		strQuery.append("   AND (:contractCode IS NULL ");
		strQuery.append("		 OR LOWER(sp.contract_code) LIKE LOWER(CONCAT('%',:contractCode,'%'))) ");
		strQuery.append("   AND (:taxCode IS NULL ");
		strQuery.append("		 OR LOWER(sp.tax_code) LIKE LOWER(CONCAT('%',:taxCode,'%'))) ");
		strQuery.append("   AND (:status IS NULL OR sp.status LIKE :status) ");

		params.put("name", shippingPartner.getName());
		params.put("phone", shippingPartner.getPhone());
		params.put("email", shippingPartner.getEmail());
		params.put("address", shippingPartner.getAddress());
		params.put("contractCode", shippingPartner.getContractCode());
		params.put("taxCode", shippingPartner.getTaxCode());
		params.put("status", shippingPartner.getStatus() == null ? null : shippingPartner.getStatus().getValue());

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT DISTINCT sp.* ");
		strSelectQuery.append(strQuery);
		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(DISTINCT sp.id) ");
		strCountQuery.append(strQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, ShippingPartnerEntity.class);
	}

}
