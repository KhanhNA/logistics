package com.tsolution._2repositories.shipping_partner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution.excetions.BusinessException;

public interface ShippingPartnerRepositoryCustom {
	Page<ShippingPartnerEntity> find(ShippingPartnerEntity shippingPartner, Pageable pageable) throws BusinessException;
}
