package com.tsolution._2repositories.shipping_partner;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;

public interface ShippingPartnerRepository extends JpaRepository<ShippingPartnerEntity, Long>,
		JpaSpecificationExecutor<ShippingPartnerEntity>, ShippingPartnerRepositoryCustom {
}