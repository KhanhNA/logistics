package com.tsolution._2repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.UserEntity;
import com.tsolution.excetions.BusinessException;

public interface UserRepositoryCustom {
	Page<UserEntity> find(@Param("status") Boolean status, @Param("text") String text, List<Long> storeIds,
			Pageable pageable) throws BusinessException;
}
