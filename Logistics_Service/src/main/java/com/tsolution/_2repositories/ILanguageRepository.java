package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.LanguageEntity;

public interface ILanguageRepository
		extends JpaRepository<LanguageEntity, Long>, JpaSpecificationExecutor<LanguageEntity> {

}