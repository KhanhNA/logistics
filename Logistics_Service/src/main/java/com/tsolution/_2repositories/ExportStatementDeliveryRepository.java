package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.export_statement.ExportStatementDelivery;

public interface ExportStatementDeliveryRepository extends JpaRepository<ExportStatementDelivery, Long>, JpaSpecificationExecutor<ExportStatementDelivery> {

}