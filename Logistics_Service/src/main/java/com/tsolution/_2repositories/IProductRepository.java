package com.tsolution._2repositories;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.ProductEntity;

public interface IProductRepository
		extends JpaRepository<ProductEntity, Long>, JpaSpecificationExecutor<ProductEntity> {
	ProductEntity findOneByCode(String code);

	@Query(value = " SELECT DISTINCT p.* " + ProductEntity.FIND, countQuery = " SELECT COUNT(DISTINCT p.id) "
			+ ProductEntity.FIND, nativeQuery = true)
	Page<ProductEntity> find(@Param("text") String text, @Param("exceptId") Collection<Long> exceptId,
			Pageable pageable);
}