package com.tsolution._2repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.DistributorEntity;

public interface DistributorRepository
		extends JpaRepository<DistributorEntity, Long>, JpaSpecificationExecutor<DistributorEntity> {
	@Query(value = "SELECT DISTINCT d.* FROM distributor d JOIN users u "
			+ " ON (:username IS NULL OR u.username = :username) "
			+ " AND (d.id = u.distributor_id OR u.distributor_id IS NULL) "
			+ " WHERE d.enabled = 1 ", nativeQuery = true)
	List<DistributorEntity> findAll(String username);
}