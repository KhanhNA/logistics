package com.tsolution._2repositories.store;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.store.StoreFileEntity;

public interface StoreFileRepository
		extends JpaRepository<StoreFileEntity, Long>, JpaSpecificationExecutor<StoreFileEntity> {
}
