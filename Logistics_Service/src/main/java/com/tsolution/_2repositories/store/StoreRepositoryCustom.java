package com.tsolution._2repositories.store;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.dto.AvailableQuantityDto;
import com.tsolution._1entities.store.dto.StoreDto;
import com.tsolution.excetions.BusinessException;

public interface StoreRepositoryCustom {
	Page<StoreEntity> find(StoreDto storeDto, Pageable pageable) throws BusinessException;

	List<AvailableQuantityDto> getAvailableQuantity(List<String> townshipCodes, List<String> productPackingCodes)
			throws BusinessException;

	List<StoreEntity> getStoreByLocationCharge(List<String> townshipCodes) throws BusinessException;
}
