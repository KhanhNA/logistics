package com.tsolution._2repositories.store;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.store.StoreProductPackingDetailEntity;

public interface StoreProductPackingDetailRepository extends JpaRepository<StoreProductPackingDetailEntity, Long>,
		JpaSpecificationExecutor<StoreProductPackingDetailEntity> {

	@Query(value = "SELECT sppd.* FROM store_product_packing_detail sppd WHERE sppd.store_product_packing_id = :storeProductPackingId AND sppd.quantity > 0 ORDER BY create_date", nativeQuery = true)
	List<StoreProductPackingDetailEntity> getDetailByStoreProductPackingId(Long storeProductPackingId);
}