package com.tsolution._2repositories.store;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.store.StoreDeliveryWaitEntity;

public interface StoreDeliveryWaitRepository
		extends JpaRepository<StoreDeliveryWaitEntity, Long>, JpaSpecificationExecutor<StoreDeliveryWaitEntity> {
	@Query(value = "SELECT sdw.* FROM store_delivery_wait sdw WHERE sdw.from_store_id in (:fromStoreIds) AND sdw.to_store_id = :toStoreId ORDER BY max_day_to_wait", nativeQuery = true)
	List<StoreDeliveryWaitEntity> getDeliveryWaitForMerchantOrderInventory(List<Long> fromStoreIds, Long toStoreId);

	@Query(value = "SELECT sdw.* FROM store_delivery_wait sdw WHERE sdw.from_store_id  = :fromStoreId", nativeQuery = true)
	List<StoreDeliveryWaitEntity> getAllByFromStore(Long fromStoreId);
}