package com.tsolution._2repositories.store;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.reportdto.InventoryAndSellOutReportDto;
import com.tsolution._1entities.reportdto.InventoryAndSellOutReportFilterDto;
import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution.excetions.BusinessException;

public interface StoreProductPackingRepositoryCustom {
	Long getTotalAvaiable(Long storeId, Long productPackingId);

	Long getTotalAvaiable(Long storeId, String productPackingCode);

	Page<StoreProductPackingEntity> inventory(InventoryDto inventoryDto, String username, Pageable pageable)
			throws BusinessException;

	Long inventory(InventoryDto inventoryDto, String username) throws BusinessException;

	Page<StoreProductPackingEntity> inventoryIgnoreExpireDate(InventoryDto inventoryDto, Long exportStatementId,
			String username, Pageable pageable) throws BusinessException;

	Long countStoreProductPackingHang(Long storeId) throws BusinessException;

	Page<InventoryAndSellOutReportDto> getInventoryAndSellOutReportDto(
			InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto, Pageable pageable)
			throws BusinessException;
}
