package com.tsolution._2repositories.store.impl;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.reportdto.InventoryAndSellOutReportDto;
import com.tsolution._1entities.reportdto.InventoryAndSellOutReportFilterDto;
import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution._2repositories.store.StoreProductPackingRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class StoreProductPackingRepositoryCustomImpl implements StoreProductPackingRepositoryCustom {

	private static final String FROM_STORE_PRODUCT_PACKING_SPP = " FROM store_product_packing spp ";
	private static final String STORE_ID = "storeId";
	@PersistenceContext
	private EntityManager em;

	@Override
	public Long getTotalAvaiable(Long storeId, Long productPackingId) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT nvl(SUM(spp.total_quantity), 0) FROM store_product_packing spp ");
		sb.append(" WHERE spp.store_id = :storeId AND spp.product_packing_id = :productPackingId ");
		sb.append("		  AND spp.expire_date >= SYSDATE() ");
		sb.append(" GROUP BY spp.store_id, spp.product_packing_id ");

		Map<String, Object> params = new HashMap<>();
		params.put(StoreProductPackingRepositoryCustomImpl.STORE_ID, storeId);
		params.put("productPackingId", productPackingId);

		Number total = BaseRepository.getScalarResult(this.em, sb.toString(), params, Number.class);

		return total == null ? 0L : total.longValue();
	}

	@Override
	public Long getTotalAvaiable(Long storeId, String productPackingCode) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT nvl(SUM(spp.total_quantity), 0) ");
		sb.append(StoreProductPackingRepositoryCustomImpl.FROM_STORE_PRODUCT_PACKING_SPP);
		sb.append(" 	 JOIN product_packing pp ON spp.product_packing_id = pp.id AND pp.code = :productPackingCode ");
		sb.append(" WHERE spp.store_id = :storeId AND spp.expire_date >= SYSDATE() ");
		sb.append(" GROUP BY spp.store_id, spp.product_packing_id ");

		Map<String, Object> params = new HashMap<>();
		params.put(StoreProductPackingRepositoryCustomImpl.STORE_ID, storeId);
		params.put("productPackingCode", productPackingCode);

		Number total = BaseRepository.getScalarResult(this.em, sb.toString(), params, Number.class);

		return total == null ? 0L : total.longValue();
	}

	private void generateInventoryQuery(InventoryDto inventoryDto, String username, StringBuilder strSelectQuery,
			StringBuilder strCountQuery, Map<String, Object> params) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(StoreProductPackingRepositoryCustomImpl.FROM_STORE_PRODUCT_PACKING_SPP);
		strQuery.append("	   JOIN product_packing pp ON pp.id = spp.product_packing_id ");
		strQuery.append("			AND (:distributorId IS NULL OR pp.distributor_id = :distributorId) ");
		strQuery.append("	   JOIN product p ON p.id = pp.product_id ");
		strQuery.append("			AND (:manufacturerId IS NULL OR p.manufacturer_id = :manufacturerId) ");
		strQuery.append("	   JOIN product_description pd ON p.id = pd.product_id ");
		strQuery.append("	   JOIN language l ON l.id = pd.language_id ");
		strQuery.append("			AND LOWER(l.code) = LOWER(:acceptLanguage) ");
		strQuery.append("	   JOIN distributor d ON pp.distributor_id = d.id ");
		strQuery.append("	   JOIN manufacturer m ON p.manufacturer_id = m.id ");
		strQuery.append("	   JOIN users u ON u.username = :username ");
		strQuery.append(" 			AND (u.distributor_id IS NULL OR u.distributor_id = pp.distributor_id) ");
		strQuery.append(" WHERE spp.store_id = :storeId AND spp.product_packing_id NOT IN (:exceptProductPackingIds) ");
		strQuery.append("		AND spp.total_quantity > 0 ");
		strQuery.append("		AND (:productPackingCode IS NULL ");
		strQuery.append("			 OR LOWER(pp.code) LIKE LOWER(CONCAT('%',:productPackingCode,'%')) ) ");
		strQuery.append("		AND (:productPackingName IS NULL ");
		strQuery.append("			 OR LOWER(pd.name) LIKE LOWER(CONCAT('%',:productPackingName,'%')) ) ");
		if (inventoryDto.getExpireDateColor() != null) {
			strQuery.append(inventoryDto.getExpireDateColor().generateQuery("spp.expire_date", "pp.lifecycle",
					"pp.warning_threshold"));
		}

		params.put("acceptLanguage", inventoryDto.getAcceptLanguage());
		params.put(StoreProductPackingRepositoryCustomImpl.STORE_ID, inventoryDto.getStoreId());
		params.put("distributorId", inventoryDto.getDistributorId());
		params.put("manufacturerId", inventoryDto.getManufacturerId());
		params.put("exceptProductPackingIds", inventoryDto.getExceptProductPackingIds());
		params.put("productPackingCode", inventoryDto.getProductPackingCode());
		params.put("productPackingName", inventoryDto.getProductPackingName());
		params.put("username", username);

		strSelectQuery.append(" SELECT DISTINCT spp.* ");
		strSelectQuery.append(strQuery);
		strSelectQuery.append(" ORDER BY pp.code, pp.name, spp.expire_date ");

		strCountQuery.append(" SELECT COUNT(DISTINCT spp.id) ");
		strCountQuery.append(strQuery);
	}

	@Override
	public Page<StoreProductPackingEntity> inventory(InventoryDto inventoryDto, String username, Pageable pageable)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strSelectQuery = new StringBuilder();
		StringBuilder strCountQuery = new StringBuilder();
		this.generateInventoryQuery(inventoryDto, username, strSelectQuery, strCountQuery, params);
		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, StoreProductPackingEntity.class);
	}

	@Override
	public Long inventory(InventoryDto inventoryDto, String username) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strCountQuery = new StringBuilder();
		this.generateInventoryQuery(inventoryDto, username, new StringBuilder(), strCountQuery, params);
		Number count = BaseRepository.getScalarResult(this.em, strCountQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}

	@Override
	public Page<StoreProductPackingEntity> inventoryIgnoreExpireDate(InventoryDto inventoryDto, Long exportStatementId,
			String username, Pageable pageable) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT spp.store_id + spp.product_packing_id id, spp.store_id, spp.product_packing_id, ");
		strQuery.append("  		 spp.product_id, null expire_date, null code, null QR_code, ");
		strQuery.append("		 NVL(esd.quantity, 0) + SUM(NVL(spp.total_quantity, 0)) total_quantity, ");
		strQuery.append("  		 null create_date, null create_user, null update_date, null update_user ");
		strQuery.append(StoreProductPackingRepositoryCustomImpl.FROM_STORE_PRODUCT_PACKING_SPP);
		strQuery.append("	   JOIN product_packing pp ON pp.id = spp.product_packing_id ");
		strQuery.append("			AND (:distributorId IS NULL OR pp.distributor_id = :distributorId) ");
		strQuery.append("	   JOIN product p ON p.id = pp.product_id ");
		strQuery.append("	   JOIN product_description pd ON p.id = pd.product_id ");
		strQuery.append("	   JOIN language l ON l.id = pd.language_id ");
		strQuery.append("			AND LOWER(l.code) = LOWER(:acceptLanguage) ");
		strQuery.append("	   JOIN distributor d ON pp.distributor_id = d.id ");
		strQuery.append("	   JOIN users u ON u.username = :username ");
		strQuery.append(" 			AND (u.distributor_id IS NULL OR u.distributor_id = pp.distributor_id) ");
		strQuery.append("	   LEFT JOIN (SELECT product_packing_id, SUM(quantity) quantity ");
		strQuery.append("				  FROM export_statement_detail ");
		strQuery.append("				  WHERE export_statement_id = :exportStatementId ");
		strQuery.append("				  GROUP BY product_packing_id) esd");
		strQuery.append("			ON esd.product_packing_id = spp.product_packing_id ");
		strQuery.append(" WHERE spp.store_id = :storeId AND spp.product_packing_id NOT IN (:exceptProductPackingIds) ");
		strQuery.append("		AND spp.expire_date >= SYSDATE() ");
		strQuery.append("		AND (spp.total_quantity > 0 OR esd.quantity > 0) ");
		strQuery.append("		AND (:code IS NULL ");
		strQuery.append("			 OR LOWER(pp.code) LIKE LOWER(CONCAT('%',:code,'%')) ) ");
		strQuery.append("		AND (:name IS NULL ");
		strQuery.append("			 OR LOWER(pd.name) LIKE LOWER(CONCAT('%',:name,'%')) ) ");
		strQuery.append(" GROUP BY spp.store_id, spp.product_packing_id, spp.product_id ");
		strQuery.append(" HAVING total_quantity > 0 ");

		params.put("acceptLanguage", inventoryDto.getAcceptLanguage());
		params.put(StoreProductPackingRepositoryCustomImpl.STORE_ID, inventoryDto.getStoreId());
		params.put("distributorId", inventoryDto.getDistributorId());
		params.put("exceptProductPackingIds", inventoryDto.getExceptProductPackingIds());
		params.put("code", inventoryDto.getProductPackingCode());
		params.put("name", inventoryDto.getProductPackingName());
		params.put("username", username);
		params.put("exportStatementId", exportStatementId == null ? 0 : exportStatementId);

		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(*) FROM ( ");
		strCountQuery.append(strQuery);
		strCountQuery.append(" ) xxx ");

		return BaseRepository.getPagedNativeQuery(this.em, strQuery.toString(), strCountQuery.toString(), params,
				pageable, StoreProductPackingEntity.class);
	}

	@Override
	public Long countStoreProductPackingHang(Long storeId) throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT COUNT(DISTINCT spp.id) FROM store_product_packing spp ");
		strQuery.append(" WHERE spp.store_id = :storeId AND spp.total_quantity > 0 ");

		Map<String, Object> params = new HashMap<>();
		params.put(StoreProductPackingRepositoryCustomImpl.STORE_ID, storeId);

		Number count = BaseRepository.getScalarResult(this.em, strQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}

	@Override
	public Page<InventoryAndSellOutReportDto> getInventoryAndSellOutReportDto(
			InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto, Pageable pageable)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT MIN(spp.id) AS id, s.name AS storeName, s.code AS storeCode, ");
		strSelectQuery.append(" 	   p.sku, pp.barcode, pd.name AS productName, pt.quantity AS packSize, ");
		strSelectQuery.append(" 	   pp.code AS packCode, ppud.name AS uom, ");
		strSelectQuery.append(" 	   s.delivery_frequency AS deliveryFrequency, ");
		strSelectQuery.append(" 	   SUM(spp.total_quantity) inventory, ");
		strSelectQuery.append(" 	   NVL((SELECT SUM(modd.quantity) ");
		strSelectQuery.append(" 	   		FROM merchant_order_detail modd ");
		strSelectQuery.append(" 	   			 JOIN merchant_order mo ON modd.merchant_order_id = mo.id ");
		strSelectQuery.append(" 	   		WHERE modd.product_packing_id = spp.product_packing_id ");
		strSelectQuery.append(" 	   			  AND mo.from_store_id = spp.store_id ");
		strSelectQuery.append(" 	   			  AND DATE(mo.order_date) >= Date(:fromDate) ");
		strSelectQuery.append(" 	   			  AND mo.order_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)) ");
		strSelectQuery.append(" 	   			  AND mo.status = 2 ");
		strSelectQuery.append(" 	   ), 0) AS sellQuantity ");
		strSelectQuery.append(StoreProductPackingRepositoryCustomImpl.FROM_STORE_PRODUCT_PACKING_SPP);
		strSelectQuery.append(" 	 JOIN store s ON spp.store_id = s.id AND s.is_DC = 1 ");
		if (!inventoryAndSellOutReportFilterDto.getStoreIds().isEmpty()) {
			strSelectQuery.append("					 AND s.id IN (:storeIds) ");
			params.put("storeIds", inventoryAndSellOutReportFilterDto.getStoreIds());
		}
		strSelectQuery.append(" 	 JOIN product_packing pp ON spp.product_packing_id = pp.id ");
		strSelectQuery.append(" 	 JOIN packing_type pt ON pp.packing_type_id = pt.id ");
		strSelectQuery.append(" 	 JOIN product p ON spp.product_id = p.id ");
		strSelectQuery.append(" 	 JOIN product_description pd ON p.id = pd.product_id ");
		strSelectQuery.append(" 	 JOIN language l ON pd.language_id = l.id AND l.code LIKE :languageCode ");
		strSelectQuery.append(" 	 JOIN product_packing_unit_description ppud ");
		strSelectQuery.append(" 	 			ON pp.uom = ppud.product_packing_unit_id ");
		strSelectQuery.append(" 	 			   AND ppud.language_id = l.id ");
		strSelectQuery.append(" GROUP BY s.name, s.code, p.sku, pp.barcode, pd.name, pt.quantity, pp.code, pp.uom, ");
		strSelectQuery.append(" 		 s.delivery_frequency ");

		params.put("fromDate", inventoryAndSellOutReportFilterDto.getFromDate());
		params.put("toDate", inventoryAndSellOutReportFilterDto.getToDate());
		params.put("languageCode", "en");

		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(t.id) FROM (").append(strSelectQuery).append(") AS t ");

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, InventoryAndSellOutReportDto.class);
	}

}
