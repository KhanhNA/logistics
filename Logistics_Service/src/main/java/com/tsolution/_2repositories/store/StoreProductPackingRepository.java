package com.tsolution._2repositories.store;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.store.StoreProductPackingEntity;
import com.tsolution._2repositories.sql.StoreProductPackingSql;

public interface StoreProductPackingRepository extends JpaRepository<StoreProductPackingEntity, Long>,
		JpaSpecificationExecutor<StoreProductPackingEntity>, StoreProductPackingRepositoryCustom {
	@Query(value = "select spp.* " + StoreProductPackingSql.NEAR_HERE, countQuery = "select count(spp.id) "
			+ StoreProductPackingSql.NEAR_HERE, nativeQuery = true)
	Page<StoreProductPackingEntity> nearHere(Float currentLat, Float currentLng, Integer maxDistance,
			String productPackingCode, Boolean isDC, Pageable pageable);

	Optional<StoreProductPackingEntity> findByStoreIdAndProductPackingIdAndExpireDate(Long storeId,
			Long productPackingId, LocalDateTime expireDate);

	@Query(value = StoreProductPackingSql.GET_INVENTORY_BY_MERCHANT_ORDER, nativeQuery = true)
	List<StoreProductPackingEntity> getInventoryByMerchantOrder(List<Long> storeIds, List<Long> productPackingIds);

	@Query(value = StoreProductPackingSql.GET_AVAIABLE_FOR_EXPORT, nativeQuery = true)
	List<StoreProductPackingEntity> getAvaiableForExport(Long storeId, List<Long> productPackingIds);
}