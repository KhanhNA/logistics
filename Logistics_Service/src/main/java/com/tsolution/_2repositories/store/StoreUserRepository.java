package com.tsolution._2repositories.store;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.store.StoreUserEntity;

public interface StoreUserRepository
		extends JpaRepository<StoreUserEntity, Long>, JpaSpecificationExecutor<StoreUserEntity> {
	@Query(value = " SELECT cast_to_bit(COUNT(su.id)) FROM store_user su WHERE su.username = :username AND (su.store_id = :storeId OR "
			+ " EXISTS(SELECT 1 FROM store ps WHERE ps.provide_store_id = su.store_id and ps.id = :storeId) ) ", nativeQuery = true)
	Boolean existsStoreUserByStoreIdAndUsername(Long storeId, String username);

	@Modifying
	@Query(value = "DELETE FROM store_user WHERE username = :username ", nativeQuery = true)
	void resetStoreUser(String username);
}