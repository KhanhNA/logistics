package com.tsolution._2repositories.store;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.store.StoreEntity;

public interface StoreRepository
		extends JpaRepository<StoreEntity, Long>, JpaSpecificationExecutor<StoreEntity>, StoreRepositoryCustom {
	StoreEntity findOneByCode(String code);

	@Query(value = "SELECT s.* FROM store s WHERE s.code in (:codes) ", nativeQuery = true)
	List<StoreEntity> findByCodes(List<String> codes);

	@Query(value = "SELECT s.* FROM store s where s.status = 1 and s.is_DC = 0 order by create_date limit 1", nativeQuery = true)
	StoreEntity getFC();

	@Modifying
	@Query(value = "UPDATE Store s SET s.status = true WHERE s.id = :id ", nativeQuery = true)
	void activate(Long id);

	@Modifying
	@Query(value = "UPDATE Store s SET s.status = false, s.out_business_since = SYSDATE() WHERE s.id = :id ", nativeQuery = true)
	void deactivate(Long id);

	@Query(value = "SELECT s.* FROM store s WHERE s.provide_store_id IN (:storeIds) AND s.id NOT IN (:storeIds) AND s.is_DC = 1 AND s.status = 1 ", nativeQuery = true)
	List<StoreEntity> getStoresByProvideStore(List<Long> storeIds);

	@Query(value = "SELECT distinct s.* "
			+ StoreEntity.GET_STORES_EXISTS_NOT_ENOUGH_QUANTITY, countQuery = "SELECT count(distinct s.id) "
					+ StoreEntity.GET_STORES_EXISTS_NOT_ENOUGH_QUANTITY, nativeQuery = true)
	Page<StoreEntity> getStoresExistsNotEnoughQuantity(@Param("storeIds") List<Long> storeIds,
			@Param("merchantOrderStatus") Integer merchantOrderStatus, Pageable pageable);

	@Query(value = "select s.* " + StoreEntity.NEAR_HERE, countQuery = "select count(s.id) "
			+ StoreEntity.NEAR_HERE, nativeQuery = true)
	Page<StoreEntity> nearHere(Float currentLat, Float currentLng, Integer maxDistance, Boolean isDC,
			Pageable pageable);
}
