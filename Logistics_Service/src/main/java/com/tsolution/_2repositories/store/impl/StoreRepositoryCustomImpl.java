package com.tsolution._2repositories.store.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.enums.LocationType;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderStatus;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.dto.AvailableQuantityDto;
import com.tsolution._1entities.store.dto.StoreDto;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution._2repositories.store.StoreRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class StoreRepositoryCustomImpl implements StoreRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<StoreEntity> find(StoreDto storeDto, Pageable pageable) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" FROM store s left join store_user su on s.id = su.store_id ");
		if (storeDto.getCountryId() != null) {
			strQuery.append("  JOIN country c ON c.id = s.country_id AND c.id = :countryId ");
		}
		if (storeDto.getRegionId() != null) {
			strQuery.append("  JOIN country_location r ON r.id = s.region_id AND r.id = :regionId ");
			strQuery.append("  		AND r.location_type = '").append(LocationType.REGION.getValue()).append("' ");
		}
		if (storeDto.getDistrictId() != null) {
			strQuery.append("  JOIN country_location d ON d.id = s.district_id AND d.id = :districtId ");
			strQuery.append("  		AND d.location_type = '").append(LocationType.DISTRICT.getValue()).append("' ");
		}
		if (storeDto.getTownshipId() != null) {
			strQuery.append("  JOIN country_location t ON t.id = s.township_id AND t.id = :townshipId ");
			strQuery.append("  		AND t.location_type = '").append(LocationType.TOWNSHIP.getValue()).append("' ");
		}
		strQuery.append(" WHERE (:status is null or s.status = :status) and (:isDc is null or s.is_DC = :isDc) ");
		strQuery.append(" 		AND (:text is null or LOWER(s.code) like LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append(" 		    or LOWER(s.name) like LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append(" 		    or LOWER(s.domain_name) like LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append(" 		    or LOWER(s.address) like LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append(" 		    or LOWER(s.phone) like LOWER(CONCAT('%',:text,'%'))     ) ");
		strQuery.append(" 		AND s.id not in (:exceptIds) ");
		strQuery.append(" 		AND (:currentUser is null or su.username = :currentUser) ");
		strQuery.append(" ORDER BY s.is_DC, s.code ");

		params.put("status", storeDto.getStatus());
		params.put("isDc", storeDto.getIsDc());
		params.put("text", storeDto.getText());
		params.put("exceptIds", storeDto.getExceptIds());
		params.put("currentUser", storeDto.getCurrentUser());
		if (storeDto.getCountryId() != null) {
			params.put("countryId", storeDto.getCountryId());
		}
		if (storeDto.getRegionId() != null) {
			params.put("regionId", storeDto.getRegionId());
		}
		if (storeDto.getDistrictId() != null) {
			params.put("districtId", storeDto.getDistrictId());
		}
		if (storeDto.getTownshipId() != null) {
			params.put("townshipId", storeDto.getTownshipId());
		}

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT distinct s.* ");
		strSelectQuery.append(strQuery);
		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT count(distinct s.id) ");
		strCountQuery.append(strQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, StoreEntity.class);
	}

	@Override
	public List<AvailableQuantityDto> getAvailableQuantity(List<String> storeCodes, List<String> productPackingCodes)
			throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		Map<String, Object> params = new HashMap<>();

		strQuery.append(" SELECT pp.code AS productPackingCode, ");
		strQuery.append("		 SUM(spp.total_quantity) - NVL(( ");
		strQuery.append("			SELECT SUM(modd.quantity) ");
		strQuery.append("			FROM merchant_order_detail modd ");
		strQuery.append("				 JOIN merchant_order mo ON mo.id = modd.merchant_order_id ");
		strQuery.append("				 	  AND mo.status = :orderStatus ");
		strQuery.append("				 JOIN store os ON os.id = mo.from_store_id ");
		strQuery.append("				 	  AND os.code IN (:storeCodes) AND os.status = true ");
		strQuery.append("				 JOIN product_packing opp ON opp.id = modd.product_packing_id ");
		strQuery.append("				 	  AND opp.code IN (:productPackingCodes) ");
		strQuery.append("			WHERE modd.product_packing_id = spp.product_packing_id ");
		strQuery.append("			GROUP BY modd.product_packing_id ");
		strQuery.append("		 ), 0) AS quantity ");
		strQuery.append(" FROM store_product_packing spp ");
		strQuery.append("	   JOIN store s ON spp.store_id = s.id");
		strQuery.append("			AND s.code IN (:storeCodes) AND s.status = true ");
		strQuery.append(" 	   JOIN product_packing pp ON pp.id = spp.product_packing_id ");
		strQuery.append("			AND pp.code IN (:productPackingCodes) ");
		strQuery.append(" GROUP BY pp.code ");

		params.put("productPackingCodes", productPackingCodes);
		params.put("storeCodes", storeCodes);
		params.put("orderStatus", MerchantOrderStatus.STATUS_STORE_NOT_ENOUGH_QUANTITY.getValue());

		return BaseRepository.getResultListNativeQuery(this.em, strQuery.toString(), params,
				AvailableQuantityDto.class);
	}

	@Override
	public List<StoreEntity> getStoreByLocationCharge(List<String> townshipCodes) throws BusinessException {
		if ((townshipCodes == null) || townshipCodes.isEmpty()) {
			return new ArrayList<>();
		}
		StringBuilder strQuery = new StringBuilder();
		Map<String, Object> params = new HashMap<>();

		strQuery.append(" SELECT s.* ");
		strQuery.append(" FROM store s ");
		strQuery.append(" 	   JOIN store_location_charge slc ON slc.store_id = s.id ");
		strQuery.append(" 	   JOIN country_location cl ON cl.id = slc.country_location_id ");
		strQuery.append(" 	   		AND cl.location_type = 'TOWNSHIP' ");
		strQuery.append("			AND cl.code IN (:townshipCodes) ");
		strQuery.append(" WHERE s.status = 1 ");

		params.put("townshipCodes", townshipCodes);

		return BaseRepository.getResultListNativeQuery(this.em, strQuery.toString(), params, StoreEntity.class);
	}
}
