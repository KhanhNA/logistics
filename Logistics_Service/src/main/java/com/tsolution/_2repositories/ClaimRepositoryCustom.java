package com.tsolution._2repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.claim.Claim;
import com.tsolution._1entities.claim.dto.ListDamagedGoodsDto;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution.excetions.BusinessException;

public interface ClaimRepositoryCustom {
	Page<Claim> find(Claim claim, List<Long> managedStoreIncludeProvideStores, Pageable pageable)
			throws BusinessException;

	Page<ListDamagedGoodsDto> getListDamagedGoods(Claim claim, List<Long> managedStoreIncludeProvideStores,
			String acceptLanguage, Pageable pageable) throws BusinessException;

	Optional<ProductPackingPriceEntity> getProductPackingPrice(String query) throws BusinessException;

	Optional<Claim> getClaimByTypeAndReferenceId(ClaimType claimType, Long referenceId) throws BusinessException;
}
