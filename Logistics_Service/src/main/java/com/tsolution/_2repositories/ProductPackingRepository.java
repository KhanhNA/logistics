package com.tsolution._2repositories;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.ProductPackingEntity;

public interface ProductPackingRepository
		extends JpaRepository<ProductPackingEntity, Long>, JpaSpecificationExecutor<ProductPackingEntity> {
	ProductPackingEntity findOneByCode(String code);

	@Query(value = " SELECT DISTINCT pp.* " + ProductPackingEntity.FIND, countQuery = " SELECT COUNT(DISTINCT pp.id) "
			+ ProductPackingEntity.FIND, nativeQuery = true)
	Page<ProductPackingEntity> find(@Param("text") String text, @Param("exceptId") Collection<Long> exceptId,
			@Param("manufacturerId") Long manufacturerId, @Param("distributorId") Long distributorId,
			Pageable pageable);
}