package com.tsolution._2repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.CountryLocationEntity;
import com.tsolution._1entities.dto.CountryLocationDto;
import com.tsolution.excetions.BusinessException;

public interface CountryLocationRepositoryCustom {

	List<CountryLocationDto> getLocationsTree() throws BusinessException;

	Page<CountryLocationEntity> findTownship(List<String> townshipCodes, Pageable pageable) throws BusinessException;
}