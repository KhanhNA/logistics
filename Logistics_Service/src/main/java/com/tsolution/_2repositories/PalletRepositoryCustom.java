package com.tsolution._2repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution.excetions.BusinessException;

public interface PalletRepositoryCustom {
	Page<PalletEntity> find(PalletDetailDto palletDetailDto, String acceptLanguage, Pageable pageable)
			throws BusinessException;
}
