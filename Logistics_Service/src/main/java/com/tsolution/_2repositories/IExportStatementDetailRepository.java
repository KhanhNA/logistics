package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.export_statement.ExportStatementDetailEntity;

public interface IExportStatementDetailRepository extends JpaRepository<ExportStatementDetailEntity, Long>, JpaSpecificationExecutor<ExportStatementDetailEntity> {

}