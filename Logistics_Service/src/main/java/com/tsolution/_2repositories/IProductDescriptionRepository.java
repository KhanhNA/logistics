package com.tsolution._2repositories;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.ProductDescriptionEntity;

public interface IProductDescriptionRepository
		extends JpaRepository<ProductDescriptionEntity, Long>, JpaSpecificationExecutor<ProductDescriptionEntity> {

	@Query(value = " SELECT pd.* " + ProductDescriptionEntity.FIND, countQuery = " SELECT count(*) "
			+ ProductDescriptionEntity.FIND, nativeQuery = true)
	Page<ProductDescriptionEntity> find(@Param("language") Long language, @Param("text") String text,
			@Param("exceptId") Collection<Long> exceptId, Pageable pageable);
}