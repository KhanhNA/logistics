package com.tsolution._2repositories;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.merchant_order.MerchantOrderEntity;
import com.tsolution._2repositories.sql.MerchantOrderSql;

public interface MerchantOrderRepository extends JpaRepository<MerchantOrderEntity, Long>,
		JpaSpecificationExecutor<MerchantOrderEntity>, MerchantOrderRepositoryCustom {
	@Query(value = " SELECT mo.* " + MerchantOrderSql.FIND, countQuery = "SELECT COUNT(mo.id) "
			+ MerchantOrderSql.FIND, nativeQuery = true)
	Page<MerchantOrderEntity> search(@Param("text") String text, @Param("fromDate") Date fromDate,
			@Param("toDate") Date toDate, @Param("storeIds") List<Long> storeIds, @Param("status") Integer status,
			@Param("storeId") Long storeId, Pageable pageable);

	Optional<MerchantOrderEntity> findOneByCode(String merchantOrderCode);

	Optional<MerchantOrderEntity> findOneByOrderId(Long orderId);

	@Query(value = MerchantOrderSql.FIND_MERCHANT_ORDER_NOT_ENOUGH_QUANTITY, nativeQuery = true)
	List<MerchantOrderEntity> findMerchantOrderNotEnoughtQuantity(Long storeId, List<Long> productPackingIds);
}