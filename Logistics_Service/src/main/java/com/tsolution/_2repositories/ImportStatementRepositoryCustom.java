package com.tsolution._2repositories;

import com.tsolution.excetions.BusinessException;

public interface ImportStatementRepositoryCustom {
	Long countImportStatementHang(Long storeId) throws BusinessException;
}
