package com.tsolution._2repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.RepackingPlanningEntity;
import com.tsolution._1entities.dto.RepackingPlanningDto;
import com.tsolution.excetions.BusinessException;

public interface RepackingPlanningRepositoryCustom {
	Page<RepackingPlanningEntity> find(RepackingPlanningDto repackingPlanningDto, String username, Pageable pageable)
			throws BusinessException;

	Long countRepackingPlanningHang(Long storeId) throws BusinessException;
}
