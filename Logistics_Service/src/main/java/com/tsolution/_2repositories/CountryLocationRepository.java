package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.CountryLocationEntity;

public interface CountryLocationRepository extends JpaRepository<CountryLocationEntity, Long>,
		JpaSpecificationExecutor<CountryLocationEntity>, CountryLocationRepositoryCustom {
}