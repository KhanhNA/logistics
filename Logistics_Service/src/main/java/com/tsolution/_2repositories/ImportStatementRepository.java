package com.tsolution._2repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.ImportStatementEntity;
import com.tsolution._2repositories.sql.ImportStatementSql;

public interface ImportStatementRepository extends JpaRepository<ImportStatementEntity, Long>,
		JpaSpecificationExecutor<ImportStatementEntity>, ImportStatementRepositoryCustom {
	@Query(value = "SELECT distinct i.* " + ImportStatementSql.FIND, countQuery = "SELECT COUNT(distinct i.id) "
			+ ImportStatementSql.FIND, nativeQuery = true)
	Page<ImportStatementEntity> find(String code, Integer status, String type, String username, Pageable pageable);

	Optional<ImportStatementEntity> findOneByRepackingPlanningId(Long repackingPlanningId);
}