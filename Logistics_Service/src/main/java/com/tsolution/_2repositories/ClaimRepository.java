package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.claim.Claim;

public interface ClaimRepository
		extends JpaRepository<Claim, Long>, JpaSpecificationExecutor<Claim>, ClaimRepositoryCustom {
}