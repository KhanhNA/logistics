package com.tsolution._2repositories.sql;

public class ImportPoSql {
	private ImportPoSql() {
	}

	public static final String FIND = " FROM import_po ip JOIN manufacturer m ON ip.manufacturer_id = m.id "
			+ " JOIN manufacturer_description md ON md.manufacturer_id = m.id "
			+ " JOIN language l ON md.language_id = l.id AND LOWER(l.code) = LOWER(:acceptLanguage) "
			+ " JOIN distributor d ON ip.distributor_id = d.id JOIN users u ON u.username = :username AND (u.distributor_id IS NULL OR u.distributor_id = ip.distributor_id) "
			+ " WHERE ip.store_id = :storeId AND (:distributorId IS NULL OR ip.distributor_id = :distributorId) "
			+ " AND (:text is null OR LOWER(m.code) LIKE LOWER(CONCAT('%',:text,'%')) OR LOWER(md.name) LIKE LOWER(CONCAT('%',:text,'%')) "
			+ " 	 OR LOWER(ip.code) LIKE LOWER(CONCAT('%',:text,'%')) OR LOWER(ip.description) LIKE LOWER(CONCAT('%',:text,'%')) ) "
			+ " AND (:status is null OR ip.status = :status) " + " order by ip.create_date, ip.code ";
}
