package com.tsolution._2repositories.sql;

public class MerchantOrderSql {
	private MerchantOrderSql() {
	}

	public static final String FIND = " FROM merchant_order mo WHERE (:status is null or mo.status = :status) "
			+ " AND (:text IS NULL OR LOWER(mo.code) LIKE LOWER(CONCAT('%',:text,'%')) OR LOWER(mo.merchant_code) LIKE LOWER(CONCAT('%',:text,'%')) "
			+ " 	 OR LOWER(mo.merchant_name) LIKE LOWER(CONCAT('%',:text,'%')) ) AND mo.from_store_id in (:storeIds) "
			+ " AND order_date >= Date(:fromDate) AND order_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)) "
			+ " AND (:storeId is null OR mo.from_store_id = :storeId) ";

	public static final String FIND_MERCHANT_ORDER_NOT_ENOUGH_QUANTITY = " SELECT mo.* "
			+ " FROM merchant_order mo JOIN merchant_order_detail modd ON modd.merchant_order_id = mo.id "
			+ " WHERE mo.from_store_id = :storeId AND (mo.status = 0 OR modd.status = 0) "
			+ " 	  AND modd.product_packing_id in (:productPackingIds) "
			+ " ORDER BY DATE(mo.create_date) asc, mo.amount desc ";
}
