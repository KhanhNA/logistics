package com.tsolution._2repositories.sql;

public class StoreProductPackingSql {
	private StoreProductPackingSql() {
	}

	public static final String NEAR_HERE = " from store_product_packing spp join store s on s.id = spp.store_id AND (:isDC IS NULL OR s.is_DC = :isDC) "
			+ " join product_packing pp on pp.id = spp.product_packing_id "
			+ " where calcDistanceByLatLng(:currentLat, :currentLng, s.lat, s.lng) < :maxDistance"
			+ "       and (:productPackingCode is null or pp.code = :productPackingCode) ";

	public static final String GET_INVENTORY_BY_MERCHANT_ORDER = " SELECT MIN(spp.id) id, spp.store_id, spp.product_packing_id, null code, null QR_code, "
			+ " null product_id, sum(total_quantity) total_quantity, null expire_date, null create_date, null create_user, null update_date, null update_user "
			+ " FROM store_product_packing spp WHERE spp.store_id in (:storeIds) AND spp.product_packing_id in (:productPackingIds) AND spp.expire_date >= SYSDATE() "
			+ " GROUP BY spp.store_id, spp.product_packing_id ";

	public static final String GET_AVAIABLE_FOR_EXPORT = " SELECT spp.id, spp.store_id, spp.product_packing_id, spp.code, spp.QR_code, "
			+ " spp.product_id, spp.total_quantity, spp.expire_date, spp.create_date, spp.create_user, spp.update_date, spp.update_user "
			+ " FROM store_product_packing spp "
			+ " WHERE spp.store_id = :storeId AND spp.product_packing_id in (:productPackingIds) AND spp.total_quantity > 0 "
			+ "		  AND spp.expire_date >= SYSDATE() ";
}
