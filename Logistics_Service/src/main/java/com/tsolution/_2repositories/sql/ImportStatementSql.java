package com.tsolution._2repositories.sql;

public class ImportStatementSql {
	private ImportStatementSql() {
	}

	public static final String FIND = " FROM import_statement i JOIN store_user su ON LOWER(su.username) = LOWER(:username)"
			+ " AND ( (LOWER(:type) = 'from' AND i.from_store_id = su.store_id)"
			+ "    OR (LOWER(:type) = 'to'   AND i.to_store_id = su.store_id) ) "
			+ " WHERE (:code is null OR LOWER(i.code) like LOWER(CONCAT('%',:code,'%'))) AND (:status is null OR i.status = :status) ";
}
