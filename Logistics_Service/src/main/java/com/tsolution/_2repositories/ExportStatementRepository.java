package com.tsolution._2repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.export_statement.ExportStatementEntity;

public interface ExportStatementRepository extends JpaRepository<ExportStatementEntity, Long>,
		JpaSpecificationExecutor<ExportStatementEntity>, ExportStatementRepositoryCustom {
	ExportStatementEntity findOneByCode(String code);

	@Query(value = "SELECT es.* FROM export_statement es WHERE es.merchant_order_id = :merchantOrderId LIMIT 1", nativeQuery = true)
	Optional<ExportStatementEntity> findOneByMerchantOrderId(Long merchantOrderId);
}