package com.tsolution._2repositories.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.UserEntity;
import com.tsolution._2repositories.UserRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class UserRepositoryCustomImpl implements UserRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<UserEntity> find(Boolean status, String text, List<Long> storeIds, Pageable pageable)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" FROM users u ");
		strQuery.append(" WHERE (:status is null OR u.status = :status) ");
		strQuery.append(" 	AND (:text is null ");
		strQuery.append(" 		 OR LOWER(u.username) LIKE LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append(" 		 OR LOWER(u.first_name) LIKE LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append(" 		 OR LOWER(u.last_name) LIKE LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append(" 		 OR LOWER(u.tel) LIKE LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append(" 		 OR LOWER(u.email) LIKE LOWER(CONCAT('%',:text,'%')) ) ");
		if ((storeIds != null) && !storeIds.isEmpty()) {
			strQuery.append(" 	AND (SELECT 1 FROM store_user su ");
			strQuery.append(" 		 WHERE su.username = u.username ");
			strQuery.append(" 		   AND su.store_id IN (:storeIds) )");
			params.put("storeIds", storeIds);
		}
		strQuery.append(" ORDER BY u.username, u.first_name, u.last_name ");

		params.put("text", text);
		params.put("status", status);

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT u.* ").append(strQuery);

		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT count(u.username) ").append(strQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, UserEntity.class);
	}

}
