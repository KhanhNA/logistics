package com.tsolution._2repositories.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.claim.Claim;
import com.tsolution._1entities.claim.dto.ListDamagedGoodsDto;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution._2repositories.ClaimRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;

@Repository
public class ClaimRepositoryCustomImpl implements ClaimRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<Claim> find(Claim claim, List<Long> managedStoreIncludeProvideStores, Pageable pageable)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" FROM claim c ");
		strQuery.append(" WHERE ((:storeId IS NULL AND c.store_id in (:managedStoreIncludeProvideStores)) ");
		strQuery.append("			OR c.store_id = :storeId) ");
		strQuery.append("		AND (:type IS NULL OR LOWER(c.type) = LOWER(:type)) ");
		strQuery.append("		AND (:referenceId IS NULL OR c.reference_id = :referenceId) ");
		strQuery.append("		AND (:amenable IS NULL OR LOWER(c.amenable) like LOWER(CONCAT('%',:amenable,'%'))) ");
		strQuery.append("		AND (:des IS NULL OR LOWER(c.description) like LOWER(CONCAT('%',:des,'%'))) ");
		strQuery.append("		AND (:status IS NULL OR c.status = :status) ");
		strQuery.append("		AND (:code IS NULL OR LOWER(c.code) like LOWER(CONCAT('%',:code,'%'))) ");
		strQuery.append("		AND (:fromCreateDate IS NULL OR c.create_date >= Date(:fromCreateDate)) ");
		strQuery.append("		AND (:toCreateDate IS NULL ");
		strQuery.append("			 OR c.create_date < Date(DATE_ADD(:toCreateDate, INTERVAL 1 DAY)) )");

		params.put("managedStoreIncludeProvideStores", managedStoreIncludeProvideStores);
		params.put("storeId", claim.getStore().getId());
		params.put("type", claim.getType() == null ? null : claim.getType().getValue());
		params.put("referenceId", claim.getReferenceId());
		params.put("amenable", claim.getAmenable());
		params.put("des", claim.getDescription());
		params.put("status", claim.getStatus());
		params.put("code", claim.getCode());
		params.put("fromCreateDate", claim.getFromCreateDate());
		params.put("toCreateDate", claim.getToCreateDate());

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT DISTINCT c.* ");
		strSelectQuery.append(strQuery);
		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(DISTINCT c.id) ");
		strCountQuery.append(strQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, Claim.class);
	}

	@Override
	public Optional<ProductPackingPriceEntity> getProductPackingPrice(String query) throws BusinessException {
		return BaseRepository.getFirstOptionalResultNativeQuery(this.em, query, new HashMap<>(),
				ProductPackingPriceEntity.class);
	}

	@Override
	public Optional<Claim> getClaimByTypeAndReferenceId(ClaimType claimType, Long referenceId)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT c.* FROM claim c ");
		strQuery.append(" WHERE UPPER(c.type) = UPPER(:type) AND c.reference_id = :referenceId ");
		strQuery.append(" LIMIT 1 ");

		params.put("type", claimType.getValue());
		params.put("referenceId", referenceId);

		return BaseRepository.getFirstOptionalResultNativeQuery(this.em, strQuery.toString(), params, Claim.class);
	}

	@Override
	public Page<ListDamagedGoodsDto> getListDamagedGoods(Claim claim, List<Long> managedStoreIncludeProvideStores,
			String acceptLanguage, Pageable pageable) throws BusinessException {
		List<String> groupByColumns = new ArrayList<>();
		groupByColumns.add("s.code");
		groupByColumns.add("s.name");
		groupByColumns.add("pp.code");
		groupByColumns.add("pd.name");
		groupByColumns.add("cd.expire_date");

		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT MIN(cd.id) AS id, ");
		if ((claim.getType() == null) || StringUtils.isNullOrEmpty(claim.getType().getValue())) {
			strQuery.append("	 '' AS claim_type, ");
		} else {
			strQuery.append("	 c.type AS claim_type, ");
			groupByColumns.add("c.type");
		}
		if (StringUtils.isNullOrEmpty(claim.getAmenable())) {
			strQuery.append("	 '' AS amenable, ");
		} else {
			strQuery.append("	 c.amenable AS amenable, ");
			groupByColumns.add("c.amenable");
		}
		strQuery.append(" 		 s.code AS store_code, s.name AS store_name, ");
		strQuery.append(" 		 pp.code AS product_packing_code, ");
		strQuery.append(" 		 pd.name AS product_packing_name, ");
		strQuery.append(" 		 cd.expire_date AS expire_date, ");
		strQuery.append(" 		 SUM(cd.quantity) AS quantity ");
		strQuery.append(" FROM claim_detail cd ");
		strQuery.append(" 	   JOIN claim c ON c.id = cd.claim_id ");
		strQuery.append(" 	   JOIN store s ON s.id = c.store_id ");
		strQuery.append(" 	   JOIN product_packing pp ON pp.id = cd.product_packing_id ");
		strQuery.append(" 	   JOIN product p ON p.id = pp.product_id ");
		strQuery.append(" 	   JOIN product_description pd ON pd.product_id = p.id ");
		strQuery.append(" 	   JOIN language l ON l.id = pd.language_id ");
		strQuery.append(" WHERE c.status = :status AND s.id = :storeId ");
		strQuery.append(" 		AND s.id IN (:managedStoreIncludeProvideStores) ");
		strQuery.append(" 		AND LOWER(l.code) = LOWER(:acceptLanguage) ");
		strQuery.append("		AND (:fromCreateDate IS NULL OR c.create_date >= Date(:fromCreateDate)) ");
		strQuery.append("		AND (:toCreateDate IS NULL ");
		strQuery.append("			 OR c.create_date < Date(DATE_ADD(:toCreateDate, INTERVAL 1 DAY)) ) ");
		if ((claim.getType() != null) && !StringUtils.isNullOrEmpty(claim.getType().getValue())) {
			strQuery.append("	AND LOWER(c.type) LIKE LOWER(CONCAT('%',:claimType,'%')) ");
			params.put("claimType", claim.getType().getValue());
		}
		if (!StringUtils.isNullOrEmpty(claim.getAmenable())) {
			strQuery.append("	AND LOWER(c.amenable) LIKE LOWER(CONCAT('%',:amenable,'%')) ");
			params.put("amenable", claim.getAmenable());
		}
		if (!StringUtils.isNullOrEmpty(claim.getProductPackingCode())) {
			strQuery.append("	AND LOWER(pp.code) LIKE LOWER(CONCAT('%',:productPackingCode,'%')) ");
			params.put("productPackingCode", claim.getProductPackingCode());
		}
		if (!StringUtils.isNullOrEmpty(claim.getProductName())) {
			strQuery.append("	AND LOWER(p.name) LIKE LOWER(CONCAT('%',:productName,'%')) ");
			params.put("productName", claim.getProductName());
		}
		if (!groupByColumns.isEmpty()) {
			strQuery.append(" GROUP BY ").append(String.join(", ", groupByColumns));
		}
		params.put("status", claim.getStatus());
		params.put("storeId", claim.getStore().getId());
		params.put("managedStoreIncludeProvideStores", managedStoreIncludeProvideStores);
		params.put("acceptLanguage", acceptLanguage);
		params.put("fromCreateDate", claim.getFromCreateDate());
		params.put("toCreateDate", claim.getToCreateDate());

		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(*) FROM ( ").append(strQuery).append(" ) t ");

		return BaseRepository.getPagedNativeQuery(this.em, strQuery.toString(), strCountQuery.toString(), params,
				pageable, ListDamagedGoodsDto.class);
	}

}
