package com.tsolution._2repositories.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.dto.PoDto;
import com.tsolution._1entities.enums.PoStatus;
import com.tsolution._2repositories.PoRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class PoRepositoryCustomImpl implements PoRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<PoEntity> find(PoDto poDto, String username, Pageable pageable, String acceptLanguage)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" FROM po po JOIN manufacturer manufacturer ON po.manufacturer_id = manufacturer.id ");
		strQuery.append("	   JOIN manufacturer_description manufacturer_description ");
		strQuery.append("			ON manufacturer_description.manufacturer_id = manufacturer.id ");
		strQuery.append("	   JOIN language language ON manufacturer_description.language_id = language.id ");
		strQuery.append("			AND LOWER(language.code) = LOWER(:acceptLanguage) ");
		strQuery.append("	   JOIN distributor distributor ON po.distributor_id = distributor.id ");
		strQuery.append("	   JOIN users u ON u.username = :username ");
		strQuery.append("					AND (u.distributor_id IS NULL OR u.distributor_id = po.distributor_id) ");
		strQuery.append(" WHERE (:text is null ");
		strQuery.append("		 OR LOWER(manufacturer.code) LIKE LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append("		 OR LOWER(manufacturer_description.name) LIKE LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append("		 OR LOWER(po.code) LIKE LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append("		 OR LOWER(po.description) LIKE LOWER(CONCAT('%',:text,'%')) ) ");
		strQuery.append("		AND po.store_id = :storeId ");
		strQuery.append("		AND (:manufacturerId IS NULL OR po.manufacturer_id = :manufacturerId) ");
		strQuery.append("		AND (:distributorId IS NULL OR po.distributor_id = :distributorId) ");
		strQuery.append("		AND (:status is null OR po.status = :status) ");
		switch (poDto.getDateType()) {
		case NEW:
			strQuery.append("	AND (po.create_date >= Date(:fromDate) ");
			strQuery.append("	     AND po.create_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)) )");
			break;
		case APPROVED:
			strQuery.append("	AND (po.approve_date >= Date(:fromDate) ");
			strQuery.append("	     AND po.approve_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)) )");
			break;
		case IMPORTED:
			strQuery.append("	AND (po.import_date >= Date(:fromDate) ");
			strQuery.append("	     AND po.import_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)) )");
			break;
		case ARRIVED_VIETNAM_PORT:
			strQuery.append("	AND (po.arrived_vietnam_port_date >= Date(:fromDate) ");
			strQuery.append("	     AND po.arrived_vietnam_port_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)) )");
			break;
		case ARRIVED_MYANMAR_PORT:
			strQuery.append("	AND (po.arrived_myanmar_port_date >= Date(:fromDate) ");
			strQuery.append("	     AND po.arrived_myanmar_port_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)) )");
			break;
		case REJECT:
			strQuery.append("	AND (po.update_date >= Date(:fromDate) ");
			strQuery.append("	     AND po.update_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)) )");
			break;
		default:
			break;
		}

		params.put("storeId", poDto.getStoreId());
		params.put("manufacturerId", poDto.getManufacturerId());
		params.put("distributorId", poDto.getDistributorId());
		params.put("text", poDto.getText());
		params.put("fromDate", poDto.getFromDate());
		params.put("toDate", poDto.getToDate());
		params.put("status", poDto.getStatus() == null ? null : poDto.getStatus().getValue());
		params.put("username", username);
		params.put("acceptLanguage", acceptLanguage);

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT po.* ");
		strSelectQuery.append(strQuery);
		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(DISTINCT po.id) ");
		strCountQuery.append(strQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, PoEntity.class);
	}

	@Override
	public Long countPoHang(Long storeId) throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT COUNT(DISTINCT po.id) ");
		strQuery.append(" FROM po po ");
		strQuery.append(" WHERE po.store_id = :storeId ");
		strQuery.append("   AND po.status not in (:status) ");

		Map<String, Object> params = new HashMap<>();
		params.put("storeId", storeId);
		params.put("status", Arrays.asList(PoStatus.IMPORTED.getValue(), PoStatus.REJECT.getValue()));

		Number count = BaseRepository.getScalarResult(this.em, strQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}

	@Override
	public List<PoEntity> getTaskListPo(String username, List<String> statuses) throws BusinessException {
		if ((statuses == null) || statuses.isEmpty()) {
			return new ArrayList<>();
		}
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT po.* ");
		strQuery.append(" FROM po po ");
		strQuery.append("	   JOIN distributor d ON po.distributor_id = d.id ");
		strQuery.append("	   JOIN users u ON u.username = :username ");
		strQuery.append("					AND (u.distributor_id IS NULL OR u.distributor_id = po.distributor_id) ");
		strQuery.append("	   JOIN store_user su ON su.store_id = po.store_id AND su.username = :username ");
		strQuery.append(" WHERE po.status in (:statuses) ");

		params.put("username", username);
		params.put("statuses", statuses);

		return BaseRepository.getResultListNativeQuery(this.em, strQuery.toString(), params, PoEntity.class);
	}

}
