package com.tsolution._2repositories.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.tsolution._1entities.merchant_order.enums.MerchantOrderStatus;
import com.tsolution._2repositories.MerchantOrderRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class MerchantOrderRepositoryCustomImpl implements MerchantOrderRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long countMerchantOrderHang(Long storeId) throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT COUNT(DISTINCT mo.id) ");
		strQuery.append(" FROM merchant_order mo ");
		strQuery.append(" WHERE mo.from_store_id = :storeId ");
		strQuery.append("   AND mo.status not in (:status) ");

		Map<String, Object> params = new HashMap<>();
		params.put("storeId", storeId);
		params.put("status", Arrays.asList(MerchantOrderStatus.STATUS_STORE_EXPORTED.getValue(),
				MerchantOrderStatus.STATUS_CANCELED.getValue(), MerchantOrderStatus.STATUS_RETURNED.getValue()));

		Number count = BaseRepository.getScalarResult(this.em, strQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}

}
