package com.tsolution._2repositories.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.RepackingPlanningEntity;
import com.tsolution._1entities.dto.RepackingPlanningDto;
import com.tsolution._1entities.enums.RepackingPlanningStatus;
import com.tsolution._2repositories.RepackingPlanningRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class RepackingPlanningRepositoryCustomImpl implements RepackingPlanningRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<RepackingPlanningEntity> find(RepackingPlanningDto repackingPlanningDto, String username,
			Pageable pageable) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" FROM repacking_planning rp ");
		strQuery.append("	   JOIN distributor d ON rp.distributor_id = d.id ");
		strQuery.append("	   JOIN users u ON u.username = :username ");
		strQuery.append("					AND (u.distributor_id IS NULL OR u.distributor_id = rp.distributor_id) ");
		strQuery.append(" WHERE (:text is null OR LOWER(rp.code) LIKE LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append("	   	 OR LOWER(rp.description) LIKE LOWER(CONCAT('%',:text,'%')) ) ");
		strQuery.append("		AND (:status is null OR rp.status = :status) ");
		strQuery.append("		AND ( ");
		strQuery.append("			(:dateType = 0 AND rp.planning_date >= Date(:fromDate) ");
		strQuery.append("						   AND rp.planning_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY))) ");
		strQuery.append("		 OR (:dateType = 1 AND rp.repacked_date >= Date(:fromDate) ");
		strQuery.append("						   AND rp.repacked_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY))) ");
		strQuery.append("		 OR (:dateType = 2 AND rp.imported_date >= Date(:fromDate) ");
		strQuery.append("						   AND rp.imported_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY))) ");
		strQuery.append("		 OR (:dateType = -1 AND rp.update_date >= Date(:fromDate) ");
		strQuery.append("						    AND rp.update_date < Date(DATE_ADD(:toDate, INTERVAL 1 DAY)))  ) ");
		strQuery.append("		AND rp.store_id = :storeId ");
		strQuery.append("		AND (:distributorId IS NULL OR rp.distributor_id = :distributorId) ");

		params.put("storeId", repackingPlanningDto.getStoreId());
		params.put("distributorId", repackingPlanningDto.getDistributorId());
		params.put("text", repackingPlanningDto.getText());
		params.put("dateType", repackingPlanningDto.getDateType());
		params.put("fromDate", repackingPlanningDto.getFromDate());
		params.put("toDate", repackingPlanningDto.getToDate());
		params.put("status", repackingPlanningDto.getStatus());
		params.put("username", username);

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT DISTINCT rp.* ");
		strSelectQuery.append(strQuery);
		strSelectQuery.append(" ORDER BY rp.planning_date, rp.code, rp.repacked_date ");
		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(DISTINCT rp.id) ");
		strCountQuery.append(strQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, RepackingPlanningEntity.class);
	}

	@Override
	public Long countRepackingPlanningHang(Long storeId) throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT COUNT(DISTINCT rp.id) ");
		strQuery.append(" FROM repacking_planning rp ");
		strQuery.append(" WHERE rp.store_id = :storeId ");
		strQuery.append("   AND rp.status not in (:status) ");

		Map<String, Object> params = new HashMap<>();
		params.put("storeId", storeId);
		params.put("status",
				Arrays.asList(RepackingPlanningStatus.IMPORTED.getValue(), RepackingPlanningStatus.CANCEL.getValue()));

		Number count = BaseRepository.getScalarResult(this.em, strQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}
}
