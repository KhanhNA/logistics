package com.tsolution._2repositories.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.CountryLocationEntity;
import com.tsolution._1entities.dto.CountryLocationDto;
import com.tsolution._1entities.enums.LocationType;
import com.tsolution._2repositories.CountryLocationRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class CountryLocationRepositoryCustomImpl implements CountryLocationRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<CountryLocationDto> getLocationsTree() throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT CONCAT(NVL(c.id, 0), NVL(region.id, 0), ");
		strQuery.append("		 	    NVL(district.id, 0), NVL(township.id, 0)) id, ");
		strQuery.append("		 c.id countryId, c.isocode countryCode, ");
		strQuery.append("		 region.id regionId, region.code regionCode, region.name regionName, ");
		strQuery.append("		 region.country_id regionCountryId, ");
		strQuery.append("		 district.id districtId, district.code districtCode, district.name districtName, ");
		strQuery.append("		 district.parent_id districtParentId, ");
		strQuery.append("		 township.id townshipId, township.code townshipCode, township.name townshipName, ");
		strQuery.append("		 township.parent_id townshipParentId ");
		strQuery.append(" FROM country c ");
		strQuery.append(" 	   LEFT JOIN country_location region ");
		strQuery.append(" 	   		ON c.id = region.country_id ");
		strQuery.append(" 	   		AND region.status = 1 ");
		strQuery.append(" 	   		AND region.location_type = 'REGION' ");
		strQuery.append(" 	   LEFT JOIN country_location district ");
		strQuery.append(" 	   		ON c.id = district.country_id ");
		strQuery.append(" 	   		AND district.status = 1 ");
		strQuery.append(" 	   		AND district.location_type = 'DISTRICT' ");
		strQuery.append(" 	   		AND district.parent_id = region.id ");
		strQuery.append(" 	   LEFT JOIN country_location township ");
		strQuery.append(" 	   		ON c.id = township.country_id ");
		strQuery.append(" 	   		AND township.status = 1 ");
		strQuery.append(" 	   		AND township.location_type = 'TOWNSHIP' ");
		strQuery.append(" 	   		AND township.parent_id = district.id ");
		strQuery.append(" WHERE c.is_support = 1 ");
		Map<String, Object> params = new HashMap<>();
		return BaseRepository.getResultListNativeQuery(this.em, strQuery.toString(), params, CountryLocationDto.class);
	}

	@Override
	public Page<CountryLocationEntity> findTownship(List<String> townshipCodes, Pageable pageable)
			throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		strQuery.append(" FROM country_location cl ");
		strQuery.append(" WHERE cl.location_type LIKE :locationType ");
		strQuery.append("		AND cl.status = 1 ");
		if ((townshipCodes != null) && !townshipCodes.isEmpty()) {
			strQuery.append("	AND cl.code IN (:townshipCodes) ");
			params.put("townshipCodes", townshipCodes);
		}
		strQuery.append(" ORDER BY cl.code, cl.name ");

		params.put("locationType", LocationType.TOWNSHIP.getValue());

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT cl.* ").append(strQuery);

		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(DISTINCT cl.id) ").append(strQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, CountryLocationEntity.class);
	}

}
