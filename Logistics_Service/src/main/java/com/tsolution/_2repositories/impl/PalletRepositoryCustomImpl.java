package com.tsolution._2repositories.impl;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution._2repositories.PalletRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;

@Repository
public class PalletRepositoryCustomImpl implements PalletRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<PalletEntity> find(PalletDetailDto palletDetailDto, String acceptLanguage, Pageable pageable)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" FROM pallet p ");
		if (!StringUtils.isNullOrEmpty(palletDetailDto.getText())) {
			strQuery.append("  JOIN pallet_detail pd ON p.id = pd.pallet_id AND pd.quantity > 0 ");
			strQuery.append("  JOIN product_packing pp ON pp.id = pd.product_packing_id ");
			strQuery.append("  JOIN product pr ON pr.id = pd.product_id ");
			strQuery.append("  JOIN product_description prd ON pr.id = prd.product_id ");
			strQuery.append("  JOIN language l ON l.id = prd.language_id ");
			strQuery.append("		AND LOWER(l.code) = LOWER(:acceptLanguage) ");
		}
		strQuery.append(" WHERE p.store_id = :storeId AND (:status IS NULL OR p.status = :status ) ");
		strQuery.append("	AND (:step IS NULL OR p.step = :step) ");
		strQuery.append("	AND (:text is null ");
		strQuery.append("	 	 OR LOWER(p.code) like LOWER(CONCAT('%',:text,'%')) ");
		strQuery.append("	 	 OR LOWER(p.name) like LOWER(CONCAT('%',:text,'%')) ");
		if (!StringUtils.isNullOrEmpty(palletDetailDto.getText())) {
			strQuery.append("	 OR LOWER(pp.code) like LOWER(CONCAT('%',:text,'%')) ");
			strQuery.append("	 OR LOWER(prd.name) like LOWER(CONCAT('%',:text,'%')) ");
		}
		strQuery.append("		) ");
		strQuery.append(" ORDER BY p.code, p.name ");

		params.put("storeId", palletDetailDto.getStoreId());
		params.put("status", palletDetailDto.getStatus());
		params.put("step", palletDetailDto.getStep());
		params.put("text", palletDetailDto.getText());
		if (!StringUtils.isNullOrEmpty(palletDetailDto.getText())) {
			params.put("acceptLanguage", acceptLanguage);
		}

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT DISTINCT p.* ");
		strSelectQuery.append(strQuery);
		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(DISTINCT p.id) ");
		strCountQuery.append(strQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, PalletEntity.class);
	}

}
