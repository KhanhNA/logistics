package com.tsolution._2repositories.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.PalletDetailEntity;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution._2repositories.PalletDetailRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class PalletDetailRepositoryCustomImpl implements PalletDetailRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<PalletDetailEntity> findGroupByProductPackingAndExpireDate(PalletDetailDto palletDetailDto,
			Long repackingPlanningId, Pageable pageable) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT min(pld.id) as id, pld.product_packing_id, date(pld.expire_date) as expire_date, ");
		strQuery.append("	 	 NVL(rpd.quantity, 0) + SUM(NVL(pld.quantity, 0)) quantity, ");
		strQuery.append("		 null as product_id, null store_product_packing_detail_id, ");
		strQuery.append("  		 null as pallet_id, null as org_quantity, null as create_date, null as create_user, ");
		strQuery.append("  		 null as update_date, null as update_user ");
		strQuery.append(" FROM pallet_detail pld ");
		strQuery.append("	   JOIN pallet p ON p.id = pld.pallet_id AND p.step in (:palletSteps) ");
		strQuery.append("					 AND (:palletId is null OR p.id = :palletId) ");
		strQuery.append("					 AND p.store_id = :storeId ");
		strQuery.append("	   LEFT JOIN ( ");
		strQuery.append("			SELECT product_packing_id, expire_date, SUM(quantity) quantity ");
		strQuery.append("			FROM repacking_planning_detail ");
		strQuery.append("			WHERE repacking_planning_id = :repackingPlanningId ");
		strQuery.append("			GROUP BY product_packing_id, expire_date ");
		strQuery.append("	   ) rpd ON pld.product_packing_id = rpd.product_packing_id ");
		strQuery.append("	   			AND pld.expire_date = rpd.expire_date ");
		strQuery.append(" WHERE pld.product_packing_id in ( ");
		strQuery.append("			SELECT pp.id ");
		strQuery.append("			FROM product_packing pp join product p on pp.product_id = p.id ");
		strQuery.append("				 JOIN product_packing_price ppp on pp.id = ppp.product_packing_id ");
		strQuery.append("				 JOIN distributor d ON pp.distributor_id = d.id ");
		strQuery.append("				 JOIN users u ON u.username = :username ");
		strQuery.append("					  		  AND (u.distributor_id IS NULL ");
		strQuery.append("								   OR u.distributor_id = pp.distributor_id) ");
		strQuery.append("				 LEFT JOIN product_description pd on pd.product_id = p.id ");
		strQuery.append("			WHERE p.available = 1 ");
		strQuery.append("				  AND (:distributorId IS NULL OR pp.distributor_id = :distributorId) ");
		strQuery.append("				  AND ( :text is null OR ");
		strQuery.append("				  		LOWER(p.code)  LIKE LOWER(CONCAT('%',:text,'%')) OR ");
		strQuery.append("				  		LOWER(p.name)  LIKE LOWER(CONCAT('%',:text,'%')) OR ");
		strQuery.append("				  		LOWER(pp.code) LIKE LOWER(CONCAT('%',:text,'%')) OR ");
		strQuery.append("				  		LOWER(pp.name) LIKE LOWER(CONCAT('%',:text,'%')) OR ");
		strQuery.append("				  		LOWER(pd.code) LIKE LOWER(CONCAT('%',:text,'%')) OR ");
		strQuery.append("				  		LOWER(pd.name) like LOWER(CONCAT('%',:text,'%')) ) ");
		strQuery.append("			ORDER BY p.sort_order, p.is_hot, p.is_popular, p.quantity_ordered, p.code ");
		strQuery.append("		) ");
		if (!palletDetailDto.getProductPackingExpireDates().isEmpty()) {
			strQuery.append("	AND ( ");
			strQuery.append("			pld.product_packing_id, ");
			strQuery.append("			DATE_FORMAT(DATE(pld.expire_date), '%Y-%m-%d') ");
			strQuery.append("		) ");
			strQuery.append(Boolean.TRUE.equals(palletDetailDto.getIsIncludeProductPackingIdExpireDate()) ? " IN "
					: " NOT IN ");
			strQuery.append("		( ");
			for (int i = 0; i < palletDetailDto.getProductPackingExpireDates().size(); i++) {
				if (i == 0) {
					strQuery.append(String.format("	  (:product_packing_id%d, :expire_date%d) ", i, i));
				} else {
					strQuery.append(String.format("	  ,(:product_packing_id%d, :expire_date%d) ", i, i));
				}
			}
			strQuery.append("		) ");
		}
		strQuery.append("		AND (:palletId IS NOT NULL ");
		strQuery.append("			 OR (:palletId is null AND pld.expire_date >= SYSDATE()) ) ");
		strQuery.append(" GROUP BY pld.product_packing_id, date(pld.expire_date) HAVING quantity > 0 ");

		params.put("storeId", palletDetailDto.getStoreId());
		params.put("distributorId", palletDetailDto.getDistributorId());
		params.put("text", palletDetailDto.getText());
		params.put("palletId", palletDetailDto.getPalletId());
		params.put("palletSteps", palletDetailDto.getPalletSteps());
		params.put("username", palletDetailDto.getUsername());
		params.put("repackingPlanningId", repackingPlanningId == null ? 0 : repackingPlanningId);

		for (int i = 0; i < palletDetailDto.getProductPackingExpireDates().size(); i++) {
			params.put(String.format("product_packing_id%d", i), palletDetailDto.getProductPackingIds().get(i));
			params.put(String.format("expire_date%d", i), palletDetailDto.getProductPackingExpireDates().get(i));
		}

		StringBuilder strSelectQuery = new StringBuilder(strQuery);
		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(*) FROM ( ");
		strCountQuery.append(strQuery);
		strCountQuery.append(" ) x ");

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, PalletDetailEntity.class);
	}

	@Override
	public Page<PalletDetailEntity> findGroupByPalletStepAndProductPackingAndExpireDate(PalletDetailDto palletDetailDto,
			List<Long> managedStoreIds, Pageable pageable) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT MAX(pld.id) id, pld.pallet_id, pld.product_id, pld.product_packing_id, ");
		strQuery.append("		 sum(pld.quantity) as quantity, SUM(pld.org_quantity) org_quantity, ");
		strQuery.append("		 DATE(pld.expire_date) expire_date, NULL store_product_packing_detail_id, ");
		strQuery.append("  		 NULL create_date, NULL create_user, NULL update_date, NULL update_user ");
		strQuery.append(" FROM pallet_detail pld ");
		strQuery.append("	   JOIN product_packing pp ON pp.id = pld.product_packing_id ");
		strQuery.append("	   JOIN pallet pl ON pl.id = pld.pallet_id AND pl.step IN (:palletSteps) ");
		strQuery.append("						 AND (:palletId IS NULL OR pl.id = :palletId) ");
		strQuery.append("					 	 AND pl.store_id = :storeId ");
		strQuery.append("					 	 AND pl.store_id in (:managedStoreIds) ");
		strQuery.append(" WHERE pld.product_packing_id IN ( ");
		strQuery.append("			SELECT DISTINCT pp.id ");
		strQuery.append("			FROM product_packing pp ");
		strQuery.append("				 JOIN product p ON pp.product_id = p.id ");
		strQuery.append("				 JOIN product_description pd ON pd.product_id = p.id ");
		strQuery.append("				 JOIN distributor d ON pp.distributor_id = d.id ");
		strQuery.append("					  AND (:distributorId IS NULL OR d.id = :distributorId) ");
		strQuery.append("				 JOIN users u ON u.username = :username ");
		strQuery.append("				 	  AND ( u.distributor_id IS NULL OR ");
		strQuery.append("				 	  		u.distributor_id = pp.distributor_id ) ");
		strQuery.append("			WHERE ( :text IS NULL OR ");
		strQuery.append("					LOWER(pd.name) LIKE LOWER(CONCAT('%',:text,'%')) OR");
		strQuery.append("					LOWER(pp.code) LIKE LOWER(CONCAT('%',:text,'%')) ) ");
		strQuery.append("			  AND ( :productPackingCode IS NULL OR ");
		strQuery.append("					LOWER(pp.code) LIKE LOWER(CONCAT('%',:productPackingCode,'%')) ) ");
		strQuery.append("			  AND ( :productPackingName IS NULL OR ");
		strQuery.append("					LOWER(pd.name) LIKE LOWER(CONCAT('%',:productPackingName,'%')) ) ");
		strQuery.append("		) ");
		if (!palletDetailDto.getProductPackingExpireDates().isEmpty()) {
			strQuery.append(" 	AND ( ");
			strQuery.append("		pld.product_packing_id, ");
			strQuery.append("		DATE_FORMAT(DATE(pld.expire_date), '%Y-%m-%d') ");
			strQuery.append("	) NOT IN ( ");
			for (int i = 0; i < palletDetailDto.getProductPackingExpireDates().size(); i++) {
				if (i == 0) {
					strQuery.append(String.format("	  (:product_packing_id%d, :expire_date%d) ", i, i));
				} else {
					strQuery.append(String.format("	  ,(:product_packing_id%d, :expire_date%d) ", i, i));
				}
			}
			strQuery.append("	) ");
		}
		if (palletDetailDto.getExpireDateColor() != null) {
			strQuery.append(palletDetailDto.getExpireDateColor().generateQuery("pld.expire_date", "pp.lifecycle",
					"pp.warning_threshold"));
		}
		strQuery.append(" GROUP BY pld.pallet_id, pld.product_id, pld.product_packing_id, DATE(pld.expire_date) ");
		strQuery.append(" HAVING SUM(pld.quantity) > 0 ");

		params.put("text", palletDetailDto.getText());
		params.put("productPackingCode", palletDetailDto.getProductPackingCode());
		params.put("productPackingName", palletDetailDto.getProductPackingName());
		params.put("palletId", palletDetailDto.getPalletId());
		params.put("distributorId", palletDetailDto.getDistributorId());
		params.put("storeId", palletDetailDto.getStoreId());
		params.put("palletSteps", palletDetailDto.getPalletSteps());
		params.put("managedStoreIds", managedStoreIds);
		params.put("username", palletDetailDto.getUsername());
		for (int i = 0; i < palletDetailDto.getProductPackingExpireDates().size(); i++) {
			params.put(String.format("product_packing_id%d", i), palletDetailDto.getProductPackingIds().get(i));
			params.put(String.format("expire_date%d", i), palletDetailDto.getProductPackingExpireDates().get(i));
		}

		StringBuilder strSelectQuery = new StringBuilder(strQuery);
		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(*) FROM ( ");
		strCountQuery.append(strQuery);
		strCountQuery.append(" ) x ");

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, PalletDetailEntity.class);
	}

}
