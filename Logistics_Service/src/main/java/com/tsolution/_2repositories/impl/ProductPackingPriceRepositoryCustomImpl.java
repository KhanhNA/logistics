package com.tsolution._2repositories.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.enums.SaleType;
import com.tsolution._2repositories.ProductPackingPriceRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class ProductPackingPriceRepositoryCustomImpl implements ProductPackingPriceRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ProductPackingPriceEntity> getCurrentPrices(Long productPackingId, SaleType saleType)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT ppp.* ");
		strQuery.append(" FROM product_packing_price ppp ");
		strQuery.append(" WHERE ppp.product_packing_id = :productPackingId ");
		strQuery.append(" 		AND ppp.from_date <= SYSDATE() ");
		strQuery.append(" 		AND ( ppp.to_date IS NULL OR ppp.to_date >= DATE(SYSDATE()) ) ");
		strQuery.append("		AND ppp.status = 1 AND LOWER(ppp.sale_type) = :saleType ");
		strQuery.append(" LIMIT 1 ");

		params.put("productPackingId", productPackingId);
		params.put("saleType", saleType.getValue().toLowerCase());

		return BaseRepository.getResultListNativeQuery(this.em, strQuery.toString(), params,
				ProductPackingPriceEntity.class);
	}

	@Override
	public List<ProductPackingPriceEntity> getLastestPrices(Long productPackingId, SaleType saleType)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT ppp.* ");
		strQuery.append(" FROM product_packing_price ppp ");
		strQuery.append(" WHERE ppp.product_packing_id = :productPackingId ");
		strQuery.append("		AND ppp.status = 1 AND LOWER(ppp.sale_type) = :saleType ");
		strQuery.append("		AND ppp.from_date = ( ");
		strQuery.append(" 	   		SELECT MAX(x.from_date) AS fromDate ");
		strQuery.append(" 	   		FROM product_packing_price x ");
		strQuery.append(" 	   		WHERE x.product_packing_id = :productPackingId ");
		strQuery.append(" 	   			  AND x.status = 1 AND LOWER(x.sale_type) = :saleType ");
		strQuery.append(" 	   ) ");
//		strQuery.append(" LIMIT 1 ")

		params.put("productPackingId", productPackingId);
		params.put("saleType", saleType.getValue().toLowerCase());

		return BaseRepository.getResultListNativeQuery(this.em, strQuery.toString(), params,
				ProductPackingPriceEntity.class);
	}
}
