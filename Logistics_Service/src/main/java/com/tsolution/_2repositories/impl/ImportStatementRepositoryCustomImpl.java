package com.tsolution._2repositories.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.tsolution._1entities.enums.ImportStatementStatus;
import com.tsolution._2repositories.ImportStatementRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class ImportStatementRepositoryCustomImpl implements ImportStatementRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long countImportStatementHang(Long storeId) throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT COUNT(DISTINCT iss.id) ");
		strQuery.append(" FROM import_statement iss ");
		strQuery.append(" WHERE iss.to_store_id = :storeId ");
		strQuery.append("   AND iss.status not in (:status) ");

		Map<String, Object> params = new HashMap<>();
		params.put("storeId", storeId);
		params.put("status", Arrays.asList(ImportStatementStatus.UPDATED_INVENTORY.getValue()));

		Number count = BaseRepository.getScalarResult(this.em, strQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}

}
