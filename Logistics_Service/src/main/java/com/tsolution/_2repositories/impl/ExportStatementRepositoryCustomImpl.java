package com.tsolution._2repositories.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.enums.ExportStatementStatus;
import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.export_statement.dto.ExportStatementFilterDto;
import com.tsolution._2repositories.ExportStatementRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.DatetimeUtils;

@Repository
public class ExportStatementRepositoryCustomImpl implements ExportStatementRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long countExportStatementHang(Long storeId) throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT COUNT(DISTINCT es.id) ");
		strQuery.append(" FROM export_statement es ");
		strQuery.append(" WHERE es.from_store_id = :storeId ");
		strQuery.append("   AND es.status not in (:status) ");

		Map<String, Object> params = new HashMap<>();
		params.put("storeId", storeId);
		params.put("status",
				Arrays.asList(ExportStatementStatus.COMPLETED.getValue(), ExportStatementStatus.CANCELED.getValue()));

		Number count = BaseRepository.getScalarResult(this.em, strQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}

	private void generateFindAndCount(ExportStatementFilterDto exportStatementFilterDto, String currentUsername,
			StringBuilder strSelectQuery, StringBuilder strCountQuery, Map<String, Object> params) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" FROM export_statement es ");
		strQuery.append(" JOIN store_user su ON LOWER(su.username) = LOWER(:username) ");
		strQuery.append("					 AND ( (LOWER(:type) = 'from' AND es.from_store_id = su.store_id) ");
		strQuery.append("					 	    OR (LOWER(:type) = 'to' AND es.to_store_id = su.store_id) ) ");
		strQuery.append(" WHERE (:code IS NULL OR LOWER(es.code) LIKE LOWER(CONCAT('%',:code,'%'))) ");
		strQuery.append("		AND (:status IS NULL OR es.status = :status) ");
		strQuery.append("		AND (:fromStoreId IS NULL OR es.from_store_id = :fromStoreId) ");
		strQuery.append("		AND (:toStoreId IS NULL OR es.to_store_id = :toStoreId) ");
		strQuery.append("		AND (:createDate IS NULL OR ");
		strQuery.append("			 (export_date >= Date(:createDate) ");
		strQuery.append("			  AND export_date < Date(DATE_ADD(:createDate, INTERVAL 1 DAY)) ) ) ");
		if (exportStatementFilterDto.getIsExportSo() != null) {
			if (Boolean.TRUE.equals(exportStatementFilterDto.getIsExportSo())) {
				strQuery.append(" AND EXISTS ( ");
				strQuery.append("		SELECT 1 FROM merchant_order mo ");
				strQuery.append("		WHERE mo.id = es.merchant_order_id ");
				if (exportStatementFilterDto.getMerchantOrderGoodsReceiveForm() != null) {
					strQuery.append("		  AND mo.goods_receive_form LIKE :merchantOrderGoodsReceiveForm ");
					params.put("merchantOrderGoodsReceiveForm",
							exportStatementFilterDto.getMerchantOrderGoodsReceiveForm().getValue());
				}
				strQuery.append("	  )");
			} else {
				strQuery.append(" AND es.merchant_order_id IS NULL ");
			}
		}

		params.put("username", currentUsername);
		params.put("code", exportStatementFilterDto.getCode());
		params.put("status",
				exportStatementFilterDto.getStatus() == null ? null : exportStatementFilterDto.getStatus().getValue());
		params.put("fromStoreId", exportStatementFilterDto.getFromStoreId());
		params.put("toStoreId", exportStatementFilterDto.getToStoreId());
		params.put("createDate", DatetimeUtils.localDateTime2SqlDate(exportStatementFilterDto.getCreateDate()));
		params.put("type", exportStatementFilterDto.getType());

		strSelectQuery.append(" SELECT distinct es.* ").append(strQuery);

		strCountQuery.append(" SELECT COUNT(distinct es.id) ").append(strQuery);
	}

	@Override
	public Page<ExportStatementEntity> find(ExportStatementFilterDto exportStatementFilterDto, String currentUsername,
			Pageable pageable) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strSelectQuery = new StringBuilder();
		StringBuilder strCountQuery = new StringBuilder();
		this.generateFindAndCount(exportStatementFilterDto, currentUsername, strSelectQuery, strCountQuery, params);
		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, ExportStatementEntity.class);
	}

	@Override
	public Long count(ExportStatementFilterDto exportStatementFilterDto, String currentUsername)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		StringBuilder strCountQuery = new StringBuilder();
		this.generateFindAndCount(exportStatementFilterDto, currentUsername, new StringBuilder(), strCountQuery,
				params);
		Number count = BaseRepository.getScalarResult(this.em, strCountQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}

}
