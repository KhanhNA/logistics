package com.tsolution._2repositories.impl;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.CurrencyExchangeRateEntity;
import com.tsolution._2repositories.CurrencyExchangeRateRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class CurrencyExchangeRateRepositoryCustomImpl implements CurrencyExchangeRateRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	private String generateFromQuery(Long fromCurrencyId, Long toCurrencyId, Date fromDate, Date toDate,
			Map<String, Object> params) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" FROM currency_exchange_rate cer ");
		strQuery.append(" WHERE (:fromCurrencyId IS NULL OR cer.from_currency_id = :fromCurrencyId) ");
		strQuery.append("   AND (:toCurrencyId IS NULL OR cer.to_currency_id = :toCurrencyId) ");
		strQuery.append("   AND GREATEST(cer.from_date, :fromDate) <= LEAST( ");
		strQuery.append("			COALESCE(cer.to_date, STR_TO_DATE('31/12/9999','%d/%m/%Y')), ");
		strQuery.append("			COALESCE(:toDate, STR_TO_DATE('31/12/9999','%d/%m/%Y')) ) ");

		params.put("fromCurrencyId", fromCurrencyId);
		params.put("toCurrencyId", toCurrencyId);
		params.put("fromDate", fromDate);
		params.put("toDate", toDate);

		return strQuery.toString();
	}

	@Override
	public Page<CurrencyExchangeRateEntity> findCurrencyExchangeRate(Long fromCurrencyId, Long toCurrencyId,
			Date fromDate, Date toDate, Pageable pageable) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		String fromQuery = this.generateFromQuery(fromCurrencyId, toCurrencyId, fromDate, toDate, params);

		StringBuilder strSelectQuery = new StringBuilder();
		strSelectQuery.append(" SELECT cer.* ");
		strSelectQuery.append(fromQuery);

		StringBuilder strCountQuery = new StringBuilder();
		strCountQuery.append(" SELECT COUNT(cer.id) ");
		strCountQuery.append(fromQuery);

		return BaseRepository.getPagedNativeQuery(this.em, strSelectQuery.toString(), strCountQuery.toString(), params,
				pageable, CurrencyExchangeRateEntity.class);
	}

	@Override
	public Boolean isExistsCurrencyExchangeRate(Long id, Long fromCurrencyId, Long toCurrencyId, Date fromDate,
			Date toDate) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		String fromQuery = this.generateFromQuery(fromCurrencyId, toCurrencyId, fromDate, toDate, params);

		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT cast_to_bit(COUNT(cer.id)) ");
		strQuery.append(fromQuery);
		strQuery.append(" AND (:id IS NULL OR cer.id <> :id) ");

		params.put("id", id);

		return BaseRepository.getScalarResult(this.em, strQuery.toString(), params, Boolean.class);
	}

}
