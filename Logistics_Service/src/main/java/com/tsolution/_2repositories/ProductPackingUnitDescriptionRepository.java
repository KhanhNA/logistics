package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.ProductPackingUnitDescriptionEntity;

public interface ProductPackingUnitDescriptionRepository
		extends JpaRepository<ProductPackingUnitDescriptionEntity, Long>,
		JpaSpecificationExecutor<ProductPackingUnitDescriptionEntity> {
}