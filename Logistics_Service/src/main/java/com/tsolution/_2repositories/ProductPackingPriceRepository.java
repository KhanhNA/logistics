package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.ProductPackingPriceEntity;

public interface ProductPackingPriceRepository extends JpaRepository<ProductPackingPriceEntity, Long>,
		JpaSpecificationExecutor<ProductPackingPriceEntity>, ProductPackingPriceRepositoryCustom {
}