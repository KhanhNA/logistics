package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.PoDetailEntity;

public interface PoDetailRepository extends JpaRepository<PoDetailEntity, Long>, JpaSpecificationExecutor<PoDetailEntity> {

}