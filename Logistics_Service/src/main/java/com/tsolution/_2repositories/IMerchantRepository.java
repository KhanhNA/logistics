package com.tsolution._2repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tsolution._1entities.MerchantEntity;

public interface IMerchantRepository
		extends JpaRepository<MerchantEntity, Long>, JpaSpecificationExecutor<MerchantEntity> {
	@Query(value = "SELECT m.* from merchant m where (:status is null or m.status = :status)", nativeQuery = true)
	List<MerchantEntity> find(@Param("status") Boolean status);

	MerchantEntity findOneByCode(String code);
}