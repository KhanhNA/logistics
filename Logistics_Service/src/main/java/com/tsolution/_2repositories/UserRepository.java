package com.tsolution._2repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.UserEntity;

public interface UserRepository
		extends JpaRepository<UserEntity, Long>, JpaSpecificationExecutor<UserEntity>, UserRepositoryCustom {

	Optional<UserEntity> findOneByUsername(String username);

	@Modifying
	@Query(value = "UPDATE users u SET u.status = true WHERE u.username = :username ", nativeQuery = true)
	void activate(String username);

	@Modifying
	@Query(value = "UPDATE users u SET u.status = false WHERE u.username = :username ", nativeQuery = true)
	void deactive(String username);

	@Query(value = " SELECT cast_to_bit(COUNT(1)) FROM users u WHERE u.username = :username ", nativeQuery = true)
	Boolean existsUserByUsername(String username);

	@Query(value = "SELECT u.* FROM users u JOIN store_user su ON u.username = su.username AND su.store_id = :storeId ", nativeQuery = true)
	List<UserEntity> findByStore(Long storeId);
}
