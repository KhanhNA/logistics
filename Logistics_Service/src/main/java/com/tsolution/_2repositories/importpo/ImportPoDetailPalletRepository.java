package com.tsolution._2repositories.importpo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.importpo.ImportPoDetailPalletEntity;

public interface ImportPoDetailPalletRepository extends JpaRepository<ImportPoDetailPalletEntity, Long>, JpaSpecificationExecutor<ImportPoDetailPalletEntity> {

}