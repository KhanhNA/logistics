package com.tsolution._2repositories.importpo;

import com.tsolution.excetions.BusinessException;

public interface ImportPoRepositoryCustom {
	Long countImportPoHang(Long storeId) throws BusinessException;
}
