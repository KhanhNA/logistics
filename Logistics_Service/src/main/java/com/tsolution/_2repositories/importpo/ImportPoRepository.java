package com.tsolution._2repositories.importpo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.importpo.ImportPoEntity;
import com.tsolution._2repositories.sql.ImportPoSql;

public interface ImportPoRepository extends JpaRepository<ImportPoEntity, Long>,
		JpaSpecificationExecutor<ImportPoEntity>, ImportPoRepositoryCustom {
	@Query(value = "select * " + ImportPoSql.FIND, countQuery = "select count(*) "
			+ ImportPoSql.FIND, nativeQuery = true)
	Page<ImportPoEntity> find(Long storeId, Long distributorId, String text, Integer status, String username,
			Pageable pageable, String acceptLanguage);
}