package com.tsolution._2repositories.importpo.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.tsolution._1entities.enums.ImportPoStatus;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution._2repositories.importpo.ImportPoRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class ImportPoRepositoryCustomImpl implements ImportPoRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long countImportPoHang(Long storeId) throws BusinessException {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT COUNT(DISTINCT ip.id) ");
		strQuery.append(" FROM import_po ip ");
		strQuery.append(" WHERE ip.store_id = :storeId ");
		strQuery.append("   AND ip.status not in (:status) ");

		Map<String, Object> params = new HashMap<>();
		params.put("storeId", storeId);
		params.put("status", Arrays.asList(ImportPoStatus.OUT_OF_PALLET.getValue()));

		Number count = BaseRepository.getScalarResult(this.em, strQuery.toString(), params, Number.class);

		return count == null ? 0 : count.longValue();
	}

}
