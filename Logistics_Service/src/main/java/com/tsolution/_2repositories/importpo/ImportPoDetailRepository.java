package com.tsolution._2repositories.importpo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tsolution._1entities.importpo.ImportPoDetailEntity;

public interface ImportPoDetailRepository
		extends JpaRepository<ImportPoDetailEntity, Long>, JpaSpecificationExecutor<ImportPoDetailEntity> {

}