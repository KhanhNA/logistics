package com.tsolution._2repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.export_statement.ExportStatementEntity;
import com.tsolution._1entities.export_statement.dto.ExportStatementFilterDto;
import com.tsolution.excetions.BusinessException;

public interface ExportStatementRepositoryCustom {
	Long countExportStatementHang(Long storeId) throws BusinessException;

	Page<ExportStatementEntity> find(ExportStatementFilterDto exportStatementFilterDto, String currentUsername,
			Pageable pageable) throws BusinessException;

	Long count(ExportStatementFilterDto exportStatementFilterDto, String currentUsername) throws BusinessException;
}
