package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.ProductImageEntity;

public interface ProductImageEntityRepository extends JpaRepository<ProductImageEntity, Long>, JpaSpecificationExecutor<ProductImageEntity> {

}