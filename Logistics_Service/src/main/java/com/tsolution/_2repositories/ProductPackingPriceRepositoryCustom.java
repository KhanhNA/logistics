package com.tsolution._2repositories;

import java.util.List;

import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.enums.SaleType;
import com.tsolution.excetions.BusinessException;

public interface ProductPackingPriceRepositoryCustom {
	List<ProductPackingPriceEntity> getCurrentPrices(Long productPackingId, SaleType saleType) throws BusinessException;

	List<ProductPackingPriceEntity> getLastestPrices(Long productPackingId, SaleType saleType) throws BusinessException;
}
