package com.tsolution._4controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.store.StoreDeliveryWaitEntity;
import com.tsolution._3services.StoreDeliveryWaitService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/store-delivery-waits")
public class StoreDeliveryWaitController implements IBaseController<StoreDeliveryWaitEntity> {

	@Autowired
	private StoreDeliveryWaitService storeDeliveryWaitService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/store-delivery-waits/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.storeDeliveryWaitService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/store-delivery-waits/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.storeDeliveryWaitService.findById(id);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/store-delivery-waits')")
	public ResponseEntity<Object> create(@RequestBody List<StoreDeliveryWaitEntity> entity) throws BusinessException {
		return this.storeDeliveryWaitService.create(entity);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/store-delivery-waits/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id,
			@RequestBody Optional<StoreDeliveryWaitEntity> source) throws BusinessException {
		if (source.isPresent()) {
			return this.storeDeliveryWaitService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@GetMapping("/from-store/{id}")
	@PreAuthorize("hasAuthority('get/store-delivery-waits/from-store/{id}')")
	public ResponseEntity<Object> findByFromStore(@PathVariable("id") Long fromStoreId) throws BusinessException {
		return this.storeDeliveryWaitService.getAllByFromStore(fromStoreId);
	}

	@PostMapping("/export-from-store/{id}")
	@PreAuthorize("hasAuthority('post/store-delivery-waits/export-from-store/{id}')")
	public ResponseEntity<Object> exportFromStore(@PathVariable("id") Long fromStoreId) throws BusinessException {
		return this.storeDeliveryWaitService.exportFromStore(fromStoreId);
	}

	@PostMapping(path = "/import-from-store/{id}", consumes = { "multipart/form-data" })
	@PreAuthorize("hasAuthority('post/store-delivery-waits') OR hasAuthority('patch/store-delivery-waits/{id}')")
	public ResponseEntity<Object> importFromStore(@PathVariable("id") Long fromStoreId,
			@RequestPart(name = "file", required = true) MultipartFile file) throws BusinessException {
		return this.storeDeliveryWaitService.importFromStore(fromStoreId, file);
	}
}
