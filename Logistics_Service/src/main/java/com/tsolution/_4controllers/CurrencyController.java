package com.tsolution._4controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._3services.ICurrencyService;
import com.tsolution.excetions.BusinessException;

@RequestMapping("/currencies")
@RestController
public class CurrencyController implements IBaseController<CurrencyEntity> {

	@Autowired
	private ICurrencyService iCurrencyService;

	@GetMapping("/all")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.iCurrencyService.findAll();
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		return null;
	}

	@Override
	public ResponseEntity<Object> create(List<CurrencyEntity> entity) throws BusinessException {
		return null;
	}

	@Override
	public ResponseEntity<Object> update(Long id, Optional<CurrencyEntity> source) throws BusinessException {
		return null;
	}
}
