package com.tsolution._4controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._3services.TaskListService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/task-list")
public class TaskListController {

	@Autowired
	private TaskListService taskListService;

	@GetMapping("/po")
	@PreAuthorize("hasAuthority('patch/pos/{id}/arrive-vietnam-port') OR hasAuthority('patch/pos/{id}/arrive-myanmar-port') OR hasAuthority('patch/pos/{id}/arrive-myanmar-port')")
	public ResponseEntity<Object> getTaskListPo() throws BusinessException {
		return this.taskListService.getTaskListPo();
	}

	@GetMapping("/store/{username}/app-dashboard")
	@PreAuthorize("hasAuthority('**/**')")
	public ResponseEntity<Object> getStoreAppDashboard(@PathVariable(name = "username") String username,
			@RequestHeader("Accept-Language") String acceptLanguage) throws BusinessException {
		return this.taskListService.getStoreAppDashboard(username, acceptLanguage);
	}
}
