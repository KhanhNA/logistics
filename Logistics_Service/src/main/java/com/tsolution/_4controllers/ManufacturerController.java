package com.tsolution._4controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution._3services.IManufacturerService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/manufacturers")
public class ManufacturerController implements IBaseController<ManufacturerEntity> {

	@Autowired
	private IManufacturerService manufacturerService;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/manufacturers/all')")
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/manufacturers/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/manufacturers')")
	public ResponseEntity<Object> find(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "status", required = true) Boolean status,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		return this.manufacturerService.find(text, status, pageNumber, pageSize, acceptLanguage);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/manufacturers')")
	public ResponseEntity<Object> create(List<ManufacturerEntity> entity) throws BusinessException {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/manufacturers/{id}')")
	public ResponseEntity<Object> update(Long id, Optional<ManufacturerEntity> source) throws BusinessException {
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
