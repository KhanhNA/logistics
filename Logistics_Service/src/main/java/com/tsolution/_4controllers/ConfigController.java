package com.tsolution._4controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._3services.ConfigService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/configs")
public class ConfigController {

	@Autowired
	private ConfigService configService;

	@GetMapping
	public ResponseEntity<Object> find(@RequestParam(required = false) String code) throws BusinessException {
		return this.configService.find(code);
	}

}
