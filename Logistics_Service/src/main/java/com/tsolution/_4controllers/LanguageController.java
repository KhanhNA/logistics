package com.tsolution._4controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._3services.ILanguageService;

@RestController
@RequestMapping("/languages")
public class LanguageController {

	@Autowired
	private ILanguageService languageService;

	@GetMapping
	public ResponseEntity<Object> getAll() {
		return this.languageService.getAll();
	}
}
