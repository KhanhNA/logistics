package com.tsolution._4controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.RepackingPlanningDetailRepackedEntity;
import com.tsolution._1entities.RepackingPlanningEntity;
import com.tsolution._1entities.dto.RepackingPlanningDto;
import com.tsolution._3services.RepackingPlanningService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/repacking-plannings")
public class RepackingPlanningController implements IBaseController<RepackingPlanningEntity> {
	@Autowired
	private RepackingPlanningService repackingPlanningService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/repacking-plannings/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.repackingPlanningService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/repacking-plannings/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.repackingPlanningService.findById(id);
	}

	@GetMapping("/detail-repacked/{id}")
	@PreAuthorize("hasAuthority('get/repacking-plannings/detail-repacked/{id}')")
	public ResponseEntity<Object> findDetailRepackedById(@PathVariable("id") Long id) throws BusinessException {
		return this.repackingPlanningService.findDetailRepackedById(id);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/repacking-plannings')")
	public ResponseEntity<Object> find(@RequestParam(value = "storeId", required = true) Long storeId,
			@RequestParam(value = "distributorId", required = false) Long distributorId,
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "dateType", required = true) Integer dateType,
			@RequestParam(value = "fromDate", required = true) String fromDate,
			@RequestParam(value = "toDate", required = true) String toDate,
			@RequestParam(value = "status", required = false) Integer status,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize)
			throws ParseException, BusinessException {

		RepackingPlanningDto repackingPlanningDto = new RepackingPlanningDto();
		repackingPlanningDto.setStoreId(storeId);
		repackingPlanningDto.setDistributorId(distributorId);
		repackingPlanningDto.setText(text);
		repackingPlanningDto.setDateType(dateType);
		repackingPlanningDto.setFromDate(new SimpleDateFormat(Constants.DATE_PATTERN).parse(fromDate));
		repackingPlanningDto.setToDate(new SimpleDateFormat(Constants.DATE_PATTERN).parse(toDate));
		repackingPlanningDto.setStatus(status);

		return this.repackingPlanningService.find(repackingPlanningDto, pageNumber, pageSize);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/repacking-plannings')")
	public ResponseEntity<Object> create(@RequestBody List<RepackingPlanningEntity> entity) throws BusinessException {
		return this.repackingPlanningService.create(entity);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/repacking-plannings/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id,
			@RequestBody Optional<RepackingPlanningEntity> source) throws BusinessException {
		if (source.isPresent()) {
			return this.repackingPlanningService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PostMapping("/repack/{id}")
	@PreAuthorize("hasAuthority('post/repacking-plannings/repack/{id}')")
	public ResponseEntity<Object> repack(@PathVariable("id") Long id,
			@RequestBody List<RepackingPlanningDetailRepackedEntity> repackingPlanningDetailRepackeds)
			throws BusinessException {
		return this.repackingPlanningService.repack(id, repackingPlanningDetailRepackeds);
	}

	@PostMapping("/confirm-repack/{id}")
	@PreAuthorize("hasAuthority('post/repacking-plannings/confirm-repack/{id}')")
	public ResponseEntity<Object> confirmRepack(@PathVariable("id") Long id) throws BusinessException {
		return this.repackingPlanningService.confirmRepack(id);
	}

	@PostMapping("/cancel-repack/{id}")
	@PreAuthorize("hasAuthority('post/repacking-plannings/cancel-repack/{id}')")
	public ResponseEntity<Object> cancelRepack(@PathVariable("id") Long id) throws BusinessException {
		return this.repackingPlanningService.cancelRepack(id);
	}

	@PostMapping("/cancel-plan/{id}")
	@PreAuthorize("hasAuthority('post/repacking-plannings/cancel-plan/{id}')")
	public ResponseEntity<Object> cancelPlan(@PathVariable("id") Long id) throws BusinessException {
		return this.repackingPlanningService.cancelPlan(id);
	}

	@PostMapping("/print/{id}")
	@PreAuthorize("hasAuthority('post/repacking-plannings/print/{id}')")
	public ResponseEntity<Object> print(@PathVariable("id") Long id, HttpServletResponse response)
			throws BusinessException {
		return this.repackingPlanningService.print(id);
	}

	@PostMapping("/print-qrcode/{id}")
	@PreAuthorize("hasAuthority('post/repacking-plannings/print-qrcode/{id}')")
	public ResponseEntity<Object> printQrCode(@PathVariable("id") Long id) throws BusinessException {
		return this.repackingPlanningService.printQrCode(id);
	}
}
