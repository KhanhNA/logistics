package com.tsolution._4controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.shipping_partner.ShippingPartnerEntity;
import com.tsolution._1entities.shipping_partner.enums.ShippingPartnerStatus;
import com.tsolution._3services.shipping_partner.ShippingPartnerService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/shipping-partners")
public class ShippingPartnerController extends BaseController implements IBaseController<ShippingPartnerEntity> {

	@Autowired
	private ShippingPartnerService shippingPartnerService;

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/shipping-partners/{id}')")
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		return this.shippingPartnerService.findById(id);
	}

	@Override
	public ResponseEntity<Object> create(List<ShippingPartnerEntity> entity) throws BusinessException {
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_NOT_SUPPORT));
	}

	@PostMapping(consumes = { "multipart/form-data" })
	@PreAuthorize("hasAuthority('post/shipping-partners')")
	public ResponseEntity<Object> create(
			@RequestPart(name = "source", required = true) Optional<ShippingPartnerEntity> source,
			@RequestPart(name = "files", required = false) MultipartFile[] files) throws BusinessException {
		if (source.isPresent()) {
			return this.shippingPartnerService.create(source.get(), files);
		}
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
	}

	@Override
	public ResponseEntity<Object> update(Long id, Optional<ShippingPartnerEntity> source) throws BusinessException {
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_NOT_SUPPORT));
	}

	@PatchMapping(path = "/{id}", consumes = { "multipart/form-data" })
	@PreAuthorize("hasAuthority('patch/shipping-partners/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id,
			@RequestPart(name = "source", required = true) Optional<ShippingPartnerEntity> source,
			@RequestPart(name = "files", required = false) MultipartFile[] files,
			@RequestPart(name = "removeFileIds", required = false) List<Long> removeFileIds) throws BusinessException {
		if (source.isPresent()) {
			return this.shippingPartnerService.update(id, source.get(), files, removeFileIds);
		}
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
	}

	@PatchMapping("/{id}/accept")
	@PreAuthorize("hasAuthority('patch/shipping-partners/{id}/accept')")
	public ResponseEntity<Object> accept(@PathVariable("id") Long id) throws BusinessException {
		return this.shippingPartnerService.accept(id);
	}

	@PatchMapping("/{id}/reject")
	@PreAuthorize("hasAuthority('patch/shipping-partners/{id}/reject')")
	public ResponseEntity<Object> reject(@PathVariable("id") Long id,
			@RequestBody(required = true) Optional<ShippingPartnerEntity> oShippingPartner) throws BusinessException {
		if (oShippingPartner.isPresent()) {
			return this.shippingPartnerService.reject(id, oShippingPartner.get());
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/shipping-partners')")
	public ResponseEntity<Object> find(@RequestParam(required = false) String name,
			@RequestParam(required = false) String phone, @RequestParam(required = false) String email,
			@RequestParam(required = false) String address, @RequestParam(required = false) String contractCode,
			@RequestParam(required = false) String taxCode,
			@RequestParam(required = false) ShippingPartnerStatus status,
			@RequestParam(required = true) Integer pageNumber, @RequestParam(required = true) Integer pageSize)
			throws BusinessException {
		ShippingPartnerEntity shippingPartner = new ShippingPartnerEntity();
		shippingPartner.setName(name);
		shippingPartner.setPhone(phone);
		shippingPartner.setEmail(email);
		shippingPartner.setAddress(address);
		shippingPartner.setContractCode(contractCode);
		shippingPartner.setTaxCode(taxCode);
		shippingPartner.setStatus(status);
		return this.shippingPartnerService.find(shippingPartner, pageNumber, pageSize);
	}

}
