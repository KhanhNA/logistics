package com.tsolution._4controllers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.dto.ImportStatementDto;
import com.tsolution._1entities.enums.ExportStatementStatus;
import com.tsolution._1entities.export_statement.dto.ExportStatementDto;
import com.tsolution._1entities.export_statement.dto.ExportStatementFilterDto;
import com.tsolution._1entities.merchant_order.enums.MerchantOrderGoodsReceiveForm;
import com.tsolution._3services.ExportStatementService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/export-statements")
public class ExportStatementController {
	@Autowired
	private ExportStatementService exportStatementService;

	@Autowired
	private Translator translator;

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/export-statements/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.exportStatementService.findById(id);
	}

	@GetMapping("/get-by-code")
	@PreAuthorize("hasAuthority('get/export-statements/get-by-code')")
	public ResponseEntity<Object> getByCode(@RequestParam(name = "code", required = true) String code)
			throws BusinessException {
		return this.exportStatementService.findOneByCode(code);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/export-statements')")
	public ResponseEntity<Object> find(@RequestParam(value = "code", required = false) String code,
			@RequestParam(value = "fromStoreId", required = false) Long fromStoreId,
			@RequestParam(value = "toStoreId", required = false) Long toStoreId,
			@RequestParam(value = "createDate", required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime createDate,
			@RequestParam(value = "status", required = true) Integer status,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "isExportSo", required = false) Boolean isExportSo,
			@RequestParam(value = "merchantOrderGoodsReceiveForm", required = false) MerchantOrderGoodsReceiveForm merchantOrderGoodsReceiveForm,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		ExportStatementFilterDto exportStatementFilterDto = new ExportStatementFilterDto();
		exportStatementFilterDto.setCode(code);
		exportStatementFilterDto.setFromStoreId(fromStoreId);
		exportStatementFilterDto.setToStoreId(toStoreId);
		exportStatementFilterDto.setCreateDate(createDate);
		exportStatementFilterDto.setStatus(ExportStatementStatus.findByValue(status));
		exportStatementFilterDto.setType(type);
		exportStatementFilterDto.setIsExportSo(isExportSo);
		exportStatementFilterDto.setMerchantOrderGoodsReceiveForm(merchantOrderGoodsReceiveForm);
		return this.exportStatementService.find(exportStatementFilterDto, pageNumber, pageSize);
	}

	@PostMapping
	@PreAuthorize("hasAuthority('post/export-statements')")
	public ResponseEntity<Object> create(@RequestBody(required = true) List<ExportStatementDto> exportStatementDtos)
			throws BusinessException {
		return this.exportStatementService.create(exportStatementDtos);
	}

	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/export-statements/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<ExportStatementDto> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.exportStatementService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PostMapping("/from-merchant-order")
	@PreAuthorize("hasAuthority('post/export-statements/from-merchant-order')")
	public ResponseEntity<Object> createFromMerchantOrder(@RequestBody ExportStatementDto exportStatementDto)
			throws BusinessException {
		return this.exportStatementService.createFromMerchantOrder(exportStatementDto);
	}

	@PostMapping("/by-store-demand")
	@PreAuthorize("hasAuthority('post/export-statements/by-store-demand')")
	public ResponseEntity<Object> createByStoreDemand(@RequestBody ExportStatementDto exportStatementDto)
			throws BusinessException {
		return this.exportStatementService.createByStoreDemand(exportStatementDto);
	}

	@PatchMapping("/{id}/export")
	@PreAuthorize("hasAuthority('patch/export-statements/{id}/export')")
	public ResponseEntity<Object> actuallyExport(@PathVariable("id") Long id,
			@RequestBody Optional<ImportStatementDto> source) throws BusinessException {
		if (source.isPresent()) {
			return this.exportStatementService.actuallyExport(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PostMapping("/print/{id}")
	@PreAuthorize("hasAuthority('post/export-statements/print/{id}')")
	public ResponseEntity<Object> print(@PathVariable("id") Long id, HttpServletResponse response)
			throws BusinessException {
		return this.exportStatementService.print(id);
	}

	@PostMapping("/print-pdf/{id}")
	@PreAuthorize("hasAuthority('post/export-statements/print-pdf/{id}')")
	public ResponseEntity<Object> printPdf(@PathVariable("id") Long id, HttpServletResponse response)
			throws BusinessException {
		return this.exportStatementService.printPdf(id);
	}

	@PostMapping("/cancel/{id}")
	@PreAuthorize("hasAuthority('post/export-statements/cancel/{id}')")
	public ResponseEntity<Object> cancel(@PathVariable("id") Long id) throws BusinessException {
		return this.exportStatementService.cancel(id);
	}

	@PostMapping("/cancel-detail/{id}")
	@PreAuthorize("hasAuthority('post/export-statements/cancel-detail/{id}')")
	public ResponseEntity<Object> cancelDetail(@PathVariable("id") Long id,
			@RequestBody(required = true) List<Long> detailIds) throws BusinessException {
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_NOT_SUPPORT));
	}
}
