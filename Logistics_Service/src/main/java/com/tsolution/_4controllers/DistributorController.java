package com.tsolution._4controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.DistributorEntity;
import com.tsolution._3services.DistributorService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/distributors")
public class DistributorController implements IBaseController<DistributorEntity> {

	@Autowired
	private DistributorService distributorService;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/distributors/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.distributorService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/distributors/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.distributorService.findById(id);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/distributors')")
	public ResponseEntity<Object> create(@RequestBody List<DistributorEntity> entity) throws BusinessException {
		return null;
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('pathch/distributors/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<DistributorEntity> source)
			throws BusinessException {
		return null;
	}
}
