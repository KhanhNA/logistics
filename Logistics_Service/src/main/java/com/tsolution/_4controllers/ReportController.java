package com.tsolution._4controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution._1entities.reportdto.InventoryAndSellOutReportFilterDto;
import com.tsolution._3services.ReportsService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;

@RestController
@RequestMapping("/report")
public class ReportController {

	@Autowired
	private ReportsService reportsService;

	@GetMapping("/inventory-and-sell-out")
	@PreAuthorize("hasAuthority('get/report/inventory-and-sell-out')")
	public ResponseEntity<Object> getInventoryAndSellOutReportDto(
			@RequestParam(value = "storeIds", required = false) List<Long> storeIds,
			@RequestParam(value = "fromDate", required = true) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fromDate,
			@RequestParam(value = "toDate", required = true) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime toDate,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto = new InventoryAndSellOutReportFilterDto();
		inventoryAndSellOutReportFilterDto.setStoreIds(storeIds);
		inventoryAndSellOutReportFilterDto.setFromDate(fromDate);
		inventoryAndSellOutReportFilterDto.setToDate(toDate);
		return this.reportsService.getInventoryAndSellOutReportDto(inventoryAndSellOutReportFilterDto, pageNumber,
				pageSize);
	}

	@PostMapping("/inventory-and-sell-out/export-excel")
	@PreAuthorize("hasAuthority('post/report/inventory-and-sell-out/export-excel')")
	public ResponseEntity<Object> exportExcelInventoryAndSellOutReportDto(
			@RequestBody(required = true) InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto)
			throws BusinessException {
		return this.reportsService.exportExcelInventoryAndSellOutReportDto(inventoryAndSellOutReportFilterDto);
	}

	@PostMapping("/inventory-and-sel-out/export-pdf")
	@PreAuthorize("hasAuthority('post/report/inventory-and-sel-out/export-pdf')")
	public ResponseEntity<Object> exportPdfInventoryAndSellOutReportDto(
			@RequestBody(required = true) InventoryAndSellOutReportFilterDto inventoryAndSellOutReportFilterDto)
			throws BusinessException {
		return this.reportsService.exportPdfInventoryAndSellOutReportDto(inventoryAndSellOutReportFilterDto);
	}

	@PostMapping("/inventory")
	@PreAuthorize("hasAuthority('post/report/inventory')")
	public ResponseEntity<Object> getInventoryDto(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestBody(required = true) InventoryDto inventoryDto,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		inventoryDto.setAcceptLanguage(acceptLanguage);
		return this.reportsService.getInventoryDto(inventoryDto, pageNumber, pageSize);
	}

	@PostMapping("/inventory/export-excel")
	@PreAuthorize("hasAuthority('post/report/inventory/export-excel')")
	public ResponseEntity<Object> exportExcelInventoryDto(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestBody(required = true) InventoryDto inventoryDto) throws BusinessException {
		inventoryDto.setAcceptLanguage(acceptLanguage);
		return this.reportsService.exportExcelInventoryDto(inventoryDto);
	}

	@PostMapping("/inventory/export-pdf")
	@PreAuthorize("hasAuthority('post/report/inventory/export-pdf')")
	public ResponseEntity<Object> exportPdfInventoryDto(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestBody(required = true) InventoryDto inventoryDto) throws BusinessException {
		inventoryDto.setAcceptLanguage(acceptLanguage);
		return this.reportsService.exportPdfInventoryDto(inventoryDto);
	}

	@PostMapping("/pallet-inventory/export-excel")
	@PreAuthorize("hasAuthority('post/report/pallet-inventory/export-excel')")
	public ResponseEntity<Object> exportExcelPalletInventoryDto(
			@RequestBody(required = true) PalletDetailDto palletDetailDto) throws BusinessException {
		return this.reportsService.exportExcelPalletInventoryDto(palletDetailDto);
	}

	@PostMapping("/pallet-inventory/export-pdf")
	@PreAuthorize("hasAuthority('post/report/pallet-inventory/export-pdf')")
	public ResponseEntity<Object> exportPdfPalletInventoryDto(
			@RequestBody(required = true) PalletDetailDto palletDetailDto) throws BusinessException {
		return this.reportsService.exportPdfPalletInventoryDto(palletDetailDto);
	}

}
