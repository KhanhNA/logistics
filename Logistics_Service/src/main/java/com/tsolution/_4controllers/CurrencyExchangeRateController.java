package com.tsolution._4controllers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.CurrencyExchangeRateEntity;
import com.tsolution._3services.CurrencyExchangeRateService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/currency-exchange-rate")
public class CurrencyExchangeRateController implements IBaseController<CurrencyExchangeRateEntity> {

	@Autowired
	private CurrencyExchangeRateService currencyExchangeRateService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/currency-exchange-rate/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.currencyExchangeRateService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/currency-exchange-rate/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.currencyExchangeRateService.findById(id);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/currency-exchange-rate')")
	public ResponseEntity<Object> create(@RequestBody List<CurrencyExchangeRateEntity> entities)
			throws BusinessException {
		return this.currencyExchangeRateService.create(entities);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/currency-exchange-rate/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id,
			@RequestBody Optional<CurrencyExchangeRateEntity> source) throws BusinessException {
		if (source.isPresent()) {
			return this.currencyExchangeRateService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/currency-exchange-rate')")
	public ResponseEntity<Object> findCurrencyExchangeRate(
			@RequestParam(value = "fromCurrencyId", required = false) Long fromCurrencyId,
			@RequestParam(value = "toCurrencyId", required = false) Long toCurrencyId,
			@RequestParam(value = "fromDate", required = true) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime toDate,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		return this.currencyExchangeRateService.findCurrencyExchangeRate(fromCurrencyId, toCurrencyId, fromDate, toDate,
				pageNumber, pageSize);
	}

}
