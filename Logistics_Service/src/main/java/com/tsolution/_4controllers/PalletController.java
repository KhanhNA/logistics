package com.tsolution._4controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.PalletEntity;
import com.tsolution._1entities.dto.PalletDetailDto;
import com.tsolution._1entities.enums.ExpireDateColors;
import com.tsolution._3services.PalletService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.StringUtils;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/pallets")
public class PalletController implements IBaseController<PalletEntity> {

	private static Logger log = LogManager.getLogger(PalletController.class);

	@Autowired
	private PalletService palletService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/pallets/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.palletService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/pallets/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.palletService.findById(id);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/pallets')")
	public ResponseEntity<Object> find(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestParam(value = "storeId", required = true) Long storeId,
			@RequestParam(value = "distributorId", required = false) Long distributorId,
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "status", required = true) Boolean status,
			@RequestParam(value = "step", required = true) Integer step,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		PalletDetailDto palletDetailDto = new PalletDetailDto();
		palletDetailDto.setStoreId(storeId);
		palletDetailDto.setDistributorId(distributorId);
		palletDetailDto.setText(text);
		palletDetailDto.setStatus(status);
		palletDetailDto.setStep(step);
		return this.palletService.find(palletDetailDto, acceptLanguage, pageNumber, pageSize);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/pallets')")
	public ResponseEntity<Object> create(@RequestBody List<PalletEntity> entity) throws BusinessException {
		return this.palletService.create(entity);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/pallets/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<PalletEntity> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.palletService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@GetMapping("/product-packing-group-by-expire-date")
	@PreAuthorize("hasAuthority('get/pallets/product-packing-group-by-expire-date')")
	public ResponseEntity<Object> findProductPackingGroupByExpireDate(
			@RequestParam(value = "storeId", required = true) Long storeId,
			@RequestParam(value = "distributorId", required = false) Long distributorId,
			@RequestParam(value = "text", required = false) String text, @RequestParam(required = false) Long palletId,
			@RequestParam(required = false) Boolean isIncludeProductPackingIdExpireDate,
			@RequestParam(required = true) MultiValueMap<String, String> productPackingIdExpireDate,
			@RequestParam(required = true) List<Integer> palletSteps,
			@RequestParam(required = false) Long repackingPlanningId,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		PalletDetailDto palletDetailDto = new PalletDetailDto();
		palletDetailDto.setStoreId(storeId);
		palletDetailDto.setDistributorId(distributorId);
		palletDetailDto.setText(text);
		palletDetailDto.setPalletId(palletId);
		palletDetailDto.setPalletSteps(palletSteps);
		palletDetailDto.setIsIncludeProductPackingIdExpireDate(isIncludeProductPackingIdExpireDate);
		this.convertMultuValueMap(palletDetailDto, productPackingIdExpireDate);
		return this.palletService.findProductPackingGroupByExpireDate(palletDetailDto, productPackingIdExpireDate,
				repackingPlanningId, pageNumber, pageSize);
	}

	@GetMapping("/group-by-pallet-product-packing-expire-date")
	@PreAuthorize("hasAuthority('get/pallets/group-by-pallet-product-packing-expire-date')")
	public ResponseEntity<Object> findProductPackingGroupByExpireDate(
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(required = false) ExpireDateColors expireDateColor,
			@RequestParam(required = false) String productPackingCode,
			@RequestParam(required = false) String productPackingName,
			@RequestParam(value = "palletId", required = false) Long palletId,
			@RequestParam(value = "distributorId", required = false) Long distributorId,
			@RequestParam(value = "storeId", required = true) Long storeId,
			@RequestParam(value = "palletSteps", required = true) List<Integer> palletSteps,
			@RequestParam(required = true) MultiValueMap<String, String> exceptProductPackingIdExpireDate,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		PalletDetailDto palletDetailDto = new PalletDetailDto();
		palletDetailDto.setText(text);
		palletDetailDto.setProductPackingCode(productPackingCode);
		palletDetailDto.setProductPackingName(productPackingName);
		palletDetailDto.setPalletId(palletId);
		palletDetailDto.setDistributorId(distributorId);
		palletDetailDto.setStoreId(storeId);
		palletDetailDto.setPalletSteps(palletSteps);
		this.convertMultuValueMap(palletDetailDto, exceptProductPackingIdExpireDate);
		palletDetailDto.setExpireDateColor(expireDateColor);
		return this.palletService.findGroupByPalletStepAndProductPackingAndExpireDate(palletDetailDto, pageNumber,
				pageSize);
	}

	public void convertMultuValueMap(PalletDetailDto palletDetailDto,
			MultiValueMap<String, String> productPackingIdExpireDate) {
		List<Long> productPackingIds = new LinkedList<>();
		List<String> productPackingExpireDates = new LinkedList<>();
		try {
			for (Map.Entry<String, List<String>> entry : productPackingIdExpireDate.entrySet()) {
				String key = entry.getKey();
				if (!StringUtils.isNumber(key)) {
					continue;
				}
				for (String value : productPackingIdExpireDate.get(key)) {
					productPackingIds.add(Long.parseLong(key));
					Date tempDate = StringUtils.isNullOrEmpty(value) ? new java.util.Date()
							: new SimpleDateFormat(Constants.DATE_PATTERN).parse(value);
					productPackingExpireDates.add(new SimpleDateFormat(Constants.DATE_QUERY).format(tempDate));
				}
			}
			palletDetailDto.setProductPackingIds(productPackingIds);
			palletDetailDto.setProductPackingExpireDates(productPackingExpireDates);
		} catch (Exception e) {
			PalletController.log.error(e.getMessage(), e);
		}
	}
}
