package com.tsolution._4controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._3services.IMerchantService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/merchants")
public class MerchantController {
	@Autowired
	private IMerchantService merchantService;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/merchants/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.merchantService.findAll();
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/merchants')")
	public ResponseEntity<Object> find(@RequestParam(value = "status", required = false) Boolean status) {
		return this.merchantService.find(status);
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/merchants/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.merchantService.findById(id);
	}
}
