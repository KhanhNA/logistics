package com.tsolution._4controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.dto.AvailableQuantityFilterDto;
import com.tsolution._1entities.dto.InventoryDto;
import com.tsolution._1entities.enums.ExpireDateColors;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._1entities.store.dto.StoreDto;
import com.tsolution._3services.StoreService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/stores")
public class StoreController implements IBaseController<StoreEntity> {

	@Autowired
	private StoreService storeService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/stores/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.storeService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/stores/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.storeService.findById(id);
	}

	@GetMapping("/near-here")
	@PreAuthorize("hasAuthority('get/stores/near-here') or hasAuthority('/logistics-infomation')")
	public ResponseEntity<Object> nearHere(@RequestParam(required = true) Float currentLat,
			@RequestParam(required = true) Float currentLng, @RequestParam(required = true) Integer maxDistance,
			@RequestParam(required = false) String productPackingCode, @RequestParam(required = false) Boolean isDC,
			@RequestParam(required = true) Integer pageNumber, @RequestParam(required = true) Integer pageSize)
			throws BusinessException {
		return this.storeService.nearHere(currentLat, currentLng, maxDistance, productPackingCode, isDC, pageNumber,
				pageSize);
	}

	@GetMapping("/inventory")
	@PreAuthorize("hasAuthority('get/stores/inventory')")
	public ResponseEntity<Object> inventory(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestParam(required = true) Long storeId, @RequestParam(required = false) Long distributorId,
			@RequestParam(required = false) Long manufacturerId,
			@RequestParam(required = true) List<Long> exceptProductPackingIds,
			@RequestParam(required = false) String productPackingCode,
			@RequestParam(required = false) String productPackingName,
			@RequestParam(required = false) ExpireDateColors expireDateColor,
			@RequestParam(required = true) Integer pageNumber, @RequestParam(required = true) Integer pageSize)
			throws BusinessException {
		InventoryDto inventoryDto = new InventoryDto();
		inventoryDto.setAcceptLanguage(acceptLanguage);
		inventoryDto.setStoreId(storeId);
		inventoryDto.setDistributorId(distributorId);
		inventoryDto.setManufacturerId(manufacturerId);
		inventoryDto.setExceptProductPackingIds(exceptProductPackingIds);
		inventoryDto.setProductPackingCode(productPackingCode);
		inventoryDto.setProductPackingName(productPackingName);
		inventoryDto.setExpireDateColor(expireDateColor);
		return this.storeService.inventory(inventoryDto, pageNumber, pageSize);
	}

	@GetMapping("/inventory-ignore-expire-date")
	@PreAuthorize("hasAuthority('get/stores/inventory-ignore-expire-date')")
	public ResponseEntity<Object> inventoryIgnoreExpireDate(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestParam(required = true) Long storeId,
			@RequestParam(required = true) List<Long> exceptProductPackingIds,
			@RequestParam(required = false) String code, @RequestParam(required = false) String name,
			@RequestParam(required = false) Long exportStatementId, @RequestParam(required = true) Integer pageNumber,
			@RequestParam(required = true) Integer pageSize) throws BusinessException {
		InventoryDto inventoryDto = new InventoryDto();
		inventoryDto.setAcceptLanguage(acceptLanguage);
		inventoryDto.setStoreId(storeId);
		inventoryDto.setDistributorId(null);
		inventoryDto.setExceptProductPackingIds(exceptProductPackingIds);
		inventoryDto.setProductPackingCode(code);
		inventoryDto.setProductPackingName(name);
		return this.storeService.inventoryIgnoreExpireDate(inventoryDto, exportStatementId, pageNumber, pageSize);
	}

	@Override
	@PostMapping("/old")
	@PreAuthorize("hasAuthority('post/stores')")
	public ResponseEntity<Object> create(@RequestBody List<StoreEntity> entity) throws BusinessException {
		return this.storeService.create(entity);
	}

	@PostMapping(consumes = { "multipart/form-data" })
	@PreAuthorize("hasAuthority('post/stores')")
	public ResponseEntity<Object> create(@RequestPart(name = "source", required = true) Optional<StoreEntity> source,
			@RequestPart(name = "files", required = false) MultipartFile[] files) throws BusinessException {
		if (source.isPresent()) {
			return this.storeService.create(source.get(), files);
		}
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
	}

	@Override
	@PatchMapping("/{id}/old")
	@PreAuthorize("hasAuthority('patch/stores/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<StoreEntity> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.storeService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PatchMapping(path = "/{id}", consumes = { "multipart/form-data" })
	@PreAuthorize("hasAuthority('patch/stores/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id,
			@RequestPart(name = "source", required = true) Optional<StoreEntity> source,
			@RequestPart(name = "files", required = false) MultipartFile[] files,
			@RequestPart(name = "removeFileIds", required = false) List<Long> removeFileIds) throws BusinessException {
		if (source.isPresent()) {
			return this.storeService.update(id, source.get(), files, removeFileIds);
		}
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/stores') or hasAuthority('/logistics-infomation') or hasAuthority('get/promotions/{id}')")
	public ResponseEntity<Object> find(@RequestParam(value = "status", required = true) Boolean status,
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "country_id", required = false) Long countryId,
			@RequestParam(value = "region_id", required = false) Long regionId,
			@RequestParam(value = "district_id", required = false) Long districtId,
			@RequestParam(value = "township_id", required = false) Long townshipId,
			@RequestParam(value = "is_DC", required = false) Boolean isDc,
			@RequestParam(value = "exceptIds", required = true) List<Long> exceptIds,
			@RequestParam(value = "ignoreCheckPermission", required = true) Boolean ignoreCheckPermission,
			@RequestParam(required = true) Integer pageNumber, @RequestParam(required = true) Integer pageSize)
			throws IOException, BusinessException {
		StoreDto storeDto = new StoreDto();
		storeDto.setStatus(status);
		storeDto.setText(text);
		storeDto.setCountryId(countryId);
		storeDto.setRegionId(regionId);
		storeDto.setDistrictId(districtId);
		storeDto.setTownshipId(townshipId);
		storeDto.setIsDc(isDc);
		storeDto.setExceptIds(exceptIds);
		return this.storeService.find(storeDto, ignoreCheckPermission, pageNumber, pageSize);
	}

	@GetMapping("/by-provide-store")
	@PreAuthorize("hasAuthority('get/stores/by-provide-store')")
	public ResponseEntity<Object> getStores(@RequestParam(required = true) List<Long> storeIds) {
		return this.storeService.getStoresByProvideStore(storeIds);
	}

	@PatchMapping("/activate/{id}")
	@PreAuthorize("hasAuthority('patch/stores/activate/{id}')")
	public ResponseEntity<Object> activate(@PathVariable Long id) throws BusinessException {
		return this.storeService.activate(id);
	}

	@PatchMapping("/deactivate/{id}")
	@PreAuthorize("hasAuthority('patch/stores/deactivate/{id}')")
	public ResponseEntity<Object> deactivate(@PathVariable Long id) throws BusinessException {
		return this.storeService.deactivate(id);
	}

	@GetMapping("/inventory/pallet-details")
	@PreAuthorize("hasAuthority('get/stores/inventory/pallet-details')")
	public ResponseEntity<Object> inventoryPalletDetail(@RequestParam(required = true) Long storeId,
			@RequestParam(required = true) Long productPackingId, @RequestParam(required = true) String expireDate)
			throws BusinessException, ParseException {
		return this.storeService.inventoryPalletDetail(storeId, productPackingId,
				new java.sql.Date(new SimpleDateFormat(Constants.DATE_PATTERN).parse(expireDate).getTime()));
	}

	@GetMapping("/users/{id}")
	@PreAuthorize("hasAuthority('get/stores/users/{id}')")
	public ResponseEntity<Object> getStoreUsers(@PathVariable("id") Long id) throws BusinessException {
		return this.storeService.getStoreUsers(id);
	}

	@GetMapping("/m-store/available-quantity")
	@PreAuthorize("hasAuthority('**/**')")
	public ResponseEntity<Object> getAvailableQuantity(@RequestParam(required = true) String townshipCode,
			@RequestParam(required = true) List<String> productPackingCodes) throws BusinessException {
		return this.storeService.getAvailableQuantity(townshipCode, productPackingCodes);
	}

	@PostMapping("/m-store/available-quantity")
	@PreAuthorize("hasAuthority('**/**')")
	public ResponseEntity<Object> getInventory(
			@RequestBody(required = true) AvailableQuantityFilterDto availableQuantityFilterDto)
			throws BusinessException {
		return this.storeService.getInventory(availableQuantityFilterDto);
	}

	@GetMapping("/m-store/get-store-by-location-charge")
	@PreAuthorize("hasAuthority('**/**')")
	public ResponseEntity<Object> getStoreByLocationCharge(@RequestParam(required = true) String townshipCode)
			throws BusinessException {
		return this.storeService.getStoreByLocationCharge(townshipCode);
	}

	@GetMapping("/townships")
	@PreAuthorize("hasAuthority('**/**')")
	public ResponseEntity<Object> findTownship(@RequestParam(required = true) List<String> townshipCodes,
			@RequestParam(required = true) Integer pageNumber, @RequestParam(required = true) Integer pageSize)
			throws BusinessException {
		return this.storeService.findTownship(townshipCodes, pageNumber, pageSize);
	}
}
