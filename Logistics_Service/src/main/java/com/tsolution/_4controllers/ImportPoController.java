package com.tsolution._4controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.importpo.ImportPoDetailPalletEntity;
import com.tsolution._1entities.importpo.ImportPoEntity;
import com.tsolution._3services.ImportPoService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/import-pos")
public class ImportPoController implements IBaseController<ImportPoEntity> {
	@Autowired
	private ImportPoService importPoService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/import-pos/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.importPoService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/import-pos/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.importPoService.findById(id);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/import-pos')")
	public ResponseEntity<Object> find(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestParam(value = "storeId", required = true) Long storeId,
			@RequestParam(value = "distributorId", required = false) Long distributorId,
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "status", required = false) Integer status,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		return this.importPoService.find(storeId, distributorId, text, status, pageNumber, pageSize, acceptLanguage);
	}

	@Override
	@PostMapping("/old")
	@PreAuthorize("hasAuthority('post/import-pos')")
	public ResponseEntity<Object> create(@RequestBody List<ImportPoEntity> entity) throws BusinessException {
		return this.importPoService.create(entity);
	}

	@PostMapping(consumes = { "multipart/form-data" })
	@PreAuthorize("hasAuthority('post/import-pos')")
	public ResponseEntity<Object> create(@RequestPart(name = "source", required = true) Optional<ImportPoEntity> source,
			@RequestPart(name = "files", required = false) MultipartFile[] files) throws BusinessException {
		if (source.isPresent()) {
			return this.importPoService.create(source.get(), files);
		}
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/import-pos/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<ImportPoEntity> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.importPoService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PostMapping("/update-pallet/{id}")
	@PreAuthorize("hasAuthority('post/import-pos/update-pallet/{id}')")
	public ResponseEntity<Object> updatePallet(@PathVariable("id") Long id,
			@RequestBody List<ImportPoDetailPalletEntity> importPoDetailPallets) throws BusinessException {
		return this.importPoService.updatePallet(id, importPoDetailPallets);
	}
}
