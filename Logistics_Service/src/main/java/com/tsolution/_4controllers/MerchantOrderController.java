package com.tsolution._4controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.dcommerce.MerchantOrderInventoryDto;
import com.tsolution._1entities.merchant_order.dto.InputOrderDto;
import com.tsolution._3services.MerchantOrderService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;

@RestController
@RequestMapping("/merchant-orders")
public class MerchantOrderController {
	@Autowired
	private MerchantOrderService merchantOrderService;

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/merchant-orders/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.merchantOrderService.findById(id);
	}

	@GetMapping("/{code}/status")
	@PreAuthorize("hasAuthority('get/merchant-orders/{code}/status')")
	public ResponseEntity<Object> findByCode(@PathVariable("code") String code) throws BusinessException {
		return this.merchantOrderService.findByCode(code);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/merchant-orders')")
	public ResponseEntity<Object> search(@RequestParam(name = "text", required = false) String text,
			@RequestParam(name = "status", required = false) Integer status,
			@RequestParam(value = "fromDate", required = true) String fromDate,
			@RequestParam(value = "toDate", required = true) String toDate,
			@RequestParam(value = "storeId", required = false) Long storeId,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize)
			throws ParseException, BusinessException {
		return this.merchantOrderService.search(text, new SimpleDateFormat(Constants.DATE_PATTERN).parse(fromDate),
				new SimpleDateFormat(Constants.DATE_PATTERN).parse(toDate), status, storeId, pageNumber, pageSize);
	}

	@GetMapping("/details")
	@PreAuthorize("hasAuthority('get/merchant-orders/details')")
	public ResponseEntity<Object> details(
			@RequestParam(name = "merchantOrderIds", required = true) List<Long> merchantOrderIds)
			throws BusinessException {
		return this.merchantOrderService.details(merchantOrderIds);
	}

	@GetMapping("/store-exists-not-enough-quantity")
	@PreAuthorize("hasAuthority('get/merchant-orders/store-exists-not-enough-quantity')")
	public ResponseEntity<Object> getStoresExistsNotEnoughQuantity(
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		return this.merchantOrderService.getStoresExistsNotEnoughQuantity(pageNumber, pageSize);
	}

	@GetMapping("/get-inventory")
	@PreAuthorize("hasAuthority('get/merchant-orders/get-inventory')")
	public ResponseEntity<Object> getInventoryByMerchantOrder(@RequestParam(required = true) List<String> storeCodes,
			@RequestParam(required = true) List<String> productPackingCodes) throws BusinessException {
		return this.merchantOrderService.getInventoryByMerchantOrder(storeCodes, productPackingCodes);
	}

	@PostMapping("/get-inventory")
	@PreAuthorize("hasAuthority('post/merchant-orders/get-inventory') or hasAuthority('/logistics-infomation')")
	public ResponseEntity<Object> getInventoryByMerchantOrder(
			@RequestBody MerchantOrderInventoryDto merchantOrderInventory) throws BusinessException {
		return this.merchantOrderService.getInventoryByMerchantOrder(merchantOrderInventory);
	}

	@PostMapping
	@PreAuthorize("hasAuthority('post/merchant-orders') or hasAuthority('/logistics-infomation')")
	public ResponseEntity<Object> create(@RequestBody List<InputOrderDto> inputOrderDto) throws BusinessException {
		return this.merchantOrderService.create(inputOrderDto);
	}

	@PostMapping("/cancel/{code}")
	@PreAuthorize("hasAuthority('post/merchant-orders/cancel/{code}') or hasAuthority('/logistics-infomation')")
	public ResponseEntity<Object> cancel(@PathVariable("code") String code) throws BusinessException {
		return this.merchantOrderService.cancel(code);
	}

}
