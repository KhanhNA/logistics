package com.tsolution._4controllers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.PoEntity;
import com.tsolution._1entities.dto.PoDto;
import com.tsolution._1entities.enums.PoStatus;
import com.tsolution._3services.PoService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DatetimeUtils;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/pos")
public class PoController implements IBaseController<PoEntity> {
	@Autowired
	private PoService poService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/pos/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.poService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/pos/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.poService.findById(id);
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/pos')")
	public ResponseEntity<Object> find(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestParam(value = "storeId", required = true) Long storeId,
			@RequestParam(value = "manufacturerId", required = false) Long manufacturerId,
			@RequestParam(value = "distributorId", required = false) Long distributorId,
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "dateType", required = true) PoStatus dateType,
			@RequestParam(value = "fromDate", required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fromDate,
			@RequestParam(value = "toDate", required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime toDate,
			@RequestParam(value = "status", required = false) PoStatus status,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize,
			@RequestParam(value = "orderBy", required = false) String orderBy) throws BusinessException {
		PoDto poDto = new PoDto();
		poDto.setStoreId(storeId);
		poDto.setManufacturerId(manufacturerId);
		poDto.setDistributorId(distributorId);
		poDto.setText(text);
		poDto.setDateType(dateType);
		poDto.setFromDate(DatetimeUtils.localDateTime2UtilDate(fromDate));
		poDto.setToDate(DatetimeUtils.localDateTime2UtilDate(toDate));
		poDto.setStatus(status);
		return this.poService.find(poDto, pageNumber, pageSize, orderBy, acceptLanguage);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/pos')")
	public ResponseEntity<Object> create(@RequestBody List<PoEntity> entity) throws BusinessException {
		return this.poService.create(entity);
	}

	@PostMapping("/print/{id}")
	@PreAuthorize("hasAuthority('post/pos/print/{id}')")
	public ResponseEntity<Object> print(@PathVariable("id") Long id) throws BusinessException {
		return this.poService.print(id);
	}

	@PostMapping("/print-pdf/{id}")
	@PreAuthorize("hasAuthority('post/pos/print-pdf/{id}')")
	public ResponseEntity<Object> printPdf(@PathVariable("id") Long id) throws BusinessException {
		return this.poService.printPdf(id);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/pos/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<PoEntity> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.poService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PatchMapping("/{id}/arrive-vietnam-port")
	@PreAuthorize("hasAuthority('patch/pos/{id}/arrive-vietnam-port')")
	public ResponseEntity<Object> updateArriveVietnamPort(@PathVariable("id") Long id,
			@RequestBody Optional<PoEntity> source) throws BusinessException {
		if (source.isPresent()) {
			return this.poService.updateArriveVietnamPort(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PatchMapping("/{id}/arrived-vietnam-port")
	@PreAuthorize("hasAuthority('patch/pos/{id}/arrived-vietnam-port')")
	public ResponseEntity<Object> updateArrivedVietnamPort(@PathVariable("id") Long id,
			@RequestBody Optional<PoEntity> source) throws BusinessException {
		if (source.isPresent()) {
			return this.poService.updateArrivedVietnamPort(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PatchMapping("/{id}/arrive-myanmar-port")
	@PreAuthorize("hasAuthority('patch/pos/{id}/arrive-myanmar-port')")
	public ResponseEntity<Object> updateArriveMyanmarPort(@PathVariable("id") Long id,
			@RequestBody Optional<PoEntity> source) throws BusinessException {
		if (source.isPresent()) {
			return this.poService.updateArriveMyanmarPort(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PatchMapping("/{id}/arrived-myanmar-port")
	@PreAuthorize("hasAuthority('patch/pos/{id}/arrived-myanmar-port')")
	public ResponseEntity<Object> updateArrivedMyanmarPort(@PathVariable("id") Long id,
			@RequestBody Optional<PoEntity> source) throws BusinessException {
		if (source.isPresent()) {
			return this.poService.updateArrivedMyanmarPort(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PatchMapping("/{id}/arrive-fc")
	@PreAuthorize("hasAuthority('patch/pos/{id}/arrive-fc')")
	public ResponseEntity<Object> updateArriveFc(@PathVariable("id") Long id, @RequestBody Optional<PoEntity> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.poService.updateArriveFc(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PatchMapping("/approve/{id}")
	@PreAuthorize("hasAuthority('patch/pos/approve/{id}')")
	public ResponseEntity<Object> approve(@PathVariable("id") Long id) throws BusinessException {
		return this.poService.approve(id);
	}

	@PatchMapping("/reject/{id}")
	@PreAuthorize("hasAuthority('patch/pos/reject/{id}')")
	public ResponseEntity<Object> reject(@PathVariable("id") Long id,
			@RequestBody(required = true) Optional<PoEntity> oPo) throws BusinessException {
		if (oPo.isPresent()) {
			return this.poService.reject(id, oPo.get());
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@PostMapping("/print")
	@PreAuthorize("hasAuthority('post/pos/print')")
	public ResponseEntity<Object> print(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestBody List<Long> ids) throws BusinessException {
		return this.poService.printMultiple(acceptLanguage, ids);
	}

	@PostMapping("/download-template")
	@PreAuthorize("hasAuthority('post/pos/download-template') OR hasAuthority('**/**')")
	public ResponseEntity<Object> downloadTemplate(
			@RequestParam(value = "exceptId", required = true) List<Long> exceptId,
			@RequestParam(value = "manufacturerId", required = true) Long manufacturerId,
			@RequestParam(value = "distributorId", required = true) Long distributorId) throws BusinessException {
		return this.poService.downloadTemplate(manufacturerId, distributorId, exceptId);
	}
}
