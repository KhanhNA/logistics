package com.tsolution._4controllers;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.claim.Claim;
import com.tsolution._1entities.claim.ClaimImage;
import com.tsolution._1entities.claim.enums.ClaimStatus;
import com.tsolution._1entities.claim.enums.ClaimType;
import com.tsolution._1entities.store.StoreEntity;
import com.tsolution._3services.ClaimService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DatetimeUtils;
import com.tsolution.utils.StringUtils;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/claims")
public class ClaimController implements IBaseController<Claim> {

	@Autowired
	private ClaimService claimService;

	@Autowired
	private Translator translator;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/claims/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.claimService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/claims/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.claimService.findById(id);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/claims')")
	public ResponseEntity<Object> create(@RequestBody List<Claim> entities) throws BusinessException {
		return this.claimService.create(entities);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/claims/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<Claim> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.claimService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/claims')")
	public ResponseEntity<Object> find(@RequestParam(value = "code", required = false) String code,
			@RequestParam(value = "storeId", required = true) Long storeId,
			@RequestParam(value = "type", required = false) String type,
			@RequestParam(value = "amenable", required = false) String amenable,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "status", required = false) Integer status,
			@RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fromCreateDate,
			@RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime toCreateDate,
			@RequestParam(required = true) Integer pageNumber, @RequestParam(required = true) Integer pageSize)
			throws BusinessException {
		Claim claim = new Claim();
		claim.setCode(code);
		claim.setStore(new StoreEntity(storeId));
		claim.setType(StringUtils.isNullOrEmpty(type) ? null : ClaimType.valueOf(type));
		claim.setAmenable(amenable);
		claim.setDescription(description);
		claim.setStatus(status);
		claim.setFromCreateDate(DatetimeUtils.localDateTime2SqlDate(fromCreateDate));
		claim.setToCreateDate(DatetimeUtils.localDateTime2SqlDate(toCreateDate));
		return this.claimService.find(claim, pageNumber, pageSize);
	}

	@GetMapping("/by-reference")
	@PreAuthorize("hasAuthority('get/claims/by-reference')")
	public ResponseEntity<Object> findByReference(@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "referenceId", required = true) Long referenceId) throws BusinessException {
		return this.claimService.findByReference(StringUtils.isNullOrEmpty(type) ? null : ClaimType.valueOf(type),
				referenceId);
	}

	@PatchMapping("/accept/{id}")
	@PreAuthorize("hasAuthority('patch/claims/accept/{id}')")
	public ResponseEntity<Object> accept(@PathVariable(value = "id", required = true) Long id)
			throws BusinessException {
		return this.claimService.accept(id);
	}

	@PatchMapping("/reject/{id}")
	@PreAuthorize("hasAuthority('patch/claims/reject/{id}')")
	public ResponseEntity<Object> reject(@PathVariable(value = "id", required = true) Long id,
			@RequestBody(required = true) Optional<Claim> oClaim) throws BusinessException {
		if (oClaim.isPresent()) {
			return this.claimService.reject(id, oClaim.get());
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PatchMapping("/{id}/confirm-completed")
	@PreAuthorize("hasAuthority('patch/claims/{id}/confirm-completed')")
	public ResponseEntity<Object> confirmCompleted(@PathVariable(value = "id", required = true) Long id,
			@RequestPart(name = "adds", required = false) List<ClaimImage> adds,
			@RequestPart(name = "files", required = false) MultipartFile[] files) throws BusinessException {
		return this.claimService.confirmCompleted(id, adds, files);
	}

	@PatchMapping(path = "/{id}/update-image", consumes = { "multipart/form-data" })
	@PreAuthorize("hasAuthority('patch/claims/{id}/update-image')")
	public ResponseEntity<Object> updateImage(@PathVariable("id") Long id,
			@RequestPart(name = "deletes", required = false) List<ClaimImage> deletes,
			@RequestPart(name = "adds", required = false) List<ClaimImage> adds,
			@RequestPart(name = "edits", required = false) List<ClaimImage> edits,
			@RequestPart(name = "files", required = false) MultipartFile[] files) throws BusinessException {
		return this.claimService.updateImage(id, deletes, adds, edits, files);
	}

	@PostMapping("/{id}/print-pdf")
	@PreAuthorize("hasAuthority('post/claims/{id}/print-pdf')")
	public ResponseEntity<Object> printPdf(@PathVariable(value = "id", required = true) Long id)
			throws BusinessException {
		return this.claimService.printPdf(id);
	}

	@GetMapping("/list-damaged-goods")
	@PreAuthorize("hasAuthority('get/claims/list-damaged-goods')")
	public ResponseEntity<Object> getListDamagedGoods(@RequestHeader("Accept-Language") String acceptLanguage,
			@RequestParam(value = "storeId", required = true) Long storeId,
			@RequestParam(value = "status", required = true) ClaimStatus status,
			@RequestParam(value = "type", required = false) ClaimType type,
			@RequestParam(value = "amenable", required = false) String amenable,
			@RequestParam(value = "productPackingCode", required = false) String productPackingCode,
			@RequestParam(value = "productName", required = false) String productName,
			@RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fromCreateDate,
			@RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime toCreateDate,
			@RequestParam(required = true) Integer pageNumber, @RequestParam(required = true) Integer pageSize)
			throws BusinessException {
		if (Arrays.asList(ClaimStatus.APPROVED, ClaimStatus.COMPLETED).stream()
				.noneMatch(x -> x.getValue().equals(status.getValue()))) {
			throw new BusinessException(BusinessException.COMMON_INPUT_INFO_INVALID);
		}
		Claim claim = new Claim();
		claim.setStore(new StoreEntity(storeId));
		claim.setType(type);
		claim.setAmenable(amenable);
		claim.setStatus(status.getValue());
		claim.setProductPackingCode(productPackingCode);
		claim.setProductName(productName);
		claim.setFromCreateDate(DatetimeUtils.localDateTime2SqlDate(fromCreateDate));
		claim.setToCreateDate(DatetimeUtils.localDateTime2SqlDate(toCreateDate));
		return this.claimService.getListDamagedGoods(claim, acceptLanguage, pageNumber, pageSize);
	}
}
