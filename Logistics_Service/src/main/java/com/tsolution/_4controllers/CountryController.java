package com.tsolution._4controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._3services.CountryService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/countries")
public class CountryController {

	@Autowired
	private CountryService countryService;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/countries/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.countryService.findAll();
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/countries')")
	public ResponseEntity<Object> getAll(@RequestParam(value = "support", required = false) Boolean support,
			@RequestParam(value = "language", required = false) Long language) {
		return this.countryService.find(support, language);
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/countries/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.countryService.findById(id);
	}

	@GetMapping("/locations/tree")
	@PreAuthorize("hasAuthority('get/countries/locations/tree') OR hasAuthority('get/promotions/{id}')")
	public ResponseEntity<Object> getLocationsTree() throws BusinessException {
		return this.countryService.getLocationsTree();
	}

	@GetMapping("/locations/flat-tree-node")
	@PreAuthorize("hasAuthority('get/countries/locations/flat-tree-node') OR hasAuthority('get/promotions/{id}')")
	public ResponseEntity<Object> getLocationsTownship() throws BusinessException {
		return this.countryService.getLocationsTownship();
	}
}
