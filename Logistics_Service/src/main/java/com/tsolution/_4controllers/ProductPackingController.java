package com.tsolution._4controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._3services.ProductPackingService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/product-packings")
public class ProductPackingController {
	@Autowired
	private ProductPackingService productPackingService;

	@GetMapping
	@PreAuthorize("hasAuthority('get/product-packings')")
	public ResponseEntity<Object> find(@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "exceptId", required = true) List<Long> exceptId,
			@RequestParam(value = "manufacturerId", required = true) Long manufacturerId,
			@RequestParam(value = "distributorId", required = true) Long distributorId,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException {
		return this.productPackingService.find(text, exceptId, manufacturerId, distributorId, pageNumber, pageSize);
	}
}
