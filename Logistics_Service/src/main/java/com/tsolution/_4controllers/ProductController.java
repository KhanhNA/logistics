package com.tsolution._4controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._3services.IProductService;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private IProductService productService;

	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/products/all')")
	public ResponseEntity<Object> findAll() throws BusinessException {
		return this.productService.findAll();
	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/products')")
	public ResponseEntity<Object> find(@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "exceptId", required = true) List<Long> exceptId,
			@RequestParam(value = "PageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "PageSize", required = true) Integer pageSize) throws BusinessException {
		return this.productService.find(text, exceptId, pageNumber, pageSize);
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/products/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.productService.findById(id);
	}

	@GetMapping("/packing-type")
	@PreAuthorize("hasAuthority('get/products/packing-type')")
	public ResponseEntity<Object> getPackingType(@RequestParam(value = "status", required = true) Boolean status) {
		return this.productService.getPackingType(status);
	}
}
