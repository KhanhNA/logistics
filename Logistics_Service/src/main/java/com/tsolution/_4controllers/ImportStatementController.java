package com.tsolution._4controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsolution._1entities.dto.ImportStatementDto;
import com.tsolution._3services.ImportStatementService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Translator;

@RestController
@RequestMapping("/import-statements")
public class ImportStatementController {
	@Autowired
	private ImportStatementService importStatementService;

	@Autowired
	private Translator translator;

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/import-statements/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.importStatementService.findById(id);
	}

	@GetMapping("/repacking-planning/{id}")
	@PreAuthorize("hasAuthority('get/import-statements/repacking-planning/{id}')")
	public ResponseEntity<Object> findByRepackingPlanningId(@PathVariable("id") Long repackingPlanningId)
			throws BusinessException {
		return this.importStatementService.findByRepackingPlanningId(repackingPlanningId);
	}

	@GetMapping("/find")
	@PreAuthorize("hasAuthority('get/import-statements/find')")
	public ResponseEntity<Object> getAll(@RequestParam(value = "code", required = false) String code,
			@RequestParam(value = "status", required = true) Integer status,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(value = "pageSize", required = true) Integer pageSize) throws BusinessException, IOException {
		return this.importStatementService.find(code, status, type, pageNumber, pageSize);
	}

	@PostMapping
	@PreAuthorize("hasAuthority('post/import-statements')")
	public ResponseEntity<Object> create(@RequestBody List<ImportStatementDto> importStatementDtos)
			throws BusinessException {
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_NOT_SUPPORT));
	}

	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/import-statements/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id, @RequestBody Optional<ImportStatementDto> source)
			throws BusinessException {
		if (source.isPresent()) {
			return this.importStatementService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}

	@PostMapping("/import-fc")
	@PreAuthorize("hasAuthority('post/import-statements/import-fc')")
	public ResponseEntity<Object> importFC(@RequestBody ImportStatementDto importStatementDto)
			throws BusinessException {
		throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_NOT_SUPPORT));
	}

	@PostMapping("/from-export-statement")
	@PreAuthorize("hasAuthority('post/import-statements/from-export-statement')")
	public ResponseEntity<Object> createFromExportStatement(@RequestBody ImportStatementDto importStatementDto)
			throws BusinessException {
		return this.importStatementService.createFromExportStatement(importStatementDto);
	}

	@PostMapping("/from-repacking-planning")
	@PreAuthorize("hasAuthority('post/import-statements/from-repacking-planning')")
	public ResponseEntity<Object> createFromRepackingPlanning(@RequestBody ImportStatementDto importStatementDto)
			throws BusinessException {
		return this.importStatementService.createFromRepackingPlanning(importStatementDto);
	}

	@PostMapping("/print/{id}")
	@PreAuthorize("hasAuthority('post/import-statements/print/{id}')")
	public ResponseEntity<Object> print(@PathVariable("id") Long id, HttpServletResponse response)
			throws BusinessException {
		return this.importStatementService.print(id);
	}

	@PatchMapping("/{id}/import")
	@PreAuthorize("hasAuthority('patch/import-statements/{id}/import')")
	public ResponseEntity<Object> actuallyImport(@PathVariable("id") Long id,
			@RequestBody(required = true) Optional<ImportStatementDto> oImportStatementDto) throws BusinessException {
		if (oImportStatementDto.isPresent()) {
			ImportStatementDto importStatementDto = oImportStatementDto.get();
			return this.importStatementService.actuallyImport(id, importStatementDto.getDescription(),
					importStatementDto.getImportStatementDetailPallets());
		} else {
			throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
		}
	}
}
