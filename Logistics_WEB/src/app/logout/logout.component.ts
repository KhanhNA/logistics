import {Component, OnInit} from '@angular/core';
import {LoginComponent} from '../login/login.component';
import {CookieService} from 'ngx-cookie-service';
import {UiStateService} from '../_services/ui.state.service';

@Component({
  selector: 'app-logout',
  template: ``,
  styles: [``]
})
export class LogoutComponent implements OnInit {
  constructor(private loginComponent: LoginComponent,
              private cookieService: CookieService,
              private uiStateService: UiStateService) {
  }
  ngOnInit() {
    this.uiStateService.setUiState$({formDataMap: new Map()});
    window.sessionStorage.removeItem('token');
    this.cookieService.delete('username');
    this.cookieService.delete('roles');
    this.loginComponent.showLoginModal();
  }
}
