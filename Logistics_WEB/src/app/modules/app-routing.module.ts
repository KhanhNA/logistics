import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {LogoutComponent} from '../logout/logout.component';
import {StoreInventoryComponent} from '../components/store/inventory/store-inventory.component';
import {UserComponent} from '../components/user/l-d-user/user.component';
import {AddEditUserComponent} from '../components/user/a-e-user/add.edit.user.component';
import {MerchantOrderComponent} from '../components/merchant-order/merchant.order.component';
import {LDPalletInventoryComponent} from '../components/pallet-inventory/l-d-pallet-inventory.component';
import {LDPoComponent} from '../components/po/l-d-po.component';
import {GoodsBonusComponent} from '../components/goods-bonus/goods-bonus.component';
import {LDRepackingComponent} from '../components/repacking/l-d-repacking.component';
import {AERepackingComponent} from '../components/repacking/a-e-repacking/a-e-repacking.component';
import {ListPalletComponent} from '../components/pallet/list-pallet.component';
import {ListStoreComponent} from '../components/store/l-d-store/list-store.component';
import {MaxDeliveryDayComponent} from '../components/store/max-delivery-day/max-delivery-day.component';
import {AddEditPoComponent} from '../components/po/add-edit-po/add-edit-po.component';
import {AddEditStoreComponent} from '../components/store/add-edit-store/add-edit-store.component';
import {ExchangeRateComponent} from '../components/exchange-rate/exchange-rate.component';
import {AddEditExchangeRateComponent} from '../components/exchange-rate/add-edit-exchange-rate/add-edit-exchange-rate.component';
import {AuthoritiesResolverService} from '@next-solutions/next-solutions-base';
import {AddEditPalletComponent} from "../components/pallet/add-edit-pallet/add-edit-pallet.component";

const routes: Routes = [
  {path: '', component: DashboardComponent, resolve: {me: AuthoritiesResolverService}},
  {path: 'home', component: DashboardComponent, resolve: {me: AuthoritiesResolverService}},
  {path: 'logout', component: LogoutComponent, resolve: {me: AuthoritiesResolverService}},
  {
    path: 'store-inventory',
    component: StoreInventoryComponent,
    data: {breadcrumb: 'menu.store.inventory'},
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'merchant-orders',
    component: MerchantOrderComponent,
    data: {breadcrumb: 'menu.merchant.orders'},
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'app-pallet-inventory-v1',
    component: LDPalletInventoryComponent,
    data: {breadcrumb: 'menu.pallet.inventory'},
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'app-pallet-inventory',
    // component: PalletInventoryV2Component,
    data: {breadcrumb: 'menu.pallet.inventory'},
    resolve: {me: AuthoritiesResolverService},
    loadChildren: () =>
      import('../components/pallet-inventory-v2/pallet-inventory-v2.module').then(m => m.PalletInventoryV2Module)

  },

  {
    path: 'orders-bonus',
    component: GoodsBonusComponent,
    data: {breadcrumb: 'menu.goods.bonus'},
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'exchange-rate',
    data: {breadcrumb: 'common.exchange.rate'},
    children: [
      {path: '', component: ExchangeRateComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
      {
        path: ':actType',
        component: AddEditExchangeRateComponent,
        data: {breadcrumb: 'add'},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: ':actType/:id',
        component: AddEditExchangeRateComponent,
        data: {breadcrumb: 'edit'},
        resolve: {me: AuthoritiesResolverService}
      },
    ]
  },

  {
    path: 'user',
    data: {breadcrumb: 'menu.users'},
    children: [
      {path: '', component: UserComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
      {
        path: 'add',
        component: AddEditUserComponent,
        data: {breadcrumb: 'user.label.add'},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: 'edit/:username',
        component: AddEditUserComponent,
        data: {breadcrumb: 'user.label.edit'},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: 'view/:username',
        component: AddEditUserComponent,
        data: {breadcrumb: 'user.label.view', isView: true},
        resolve: {me: AuthoritiesResolverService}
      },
    ]
  },
  {
    path: 'partner',
    data: {breadcrumb: 'menu.partner'},
    loadChildren: () =>
      import('../components/partner/partner.module').then(m => m.PartnerModule)
  },
  {
    path: 'store',
    data: {breadcrumb: 'menu.store'},
    children: [
      {path: '', component: ListStoreComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
      {
        path: ':actType',
        component: AddEditStoreComponent,
        data: {breadcrumb: 'menu.store.add'},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: ':actType/:id',
        data: {breadcrumb: 'menu.store.edit'},
        children: [
          {
            pathMatch: 'full',
            path: '',
            component: AddEditStoreComponent,
            resolve: {me: AuthoritiesResolverService}
          },
          {
            path: 'app-pallet/add',
            component: AddEditPalletComponent,
            data: {breadcrumb: 'menu.pallet.add'},
            resolve: {me: AuthoritiesResolverService}
          },
          {
            path: 'app-pallet/edit/:palletId',
            component: AddEditPalletComponent,
            data: {breadcrumb: 'menu.pallet.edit'},
            resolve: {me: AuthoritiesResolverService}
          },
        ]
      },
      {
        path: ':actType/max-delivery/:id',
        component: MaxDeliveryDayComponent,
        data: {breadcrumb: 'store.max-delivery'},
        resolve: {me: AuthoritiesResolverService}
      },
    ]
  },

  {
    path: 'app-pallet',
    data: {breadcrumb: 'menu.po.pallet'},
    children: [
      {path: '', component: ListPalletComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
      {
        path: 'add',
        component: AddEditPalletComponent,
        data: {breadcrumb: 'menu.pallet.add'},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: 'edit/:id',
        component: AddEditPalletComponent,
        data: {breadcrumb: 'menu.pallet.edit'},
        resolve: {me: AuthoritiesResolverService}
      },
    ]
  },

  // {
  //   path: 'pov1',
  //   data: {breadcrumb: 'menu.po.list'},
  //   children: [
  //     {path: '', component: LDPoComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
  //     {
  //       path: ':actType',
  //       component: AddEditPoComponent,
  //       data: {breadcrumb: ':actType'},
  //       resolve: {me: AuthoritiesResolverService}
  //     },
  //     {
  //       path: ':actType/:id',
  //       component: AddEditPoComponent,
  //       data: {breadcrumb: ':actType'},
  //       resolve: {me: AuthoritiesResolverService}
  //     },
  //   ]
  // },
  {
    path: 'po',
    data: {breadcrumb: 'menu.po.list'},
    loadChildren: () =>
      import('../components/pov2/pov2.module').then(m => m.Pov2Module)
  },
  /*  {
      path: 'import-po',
      data: {breadcrumb: 'menu.import-po.list'},
      children: [
        {path: '', component: LDImportPoComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
        {
          path: ':actType',
          component: AEImportPoComponent,
          data: {breadcrumb: ':actType'},
          resolve: {me: AuthoritiesResolverService}
        },
        {
          path: ':actType/:id',
          component: AEImportPoComponent,
          data: {breadcrumb: ':actType'},
          resolve: {me: AuthoritiesResolverService}
        },
      ]
    },*/
  {
    path: 'import-po',
    data: {breadcrumb: 'menu.import-po.list'},
    loadChildren: () => import('../components/import-po-v2/import-po-v2.module').then(m => m.ImportPoV2Module)
  },
  // {
  //   path: 'app-repacking',
  //   data: {breadcrumb: 'menu.po.packing'},
  //   children: [
  //     {path: '', component: LDRepackingComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
  //     {
  //       path: ':actType',
  //       component: AERepackingComponent,
  //       data: {breadcrumb: ':actType'},
  //       resolve: {me: AuthoritiesResolverService}
  //     },
  //     {
  //       path: ':actType/:id',
  //       component: AERepackingComponent,
  //       data: {breadcrumb: ':actType'},
  //       resolve: {me: AuthoritiesResolverService}
  //     },
  //   ]
  // },
  {
    path: 'repacking',
    data: {breadcrumb: 'menu.po.packing'},
    loadChildren: () => import('../components/repacking-v2/repacking-v2.module').then(m => m.RepackingV2Module)
  },


  /*
    {
      path: 'import-statement',
      data: {breadcrumb: 'menu.import.statement.list'},
      children: [
        {path: '', component: ListImportStatementComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
        {
          path: ':actType/:id',
          component: EditImportStatementComponent,
          data: {breadcrumb: ':actType'},
          resolve: {me: AuthoritiesResolverService}
        },
      ],
    },
  */
  {
    path: 'import-statement',
    data: {breadcrumb: 'menu.import.statement.list'},
    loadChildren: () => import('../components/import-statement-v2/import-statement-v2.module').then(m => m.ImportStatementV2Module)
  },
  // {
  //   path: 'export-statement',
  //   data: {breadcrumb: 'menu.export.statement.list'},
  //   children: [
  //     {path: '', component: LDExportStatementComponent, pathMatch: 'full', resolve: {me: AuthoritiesResolverService}},
  //     {
  //       path: 'add',
  //       component: AddExportStatementComponent,
  //       data: {breadcrumb: 'add'},
  //       resolve: {me: AuthoritiesResolverService}
  //     },
  //     {
  //       path: ':actType/:id',
  //       component: EditExportStatementComponent,
  //       data: {breadcrumb: ':actType'},
  //       resolve: {me: AuthoritiesResolverService}
  //     },
  //   ]
  // },
  {
    path: 'damaged-goods',
    data: {breadcrumb: 'menu.damaged-goods'},
    loadChildren: () => import('../components/damaged-goods/damaged-goods.module').then(m => m.DamagedGoodsModule)
  },
  {
    path: 'export-statement',
    data: {breadcrumb: 'menu.export.statement.list'},
    loadChildren: () => import('../components/export-statement-v2/export-statement-v2.module').then(m => m.ExportStatementV2Module)
  },
  {
    path: 'list-export-statement-for-merchant-order',
    data: {breadcrumb: 'menu.export.statement.list'},
    loadChildren: () => import('../components/export-statement-merchant-order/export-statement-merchant-order.module')
      .then(m => m.ExportStatementMerchantOrderModule)
  },
  {
    path: 'list-store-inventory-report',
    data: {breadcrumb: 'menu.store.inventory.report'},
    loadChildren: () => import('../components/report/inventory/inventory-list/store-inventory-list.module')
      .then(m => m.StoreInventoryListModule)
  },
  {
    path: 'pallet-inventory-report',
    data: {breadcrumb: 'menu.pallet.inventory.report'},
    loadChildren: () => import('../components/report/inventory/pallet-inventory-report/pallet-inventory-report.module')
      .then(m => m.PalletInventoryReportModule)
  },
  {
    path: 'inventory-sell-out-report',
    data: {breadcrumb: 'menu.inventory.sell.out.report'},
    loadChildren: () => import('../components/report/inventory-sell-out/inventory-sell-out.module')
      .then(m => m.InventorySellOutModule)
  },
  {
    path: 'claim',
    data: {breadcrumb: 'menu.claim'},
    loadChildren: () => import('../components/claim/claim.module').then(m => m.ClaimModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
      useHash: true,
      onSameUrlNavigation: 'reload',
    }
  )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
