import {ReadComponent} from '../base/field/components/read.component';
import {InputComponent} from '../base/field/components/input.component';
import {ButtonComponent} from '../base/field/components/button.component';
import {SelectComponent} from '../base/field/components/select.component';
import {DateComponent} from '../base/field/components/date.component';
import {RadiobuttonComponent} from '../base/field/components/radiobutton.component';
import {CheckboxComponent} from '../base/field/components/checkbox.component';
import {DynamicFormComponent} from '../base/field/components/dynamic-form/dynamic-form.component';
import {FileImgComponent} from '../base/field/components/file-img.component';
import {LDComponent} from '../base/l-d/l-d-component';
import {BaseFormAEComponent} from '../base/base-form-a-e.component';
import {AutocompleteComponent} from '../base/field/components/autocomplete.component';
import {TsTableV2Component} from '../base/ts-table-v2/ts-table-v2-component';
import {LDDialogSearchComponent} from '../components/dialog-search/l-d-dialog-search.component';

export const ThonvEntryComponent = [
  ReadComponent,
  InputComponent,
  ButtonComponent,
  SelectComponent,
  DateComponent,
  RadiobuttonComponent,
  CheckboxComponent,
  DynamicFormComponent,
  FileImgComponent,
  LDComponent,
  BaseFormAEComponent,
  AutocompleteComponent,
  TsTableV2Component,
  LDDialogSearchComponent,
];
