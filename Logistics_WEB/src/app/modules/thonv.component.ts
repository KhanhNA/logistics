import {InputComponent} from '../base/field/components/input.component';
import {ButtonComponent} from '../base/field/components/button.component';
import {SelectComponent} from '../base/field/components/select.component';
import {DateComponent} from '../base/field/components/date.component';
import {RadiobuttonComponent} from '../base/field/components/radiobutton.component';
import {CheckboxComponent} from '../base/field/components/checkbox.component';
import {DynamicFormComponent} from '../base/field/components/dynamic-form/dynamic-form.component';
import {DynamicFieldDirective} from '../base/field/components/dynamic-field/dynamic-field.directive';
import {LanguageComponent} from '../base/language/language.component';
import {ActionComponent} from '../base/action/action.component';
import {FileImgComponent} from '../base/field/components/file-img.component';
import {DragDropDirective} from '../base/field/components/dynamic-field/drag-drop.directive';
import {LDComponent} from '../base/l-d/l-d-component';
import {BaseFormLDComponent} from '../base/base-form-l-d.component';
import {BaseFormAEComponent} from '../base/base-form-a-e.component';
import {AutocompleteComponent} from '../base/field/components/autocomplete.component';
import {TsTableV2Component} from '../base/ts-table-v2/ts-table-v2-component';
import {ReadComponent} from '../base/field/components/read.component';
import {LDDialogSearchComponent} from '../components/dialog-search/l-d-dialog-search.component';
import {ConfirmationDialogComponent} from '../base/confirm-dialog/confirm.component';

export const ThonvComponent = [
  InputComponent,
  ButtonComponent,
  SelectComponent,
  DateComponent,
  RadiobuttonComponent,
  CheckboxComponent,
  DynamicFormComponent,
  DynamicFieldDirective,
  LanguageComponent,
  ActionComponent,
  FileImgComponent,
  DragDropDirective,
  LDComponent,
  BaseFormLDComponent,
  BaseFormAEComponent,
  AutocompleteComponent,
  TsTableV2Component,
  ReadComponent,
  LDDialogSearchComponent,
  ConfirmationDialogComponent,
];
