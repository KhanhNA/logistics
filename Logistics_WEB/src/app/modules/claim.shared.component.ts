import {AddEditPoV2Component} from '../components/pov2/add-edit-po/add-edit-po-v2.component';
import {ImportPoV2EditComponent} from '../components/import-po-v2/import-po-v2-edit/import-po-v2-edit.component';
import {AERepackingComponent} from '../components/repacking/a-e-repacking/a-e-repacking.component';
import {AddEditExportStatementV2Component} from '../components/export-statement-v2/add-edit-export-statement-v2/add-edit-export-statement-v2.component';
import {EditImportStatementComponent} from '../components/import-statement/edit-import-statement/edit-import-statement.component';
import {AddEditPoManufactureComponent} from '../components/pov2/add-edit-po/add-edit-po-manufacture/add-edit-po-manufacture.component';
import {AddEditPoProductComponent} from '../components/pov2/add-edit-po/add-edit-po-product/add-edit-po-product.component';

export const ClaimSharedComponent = [
  // po
  AddEditPoV2Component,
  AddEditPoManufactureComponent,
  AddEditPoProductComponent,

  // import po
  ImportPoV2EditComponent,

  // repacking
  AERepackingComponent,

  // export statement
  AddEditExportStatementV2Component,

  // import statment
  EditImportStatementComponent
];
