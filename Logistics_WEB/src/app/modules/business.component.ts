import {StoreInventoryComponent} from '../components/store/inventory/store-inventory.component';
import {UserComponent} from '../components/user/l-d-user/user.component';
import {AddEditUserComponent} from '../components/user/a-e-user/add.edit.user.component';
import {MerchantOrderComponent} from '../components/merchant-order/merchant.order.component';
import {MerchantOrderDetailComponent} from '../components/merchant-order/detail/merchant.order.detail.component';
import {DividePackingComponent} from '../components/divide-packing/divide-packing.component';
import {StoreInventoryPalletDetailsComponent} from '../components/store/inventory/pallet-details/store-inventory-pallet-details.component';
import {LDPalletInventoryComponent} from '../components/pallet-inventory/l-d-pallet-inventory.component';
import {ClaimComponent} from '../components/claim/claim.component';
import {ClaimInfoComponent} from '../components/claim/claim-info/claim-info.component';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {ListImportStatementComponent} from '../components/import-statement/list-import-statement/list-import-statement.component';
import {ListStoreComponent} from '../components/store/l-d-store/list-store.component';
import {StoresInventoryIgnoreExpireDateComponent} from '../components/dialog-select-product-packing/stores-inventory-ignore-expire-date.component';
import {ListPalletComponent} from '../components/pallet/list-pallet.component';
import {ProductPackingsComponent} from '../components/dialog-select-product-packing/product-packings.component';
import {PalletsProductPackingGroupByExpireDateComponent} from '../components/dialog-select-product-packing/pallets-product-packing-group-by-expire-date.component';
import {LDExportStatementComponent} from '../components/export-statement/list-export-statement/l-d-export-statement.component';
import {MerchantComponent} from '../components/store/merchant/merchant.component';
import {LDPoComponent} from '../components/po/l-d-po.component';
import {GoodsBonusComponent} from '../components/goods-bonus/goods-bonus.component';
import {AddExportStatementComponent} from '../components/export-statement/add-export-statement/add-export-statement.component';
import {EditExportStatementComponent} from '../components/export-statement/edit-export-statement/edit-export-statement.component';
import {LDImportPoComponent} from '../components/import-po/l-d-import-po.component';
import {AEImportPoComponent} from '../components/import-po/a-e-import-po/a-e-import-po.component';
import {LDRepackingComponent} from '../components/repacking/l-d-repacking.component';
import {AERepackingComponent} from '../components/repacking/a-e-repacking/a-e-repacking.component';
import {LDImportStockComponent} from '../components/repacking/a-e-repacking/import-stock/l-d-import-stock.component';
import {PlanComponent} from '../components/repacking/a-e-repacking/plan/plan.component';
import {RepackComponent} from '../components/repacking/a-e-repacking/repack/repack.component';
import {LDDialogSearchClientComponent} from '../components/export-statement/dialog-search-client/l-d-dialog-search-client.component';
import {AEPalletComponent} from '../components/pallet/a-e-pallet/a-e-pallet.component';
import {AEStoreComponent} from '../components/store/a-e-store/a-e-store.component';
import {MaxDeliveryDayComponent} from '../components/store/max-delivery-day/max-delivery-day.component';
import {AddImportStatementComponent} from '../components/import-statement/add-import-statement/add-import-statement.component';
import {ExportStatementListComponent} from '../components/import-statement/export-statement-list/export-statement-list.component';
import {EditImportStatementComponent} from '../components/import-statement/edit-import-statement/edit-import-statement.component';
import {AddEditPoComponent} from '../components/po/add-edit-po/add-edit-po.component';
import {AddEditStoreComponent} from '../components/store/add-edit-store/add-edit-store.component';
import {ExchangeRateComponent} from '../components/exchange-rate/exchange-rate.component';
import {AddEditExchangeRateComponent} from '../components/exchange-rate/add-edit-exchange-rate/add-edit-exchange-rate.component';
import {ChangePasswordComponent} from '../components/change-password/change-password.component';
import {ClaimSharedComponent} from "./claim.shared.component";

export const BusinessComponent = [
  DashboardComponent,
  StoreInventoryComponent,
  UserComponent,
  AddEditUserComponent,
  MerchantOrderComponent,
  MerchantOrderDetailComponent,
  DividePackingComponent,
  StoreInventoryPalletDetailsComponent,
  LDPalletInventoryComponent,
  // ClaimComponent,
  // ClaimInfoComponent,
  StoresInventoryIgnoreExpireDateComponent,
  ProductPackingsComponent,
  PalletsProductPackingGroupByExpireDateComponent,
  GoodsBonusComponent,
  LDDialogSearchClientComponent,
  // Store
  ListStoreComponent,
  AEStoreComponent,
  MaxDeliveryDayComponent,
  MerchantComponent,
  AddEditStoreComponent,
  ExchangeRateComponent,
  AddEditExchangeRateComponent,
  // Pallet
  ListPalletComponent,
  AEPalletComponent,
  // PO
  LDPoComponent,
  AddEditPoComponent,
  // Import PO
  LDImportPoComponent,
  AEImportPoComponent,
  // Repacking Planning
  LDRepackingComponent,
  // AERepackingComponent,
  LDImportStockComponent,
  PlanComponent,
  RepackComponent,
  // Import Statement
  ListImportStatementComponent,
  AddImportStatementComponent,
  // EditImportStatementComponent,
  ExportStatementListComponent,
  // Export Statement
  LDExportStatementComponent,
  AddExportStatementComponent,
  EditExportStatementComponent,

  ChangePasswordComponent,

  ClaimSharedComponent
];

