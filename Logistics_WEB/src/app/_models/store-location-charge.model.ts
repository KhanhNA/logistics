
import {SuperEntity} from "@next-solutions/next-solutions-base";
import {LocationTreeModel} from "./LocationTreeModel";

export class StoreLocationChargeModel extends SuperEntity{
  countryLocation?: LocationTreeModel;
}
