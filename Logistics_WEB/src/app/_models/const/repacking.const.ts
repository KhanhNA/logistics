import {RepackingPlanningModel} from '../repacking.planning.model';

export const repackingStatus = {
  // NEW
  NEW: {
    code: 0,
    label: 'common.status.new'
  },

  // REPACKED
  REPACKED: {
    code: 1,
    label: 'common.status.repacked'
  },

  // IMPORTED
  IMPORTED: {
    code: 2,
    label: 'common.status.imported'
  },

  // PLANNED
  PLANNED: {
    code: 3,
    label: 'common.status.planned'
  },

  // CANCEL
  CANCELED: {
    code: -1,
    label: 'common.status.cancel'
  }
};

export const repackingStatusAction = {
  isNewStatus(repacking: RepackingPlanningModel | number | undefined): boolean {
    if (typeof repacking === 'number') {
      return repacking === repackingStatus.NEW.code;
    }
    return !!repacking && repacking.status === repackingStatus.NEW.code;
  },

  isPlannedStatus(repacking: RepackingPlanningModel | number | undefined): boolean {
    if (typeof repacking === 'number') {
      return repacking === repackingStatus.PLANNED.code;
    }
    return !!repacking && repacking.status === repackingStatus.PLANNED.code;
  },
  isRepackedStatus(repacking: RepackingPlanningModel | number | undefined): boolean {
    if (typeof repacking === 'number') {
      return repacking === repackingStatus.REPACKED.code;
    }
    return !!repacking && repacking.status === repackingStatus.REPACKED.code;
  },
  isImportedStatus(repacking: RepackingPlanningModel | number | undefined): boolean {
    if (typeof repacking === 'number') {
      return repacking === repackingStatus.IMPORTED.code;
    }
    return !!repacking && repacking.status === repackingStatus.IMPORTED.code;
  },
  isCanceledStatus(repacking: RepackingPlanningModel | number | undefined): boolean {
    if (typeof repacking === 'number') {
      return repacking === repackingStatus.CANCELED.code;
    }
    return !!repacking && repacking.status === repackingStatus.CANCELED.code;
  },

  getStatusLabelKey(repacking: RepackingPlanningModel | number | undefined) {
    if (this.isNewStatus(repacking)) {
      return repackingStatus.NEW.label;
    } else if (this.isPlannedStatus(repacking)) {
      return repackingStatus.PLANNED.label;
    } else if (this.isRepackedStatus(repacking)) {
      return repackingStatus.REPACKED.label;
    } else if (this.isImportedStatus(repacking)) {
      return repackingStatus.IMPORTED.label;
    } else if (this.isCanceledStatus(repacking)) {
      return repackingStatus.CANCELED.label;
    }
    return '';
  },
  // getStatusCode(repacking: RepackingPlanningModel | number | undefined) {
  //   if (this.isNewStatus(repacking)) {
  //     return repackingStatus.NEW.code;
  //   } else if (this.isPlannedStatus(repacking)) {
  //     return repackingStatus.PLANNED.code;
  //   } else if (this.isRepackedStatus(repacking)) {
  //     return repackingStatus.REPACKED.code;
  //   } else if (this.isImportedStatus(repacking)) {
  //     return repackingStatus.IMPORTED.code;
  //   } else if (this.isCanceledStatus(repacking)) {
  //     return repackingStatus.CANCELED.code;
  //   }
  //   return '';
  // }
};
