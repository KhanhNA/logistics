export const merchantOrderShippingType = {
  SHIPPING: {
    code: 'SHIPPING'
  },
  PICK_UP: {
    code: 'PICK_UP'
  }
}
