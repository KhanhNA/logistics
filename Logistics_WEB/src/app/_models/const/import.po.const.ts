import {RepackingPlanningModel} from "../repacking.planning.model";
import {repackingStatus} from "./repacking.const";
import {ImportPoModel} from "../import-po/import.po.model";

export const importPoStatus = {
  NEW: {
    code: 0,
    label: 'import-po.status.new'
  },
  IN_PALLET: {
    code: 1,
    label: 'import-po.status.in_pallet'
  },
  OUT_OF_PALLET: {
    code: 2,
    label: 'import-po.status.out_of_pallet'
  }
}

export const importPoStatusAction = {
  isNewStatus(importPo: ImportPoModel | number | undefined): boolean {
    if (typeof importPo === 'number') {
      return importPo === importPoStatus.NEW.code;
    }
    return !!importPo && importPo.status === importPoStatus.NEW.code;
  },
  isInPalletStatus(importPo: ImportPoModel | number | undefined): boolean {
    if (typeof importPo === 'number') {
      return importPo === importPoStatus.IN_PALLET.code;
    }
    return !!importPo && importPo.status === importPoStatus.IN_PALLET.code;
  },
  isOutOfPalletStatus(importPo: ImportPoModel | number | undefined): boolean {
    if (typeof importPo === 'number') {
      return importPo === importPoStatus.OUT_OF_PALLET.code;
    }
    return !!importPo && importPo.status === importPoStatus.OUT_OF_PALLET.code;
  },
}
