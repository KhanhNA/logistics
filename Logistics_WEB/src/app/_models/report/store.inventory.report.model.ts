export class StoreInventoryReportModel {
  amount: number | undefined;
  barcode: string | undefined;
  currency: string | undefined;
  expiredDate: string | undefined;
  importPrice: number | undefined;
  packCode: string | undefined;
  packSize: number | undefined;
  productName: string | undefined;
  quantity: number | undefined;
  status: string | undefined;
  storeCode: string | undefined;
  storeName: string | undefined;
  uom: string | undefined;
}
