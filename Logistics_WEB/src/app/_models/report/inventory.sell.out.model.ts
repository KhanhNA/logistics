export class InventorySellOutModel {
  barcode: string | undefined;
  deliveryFrequency: string | undefined;
  id: number | undefined;
  inventory: number | undefined;
  packCode: string | undefined;
  packSize: number | undefined;
  productName: string | undefined;
  sellQuantity: number | undefined;
  sku: string | undefined;
  storeCode: string | undefined;
  storeName: string | undefined;
  uom: string | undefined;
}
