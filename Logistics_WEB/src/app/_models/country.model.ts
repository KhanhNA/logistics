import {FormGroup} from '@angular/forms';
import {throwError} from 'rxjs';

export class CountryModel {
  countryDescriptions: any[] | undefined;
  id: number | undefined;
  isocode: string | undefined;
  support: boolean | undefined;

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      // override
      throwError('not init object from Form')
    } else {
      this.id = form;
    }
  }
}
