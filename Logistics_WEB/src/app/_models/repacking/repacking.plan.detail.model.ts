import {SuperEntity} from "@next-solutions/next-solutions-base";
import {ProductPackingModel} from "../product.packing.model";
import {RepackingPlanDetailRepackedModel} from "./repacking.plan.detail.repacked.model";
import {RepackingPlanningModel} from "../repacking.planning.model";
import {RepackingPlanDetailPalletModel} from "./repacking.plan.detail.pallet.model";

export class RepackingPlanDetailModel extends SuperEntity {
  createDate?: string | undefined;
  createUser?: string | undefined;
  expireDate: string | undefined;
  productPacking: ProductPackingModel | undefined;
  quantity: number | undefined;
  repackingPlanning?: RepackingPlanningModel | undefined;
  repackingPlanningDetailPallets: RepackingPlanDetailPalletModel[] = [];
  repackingPlanningDetailRepackeds: RepackingPlanDetailRepackedModel[] = [];
  updateDate?: string | undefined;
  updateUser?: string | undefined;

  // not storage
  maxQuantity: number | undefined;
  totalQuantity = 0;
  orgQuantity = 0;
  claimQuantity = 0;
  repackingPlanningDetail: RepackingPlanDetailModel | undefined;
}
