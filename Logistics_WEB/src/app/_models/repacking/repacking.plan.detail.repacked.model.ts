import {ProductPackingModel} from "../product.packing.model";
import {SuperEntity} from "@next-solutions/next-solutions-base";
import {RepackingPlanDetailModel} from "./repacking.plan.detail.model";
import {PalletModel} from "../pallet/pallet.model";

export class RepackingPlanDetailRepackedModel extends SuperEntity {
  code: string | undefined;
  expireDate: string | undefined;
  productPacking: ProductPackingModel | undefined;
  qRCode: string | undefined;
  quantity: number | undefined;
  repackingPlanningDetail: RepackingPlanDetailModel | undefined;

  // not storage
  orgQuantity: number | undefined; // to display
  claimQuantity: number | undefined; // to display
  maxQuantity: number | undefined;
  pallet: PalletModel | undefined;
  palletId: number | undefined;
}
