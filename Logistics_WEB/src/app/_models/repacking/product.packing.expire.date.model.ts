import {ProductPackingModel} from "../product.packing.model";

export class ProductPackingExpireDateModel {
  className: string | undefined;
  expireDate: string | undefined;
  id: number | undefined
  productPacking: ProductPackingModel | undefined;
  quantity: number | undefined;
}
