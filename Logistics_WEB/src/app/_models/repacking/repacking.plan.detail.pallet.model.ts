import {SuperEntity} from "@next-solutions/next-solutions-base";
import {RepackingPlanDetailModel} from "./repacking.plan.detail.model";
import {PalletDetailModel} from "../pallet/pallet.detail.model";

export class RepackingPlanDetailPalletModel extends SuperEntity {
  palletDetail: PalletDetailModel | undefined;
  quantity: number | undefined;
  repackingPlanningDetail: RepackingPlanDetailModel | undefined;
}
