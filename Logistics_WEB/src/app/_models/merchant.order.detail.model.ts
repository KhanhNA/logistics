import {ProductPackingModel} from './product.packing.model';
import {DistributorModel} from './distributor.model';

export class MerchantOrderDetailModel {
  id: number | undefined;
  price: number | undefined;
  productPacking: ProductPackingModel | undefined;
  quantity: number | undefined;
  status: number | undefined;
  distributor: DistributorModel | undefined;
}
