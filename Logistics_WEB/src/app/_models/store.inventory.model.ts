import {ProductPackingModel} from "./product.packing.model";
import {ProductModel} from "./product.model";

export class StoreInventoryModel {
  productPackingId: number | undefined;
  expireDate: string | undefined;
  freeVolume: number | undefined;
  productPacking: ProductPackingModel | undefined;
  product: ProductModel | undefined;
  qRCode: string | undefined;
  totalQuantity: number | undefined;
}
