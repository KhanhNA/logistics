export class ProductListModel {
  id: number | undefined;
  storeCode: string | undefined;
  storeName: string | undefined;
  quantity: number | undefined;
  claimType: string | undefined;
  productPackingCode: string | undefined;
  productPackingName: string | undefined;
  expireDate: string | undefined;
  amenable: string | undefined;
}
