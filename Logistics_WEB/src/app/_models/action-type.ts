export class ActionTypeConstant {
  readonly new = 'add';
  readonly editPo = 'edit';
  readonly deliveryDate = 'deliveryDate';
  readonly GR = 'gr'; // Goods receive
  readonly Pal = 'Pal'; // pallet
  readonly PP = 'PPE'; // get product packing by expiredate
  readonly del = 'delete';
  readonly view = 'dashboard';
  readonly viewFromClaim = 'dashboard-from-claim';
  readonly clear = 'clear';
}

export enum ActionTypeEneum {
  clear = 'clear',
  new = 'add',
  editPo = 'edit',
  deliveryDate = 'deliveryDate',
  edit = 'edit',
  GR = 'gr', // Goods receive
  Pal = 'Pal', // pallet
  PP = 'PPE', // get product packing by expiredate
  del = 'delete',
  view = 'dashboard',
  view_from_claim = 'dashboard-from-claim'
}

export enum PoStatus {
  NEW = 'NEW',
  APPROVED = 'APPROVED',
  ARRIVED_VIETNAM_PORT = 'ARRIVED_VIETNAM_PORT',
  ARRIVED_MYANMAR_PORT = 'ARRIVED_MYANMAR_PORT',
  IMPORTED = 'IMPORTED',
  REJECT = 'REJECT'
}

export enum PoStatusDescription {
  NEW = 'po.status.new',
  APPROVED = 'po.status.approved',
  ARRIVED_VIETNAM_PORT = 'po.status.arrived.vietnam.port',
  ARRIVED_MYANMAR_PORT = 'po.status.arrived.myanmar.port',
  IMPORTED = 'po.status.gr',
  REJECT = 'po.status.approved.refuse'
}

export enum ImportPoStatus {
  NEW = 0,
  IN_PALLET = 1,
  OUT_OF_PALLET = 2
}


export enum RepackingStatusEnum {
  CANCEL = -1,
  NEW = 0,
  REPACKED = 1,
  IMPORTED = 2

}

export class RepackingStatusConst {
  readonly NEW = 0;
  readonly PLANNED = 3;
  readonly REPACKED = 1;
  readonly IMPORTED = 2;
  readonly CANCEL = -1;
}


export enum ExportStatementStatus {
  // Mới tạo lệnh xuất chưa thực xuất
  REQUESTED = 0,
  // Đã tạo lệnh thực xuất chưa có lệnh nhập tương ứng
  RELEASED = 1,
  // Đang được thực thi
  // 2.1: Đã tạo lệnh nhập nhưng chưa thực nhập,
  // 2.2: Tạo lệnh từ Merchant_order
  PROCESSING = 2,
  // Được hoàn thành thực nhập/thực xuất cho merchant
  COMPLETED = 3,
  // Lệnh bị hủy
  CANCELED = 4,
  // Lệnh bị trả lại từ phía nhận
  RETURNED = 5

}

export enum ImportStatementStatus {
  // Mới tạo lệnh nhập chưa thực nhập và chưa cộng kho
  NOT_UPDATE_INVENTORY = 0,
  // Đã tạo lệnh thực nhập và cộng kho
  UPDATED_INVENTORY = 1
}

export enum ClaimEnum {
  IMPORT_PO = 'IMPORT_PO',
  IMPORT_PO_UPDATE_PALLET = 'IMPORT_PO_UPDATE_PALLET',
  REPACKING_PLANNING = 'REPACKING_PLANNING',
  IMPORT_STATEMENT_REPACKING_PLANNING = 'IMPORT_STATEMENT_REPACKING_PLANNING',
  IMPORT_STATEMENT_EXPORT_STATEMENT = 'IMPORT_STATEMENT_EXPORT_STATEMENT',
  EXPORT_STATEMENT = 'EXPORT_STATEMENT',
  DELIVERY = 'DELIVERY',
  OUT_OF_DATE_PALLET_STEP_1 = 'OUT_OF_DATE_PALLET_STEP_1',
  OUT_OF_DATE_PALLET_STEP_2 = 'OUT_OF_DATE_PALLET_STEP_2',
  OTHER = 'OTHER',

}
