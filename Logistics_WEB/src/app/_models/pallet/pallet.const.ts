export const palletConst = {
  OUT_OF_DATE: {code: 'OUT_OF_DATE', label: 'palletv2.expireDateColor.OUT_OF_DATE', styleTable: 'viewData pending'},
  WARNING: {code: 'WARNING', label: 'palletv2.expireDateColor.WARNING', styleTable: 'approved'},
  STILL_EXPIRY_DATE: {
    code: 'STILL_EXPIRY_DATE',
    label: 'palletv2.expireDateColor.STILL_EXPIRY_DATE',
    styleTable: 'viewData rejected'
  }
};

export const palletStep = {
  1: {code: 1, label: 'palletv2.palletStep.receive'},
  2: {code: 2, label: 'palletv2.palletStep.storage'}
};
