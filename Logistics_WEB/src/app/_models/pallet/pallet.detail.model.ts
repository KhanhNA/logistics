import {SuperEntity} from '@next-solutions/next-solutions-base';
import {ProductPackingModel} from '../product.packing.model';
import {PalletModel} from './pallet.model';
import {FormGroup} from '@angular/forms';

export class PalletDetailModel extends SuperEntity {
  className: string | undefined;
  expireDate: string | undefined;
  productPacking: ProductPackingModel | undefined;
  quantity: number | undefined;
  pallet: PalletModel | undefined;

  constructor(form: FormGroup | number | undefined) {
    super();
    if (typeof form === 'number') {
      this.id = form;
    }
  }
}
