import {SuperEntity} from '@next-solutions/next-solutions-base';
import {StoreModel} from '../store.model';
import {FormGroup} from "@angular/forms";

export class PalletModel extends SuperEntity {
  code: string | undefined;
  name: string | undefined;
  qRCode: string | undefined;
  displayName: string | undefined;
  freeVolume: number | undefined;
  totalVolume: number | undefined;
  height: number | undefined;
  length: number | undefined;
  width: number | undefined;
  parentPallet: PalletModel | undefined;
  status: boolean | undefined;
  step: number | undefined;
  store: StoreModel | undefined;

  constructor(form: FormGroup | number) {
    super();
    if (typeof form === 'number') {
      this.id = form;
      return;
    }

    const fId = form.get('id');
    if (fId?.value) {
      this.id = fId.value;
    }
    const fCode = form.get('code');
    if (fCode?.value) {
      this.code = fCode.value;
    }

    const fName = form.get('name');
    if (fName?.value) {
      this.name = fName.value;
    }
    const fStep = form.get('step');
    if (fStep?.value) {
      this.step = fStep.value;
    }
    const fHeight = form.get('height');
    if (fHeight?.value) {
      this.height = fHeight.value;
    }
    const fWidth = form.get('width');
    if (fWidth?.value) {
      this.width = fWidth.value;
    }
    const fLength = form.get('length');
    if (fLength?.value) {
      this.length = fLength.value;
    }
    const fStore = form.get('store');
    if (fStore?.value) {
      this.store = new StoreModel(fStore.value);
    }
    const fStatus = form.get('status');
    if (fStatus?.value) {
      this.status = isNaN(Number(fStatus.value)) ? false : !!Number(fStatus.value);
    }
  }
}
