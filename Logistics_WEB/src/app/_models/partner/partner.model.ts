import {FormGroup} from '@angular/forms';
import {SuperEntity} from "@next-solutions/next-solutions-base";
import {FileModel} from "./file.model";

export class PartnerModel extends SuperEntity {
  acceptDate?: string;

  acceptUser?: string;
  address?: string;
  contractCode?: string;
  createDate?: string;
  // example: yyyy-MM-dd HH:mm:ss.SSS'Z'
  createUser?: string;
  description?: string;
  email?: string;
  name?: string;
  phone?: string;
  rejectReason?: string;
  shippingPartnerFiles?: FileModel[];
  status?: string;
  // Enum:
  //   Array [ 3 ]
  taxCode?: string;
  updateDate?: string;
  // example: yyyy-MM-dd HH:mm:ss.SSS'Z'
  updateUser?: string;

  constructor(form: FormGroup | number) {
    super();
    if (typeof form === 'number') {
      this.id = form;
      return;
    }
    let ctrl = form.get('name');
    this.name = ctrl ? ctrl.value : '';

    ctrl = form.get('phone');
    this.phone = ctrl ? ctrl.value : '';

    ctrl = form.get('email');
    this.email = ctrl ? ctrl.value : '';

    ctrl = form.get('contractCode');
    this.contractCode = ctrl ? ctrl.value : '';

    ctrl = form.get('taxCode');
    this.taxCode = ctrl ? ctrl.value : '';

    ctrl = form.get('address');
    this.address = ctrl ? ctrl.value : '';

    ctrl = form.get('description');
    this.description = ctrl ? ctrl.value : '';
    this.shippingPartnerFiles = [];




  }
}
