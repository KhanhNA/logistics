import {SuperEntity} from '@next-solutions/next-solutions-base';
import {FormGroup} from '@angular/forms';
import {PartnerModel} from './partner.model';

export class FileModel extends SuperEntity {
  createDate?: string;
  // example: yyyy-MM-dd HH:mm:ss.SSS'Z'
  createUser?: string;
  description?: string;

  shippingPartner?: PartnerModel;
  updateDate?: string;
// example: yyyy-MM-dd HH:mm:ss.SSS'Z'
  updateUser?: string;
  url?: string;

  constructor(form: FormGroup | number) {
    super();
    if (typeof form === 'number'){
      this.id = form;
    }
    return;
  }
}
