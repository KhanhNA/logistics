import {PartnerModel} from "./partner.model";

export const partnerStatus = {
  _: {code: '_', label: 'partner.status.All', styleTable: 'viewData pending'},
  PENDING: {code: 'PENDING', label: 'partner.status.PENDING', styleTable: 'viewData pending'},
  APPROVED: {code: 'APPROVED', label: 'partner.status.APPROVED', styleTable: 'approved'},
  REJECTED: {code: 'REJECTED', label: 'partner.status.REJECTED', styleTable: 'viewData rejected'}
};

export const partnerStatusAction = {
  isPendingStatus(partner: PartnerModel | undefined) {
    return !!partner && partnerStatus[partner.status + ''] === partnerStatus.PENDING;
  },
  isApprovedStatus(partner: PartnerModel | undefined) {
    return !!partner && partnerStatus[partner.status + ''] === partnerStatus.APPROVED;
  },
  isRejectedStatus(partner: PartnerModel | undefined) {
    return !!partner && partnerStatus[partner.status + ''] === partnerStatus.REJECTED;
  },
};

export const message = {
  onSave: {
    dlgType: 'showConfirmDialog',
    confirmMessage: 'partner.message.save.confirmTitle',
    success: 'partner.message.save.success'
  },
  onApprove: {
    dlgType: 'showConfirmDialog',
    confirmMessage: 'partner.message.approve.confirmTitle',
    success: 'partner.message.approve.success'
  },
  onReject: {
    dlgType: 'showConfirmInputDialog',
    confirmMessage: 'partner.message.reject.confirmTitle',
    success: 'partner.message.reject.success'
  },

}
