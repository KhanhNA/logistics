export enum MerchantOrderDetailStatusEnum {
  _0 = 'merchant.order.status.not.enough.quantity',
  _1 = 'merchant.order.status.waiting.for.import',
  _2 = 'merchant.order.status.enough.quantity',
}
