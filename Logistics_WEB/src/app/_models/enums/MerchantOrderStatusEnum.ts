export enum MerchantOrderStatusEnum {
  _ = 'merchant.order.status.all',
  _0 = 'merchant.order.status.not.enough.quantity',
  _1 = 'merchant.order.status.enough.quantity',
  _2 = 'merchant.order.status.exported',
  _3 = 'merchant.order.status.canceled',
  _4 = 'merchant.order.status.returned',
  _5 = 'merchant.order.status.waiting.for.import',
}
