export enum ClaimStatusEnum {
  _ = 'claim.status.all',
  _0 = 'claim.status.wait',
  _1 = 'claim.status.approved',
  _2 = 'claim.status.rejected',
  _3 = 'claim.status.completed',

}
