export enum ProductListStatusEnum {
  _APPROVED = 'common.status.product-list.approved',
  _COMPLETED = 'common.status.product-list.completed'
}
