export enum ClaimTypeEnum {
  _ = 'claim.type.all',
  _IMPORT_PO = 'claim.type.importPo',
  _IMPORT_PO_UPDATE_PALLET = 'claim.type.importPoUpdatePallet',
  _REPACKING_PLANNING = 'claim.type.repackingPlanning',
  _IMPORT_STATEMENT_REPACKING_PLANNING = 'claim.type.importStatement.repacking.plan',
  _IMPORT_STATEMENT_EXPORT_STATEMENT = 'claim.type.importStatement.export.statement',
  _EXPORT_STATEMENT = 'claim.type.exportStatement',
  _DELIVERY = 'claim.type.delivery',
  _OUT_OF_DATE_PALLET_STEP_1 = 'claim.type.outOfDatePalletStep1',
  _OUT_OF_DATE_PALLET_STEP_2 = 'claim.type.outOfDatePalletStep2',
  _OTHER = 'claim.type.other',
}
