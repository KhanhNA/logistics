export enum StoreStatusEnum {
  _ = 'common.status.all',
  _true = 'common.status.active',
  _false = 'common.status.deactive',
}
