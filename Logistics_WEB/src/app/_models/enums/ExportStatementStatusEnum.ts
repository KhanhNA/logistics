export enum ExportStatementStatusEnum {
  _0 = 'exportStatementStatus.requested',
  // _1 = 'exportStatementStatus.released',
  _2 = 'exportStatementStatus.beingtransported',
  _3 = 'exportStatementStatus.completed',
  _4 = 'exportStatementStatus.canceled',
  // _5 = 'exportStatementStatus.returned',
}
