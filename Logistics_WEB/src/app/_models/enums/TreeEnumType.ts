export enum TreeEnumType {
  REGION = 'REGION',
  TOWNSHIP = 'TOWNSHIP',
  DISTRICT = 'DISTRICT'
}
