import {PoModel} from "./po.model";
import {PoStatus} from "../action-type";

export const poConst = {
  NEW: {code: 'NEW', label: 'po.status.new', styleTable: 'viewData pending'},
  APPROVED: {code: 'APPROVED', label: 'po.status.approved', styleTable: 'approved'},
  ARRIVED_VIETNAM_PORT: {
    code: 'ARRIVED_VIETNAM_PORT',
    label: 'po.status.arrived.vietnam.port',
    styleTable: 'approved',
    expect_uri: '/arrive-vietnam-port',
    actual_uri: '/arrived-vietnam-port',
  },
  ARRIVED_MYANMAR_PORT: {
    code: 'ARRIVED_MYANMAR_PORT',
    label: 'po.status.arrived.myanmar.port',
    styleTable: 'approved',
    expect_uri: '/arrive-myanmar-port',
    actual_uri: '/arrived-myanmar-port',
  },
  IMPORTED: {
    code: 'IMPORTED', label: 'po.status.gr', styleTable: 'approved',
    expect_uri: '/arrive-fc',
    actual_uri: '',

  },
  REJECT: {code: 'REJECT', label: 'po.status.approved.refuse', styleTable: 'viewData rejected'}
};

export const poStatusAction = {
  isNewStatus(po: PoModel | undefined) {
    return !!po && PoStatus[po.status + ''] === PoStatus.NEW;
  },
  isArrivedVietnamPortStatus(po: PoModel | undefined) {
    return !!po && PoStatus[po.status + ''] === PoStatus.ARRIVED_VIETNAM_PORT;
  },
  isArrivedMyanmarPortStatus(po: PoModel | undefined) {
    return !!po && PoStatus[po.status + ''] === PoStatus.ARRIVED_MYANMAR_PORT;
  },
  isApprovedStatus(po: PoModel | undefined) {
    return !!po && PoStatus[po.status + ''] === PoStatus.APPROVED;
  },
  isRejetedStatus(po: PoModel | undefined) {
    return !!po && PoStatus[po.status + ''] === PoStatus.REJECT;
  },
  isImportedStatus(po: PoModel | undefined) {
    return !!po && PoStatus[po.status + ''] === PoStatus.IMPORTED;
  }
}

export const message = {
  onSave: {
    dlgType: 'showConfirmDialog',
    confirmMessage: 'po.message.save.confirmTitle',
    success: 'po.message.save.success'
  },
  onApprove: {
    dlgType: 'showConfirmDialog',
    confirmMessage: 'po.message.approve.confirmTitle',
    success: 'po.message.approve.success'
  },
  onReject: {
    dlgType: 'showConfirmInputDialog',
    confirmMessage: 'po.message.reject.confirmTitle',
    success: 'po.message.reject.success'
  },
}
