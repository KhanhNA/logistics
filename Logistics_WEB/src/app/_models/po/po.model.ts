import {UserModel} from '../user.model';
import {DistributorModel} from '../distributor.model';
import {ManufacturerModel} from '../manufacturer.model';
import {StoreModel} from '../store.model';
import {CurrencyModel} from '../currency.model';
import {PoDetailModel} from './po.detail.model';
import {FormGroup} from '@angular/forms';
import {PoStatus} from "../action-type";
import {ProductPackingModel} from "../product.packing.model";
import {ProductPackingPriceModel} from "../product.packing.price.model";

export class PoModel {
  approveDate: string | undefined;
  approveUser: string | undefined;
  approveUserObj: UserModel | undefined;
  code: string | undefined;
  createDate: string | undefined;
  createUser: string | undefined;
  createUserObj: UserModel | undefined;
  arriveVietnamPortDate?: string;
  arriveMyanmarPortDate?: string;
  arriveFcDate?: string;
  deliveryAddress: string | undefined;
  arrivedVietnamPortDate: string | undefined;
  arrivedMyanmarPortDate: string | undefined;
  arrivedFcDate: string | undefined;

  arrivedFcDateString: string | undefined;
  arrivedMyanmarPortDateString: string | undefined;
  arrivedVietnamPortDateString: string | undefined;
  arrivedFcUser: string | undefined;
  arrivedFcUserObj: UserModel | undefined;
  arrivedMyanmarPortUser: string | undefined;
  arrivedMyanmarPortUserObj: UserModel | undefined;
  arrivedVietnamPortUser: string | undefined;
  arrivedVietnamPortUserObj: UserModel | undefined;

  description: string | undefined;
  distributor: DistributorModel | undefined;
  exchangeRate: number | undefined;
  id: number | null | undefined;
  importDate: string | undefined;
  importPo: any | undefined;
  importUser: string | undefined;
  importUserObj: UserModel | undefined;
  manufacturer: ManufacturerModel | undefined;
  poDetails: PoDetailModel[] | undefined;
  qRCode: string | undefined;
  rejectReason: string | undefined;
  status: string | undefined | PoStatus;
  statusString: string | undefined;
  store: StoreModel | undefined;
  fromCurrency: CurrencyModel | undefined;
  toCurrency: CurrencyModel | null | undefined;
  total?: number;
  createDateString?: string;

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      if (form.get('id')) {
        this.id = form.get('id')?.value;
      }
      const fManufacturer = form.get('manufacturer')?.value;
      this.manufacturer = fManufacturer ? new ManufacturerModel(fManufacturer) : undefined;
      const fStore = form.get('store')?.value;
      this.store = fStore ? new StoreModel(fStore) : undefined;
      const fDistributor = form.get('distributor')?.value;
      this.distributor = fDistributor ? new DistributorModel(fDistributor) : undefined;

      const fDeliveryAddress = form.get('deliveryAddress')?.value;
      this.deliveryAddress = fDeliveryAddress ? fDeliveryAddress : '';

      const fExchangeRate = form.get('exchangeRate')?.value;
      if (fExchangeRate) {
        this.exchangeRate = Number(fExchangeRate);
      } else {
        this.exchangeRate = undefined;
      }

      const fFromCurrency = form.get('fromCurrency')?.value;
      this.fromCurrency = fFromCurrency ? new CurrencyModel(fFromCurrency) : undefined;
      const fToCurrency = form.get('toCurrency')?.value;
      this.toCurrency = fToCurrency ? new CurrencyModel(fToCurrency) : null;

      const fDescription = form.get('description')?.value;
      this.description = fDescription ? fDescription : '';

      const fArriveVNDate = form.get('arriveVietnamPortDate')?.value;
      this.arriveVietnamPortDate = fArriveVNDate ? fArriveVNDate : '';

      const fArrivedVNDate = form.get('arrivedVietnamPortDate')?.value;
      this.arrivedVietnamPortDate = fArrivedVNDate ? fArrivedVNDate : '';

      const arriveMyanmarPortDate = form.get('arriveMyanmarPortDate')?.value;
      this.arriveMyanmarPortDate = arriveMyanmarPortDate ? arriveMyanmarPortDate : '';

      const arrivedMyanmarPortDate = form.get('arrivedMyanmarPortDate')?.value;
      this.arrivedMyanmarPortDate = arrivedMyanmarPortDate ? arrivedMyanmarPortDate : '';

      const arriveFcDate = form.get('arriveFcDate')?.value;
      this.arriveFcDate = arriveFcDate ? arriveFcDate : '';


      this.poDetails = form.get('poDetails')?.value ? form.get('poDetails')?.value : [];
      // this.poDetails?.forEach(detail => {
      //   detail.productPacking = detail.productPacking ? new ProductPackingModel(detail.productPacking.id) : undefined;
      //   detail.productPackingPrice = detail.productPackingPrice ? new ProductPackingPriceModel(detail.productPackingPrice.id) : undefined;
      //   detail.productPackingPrices?.forEach(price => {
      //     if (price) {
      //       price = new ProductPackingPriceModel(price.id);
      //     }
      //   });
      // });

      this.status = form.get('status')?.value;
    } else {
      this.id = form;
    }
  }
}
