import {PoModel} from './po.model';
import {ProductModel} from '../product.model';
import {ProductPackingModel} from '../product.packing.model';
import {ProductPackingPriceModel} from '../product.packing.price.model';
import {ManufacturerModel} from "../manufacturer.model";
import {DistributorModel} from "../distributor.model";
import {ProductDescriptionModel} from "../product.description.model";

export class PoDetailModel {
  createDate: string | undefined;
  createUser: string | undefined;
  id: number | undefined;
  po: PoModel | undefined;
  product: ProductModel | undefined;
  productPacking: ProductPackingModel | undefined;
  productPackingPrice: ProductPackingPriceModel | undefined;
  productPackingPrices: ProductPackingPriceModel[] | undefined;
  quantity: number | undefined;
  vat: number | undefined;

  // not storage
  maxQuantity: number | undefined;
  poDetail: PoDetailModel | undefined | any;

  static reduceCircleReference(poDetail: PoDetailModel | undefined) {
    if (poDetail) {
      poDetail.po = poDetail.po?.id ? new PoModel(poDetail.po.id) : undefined;

    }
  }
}
