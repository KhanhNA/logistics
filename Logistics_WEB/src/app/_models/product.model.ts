import {ProductDescriptionModel} from './product.description.model';
import {ManufacturerModel} from './manufacturer.model';
import {DistributorModel} from './distributor.model';
import {SuperEntity} from '@next-solutions/next-solutions-base';
import {ProductPackingPriceModel} from './product.packing.price.model';
import {FormGroup} from "@angular/forms";
import {ProductPackingModel} from "./product.packing.model";

export class ProductModel extends SuperEntity {
  name: string | undefined;

  productDescriptions: ProductDescriptionModel[] = [];
  lifecycle: number | undefined;
  manufacturer: ManufacturerModel | undefined;

  code: string | undefined;
  distributor: DistributorModel | undefined;
  height: number | undefined;
  // hot: null
  length: number | undefined;
  // popular: null
  productPackingPrices: ProductPackingPriceModel[] = [];
  productPackings: ProductPackingModel[] = [];
  productType: any;
  // quantityOrdered: null
  sku: string | undefined;
  // sortOrder: null
  vat: number | undefined;
  warningThreshold: number | undefined;
  weight: number | undefined;
  width: number | undefined;

  constructor(form: number | undefined | FormGroup) {
    super();
    if (form) {
      if (typeof form === 'number') {
        this.id = form;
      }
    }
  }

  static reduceCircleReference(product: ProductModel | undefined) {
    if (product) {
      product.manufacturer = product.manufacturer?.id ? new ManufacturerModel(product.manufacturer.id) : undefined;
      product.distributor = product.distributor?.id ? new DistributorModel(product.distributor?.id) : undefined;
      product.productPackings?.forEach(pp => {
        ProductPackingModel.reduceCircleReference(pp);
      });
      product.productDescriptions?.forEach(des => {
        ProductDescriptionModel.reduceCircleReference(des);
      });
      product.productPackingPrices?.forEach(price => {
        ProductPackingPriceModel.reduceCircleReference(price);
      });

    }
  }
}
