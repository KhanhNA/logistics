export class PackingTypeModel {
  id: number | undefined;
  code: string | undefined;
  quantity: number | undefined;
  status: boolean | undefined;
}
