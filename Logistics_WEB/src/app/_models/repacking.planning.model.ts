import {FormGroup} from '@angular/forms';
import {UserModel} from './user.model';
import {DistributorModel} from './distributor.model';
import {ImportStatementModel} from './import-statement/import.statement.model';
import {StoreModel} from './store.model';
import {RepackingPlanDetailModel} from './repacking/repacking.plan.detail.model';
import {ProductPackingModel} from "./product.packing.model";

export class RepackingPlanningModel {
  id: number | undefined;
  code: string | undefined;
  createDate: string | undefined;
  createUser: string | undefined;
  createUserObj: UserModel | undefined;
  description: string | undefined;
  distributor: DistributorModel | undefined;
  importStatement: ImportStatementModel | undefined;
  importedDate: string | undefined;
  planningDate: string | undefined;
  repackedDate: string | undefined;
  repackingPlanningDetails: RepackingPlanDetailModel [] = [];
  status: number | undefined;
  store: StoreModel | undefined;
  updateDate: string | undefined;
  updateUser: string | undefined;

  constructor(form: FormGroup | number) {
    if (typeof form === 'number') {
      this.id = form;
    } else if (form instanceof FormGroup) {
      const rawValue: RepackingPlanningModel = form.getRawValue();
      this.id = rawValue.id;
      this.code = rawValue.code;
      this.description = rawValue.description;
      this.distributor = isNaN(Number(rawValue.distributor)) ? undefined : new DistributorModel(Number(rawValue.distributor));
      this.store = isNaN(Number(rawValue.store)) ? undefined : new StoreModel(Number(rawValue.store));
      this.repackingPlanningDetails = rawValue.repackingPlanningDetails.map(detail => {
        return {
          quantity: detail.quantity,
          productPacking: new ProductPackingModel(detail.productPacking?.id),
          expireDate: detail.expireDate
        } as RepackingPlanDetailModel;
      });
      this.status = isNaN(Number(rawValue.status)) ? 0 : Number(rawValue.status);
    }
  }
}
