import {FormGroup} from "@angular/forms";

export class CurrencyModel {
  code: string | undefined;
  id: number | undefined;
  name: string | undefined;
  support: boolean | undefined;

  constructor(form: FormGroup | number) {
    if (form){
      if (form instanceof FormGroup) {
        this.id = form.get('id')?.value;
        const fCode = form.get('code')?.value;
        this.code = fCode ? fCode : '';
        const fName = form.get('name')?.value;
        this.name = fName ? fName : '';
        const fSupport = form.get('support')?.value;
        this.support = fSupport ? fSupport : true;
      } else {
        this.id = form;
      }
    }
  }
}
