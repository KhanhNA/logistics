export interface ColumnType {
  name: string;
  type?: 1|2|3|4|5|6|7|8;
  data?: any;
}

export class ColumnTypeConstant {
  // khong duoc sua gia tri
  readonly string = 1;
  readonly date = 2;
  readonly number = 3;
  readonly currency = 4;
  readonly img = 5;
  readonly check = 6;
  readonly btn = 7;
  readonly index = 8;
}

export enum ColumnTypeEnum {
  // khong duoc sua gia tri
  string = 1,
  date = 2,
  number = 3,
  currency = 4,
  img = 5,
  check = 6,
  btn = 7,
  index = 8,
}
