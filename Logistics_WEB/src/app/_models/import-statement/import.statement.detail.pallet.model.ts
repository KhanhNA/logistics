import {PalletModel} from "../pallet/pallet.model";
import {PalletDetailModel} from "../pallet/pallet.detail.model";
import {StoreProductPackingDetailModel} from "../store/store.product.packing.detail.model";
import {ProductPackingModel} from "../product.packing.model";
import {SuperEntity} from "@next-solutions/next-solutions-base";

export class ImportStatementDetailPalletModel extends SuperEntity{
  createDate: string | undefined;
  createUser: string | undefined;
  expireDate: string | undefined;
  id: number | undefined;
  importStatementDetailId: number | undefined;
  importStatementId: number | undefined;
  pallet: PalletModel | undefined;
  palletDetail: PalletDetailModel | undefined;
  productPackingId: number | undefined;
  quantity: number | undefined;
  storeProductPackingDetail: StoreProductPackingDetailModel | undefined;
  storeProductPackingDetailId: number | undefined;
  updateDate: string | undefined;
  updateUser: string | undefined;

  // not storage
  productPacking: ProductPackingModel | undefined;
  // not storage
  totalQuantity: number | undefined;
  orgQuantity: number | undefined;
  claimQuantity: number | undefined;
  palletId: number | undefined;
}
