import {ExportStatementModel} from '../export-statement/export.statement.model';
import {StoreModel} from '../store.model';
import {RepackingPlanningModel} from '../repacking.planning.model';
import {ImportStatementDetailPalletModel} from "./import.statement.detail.pallet.model";
import {ImportStatementDetailModel} from "./import.statement.detail.model";

export class ImportStatementModel {
  id: number | undefined;
  code: string | undefined;
  description: string | undefined;
  estimatedTimeOfArrival: string | undefined;
  exportStatement: ExportStatementModel | undefined;
  fromStore: StoreModel | undefined;
  toStore: StoreModel | undefined;
  importDate: string | undefined;
  qRCode: string | undefined;
  repackingPlanning: RepackingPlanningModel | undefined;
  status: number | undefined;
  totalQuantity: number | undefined;
  totalVolume: number | undefined;

  importStatementDetailPallets: ImportStatementDetailPalletModel[] = []
  importStatementDetails: ImportStatementDetailModel[] = [];
}
