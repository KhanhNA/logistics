import {SuperEntity} from '@next-solutions/next-solutions-base';

export class ImportStatementTable extends SuperEntity {
  packageCode?: string;
  qrCode?: string;
  productName?: string;
  expireDate?: string;
  packingType?: number;
  quantity?: number;
  pallet?: string;
}
