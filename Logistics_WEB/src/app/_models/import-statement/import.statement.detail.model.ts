import {ImportStatementModel} from "./import.statement.model";
import {ProductPackingModel} from "../product.packing.model";
import {ImportStatementDetailPalletModel} from "./import.statement.detail.pallet.model";

export class ImportStatementDetailModel {
  createDate: string | undefined;
  createUser: string | undefined;
  expireDate: string | undefined;
  id: number | undefined;
  importDate: string | undefined;
  importStatement: ImportStatementModel | undefined;
  importStatementDetailPallets: ImportStatementDetailPalletModel[] = [];
  productPacking: ProductPackingModel | undefined;
  quantity: number | undefined;
  updateDate: string | undefined;
  updateUser: string | undefined;
}
