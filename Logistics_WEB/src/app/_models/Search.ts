import {environment} from "../../environments/environment";

export class Search {
  text: string;
  pageSize: number;
  pageNumber: number;
  totalElements: number | undefined;
  method: any;

  constructor() {
    this.text = '';
    this.pageSize = environment.PAGE_SIZE;
    this.pageNumber = 0;
  }
}
