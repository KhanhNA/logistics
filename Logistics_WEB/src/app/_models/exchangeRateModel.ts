import {CurrencyModel} from './currency.model';
import {FormGroup} from '@angular/forms';

export class ExchangeRateModel {
  createDate: string | undefined;
  createUser: string | undefined;
  exchangeRate: number | undefined;
  fromCurrency: CurrencyModel | undefined;
  fromDate: string | undefined;
  id: number;
  toCurrency: CurrencyModel | undefined;
  toDate: string | undefined;
  updateDate: string | undefined;
  updateUser: string | undefined;

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      this.id = form.get('id')?.value;
      this.exchangeRate = form.get('exchangeRate')?.value;
      this.fromCurrency = new CurrencyModel(form.get('fromCurrency')?.value);
      this.toCurrency = new CurrencyModel(form.get('toCurrency')?.value);
      this.fromDate = form.get('fromDate')?.value;
      this.toDate = form.get('toDate')?.value;
    } else {
      this.id = form;
    }
  }

}
