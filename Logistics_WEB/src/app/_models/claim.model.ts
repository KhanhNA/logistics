import {FormGroup} from '@angular/forms';
import {ClaimDetailModel} from './claim.detail.model';
import {ClaimImageModel} from './claim.image.model';

export class Claim {
  id: number | undefined;
  code: string | undefined;
  storeId: string | undefined;
  store: any | undefined;
  type: string | undefined;
  referenceId: string | undefined;
  amenable: string | undefined;
  status: number | undefined;
  description: string | undefined;
  claimDetails: ClaimDetailModel[] | undefined;
  claimImages: ClaimImageModel[] | undefined;

  constructor(form: FormGroup) {
    this.storeId = form.get('storeId')?.value;
    this.type = form.get('type')?.value;
    this.amenable = form.get('amenable')?.value;
    this.description = form.get('description')?.value;
    this.status = form.get('status')?.value;
  }
}
