import {ProductModel} from './product.model';
import {PackingTypeModel} from './packing.type.model';
import {SuperEntity} from '@next-solutions/next-solutions-base';
import {DistributorModel} from './distributor.model';
import {ProductPackingPriceModel} from "./product.packing.price.model";
import {FormGroup} from "@angular/forms";
import {ManufacturerModel} from "./manufacturer.model";
import {ProductDescriptionModel} from "./product.description.model";

export class ProductPackingModel extends SuperEntity {
  id: number | undefined;
  code: string | undefined;
  name: string | undefined;
  product: ProductModel | undefined;
  packingType: PackingTypeModel | undefined;
  uom: string | undefined;
  vat: number | undefined;
  distributor: DistributorModel | undefined;
  productPackingPrices: ProductPackingPriceModel[] = [];
  barCode: string | undefined | null;
  barcode: string | undefined | null;
  weight: number | undefined;
  width: number | undefined;
  length: number | undefined;
  height: number | undefined;
  volume: number | undefined;

  // not storage
  quantity: number | undefined;

  constructor(form: FormGroup | number | undefined) {
    super();
    if (form) {

      if (typeof form === 'number') {
        this.id = form;
      }
    }

  }

  static reduceCircleReference(packing: ProductPackingModel | undefined) {
    if (packing) {
      packing.product = packing.product?.id ? new ProductModel(packing.product.id) : undefined;
      packing.distributor = packing.distributor?.id ? new DistributorModel(packing.distributor?.id) : undefined;
      packing.productPackingPrices?.forEach(price => {
        ProductPackingPriceModel.reduceCircleReference(price);
      });

    }
  }
}
