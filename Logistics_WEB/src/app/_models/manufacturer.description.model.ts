import {LanguageModel} from './language.model';
import {ManufacturerModel} from './manufacturer.model';

export class ManufacturerDescriptionModel {
  address: string | undefined;
  id: number | undefined;
  language: LanguageModel | undefined;
  manufacturer: ManufacturerModel | undefined;
  name: string | undefined;
}
