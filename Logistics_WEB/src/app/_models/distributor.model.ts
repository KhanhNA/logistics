import {FormGroup} from '@angular/forms';

export class DistributorModel {
  id: number;
  code: string | undefined;
  name: string | undefined;
  phoneNumber: string | undefined;
  email: string | undefined;
  address: string | undefined;
  aboutUs: string | undefined;
  enabled: boolean | undefined;

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      this.id = form.get('id')?.value;
      this.code = form.get('code')?.value;
      this.name = form.get('name')?.value;
    } else {
      this.id = form;
    }
  }
}
