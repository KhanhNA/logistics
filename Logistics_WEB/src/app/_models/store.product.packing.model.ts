import {SuperEntity} from '@next-solutions/next-solutions-base';
import {StoreModel} from './store.model';
import {ProductPackingModel} from './product.packing.model';
import {ProductModel} from './product.model';

export class StoreProductPackingModel extends SuperEntity {
  store: StoreModel | undefined;
  productPackingId: number | undefined;
  productPacking: ProductPackingModel | undefined;
  product: ProductModel | undefined;
  totalQuantity: number | undefined;
  expireDate: string | undefined;
  className: string | undefined;
  code: string | undefined;
  qRCode: string | undefined;
}
