import {FormGroup, Validators} from '@angular/forms';
import {CurrencyModel} from './currency.model';
import {UserModel} from './user.model';
import {LanguageModel} from './language.model';
import {LocationTreeModel} from './LocationTreeModel';
import {CountryModel} from './country.model';
import {StoreLocationChargeModel} from "./store-location-charge.model";
import {SelectModel} from "@next-solutions/next-solutions-base";
import {FileModel} from "./partner/file.model";

export class StoreModel {
  id: number | null = null;
  status: boolean | any = null;
  code: string | null = null;
  name: string | null = null;
  domainName: string | null = null;
  address: string | null = null;
  district: LocationTreeModel | undefined;
  region: LocationTreeModel | undefined;
  township: LocationTreeModel | undefined;
  phone: string | null = null;
  dc: boolean | null = null;
  currency: CurrencyModel | undefined;
  inBusinessSince: string | undefined;
  outBusinessSince: string | undefined;

  provideStore: StoreModel | undefined;
  country: CountryModel | undefined;
  floorNo: number | undefined;
  area: number | undefined;
  lat: number | undefined;
  lng: number | undefined;
  email: string | undefined;
  user: UserModel | undefined;
  language: LanguageModel | undefined;
  logo: string | undefined;
  postalCode: string | undefined;
  storeLocationCharges: StoreLocationChargeModel[] = [];
  width?: number;
  length?: number;
  height?: number;
  storeFiles?: FileModel[];
  deliveryFrequency?: number;
  // value cua location luc edit. anh xa bang ten cua control tren form
  locationCharges?: (string | any)[] = [];

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      this.id = form.get('id')?.value;
      this.status = form.get('status')?.value === '1';
      this.code = form.get('code')?.value;
      this.name = form.get('name')?.value;
      this.inBusinessSince = form.get('inBusinessSince')?.value;
      this.outBusinessSince = form.get('outBusinessSince') && form.get('outBusinessSince')?.value ?
        form.get('outBusinessSince')?.value : null;
      this.dc = form.get('dc')?.value === '1';
      this.provideStore = form.get('provideStore') && form.get('provideStore')?.value ?
        new StoreModel(form.get('provideStore')?.value) : undefined;
      this.currency = new CurrencyModel(form.get('currency')?.value);

      this.country = new CountryModel(form.get('country')?.value);
      this.region = new LocationTreeModel(form.get('region')?.value);
      this.district = new LocationTreeModel(form.get('district')?.value);
      this.township = new LocationTreeModel(form.get('township')?.value);
      this.address = form.get('address')?.value;
      this.floorNo = form.get('floorNo') && form.get('floorNo')?.value ? form.get('floorNo')?.value : '';
      this.area = form.get('area')?.value;
      this.lat = form.get('lat')?.value;
      this.lng = form.get('lng')?.value;

      this.email = form.get('email')?.value;
      this.phone = form.get('phone')?.value;
      this.postalCode = form.get('postalCode') && form.get('postalCode')?.value ? form.get('postalCode')?.value : '';
      this.domainName = form.get('domainName') && form.get('domainName')?.value ? form.get('domainName')?.value : '';
      this.length = form.get('length')?.value;
      this.width = form.get('width')?.value;
      this.height = form.get('height')?.value;
      this.deliveryFrequency = form.get('deliveryFrequency')?.value;
      // this.user = form.get('user')?.value ?  new UserModel(form.get('user')?.value) : null;
      this.language = new LanguageModel(form.get('language')?.value);
      this.storeLocationCharges = [];
      if (this.dc){
        // cần set tay bên ngoài để chuyển code thành id, value form đang là code
        const storeLocationCharges = form.get('locationCharges')?.value ? form.get('locationCharges')?.value : [];
        storeLocationCharges.forEach((value: number) => {
          this.storeLocationCharges?.push(({
            countryLocation: {id: value} as LocationTreeModel,
            store: this.id ? new StoreModel(this.id) : undefined
          }) as StoreLocationChargeModel);
        });
      }

    } else {
      this.id = form;
    }
  }
}
