import {TranslateService} from '@ngx-translate/core';

import {PhoneInputFormat} from '../phone-input-format';
import {SelectModel, UploadModel} from '@next-solutions/next-solutions-base';
import {AbstractControl} from "@angular/forms";
import {NumericInputFormat} from "../numeric-input-format";

export class ModelUtils {

  static getInstance(): ModelUtils {
    return new ModelUtils();
  }


  getSelectModel(modelConst: any) {
    const select: SelectModel[] = [];
    Object.keys(modelConst).forEach(key => {
      select.push(new SelectModel(key, modelConst[key].label));
    });
    return select;
  }

  formatPhoneNumber(phoneNumberString?: string): string {
    if (!phoneNumberString) {
      return '';
    }
    const format = new PhoneInputFormat();
    return format.formatValue(phoneNumberString);
  }

  formatNumber(num: any) {
    if (!num) {
      return '';
    }
    const format = new NumericInputFormat();
    return format.formatValue(num);
  }

  addElementAll(translateService: TranslateService, displayKey?: any): SelectModel {
    if (!displayKey || displayKey === '') {
      displayKey = 'common.all';
    }
    return new SelectModel('-1000', translateService.instant(displayKey));
  }

  getSelectAllValue() {
    return '-1000';
  }

  getFileList(control: AbstractControl, param: string, formData: FormData): number[] {
    const files = control?.value;
    if (!files || !files.length) {
      return [];
    }
    const returnFiles: number[] = [];
    for (let i = 0; i < files.length; i++) {
      const upload = files[i].imageUrl as UploadModel;
      if (!upload) {
        continue;
      }
      if (upload.binary) {
        formData.append(param, upload.binary, upload.name);
      } else if (upload.id) {
        returnFiles.push(upload.id);
      }
    }
    return returnFiles;
  }
}
