import {FormGroup} from '@angular/forms';
import {DistributorModel} from './distributor.model';
import {StoreModel} from './store.model';

export class UserModel {
  id: number | undefined;
  username: string | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  tel: string | undefined;
  email: string | undefined;
  status: boolean | undefined;
  role: any | undefined;
  distributorId: number | undefined;
  distributor: DistributorModel | undefined;
  stores: any[] | any | undefined;
  storeIds: any[] | any | undefined;

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      this.username = form.get('username')?.value;
      this.firstName = form.get('firstName')?.value;
      this.lastName = form.get('lastName')?.value;
      this.tel = form.get('tel')?.value;
      this.email = form.get('email')?.value;
      this.status = form.get('status')?.value === '1';
      this.role = form.get('role')?.value;
      this.distributorId = form.get('distributorId')?.value === ' ' ? '' : form.get('distributorId')?.value;
      this.stores = form.get('storeIds')?.value;
      if (this.stores) {
        let storeUsers: any[] = [];
        if (Array.isArray(this.stores)) {
          this.stores.forEach((value: number) => {
            storeUsers.push(new StoreModel(value));
          });
        } else {
          storeUsers = [new StoreModel(this.stores)];
        }

        this.stores = storeUsers;
      } else {
        this.stores = [];
      }
    } else {
      this.id = form;
    }
  }
}
