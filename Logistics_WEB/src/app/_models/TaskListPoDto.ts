export class TaskListPoDto {
  titleText: string = '';
  labels: string[] = [];
  datasets: any[] = [];
}
