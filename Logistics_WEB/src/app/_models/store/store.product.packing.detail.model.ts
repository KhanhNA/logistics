import {SuperEntity} from '@next-solutions/next-solutions-base';
import {StoreProductPackingModel} from '../store.product.packing.model';

export class StoreProductPackingDetailModel extends SuperEntity{
  code: string | undefined;
  originalQuantity: number | undefined;
  qRCode: string | undefined;
  quantity: number | undefined;
  storeProductPacking: StoreProductPackingModel | undefined;
}
