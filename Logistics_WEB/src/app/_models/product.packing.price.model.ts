import {CurrencyModel} from './currency.model';
import {ProductModel} from './product.model';
import {ProductPackingModel} from './product.packing.model';
import {FormGroup} from "@angular/forms";
import {subscribeOn} from "rxjs/internal/operators";

export class ProductPackingPriceModel {
  createDate: string | undefined;
  createUser: string | undefined;
  currency: CurrencyModel | undefined;
  fromDate: string | undefined;
  id: number | undefined;
  packingTypeQuantity: number | undefined;
  price: number | undefined;
  product: ProductModel | undefined;
  productPacking: ProductPackingModel | undefined;
  saleType: string | undefined;
  status: boolean | undefined;
  toDate: string | undefined;

  constructor(form: number | FormGroup | undefined) {
    if (form) {
      if (typeof form === 'number') {
        this.id = form;
      }
    }
  }

  static reduceCircleReference(price: ProductPackingPriceModel | undefined) {
    if (price) {
      price.product = price.product?.id ? new ProductModel(price.product.id) : undefined;
      price.productPacking = price.productPacking?.id ? new ProductPackingModel(price.productPacking.id) : undefined;
    }
  }
}
