import {Claim} from './claim.model';

export class ClaimImageModel {
  id: number | undefined;
  claim: Claim | undefined;
  url: string | undefined;
  description: string | undefined;
}
