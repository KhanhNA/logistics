import {LanguageModel} from './language.model';
import {ProductModel} from "./product.model";
import {ManufacturerModel} from "./manufacturer.model";
import {DistributorModel} from "./distributor.model";

export class ProductDescriptionModel {
  id: number | undefined;
  code: string | undefined;
  language: LanguageModel | undefined;
  name: string | undefined;
  product: ProductModel | undefined;

  static reduceCircleReference(productDes: ProductDescriptionModel | undefined) {
    if (productDes) {
      productDes.product = productDes.product?.id ? new ProductModel(productDes.product.id) : undefined;
    }
  }
}
