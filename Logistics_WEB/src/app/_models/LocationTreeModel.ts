import {CountryModel} from './country.model';
import {FormGroup} from '@angular/forms';
import {throwError} from 'rxjs';

export class LocationTreeModel {
  code: string | undefined;
  country: CountryModel| undefined;
  id: number| undefined;
  locationType: string| undefined;
  name: string| undefined;
  parent: any;
  status: number| undefined;

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
    // override
      throwError('not init object from Form')
    } else {
      this.id = form;
    }
  }
}
