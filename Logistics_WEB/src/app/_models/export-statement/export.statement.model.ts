import {StoreModel} from '../store.model';
import {ExportStatementDetailModel} from './export.statement.detail.model';
import {SuperEntity} from '@next-solutions/next-solutions-base';
import {FormGroup} from '@angular/forms';
import {PartnerModel} from "../partner/partner.model";

export class ExportStatementModel extends SuperEntity {
  code: string | undefined;
  description: string | undefined;
  estimatedTimeOfArrival?: string | undefined;
  exportDate?: string | undefined;
  exportStatementDetails: ExportStatementDetailModel[] = [];
  fromStore: StoreModel | undefined;
  fromStoreId: number | undefined;
  merchantCode: string | undefined | null = null;
  merchantOrder?: any;
  merchantOrderFromDC?: any;
  merchantOrderId: number | undefined | null = null;
  merchantOrderIds: number[] | undefined;
  qRCode?: string | undefined;
  status: number | undefined;
  toStore: StoreModel | undefined;
  toStoreId: number | undefined;
  total?: number;
  shippingPartner: PartnerModel | undefined;

  fromStoreUsername?: string; // to display
  fromStoreUserTel?: string; // to display
  toStoreUsername?: string; // to display
  toStoreUserTel?: string; // to display

  constructor(form: FormGroup | number | undefined) {
    super();
    if (form instanceof FormGroup) {
      const fId = form.get('id');
      if (fId && fId.value) {
        this.id = fId.value;
      }
      const fCode = form.get('code');
      if (fCode && fCode.value) {
        this.code = fCode.value;
      }
      const fDescription = form.get('description');
      if (fDescription && fDescription.value) {
        this.description = fDescription.value;
      }
      const fEstimatedTimeOfArrival = form.get('estimatedTimeOfArrival');
      if (fEstimatedTimeOfArrival && fEstimatedTimeOfArrival.value) {
        this.estimatedTimeOfArrival = fEstimatedTimeOfArrival.value;
      }

      const fExportStatementDetails = form.get('exportStatementDetails');
      if (fExportStatementDetails && fExportStatementDetails.value) {
        this.exportStatementDetails = fExportStatementDetails.value;
      }
      const fFromStore = form.get('fromStore');
      if (fFromStore && fFromStore.value) {
        this.fromStore = new StoreModel(fFromStore.value);
      }
      const fFromStoreId = form.get('fromStoreId');
      if (fFromStoreId && fFromStoreId.value) {
        this.fromStoreId = fFromStoreId.value;
      }
      const fMerchantCode = form.get('merchantCode');
      if (fMerchantCode && fMerchantCode.value) {
        this.merchantCode = fMerchantCode.value;
      }
      const fMerchantOrderId = form.get('merchantOrderId');
      if (fMerchantOrderId && fMerchantOrderId.value) {
        this.merchantOrderId = fMerchantOrderId.value;
      }
      const fMerchantOrderIds = form.get('merchantOrderIds');
      if (fMerchantOrderIds && fMerchantOrderIds.value) {
        this.merchantOrderIds = fMerchantOrderIds.value;
      }
      const fStatus = form.get('status');
      if (fStatus && fStatus.value) {
        this.status = fStatus.value;
      }
      const fToStore = form.get('toStore');
      if (fToStore && fToStore.value) {
        this.toStore = new StoreModel(fToStore.value);
      }
      const fToStoreId = form.get('toStoreId');
      if (fToStoreId && fToStoreId.value) {
        this.toStoreId = fToStoreId.value;
      }

      const fShippingPartner = form.get('shippingPartner');
      if (fShippingPartner && fShippingPartner.value) {
        this.shippingPartner = new PartnerModel(fShippingPartner.value);
      }

    } else {
      this.id = form;
    }
  }
}
