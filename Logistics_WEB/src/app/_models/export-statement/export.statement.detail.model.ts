import {ExportStatementModel} from "./export.statement.model";
import {SuperEntity} from "@next-solutions/next-solutions-base";
import {PalletDetailModel} from "../pallet/pallet.detail.model";
import {ProductPackingModel} from "../product.packing.model";
import {ProductPackingPriceModel} from "../product.packing.price.model";
import {StoreProductPackingModel} from "../store.product.packing.model";
import {StoreProductPackingDetailModel} from "../store/store.product.packing.detail.model";
import {ProductModel} from "../product.model";

export class ExportStatementDetailModel extends SuperEntity {
  expireDate?: string | undefined;
  exportDate?: string | undefined;
  exportStatement?: ExportStatementModel | undefined;
  palletDetail?: PalletDetailModel | undefined;
  productPacking: ProductPackingModel | undefined;
  productPackingPrice: ProductPackingPriceModel[] | any;
  quantity: number | undefined;
  storeProductPackingDetail: StoreProductPackingDetailModel | undefined;

  price: number | undefined;
  productPackingId: number | undefined;
  productPackingCode: number | undefined;

  totalQuantity = 0; // to display inventory
  weight = 0; // to display
  volume = 0; // to display
  orgQuantity: number | undefined = 0; // to display
  claimQuantity: number | undefined = 0; // to display
  product: ProductModel | undefined; // to display
}
