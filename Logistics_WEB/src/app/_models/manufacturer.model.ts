import {ManufacturerDescriptionModel} from "./manufacturer.description.model";
import {FormGroup} from "@angular/forms";

export class ManufacturerModel {
  code: string | undefined;
  email: string | undefined;
  id: number | undefined;
  manufacturerDescriptions: ManufacturerDescriptionModel[] = [];
  name: string | undefined;
  status: boolean | undefined;
  tel: string | undefined;

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      this.id = form.get('id')?.value;
      this.status = form.get('status')?.value;
      this.code = form.get('code')?.value;
    } else {
      this.id = form;
    }
  }
}
