import {ImportPoModel} from "./import.po.model";
import {PoDetailModel} from "../po/po.detail.model";
import {ProductModel} from "../product.model";
import {ProductPackingModel} from "../product.packing.model";
import {ProductPackingPriceModel} from "../product.packing.price.model";
import {PoModel} from "../po/po.model";

export class ImportPoQcModel {
  createDate: string | undefined;
  createUser: string | undefined;
  id: number | undefined;
  importPo: ImportPoModel | undefined;
  po: PoModel | undefined;
  updateDate: string | undefined;
  updateUser: string | undefined;
  url: string | undefined;
}
