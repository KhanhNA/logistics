import {ImportPoModel} from './import.po.model';
import {PalletModel} from '../pallet/pallet.model';
import {PalletDetailModel} from '../pallet/pallet.detail.model';
import {ImportPoDetailModel} from './import.po.detail.model';
import {ProductModel} from "../product.model";
import {ProductPackingModel} from "../product.packing.model";
import {ProductPackingPriceModel} from "../product.packing.price.model";

export class ImportPoDetailPalletModel {
  id: number | undefined;
  importPo: ImportPoModel | undefined;
  importPoDetail: ImportPoDetailModel | undefined;
  pallet: PalletModel | undefined;
  palletDetail: PalletDetailModel | undefined;
  quantity: number | undefined;

  // not storage
  product: ProductModel | undefined;
  productPacking: ProductPackingModel | undefined;
  productPackingPrice: ProductPackingPriceModel | undefined;
  expireDate: string | undefined;
  maxQuantity: number | undefined;
  claimQuantity: number | undefined;
  orgQuantity: number | undefined;
  palletId: number | undefined;

  constructor(params: {
    importPoId: number | undefined;
    id: number | undefined;
    importPoDetailId: number | undefined;
    pallet: number | undefined;
    palletDetailId: number | undefined;
    quantity: number | undefined;
  }) {
    this.id = params.id;
    this.importPo = params.importPoId ? new ImportPoModel(params.importPoId) : undefined;
    this.importPoDetail = params.importPoDetailId ? new ImportPoDetailModel(params.importPoDetailId) : undefined;
    this.pallet = params.pallet ? new PalletModel(params.pallet) : undefined;
    this.palletDetail = params.palletDetailId ? new PalletDetailModel(params.palletDetailId) : undefined;
    this.quantity = params.quantity ? params.quantity : 0;
  }
}
