import {DistributorModel} from '../distributor.model';
import {ImportPoDetailModel} from './import.po.detail.model';
import {ManufacturerModel} from '../manufacturer.model';
import {PoModel} from '../po/po.model';
import {StoreModel} from '../store.model';
import {FormGroup} from '@angular/forms';
import {ImportPoQcModel} from './import.po.qc.model';
import {ImportPoDetailPalletModel} from "./import.po.detail.pallet.model";

export class ImportPoModel {
  code: string | undefined;
  description: string | undefined;
  distributor: DistributorModel | undefined;
  id: number | undefined;
  importPoDetails: ImportPoDetailModel[] = [];
  importPoQcs: ImportPoQcModel[] = [];
  manufacturer: ManufacturerModel | undefined;
  po: PoModel | undefined;
  qRCode: string | undefined;
  status: number | undefined;
  store: StoreModel | undefined;

  // not storage
  importPoDetailPallets: ImportPoDetailPalletModel[] = [];

  constructor(form: FormGroup | number) {
    if (typeof form === 'number'){
      this.id = form;
      return;
    }
    if (form.get('id')) {
      this.id = form.get('id')?.value;
    }

    const fManufacturer = form.get('manufacturer')?.value;
    this.manufacturer = fManufacturer ? new ManufacturerModel(fManufacturer) : undefined;
    const fStore = form.get('store')?.value;
    this.store = fStore ? new StoreModel(fStore) : undefined;
    const fDistributor = form.get('distributor')?.value;
    this.distributor = fDistributor ? new DistributorModel(fDistributor) : undefined;

    this.po = form.get('po')?.value ? new PoModel(form.get('po')?.value) : undefined;

    const fDescription = form.get('description')?.value;
    this.description = fDescription ? fDescription : '';

    if (form.get('importPoDetails')) {
      this.importPoDetails = form.get('importPoDetails')?.value;
    }

    this.status = form.get('status')?.value;
  }
}
