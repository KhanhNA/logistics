import {ImportPoModel} from "./import.po.model";
import {PoDetailModel} from "../po/po.detail.model";
import {ProductModel} from "../product.model";
import {ProductPackingModel} from "../product.packing.model";
import {ProductPackingPriceModel} from "../product.packing.price.model";
import {FormGroup} from "@angular/forms";
import {ImportPoDetailPalletModel} from "./import.po.detail.pallet.model";
import {PalletModel} from "../pallet/pallet.model";

export class ImportPoDetailModel {
  expireDate: string | undefined;
  id: number | undefined;
  importPo: ImportPoModel | undefined;
  importPoDetailPallets: ImportPoDetailPalletModel[] = [];
  poDetail: PoDetailModel | undefined;
  product: ProductModel | undefined;
  productPacking: ProductPackingModel | undefined;
  productPackingPrice: ProductPackingPriceModel | undefined;
  quantity: number | undefined;

  // not storage
  maxQuantity: number | undefined;
  claimQuantity: number | undefined;
  orgQuantity: number | undefined;
  palletId: number | undefined;
  pallet: PalletModel | undefined;

  constructor(form: FormGroup | number) {
    if (typeof form === 'number') {
      this.id = form;
      return;
    }
  }
}
