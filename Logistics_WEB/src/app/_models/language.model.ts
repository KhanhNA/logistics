export class LanguageModel {
  id: number;
  code: string | undefined;

  constructor(id: number) {
    this.id = id;
  }
}
