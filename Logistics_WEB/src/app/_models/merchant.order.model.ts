import {FormGroup} from '@angular/forms';
import {StoreModel} from './store.model';
import {MerchantOrderDetailModel} from './merchant.order.detail.model';

export class MerchantOrderModel {
  id: number | undefined;
  code: string;
  qRCode: string;
  merchantCode: string;
  merchantName: string;
  amount: number;
  orderDate: string;
  orderCode: string;
  fromStore: StoreModel;
  status: number;
  merchantOrderDetails: MerchantOrderDetailModel[] | undefined;

  constructor(form: FormGroup) {
    this.code = form.get('code')?.value;
    this.qRCode = form.get('qRCode')?.value;
    this.merchantCode = form.get('merchantCode')?.value;
    this.merchantName = form.get('merchantName')?.value;
    this.amount = form.get('amount')?.value;
    this.orderDate = form.get('orderDate')?.value;
    this.orderCode = form.get('orderCode')?.value;
    this.fromStore = new StoreModel(form.get('fromStore')?.value);
    this.status = form.get('status')?.value;
  }
}
