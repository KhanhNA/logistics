import {ProductPackingModel} from './product.packing.model';
import {Claim} from './claim.model';
import {PalletDetailModel} from "./pallet/pallet.detail.model";

export class ClaimDetailModel {
  productPacking: ProductPackingModel | undefined;
  expireDate: string | undefined | null;
  quantity: number | undefined;
  claim: Claim | undefined;
  productPackingPrice: any | undefined;
  palletDetailId: string | undefined;
  palletDetail: PalletDetailModel | undefined;

  orgQuantity: number | undefined = 0;
  claimQuantity: number | undefined = 0;
}
