import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ApiService, AuthoritiesService, BaseTableLayout} from '@next-solutions/next-solutions-base';
import {ActivatedRoute, Router} from '@angular/router';
import * as Chart from 'chart.js';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {TaskListPoDto} from '../_models/TaskListPoDto';
import {LoginComponent} from "../login/login.component";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends BaseTableLayout implements OnInit, AfterViewInit {
  canvas: any;
  ctx: any;
  permissionTaskListPo: string[] = [
    'patch/pos/{id}/arrive-vietnam-port',
    'patch/pos/{id}/arrive-myanmar-port',
    'patch/pos/{id}/arrive-fc'
  ];

  constructor(
    protected activatedRoute: ActivatedRoute,
    public authoritiesService: AuthoritiesService,
    protected translateService: TranslateService,
    private router: Router,
    private loginComponent: LoginComponent,
    protected apiService: ApiService
  ) {
    super(activatedRoute, authoritiesService);
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.canvas = document.getElementById('canvas');
    if (this.canvas) {
      this.ctx = this.canvas.getContext('2d');
      this.apiService.get<TaskListPoDto>('/task-list/po', new HttpParams()).subscribe((data: TaskListPoDto) => {
        new Chart(this.ctx, {
          type: 'bar',
          data: {labels: data.labels, datasets: data.datasets},
          options: {
            title: {
              display: true,
              text: data.titleText
            },
            tooltips: {
              mode: 'index',
              intersect: false
            },
            responsive: true,
            scales: {
              xAxes: [{
                stacked: true,
              }],
              yAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                  callback(value: number) {
                    if (value % 1 === 0) {
                      return value;
                    }
                  }
                }
              }]
            },
          }
        });
      });
    }
  }
}
