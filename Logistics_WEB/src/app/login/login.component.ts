import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NavigationEnd, Router} from '@angular/router';
import {AuthenticationService} from '../_services/authentication.service';
import {HttpParams} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {NavService} from '../_services/nav.service';
import {
  ApiService,
  AuthoritiesService, FormStateService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {environment} from '../../environments/environment';
import {ChangePasswordService} from '../components/change-password/change.password.service';
import {CommonUtils} from '../_utils/common.utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
@Injectable()
export class LoginComponent implements OnInit {
  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    remember: new FormControl('')
  });

  user$ = this.authoritiesService.stateMe;
  formChangePassword = new FormGroup({
    oldPassword: new FormControl('abc@123'),
    newPassword: new FormControl('', {
      validators: Validators.compose([ChangePasswordService.newPasswordValidationConfirm('newConfirmPassword'),
        ChangePasswordService.newPasswordSameOldPasswordConfirm('oldPassword')])
    }),
    newConfirmPassword: new FormControl('', {
      validators: ChangePasswordService.newPasswordValidationConfirm('newPassword')
    })
  });
  error: string | null | undefined;

  roles: any[] = [];

  constructor(public dialog: MatDialog, private formBuilder: FormBuilder, private router: Router,
              private authenticationService: AuthenticationService, private apiService: ApiService,
              private utilsService: UtilsService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private matDialogRef: MatDialogRef<LoginComponent>,
              private cookieService: CookieService, private location: Location, private translateService: TranslateService,
              private authoritiesService: AuthoritiesService, protected formStateService: FormStateService,
              private navService: NavService) {
  }

  submit() {
    this.resetInfo();
    if (this.form.valid) {
      const body = new HttpParams()
        .set('username', this.form.controls.username.value)
        .set('password', this.form.controls.password.value)
        .set('grant_type', 'password');

      this.authenticationService.login(body.toString()).subscribe(data => {
        window.sessionStorage.setItem('token', JSON.stringify(data));
        this.cookieService.set('remember', this.form.controls.remember.value.toString());
        this.hideLoginModal();

        // console.log(this.location.isCurrentPathEqualTo('/logout'));
        // if (this.location.isCurrentPathEqualTo('/logout')) {
        this.router.navigate(['/home']);
        // }
        this.cookieService.set('username', this.form.controls.username.value);
      }, error => {
        console.log(error);
        this.translateService.get('login.error').subscribe(e => {
          this.error = e;
        });
      });
    }
  }

  onSave() {
    const params = new HttpParams()
      .set('oldPassword', this.formChangePassword.get('oldPassword')?.value + '')
      .set('newPassword', this.formChangePassword.get('newPassword')?.value + '')
      .set('newConfirmPassword', this.formChangePassword.get('newConfirmPassword')?.value + '');
    const api = () => this.apiService.post('/user/change-password', null, {params}, environment.BASE_AUTHORIZATION_URL);
    CommonUtils.customExecuteErrorHandle(this.utilsService, api, this.onSuccessFunc, '.edit.success', 'common.confirmSave', ['common.password.param'],
      this.onErrorFunc);
  }

  onErrorFunc = (err: string) => {
    this.utilsService.showErrorToarst('validation.change.password.fail');
    // this.dialogRef.close();
  };

  onSuccessFunc = (data: any, msg?: string) => {
    this.utilsService.onSuccessFunc(msg);
    // this.dialogRef.close();
    this.hideLoginModal();
    this.router.navigate(['/logout']).then();
  };

  resetInfo() {
    this.authoritiesService.me = null;
    this.navService.navItems = null;
    this.formStateService.setMapState(new Map<string, FormGroup>());
  }

  ngOnInit() {
    if (this.data === true) {
      return;
    }
    window.sessionStorage.removeItem('token');
    this.cookieService.delete('username');
    this.cookieService.delete('roles');
  }

  showLoginModal(isChangePassword?: boolean) {
    return this.dialog.open(LoginComponent, {disableClose: true, data: isChangePassword});
  }

  hideLoginModal() {
    this.dialog.closeAll();
  }

  toggleShowPassword(input: HTMLInputElement) {
    if (input.type === 'password') {
      input.type = 'text';
    } else if (input.type === 'text') {
      input.type = 'password';
    }
  }

  isDisplayFormLogin() {
    if (!this.authenticationService.isAuthenticated()) {
      return true;
    } else if (!this.user$.getValue().userAuthentication) {
      return true;
    } else {
      return !this.user$.getValue().userAuthentication?.principal?.mustChangePassword;
    }
  }
}
