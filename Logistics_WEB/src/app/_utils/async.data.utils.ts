import {TsUtilsService} from '../base/Utils/ts-utils.service';

export class AsyncDataUtils {

  static getStores(utilsService: TsUtilsService) {
    return utilsService.getFCStores();
  }

  static getDistributors(utilsService: TsUtilsService) {
    return utilsService.getDistributors2();
  }

  static getPallets(utilsService: any, storeId: number, step: number) {
    return utilsService.getPallets(storeId, step, true);
  }

  static async getAsyncDataStoresAndDistributors(storesService: TsUtilsService, distributorService: any) {
    const stores = this.getStores(storesService).toPromise();
    const distributors = this.getDistributors(distributorService).toPromise();
    await stores;
    await distributors;
    return {stores, distributors};
  }

  static async getPalletAsyncData(palletService: any, storeId: number, step: number) {
    const pallets = await this.getPallets(palletService, storeId, step).toPromise();
    return {pallets};
  }
}
