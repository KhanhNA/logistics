import {AuthoritiesService, ColumnFields, FlatTreeNode, UtilsService} from "@next-solutions/next-solutions-base";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

export class CommonUtils {

  static readonly MAX_PAGE_SIZE = 2147483647;

  static getPosition(e: any, arr: any[]) {
    return !!arr && arr.length > 0 ? (arr.indexOf(e) + 1).toString() : '';
  }

  static isEmptyList(list: any) {
    return !list || (Array.isArray(list) && list.length === 0);
  }

  // Đối với nghiệp vụ hiện tại, tài khoản nào có quyền nhận hàng PO là trưởng kho FC
  static isFCSKRole(authoritiesService: AuthoritiesService) {
    return authoritiesService.hasAuthority('post/import-pos');
  }

  static deletePropertyValueOfObject(properties: string[], obj: object, except: string[]): any {
    for (const key in obj) {

      if (except.includes(key.toString())) {
        continue;
      }
      if (properties.includes(key.toString())) {
        delete obj[key];
        continue;
      }
      if (obj[key] instanceof Array) {
        for (const element of obj[key]) {
          CommonUtils.deletePropertyValueOfObject(properties, element, except);
        }
      } else if (obj[key] instanceof Object) {
        CommonUtils.deletePropertyValueOfObject(properties, obj[key], except);
      }
    }
    return obj;
  }

  static deepCopyAndReduceReferenceManual(obj: object, copiedObj: object, except: string[]): any {
    if (obj === undefined || obj === null) {
      return;
    }
    for (const key in obj) {

      if (except.includes(key.toString())) {
        copiedObj[key] = obj[key]
          ? (obj[key].id
            ? {id: obj[key].id}
            : {code: obj[key].code ? obj[key].code : ''})
          : undefined;
        continue;
      }
      if (obj[key] instanceof Array) {
        copiedObj[key] = [];
        for (const element of obj[key]) {
          CommonUtils.deepCopyAndReduceReferenceManual(element, copiedObj[key], except);
        }
      } else if (obj[key] instanceof Object) {
        copiedObj[key] = {};
        CommonUtils.deepCopyAndReduceReferenceManual(obj[key], copiedObj[key], except);
        copiedObj[key] = obj[key]
          ? (obj[key].id
            ? {id: obj[key].id}
            : {code: obj[key].code ? obj[key].code : ''})
          : undefined;
      } else {
        if (copiedObj) {
          copiedObj[key] = obj[key];
        }
      }
    }
    return copiedObj;
  }

  static reduceCircleReferenceObject(obj: any, exceptProp?: string[] | undefined) {
    if (Array.isArray(obj)) {
      obj.forEach(row => {
        CommonUtils.reduceCircleReferenceObject(row, exceptProp);
      });
    } else if (obj && typeof obj === 'object') {
      Object.keys(obj).forEach(key => {
        if (obj[key] && typeof (obj[key]) === 'object') {
          CommonUtils.reduceCircleReferenceObject(obj[key], exceptProp);
          if (!Array.isArray(obj[key]) && (!exceptProp || !exceptProp.includes(key))) {
            obj[key] = obj[key]
              ? (obj[key].id
                ? {id: obj[key].id}
                : {code: obj[key].code ? obj[key].code : ''})
              : undefined;
          }
        }
      });
    }
  }

  static isNullOrUndefinedOrBlankValue(value: any) {
    return value === null || value === undefined || value === '';
  }

  static isMFCDistributor(distributorCodes: string[] | FlatTreeNode[] | undefined): boolean {
    if (distributorCodes) {
      return distributorCodes && distributorCodes.length === 1
        && (distributorCodes[0] === environment.MFC_CODE
          || (typeof distributorCodes[0] === 'object' ? distributorCodes[0]?.value === environment.MFC_CODE : false));
    }
    return false;
  }

  static getTotalFooterColspanDynamic(columns: ColumnFields[], dataTable: any[],
                                      exceptColumns: string[]) {
    let display = false;
    return columns.filter(col => !exceptColumns.includes(col.columnDef))
      .reduce((colspan, col) => {
        display = false;
        dataTable.forEach(row => {
          display = display || !!(typeof (col.display) === 'function' ? col.display(row) : col.display);
        });
        return colspan + (display ? 1 : 0);
      }, 0);
  }

  static customExecuteErrorHandle(utilsService: UtilsService,
                                  method: () => Observable<any>,
                                  onSuccessFunc: (this: void, d: any, onSuccessMessage?: string) => void,
                                  onSuccessMessage: string,
                                  confirmMsg: string,
                                  confirmMsgParamsFormat: string[] = [],
                                  onErrorFunc: (err: any) => void,
                                  confirmDialogButtonOk: string = 'common.OK', confirmDialogButtonCancel: string = 'common.Cancel',
  ) {
    utilsService.showConfirmDialog(confirmMsg, confirmMsgParamsFormat, '', [], undefined, confirmDialogButtonOk, confirmDialogButtonCancel)
      .afterClosed().subscribe(result => {
      if (result && result.value) {
        method().subscribe((data: any) => {
          onSuccessFunc(data, onSuccessMessage);
        }, error1 => {
          if (error1 !== '401') {
            onErrorFunc(error1);
          }
        });
      }
    });
  }
}
