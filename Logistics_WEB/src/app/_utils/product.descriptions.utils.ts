import {ProductModel} from '../_models/product.model';
import {environment} from '../../environments/environment';
import {ProductDescriptionModel} from '../_models/product.description.model';

export class ProductDescriptionsUtils {
  static parseDisplayProductDescriptionsToName(product: ProductModel | undefined) {
    if (product?.productDescriptions) {
      product.productDescriptions.forEach((des: ProductDescriptionModel) => {
        if (des.language && des.language.code === environment.CURR_LANG) {
          product.name = des.name;
        }
        des.product = product.id ? new ProductModel(product.id) : undefined;
      });
    }
    return product;
  }

  static getProductNameByCurrentLanguage(product: ProductModel | undefined) {
    let displayName: any = '';
    if (product) {
      displayName = product.name;
      product.productDescriptions?.forEach((des: ProductDescriptionModel) => {
        if (des.language && des.language.code === environment.CURR_LANG) {
          displayName = des.name;
        }
        des.product = product.id ? new ProductModel(product.id) : undefined;
      });
    }

    return displayName;
  }

}
