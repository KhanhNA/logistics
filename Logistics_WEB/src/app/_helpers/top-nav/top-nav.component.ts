import {Component, ElementRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {NavService} from '../../_services/nav.service';
import {Router} from '@angular/router';
import {LoginComponent} from '../../login/login.component';
import {MatDialog} from '@angular/material/dialog';
import {AppComponent} from '../../app.component';
import {AuthoritiesService, SingletonTranslateService} from '@next-solutions/next-solutions-base';
import {ChangePasswordComponent} from '../../components/change-password/change-password.component';
import {TranslateService} from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopNavComponent {
  selectedLanguage: string;

  currentTheme: string;

  languageKey: string = 'lang' + environment.CLIENT_ID;

  @ViewChild('userName') userName!: ElementRef;

  constructor(public navService: NavService, private translate: TranslateService,
              public cookieService: CookieService,
              private appComponent: AppComponent,
              private router: Router,
              private matDialog: MatDialog,
              private loginComponent: LoginComponent,
              protected authoritiesService: AuthoritiesService,
              protected singletonTranslateService: SingletonTranslateService) {
    if (!sessionStorage.getItem('lang' + environment.CLIENT_ID) || sessionStorage.getItem('lang' + environment.CLIENT_ID) === '') {
      sessionStorage.setItem('lang' + environment.CLIENT_ID, environment.DEFAULT_LANGUAGE);
      translate.setDefaultLang(environment.DEFAULT_LANGUAGE);

    } else {
      translate.setDefaultLang('' + sessionStorage.getItem('lang' + environment.CLIENT_ID));
    }
    translate.setDefaultLang('' + sessionStorage.getItem(this.languageKey));
    this.selectedLanguage = '' + sessionStorage.getItem(this.languageKey);

    if (this.cookieService.get('theme') === 'undefined' || this.cookieService.get('theme') === '') {
      this.currentTheme = environment.DEFAULT_THEME;
    } else {
      this.currentTheme = this.cookieService.get('theme');
    }

    appComponent.changeTheme(this.currentTheme);
    this.cookieService.set('theme', this.currentTheme);
  }

  onChangeLanguage(event: any) {
    this.translate.use(event.value);
    this.selectedLanguage = event.value;
    sessionStorage.setItem(this.languageKey, event.value);
    this.singletonTranslateService.currentLanguage.next(event.value);
  }

  onChangeTheme(event: any) {
    this.currentTheme = event.value;
    this.appComponent.changeTheme(this.currentTheme);
    this.cookieService.set('theme', this.currentTheme);
  }

  goToHome() {
    this.router.navigate(['home']);
  }

  getLanguages() {
    return environment.LANGS;
  }

  get environment() {
    return environment;
  }

  get username() {
    return !!this.authoritiesService.me ? this.authoritiesService.me.name : '';
  }

  openDialogChangePassword() {
    this.matDialog.open(ChangePasswordComponent, {
      width: '80%',
      height: '80%'
    });
  }

  showUserMenu() {
    const username = this.userName.nativeElement;
    if (username.classList.contains('off')) {
      username.classList.remove('off');
    }
    else {
      username.classList.add('off');
    }

  }
}
