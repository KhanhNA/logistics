import {Location} from '@angular/common';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TsUtilsService} from './Utils/ts-utils.service';
import {ToastrService} from 'ngx-toastr';
import {FieldConfigExt} from './field/field.interface';
import {AfterViewInit, OnDestroy, Directive} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Language} from '../components/language.model';
import {Search} from '../_models/Search';
import {TsTableV2Model} from './ts-table-v2/ts-table-v2.model';
import {ColumnTypeConstant} from '../_models/Column.type';
import {TranslateService} from '@ngx-translate/core';
import {SelectionModel} from '@angular/cdk/collections';
import {environment} from '../../environments/environment';
import {AuthoritiesService, BaseTableLayout} from '@next-solutions/next-solutions-base';

@Directive()
export class LDBaseLayout extends BaseTableLayout implements OnDestroy, AfterViewInit {
  form: FormGroup;
  _formData: any = {};
  inputFields: FieldConfigExt[] | undefined;
  tsTable: TsTableV2Model;
  languages = [environment.LANGS[0]];
  currLang = environment.LANGS[0];
  inputFieldsLangs: any = {};
  descriptions = {};

  lastSearch: any;


  // search: Search;
  readonly columnType = new ColumnTypeConstant();
  actionType: string | undefined;
  entity = '';
  parentId: number | undefined;
  private subscriber: any;
  functionTitle = ''; // title hien thi phan navi tren cung
  setCols(cols: Array<FieldConfigExt> | undefined) {
    if (cols) {
      this.tsTable.cols = cols;
    }
  }

  setSelection(multiSel?: boolean) {
    // this.tsTable.cols.push(...cols);
    if (multiSel === false) {
      this.tsTable.selection = new SelectionModel<any>(false, []);
    } else {
      this.tsTable.selection = new SelectionModel<any>(true, []);
    }

  }

  get getCols() {
    return this.tsTable.cols;
  }

  get selectedRowInd() {
    return this.tsTable.selectedRowIndex;
  }

  get orderBy() {
    return this.tsTable.orderBy;
  }

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location,
              protected authoritiesService: AuthoritiesService,
              ) {
    super(route, authoritiesService);
    this.tsTable = new TsTableV2Model(tsuService, fb);
    this.form = fb.group({});
    this.subscriber = this.route.params.subscribe(params => {
      this.actionType = params.actType;
      this.parentId = params.id;
    });

    this.lastSearch = {};
  }

  convertToRows(data: any) {
    const rows: any[] = [];
    return rows;
  }

  addTableData(data: any) {
    const rows = this.convertToRows(data);
    this.tsTable.addRows(rows);
  }

  updateSearchInfo(search: Search) {
  }

  beforeUpdate = (data?: any) => {

  }
  afterSearch = (data?: any) => {

  }

  get search() {
    return this.tsTable.search;
  }

  onSearchData(page?: any) {
    const search = this.search;
    const changed = this.checkChangeSearchForm(page);
    if (changed) {
      this.tsTable.paginator?.firstPage();
    }
    if (page !== undefined) {
      search.pageNumber = page.pageIndex;
      search.pageSize = page.pageSize;
    } else if (this.tsTable && this.tsTable.paginator) {
      this.tsTable.paginator.firstPage();
    }
    const canSearch = this.updateSearchInfo(this.search);
    if (canSearch === undefined || (canSearch != null && canSearch)) {
      if (this.search.method !== undefined) {
        this.tsuService.execute6(this.search.method, this.updateTable, '', undefined);
      }
    }
  }

  checkChangeSearchForm(page?: any): boolean {
    for (const key in this.lastSearch) {
      if (key !== 'table' && this.lastSearch[key] !== this.formData[key]) {
        if (page !== undefined) {
          page.pageIndex = 0;
        }
        this.search.pageNumber = 0;
        return true;
      }
    }
    return false;
  }

  updateTable = (data: any) => {
    this.lastSearch = {...this.formData};
    this.beforeUpdate(data);
    this.tsTable.updatePaging(data);
    this.afterSearch(data);
  }

  getTblData() {
    return this.tsTable.dataSrc.data;
  }

  get fv() {
    return this.form.value;
  }

  set formData(data: any) {
    this._formData = data;
    this.reBinding();
  }

  get formData() {
    return this._formData;
  }

  reBindingOne(lang?: Language) {
    if (lang && lang.code) {
      if (this.inputFieldsLangs[lang.code]) {
        for (const field of this.inputFieldsLangs[lang.code]) {
          if (field.binding && this.formData) {
            field.bindingRefs = this.tsuService.getParentFromNode(this.formData, field.binding);

          }
        }
      }
    }
  }

  reBinding() {
    for (const lang of this.languages) {
      this.reBindingOne(lang);
    }
  }


  init(inputFields: Array<FieldConfigExt> | undefined, languages?: any): void {
    if (inputFields) {

      this.inputFields = inputFields;
      if (languages) {
        this.languages = languages;
      }

      for (const lang of this.languages) {
        this.inputFieldsLangs[lang.code] = [];
        this.inputFields.forEach(field => {
          field.form = this.form;
          if (field.multiLanguage) {

            const f = new FieldConfigExt(Object.assign({}, field));
            f.dataName = field.name;
            f.name = 'descriptions.' + lang.code + '.' + field.name;
            f.binding = f.name;
            this.inputFieldsLangs[lang.code].push(f);

          } else {
            this.inputFieldsLangs[lang.code].push(field);
          }

        });
        this.reBindingOne(lang);
        this.form = this.tsuService.createControlV2(this.inputFieldsLangs[lang.code], this.form);
      }
    }
  }

  ngOnDestroy(): void {
    if (this.subscriber) {
      this.subscriber.unsubscribe();
    }
  }

  saveSuccess = (data: any) => {
    console.log(this.entity + '.' + this.actionType + '.success');
    const msg = this.translateService.instant(this.entity + '.' + this.actionType + '.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
  }

  showWarningMsg = (msgKey: string) => {
    const msg = this.translateService.instant(msgKey);
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.warning(msg);
  }

  showErrorMsg = (msgKey: string, rowIndex?: any[], msgParams?: any[]) => {
    let msg = this.translateService.instant(msgKey);
    if (msgParams) {
      msg = this.tsuService.strFormat(msgKey, msgParams);
    }

    if (rowIndex && rowIndex.length > 0) {
      msg = '' + msg + rowIndex.join(', ');
    }
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.error(msg);
  }

  showMaxQuantityWarning = (obj: any, msg: string) => {
    if (obj.field && obj.obj.quantity > obj.obj.maxQuantity) {
      this.showWarningMsg(msg);
      obj.obj.quantity = obj.obj.maxQuantity;
      // obj.field.form.controls[obj.field.name].setValue(obj.obj.maxQuantity);
      obj.field.form.get('table').controls[obj.obj.rowIndex].controls[obj.field.name].setValue(obj.obj.maxQuantity);
    }
  }

  onDisplayWithAuthority(authorities: any): boolean {
    if (typeof (authorities) === 'string') {
      return this.authoritiesService.hasAuthority(authorities);
    }
    if (typeof (authorities) === 'object') {
      let hasAuthority = false;
      for (const authority of authorities) {
        hasAuthority = hasAuthority || this.authoritiesService.hasAuthority(authority);
        if (hasAuthority) {
          return hasAuthority;
        }
      }
    }
    return false;
  }

  onPage(page: any) {
    this.tsTable.selection.selected.length = 0;
    this.tsTable.allSelect = {};
    this.tsTable.selectedRowIndex = 0;
    this.onSearchData(page);
  }

  onSort(event: any) {
    console.log(event);
    this.onSearchData();
  }

  ngAfterViewInit(): void {
  }
}
