import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {FieldConfigExt} from '../field/field.interface';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {DistributorModel} from '../../_models/distributor.model';
import {ApiService, NsCustomDialogDataConfig, Page, UtilsService} from '@next-solutions/next-solutions-base';
import {MatDialogConfig} from "@angular/material/dialog";

@Injectable({
  providedIn: 'root',
})
export class TsUtilsService {

  constructor(private transService: TranslateService, private toastr: ToastrService,
              private utilsService: UtilsService,
              private userService: ApiService) {
  }

  getParentFromNode(row: any, key: any) {

    key = key.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    key = key.replace(/^\./, '');           // strip a leading dot
    const a = key.split('.');
    let parent = row;
    let k;
    for (let i = 0, n = a.length; i < n; ++i) {
      k = a[i];
      if (!(k in row)) {
        row[k] = (i < n - 1) ? {} : null;
      }
      parent = row;
      row = row[k];

    }
    return [parent, k];
  }

  createControlV2(inputFields: Array<FieldConfigExt>, form: FormGroup) {
    inputFields.forEach(field => {


      if (field.type === 'button') {
        return;
      }
      if (field.type === 'fileImg') {
        const dat = {src: field.value, file: null as any};
      }
      const control = new FormControl(
        field.bindingRef ? field.bindingRef[0][field.bindingRef[1]] : '',
        this.bindValidations(field.validations || [])
      );
      form.addControl(field.name ? field.name : '', control);
      field.form = form;
    });
    return form;
  }

  bindValidations(validations: any) {
    if (validations.length > 0) {
      const validList: any[] = [];
      validations.forEach((valid: any) => {
        validList.push(valid.validator);
      });
      return Validators.compose(validList);
    }
    return null;
  }

  public strFormat(str: string, replacement: string[] | undefined) {
    let a = this.transService.instant(str);
    if (replacement === null || replacement === undefined || replacement.length === 0) {
      return a;
    }

    for (const k of Object.keys(replacement)) {
      a = a.replace('{' + k + '}', this.transService.instant(replacement[k]));
    }
    return a;
  }

  public onSuccessFunc = (onSuccessMessage?: string): void => {
    const msg = this.transService.instant(onSuccessMessage ? onSuccessMessage : '');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
  }

  public showError = (err: string): void => {
    let errLocale = '';
    if (err !== undefined && err !== null) {
      errLocale = this.transService.instant(err);
    }
    this.utilsService.showError(errLocale);
  }

  private execute3(apiCall: any, onSuccessFunc: (this: void, data: any, onSuccessMessage?: string) => void, onSuccessMessage?: string) {

    if (!apiCall) {
      if (onSuccessFunc) {
        onSuccessFunc(null);
      }
      return;
    }
    apiCall.subscribe((data: any) => {
      if (onSuccessFunc) {
        if (onSuccessMessage) {
          onSuccessFunc(data, onSuccessMessage);
        } else {
          onSuccessFunc(data);
        }
      } else {
        this.onSuccessFunc(onSuccessMessage ? onSuccessMessage : '');
      }
    }, (error1: any) => {
      if (error1 !== '401') {
        this.showError(error1);
      }
    });
  }

  public execute6(apiCall: any, onSuccessFunc: (this: void, d: any) => void, confirmMsg: string,
                  confirmMsgParamsFormat: string[] | undefined,
                  confirmDialogButtonOk: string = 'common.OK', confirmDialogButtonCancel: string = 'common.Cancel'
  ) {
    if (confirmMsg !== undefined && confirmMsg !== null && confirmMsg !== '') {
      this.showConfirmDialog(confirmMsg, confirmMsgParamsFormat)
        .afterClosed().subscribe((result: any) => {
        if (result.value) {
          this.execute3(apiCall, onSuccessFunc, undefined);
        }
      });
    } else {
      this.execute3(apiCall, onSuccessFunc, undefined);
    }

  }

  public executeMrHieu(apiCall: any, onSuccessFunc: (this: void, d: any, onSuccessMessage?: string) => void, onSuccessMessage: string,
                       confirmMsg: string, confirmMsgParamsFormat: string[] | undefined,
                       confirmDialogButtonOk: string = 'common.OK', confirmDialogButtonCancel: string = 'common.Cancel'
  ) {
    if (confirmMsg !== undefined && confirmMsg !== null && confirmMsg !== '') {
      this.showConfirmDialog(confirmMsg, confirmMsgParamsFormat)
        .afterClosed().subscribe((result: any) => {
        if (result.value) {
          this.execute3(apiCall, onSuccessFunc, onSuccessMessage);
        }
      });
    } else {
      this.execute3(apiCall, onSuccessFunc, onSuccessMessage);
    }

  }


  public showConfirmDialog(str1: string, replacement: string[] | undefined,
                           strDetailMsg: string = '', replacementDetail: string[] = [],
                           confirmDialogConfig?: MatDialogConfig<NsCustomDialogDataConfig>,
                           strOkText: string = 'common.OK', strCancelTex: string = 'common.Cancel'
  ) {
    // const str = this.strFormat(str1, replacement);
    // const strOk = this.transService.instant(strOkText);
    // const strCancel = this.transService.instant(strCancelTex);
    return this.utilsService.showConfirmDialog(str1, replacement ? replacement : []);
  }

  showInputConfirmDialog(titleMsg: string, textMsg: string, requiredMsg: string, method: any,
                         onSuccessFunc: (data: any, onSuccessMessage: string) => void, messageSuccess: string,
                         onErrorFunc: (msg: any) => void) {

    return this.utilsService.showConfirmInputDialog(titleMsg, [])
      .afterClosed().subscribe(result => {
        if (result.value) {
          method(result.value).subscribe((response: any) => {
            onSuccessFunc(response, messageSuccess);
          }, (errorMsg: any) => {
            console.log(errorMsg);
            onErrorFunc(errorMsg);
          });
        }
      });
  }

  public showSuccessMsg(strMsg: string, strTitle: string = 'Logistic') {
    const msg = this.transService.instant(strMsg);
    const msgTitle = this.transService.instant(strTitle);
    this.toastr.success(msg, msgTitle);
  }

  public getManufacturers = (value: any) => {
    const params = new HttpParams()
      .set('text', (value === null || value === undefined) ? '' : value)
      .set('status', '1')
      .set('pageNumber', '1')
      .set('pageSize', '20');
    const method = this.userService.get('/manufacturers', params);
    return method;
  }

  public getAllManufacturers = () => {
    const params = new HttpParams()
      .set('text', '')
      .set('status', '1')
      .set('pageNumber', '1')
      .set('pageSize', '99999');
    const method = this.userService.get<Page>('/manufacturers', params);
    return method;
  }

  public getFCStores = (): Observable<any> => {
    const params = new HttpParams()
      .set('status', 'true')
      .set('text', '')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('is_DC', 'false')
      .set('pageNumber', '1')
      .set('pageSize', '' + 9999);
    return this.userService.get('/stores', params);
  }

  public getPallets = (storeId: any, step: any, status: any) => {
    if (!step) {
      step = '1';
    }
    if (!status) {
      status = true;
    }
    const params = new HttpParams()
      .set('storeId', storeId ? storeId : '')
      // .set('text', '')
      .set('step', step)
      .set('status', status)
      .set('pageNumber', '1')
      .set('pageSize', '99999');
    const method = this.userService.get('/pallets', params);
    return method;
  }

  public getDistributors = (): Observable<DistributorModel[]> => {
    const method = this.userService.get<DistributorModel[]>('/distributors/all', new HttpParams());
    return method;
  }

  public getDistributors2 = (): Observable<any> => {
    return this.userService.get('/distributors/all', new HttpParams());
  }
}
