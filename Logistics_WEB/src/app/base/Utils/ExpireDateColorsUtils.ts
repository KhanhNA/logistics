import {TranslateService} from '@ngx-translate/core';
import {Injectable} from '@angular/core';

@Injectable()
export class ExpireDateColorsUtils {
  constructor(private transService: TranslateService) {
  }
  public getTranslated(value: string): string {
    if (value) {
      return this.transService.instant(value);
    }
    return '';
  }
  public getIcon(value: string): string {
    if (value) {
      switch (value) {
        case 'mat-row-red': return 'cancel';
        case 'mat-row-orange': return 'error';
        case 'mat-row-yellow': return 'warning';
        default: return '';
      }
    }
    return '';
  }
}
