export class DateUtils {
  static getCurrentDate(): Date {
    return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(),
      -(new Date().getTimezoneOffset()) / 60, 0, 0);
  }

  static getDateFromString(date: any): Date {
    if (date === 0 || date) {
      return new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate(),
        -(new Date().getTimezoneOffset()) / 60, 0, 0);
    } else {
      return DateUtils.getCurrentDate();
    }
  }

  static getCurrentDate_0H(): Date {
    return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(),
      0, 0, 0);
  }

  static getDateFromString_0H(date: any): Date {
    if (date === 0 || date) {
      return new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate(),
        0, 0, 0);
    } else {
      return DateUtils.getCurrentDate_0H();
    }
  }

  static getDayBeforeNow(numberDay: number): Date {
    const result = DateUtils.getCurrentDate();
    result.setDate(result.getDate() - numberDay);
    return result;
  }

  static getDayBeforeDate(dateString: string, numberDay: number): Date {
    const result = new Date(dateString);
    result.setDate(result.getDate() - numberDay);
    return result;
  }

  static getDayAfterDate(dateString: string, numberDay: number): Date {
    const result = new Date(dateString);
    result.setDate(result.getDate() + numberDay);
    return result;
  }

  static getDayBeforeOneMonth(): Date {
    return DateUtils.getDayBeforeNow(30);
  }
}
