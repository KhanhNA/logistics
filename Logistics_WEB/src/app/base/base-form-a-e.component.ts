import {CookieService} from 'ngx-cookie-service';
import {AfterViewInit, ChangeDetectorRef, Component, Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ConfirmationDialogComponent} from './confirm-dialog/confirm.component';
import {ApiService} from '@next-solutions/next-solutions-base';


@Component({
  template: ''
})
export class BaseFormAEComponent implements AfterViewInit {
  // @ViewChild('confirmationDialog') confirmationDialog: TdConfirmDialogComponent;
  dialog: MatDialog | undefined;
  form: FormGroup | undefined;
  cols: number | undefined;
  // viewCols: any;
  aeCols: any;
  // apiService: ApiService;
  apiUrl: string | undefined;

  get fc() {
    return this.form?.controls;
  }

  get fv() {
    return this.form?.value;
  }

  get sv() {
    return this.userService;
  }


  constructor(protected formBuilder: FormBuilder, protected cookieService: CookieService, protected userService: ApiService,
              @Inject(MAT_DIALOG_DATA) protected row: any, protected changeDetector: ChangeDetectorRef) {

  }


  openDialogParam(msg: string, callback: any): void {
    const dialogRef = this.dialog?.open(ConfirmationDialogComponent, {
      width: '350px',
      data: msg,
      disableClose: true
    });
    dialogRef?.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        callback();
        // DO SOMETHING
      }
    });
  }

  ngAfterViewInit(): void {
    // this.ae.apiUrl = this.apiUrl;
    // this.ae.loadData(this.ae, this.aeCols, this.row);
    // // this.ae.doSave = this.doSave;
    // this.changeDetector.detectChanges();
  }

  // ngOnInit(): void {
  // console.log('init');
  // const control = {};
  // for (const key of this.viewCols) {
  //   // this.ae.addControl(key, new FormControl(''));
  //   if ('objLang' === key) {
  //     control[key] = {};
  //   } else if ('ae' !== key) {
  //     control[key] = [''];
  //   }
  // }
  // this.ae = this.formBuilder.group(control);

  // this.data.ae = this.ae;
  // console.log('here2', this.ae, this.ae);


  // }

}
