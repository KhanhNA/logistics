import {Component, OnInit, Output, EventEmitter } from '@angular/core';

import {TranslateService} from '@ngx-translate/core';
import {CookieService} from 'ngx-cookie-service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionComponent implements OnInit {
  @Output() backEvent = new EventEmitter<number>();
  @Output() nextEvent = new EventEmitter<number>();
  @Output() saveEvent = new EventEmitter<number>();
  @Output() clickEvent = new EventEmitter<number>();
  constructor(private translate: TranslateService,
              private cookieService: CookieService) {


  }

  ngOnInit() {
  }

  onBackEvent(event: any) {
    console.log('xxx');
    console.log(event);
    this.clickEvent.emit(environment.BUTTON_BACK);

  }
  onNextEvent(event: any) {
    console.log('xxx');
    console.log(event);
    event.value = environment.BUTTON_NEXT;
    this.clickEvent.emit(event);

  }
  onSaveEvent(event: any) {
    console.log('xxx');
    console.log(event);
    event.value = environment.BUTTON_SAVE;
    this.clickEvent.emit(event);

  }

}
