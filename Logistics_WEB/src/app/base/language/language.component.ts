import {Component, EventEmitter, OnInit, Output} from '@angular/core';

import {TranslateService} from '@ngx-translate/core';
import {CookieService} from 'ngx-cookie-service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-lang',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class LanguageComponent implements OnInit {
  @Output() selectChangeEvent = new EventEmitter<number>();
  selectedLanguage: string;
  isDefaultLang: boolean;
  langId: number;
  apSetting: any;

  VIFlag = 'assets/Flags/vi.ico';
  ENFlag = 'assets/Flags/en.ico';


  constructor(private translate: TranslateService,
              private cookieService: CookieService) {
    if (this.cookieService.get('langId') === 'undefined' || this.cookieService.get('langId') === '') {
      this.langId = 1;
    } else {
      this.langId = Number(this.cookieService.get('langId'));
    }
    this.selectedLanguage = environment.LANGS[this.langId].code;
    this.isDefaultLang = this.selectedLanguage === environment.DEFAULT_LANGUAGE;
    this.apSetting = environment.LANGS;
  }

  ngOnInit() {
  }

  onChangeLanguage(event: any) {
    console.log(event);
    this.langId = event.value;
    this.isDefaultLang = event.value === 1;
    this.selectChangeEvent.emit(this.langId);
  }


}
