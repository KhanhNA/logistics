import {CookieService} from 'ngx-cookie-service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DatePipe} from '@angular/common';
import {Search} from '../../_models/Search';
import {MultilanguagePanigator} from '../../_helpers/multilanguage.paginator';
import {SelectionModel} from '@angular/cdk/collections';
import {ColumnTypeEnum} from '../../_models/Column.type';
import {environment} from '../../../environments/environment';
import {ApiService, UtilsService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-ld', /* dinh nghia id*/
  templateUrl: './l-d.html',
  styleUrls: ['./l-d.scss'],
  providers: [
    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator},
    DatePipe
  ]
})
export class LDComponent implements OnInit, AfterViewInit {
  @Output() addEvent = new EventEmitter<any>();
  @Output() editEvent = new EventEmitter<any>();
  @Output() delEvent = new EventEmitter<number>();
  @Output() searchEvent = new EventEmitter<any>();
  @Output() headerActionEvent = new EventEmitter<any>();
  @Output() cellActionEvent = new EventEmitter<any>();
  @Output() showButtonEvent = new EventEmitter<any>();
  @Output() rowClickEvent = new EventEmitter<any>();
  // @Input() ldCols;
  // @Input() api: string;
  @Input() info: any;
  // form: FormGroup;
  @Input() group?: FormGroup;
  models = new MatTableDataSource<any>([]);

  search: Search | undefined;

  displayedColumns: any[] = [];
  selection = new SelectionModel<any>(true, []);
  allSelect: any = {};
  pageSizeOption = environment.PAGE_SIZE_OPTIONS;

  // compAdd: ;
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, public dialog: MatDialog, private datePipe: DatePipe,
              private changeDetector: ChangeDetectorRef,
              private utilsService: UtilsService
  ) {
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.models.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.models.data.forEach(row => {
        delete this.allSelect[row[this.info.id]];
      });
    } else {

      this.models.data.forEach(row => {
        this.selection.select(row);
        this.allSelect[row[this.info.id]] = row;
      });
    }

  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    const tmp = `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
    return tmp;
  }

  isShowBtn(act: any, row: any) {
    const data = {act, row, show: false};
    this.showButtonEvent.emit(data);

    return data.show;
  }

  onAddData(): void {
    // console.log('abc');
    // this.dialog.open(ExampleComponent, {
    //   data: { message: 'I am a dynamic component inside of a dialog!' },
    // });
    const event = {};
    this.addEvent.emit(event);
    if (this.info.type === 0) {
      this.dialog.open(this.info.aeComponent, {disableClose: false, width: '90%', maxHeight: '90vh', data: event});
    } else {

      this.router.navigate([this.router.url, 'new']);
    }
  }

  onEdit(data: any) {

    this.editEvent.emit(data);

    if (this.info.type === 0) {
      this.dialog.open(this.info.aeComponent, {disableClose: false, width: '1500px', data});
    } else {
      // let extra = new NavigationExtras()
      // const caller = data;

      // this.router.paramsInheritanceStrategy = 'always';

      console.log('this data:', data);
      this.router.navigate([this.router.url, 'edit', data.id]);


      console.log(this.router);
    }
  }

  onDelete(data: any) {
    // this.dialog.open(this.info.aeComponent, {disableClose: false, width: '500px', data: event});
    console.log('still not implement');
  }

  getSearchParam() {

  }

  onSearchData() {

    if (this.group && this.search) {
      this.searchEvent.emit(this.group.value);
      console.log('this method:', this.search.method);
      if (this.search.method !== undefined) {
        this.search.method.subscribe((data: any) => {
          this.updatePaging(data);

        }, (error1: any) => {
          console.log('error', error1);
        });

      }
    }

    // this.search.text = this.ae.controls.text.value;
    // // const params = new HttpParams().set('text', this.search.text).set('pageNumber', this.search.pageNumber.toString()).set('pageSize', this.search.pageSize.toString());
    // const params = new HttpParams().set('langId', '1'); // .set('pageSize', this.search.pageSize.toString());
    // console.log('varPaging here');
    // this.varPaging(params);
  }

  getValueFromModels(row: any, key: any) {

    key = key.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    key = key.replace(/^\./, '');           // strip a leading dot
    const a = key.split('.');
    for (let i = 0, n = a.length; i < n; ++i) {
      const k = a[i];
      if (k in row) {
        row = row[k];
      } else {
        return;
      }
    }
    return row;
  }

  getData(row: any, column: any) {
    const value = this.getValueFromModels(row, column.name);

    if (column.type === undefined || value === null
      || value === undefined) {
      return value;
    }


    switch (column.type) {
      case ColumnTypeEnum.date:
        const d = new Date(value);
        return this.datePipe.transform(d, 'dd/MM/yyyy');

      default:
        return value;

    }
  }

  getColumnType(type: any) {
    // console.log('type:', type);
    if (type === undefined) {
      return 0;
    }
    return type;
  }

  getSuccess(data: any) {

  }

  // varPaging(params: HttpParams) {
  //   console.log('api', this.info.apiSearch, params);
  //   if ('POST' === this.info.apiSearch.method) {
  //     this.apiService.postPaging(this.info.apiSearch, params)
  //       .subscribe(data => {
  //         this.getSuccess(data);
  //
  //       }, error1 => {
  //         console.log('error', error1);
  //       });
  //   } else {
  //     this.apiService.getPaging(this.info.apiSearch, params)
  //       .subscribe(data => {
  //         this.getSuccess(data);
  //
  //       }, error1 => {
  //         console.log('error', error1);
  //       });
  //   }
  //
  // }

  updatePaging(data: any) {
    // this.models = new MatTableDataSource<BaseModel>(data.content);

    // this.models = data.content;
    // this.models.length = 0;
    // const data1 = new Array<Category>();
    // // this.models.concat(data.content);
    // for (const item of data.content) {
    //   const c = Object.assign(new Category(), item);
    //   // Object.assign(c, {dateCreated: new Date(item.dateCreated)});
    //   data1.push(c);
    // }
    // this.models = data1;
    console.log('updateData:', data, this.search);
    this.models.data = data.content;
    this.models._updateChangeSubscription();
    // console.log(this.models, data1[0].dateCreated.getDate());
    // console.log(this.models[0].dateCreated.getDay());
    if (this.search) {
      this.search.pageSize = data.size;
      this.search.pageNumber = data.number + 1;
      this.search.totalElements = data.totalElements;
    }

    this.selection.clear();
    if (this.models !== undefined) {
      for (const row of this.models.data) {
        console.log('selected row:', row);
        if (this.allSelect[row[this.info.id]] !== undefined) {
          this.selection.select(row);
        }
        // this.selection.select(this.models[row.stt]);
      }
    }
  }

  onChangeCheckBox(row: any) {
    console.log('change:', row);
    this.selection.toggle(row);
    if (this.selection.isSelected(row)) {
      this.allSelect[row[this.info.id]] = row;
    } else {
      delete this.allSelect[row[this.info.id]];
    }
    console.log('allselect:', this.allSelect);
  }

  getPage(page: any) {

    // for (const sel of this.selection.selected) {
    //   this.allSelect[sel[this.info.id]] = sel;
    // }
    // this.allSelect[this.search.pageNumber] = this.selection;

    if (this.search) {
      this.search.pageNumber = page.pageIndex + 1;
      this.search.pageSize = page.pageSize;
    }
    console.log('getPage:', page);

    // this.selection = this.allSelect[this.search.pageNumber];
    // console.log('this selection:', this.selection);
    // if (this.selection === undefined) {
    //   this.selection = new SelectionModel<any>(true, []);
    //   this.allSelect[this.search.pageNumber] = this.selection;
    // }
    this.onSearchData();

    // this.models.forEach(row => this.selection.select(row));


  }

  ngOnInit(): void {
    if (this.info.ldCols === undefined) {
      this.info.ldCols = [''];
    }
    if (this.info.colTypes === undefined) {
      this.info.colTypes = {};
    }
    if (!this.group) {
      this.group = this.formBuilder.group({});
    }
    this.group.addControl('text', this.formBuilder.control(null));
    //   _displayCols: [this.info.ldCols],
    // });
    // }

    console.log('thisForm:', this.group);
    this.search = new Search();
    this.search.text = '';
    this.search.pageSize = environment.PAGE_SIZE;
    this.search.pageNumber = 1;
    this.search.totalElements = 0;

    // this.repackingCols = ['stt'];
    // if (this.info.imgKey !== null && this.info.imgKey !== undefined) {
    //   this.repackingCols = this.repackingCols.concat(['image']);
    // }
    for (const item of this.info.ldCols) {
      this.displayedColumns.push(item.name);
      // this.repackingCols = this.repackingCols.concat(this.info.ldCols).concat(['addEdit']);
    }
    // this.repackingCols.push('addEdit');

    // this.info.aeComponent = PoDetailComponent

  }

  getIndex(row: any, i: any) {
    // console.log(this.search, row);
    if (this.search) {
      const ind = (this.search.pageNumber - 1) * this.search.pageSize + i + 1;
      row.stt = i;
      return ind;
    }
    return -1;
  }

  onHeaderAction(name: string) {
    if (name === 'add') {
      this.onAddData();
    } else {
      this.headerActionEvent.emit({action: name, data: this.models});
    }

  }

  onCellClick(act: string, model: any) {
    if (act === 'edit') {
      this.onEdit(model);
    } else if (act === 'delete') {
      this.onDelete(model);
    } else {
      console.log('call action cellActionEvent');
      this.cellActionEvent.emit({action: act, model, data: this.models});
    }
  }

  onRowClick(event: any, row: any) {
    console.log('event row clickAction', event, row);
    this.rowClickEvent.emit({event, row});
  }

  ngAfterViewInit(): void {
    this.onSearchData();

  }

  setTableData(data: any) {
    this.models = new MatTableDataSource(data);
  }

  addTableRow(row: any) {
    this.models.data.push(row);
    this.models._updateChangeSubscription();
  }

  removeTableRow(ind: number, count: number = 1) {
    this.models.data.splice(ind, count);
    this.models._updateChangeSubscription();
  }
}
