import {FormBuilder, FormGroup} from '@angular/forms';
import {Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

import {LDComponent} from './l-d/l-d-component';
import {LanguageComponent} from './language/language.component';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {ApiService} from '@next-solutions/next-solutions-base';


@Component({
  template: ''
})
export class BaseFormLDComponent implements OnInit {
  // @ViewChild('confirmationDialog') confirmationDialog: TdConfirmDialogComponent;
  @ViewChild(LanguageComponent) lang: LanguageComponent | undefined;
  @ViewChildren(LDComponent) ld: QueryList<LDComponent> | undefined;
  // dialog: MatDialog;
  form: FormGroup | undefined;
  firstLanguage: string;
  isDefaultLang: boolean | undefined;
  firstLangId: number;
  // apiService: ApiService;
  // api: string;
  // ldCols: any;
  ldInfo: any;

  get fc() {
    return this.form?.controls;
  }

  get fv() {
    return this.form?.value;
  }

  get sv() {
    return this.apiService;
  }

  constructor(protected formBuilder: FormBuilder, cookieService: CookieService, protected apiService: ApiService,
              protected transService: TranslateService, toastr: ToastrService
  ) {
    this.firstLanguage = cookieService.get('lang');
    this.firstLangId = Number(cookieService.get('langId'));
  }


  // doBack() {
  //   let newLangId;
  //
  //   if (this.lang.langId !== this.firstLangId) {
  //     newLangId = (this.lang.langId - 1) / AppSettings.LANGS.length;
  //     this.lang.langId = newLangId;
  //   }
  //
  // }


  // onDelete(model: BaseModel): void {
  //   this.apiService.delete(this.api + model.id)
  //     .subscribe(data => {
  //       this.onSearch();
  //     });
  // }
  //
  // onEdit(model: BaseModel): void {
  //   window.sessionStorage.removeItem('editId');
  //   window.sessionStorage.setItem('editId', model.id.toString());
  //   this.dialog.open(AEProductComponent, {disableClose: false, width: '500px'})
  //     .beforeClosed().subscribe(x => {
  //     window.sessionStorage.removeItem('editUserId');
  //     this.onSearch();
  //   });
  // }
  //
  // onAdd(): void {
  //   this.dialog.open(AEProductComponent, {disableClose: false, width: '1500px'})
  //     .beforeClosed().subscribe(x => {
  //     this.onSearch();
  //   });
  // }
  //
  // onSearch() {
  //   this.search.text = this.ae.controls.text.value;
  //
  //   const params = new HttpParams().set('langId', '1'); // .set('pageSize', this.search.pageSize.toString());
  //   this.apiService.getPaging(this.api, params)
  //     .subscribe(data1 => {
  //       console.log(data1);
  //
  //       // this.models = new MatTableDataSource <this.data> (data1.content);
  //       this.search.pageSize = data1.size;
  //       this.search.pageNumber = data1.number;
  //       this.search.totalElements = data1.totalElements;
  //     });
  // }

  //
  // ngAfterViewInit(): void {
  //   // this.ld.defTblHeader(this.ldCols);
  //   // this.changeDetector.detectChanges();
  //
  // }

  ngOnInit(): void {
    const control = {text: ''};
    // for (const key in this.data) {
    //   // this.ae.addControl(key, new FormControl(''));
    //   if ('objLang' === key) {
    //     control[key] = {};
    //   } else if ('form' !== key) {
    //     control[key] = [''];
    //   }
    // }
    this.form = this.formBuilder.group(control);
    // this.ld.onSearchData();
  }

  onSearchData(event: any) {
    // Swal.close();
  }

  get search() {
    if (this.ld === undefined || this.ld.first === undefined) {
      return null;
    }
    return this.ld.first.search;
  }

}
