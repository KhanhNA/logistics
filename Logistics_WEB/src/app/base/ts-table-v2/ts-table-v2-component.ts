import {FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {DatePipe} from '@angular/common';
import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MultilanguagePanigator} from '../../_helpers/multilanguage.paginator';
import {ColumnTypeConstant} from '../../_models/Column.type';
import {TsTableV2Model} from './ts-table-v2.model';
import {MatPaginator} from '@angular/material/paginator';
import {ExpireDateColorsUtils} from '../Utils/ExpireDateColorsUtils';
import {AuthoritiesService} from '@next-solutions/next-solutions-base';


@Component({
  selector: 'ts-table-v2', /* dinh nghia id*/
  templateUrl: './ts-table-v2.html',
  styleUrls: ['./ts-table-v2.scss'],
  providers: [
    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator},
    DatePipe,
  ]
})
export class TsTableV2Component implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort | undefined;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator | undefined;
  @Output() addEvent = new EventEmitter<any>();
  @Output() editEvent = new EventEmitter<any>();
  @Output() delEvent = new EventEmitter<number>();
  @Output() searchEvent = new EventEmitter<any>();
  @Output() cellEvent = new EventEmitter<any>();
  @Output() rowClickEvent = new EventEmitter<any>();
  @Output() isShowButtonEvent = new EventEmitter<any>();
  @Output() onPage = new EventEmitter<any>();
  @Output() sortEvent = new EventEmitter<any>();

  @Input() model: TsTableV2Model | undefined;
  @Input() form: FormGroup | undefined;
  readonly colType = new ColumnTypeConstant();

  // compAdd: ;
  constructor(public dialog: MatDialog,
              public expireDateUtils: ExpireDateColorsUtils,
              public expireDateColorsUtils: ExpireDateColorsUtils,
              protected authoritiesService: AuthoritiesService) {
  }

  matSortChangeEvent(event: any) {
    if (this.model) {
      this.model.orderBy = event;
    }
    this.sortEvent.emit(event);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.model?.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    const tmp = `${this.model?.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
    return tmp;
  }

  onAddData(): void {
    const data = {};
    this.addEvent.emit(data);
  }

  onEdit(data: any) {
    this.editEvent.emit(data);
  }

  onDelete(data: any) {
    this.delEvent.emit(data);
  }


  getSuccess(data: any) {

  }


  getColumnType(type: any) {
    if (type === undefined) {
      return 0;
    }
    return type;
  }

  onRowClick(event: any, row: any, ind: number) {
    if (this.model) {
      this.model.selectedRowIndex = ind;
    }
    this.rowClickEvent.emit({event, row, ind});
  }

  onHeaderAction(name: string) {
    this.addEvent.emit(name);
  }

  onCellClick(act: any, row: any, ind: number) {
    if (this.model) {
      this.model.selectedRowIndex = ind;
    }
    const data = {act, row, ind};
    this.cellEvent.emit(data);
  }

  isShowBtn(act: any, row: any) {
    const data = {act, row, isShow: true};
    this.isShowButtonEvent.emit(data);
    return data.isShow;
  }

  hasAuthority(authorities: [string]) {
    if (!authorities) {
      return false;
    }
    let checkAuthority = false;
    for (const authority of authorities) {
      checkAuthority = checkAuthority || this.authoritiesService.hasAuthority(authority.toLowerCase());
      if (checkAuthority) {
        break;
      }
    }
    return checkAuthority;
  }

  showButtonTitle(field: any, act: string) {
    if (field.titles && field.titles[act]) {
      return field.titles[act];
    }
    return '';
  }

  getPage(page: any) {
    this.onPage.emit(page);
    // this.onSearchData(page);
  }

  ngOnInit(): void {
    if (this.model) {
      this.model.form = this.form;
      this.model.dataSrc.sort = this.sort ? this.sort : null;
      this.model.paginator = this.paginator;
    }
    // this.form = this.tsuService.createControlV2(this.model.cols, this.form);

    // this.onSearchData();

  }
}
