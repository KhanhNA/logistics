import {TsUtilsService} from '../Utils/ts-utils.service';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field/field.interface';
import {Search} from '../../_models/Search';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {environment} from '../../../environments/environment';

export class TsTableV2Model {
  form: FormGroup | undefined;
  rowIdName = 'id';
  dataSrc: MatTableDataSource<any>;
  _cols: Array<FieldConfigExt> | undefined;
  displayCols: Array<string> | undefined;
  search: Search;
  entity: string | undefined;
  tblInputFields: Array<any> | undefined;
  currLang = environment.LANGS[0];
  orderBy: any;
  pageSizeOption = environment.PAGE_SIZE_OPTIONS;
  paginator: MatPaginator | undefined;

  allSelect: any = {};
  selection = new SelectionModel<any>(false, []);
  selectedRowIndex = -1;

  constructor(private tsuService: TsUtilsService, private fb: FormBuilder) {
    this.search = new Search();
    this.dataSrc = new MatTableDataSource<any>([]);
  }

  get cols(): Array<FieldConfigExt> {
    return this._cols ? this._cols : [];
  }

  set cols(cols: Array<FieldConfigExt>) {
    this._cols = cols;
    this.displayCols = this.getDisplayCols();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSrc.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.dataSrc.data.forEach(row => {
        delete this.allSelect[row[this.rowIdName]];
      });
    } else {

      this.dataSrc.data.forEach(row => {
        this.selection.select(row);
        this.allSelect[row[this.rowIdName]] = row;
      });
    }

  }

  getDisplayCols() {
    const ret: any[] = [];
    if (this._cols) {
      for (const col of this._cols) {
        // if (col.inputType === 'fileImgBase64') {
        //   if (!environment.SHOW_QR_CODE) {
        //     continue;
        //   }
        // }
        ret.push(col.name);
      }
    }
    return ret;
  }

  onChangeCheckBox(row: any) {
    this.selection.toggle(row);
    if (this.selection.isSelected(row)) {
      this.allSelect[row[this.rowIdName]] = row;
    } else {
      delete this.allSelect[row[this.rowIdName]];
    }
  }

  updatePaging = (data: any) => {

    // this.tsuService.createTblControlV2(this._cols, this.form, data.content);
    this.dataSrc.data.length = 0;
    if (data.content) {
      // this.dataSrc.data = data.content;
      if (data.size) {
        this.search.pageSize = data.size;
        this.search.pageNumber = data.number;
        this.search.totalElements = data.totalElements;
      } else {
        this.search.pageSize = data.content.length;
        this.search.pageNumber = 0;
        this.search.totalElements = data.content.length;
      }
      this.addRows(data.content);
    } else {
      this.addRows(data);
    }

    // this.createTableField();
    if (this.selectedRowIndex === -1) {
      this.selectedRowIndex = 0;
    }
    this.dataSrc._updateChangeSubscription();
    this.selection.clear();
    if (this.dataSrc !== undefined) {
      for (const row of this.dataSrc.data) {
        if (this.allSelect[row[this.rowIdName]] !== undefined) {
          this.selection.select(row);
        }
      }
    }
  };

  getIndex(row: any, i: any) {
    // console.log(this.search, row);
    const ind = (this.search.pageNumber) * this.search.pageSize + i + 1;
    row.stt = i;
    return ind;
  }

  addRows(rows: Array<any>) {
    // console.log('NMQ add row: ', rows);
    if (this.dataSrc.data.length === 0) {
      console.log('create table');
      this.dataSrc.data.push(...rows);
      this.createTableField();
    } else {
      console.log('update table');
      const from = this.dataSrc.data.length;
      const size = rows.length;
      this.dataSrc.data.push(...rows);
      this.updateTableField(from, size);
    }

    this.dataSrc._updateChangeSubscription();
  }

  removeRows(start: number, count: number = 1) {
    this.dataSrc.data.splice(start, count);
    const tableArray = this.form?.get('table') as FormArray;

    if (tableArray != null) {
      for (let index = start; index < this.dataSrc.data.length; index++) {
        const rowData = this.dataSrc.data[index];
        rowData.rowIndex = Number(rowData.rowIndex) - count;
      }
      while (count--) {
        const rowGroup = tableArray.at(start + count);
        // console.log('remove position 1: ', start + count);
        if (rowGroup != null) {
          tableArray.removeAt(start + count);
          // console.log('remove position 2 : ', start + count);
          this.tblInputFields?.splice(start + count, 1);
        }
      }
    }
    // // console.log('dataSrc : ', this.dataSrc);
    // // console.log('form : ', this.form);
    this.dataSrc._updateChangeSubscription();
  }

  getField(rowGroup: FormGroup, field: any, index: any, row: any) {

    if (!this.tblInputFields) {
      this.tblInputFields = [];
    }
    let inputRow = this.tblInputFields[index];
    if (!inputRow) {
      this.tblInputFields[index] = {};
      this.tblInputFields.push({});
      inputRow = this.tblInputFields[index];
    }
    let f = inputRow[field.name];
    if (!f) {
      f = new FieldConfigExt(field);
      inputRow[field.name] = f;
    }
    if (!['button', 'view', 'index'].includes(field.type)) {
      f.form = this.form;
      const control = new FormControl(
        f.bindingRef ? f.bindingRef[0][f.bindingRef[1]] : '',
        this.tsuService.bindValidations(f.validations || [])
      );
      if (f.name.includes('.')) {
        // // console.log('field, index, f.name: ', field, index, f.name);
        // // console.log('f.name.toString(): ', f.name.toString());
        const nameParts = f.name.toString().split('.');
        // // console.log(names);
        let value = row;
        let isBreak = false;
        let i = 0;
        for (i; i < nameParts.length - 1; i++) {
          if (value === undefined || value === null) {
            isBreak = true;
            break;
          }
          value = value[nameParts[i]];
        }
        if (!isBreak) {
          control.setValue(value);
        }
      } else {
        control.setValue(row[f.name]);
      }
      rowGroup.addControl(f.name, control);
    }
    if (!field.binding) {
      field.binding = field.name;
    }
    f.label = '';
    f.bindingRefs = this.tsuService.getParentFromNode(row, field.binding);
    if (f.emit && f.bindingRef) {
      f.emit({obj: f.bindingRef[0], value: f.bindingRef[1]});
    }

    return f;

  }

  createTableField() {
    this.tblInputFields = [];
    this.form?.removeControl('table');
    const tableFormArray = this.fb.array([]);
    const size = this.dataSrc.data.length;
    this.createRowGroup(tableFormArray, 0, size);
    this.form?.setControl('table', tableFormArray);
  }

  createRowGroup(tableFormArray: FormArray, from: number, size: number) {
    // // console.log('from, size : ', from, size);
    for (let index = 0; index < size; index++) {
      const rowGroup = this.fb.group([]);
      if (this.cols && this.cols.length > 0) {
        this.cols.forEach(field1 => {
          this.dataSrc.data[index + from].rowIndex = index + from;
          this.getField(rowGroup, field1, index + from, this.dataSrc.data[index + from]);
        });
      }
      tableFormArray.push(rowGroup);
    }
  }

  showIWant(i: any, name: any) {
    if (this.tblInputFields)
      // if (this.tblInputFields[i] === undefined) {
      // console.log('this is: ', i);
      // }
      return this.tblInputFields[i] && this.tblInputFields[i][name] ? this.tblInputFields[i][name] : '';
    return '';
  }

  showIWant2(form: any, i: any) {
    return form.get('table').controls[i];
  }

  private updateTableField(from: number, size: number) {
    const tableForm = this.form?.get('table');
    // console.log('update', tableForm);
    this.updateRowGroup(tableForm, size, from);
  }

  private updateRowGroup(tableFormArray: any, size: number, from: number) {
    // console.log(this.tblInputFields);
    // console.log('updateRowGroup', tableFormArray);
    this.createRowGroup(tableFormArray, from, size);
    this.form?.setControl('table', tableFormArray);
  }
}
