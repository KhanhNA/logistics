import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class NumberValidators {

  static quantityValidation(totalQuantity?: any, min?: any): ValidatorFn {
    return quantityValidationFn;

    function quantityValidationFn(control: AbstractControl): ValidationErrors | null {

      if (control.value === null || control.value === '') {
        return null;
      }


      if (Number(control.value) <= min) {
        return {
          minQuantity: {
            max: totalQuantity,
            current: control.value
          }
        };
      }

      if (Number(control.value) > Number(totalQuantity)) {
        return {
          maxQuantity: {
            max: totalQuantity,
            current: control.value
          }
        };
      }
      return null;
    }
  };

  static minQuantityValidation(min: any): ValidatorFn {
    return minQuantityValidationFn;

    function minQuantityValidationFn(control: AbstractControl): ValidationErrors | null {
      if (control.value === null || control.value === '') {
        return null;
      }

      if (Number(control.value) <= min) {
        const minQuantity = {
          minQuantity: {
            min: min + 1,
            current: control.value
          }
        };
        return minQuantity;
      }
      return null;
    }
  }

  static integerValidation(): ValidatorFn {
    return integerValidationFn;

    function integerValidationFn(control: AbstractControl): ValidationErrors | null {

      if (control.value === null || control.value === '') {
        return null;
      }

      const INTEGER_NUMBER_PATTERN = '^\\-?\\d+$';

      if (control.value.toString().match(INTEGER_NUMBER_PATTERN)) {
        return null;
      }

      const integerNumber = {
        integerNumber: {
          current: control.value
        }
      };
      return integerNumber;
    }
  }
}
