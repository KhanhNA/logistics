import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class EmailValidator {

  static emailValidation(): ValidatorFn {
    return emailValidationFn;

    function emailValidationFn(control: AbstractControl): ValidationErrors | null {
      if (control.value == null || control.value === '') {
        return null;
      }

      const emailPattern = '^[\\w\\.]{5,32}@[a-z]{2,}(\\.[a-z]{2,4}){1,3}$';

      if (control.value.toString().match(emailPattern)) {
        return null;
      }

      return {
        emailValidation: control.value
      };

    }
  }

}
