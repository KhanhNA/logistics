// extend NativeDateAdapter's format method to specify the date format.
import { NativeDateAdapter } from '@angular/material/core';
import {Injectable} from '@angular/core';

export const MY_FORMATS_1 = {
  parse: {
    dateInput: 'dd/MM/yyyy',
  },
  display: {
    dateInput: '/',
    monthYearLabel: 'MM yyyy',
    dateA11yLabel: 'dd/MM/yyyy',
    monthYearA11yLabel: 'MM yyyy',
  },
};
export const MY_FORMATS_2 = {
  parse: {
    dateInput: 'dd/MM/yyyy',
  },
  display: {
    dateInput: '-',
    monthYearLabel: 'MM yyyy',
    dateA11yLabel: 'dd/MM/yyyy',
    monthYearA11yLabel: 'MM yyyy',
  },
};

@Injectable()
export class CustomDateAdapter extends NativeDateAdapter {
  public static DIS_DATE_FORMAT = 'dd/MM/yyyy';
  public static DIS_DATETIME_FORMAT = 'dd/MM/yyyy HH:mm:ss';

  format(date: Date, displayFormat: any): string {
    if (displayFormat === '/' || displayFormat === '-') {
      const day = date.getUTCDate();
      const month = date.getUTCMonth() + 1;
      const year = date.getFullYear();
      // Return the format as per your requirement
      const daystr = day < 10 ? '0' + day : '' + day;
      const monstr = month < 10 ? '0' + month : '' + month;
      return `${daystr + displayFormat + monstr + displayFormat + year}`;
    } else {

      return super.format(date, displayFormat);
    }
  }

  // If required extend other NativeDateAdapter methods.
}

@Injectable()
export class CustomDateNotUTCAdapter extends NativeDateAdapter {
  public static DIS_DATE_FORMAT = 'dd/MM/yyyy';
  public static DIS_DATETIME_FORMAT = 'dd/MM/yyyy HH:mm:ss';

  format(date: Date, displayFormat: any): string {
    if (displayFormat === '/' || displayFormat === '-') {
      const day = date.getDate();
      const month = date.getMonth() + 1;
      const year = date.getFullYear();
      // Return the format as per your requirement
      const daystr = day < 10 ? '0' + day : '' + day;
      const monstr = month < 10 ? '0' + month : '' + month;
      return `${daystr + displayFormat + monstr + displayFormat + year}`;
    } else {

      return super.format(date, displayFormat);
    }
  }

  // If required extend other NativeDateAdapter methods.
}
