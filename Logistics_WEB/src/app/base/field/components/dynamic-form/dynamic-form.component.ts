import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FieldConfig} from '../../field.interface';

@Component({
  exportAs: 'dynamicForm',
  selector: 'dynamic-form',
  templateUrl: './dynamic-form.html',
  // template: `
  //   <ae class="dynamic-ae" [formGroup]="ae" (submit)="onSubmit($event)">
  //     <ng-container *ngFor="let field of fields;" dynamicField [field]="field" [group]="ae">
  //     </ng-container>
  //   </ae>
  // `,
  styles: []
})


export class DynamicFormComponent implements OnInit {
  @Input() fields: FieldConfig[] = [];

  @Output() submit: EventEmitter<any> = new EventEmitter<any>();

  form: FormGroup | undefined;

  get value() {
    return this.form?.value;
  }

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    console.log('create control');
    // this.form =
    this.createControl();
    console.log(this.form);
  }

  onSubmit(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.form?.valid) {
      this.submit.emit(this.form?.value);
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  createControl() {
    // const group = this.fb.group({});
    this.fields.forEach(field => {
      if (field.type === 'button') {
        return;
      }
      const control = new FormControl(
        field.value,
        this.bindValidations(field.validations || [])
      );
      if (field.name)
        this.form?.addControl(field.name, control);
    });
    // return group;
  }

  bindValidations(validations: any) {
    if (validations.length > 0) {
      const validList: any[] = [];
      validations.forEach((valid: any) => {
        validList.push(valid.validator);
      });
      return Validators.compose(validList);
    }
    return null;
  }

  validateAllFormFields(formGroup?: FormGroup) {
    if (formGroup) {
      Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control) {
          control.markAsTouched({onlySelf: true});
        }
      });
    }
  }

  onChangedLanguage($event: any) {
  }

  onResize(event: any): void {
  }

  onActionClick($event: any) {
  }
}
