import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field.interface';

@Component({
  selector: 'app-checkbox',
  template: `
    <div class="demo-full-width margin-top" [formGroup]="group">
      <mat-checkbox [formControlName]="field?.name"
                    [value]="field && field.bindingRef ? field.bindingRef[0][field.bindingRef[1]] : ''"
                    (change)="onChange($event)"
      >{{(field?.label + '') | translate}}
      </mat-checkbox>
    </div>
  `,
  styles: []
})
export class CheckboxComponent implements OnInit {
  field: FieldConfigExt | undefined;
  group: FormGroup | undefined;

  onChange(event: any) {
    if (this.field && this.field.bindingRef)
      this.field.bindingRef[0][this.field.bindingRef[1]] = event.checked;
    if (this.field && this.field.emit) {
      this.field.emit(event);
    }
  }

  constructor() {
  }

  ngOnInit() {
  }
}
