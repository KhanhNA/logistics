import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field.interface';

@Component({
  selector: 'app-select',
  template: `
    <div class="float_label">
      <mat-form-field class="demo-full-width margin-top" [formGroup]="group" style="width: 100%"  [appearance]="'outline'">
        <mat-label>{{field?.label | translate}}</mat-label>
        <mat-select [placeholder]="(field?.label ? (field?.label + '') : '') |translate"
                    [formControlName]="field?.name"
                    value="{{field && field.bindingRef ? (field.bindingRef[0][field.bindingRef[1]] + '') : ''}}"
                    (valueChange)="field && field.bindingRef ? field.bindingRef[0][field.bindingRef[1]] = $event : null"
                    [required]="field?.require ? !!field?.require : false"
                    (selectionChange)="onChange($event)"
                    [disabled]="field?.readonly ? !!field?.readonly : false">

          <mat-option *ngFor="let item of keys;" [value]="item"
                      title="{{field?.options[item] | translate}}">
            {{field?.options[item] | translate}}
          </mat-option>
        </mat-select>
      </mat-form-field>
    </div>
  `,
  styles: [],
})
export class SelectComponent implements OnInit {
  field?: FieldConfigExt;
  group?: FormGroup;
  keys: any;

  constructor() {
  }

  ngOnInit() {
    this.keys = Object.keys(this.field?.options);
  }

  onChange($event: any) {
    if (this.field?.emit) {
      if (this.field.options instanceof Array) {
        this.field.emit({
          obj: this.field,
          index: $event.value,
          field: this.field
        });
      } else if (this.field.options instanceof Object) {
        this.field.emit({
          obj: this.field,
          value: $event.value,
          field: this.field
        });
      }
    }
  }
}
