import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field.interface';
import {environment} from '../../../../environments/environment';
import {ApiService} from '@next-solutions/next-solutions-base';
// class ImageSnippet {
//   constructor(public src: string, public file: File) {
//   }
// }

@Component({
  selector: 'app-file-img',
  template: `
    <div [formGroup]="group" style="width: 100%;text-align: center" class="uploadfilecontainer"
         (click)="fileInput.click()"
         appDragDrop (onFileDropped)="processFile($event)">
      <!--<input matInput formControlName="{{field.name}}" hidden>-->
      <img [src]="imgURL?imgURL:baseURL + (!!field?.bindingRef && !!field?.options  ? field?.bindingRef[0][field?.options[0]] : '')"
           style="width: 60px; height: 60px; text-align: center">
      <input hidden type="file" #fileInput (change)="processFile($event.target['files'])" accept="image/*">
    </div>

  `,
  styleUrls: ['../styles/file-img-styles.scss']
})


export class FileImgComponent implements OnInit {
  @HostBinding('style.background-color') background = '#f5fcff';
  @HostBinding('style.opacity') opacity = '1';


  @Input() field?: FieldConfigExt;
  @Input() group?: FormGroup;
  // selectedFile: ImageSnippet;
  // imagePath;
  imgURL: any = null;
  baseURL = environment.BASE_URL + '/files/';


  constructor(private userService: ApiService) {
  }

  ngOnInit() {
    console.log('field:', this.field);
    // if (this.field.bindingRef) {
    //   // this.imgURL = this.field.bindingRef[0][this.field.stores[0]];
    //   this.imgURL = 'http://localhost:8234/api/v1/files/pig.png';
    // }


  }


  processFile(imageInput: any) {
    console.log('imageInput:', imageInput);
    const file: File = imageInput[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      if (this.field?.bindingRef) {
        // this.field.value.src = event.target.result;
        // this.field.value.file = file;
        this.imgURL = reader.result;
        this.field.bindingRef[0][this.field.options[0]] = file.name;
        if (!this.field.bindingRef[0][this.field.options[1]]) {
          this.field.bindingRef[0][this.field.options[1]] = [];
        }
        this.field.bindingRef[0][this.field.options[1]] = [];
        this.field.bindingRef[0][this.field.options[1]].push(file);
      }
      // this.group.get(this.field.name).setValue(file.name);
    });

    reader.readAsDataURL(file);
  }


}
