import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field.interface';


@Component({
  selector: 'app-input',
  template: `
    <div class="float_label">
      <mat-form-field [formGroup]="group"  [appearance]="'outline'" style="width: 100%"
      [floatLabel]="'always'">
        <!--      style="width: 100%;"-->
        <mat-label>{{field?.label | translate}}</mat-label>
        <input matInput formControlName="{{field?.name}}" [placeholder]="field?.label | translate" [type]="field?.inputType"
               (change)="onchange($event)" [id]="field?.name"
               [required]="field?.require"
               [readonly]="readonly || field?.readonly"
               [value]="field?.bindingRef[0][field?.bindingRef[1]]"
               [title]="field?.inputType === 'number' ? ((field?.bindingRef[0][field?.bindingRef[1]] |number)
                ? (field?.bindingRef[0][field?.bindingRef[1]] |number) : ''  ) :
               field?.bindingRef[0][field?.bindingRef[1]] ? field?.bindingRef[0][field?.bindingRef[1]] : ''"
        >
        <!--      <ng-container *ngFor="let validation of field?.validations;" ngProjectAs="mat-error">-->
        <mat-error
          *ngIf="group?.get([field?.name]) && group?.get([field?.name])?.invalid">
          {{getValidationMessage(field?.name) | translate}}
        </mat-error>
        <!--        </ng-container>-->
      </mat-form-field>
    </div>
  `,
  styles: []
})
export class InputComponent implements OnInit {
  field?: FieldConfigExt;
  group?: FormGroup;
  readonly?: boolean;

  onchange(event: any) {
    if (this.field?.bindingRef) {
      // this.field.binding[this.field.name] = event.target.value;
      this.field.bindingRef[0][this.field.bindingRef[1]] = event.target.value;
      if (this.field.emit) {
        this.field.emit({obj: this.field.bindingRef[0], value: event.target.value, field: this.field});
      }
    }

  }

  getValidationMessage(fieldName: any): string {
    if (this.field?.validations) {
      for (const validation of this.field.validations) {
        if (this.group?.get(fieldName)?.hasError(validation.name)) {
          return validation.message;
        }
      }
    }
    return '';
  }

  constructor() {

  }

  ngOnInit() {
  }
}
