import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field.interface';
import {DatePipe} from '@angular/common';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import {environment} from '../../../../environments/environment';
import {CustomDateNotUTCAdapter, MY_FORMATS_1} from '../custom-date-adapter';
import {DateUtils} from '../../Utils/date.utils';

@Component({
  selector: 'app-date',
  template: `
    <div class="float_label">
      <mat-form-field class="demo-full-width margin-top"
                      [formGroup]="group"
                      [appearance]="'outline'"
                      style="width: 100%;">
        <mat-label>{{field?.label | translate}}</mat-label>
        <input matInput
               [matDatepicker]="picker"
               [formControlName]="field?.name"
               [placeholder]="(field?.label + '')|translate"
               (dateChange)="onDateChange($event)"
               [required]="!!field?.require"
               [value]="viewDate"
               (focus)="!field?.readonly ? picker.open() : doNothing()"
               [matDatepickerFilter]="myFilter"
               [readonly]="!!field?.readonly || !!field?.require"
        >
        <mat-datepicker-toggle matSuffix [for]="picker" [disabled]="!!field?.readonly"></mat-datepicker-toggle>
        <mat-datepicker #picker></mat-datepicker>
        <mat-hint></mat-hint>
        <ng-container *ngFor="let validation of (field?.validations ? field?.validations : []);" ngProjectAs="mat-error">
          <mat-error
            *ngIf="group?.get([field?.name])?.hasError(validation.name)">{{validation.message | translate}}</mat-error>
        </ng-container>
      </mat-form-field>
    </div>
  `,
  styles: [],
  providers: [
    DatePipe,
    {provide: DateAdapter, useClass: CustomDateNotUTCAdapter},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_1}
  ]
})
export class DateComponent implements OnInit {
  field: FieldConfigExt | undefined;
  group: FormGroup | undefined;
  viewDate: any;

  constructor(private datePipe: DatePipe) {
  }

  myFilter = (d: Date): boolean => {
    if (!this.field?.options) {
      return true;
    }
    let cmpDate: Date;
    if (this.field.options.min != null) {
      for (const item of this.field.options.min) {
        if ('curDate' === item) {
          cmpDate = new Date();
        } else {
          if (this.field.bindingRef) {
            const convertDate = new Date(this.field.bindingRef[0][item]);
            cmpDate = new Date(convertDate.getFullYear(),
              convertDate.getMonth(),
              convertDate.getDate(),
              0, 0, 0);
          } else {
            cmpDate = new Date(1900, 1, 1);
          }
        }
        if (d < cmpDate) {
          return false;
        }
      }
    }
    if (this.field.options.max != null && this.field.bindingRef) {
      for (const item of this.field.options.max) {
        if ('curDate' === item) {
          cmpDate = new Date();
        } else if ('devide' === item) {
          d.setHours(0, 0, 0)
          cmpDate = this.addDays(DateUtils.getCurrentDate_0H(), this.field.bindingRef[0].product.lifecycle)
        } else if (this.field.bindingRef[0][item] === null) {
          return true;
        } else {
          cmpDate = new Date(this.field.bindingRef[0][item]);
        }
        if (d > cmpDate) {
          return false;
        }
      }

    }
    return true;
  }

  onDateChange(event: any) {
    console.log(event);
    if (this.field?.bindingRef) {
      this.viewDate = event.value;
      this.field.bindingRef[0][this.field.bindingRef[1]] = this.datePipe.transform(event.value, environment.API_DATE_FORMAT);
      if (this.field.emit) {
        this.field.emit({obj: this.field.bindingRef[0], field: this.field, value: event.value});
        this.viewDate = this.field.bindingRef[0][this.field.bindingRef[1]];
        this.field.bindingRef[0][this.field.bindingRef[1]] = this.datePipe.transform(this.viewDate, environment.API_DATE_FORMAT);
      }
    }
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  doNothing() {

  }

  ngOnInit() {
    if (this.field?.bindingRef) {
      this.viewDate = this.field.bindingRef[0][this.field.bindingRef[1]];
      this.field.bindingRef[0][this.field.bindingRef[1]] = this.datePipe.transform(this.viewDate, environment.API_DATE_FORMAT);
    }
  }
}
