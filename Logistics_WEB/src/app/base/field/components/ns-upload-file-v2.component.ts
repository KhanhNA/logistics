import {Component, ElementRef, EventEmitter, Input, Optional, Output, Self, ViewChild} from '@angular/core';
import {ControlValueAccessor, NgControl} from '@angular/forms';
import {UploadModel} from '@next-solutions/next-solutions-base';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {FileSaverService} from 'ngx-filesaver';


@Component({
  selector: 'ns-upload-file-v2',
  template: `
    <div class="ns-multi-upload"
         fxLayout="row" fxLayout.lt-sm="row wrap">
      <mat-label *ngIf="label"
                 fxLayout="column" fxFlex="140px" fxFlex.lt-md="100%">
        <div class="label">{{(label ? label : '') | translate}}<span
          class="required-label-outside">{{!!required ? '*' : ''}}</span></div>
      </mat-label>
      <div class="uploadArea" style="background-color: #FFFFFF"
           fxLayout="column" fxFlex="auto" fxFlex.lt-md="100%"
           appDragDrop (onFileDropped)="onFileDroppedAction($event)">
        <input type="file" #fileUpload (change)="onFileChangeAction($event)"
               [disabled]="disabled"
               accept="{{accept ? accept.join() : '*/*'}}">
        <mat-card>
          <mat-card-actions>
            <div fxLayout="column">
              <button mat-button
                      class="{{(files[0] && files[0].type?.startsWith('image'))?'upload-button preview': 'upload-button'}}"
                      color="warn" (click)="onClick()">
                <div class="preview-image"
                     *ngIf="files[0] && files[0].type?.startsWith('image')">
                  <img (load)="onLoad(img)"
                       #img
                       style="display: none"
                       [src]="getSrcImg()| secure | async"
                       *ngIf="files[0] && files[0].type?.startsWith('image')"
                  />
                </div>

                <!--                <div class="preview-image" style="background-image:url({{files[0]?.previewValue}})"-->
                <!--                     *ngIf="fileType.startsWith('image') && files[0]"></div>-->
                <!--                -->


                <i [ngClass]="files[0]?'fa fa-file-alt':'fa fa-plus'"
                   *ngIf="!files[0] || !files[0].type?.startsWith('image')"></i>
                <span *ngIf="!files[0]">{{('common.button.importExcel' | translate)}}</span>

                <i *ngIf="files[0]" class="fa fa-eye" (click)="previewFiles($event, downloadId)">
                  <a download (click)="downloadFile()" #downloadId style="display:none;"></a>
                </i>
                <i *ngIf="files[0] && !disabled" class="fa fa-trash-alt" (click)="removeFiles($event, this)"></i>
              </button>
            </div>
            <label *ngIf="!!files[0] && files[0].name && files[0].name.trim()"
                   style="word-break: break-word;display: inline-block; max-width: 150px; margin-top: 20px">{{files[0].name}}</label>
          </mat-card-actions>

          <mat-card-content>

          </mat-card-content>
        </mat-card>
        <mat-error *ngIf="sizeError!==null">{{sizeError + ' ' + ('common.is.over-size' | translate)}}</mat-error>
        <mat-error *ngIf="typeError!==null">{{typeError + ' ' + ('common.is.not-accept' | translate)}}</mat-error>
      </div>
    </div>
  `,
  // styleUrls: ['../../styles/ns-style/ns-multi-upload.scss'],
  providers:
    [
      // {
      //   provide: NG_VALUE_ACCESSOR,
      //   useExisting: forwardRef(() => NsUploadFileComponent),
      //   multi: true,
      // },
    ],
})

export class NsUploadFileV2Component implements ControlValueAccessor {

  constructor(private http: HttpClient,
              @Self() @Optional() private ngControl: NgControl,
              private fileSaverService: FileSaverService,
              protected matDialog: MatDialog,
  ) {
    if (ngControl) {
      ngControl.valueAccessor = this;
    }
  }

  @ViewChild('fileUpload', {static: false}) fileUpload?: ElementRef;
  @Input() files: UploadModel[] = [];

  @Input() maxSize: number | null = null;
  @Input() accept: string[] | null = null;
  @Input() label = '';
  @Input() required = '';
  @Input() disabled = false;
  /* percent label outside css */
  @Input() percentOfLabelOutside = 25;

  @Output() onFileChange = new EventEmitter();
  @Output() onFileDelete = new EventEmitter();
  sizeError: string | null = null;
  typeError: string | null = null;

  getSrcImg(): any {
    const file: UploadModel = this.files[0];
    if (!file) {
      return '';
    }
    const src = file.type?.startsWith('image') ? file.previewValue : '';

    return src;
  }

  propagateChange = (_: any) => {
    /*NON-EMPTY FOR COMPILE*/
  }

  writeValue(obj: any): void {
    if (!obj) {
      return;
    }
    if (typeof obj === 'string') {
      this.files = [UploadModel.init(obj)];
    } else {
      this.files = [obj];
    }


    this.propagateChange(this.files[0]);
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
    /*NON-EMPTY FOR COMPILE*/
  }

  onClick() {

    const fileUpload = this.fileUpload?.nativeElement;

    fileUpload.click();
  }

  convertToUploadModel(fileList: FileList): void {
    this.sizeError = null;
    this.typeError = null;
    if (fileList && fileList[0]) {
      if (!this.validFile(fileList)) {
        return;
      }
      for (let i = 0; i < fileList.length; i++) {
        const file: File = fileList[i];
        if (this.isContainFileName(file.name)) {
          continue;
        }
        const reader = new FileReader();
        // console.log('reader', file);
        reader.onload = (event: any) => {

          this.writeValue(new UploadModel(file.name, file, (event.target as FileReader).result, file.type));
          this.onFileChange.emit();

        };
        reader.readAsDataURL(file);

        break;

      }
    }
  }

  isContainFileName(name: string): boolean {
    for (const file of this.files) {
      if (file.name === name) {
        return true;
      }
    }
    return false;
  }

  onFileChangeAction(event: any) {
    const target = event.target as HTMLInputElement;
    if (target && target.files) {

      this.convertToUploadModel(target.files);
    }
  }

  onFileDroppedAction(fileList: FileList) {
    this.convertToUploadModel(fileList);
  }

  validFile(fileList: FileList): boolean {
    const acceptStr = this.accept ? this.accept.join() : '';
    for (let i = 0; i < fileList.length; i++) {
      const file: File = fileList[i];
      if (acceptStr.indexOf(file.type) < 0 || !file.type || file.type === '') {
        this.typeError = file.name;
        return false;
      }
      if (this.maxSize && file.size / 1024 > this.maxSize) {
        this.sizeError = file.name;
        return false;
      }
    }
    return true;
  }

  removeFiles(event: any, control: any) {
    event.stopPropagation();
    if (this.files && this.files.length > 0) {
      this.files.splice(0, 1);
      this.propagateChange(this.files);
      // this.onFileDelete.emit();
    }
    this.onFileDelete.emit(this.ngControl);
  }

  removeFile(event: any, fileUpload: any, index: number) {
    event.stopPropagation();
    // fileUpload.value = '';

    if (this.files && this.files.length > index) {
      this.files.splice(index, 1);
      this.propagateChange(this.files);
      this.onFileDelete.emit();
    }

  }

  previewFiles(event: any, downloadId: any) {
    event.stopPropagation();
    downloadId.click();
  }

  onLoad(img: HTMLImageElement) {
    const parentElement = img.closest('.preview-image') as HTMLElement;
    parentElement.setAttribute('style', 'background-image:url("' + img.src + '")');
  }

  downloadFile() {
    const file = this.files[0] as UploadModel;
    if (!file) {
      return;
    }
    this.http.post(file.previewValue + '', null, {
      headers: undefined,
      params: undefined,
      observe: 'response',
      responseType: 'blob',
    }).subscribe((l: HttpResponse<Blob>) => {
      let filename = '';
      const disposition = l.headers.get('Content-Disposition');
      if (disposition && disposition.indexOf('attachment') !== -1) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
      }
      const blobData: Blob | null = l.body;
      const contentType: string | null = l.headers.get('Content-Type');
      if (blobData && contentType) {
        this.fileSaverService.save(blobData, filename, contentType);
      }
    });
  }
}
