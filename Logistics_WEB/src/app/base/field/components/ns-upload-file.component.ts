import {Component, ElementRef, EventEmitter, Input, Optional, Output, Self, ViewChild} from '@angular/core';
import {ControlValueAccessor, NgControl} from '@angular/forms';
import {UploadModel} from '@next-solutions/next-solutions-base';
import {HttpClient, HttpResponse} from '@angular/common/http';
// import {ViewFilePdfComponent} from "../../components/manufacturer/view-file-pdf/view-file-pdf.component";
import {MatDialog} from "@angular/material/dialog";
import {FileSaverService} from "ngx-filesaver";


@Component({
  selector: 'ns-upload-file',
  template: `
    <div class="ns-multi-upload"
         fxLayout="row" fxLayout.lt-sm="row wrap">
      <mat-label *ngIf="label"
                 fxLayout="column" fxFlex="140px" fxFlex.lt-md="100%">
        <div class="label">{{(label ? label : '') | translate}}<span
          class="required-label-outside">{{!!required ? '*' : ''}}</span></div>
      </mat-label>
      <div class="uploadArea" style="background-color: #FFFFFF"
           fxLayout="column" fxFlex="auto" fxFlex.lt-md="100%"
           appDragDrop (onFileDropped)="onFileDroppedAction($event)">
        <input type="file" #fileUpload (change)="onFileChangeAction($event)"
               [disabled]="disabled"
               accept="{{accept ? accept.join() : '*/*'}}">
        <mat-card>
          <mat-card-actions>
            <div fxLayout="column">
              <button mat-button
                      class="{{(fileType.startsWith('image') && files[0])?'upload-button preview': 'upload-button'}}"
                      color="warn" (click)="onClick()">


                <div class="preview-image"
                     *ngIf="fileType.startsWith('image') && files[0]">
                  <img (load)="onLoad(img)"
                       #img
                       style="display: none"
                       [src]="getSrcImg()| secure | async"
                       *ngIf="fileType.startsWith('image') && files[0]"
                  />
                </div>

                <!--                <div class="preview-image" style="background-image:url({{files[0]?.previewValue}})"-->
                <!--                     *ngIf="fileType.startsWith('image') && files[0]"></div>-->
                <!--                -->


                <i [ngClass]="files[0]?'fa fa-file-alt':'fa fa-plus'"
                   *ngIf="!fileType.startsWith('image')  || !files[0]"></i>
                <span *ngIf="!files[0]">{{('common.button.importExcel' | translate)}}</span>

                <i *ngIf="files[0]" class="fa fa-eye" (click)="previewFiles($event, downloadId)">
                  <a download (click)="downloadFile()" #downloadId style="display:none;"></a>
                </i>
                <i *ngIf="files[0] && !disabled" class="fa fa-trash-alt" (click)="removeFiles($event, this)"></i>
              </button>
            </div>
            <label *ngIf="fileName && fileName.trim()"
                   style="word-break: break-word;display: inline-block; max-width: 150px; margin-top: 20px">{{fileName}}</label>
          </mat-card-actions>

          <mat-card-content>

          </mat-card-content>
        </mat-card>
        <mat-error *ngIf="sizeError!==null">{{sizeError + ' ' + ('common.is.over-size' | translate)}}</mat-error>
        <mat-error *ngIf="typeError!==null">{{typeError + ' ' + ('common.is.not-accept' | translate)}}</mat-error>
      </div>
    </div>
  `,
  // styleUrls: ['../../styles/ns-style/ns-multi-upload.scss'],
  providers:
    [
      // {
      //   provide: NG_VALUE_ACCESSOR,
      //   useExisting: forwardRef(() => NsUploadFileComponent),
      //   multi: true,
      // },
    ],
})

export class NsUploadFileComponent implements ControlValueAccessor {

  constructor(private http: HttpClient,
              @Self() @Optional() private ngControl: NgControl,
              private fileSaverService: FileSaverService,
              protected matDialog: MatDialog,
  ) {
    if (ngControl) {
      ngControl.valueAccessor = this;
    }
  }

  @ViewChild('fileUpload', {static: false}) fileUpload?: ElementRef;
  @Input() files: UploadModel[] = [];

  @Input() maxSize: number | null = null;
  @Input() accept: string[] | null = null;
  @Input() label = '';
  @Input() required = '';
  @Input() disabled = false;
  /* percent label outside css */
  @Input() percentOfLabelOutside = 25;

  @Output() onFileChange = new EventEmitter();
  @Output() onFileDelete = new EventEmitter();
  sizeError: string | null = null;
  typeError: string | null = null;
  fileType = '';
  fileName = '';
  default$?: string;

  @Input() set defaultUrl(fileInfo: { url: string, name: string, type: string }) {
    // if (!fileInfo.url) {
    //   return;
    // }
    this.default$ = fileInfo.url;
    this.fileType = fileInfo.type;
    this.fileName = fileInfo.name;
    if (1 === 1) {
      return;
    }
    const oReq = new XMLHttpRequest();
    oReq.open('GET', fileInfo.url, true);
    oReq.responseType = 'arraybuffer';

    oReq.onload = (event: any) => {
      const arrayBuffer = oReq.response; // Note: not oReq.responseText
      if (arrayBuffer) {
        // Create a blob from the response
        const blob = new Blob([arrayBuffer]);
        // onload needed since Google Chrome doesn't support addEventListener for FileReader
        const fileReader = new FileReader();
        // console.log('reader', file);
        const file = new File([blob], fileInfo.name, {type: fileInfo.type});
        fileReader.onload = (event: any) => {

          this.writeValue(new UploadModel(fileInfo.name, file, (event.target as FileReader).result));


        };
        // Load blob as Data URL
        fileReader.readAsDataURL(file);
      }
    };

    oReq.send(null);

    // this.http.get<Blob>(url).subscribe((value: Blob) => {
    //   const b: any = value;
    //   value.arrayBuffer().then((arr) =>{
    //     b.lastModifiedDate = new Date();
    //     b.name = this.fileName;
    //     const f = new File([value], this.fileName);
    //     this.writeValue(new UploadModel(this.fileName, b, arr));
    //   });
    //
    // });
  }

  getSrcImg(): any {
    const src = this.fileType.startsWith('image') ? ((this.files[0]) ? this.files[0].previewValue : this.default$) : '';

    return src;
  }

  propagateChange = (_: any) => {
    /*NON-EMPTY FOR COMPILE*/
  }

  writeValue(obj: any): void {
    if (!obj) {
      return;
    }
    if (typeof obj === 'string') {

      const obj1: any = {name: this.fileName, binary: null, previewValue: obj};
      this.files = [obj1];
    } else if (typeof obj === 'object') {
      if (obj.binary === null || obj.binary === undefined) {
        const obj1: any = {name: obj.name, binary: null, previewValue: obj.url};
        this.files = [obj1];
        this.fileName = obj.name;
        this.fileType = obj.type ? obj.type : '';
      } else {
        this.fileName = obj.name;
        this.files = [obj];
        this.fileType = obj.binary.type;
      }
    }


    this.propagateChange(this.files);
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
    /*NON-EMPTY FOR COMPILE*/
  }

  onClick() {

    const fileUpload = this.fileUpload?.nativeElement;

    fileUpload.click();
  }

  convertToUploadModel(fileList: FileList): void {
    this.sizeError = null;
    this.typeError = null;
    if (fileList && fileList[0]) {
      if (!this.validFile(fileList)) {
        return;
      }
      for (let i = 0; i < fileList.length; i++) {
        const file: File = fileList[i];
        if (this.isContainFileName(file.name)) {
          continue;
        }
        const reader = new FileReader();
        // console.log('reader', file);
        reader.onload = (event: any) => {

          this.writeValue(new UploadModel(file.name, file, (event.target as FileReader).result));
          this.fileType = file.type;
          this.onFileChange.emit();

        };
        reader.readAsDataURL(file);

        break;

      }
    }
  }

  isContainFileName(name: string): boolean {
    for (const file of this.files) {
      if (file.name === name) {
        return true;
      }
    }
    return false;
  }

  onFileChangeAction(event: any) {
    const target = event.target as HTMLInputElement;
    if (target && target.files) {

      this.convertToUploadModel(target.files);
    }
  }

  onFileDroppedAction(fileList: FileList) {
    this.convertToUploadModel(fileList);
  }

  validFile(fileList: FileList): boolean {
    const acceptStr = this.accept ? this.accept.join() : '';
    for (let i = 0; i < fileList.length; i++) {
      const file: File = fileList[i];
      if (acceptStr.indexOf(file.type) < 0 || !file.type || file.type === '') {
        this.typeError = file.name;
        return false;
      }
      if (this.maxSize && file.size / 1024 > this.maxSize) {
        this.sizeError = file.name;
        return false;
      }
    }
    return true;
  }

  removeFiles(event: any, control: any) {
    event.stopPropagation();
    this.fileName = '';
    this.fileType = '';
    if (this.files && this.files.length > 0) {
      this.files.splice(0, 1);
      this.propagateChange(this.files);
      // this.onFileDelete.emit();
    }
    this.onFileDelete.emit(this.ngControl);
  }

  removeFile(event: any, fileUpload: any, index: number) {
    event.stopPropagation();
    // fileUpload.value = '';
    this.fileName = '';
    this.fileType = '';
    if (this.files && this.files.length > index) {
      this.files.splice(index, 1);
      this.propagateChange(this.files);
      this.onFileDelete.emit();
    }

  }

  previewFiles(event: any, downloadId: any) {
    event.stopPropagation();
    console.log('downloadddddddddddddddd', downloadId);

    downloadId.click();
  }

  onLoad(img: HTMLImageElement) {
    const parentElement = img.closest('.preview-image') as HTMLElement;
    parentElement.setAttribute('style', 'background-image:url("' + img.src + '")');
  }

  downloadFile() {
    const file = this.files[0] as UploadModel;
    if (!file) {
      return;
    }
    this.http.post(file.previewValue + '', null, {
      headers: undefined,
      params: undefined,
      observe: 'response',
      responseType: 'blob',
    }).subscribe((l: HttpResponse<Blob>) => {
      let filename = '';
      const disposition = l.headers.get('Content-Disposition');
      if (disposition && disposition.indexOf('attachment') !== -1) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
      }
      const blobData: Blob | null = l.body;
      const contentType: string | null = l.headers.get('Content-Type');
      if (blobData && contentType) {
        this.fileSaverService.save(blobData, filename, contentType);
      }
    });
  }
}
