import {Component, OnInit} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field.interface';
import {Observable, of} from 'rxjs';
import {debounceTime, startWith, switchMap} from 'rxjs/operators';

import {LDDialogSearchComponent} from '../../../components/dialog-search/l-d-dialog-search.component';
import {MatDialog} from '@angular/material/dialog';
import {HttpParams} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-autocomplete',
  template: `
    <div class="float_label">
      <mat-form-field [formGroup]="group" [appearance]="'outline'">
        <mat-label>{{field?.label | translate}}</mat-label>
        <input formControlName="{{field?.name}}"
               [type]="field?.inputType + ''"
               [placeholder]="(field?.label + '') | translate"
               aria-label="Number"
               matInput
               [matAutocomplete]="auto"
               (keyup.f9)="!field?.readonly? showDialog(): ''"
               (blur)="onBlur()"
               (focus)="onFocus()"
               [readonly]="field?.readonly ? !!field?.readonly : 'false'"
               [required]="!!field?.require"
               [title]="title"
        >
        <mat-autocomplete autoActiveFirstOption #auto="matAutocomplete">
          <mat-option *ngFor="let item of githubAutoComplete$ | async; let index = index" [value]="item[field?.options[0]]"
                      (onSelectionChange)="onchange($event, item)"
                      title="{{getDisplay(item)}}"
          >
            {{getDisplay(item)}}

          </mat-option>
        </mat-autocomplete>
      </mat-form-field>
    </div>
  `,
  styles: [
      `mat-form-field {
      width: 100%;
    }`
  ]
})
export class AutocompleteComponent implements OnInit {
  field: FieldConfigExt | undefined;
  group: FormGroup | undefined;
  formArray: any;
  public githubAutoComplete$: Observable<any> | undefined;
  value: string | undefined;
  private offlineData: any;
  private firstTime: boolean | undefined;
  title = '';
  currentLangCode = '';

  constructor(public dialog: MatDialog,
              public translateService: TranslateService) {
  }

  onFocus() {
    this.clearValue();
  }

  onBlur() {
    this.clearValue();
  }

  private clearValue() {
    if (this.field && this.field.bindingRef && this.field.name)
      if (!this.field.bindingRef[0][this.field.bindingRef[1]] ||
        (this.field.bindingRef[0][this.field.bindingRef[1]][this.field.options[0]] !== this.group?.get([this.field.name])?.value)) {
        this.group?.get([this.field.name])?.setValue(null);
        this.field.bindingRef[0][this.field.bindingRef[1]] = null;
      }
  }

  getDisplay(item: any) {
    let display = '';
    let multiLanguage = '';
    if (this.translateService.currentLang) {
      this.currentLangCode = this.translateService.currentLang;
    } else {
      this.currentLangCode = this.translateService.defaultLang;
    }
    this.field?.options.forEach((option: any) => {
      if (this.field?.descriptions && this.field.descriptions[option]) {
        for (const description of item[this.field.descriptions[option]]) {
          if ((description.language ? description.language.code : description.lang.code) === this.currentLangCode) {
            multiLanguage = description.name;
            break;
          } else {
            multiLanguage = 'null';
          }
        }
      } else {
        multiLanguage = item[option];
      }
      display = display + ' - ' + multiLanguage;
    });
    return display ? display.substring(3) : '';
  }

  onchange(event: any, item: any) {
    if (event.source.selected && this.field && this.field.name && this.field.bindingRef) {
      if (this.field.emit) {
        this.field.emit();
      }
      this.group?.get([this.field.name])?.setValue(this.getDisplay(item));
      this.field.bindingRef[0][this.field.bindingRef[1]] = item;
      this.firstTime = true;
      this.title = this.getDisplay(item);
    }
  }

  showDialog() {
    const params = new HttpParams()
    ;
    const ldCols = [];
    ldCols.push({name: 'stt', type: 'index', label: 'ld.no'});
    this.field?.options.forEach((x: any) => {
      ldCols.push({name: x, type: 'view', label: this.field?.label + '.' + x});
    });
    // ldCols.push({name: 'select', type: 'checkboxAll'});
    const data = {
      params,
      ldCols,
      multiSel: false,
      entity: 'common',
      rowIdName: 'id',
      method: this.field?.collections
    };

    const dialogRef = this.dialog.open(LDDialogSearchComponent, {
      disableClose: false,
      width: '70%',
      maxHeight: '90vh',
      data
    });
    const sub = dialogRef.afterClosed().subscribe(result => {
      if (this.group && this.field && this.field.name && this.field.bindingRef)
        for (const item of Object.keys(result)) {
          this.group.get([this.field.name])?.setValue(result[item][this.field.options[0]]);
          this.field.bindingRef[0][this.field.bindingRef[1]] = result[item];
          break;
        }
      // this.model.addTableData(result);
      // for(const c of this.form.get('quantity') as FormArray){
      //
      // }
      // const myClonedArray = Object.assign([], this.models);
      // this.models = myClonedArray;
    });
  }

  ngOnInit() {
    this.firstTime = true;
    if (this.field)
      if (typeof this.field.collections === 'function') {
        this.field.collections().subscribe((res: any) => {
          this.offlineData = res.content ? res.content : res;
          this.bindingData();
        });
      } else {
        this.offlineData = this.field.collections;
        this.bindingData();
      }

  }

  bindingData() {
    if (this.field && this.group && this.field.name) {
      this.githubAutoComplete$ = this.group.get([this.field.name])?.valueChanges.pipe(
        startWith(''),
        debounceTime(500),
        switchMap((value: any) => {
          if (this.field && this.group && this.field.name) {
            if (this.group.get([this.field.name]) !== null && this.group.get([this.field.name]) !== undefined) {
              const fieldValue = this.group.get([this.field.name])?.value;
              if ((value === null || value === undefined || value === '')
                && (fieldValue !== null && fieldValue !== undefined)) {
                value = fieldValue[this.field.options[0]];
                this.group.get([this.field.name])?.setValue(value);
                return of(null);
              }
              if (value instanceof Object) {
                this.title = this.getDisplay(value);
                value = value[this.field.options[0]];
                this.group.get([this.field.name])?.setValue(value);
                return of(null);
              }
            }
            this.value = value;
            // this.title = value ? value : '';
            return of(value && value.length > 0 ? this._filter(value) : this.offlineData);
          }
          return of([]);
        })
      );
    }

  }

  private _filter(value: any) {
    const filterValue = (value as string).toLowerCase();
    if (this.firstTime) {
      this.firstTime = false;
      return this.offlineData;
    }

    return this.offlineData ? this.offlineData.filter((result: any) => this.getDisplay(result).toLowerCase().includes(filterValue)) : [];
  }
}
