import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field.interface';

@Component({
  selector: 'app-radiobutton',
  template: `
    <div class="demo-full-width margin-top" [formGroup]="group">
      <label class="radio-label-padding">{{(field?.label + '') |translate}}: </label>
      <mat-radio-group [formControlName]="field?.name"
                       (change)="onChange($event)"
                       [value]="field && field.bindingRef ? field.bindingRef[0][field.bindingRef[1]] : ''">
        <mat-radio-button *ngFor="let item of (field?.options ? field?.options : [])"
                          [value]="item.value"
                          [checked]="!!field?.bindingRef && field?.bindingRef[0][field?.bindingRef[1]] === item.value">
          {{item.label |translate}}
        </mat-radio-button>
      </mat-radio-group>
    </div>
  `,
  styles: [`
    mat-radio-group {
      display: inline-table;
    }

    mat-radio-button {
      display: block;
      padding-bottom: 0.2rem;
    }
  `]
})

export class RadiobuttonComponent implements OnInit {
  field?: FieldConfigExt;
  group?: FormGroup;

  constructor() {
  }

  ngOnInit() {
  }

  onChange($event: any) {
    if (this.field?.bindingRef) {
      this.field.bindingRef[0][this.field.bindingRef[1]] = $event.value;
      if (this.field.emit) {
        this.field.emit($event, this.field);
      }
    }
  }
}
