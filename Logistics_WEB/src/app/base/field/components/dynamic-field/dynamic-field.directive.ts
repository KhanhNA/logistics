import {ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfig} from '../../field.interface';
import {InputComponent} from '../input.component';
import {ButtonComponent} from '../button.component';
import {SelectComponent} from '../select.component';
import {DateComponent} from '../date.component';
import {RadiobuttonComponent} from '../radiobutton.component';
import {CheckboxComponent} from '../checkbox.component';
import {FileImgComponent} from '../file-img.component';
import {AutocompleteComponent} from '../autocomplete.component';
import {ReadComponent} from '../read.component';

const componentMapper: any = {
  index: ReadComponent,
  view: ReadComponent,
  input: InputComponent,
  button: ButtonComponent,
  select: SelectComponent,
  date: DateComponent,
  radiobutton: RadiobuttonComponent,
  checkbox: CheckboxComponent,
  fileImg: FileImgComponent,
  autocomplete: AutocompleteComponent,
};

@Directive({
  selector: '[dynamicField]'
})
export class DynamicFieldDirective implements OnInit {
  @Input() field: FieldConfig | undefined;
  @Input() group: FormGroup | undefined;
  @Input() readonly: boolean | undefined;
  componentRef: any;

  constructor(private resolver: ComponentFactoryResolver,
              private container: ViewContainerRef) {
  }

  ngOnInit(): void {
    try {
      if (!this.field) {
        return;
      }
      if (!this.field.type) {
        this.field.type = 'view';
      }
      const factory = this.resolver.resolveComponentFactory(
        componentMapper[this.field.type]
      );
      this.componentRef = this.container.createComponent(factory);
      this.componentRef.instance.field = this.field;
      this.componentRef.instance.group = this.group;
      this.componentRef.instance.readonly = this.readonly;

    } catch (e) {
      console.log(e, this.field);
    }


  }
}
