import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfigExt} from '../field.interface';
import {TsUtilsService} from '../../Utils/ts-utils.service';
import {DatePipe} from '@angular/common';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-read',
  template: `
    <div title="{{(!!field && !field.bindingRef || (!!field?.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] === null : true)) ? '' :
      field?.inputType === 'date' ? (viewDate) :
        (field?.inputType === 'datetime' ? ((field?.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] : '') | date: 'dd/MM/yyyy HH:mm:ss') :
          field?.inputType === 'number' ? ((!!field?.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] : '') | number) :
            field?.inputType === 'select' ? ((!!field?.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] : '') | translate) :
              field?.inputType === 'autocomplete' ? ((!!field?.bindingRef && !!field?.options) ? field?.bindingRef[0][field?.bindingRef[1]][field?.options[0]] : '') :
                field?.inputType === 'fileImgBase64' ? '' :
                  (!!field?.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] : ''))
      }}">
      {{(field && !field.bindingRef || field?.bindingRef[0][field?.bindingRef[1]] === null) ? '' :
      field?.inputType === 'date' ? (viewDate) :
        (field?.inputType === 'datetime' ? ((field && field.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] : '') | date: 'dd/MM/yyyy HH:mm:ss') :
          field?.inputType === 'number' ? ((field && field.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] : '') | number) :
            field?.inputType === 'select' ? ((field && field.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] : '') | translate) :
              field?.inputType === 'autocomplete' ? ((field && field.bindingRef && field.options) ? field?.bindingRef[0][field?.bindingRef[1]][field?.options[0]] : '') :
                field?.inputType === 'fileImg' ? '' :
                  field?.inputType === 'fileImglocal' ? '' :
                    field?.inputType === 'fileImgBase64' ? '' :
                      (field && field.bindingRef ? field.bindingRef[0][field.bindingRef[1]] : ''))
      }}
      <img [src]="baseURL + (field && field.bindingRef && field.options) ? field?.bindingRef[0][field?.options[0]] : ''"
           *ngIf="field?.inputType === 'fileImg'">
      <img [src]="(field && field.bindingRef && field.options) ? field?.bindingRef[0][field?.options[0]] : ''"
           *ngIf="field?.inputType === 'fileImglocal'">
      <img [src]="'data:image/png;base64,'+(field && field.bindingRef ? field?.bindingRef[0][field?.bindingRef[1]] : '')"
           *ngIf="field?.inputType === 'fileImgBase64'">
    </div>`,
  styles: [],
  providers: [
    DatePipe,
  ]
})
export class ReadComponent implements OnInit {
  field?: FieldConfigExt;
  group?: FormGroup;
  baseURL = environment.BASE_URL + '/files/';
  viewDate?: string | null;


  constructor(private tsuService: TsUtilsService, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.formatDate();
  }

  formatDate() {
    if (this.field && this.field.bindingRef) {
      switch (this.field.inputType) {
        case 'date':
          this.viewDate = this.datePipe.transform(this.field.bindingRef[0][this.field.bindingRef[1]],
            environment.DIS_DATE_FORMAT, '-0');
          return;
        default:
          return this.field.bindingRef[0][this.field.bindingRef[1]];
      }
    }
  }
}
