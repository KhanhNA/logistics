import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldConfig} from '../field.interface';

@Component({
  selector: 'app-button',
  template: `
    <div class="demo-full-width margin-top" [formGroup]="group">
      <button [id]='field?.name' [type]="field?.inputType" mat-raised-button color="warn"
              (click)="field?.emit()"
      >{{(field?.label ? field?.label : '')|translate}}</button>
    </div>
  `,
  styles: []
})
export class ButtonComponent implements OnInit {
  field: FieldConfig | undefined;
  group: FormGroup | undefined;

  constructor() {
  }

  ngOnInit() {
  }
}
