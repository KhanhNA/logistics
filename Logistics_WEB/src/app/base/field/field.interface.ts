import {FormGroup} from '@angular/forms';

export interface Validator {
  name: string;
  validator: any;
  message: string;
  type?: string;
  options?: any;
}

export class FieldConfig {
  label?: string;
  name?: string;
  inputType?: string;
  options?: any;
  collections?: any;
  type?: string;
  value?: any;
  require?: 'true' | 'false';
  validations?: Validator[];
  form?: FormGroup;
  multiLanguage?: boolean;
  binding?: any;
  emit?: any;
  class?: string;
  readonly?: 'true' | 'false';
  authorities?: any;
  fieldSet?: any;
  titles?: any;
  descriptions?: any;
}

export class FieldConfigExt extends FieldConfig {
  bindingRef?: [any, string];
  dataName?: string;

  constructor(f: FieldConfig) {
    super();
    this.label = f.label;
    this.name = f.name;
    this.inputType = f.inputType;
    this.options = f.options;
    this.collections = f.collections;
    this.type = f.type;
    if (f.require) {
      this.require = f.require;
    } else {
      this.require = 'false';
    }
    this.validations = f.validations;
    this.value = f.value;
    this.form = f.form;
    if (f.multiLanguage) {
      this.multiLanguage = f.multiLanguage;
    } else {
      this.multiLanguage = false;
    }
    this.binding = f.binding;
    this.emit = f.emit;
    this.class = f.class;
    this.readonly = f.readonly;
    this.authorities = f.authorities;
    this.fieldSet = f.fieldSet;
    this.titles = f.titles;
    this.descriptions = f.descriptions;
  }


  set bindingRefs(a: [any, string]) {
    this.bindingRef = a;
    if (this.bindingRef && !['button', 'view', 'index'].includes(this.type ? this.type : '')
      && this.name && this.form?.get([this.name])) {
      this.form?.get([this.name])?.setValue(this.bindingRef[0][this.bindingRef[1]]);
    }
  }
}

