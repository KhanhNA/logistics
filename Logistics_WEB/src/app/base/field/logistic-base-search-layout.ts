import {BaseSearchLayout, ColumnFields, Page, Paging, SuperEntity} from '@next-solutions/next-solutions-base';
import {HttpParams} from '@angular/common/http';
import {MatTableDataSource} from '@angular/material/table';
import {ModelUtils} from '../../_models/ModelUtils';


export class LogisticBaseSearchLayout extends BaseSearchLayout {

  // _fillData(nativeUrl: string, params: HttpParams) {
  //   params = params.append('page', this.isResetPaging ? '0' : (this.paging ? (this.paging.pageNumber - 1).toString() : '0'))
  //     .append('size', this.paging ? this.paging.pageSize.toString() : this.config.PAGE_SIZE.toString());
  //   this.apiService.get<Page>(nativeUrl, params)
  //     .subscribe((data: Page) => {
  //       this.isResetPaging = false;
  //       if (data.content && data.content.length > 0) {
  //         this.columns.forEach((column: ColumnFields) => {
  //           column.isShowHeader = false;
  //         });
  //       }
  //
  //       this.results = new MatTableDataSource<SuperEntity>(data.content);
  //       this.paging = new Paging();
  //       this.paging.pageSize = data.size ? data.size : 0;
  //       this.paging.pageNumber = (data.number ? data.number : 0) + 1;
  //       this.paging.totalElements = data.totalElements ? data.totalElements : 0;
  //       this.afterSearch(data);
  //     });
  // }

  get modelUtils(): ModelUtils {
    return ModelUtils.getInstance();
  }

  getSelectValue(value: any): any {
    if (!value || value === this.modelUtils.getSelectAllValue()) {
      return '';
    }
    return value;
  }
}
