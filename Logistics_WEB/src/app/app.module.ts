import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {A11yModule} from '@angular/cdk/a11y';
import {BidiModule} from '@angular/cdk/bidi';
import {ObserversModule} from '@angular/cdk/observers';
import {OverlayModule} from '@angular/cdk/overlay';
import {PlatformModule} from '@angular/cdk/platform';
import {PortalModule} from '@angular/cdk/portal';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {MultilanguagePanigator} from './_helpers/multilanguage.paginator';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {ErrorInterceptor} from './_helpers/error.interceptor';
import {NavService} from './_services/nav.service';
import {MenuListItemComponent} from './_helpers/menu-list-item/menu-list-item.component';
import {TopNavComponent} from './_helpers/top-nav/top-nav.component';
import {LogoutComponent} from './logout/logout.component';
import {AppRoutingModule} from './modules/app-routing.module';
import {ToastrModule} from 'ngx-toastr';
import {JsogService} from 'jsog-typescript';
import {CommunicationService} from './communication-service';
import {DatePipe, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {GlobalErrorHandler} from './_services/errorHandle/global-error-handler';
import {MerchantOrderDetailComponent} from './components/merchant-order/detail/merchant.order.detail.component';
import {DividePackingComponent} from './components/divide-packing/divide-packing.component';
import {StoreInventoryPalletDetailsComponent} from './components/store/inventory/pallet-details/store-inventory-pallet-details.component';
import {ExpireDateColorsUtils} from './base/Utils/ExpireDateColorsUtils';
import {UiStateService} from './_services/ui.state.service';
import {environment} from '../environments/environment';
import {BrowserModule} from '@angular/platform-browser';
import {JsogHttpInterceptor, LoaderInterceptor, NextSolutionsModules} from '@next-solutions/next-solutions-base';
import {BusinessComponent} from './modules/business.component';
import {StoresInventoryIgnoreExpireDateComponent} from './components/dialog-select-product-packing/stores-inventory-ignore-expire-date.component';
import {ProductPackingsComponent} from './components/dialog-select-product-packing/product-packings.component';
import {PalletsProductPackingGroupByExpireDateComponent} from './components/dialog-select-product-packing/pallets-product-packing-group-by-expire-date.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MerchantComponent} from './components/store/merchant/merchant.component';
import {PlanComponent} from './components/repacking/a-e-repacking/plan/plan.component';
import {ThonvComponent} from './modules/thonv.component';
import {ThonvEntryComponent} from './modules/thonv.entry.component';
import {AuthenticationService} from './_services/authentication.service';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SharedModule} from './modules/shared.module';
import {TranslateLoaderFactoryHelper} from './_helpers/TranslateLoaderFactoryHelper';
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { AddEditPalletComponent } from './components/pallet/add-edit-pallet/add-edit-pallet.component';
import {CommonHttpService} from './_services/http/common/common.http.service';
import { WarehouseListComponent } from './components/goods-bonus/warehouse-list/warehouse-list.component';
import { WarehouseOrderComponent } from './components/goods-bonus/warehouse-order/warehouse-order.component';

@NgModule({
  exports: [
    // CDK
    A11yModule,
    BidiModule,
    ObserversModule,
    OverlayModule,
    PlatformModule,
    PortalModule,
    ScrollingModule,
    CdkStepperModule,
    CdkTableModule
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    MenuListItemComponent,
    TopNavComponent,
    DashboardComponent,

    ThonvComponent,
    BusinessComponent,
    AddEditPalletComponent,
    WarehouseListComponent,
    WarehouseOrderComponent,

  ],
  // Mấy ông mà gọi Modal là phải cho vào đây nhé @@
  entryComponents: [
    LoginComponent,
    ThonvEntryComponent,
    MerchantOrderDetailComponent,
    DividePackingComponent,
    StoreInventoryPalletDetailsComponent,
    StoresInventoryIgnoreExpireDateComponent,
    ProductPackingsComponent,
    PalletsProductPackingGroupByExpireDateComponent,
    MerchantComponent,
    PlanComponent,
    ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    ToastrModule.forRoot(), // ToastrModule added
    TranslateModule.forRoot({
      defaultLanguage: environment.DEFAULT_LANGUAGE,
      loader: {
        provide: TranslateLoader,
        useClass: TranslateLoaderFactoryHelper.forModule(),
        deps: [HttpClient],
      },
      isolate: false,
    }),
    NextSolutionsModules.forRoot({
      data: {
        BASE_URL: environment.BASE_URL,
        BASE_AUTHORIZATION_URL: environment.BASE_AUTHORIZATION_URL,
        PAGE_SIZE: environment.PAGE_SIZE,
        PAGE_SIZE_OPTIONS: environment.PAGE_SIZE_OPTIONS,
        API_DATE_FORMAT: environment.API_DATE_FORMAT,
        DIS_DATE_FORMAT: environment.DIS_DATE_FORMAT,
        DIALOG_LOGO: '' /*environment.DIALOG_LOGO*/
      }
    }),
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JsogHttpInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},
    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator},
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: ErrorHandler, useClass: GlobalErrorHandler},
    {provide: MAT_DIALOG_DATA, useValue: {}},
    {provide: MatDialogRef, useValue: {}},
    JsogService,
    DatePipe,
    CookieService,
    LoginComponent,
    AuthenticationService,
    NavService,

    CommunicationService,
    ExpireDateColorsUtils,
    UiStateService,
    CommonHttpService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})

export class AppModule {
}
