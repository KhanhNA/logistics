import {Injectable} from "@angular/core";
import {AuthoritiesService} from "@next-solutions/next-solutions-base";

@Injectable()
export class ExportStatementAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasCreateExportStatementRole() {
    return this.authoritiesService.hasAuthority('post/export-statements');
  }

  hasGetExportStatementRole() {
    return this.authoritiesService.hasAuthority('get/export-statements/{id}');
  }

  hasUpdateExportStatementRole() {
    return this.authoritiesService.hasAuthority('patch/export-statements/{id}') && this.hasGetExportStatementRole();
  }

  hasCreateOrUpdateExportStatementRole() {
    return this.hasCreateExportStatementRole() || this.hasUpdateExportStatementRole();
  }

  hasCancelDetailExportStatementRole() {
    return this.authoritiesService.hasAuthority('post/export-statements/cancel-detail/{id}');
  }

  hasCancelExportStatementRole() {
    return this.authoritiesService.hasAuthority('post/export-statements/cancel/{id}');
  }

  hasCreateFromMerchantOrderExportStatementRole() {
    return this.authoritiesService.hasAuthority('post/export-statements/from-merchant-order');
  }

  hasPrintExportStatementRole() {
    return this.authoritiesService.hasAuthority('post/export-statements/print/{id}');
  }

  hasPrintPdfRole() {
    return this.authoritiesService.hasAuthority('post/export-statements/print-pdf/{id}');
  }

}
