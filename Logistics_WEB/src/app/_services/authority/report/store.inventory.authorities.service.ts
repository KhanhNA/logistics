import {Injectable} from '@angular/core';
import {AuthoritiesService} from '@next-solutions/next-solutions-base';

@Injectable()
export class StoreInventoryAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasTKFCStoreInventoryReportRole() {
    return this.authoritiesService.hasAuthority('post/claims');
  }
}
