import {Injectable} from "@angular/core";
import {AuthoritiesService} from "@next-solutions/next-solutions-base";

@Injectable()
export class ImportPoAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasCreateImportPoRole() {
    return this.authoritiesService.hasAuthority('post/import-pos');
  }

  hasGetImportPoRole() {
    return this.authoritiesService.hasAuthority('get/import-pos/{id}');
  }

  hasUpdateImportPoRole() {
    return this.authoritiesService.hasAuthority('patch/import-pos/{id}') && this.hasGetImportPoRole();
  }

  hasCreateOrUpdateImportPoRole() {
    return this.hasCreateImportPoRole() || this.hasUpdateImportPoRole();
  }


  hasUpdatePalletImportPoRole() {
    return this.authoritiesService.hasAuthority('post/import-pos/update-pallet/{id}');
  }

}
