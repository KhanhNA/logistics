import {Injectable} from '@angular/core';
import {AuthoritiesService} from '@next-solutions/next-solutions-base';

@Injectable({
  providedIn: 'root'
})
export class UserAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasCreateUserRole() {
    return this.authoritiesService.hasAuthority('post/users');
  }

  hasGetUserRole() {
    return this.authoritiesService.hasAuthority('get/users/{username}');
  }

  hasUpdateUserRole() {
    return this.authoritiesService.hasAuthority('patch/users') && this.hasGetUserRole();
  }

  hasCreateOrUpdateUserRole() {
    return this.hasCreateUserRole() || this.hasUpdateUserRole();
  }

  hasResetPasswordUserRole() {
    return this.authoritiesService.hasAuthority('post/users/reset-pass');
  }

  hasActivateUserRole() {
    return this.authoritiesService.hasAuthority('post/users/active');
  }

  hasDeactivateUserRole() {
    return this.authoritiesService.hasAuthority('post/users/deactivate');
  }

  hasActivateOrDeactivateUserRole() {
    return this.hasActivateUserRole() || this.hasDeactivateUserRole();
  }
}
