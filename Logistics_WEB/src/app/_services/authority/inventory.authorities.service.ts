import {AuthoritiesService} from "@next-solutions/next-solutions-base";
import {Injectable} from "@angular/core";

@Injectable()
export class InventoryAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasGetMerchantOrderInventory() {
    return this.authoritiesService.hasAuthority('get/merchant-orders/get-inventory');
  }
}
