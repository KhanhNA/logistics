import {Injectable} from "@angular/core";
import {AuthoritiesService} from "@next-solutions/next-solutions-base";

@Injectable()
export class PartnerAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasCreateShippingPartnerRole() {
    return this.authoritiesService.hasAuthority('post/shipping-partners');
  }

  hasGetShippingPartnerRole() {
    return this.authoritiesService.hasAuthority('get/shipping-partners/{id}');
  }

  hasUpdateShippingPartnerRole() {
    return this.authoritiesService.hasAuthority('patch/shipping-partners/{id}') && this.hasGetShippingPartnerRole();
  }

  hasApproveShippingPartnerRole() {
    return this.authoritiesService.hasAuthority('patch/shipping-partners/{id}/accept') && this.hasGetShippingPartnerRole();
  }

  hasRejectShippingPartnerRole() {
    return this.authoritiesService.hasAuthority('patch/shipping-partners/{id}/reject') && this.hasGetShippingPartnerRole();
  }


  hasCreateOrUpdateShippingPartnerRole() {
    return this.hasCreateShippingPartnerRole() || this.hasUpdateShippingPartnerRole();
  }

}
