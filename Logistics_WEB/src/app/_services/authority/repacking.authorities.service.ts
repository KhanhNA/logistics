import {Injectable} from "@angular/core";
import {AuthoritiesService} from "@next-solutions/next-solutions-base";

@Injectable()
export class RepackingAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasCreateRepackingRole() {
    return this.authoritiesService.hasAuthority('post/repacking-plannings');
  }

  hasGetRepackingRole() {
    return this.authoritiesService.hasAuthority('get/repacking-plannings/{id}');
  }

  hasRepackRepackingRole() {
    return this.authoritiesService.hasAuthority('post/repacking-plannings/repack/{id}');
  }

  hasPrintRepackingRole() {
    return this.authoritiesService.hasAuthority('post/repacking-plannings/print/{id}');
  }

  hasPrintQRCodeRole() {
    return this.authoritiesService.hasAuthority('post/repacking-plannings/print-qrcode/{id}');
  }

  hasGetImportStatementFromRepackingRole() {
    return this.authoritiesService.hasAuthority('get/import-statements/repacking-planning/{id}');
  }

  hasViewRepackingRole() {
    return this.hasGetRepackingRole() || this.hasGetImportStatementFromRepackingRole();
  }

  hasUpdateRepackingRole() {
    return this.authoritiesService.hasAuthority('patch/repacking-plannings/{id}') && this.hasGetRepackingRole();
  }

  hasCreateImportStatementFromRepackingRole() {
    return this.authoritiesService.hasAuthority('post/repacking-plannings/cancel-plan/{id}') && this.hasGetRepackingRole();
  }

  hasCreateOrUpdateRepackingRole() {
    return this.hasCreateRepackingRole() || this.hasUpdateRepackingRole();
  }


  hasCancelRepackingRole() {
    return this.authoritiesService.hasAuthority('post/repacking-plannings/cancel-repack/{id}')
      || this.authoritiesService.hasAuthority('post/repacking-plannings/cancel-plan/{id}');
  }

  // hasPrintPdfRepackingRole() {
  //   return this.authoritiesService.hasAuthority('post/repacking-plannings/{id}/print-pdf');
  // }
  //
  // hasUpdateImageDetailRole() {
  //   return this.authoritiesService.hasAuthority('patch/repacking-plannings/{id}/update-image');
  // }
}
