import {Injectable} from "@angular/core";
import {AuthoritiesService} from "@next-solutions/next-solutions-base";

@Injectable({
  providedIn: 'root'
})
export class PalletAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasCreatePalletRole() {
    return this.authoritiesService.hasAuthority('post/pallets');
  }

  hasGetPalletRole() {
    return this.authoritiesService.hasAuthority('get/pallets/{id}');
  }

  hasUpdatePalletRole() {
    return this.authoritiesService.hasAuthority('patch/pallets/{id}') && this.hasGetPalletRole();
  }

  hasCreateOrUpdatePalletRole() {
    return this.hasCreatePalletRole() || this.hasUpdatePalletRole();
  }

}
