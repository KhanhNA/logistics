import {Injectable} from '@angular/core';
import {AuthoritiesService} from '@next-solutions/next-solutions-base';

@Injectable()
export class PoAuthoritiesService {

  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasGetPoRole(): boolean {
    return this.authoritiesService.hasAuthority('get/pos/{id}');
  }

  hasCreatePoRole(): boolean {
    return this.authoritiesService.hasAuthority('post/pos');
  }

  hasUpdatePoRole(): boolean {
    return this.authoritiesService.hasAuthority('patch/pos/{id}') && this.hasGetPoRole();
  }

  hasUpdateArriveFcDateRole() {
    return this.authoritiesService.hasAuthority('patch/pos/{id}/arrive-fc') && this.hasGetPoRole();
  }

  hasUpdateArriveMyanmarDateRole() {
    return this.authoritiesService.hasAuthority('patch/pos/{id}/arrive-myanmar-port') && this.hasGetPoRole();
  }

  hasUpdateArriveVietnamDateRole() {
    return this.authoritiesService.hasAuthority('patch/pos/{id}/arrive-vietnam-port') && this.hasGetPoRole();
  }

  hasUpdateRealArrivedMyanmarDateRole() {
    return this.authoritiesService.hasAuthority('patch/pos/{id}/arrived-myanmar-port') && this.hasGetPoRole();
  }

  hasUpdateRealArrivedVietnamDateRole() {
    return this.authoritiesService.hasAuthority('patch/pos/{id}/arrived-vietnam-port') && this.hasGetPoRole();
  }

  hasUpdateDeleveryDate() {
    return this.hasUpdateArriveVietnamDateRole() || this.hasUpdateRealArrivedVietnamDateRole()
      || this.hasUpdateArriveMyanmarDateRole() || this.hasUpdateRealArrivedMyanmarDateRole()
      || this.hasUpdateArriveFcDateRole();
  }

  hasCreateOrUpdatePoRole(): boolean {
    return this.hasUpdatePoRole() || this.hasCreatePoRole();
  }

  hasCreateAndUpdatePoRole(): boolean {
    return this.hasUpdatePoRole() && this.hasCreatePoRole();
  }

  hasPrintPoRole() {
    return this.authoritiesService.hasAuthorities(['post/pos/print']);
  }

  hasPrintPoDetailRole() {
    return this.authoritiesService.hasAuthorities(['post/pos/print/{id}']);
  }

  hasPrintPDFPoRole() {
    return this.authoritiesService.hasAuthority('post/pos/print-pdf/{id}');
  }

  hasGoodReceivePoRole() {
    return this.authoritiesService.hasAuthority('post/import-pos');
  }

  hasApprovePoRole(): boolean {
    return this.authoritiesService.hasAuthority('patch/pos/approve/{id}') && this.hasGetPoRole();
  }

  hasRejectPoRole(): boolean {
    return this.authoritiesService.hasAuthority('patch/pos/reject/{id}') && this.hasGetPoRole();
  }

  hasApproveOrRejectPoRole(): boolean {
    return this.hasApprovePoRole() || this.hasRejectPoRole();
  }

}
