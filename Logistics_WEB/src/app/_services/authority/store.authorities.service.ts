import {Injectable} from "@angular/core";
import {AuthoritiesService} from "@next-solutions/next-solutions-base";

@Injectable({
  providedIn: 'root'
})
export class StoreAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasCreateStoreRole() {
    return this.authoritiesService.hasAuthority('post/stores');
  }

  hasGetStoreRole() {
    return this.authoritiesService.hasAuthority('get/stores/{id}');
  }

  hasUpdateStoreRole() {
    return this.authoritiesService.hasAuthority('patch/stores/{id}') && this.hasGetStoreRole();
  }

  hasCreateOrUpdateStoreRole() {
    return this.hasCreateStoreRole() || this.hasUpdateStoreRole();
  }

  hasActivateStoreRole() {
    return this.authoritiesService.hasAuthority('patch/stores/activate/{id}');
  }

  hasDeactivateStoreRole() {
    return this.authoritiesService.hasAuthority('patch/stores/deactivate/{id}');
  }

  hasActivateOrDeactivateStoreRole() {
    return this.hasActivateStoreRole() || this.hasDeactivateStoreRole();
  }
}
