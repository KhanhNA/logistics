import {Injectable} from "@angular/core";
import {AuthoritiesService} from "@next-solutions/next-solutions-base";

@Injectable()
export class ClaimAuthoritiesService {
  constructor(private authoritiesService: AuthoritiesService) {
  }

  hasCreateClaimRole() {
    return this.authoritiesService.hasAuthority('post/claims');
  }

  hasGetClaimRole() {
    return this.authoritiesService.hasAuthority('get/claims/{id}');
  }

  hasUpdateClaimRole() {
    return this.authoritiesService.hasAuthority('patch/claims/{id}') && this.hasGetClaimRole();
  }

  hasCreateOrUpdateClaimRole() {
    return this.hasCreateClaimRole() || this.hasUpdateClaimRole();
  }

  hasApproveClaimRole() {
    return this.authoritiesService.hasAuthority('patch/claims/accept/{id}');
  }

  hasRejectClaimRole() {
    return this.authoritiesService.hasAuthority('patch/claims/reject/{id}');
  }

  hasGetClaimByReferenceRole() {
    return this.authoritiesService.hasAuthority('get/claims/by-reference');
  }

  hasPrintPdfClaimRole() {
    return this.authoritiesService.hasAuthority('post/claims/{id}/print-pdf');
  }

  hasUpdateImageDetailRole() {
    return this.authoritiesService.hasAuthority('patch/claims/{id}/update-image');
  }
}
