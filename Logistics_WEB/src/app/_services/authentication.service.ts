import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpRequest} from '@angular/common/http';
import {OAuth2AuthenticationDto, Page} from '@next-solutions/next-solutions-base';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) {
  }

  login(loginPayload: any) {
    const headers = {
      'Content-type': 'application/x-www-form-urlencoded'
    };
    return this.http.post(environment.BASE_AUTHORIZATION_URL + '/oauth/token', loginPayload, {headers});
  }

  isAuthenticated() {
    if (window.sessionStorage.getItem('token') != null) {
      return true;
    }
    return false;
  }

}
