import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ApiService, FlatTreeNode, SelectModel} from '@next-solutions/next-solutions-base';
import {Injectable} from '@angular/core';

@Injectable()
export class SharedService {
  constructor(protected apiService: ApiService) {
  }

  async getLocationTree() {
    const regionOptions = await this.apiService.get('/countries/locations/flat-tree-node',
      new HttpParams()).toPromise() as FlatTreeNode[];
    return regionOptions;
  }
}
