import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FormGroup} from '@angular/forms';

@Injectable()
export class UiStateService {
  private subject = new BehaviorSubject<UIState | undefined>(undefined);
  uiState$ = this.subject.asObservable();

  constructor() {
    console.log(this.uiState$.subscribe((ui: any) =>{
      console.log(ui);
    }));
  }

  setUiState$(uiState: UIState) {
    this.subject.next(uiState);
  }
}

export class UIState {
  form?: FormGroup;
  // formData: any;
  formDataMap: Map<string, any> | undefined;
  selectedRowIndex?: number;
}
