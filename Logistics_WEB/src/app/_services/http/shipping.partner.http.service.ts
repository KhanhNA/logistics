import {Injectable} from "@angular/core";
import {ApiService, Page} from "@next-solutions/next-solutions-base";
import {HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {partnerStatus} from "../../_models/partner/partnerStatus";
import {CommonUtils} from "../../_utils/common.utils";

@Injectable()
export class ShippingPartnerHttpService {
  constructor(private apiService: ApiService) {
  }

  createGetShippingPartnerAvailableObs(): Observable<Page> {
    const params = new HttpParams()
      .set('status', partnerStatus.APPROVED.code)
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '')
      .set('pageNumber', '1')
    ;
    return this.apiService.get<Page>('/shipping-partners', params);
  }
}
