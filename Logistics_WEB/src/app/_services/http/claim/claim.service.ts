import {Injectable} from '@angular/core';
import {ApiService} from '@next-solutions/next-solutions-base';
import {ClaimEnum} from '../../../_models/action-type';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ClaimDetailModel} from "../../../_models/claim.detail.model";

@Injectable()
export class ClaimService {

  constructor(private apiService: ApiService) {
  }

  getClaimByReferenceObs(referenceId: number, type: ClaimEnum): Observable<ClaimDetailModel[] | any> {
    const params = new HttpParams()
      .set('type', type)
      .set('referenceId', '' + referenceId);
    return this.apiService.get<ClaimDetailModel[] | any>('/claims/by-reference', params);
  }
}
