import {Injectable} from "@angular/core";
import {ApiService} from "@next-solutions/next-solutions-base";
import {HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class MerchantOrderHttpService {
  constructor(private apiService: ApiService) {
  }

  createGetMerchantOrderInventoryObs(packingCodes: string[] = [], storeCodes: string[] = []): Observable<any> {
    const params = new HttpParams()
      .set('productPackingCodes', packingCodes.join(', '))
      .set('storeCodes', storeCodes.join(', '));
    return this.apiService.get('/merchant-orders/get-inventory', params);
  }
}
