import {ApiService, Page} from '@next-solutions/next-solutions-base';
import {Observable, of} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {CommonUtils} from "../../../_utils/common.utils";
import {DistributorModel} from "../../../_models/distributor.model";
import {ActionTypeEneum} from "../../../_models/action-type";
import {RepackingPlanningModel} from "../../../_models/repacking.planning.model";

@Injectable()
export class CommonHttpService {
  constructor(private apiService: ApiService) {
  }

  createGetQuantityPackingExpireDateObs(repackingPlan: RepackingPlanningModel | undefined): Observable<Page> {
    if (!repackingPlan) {
      return of(new Page());
    }
    let params = new HttpParams()
      .set('storeId', repackingPlan.store?.id + '')
      .set('distributorId', repackingPlan.distributor?.id + '')
      // .set('text', '')
      .set('pageNumber', '1')
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '')
      .set('isIncludeProductPackingIdExpireDate', 'true')
      .set('palletSteps', '1');
    params = params.set('repackingPlanningId', '' + repackingPlan.id);
    params = params.set('-1', '');
    for (const item of repackingPlan.repackingPlanningDetails) {
      params = params.append('' + item.productPacking?.id, item.expireDate + '');
    }

    return this.apiService.get('/pallets/product-packing-group-by-expire-date', params);
  }

  createGetAllDistributorObservable(): Observable<DistributorModel[]> {
    return this.apiService.get<DistributorModel[]>('/distributors/all', new HttpParams());
  }

  createGetAllStoreAvailableManagementObservable(): Observable<Page> {
    const params = new HttpParams()
      .set('status', 'true')
      // .set('text', '')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('is_DC', '')
      .set('pageNumber', '1')
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '');
    return this.apiService.get('/stores', params);
  }

  createGetFCStoreAvailableObservable(): Observable<Page> {
    const params = new HttpParams().set('status', 'true')
      // .set('text', '')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('is_DC', 'false')
      .set('pageNumber', '1')
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '');
    return this.apiService.get('/stores', params);
  }

  createGetAllDCStoreManagementObservable(): Observable<Page> {
    const params = new HttpParams().set('status', 'true')
      // .set('text', '')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('is_DC', 'true')
      .set('pageNumber', '1')
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '');
    return this.apiService.get('/stores', params);
  }

  createFindAllProductPackingInPoObs(params: {
    manufacturerId: number | undefined;
    distributorId: number | undefined;
  }): Observable<Page> {
    const httpParams = new HttpParams()
      .set('manufacturerId', '' + params.manufacturerId?.toString())
      .set('distributorId', '' + params.distributorId?.toString())
      .set('exceptId', '-1')
      // .set('text', '')
      .set('pageNumber', '1')
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '');
    return this.apiService.get('/product-packings', httpParams);
  }

  createGetPalletObs(storeId: number | undefined | null, step: number | undefined, status: boolean | undefined): Observable<Page> {
    if (!step) {
      step = 1;
    }
    if (!status) {
      status = true;
    }
    const params = new HttpParams()
      .set('storeId', storeId ? storeId + '' : '')
      // .set('text', '')
      .set('step', step + '')
      .set('status', status + '')
      .set('pageNumber', '1')
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '');
    const method = this.apiService.get<Page>('/pallets', params);
    return method;
  }

  createGetManufacturerObservable(): Observable<any> {
    const manufacturerParams = new HttpParams()
      // .set('text', '')
      .set('status', '1')
      .set('pageNumber', '1')
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '');
    return this.apiService.get('/manufacturers', manufacturerParams);
  }
}
