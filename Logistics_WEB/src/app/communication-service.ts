import {Injectable} from '@angular/core';

@Injectable()
export class CommunicationService {
  // 1
  data: any = {};

  push(id: any, value: any) {
    this.data[id] = value;
  }
}
