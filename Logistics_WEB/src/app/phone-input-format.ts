export class PhoneInputFormat {
  locale = 'en';
  seperator = ' ';


  unformatValue(value: any) {
    const regExp = new RegExp(this.seperator, 'g');
    // Separate value on before and after decimal marker
    // const [integer, decimal] = value.replace(regExp, '').split(this.decimalMarker);
    const integer = value.replace(regExp, '');
    // Send non localized value, with dot as decimalMarker to API
    return integer;
  }

  formatValue(value: any) {
    if (value === null || value === undefined) {
      return '';
    }
    let ret = '';
    const index = 0;

    ret = ret.concat(value.substr(0, 4));
    value = value.substr(4);
    while (value !== null && value !== '') {
      ret = ret.concat(this.seperator);
      ret = ret.concat(value.substr(0, 3));
      value = value.substr(3);

    }
    return ret;
  }
}
