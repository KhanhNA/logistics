import {Injectable} from '@angular/core';
import {ApiService} from '@next-solutions/next-solutions-base';
import {HttpParams} from '@angular/common/http';


@Injectable()
export class PartnerService {

  constructor(private apiService: ApiService) {
  }


  searchPartner(query: any) {

  }


  createPartner(partner: any) {
    return this.apiService.post('/shipping-partners', partner);
  }

  findById(id: number) {
    return this.apiService.get('/shipping-partners/' + id, new HttpParams());
  }

  updatePartner(id: number, partner: any) {
    return this.apiService.patch('/shipping-partners/' + id, partner);
  }

  approve(id: number) {
    return this.apiService.patch('/shipping-partners/' + id + '/accept', {});
  }

  reject(id: number, rejectReason: string) {
    return this.apiService.patch('/shipping-partners/' + id + '/reject'
      , {rejectReason});
  }
}
