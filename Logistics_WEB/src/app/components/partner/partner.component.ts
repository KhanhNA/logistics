import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService, AuthoritiesService,
  BaseSearchLayout, ButtonFields, ColumnFields, ColumnTypes, DateUtilService,
  FormStateService, IconTypeEnum, LoaderService,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {environment} from 'src/environments/environment';
import {FormBuilder} from '@angular/forms';
import {NavService} from '../../_services/nav.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {PartnerModel} from '../../_models/partner/partner.model';
import {partnerStatus} from '../../_models/partner/partnerStatus';
import {ModelUtils} from '../../_models/ModelUtils';
import {HttpParams} from "@angular/common/http";
import {PartnerAuthoritiesService} from "../../_services/authority/partner.authorities.service";

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.css']
})
export class PartnerComponent extends BaseSearchLayout implements OnInit {

  statusOptions: SelectModel[] = [];
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  expandButton = { // ButtonFields
    columnDef: 'expandColumn',
    color: 'warn',
    icon: 'filter_list',
    click: '',
    title: 'common.title.expand.column',
  };
  errorMsg = new Map<string, () => string>()
    .set('min', () => '')
    .set('max', () => '')
    .set('pattern', () => '')
    .set('required', () => '');

  get environment() {
    return environment;
  }


  constructor(protected formBuilder: FormBuilder,
              public navService: NavService,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              public dateServiceUtil: DateUtilService,
              public loaderService: LoaderService,
              public partnerAuthoritiesService: PartnerAuthoritiesService,
  ) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        code: [''],
        name: [''],
        email: [''],
        phone: [''],
        contractCode: [''],
        status: ['_'],
      }));
    this.initStatus();
    this.columns.push({
        columnDef: 'stt', header: 'number',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
      },
      // {
      //   columnDef: 'code', header: 'code',
      //   title: (e: PartnerModel) => `${e.code ? e.c : ''}`,
      //   cell: (e: PartnerModel) => `${e.code ? e.code : ''}`,
      //   className: 'mat-column-code',
      // },
      {
        columnDef: 'name', header: 'name',
        title: (e: PartnerModel) => `${!!e && e.name ? e.name : ''}`,
        cell: (e: PartnerModel) => `${!!e && e.name ? e.name : ''}`,
        className: 'mat-column-name',
      },
      {
        columnDef: 'address', header: 'address',
        title: (e: PartnerModel) => `${!!e && e.address ? e.address : ''}`,
        cell: (e: PartnerModel) => `${!!e && e.address ? e.address : ''}`,
        className: 'mat-column-address',
      },
      {
        columnDef: 'phone', header: 'phone',
        title: (e: PartnerModel) => `${!!e && e.phone ? e.phone : ''}`,
        cell: (e: PartnerModel) => `${!!e && e.phone ? e.phone : ''}`,
        className: 'mat-column-phone',
      },
      {
        columnDef: 'email', header: 'email',
        title: (e: PartnerModel) => `${!!e && e.email ? e.email : ''}`,
        cell: (e: PartnerModel) => `${!!e && e.email ? e.email : ''}`,
        className: 'mat-column-email',
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: PartnerModel) => `${!!e && e.status ? (this.getStatus(e)) : ''}`,
        cell: (e: PartnerModel) => `${!!e && e.status ? (this.getStatus(e)) : ''}`,
        className: (e: PartnerModel) => `${!!e && e.status ? (this.getStyle(e)) : ''}`,
      }
    );
    this.buttons.push(
      {
        columnDef: 'edit',
        color: 'warn',
        icon: 'fa fa-pen',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'editPartner',
        header: 'common.action',
        className: 'secondary',
        display: (e: PartnerModel) => this.partnerAuthoritiesService.hasUpdateShippingPartnerRole()


        // display: (e: PartnerModel) => e && this.distributorAuthoritiesUtils.hasGetPackageRole(),
        // disabled: (e: PartnerModel) => !e || DistributorAuthoritiesUtils.isAcceptedStatus(e)
      },
      {
        columnDef: 'view',
        title: 'common.title.dashboard',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'onView',
        className: 'info',
        display: () => this.partnerAuthoritiesService.hasGetShippingPartnerRole(),
      }
    );

  }

  initStatus() {
    this.statusOptions = ModelUtils.getInstance().getSelectModel(partnerStatus);

  }

  onView(row: PartnerModel) {
    this.router.navigate(['/partner/view/' + row.id]).then();
  }

  async ngOnInit() {
    this.onSubmit();
  }

  getStatus(e: any) {
    return this.translateService.instant(partnerStatus['' + e.status].label);
  }

  getStyle(e: any) {
    return partnerStatus['' + e.status].styleTable;
  }

  // Override
  onSubmit(): void {
    this.isResetPaging = true;

    this.search();

  }

  search() {
    let status = this.searchForm.get('status')?.value;
    status = status === '_' ? '' : status;
    const params = new HttpParams()
      .set('code', this.searchForm.get('code')?.value)
      .set('email', this.searchForm.get('email')?.value)
      .set('name', this.searchForm.get('name')?.value)
      .set('phone', this.searchForm.get('phone')?.value)
      .set('contractCode', this.searchForm.get('contractCode')?.value)
      .set('status', status)
    ;
    // .set('status', this.getSelectValue(this.searchForm.get('status')?.value));


    // .set('status', `${status}`.replace('_', ''));
    this._fillData('/shipping-partners', params);
  }


  addNewPartner() {
    this.router.navigate(['partner/add']).then();
  }

  editPartner(row: any, ind: number) {
    if (!row) {
      return;
    }
    this.router.navigate(['partner/edit', row.id]).then();
  }
}
