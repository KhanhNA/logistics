import {Component, OnInit} from '@angular/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout, ButtonClickModel,
  ButtonFields, FileTypes, UploadModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute, Router} from '@angular/router';
import {AbstractControl, FormBuilder, ValidationErrors, ValidatorFn} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {PartnerModel} from '../../../_models/partner/partner.model';
import {ModelUtils} from '../../../_models/ModelUtils';
import {PartnerService} from '../partner.service';
import {environment} from '../../../../environments/environment';
import {message, partnerStatusAction} from '../../../_models/partner/partnerStatus';
import {ValidateFn} from "codelyzer/walkerFactory/walkerFn";
import {PartnerAuthoritiesService} from "../../../_services/authority/partner.authorities.service";

@Component({
  selector: 'app-partner-add-edit',
  templateUrl: './partner-add-edit.component.html',
  styleUrls: ['./partner-add-edit.component.css'],
  providers: [PartnerService]
})
export class PartnerAddEditComponent extends BaseAddEditLayout implements OnInit {
  actionButtons: ButtonFields[] = [];
  partnerModel?: PartnerModel;
  editting = false;
  isView = false;

  get FileTypes() {
    return FileTypes;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              private router: Router,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected partnerService: PartnerService,
              protected partnerAuthoritiesService: PartnerAuthoritiesService,
              protected authoritiesService: AuthoritiesService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.addEditForm = formBuilder.group({
      // code: [''],
      name: [''],
      phone: [''],
      email: [''],
      contractCode: [''],
      taxCode: [''],
      address: [''],
      description: [''],
      partnerFiles: this.formBuilder.control('', this.validatorFile())
    });
    activatedRoute.data.subscribe(data => {
      this.isView = !!data.isView;
    })
    this.actionButtons.push(...[{
      columnDef: 'back',
      color: 'warn',
      icon: '',
      click: 'onBack',
      header: 'common.action',
      title: 'common.button.back',
      className: 'primary outline',
      display: (e: any) => !this.isEdit,
    },
      {
        columnDef: 'cancel',
        color: 'warn',
        icon: '',
        click: 'onCancel',
        isShowHeader: true,
        title: 'common.button.back',
        className: 'primary',
        display: (e: any) => this.isEdit,

      },
      // {
      //   columnDef: 'edit',
      //   color: 'warn',
      //   icon: '',
      //   click: 'onEdit',
      //   isShowHeader: true,
      //   title: 'common.button.edit',
      //   className: 'primary',
      //   display: (e: any) => false,
      //   disabled: (e: any) => this.addEditForm.invalid
      // },
      {
        columnDef: 'save',
        color: 'warn',
        icon: '',
        click: 'onSave',
        isShowHeader: true,
        title: 'btnAdd',
        className: 'primary',
        display: (e: any) => !this.isEdit && !this.isView,
        disabled: (e: any) => this.addEditForm.invalid
      },
      {
        columnDef: 'save',
        color: 'warn',
        icon: '',
        click: 'onSave',
        isShowHeader: true,
        title: 'btnUpdate',
        className: 'primary',
        display: (e: any) => this.isEdit && !partnerStatusAction.isApprovedStatus(this.partnerModel) && !this.isView,
        disabled: (e: any) => this.addEditForm.invalid
      },
      {
        columnDef: 'approve',
        color: 'warn',
        icon: '',
        click: 'onApprove',
        isShowHeader: true,
        title: 'common.button.approve',
        className: 'approve',
        display: (e: any) => this.isEdit && this.isView && !partnerStatusAction.isApprovedStatus(this.partnerModel)
          && partnerAuthoritiesService.hasApproveShippingPartnerRole(),
        disabled: (e: any) => this.addEditForm.invalid
      },
      {
        columnDef: 'reject',
        color: 'warn',
        icon: '',
        click: 'onReject',
        isShowHeader: true,
        title: 'common.button.reject',
        className: 'danger',
        display: (e: any) => this.isEdit && this.isView
          && !partnerStatusAction.isRejectedStatus(this.partnerModel) && partnerAuthoritiesService.hasRejectShippingPartnerRole(),
        disabled: (e: any) => this.addEditForm.invalid
      },
    ]);
  }

  resetData() {
    if (!this.partnerModel) {
      return;
    }
    this.addEditForm.setValue(UtilsService
      .reduceEntityAttributeForFormControl(this.addEditForm, this.partnerModel));
    const files = this.partnerModel.shippingPartnerFiles;
    if (files && files.length > 0) {
      const urls = files.map(value => {
        if (value && value.url) {
          const name = UploadModel.getFileName(value.url);
          const type = UploadModel.getFileType(value.url);
          const id = value.id ? value.id : undefined;
          const url = environment.BASE_FILE_URL + value.url;
          return new UploadModel(name, null, url, type, id);
        }
      });
      this.addEditForm.get('partnerFiles')?.setValue(urls);
    }
  }

  ngOnInit() {
    super.ngOnInit();
    this.editting = false;
    if (this.isEdit) {
      this.partnerService.findById(this.id).subscribe(next => {
        this.partnerModel = next as PartnerModel;
        this.resetData();

      });
    }
  }

  // onSave(a: any, b: any): void{
  //   console.log('aaaaaaaaaaaaaaaaa',a,b)
  //   this.utilsService.showConfirmDialog('partner.message.save.confirmTitle', [])
  //     .afterClosed().subscribe(result => {
  //     if (result && result.value) {
  //       this.submitData();
  //     }
  //   });
  // }
  onBack() {
    this.back();
  }

  onCancel() {
    this.editting = false;
    this.router.navigate(['partner']);
    this.resetData();
  }

  onEdit() {
    this.editting = true;
  }

  onSave() {
    const partner = new PartnerModel(this.addEditForm);
    const files = this.addEditForm.get('partnerFiles');
    const formData = new FormData();
    if (files) {
      const ids = ModelUtils.getInstance().getFileList(files, 'files', formData);
      const removeIds: number[] = [];
      this.partnerModel?.shippingPartnerFiles?.forEach(value => {
        if (value.id && (!ids || !ids.includes(value.id))) {
          removeIds.push(value.id);
        }
      });
      if (removeIds && removeIds.length > 0) {
        formData.set('removeFileIds', this.utilsService.toBlobJon(removeIds));
      }
    }
    formData.set('source', this.utilsService.toBlobJon(partner));
    const api = this.isEdit ? this.partnerService.updatePartner(this.id, formData) :
      this.partnerService.createPartner(formData);
    return api;
    // api.subscribe(data => {
    //     this.onSuccessFunc(data, 'partner.message.save.success');
    //   },
    //   error => {
    //     this.onErrorFunc(error);
    //   });

  }

  onApprove() {
    return this.partnerService.approve(this.id);
  }

  onReject(reason: any) {
    return this.partnerService.reject(this.id, reason?.value);
  }

  onRowButtonClick(event: ButtonClickModel) {
    const type = event.action;
    const msg = message[type];
    const window = this as any;
    if (!msg || !msg.dlgType) {
      window[type]();
      return;
    }
    this.utilsService[msg.dlgType](message[type].confirmMessage, [])
      .afterClosed().subscribe((result: any) => {
      if (result && result.value) {
        // this.submitData();

        window[type](result).subscribe((data: any) => {
            this.onSuccessFunc(data, message[type].success);
          },
          (error: any) => {
            this.onErrorFunc(error);
          });
      }
    });

  }

  onSuccessFunc = (data: any, onSuccessMessage?: string): void => {
    this.utilsService.onSuccessFunc(onSuccessMessage ? onSuccessMessage : 'common.default.success');
    setTimeout(() => {
      this.router.navigate(['/partner/edit/' + data.id]).then();
    }, 500);


  }
  onErrorFunc = (error1: any) => {
    this.utilsService.showError(error1);

  }

  validatorFile(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.value) {
        return null;
      }
      const result = (control?.value as []).every((f: any) => {
        return !f.imageUrl;
      });
      if (result) {
        return {required: true};
      }
      return null;
    };
  }
}
