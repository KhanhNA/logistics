import {NgModule} from '@angular/core';
import {PartnerComponent} from './partner.component';
import {SharedModule} from '../../modules/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {PartnerRoutingModule} from './partner-routing.module';
import {MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {CommonModule} from "@angular/common";
import { PartnerAddEditComponent } from './partner-add-edit/partner-add-edit.component';
import {PartnerAuthoritiesService} from "../../_services/authority/partner.authorities.service";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/partner/', suffix: '.json'},
    {prefix: './assets/i18n/', suffix: '.json'},

  ]);
}

@NgModule({
  declarations: [PartnerComponent, PartnerAddEditComponent],
  imports: [
    SharedModule,
    CommonModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      isolate: true,
    }),
    PartnerRoutingModule,
  ],
  providers: [
    PartnerAuthoritiesService
  ]
})
export class PartnerModule { }
