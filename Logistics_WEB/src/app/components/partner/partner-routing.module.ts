import {RouterModule, Routes} from '@angular/router';
import {AuthoritiesResolverService} from '@next-solutions/next-solutions-base';
import {NgModule} from '@angular/core';
import {PartnerComponent} from './partner.component';
import {PartnerAddEditComponent} from './partner-add-edit/partner-add-edit.component';


const routes: Routes = [
  {
    pathMatch: 'full',
    path: '',
    resolve: {me: AuthoritiesResolverService},
    component: PartnerComponent
  },
  {

    path: 'add',
    data: {breadcrumb: 'partner.label.create'},
    resolve: {me: AuthoritiesResolverService},
    component: PartnerAddEditComponent
  },
  {

    path: 'edit/:id',
    data: {breadcrumb: 'partner.label.edit'},
    resolve: {me: AuthoritiesResolverService},
    component: PartnerAddEditComponent
  },
  {

    path: 'view/:id',
    data: {breadcrumb: 'partner.label.view', isView: true},
    resolve: {me: AuthoritiesResolverService},
    component: PartnerAddEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PartnerRoutingModule {

}
