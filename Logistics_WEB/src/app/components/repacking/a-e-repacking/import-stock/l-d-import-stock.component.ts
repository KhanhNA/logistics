import {DatePipe, Location} from '@angular/common';
import {CookieService} from 'ngx-cookie-service';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ChangeDetectorRef, Component, Renderer2} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {TsUtilsService} from '../../../../base/Utils/ts-utils.service';
import {CommunicationService} from '../../../../communication-service';
import {ImportStockLayout} from './import-stock.layout';
import {TranslateService} from '@ngx-translate/core';
import {ActionTypeEneum, RepackingStatusConst, RepackingStatusEnum} from '../../../../_models/action-type';
import {DividePackingComponent} from '../../../divide-packing/divide-packing.component';
import {environment} from '../../../../../environments/environment';
import {ApiService, AuthoritiesService} from "@next-solutions/next-solutions-base";

@Component({
  selector: 'app-update-pallet',
  templateUrl: './l-d-import-stock.html',
  styleUrls: ['./import-stock.scss'],

  providers: [
    DatePipe
  ]
})


export class LDImportStockComponent {
  model: ImportStockLayout;
  repackingPlan: any;
  repackingStatus = new RepackingStatusConst();
  actionType = ActionTypeEneum;

  constructor(fb: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, private toastr: ToastrService,
              public dialog: MatDialog, private renderer: Renderer2,
              private comm: CommunicationService, private route: ActivatedRoute, private changeDetector: ChangeDetectorRef,
              transService: TranslateService, private datePipe: DatePipe,
              tsuService: TsUtilsService, location: Location,
              protected authoritiesService: AuthoritiesService) {

    this.model = new ImportStockLayout(tsuService, userService, fb, route,
      this.datePipe, transService, toastr, location, this.dialog, authoritiesService);
  }


  // ngOnInit(): void {
  //   const headers = [{name: 'Accept', value: 'application/json'}];
  //   console.log('thisform:', this.form);
  //   this.btnSaveLbl = 'Save';
  //   this.sub = this.route.params.subscribe(params => {
  //     console.log('paramsxxxxxxx:', params);
  //     const id = params.id;
  //
  //     this.actionType = params.actType;
  //     this.btnSaveLbl = 'common.btn.save';
  //     if (this.actionType !== this.ACTION_TYPE.new) {
  //       this.getPalletList();
  //       this.getRepackedDetails(params);
  //     }
  //
  //
  //   });

  //
  //   this.utils.hideLoading();
  //
  // }

  // getRepackedDetails(pars) {
  //   console.log('onSearchData1:', event, this.search);
  //   let method;
  //   if (this.actionType === this.ACTION_TYPE.view) {
  //     // method = this.userService.get(`/repacking-plannings/detail-repacked/${pars.id}`, null);
  //   } else {
  //     method = this.userService.get(`/repacking-plannings/detail-repacked/${pars.id}`, null);
  //   }
  //
  //   if (method) {
  //
  //
  //     method.subscribe((repackings: any) => {
  //       console.log('repackingsnnnnnnnnnnnnnnnnnnn', repackings);
  //       if (repackings !== undefined) {
  //         console.log('planing:', repackings);
  //         this.planId = pars.id;
  //         this.models = new MatTableDataSource(repackings);
  //         // this.updateFirstFormControl(this.planModels.data);
  //         // this.updateControl(this.planModels.data);
  //         // this.form.get('code')?.setValue(plan.code);
  //         // this.form.get('description')?.setValue(plan.description);
  //       }
  //
  //     }, error1 => {
  //       console.log('error', error1);
  //       this.utils.showError(error1);
  //     });
  //   }
  // }

  // getValidateError(errors: any) {
  //   console.log('errrrrrr:', errors);
  //   let ret = '';
  //   for (const key in errors) {
  //     ret = this.transService.instant('validator.' + key);
  //     if (errors[key] === true) {
  //       return ret;
  //     }
  //     if (!errors[key] || !errors[key][key]) {
  //       break;
  //     }
  //     if (typeof errors[key][key].getMonth === 'function') {
  //       ret = ret + ' ' + errors[key][key].getDate() + '/' + errors[key][key].getMonth() + '/' + errors[key][key].getYear();
  //     } else {
  //       ret = ret + ' ' + errors[key][key];
  //     }
  //
  //     // console.log('typeof:', typeof errors[key][key], errors[key][key], typeof errors[key][key].getMonth);
  //
  //   }
  //   // return defValue;
  //   return ret;
  // }


  // updateAfterGetDetail = (data: any) => {
  //   console.log('podetail:', data);
  //   this.editRow = data;
  //   this.updateControl(this.editRow, true);
  // }


  // getDefaultPallet(row) {
  //   console.log('getDefaultPallet', row, row.importPoDetailPallets[0].pallet.id, this.pallets);
  //   return row.importPoDetailPallets[0].pallet.id;
  //   // row.importPoDetailPallets[0].pallet.id
  // }

  dateChange(row: any, event: any, ind: any) {
    console.log(row, event, ind);
    row.expireDate = this.datePipe.transform(event.value, environment.API_DATE_FORMAT);
    console.log('rowchange:', row);
  }

  refresh(repackingPlan: any, actionType: string | undefined, parentId: number | undefined) {
    this.repackingPlan = repackingPlan;
    this.model.refresh(repackingPlan, actionType, parentId);
  }

  onSave() {
    console.log(this.model.formData);
    this.model.onImport();
  }


  onCellEvent(data: any) {
    if (data.act === 'add_box') {
      const shallowCopy = {...data.row};
      this.dialog.open(DividePackingComponent, {
        disableClose: false,
        height: '38%',
        width: '40vw',
        data: {
          packing: shallowCopy, type: 'from-repack', store: this.model.repackingPlan.store,
          nameCol: {name: 'productPacking.product.name'}
        }
      }).afterClosed().subscribe(response => {
        if (response) {
          this.model.dividePacking(response.data, data);
        }
      });
    }

    if (data.act === 'clear') {
      this.model.removeDividedPacking(data);
    }
  }

  isShowBtn(event: any) {
    if (this.model.actionType === ActionTypeEneum.view || this.model.actionType === ActionTypeEneum.view_from_claim) {
      event.isShow = false;
      return;
    }
    if (this.model.repackingPlan && this.model.repackingPlan.status === RepackingStatusEnum.REPACKED) {
      if (event.act === 'add') {
        event.isShow = false;
        return;
      }

      if (event.act === 'clear') {
        const ind = this.model.getTblData().indexOf(event.row);
        if ((this.model.getTblData()[ind - 1] && this.model.getTblData()[ind - 1].id === event.row.id) ||
          (this.model.getTblData()[ind + 1] && this.model.getTblData()[ind + 1].id === event.row.id)) {
          event.isShow = true;
          return;
        } else {
          event.isShow = false;
          return;
        }
      }

      if (event.row.quantity <= 1) {
        event.isShow = ['clear'].includes(event.act);
        return;
      }
      event.isShow = ['add_box', 'clear'].includes(event.act);
      return;
    } else {
      event.isShow = false;
      return;
    }

  }

  onClaimDetail() {
    if (this.model.claimId) {
      this.router.navigate(['/claim/dashboard/' + this.model.claimId]).then();
    } else {
      this.model.showErrorMsg('claim.wait.approve.please');
    }
  }

  onReadonly() {
    return [ActionTypeEneum.view.toString(), ActionTypeEneum.view_from_claim.toString()].includes('' + this.model.actionType);
  }

  onPrintQrCode() {
    console.log(this.model.tsTable.allSelect);
    const checkedData = [];
    if (this.model.tsTable.allSelect) {
      for (const key of Object.keys(this.model.tsTable.allSelect)) {
        checkedData.push(this.model.tsTable.allSelect[key]);
      }
    }
    if (checkedData.length === 0) {
      this.model.showWarningMsg('notice.checkbox.nocheck');
      return;
    }
    const divContents = [];
    const store = this.repackingPlan.store.code;
    const hotline = this.repackingPlan.store.phone;
    const repackedDate = this.datePipe.transform(this.repackingPlan.repackedDate, 'HH:mm dd-MM-yyyy', '-0');
    for (const importStatementDetail of checkedData) {
      const expiredDate = this.datePipe.transform(importStatementDetail.expireDate, environment.DIS_DATE_FORMAT, '-0');
      const packingCode = importStatementDetail.productPacking.code;
      const qrBase64 = importStatementDetail.storeProductPackingDetail.qRCode;
      const quantityPackingType = importStatementDetail.productPacking.packingType.quantity;
      const packingName = importStatementDetail.productPacking.name;

      const content = this.createContentPrint(packingCode, qrBase64, expiredDate, store,
        packingName, quantityPackingType, repackedDate, hotline);
      divContents.push(content);
    }

    this.openWindow(divContents);
  }

  createContentPrint(code: any, base64: any, expireDate: any, store: any, name: any,
                     quantity: any, repackDate: any, hotline: any) {
    const divContent = ' <div class="invoiceForm" style="display: flex;\n' +
      '  height: 1.94in;\n' +
      '  width: 3.14in;\n' +
      '  font-size: 15px;">\n' +
      '    <div class="qrCode" style="    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: start;\n' +
      '    align-items: center;\n' +
      '    margin: 0.15in 0.2in 0 0.3in;">\n' +
      '      <div class="code">\n' +
      '        <label>Code: </label>\n' +
      '        <label style="word-break: break-all">' + code + '</label>\n' +
      '      </div>\n' +
      '      <div id="img">\n' +
      '        <img src="data:image/png;base64, ' + base64 + '" style="width: 1in;height: 1in"/>\n' +
      '      </div>\n' +
      '      <div class="expiredDate" style="display: flex;\n' +
      '       flex-direction: column;' +
      '       margin-top: 0.025in">\n' +
      '        <label>Expire date: </label>\n' +
      '        <label>' + expireDate + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '    <div class="information" style="    margin: 0.15in 0 0.25in 0;\n' +
      '    width: calc(100% - 1.5in);\n' +
      '    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: space-between;\n' +
      '    font-size: 14px;">\n' +
      '      <div class="store">\n' +
      '        <label>Store: </label>\n' +
      '        <label style="word-break: break-all">' + store + '</label>\n' +
      '      </div>\n' +
      '      <div class="name">\n' +
      '        <label>Name: </label>\n' +
      '        <label>' + name + '</label>\n' +
      '      </div>\n' +
      '      <div class="block">\n' +
      '        <label>Block: </label>\n' +
      '        <label>' + quantity + '</label>\n' +
      '      </div>\n' +
      '      <div class="repackDate">\n' +
      '        <label>Repacked date: </label>\n' +
      '        <label style="height: 0.17in">' + repackDate + '</label>\n' +
      '      </div>\n' +
      '      <div class="hotline">\n' +
      '        <label>Hotline:</label>\n' +
      '        <label>' + hotline + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '  </div>\n';

    return divContent;
  }

  openWindow(contents: any) {
    const a = window.open('', '');
    if (a) {
      a.document.write('<html><head><style>div{' +
        'margin-bottom: 0.05in}\n' +
        'div:last-child { margin-bottom: 0;}\n@page {\n' +
        '  size: 3.15in 1.97in;\n' +
        '  margin: 0;\n' +
        '}</style></head>');
      a.document.write('<body style="margin: 0;padding: 0; ">');
      for (const content of contents) {
        a.document.write(content);
      }
      a.document.write('</body></html>');
      a.document.close();
      setTimeout(() => {
        a.print();
        a.close();
      }, 250);
    }
  }
}
