import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TsUtilsService} from '../../../../base/Utils/ts-utils.service';

import {LDBaseLayout} from '../../../../base/l-d-base.layout';
import {ToastrService} from 'ngx-toastr';
import {FieldConfigExt} from '../../../../base/field/field.interface';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {ActionTypeEneum, ClaimEnum, ImportStatementStatus, RepackingStatusEnum} from '../../../../_models/action-type';
import {ProductDescriptionsUtils} from '../../../../_utils/product.descriptions.utils';
import {NumberValidators} from '../../../../base/field/number-validators';
import {HttpParams} from '@angular/common/http';
import {ClaimDetailModel} from '../../../../_models/claim.detail.model';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {RepackingPlanDetailRepackedModel} from "../../../../_models/repacking/repacking.plan.detail.repacked.model";


// @Directive()
export class ImportStockLayout extends LDBaseLayout {
  inputFields: FieldConfigExt[] | undefined;
  cols: FieldConfigExt[] | undefined;
  pallet = '';
  palletCollection: any[] = [];
  repackingPlan: any;

  importStatementDetails: any[] = [];

  isApprovePending = false;

  hasClaim = false;
  claimId: number | undefined;
  isFirstTimeUpdateCol = true;

  palletCol = new FieldConfigExt({
    type: 'autocomplete',
    label: 'common.pallet',
    binding: 'pallet',
    name: 'pallet.code',
    options: ['code', 'name'],
    inputType: 'text',
    collections: this.palletCollection,
  });

  checkboxCol = new FieldConfigExt({
    type: 'checkboxAll',
    inputType: 'all',
    name: 'chkbox'
  });

  buttonsCol = new FieldConfigExt({
    type: 'btnGrp',
    inputType: 'text',
    name: 'add',
    value: ['add_box', 'clear'],
    authorities: {
      add_box: ['post/import-statements/from-repacking-planning'],
      clear: ['post/import-statements/from-repacking-planning']
    },
    titles: {
      add_box: 'common.title.add_box',
      clear: 'common.title.clear'
    }
  });
  changeQuantity = (row: any) => {
    console.log(row);

    this.showMaxQuantityWarning(row, 'common.packing.max.quantity');
    // row.obj.total = Number(row.obj.quantity) * (100 + Number(row.obj.productPacking.vat)) *
    //   Number(row.obj.productPackingPrice.price) / 100;

    this.tsTable.dataSrc._updateChangeSubscription();
  }


  constructor(tsuService: TsUtilsService, private apiService: ApiService, fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              transService: TranslateService, toastr: ToastrService, location: Location,
              public dialog: MatDialog,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    this.setFormData();
  }

  setFormData() {

    this.inputFields = [
      new FieldConfigExt({
        type: 'input',
        label: 'repacking.ae.description',
        inputType: 'text',
        name: 'description',
        binding: 'description'
      })
    ];
    this.cols = [
      new FieldConfigExt({
        type: 'index',
        label: 'ld.no',
        inputType: 'text',
        name: 'STT',
      }),
      new FieldConfigExt({
        type: 'view',
        label: 'common.packing.code',
        inputType: 'text',
        name: 'productPacking.code',
      }),
      new FieldConfigExt({
        type: 'view',
        label: 'common.product.name',
        inputType: 'text',
        name: 'productPacking.product.name',
      }),
      new FieldConfigExt({
        type: 'view',
        label: 'common.packingType',
        inputType: 'number',
        name: 'productPacking.packingType.quantity',
      }),
      new FieldConfigExt({
        type: 'view',
        label: 'common.expireDate',
        inputType: 'date',
        name: 'expireDate',

      }),
      new FieldConfigExt({
        type: 'view',
        label: 'common.quantity',
        inputType: 'number',
        name: 'quantity',
        emit: this.changeQuantity,
        validations: [
          {
            name: 'integerNumber',
            validator: NumberValidators.integerValidation(),
            message: 'validation.integer'
          },
          {
            name: 'minQuantity',
            validator: NumberValidators.minQuantityValidation(-1),
            message: 'validation.>=0'
          },

        ],
        require: 'true'
      }),

      this.palletCol,
      this.buttonsCol
    ];

    this.setCols(this.cols);
    this.setSelection(true);
    this.init(this.inputFields);
    this.formData = {description: ''};
  }


  refresh(repackingPlan: any, actionType: string | undefined, parentId: number | undefined) {
    if (!repackingPlan) {
      return;
    }
    this.repackingPlan = repackingPlan;
    if (this.palletCollection.length === 0) {
      this.getPallets().subscribe((response: any) => {
        this.palletCollection = response.content;
        if (this.cols) {
          this.cols[this.cols.indexOf(this.palletCol)].collections = response.content;
        }
        this.setCols(this.cols);

        this.getDataAndSetData();
      });
    } else {
      this.getDataAndSetData();
    }

  }

  updateColsByAction() {
    const colsDisplay = [];
    if (this.cols) {
      for (const col of this.cols) {
        if (col.name === 'quantity') {
          if ((this.repackingPlan.status === RepackingStatusEnum.REPACKED
            || (this.actionType === ActionTypeEneum.view && this.repackingPlan.status === RepackingStatusEnum.IMPORTED))
            && this.hasClaim) {
            col.label = 'common.quantity.real.import';
            col.require = 'false';
            colsDisplay.push(
              new FieldConfigExt({
                type: 'view',
                label: 'common.quantity.org',
                inputType: 'number',
                name: 'orgQuantity',
              }),
              new FieldConfigExt({
                type: 'view',
                label: 'common.quantity.claim',
                inputType: 'number',
                name: 'claimQuantity',
              })
            );
          }
        }
        if (col.name === 'add' && this.actionType === ActionTypeEneum.view) {
          continue;
        }
        colsDisplay.push(col);
      }
    }

    this.cols = colsDisplay;
  }

  getDataAndSetData() {
    if (this.actionType === ActionTypeEneum.view) {
      this.palletCol.type = 'view';
    }
    if (this.repackingPlan && this.repackingPlan.status === RepackingStatusEnum.IMPORTED) {
      // them cot checkbox khi dong goi da nhap kho
      this.cols?.splice(0, 0, this.checkboxCol);
      this.cols?.splice(this.cols.indexOf(this.buttonsCol), 1);
      this.setCols(this.cols);
    }
    if (this.repackingPlan.status === RepackingStatusEnum.REPACKED) {
      // this.tsuService.showLoading();
      const claimMap = new Map();
      this.apiService.get(`/repacking-plannings/detail-repacked/${this.parentId}`, new HttpParams())
        .subscribe((data: RepackingPlanDetailRepackedModel[] | any) => {
          const params = new HttpParams()
            .set('type', ClaimEnum.IMPORT_STATEMENT_REPACKING_PLANNING)
            .set('referenceId', '' + this.parentId);
          this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', params)
            .subscribe((claimReference: ClaimDetailModel[]) => {
                let key = '';
                if (claimReference.length > 0) {
                  this.hasClaim = true;
                  this.claimId = claimReference[0].claim?.id;
                  claimReference.forEach((detail: ClaimDetailModel) => {
                    key = '' + detail.expireDate + detail.productPacking?.id;
                    claimMap.set(key, detail.quantity);
                  });
                }
                if (this.isFirstTimeUpdateCol) {
                  this.updateColsByAction();
                  this.setCols(this.cols);
                  this.isFirstTimeUpdateCol = false;
                }
                this.initTableData(claimMap, data);
              },
              (error: any) => {
                this.isApprovePending = true;
                this.showErrorMsg(error);
                this.initTableData(claimMap, data);
              });
        });

      this.inputFields?.push(new FieldConfigExt({
        type: 'btnGrp',
        inputType: 'text',
        name: 'add',
        value: ['add_box', 'clear'],
        authorities: {
          add_box: ['post/import-statements/from-repacking-planning'],
          clear: ['post/import-statements/from-repacking-planning']
        },
        titles: {
          add_box: 'common.title.add_box',
          clear: 'common.title.clear'
        }
      }));
      this.init(this.inputFields);

    } else if (this.repackingPlan.status === RepackingStatusEnum.IMPORTED) {
      if (this.cols) {
        for (let ind = this.cols.length - 1; ind >= 0; ind--) {
          if (this.cols[ind].name === 'pallet.code') {
            this.cols[ind].type = 'view';
            this.cols[ind].binding = 'pallet.code';
            continue;
          }
        }
      }
      this.apiService.get('/import-statements/repacking-planning/' + this.parentId, new HttpParams())
        .subscribe((data: any) => {
          this.formData.description = data.description;

          const claimMap = new Map();
          const params = new HttpParams()
            .set('type', ClaimEnum.IMPORT_STATEMENT_REPACKING_PLANNING)
            .set('referenceId', '' + this.parentId);
          this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', params)
            .subscribe((claimReference: ClaimDetailModel[]) => {
              let key = '';
              if (claimReference.length > 0) {
                this.hasClaim = true;
                this.claimId = claimReference[0].claim?.id;
                claimReference.forEach((detail: any) => {
                  key = '' + detail.expireDate + detail.productPacking.id;
                  detail.orgQuantity = detail.quantity;
                  detail.claimQuantity = detail.quantity;
                  detail.quantity = Number(detail.orgQuantity) - Number(detail.claimQuantity);
                  claimMap.set(key, detail);
                });
              }

              let claimKey = '';
              data.importStatementDetailPallets.forEach((detailPallet: any) => {
                claimKey = '' + detailPallet.expireDate + detailPallet.productPackingId;
                detailPallet.productPacking = detailPallet.palletDetail.productPacking;
                detailPallet.orgQuantity = detailPallet.quantity;
                detailPallet.claimQuantity = 0;
                if (claimMap.has(claimKey)) {
                  detailPallet.claimQuantity = claimMap.get(claimKey).orgQuantity;
                  detailPallet.orgQuantity = Number(detailPallet.orgQuantity) + Number(detailPallet.claimQuantity);
                  claimMap.delete(claimKey);
                }
              });

              const dataTable = [];
              if (this.isFirstTimeUpdateCol) {
                this.updateColsByAction();
                this.setCols(this.cols);
                this.isFirstTimeUpdateCol = false;
              }
              dataTable.push(...data.importStatementDetailPallets);

              this.updateTable(dataTable);
            });

        });
    }
  }

  initTableData(claimMap: Map<string, number>, data: RepackingPlanDetailRepackedModel[]) {
    this.importStatementDetails.length = 0;
    const map = new Map();
    const claimAllRepackingDetailIds = data.filter((repacked: RepackingPlanDetailRepackedModel) => {
      return claimMap.has(repacked.productPacking?.code + '') && repacked.quantity === 0;
    }).map(repacked => repacked.id);
    for (const repackingPlanDetail of data) {
      const repackingPlanDetailPackingId = repackingPlanDetail.productPacking?.id;
      const repackingPlanDetailExpireDate = repackingPlanDetail.repackingPlanningDetail?.expireDate;
      repackingPlanDetail.expireDate = repackingPlanDetail.repackingPlanningDetail?.expireDate;
      const key = '' + repackingPlanDetailExpireDate + repackingPlanDetailPackingId;
      if (map.has(key)) {
        const detail = map.get(key);
        detail.quantity = Number(repackingPlanDetail.quantity) + Number(detail.quantity);
      } else {
        if (Number(repackingPlanDetail.quantity) > 0 || claimAllRepackingDetailIds.includes(repackingPlanDetail.id)) {
          if (claimMap.has(key)) {
            repackingPlanDetail.orgQuantity = repackingPlanDetail.quantity;
            repackingPlanDetail.claimQuantity = Number(claimMap.get(key));
            repackingPlanDetail.quantity = Number(repackingPlanDetail.quantity) - Number(claimMap.get(key));
          } else {
            repackingPlanDetail.orgQuantity = repackingPlanDetail.quantity;
            repackingPlanDetail.claimQuantity = 0;
          }
          map.set(key, repackingPlanDetail);
        }
      }
      ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(repackingPlanDetail.productPacking?.product);
      repackingPlanDetail.maxQuantity = repackingPlanDetail.quantity;
    }

    for (const detail of map.values()) {
      this.importStatementDetails.push({
        productPackingId: detail.productPacking.id,
        productPackingCode: detail.productPacking.code,
        quantity: detail.quantity,
        expireDate: detail.expireDate
      });
    }
    this.updateTable([...map.values()]);
  }

  onImport() {
    let count = 0;
    const map = new Map();
    let key = '';
    // this.importStatementDetails.forEach(detail => {
    //   importStatementDetails.push({
    //     productPackingId: detail.productPacking.id,
    //     quantity: detail.quantity,
    //     expireDate: detail.expireDate
    //   });
    // });

    for (const item of this.getTblData()) {
      count++;
      if (!item.pallet || !item.pallet.id) {
        const msg = this.translateService.instant('notice.update.pallet') + count;
        this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
        this.toastr.warning(msg);
        return;
      }
      key = '' + item.productPacking.id + item.repackingPlanningDetail.expireDate + item.pallet.id;
      if (map.has(key)) {
        const detailPallet = map.get(key);
        detailPallet.quantity = Number(detailPallet.quantity) + Number(item.quantity);
      } else {
        map.set(key, {
          pallet: item.pallet,
          quantity: item.quantity,
          productPackingId: item.productPacking.id,
          expireDate: item.repackingPlanningDetail.expireDate,
        });
      }
    }
    const data = {
      toStoreId: this.repackingPlan.store ? this.repackingPlan.store.id : '',
      toStoreCode: this.repackingPlan.store ? this.repackingPlan.store.code : '',
      fromStoreId: this.repackingPlan.store ? this.repackingPlan.store.id : '',
      fromStoreCode: this.repackingPlan.store ? this.repackingPlan.store.code : '',
      repackingPlanningId: this.parentId,
      description: this.formData.description,
      status: ImportStatementStatus.NOT_UPDATE_INVENTORY,
      importStatementDetails: this.importStatementDetails,
      importStatementDetailPallets: [...map.values()]
    };


    let method;
    method = this.apiService.post('/import-statements/from-repacking-planning', data);
    this.tsuService.execute6(method, this.saveSuccess, 'common.confirmImport', [' ']);
  }

  getPallets = () => {
    return this.tsuService.getPallets(this.repackingPlan.store.id, 2, true);
  }

  dividePacking(dividedPacking: any, originPacking: any) {
    const tblData = this.getTblData();
    const length = tblData.length;
    const dataTable = [];
    for (let i = 0; i < length; i++) {
      dataTable.push(tblData[i]);
      if (i === originPacking.ind) {
        tblData[i].quantity = Number(tblData[i].quantity) - Number(dividedPacking.quantity);
        dividedPacking.maxQuantity = tblData[i].maxQuantity - tblData[i].quantity;
        dividedPacking.orgQuantity = dividedPacking.quantity;
        dividedPacking.claimQuantity = 0;
        tblData[i].maxQuantity = tblData[i].quantity;
        tblData[i].orgQuantity = Number(tblData[i].orgQuantity) - Number(dividedPacking.quantity);
        dataTable.push(dividedPacking);
      }
    }
    this.tsTable.removeRows(0, length);
    this.tsTable.addRows(dataTable);
  }

  removeDividedPacking(removeObj: any) {
    const beforeObj = this.getTblData()[removeObj.ind - 1];
    const afterObj = this.getTblData()[removeObj.ind + 1];
    if ((removeObj.ind === 0 && afterObj.id !== removeObj.row.id) ||
      (removeObj.ind === this.getTblData().length - 1 && beforeObj.id !== removeObj.row.id) ||
      (beforeObj && beforeObj.id !== removeObj.row.id && afterObj.id !== removeObj.row.id)) {
      this.showWarningMsg('common.packing.remove.not.divide');
      return;
    }
    if (beforeObj && beforeObj.id === removeObj.row.id) {
      beforeObj.quantity = Number(removeObj.row.quantity) + Number(beforeObj.quantity);
      beforeObj.maxQuantity = Number(removeObj.row.maxQuantity) + Number(beforeObj.maxQuantity);
      beforeObj.orgQuantity = Number(beforeObj.orgQuantity) + Number(removeObj.row.orgQuantity);
      beforeObj.claimQuantity = Number(beforeObj.claimQuantity) + Number(removeObj.row.claimQuantity);
    } else if (afterObj.id === removeObj.row.id) {
      afterObj.quantity = Number(removeObj.row.quantity) + Number(afterObj.quantity);
      afterObj.maxQuantity = Number(removeObj.row.maxQuantity) + Number(afterObj.maxQuantity);
      afterObj.orgQuantity = Number(afterObj.orgQuantity) + Number(removeObj.row.orgQuantity);
      afterObj.claimQuantity = Number(afterObj.claimQuantity) + Number(removeObj.row.claimQuantity);
    }

    this.tsTable.removeRows(removeObj.ind, 1);
  }
}
