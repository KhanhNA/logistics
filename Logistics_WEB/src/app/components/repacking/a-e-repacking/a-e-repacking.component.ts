import {CookieService} from 'ngx-cookie-service';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatStepper} from '@angular/material/stepper';
import {ActivatedRoute, Router} from '@angular/router';
import {CommunicationService} from '../../../communication-service';
import {DatePipe} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {PlanComponent} from './plan/plan.component';
import {RepackComponent} from './repack/repack.component';
import {ActionTypeEneum, RepackingStatusConst, RepackingStatusEnum} from '../../../_models/action-type';
import {LDImportStockComponent} from './import-stock/l-d-import-stock.component';
import {Observable} from 'rxjs';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {NavService} from '../../../_services/nav.service';
import {ApiService} from '@next-solutions/next-solutions-base';
import {HttpParams} from '@angular/common/http';

@Component({
  templateUrl: './a-e-repacking.html',
  styleUrls: ['../../form.layout.scss', '../../table.layout.scss', './a-e-repacking.scss'],
  providers: [
    DatePipe
  ]
})


export class AERepackingComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(PlanComponent) planComp: PlanComponent | undefined;
  @ViewChild(RepackComponent) repackComp: RepackComponent | undefined;
  @ViewChild(LDImportStockComponent) importStock: LDImportStockComponent | undefined;
  @ViewChild('stepper') stepper: MatStepper | undefined;
  secondFormGroup: any;

  form: FormGroup;
  repackingPlan: any;
  private subscriber: any;
  actionType: string | undefined;
  actionTypeEneum = ActionTypeEneum;
  parentId: number | undefined;
  repackStatus = new RepackingStatusConst();

  isIgnore = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, private toastr: ToastrService,
              public dialog: MatDialog, private renderer: Renderer2,
              private comm: CommunicationService, private route: ActivatedRoute, private changeDetector: ChangeDetectorRef,
              transService: TranslateService, private datePipe: DatePipe, private tsuService: TsUtilsService,
              private navService: NavService) {
    this.form = this.formBuilder.group(
      {},
    );
  }

  onNext() {
    console.log(this.form);
  }

  updateFirstFormControl(data: any) {
    const detail = data; // this.planModels.data;
    console.log('detail:', detail);
    for (let i = 0; i < detail.length; i++) {
      detail[i].remain = detail[i].quantity + detail[i].inventory;
      console.log(detail, detail[i]);
      let ctrl = this.form.get('quantity' + i);
      if (ctrl !== undefined && ctrl !== null) {
        ctrl.setValue(detail[i].quantity);
      } else {
        ctrl = new FormControl(detail[i].quantity, [Validators.required, Validators.max(detail[i].inventory + detail[i].quantity)]);
        this.form.addControl('quantity' + i, ctrl);
      }
    }
  }


  getDate(strDate: any) {
    // console.log('aadataaaaa', row);
    // return this.datePipe.transform(row.expireDate, AppSettings.API_DATE_FORMAT);
    return new Date(strDate);
  }

  getColumnType(type: any) {
    if (type === undefined) {
      return 0;
    }
    return type;
  }


  ngOnInit() {
    // const headers = [{name: 'Accept', value: 'application/json'}];
    // console.log('thisform:', this.form);
    // this.btnSaveLbl = 'Save';
    this.subscriber = this.route.params.subscribe(params => {
      this.actionType = params.actType;
      this.parentId = params.id;
      if (params.actType !== ActionTypeEneum.new) {
        this.getPlandetail(params);
      }

    });
    // this.utils.hideLoading();
  }

  ngAfterViewInit(): void {
    this.navService.title = '';
  }

  getPlandetail(pars: any) {
    // console.log('onSearchData1:', event, this.search);

    const method = this.userService.get(`/repacking-plannings/${pars.id}`, new HttpParams());
    method.subscribe((repackingPlan: any) => {
      this.repackingPlan = repackingPlan;

      this.setStepper();

    }, (error1: any) => {
      // this.tsu.showError(error1);
    });
  }


  setStepper() {
    if (this.stepper) {
      if (this.repackingPlan.status === RepackingStatusEnum.NEW.toString()) {
        this.planComp?.refresh(this.repackingPlan);
      } else if (this.repackingPlan.status === RepackingStatusEnum.REPACKED.toString()) {
        this.stepper.selectedIndex = 1;
      } else if (this.repackingPlan.status === RepackingStatusEnum.IMPORTED.toString()) {
        this.isIgnore = true;
        this.stepper.selectedIndex = 1;
        this.isIgnore = false;
        this.stepper.selectedIndex = 2;
      }
    }
  }

  onPrintQrCode() {
    this.repackComp?.onPrintQrCode();
  }

  onSelectionChange(event: any) {
    if (!this.isIgnore) {
      this.getRepackingFromApi(this.repackingPlan.id).then(resolve => {
        resolve.repack.then(data => {
          this.repackingPlan = data;
        })
          .then(() => {
            switch (event.selectedIndex) {
              case 0:
                this.planComp?.refresh(this.repackingPlan);
                break;
              case 1:
                this.repackComp?.refresh(this.repackingPlan, this.actionType, this.parentId);
                break;
              case 2:
                this.importStock?.refresh(this.repackingPlan, this.actionType, this.parentId);
                break;

            }
          });
      });
    }

  }

  async getRepackingFromApi(repackingPlanId: number) {
    const repack = this.getRepackingPlan(repackingPlanId).toPromise();
    await repack;
    return {repack};
  }

  getRepackingPlan(repackingPlanId: number): Observable<any> {
    return this.userService.get('/repacking-plannings/' + repackingPlanId, new HttpParams());
  }

  ngOnDestroy(): void {
    if (this.subscriber) {
      this.subscriber.unsubscribe();
    }
  }

  // onSuccessFunc = (data) => {
  //   const msg = this.transService.instant( '.' + this.actionType + '.success');
  //   this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
  //   this.toastr.success(msg);
  //   console.log(msg, data);
  // }
}
