import {CookieService} from 'ngx-cookie-service';

import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {TsUtilsService} from '../../../../base/Utils/ts-utils.service';
import {CommunicationService} from '../../../../communication-service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {ChangeDetectorRef, Component, OnInit, Renderer2} from '@angular/core';
import {DatePipe, DecimalPipe, Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {TranslateService} from '@ngx-translate/core';
import {ActionTypeEneum, ClaimEnum, RepackingStatusConst, RepackingStatusEnum} from '../../../../_models/action-type';
import {ProductDescriptionsUtils} from '../../../../_utils/product.descriptions.utils';
import {NumberValidators} from '../../../../base/field/number-validators';
import {HttpParams} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {ClaimDetailModel} from '../../../../_models/claim.detail.model';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';


@Component({
  selector: 'app-repack',
  templateUrl: './repack.html',
  styleUrls: ['./repack.scss'],
  providers: [
    DatePipe,
    DecimalPipe
  ]
})

export class RepackComponent implements OnInit {

  nextFocus: any;
  form: FormGroup;
  repackingCols: Array<string> = ['stt', 'orgQuantity', 'claimQuantity', 'totalQuan', 'Repacking'];
  actionType: string | undefined;
  parentId: number | undefined;
  planModels: MatTableDataSource<any> | undefined;
  repackingPlan: any;
  repackingStatus = new RepackingStatusConst();
  claimDetails: any[] = [];

  isPrint: any;
  claimPendingApprove = false;
  claimId: number | undefined;
  hasClaim = false;

  constructor(fb: FormBuilder,
              private router: Router,
              private apiService: ApiService,
              cookieService: CookieService,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private renderer: Renderer2,
              private comm: CommunicationService,
              private route: ActivatedRoute,
              private changeDetector: ChangeDetectorRef,
              private transService: TranslateService,
              private datePipe: DatePipe,
              private tsuService: TsUtilsService,
              location: Location,
              private decimalPipe: DecimalPipe,
              protected authoritiesService: AuthoritiesService) {
    this.form = fb.group({});
    this.route.paramMap.subscribe((params: any) => {
      this.actionType = params.params.actionType;
    });
  }

  getTotalQuan(dataRow: any, row: any, packingBlockId: any, event: any) {
    // console.log('rowwwwssQuannnnnnnnnn', dataRow, this.form, event);
    let total = 0;
    if (event) {
      dataRow.repackQuan[packingBlockId].quantity = event.target.value;
    }
    if (dataRow.repackQuan) {
      for (const item of Object.keys(dataRow.repackQuan)) {
        total = total + dataRow.repackQuan[item].quantity * dataRow.repackQuan[item].packingBlock;
      }
    }
    this.form.get('totalQuan' + row)?.setValue(this.decimalPipe.transform(dataRow.totalQuan - total, '1.0-0'));
  }

  updateControl(detail: any) {
    // const detail = this.planModels.data;
    // detail.repackQuan = [detail.length];
    console.log('detaillllllll:', detail);
    let ctrl;
    let orgCtrl;
    let claimCtrl;
    for (let i = 0; i < detail.length; i++) {
      const productPackings = detail[i].productPacking.product.productPackings;
      detail[i].repackQuan = {}; // [productPackings.length];
      detail[i].totalQuan = detail[i].quantity * detail[i].productPacking.packingType.quantity;

      detail[i].orgQuantity = (!isNaN(Number(detail[i].orgQuantity)) ? detail[i].orgQuantity : detail[i].quantity)
        * detail[i].productPacking.packingType.quantity;

      detail[i].claimQuantity = (!isNaN(Number(detail[i].claimQuantity)) ? detail[i].claimQuantity : 0)
        * detail[i].productPacking.packingType.quantity;

      ctrl = this.form.get('totalQuan' + i);
      orgCtrl = this.form.get('orgQuantity' + i);
      claimCtrl = this.form.get('claimQuantity' + i);
      if (!ctrl) {
        ctrl = new FormControl(detail[i].totalQuan,
          [Validators.required,
            Validators.min(0),
            Validators.max(0),
            NumberValidators.integerValidation()]);
        this.form.addControl('totalQuan' + i, ctrl);
      }
      if (!orgCtrl) {
        orgCtrl = new FormControl(detail[i].orgQuantity,
          [Validators.required,
            NumberValidators.integerValidation()]);
        this.form.addControl('orgQuantity' + i, orgCtrl);
      }
      if (!claimCtrl) {
        claimCtrl = new FormControl(detail[i].claimQuantity,
          [Validators.required,
            NumberValidators.integerValidation()]);
        this.form.addControl('claimQuantity' + i, claimCtrl);
      }
      for (let j = 0; j < productPackings.length; j++) {
        let value = 0; // detail[i].repackingPlanningDetailRepackeds; //productPacking.product.productPackings[j].quantity;
        ctrl = this.form.get('quantity' + i + '_' + j);
        if (!ctrl) {
          ctrl = new FormControl(0, [Validators.required, Validators.min(0), NumberValidators.integerValidation()]);
          this.form.addControl('quantity' + i + '_' + j, ctrl);
        }


        // detail[i].repackQuan[id] = value;
        if (!detail[i].repackQuan[productPackings[j].id]) {
          detail[i].repackQuan[productPackings[j].id] = {};
        }
        detail[i].repackQuan[productPackings[j].id].quantity = 0;
        detail[i].repackQuan[productPackings[j].id].packingBlock = productPackings[j].packingType.quantity;

        const packageds = detail[i].repackingPlanningDetailRepackeds;

        if (packageds !== undefined && packageds.length > 0) {
          for (const packaged of packageds) {
            if (packaged.productPacking.id === productPackings[j].id) {
              value = packaged.quantity;
              break;
            }
          }
        } else { // truong hop tao moi repackingPlanningDetailRepackeds se = []
          if (detail[i].productPacking.id === productPackings[j].id) {
            value = detail[i].quantity;
          }
        }
        // console.log(this.form)
        detail[i].repackQuan[productPackings[j].id].quantity = value;
        this.form.get('quantity' + i + '_' + j)?.setValue(value);
        this.getTotalQuan(detail[i], i, null, null);

        if (this.actionType === ActionTypeEneum.view) {
          this.form.get('quantity' + i + '_' + j)?.disable();
        }
      }
    }
  }

  ngOnInit() {
    this.isPrint = false;
  }

  onDelete(row: any, ind: any) {
    // this.planModels.data.splice(ind, 1);
    // this.planModels._updateChangeSubscription();
  }

  refresh(repackingPlan: any, actionType: string | undefined, parentId: number | undefined) {
    this.claimPendingApprove = false;
    console.log(actionType, repackingPlan);
    this.actionType = actionType;
    this.parentId = parentId;
    this.repackingPlan = repackingPlan;

    const claimMap = new Map<string, number>();
    let key = '';
    if (repackingPlan.status !== RepackingStatusEnum.CANCEL && !this.isViewNewRepackingPlanning(repackingPlan)) {
      const params = new HttpParams()
        .set('type', ClaimEnum.REPACKING_PLANNING)
        .set('referenceId', '' + this.repackingPlan.id);
      this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', params)
        .subscribe((claimDetails: ClaimDetailModel[]) => {
            this.claimDetails = claimDetails;
            if (this.claimDetails.length > 0) {
              this.hasClaim = true;
              if (this.repackingPlan.status === this.repackingStatus.REPACKED && this.actionType !== ActionTypeEneum.view
              ) {
                this.showWarningMsg('claim.approve.not.repack');
              }
              this.claimId = this.claimDetails[0].claim.id;
              this.claimDetails.forEach((detail: ClaimDetailModel) => {
                key = '' + detail.expireDate + detail.productPacking?.id;
                claimMap.set(key, detail.quantity ? detail.quantity : 0);
              });
            }

            this.updateData(claimMap);
          },
          (error: any) => {
            const msg = this.transService.instant(error);
            this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
            this.toastr.error(msg);
            this.claimPendingApprove = true;
            this.updateData();
          });
    } else {
      this.updateData();
    }
  }

  isViewNewRepackingPlanning(repackingPlan: any) {
    return repackingPlan.status === RepackingStatusEnum.NEW && this.actionType === ActionTypeEneum.view;
  }

  showWarningMsg = (msgKey: string) => {
    const msg = this.transService.instant(msgKey);
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.warning(msg);
  }

  updateData(map?: Map<string, number>) {
    if (map) {
      let checkKey = '';
      let value: any;
      this.repackingPlan.repackingPlanningDetails.forEach((repackingDetail: any) => {
        checkKey = '' + repackingDetail.expireDate + repackingDetail.productPacking.id;
        value = map.get(checkKey) ? map.get(checkKey) : 0;
        if (value) {
          repackingDetail.orgQuantity = Number(repackingDetail.quantity);
          repackingDetail.claimQuantity = value;
          repackingDetail.quantity = Number(repackingDetail.quantity) - value;
        } else {
          repackingDetail.orgQuantity = Number(repackingDetail.quantity);
          repackingDetail.claimQuantity = 0;
        }
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(repackingDetail.productPacking.product);
      });
    } else {
      this.repackingPlan.repackingPlanningDetails.forEach((repackingDetail: any) => {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(repackingDetail.productPacking.product);
      });
    }

    this.updateTable(this.repackingPlan);
    this.isPrint = false;
  }

  onSaveRepacking() {
    const data = [];
    for (const item of (this.planModels?.data ? this.planModels.data : [])) {

      for (const pack in item.repackQuan) {
        if (item.repackQuan[pack].quantity >= 0) {
          console.log(item);
          data.push({
            expireDate: item.expireDate,
            repackingPlanningDetail: {id: item.id},
            productPacking: {id: pack},
            quantity: item.repackQuan[pack].quantity
          });
        }

      }

    }
    // this.fv.repackingPlanningDetails = this.planModels.data;
    // console.log('save', this.fv, data);
    const method = this.apiService.post(`/repacking-plannings/repack/${this.parentId}`, data);
    this.tsuService.execute6(method, this.saveSuccess, 'common.confirmSave', [' ']);
  }

  saveSuccess = (data: any) => {
    const msg = this.transService.instant('.repack.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
    this.isPrint = true;

    const method = this.apiService.get(`/repacking-plannings/${this.parentId}`, new HttpParams());
    method.subscribe((repackingPlan: any) => {
      this.repackingPlan = repackingPlan;
      this.refresh(this.repackingPlan, this.actionType, this.parentId);
    });

  }

  onExportQrCode() {
    // const method = this.userService.post(`/repacking-plannings/print-qrcode/${this.parentId}`, null);
    this.tsuService.execute6(null, this.printQRCode, 'common.confirmExportExcel', ['common.QRCode']);
  }

  printQRCode = (data: any) => {
    this.apiService.saveFile(`/repacking-plannings/print-qrcode/${this.parentId}`, null, {
      headers: undefined,
      params: undefined
    });
  }

  updateTable(data: any) {
    if (!data) {
      return;
    }
    this.planModels = new MatTableDataSource(data.repackingPlanningDetails);
    this.updateControl(this.planModels.data);
  }

  onPrintQrCode() {
    console.log(this.planModels?.data, this.repackingPlan);
    const divContents = [];
    const store = this.repackingPlan.store.code;
    const hotline = this.repackingPlan.store.phone;
    const repackeDate = this.datePipe.transform(this.repackingPlan.repackedDate, 'HH:mm dd-MM-yyyy', '-0');
    for (const repackingDetail of (this.planModels?.data ? this.planModels.data : [])) {
      const expiredDate = this.datePipe.transform(repackingDetail.expireDate, environment.DIS_DATE_FORMAT, '-0');
      const repackingPlanningDetailRepackeds = repackingDetail.repackingPlanningDetailRepackeds;
      for (const entityRepacked of repackingPlanningDetailRepackeds) {
        const packingCode = entityRepacked.productPacking.code;
        const qrBase64 = entityRepacked.qRCode;
        const quantityPackingType = entityRepacked.productPacking.packingType.quantity;
        const packingName = entityRepacked.productPacking.name;

        const content = this.createContentPrint(packingCode, qrBase64, expiredDate, store,
          packingName, quantityPackingType, repackeDate, hotline);
        for (let i = 0; i < entityRepacked.quantity; i++) {
          divContents.push(content);
        }

      }
    }

    this.openWindow(divContents);
  }

  createContentPrint(code: any, base64: any, expireDate: any, store: any, name: any,
                     quantity: any, repackDate: any, hotline: any) {
    const divContent = ' <div class="invoiceForm" style="display: flex;\n' +
      '  height: 1.94in;\n' +
      '  width: 3.14in;\n' +
      '  font-size: 15px;">\n' +
      '    <div class="qrCode" style="    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: start;\n' +
      '    align-items: center;\n' +
      '    margin: 0.15in 0.2in 0 0.3in;">\n' +
      '      <div class="code">\n' +
      '        <label>Code: </label>\n' +
      '        <label style="word-break: break-all">' + code + '</label>\n' +
      '      </div>\n' +
      '      <div id="img">\n' +
      '        <img src="data:image/png;base64, ' + base64 + '" style="width: 1in;height: 1in"/>\n' +
      '      </div>\n' +
      '      <div class="expiredDate" style="display: flex;\n' +
      '       flex-direction: column;' +
      '       margin-top: 0.025in">\n' +
      '        <label>Expire date: </label>\n' +
      '        <label>' + expireDate + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '    <div class="information" style="    margin: 0.15in 0 0.25in 0;\n' +
      '    width: calc(100% - 1.5in);\n' +
      '    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: space-between;\n' +
      '    font-size: 14px;">\n' +
      '      <div class="store">\n' +
      '        <label>Store: </label>\n' +
      '        <label style="word-break: break-all">' + store + '</label>\n' +
      '      </div>\n' +
      '      <div class="name">\n' +
      '        <label>Name: </label>\n' +
      '        <label>' + name + '</label>\n' +
      '      </div>\n' +
      '      <div class="block">\n' +
      '        <label>Block: </label>\n' +
      '        <label>' + quantity + '</label>\n' +
      '      </div>\n' +
      '      <div class="repackDate">\n' +
      '        <label>Repacked date: </label>\n' +
      '        <label style="height: 0.17in">' + repackDate + '</label>\n' +
      '      </div>\n' +
      '      <div class="hotline">\n' +
      '        <label>Hotline:</label>\n' +
      '        <label>' + hotline + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '  </div>\n';

    return divContent;
  }

  openWindow(contents: any) {
    const a = window.open('', '');
    if (a) {
      a.document.write('<html><head><style>div{' +
        'margin-bottom: 0.05in}\n' +
        'div:last-child { margin-bottom: 0;}\n@page {\n' +
        '  size: 3.15in 1.97in;\n' +
        '  margin: 0;\n' +
        '}</style></head>');
      a.document.write('<body style="margin: 0;padding: 0; ">');
      for (const content of contents) {
        a.document.write(content);
      }
      a.document.write('</body></html>');
      a.document.close();
      setTimeout(() => {
        a.print();
        a.close();
      }, 250);
    }
  }


  onAuthority(authority: string) {
    return this.authoritiesService.hasAuthority(authority);
  }

  onClaimDetail() {
    if (this.claimId) {
      this.router.navigate(['/claim/dashboard/' + this.claimId]).then();
    }
  }
}
