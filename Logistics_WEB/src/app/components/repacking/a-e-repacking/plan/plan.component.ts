import {CookieService} from 'ngx-cookie-service';

import {FormBuilder} from '@angular/forms';
import {PlanLayout} from './plan.layout';
import {TsUtilsService} from '../../../../base/Utils/ts-utils.service';
import {CommunicationService} from '../../../../communication-service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {ChangeDetectorRef, Component, OnInit, Renderer2} from '@angular/core';

import {DatePipe, Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {HttpParams} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {ActionTypeEneum, ClaimEnum, RepackingStatusConst, RepackingStatusEnum} from '../../../../_models/action-type';
import {ClaimDetailModel} from '../../../../_models/claim.detail.model';
import {ApiService, AuthoritiesService, UtilsService} from '@next-solutions/next-solutions-base';
import {PalletsProductPackingGroupByExpireDateComponent} from '../../../dialog-select-product-packing/pallets-product-packing-group-by-expire-date.component';


@Component({
  selector: 'app-plan',
  templateUrl: './plan.html',
  styleUrls: ['./plan.scss'],
  providers: [
    DatePipe
  ]
})


export class PlanComponent implements OnInit {

  title?: string;
  model: PlanLayout;
  repackingStatus = new RepackingStatusConst();

  isFirstTime = true;
  isApprovedClaim = false;

  private subscriber: any;

  get ActionTypeEnum() {
    return ActionTypeEneum;
  }

  constructor(fb: FormBuilder,
              private router: Router,
              private apiService: ApiService,
              cookieService: CookieService,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private renderer: Renderer2,
              private comm: CommunicationService,
              private route: ActivatedRoute,
              private changeDetector: ChangeDetectorRef,
              private transService: TranslateService,
              private datePipe: DatePipe,
              tsuService: TsUtilsService,
              location: Location,
              private utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService) {
    this.model = new PlanLayout(tsuService, apiService, fb, route, this.datePipe, transService, utilsService, toastr, location, this.dialog, router, authoritiesService);
  }

  getExceptId() {
    let params = new HttpParams()
      .set('storeId', this.model.formData.store ? this.model.formData.store.id : '')
      .set('distributorId', this.model.formData.distributor ? this.model.formData.distributor.id : '')
      .set('text', '')
      .set('pageNumber', '1')
      .set('pageSize', '20')
      .set('palletSteps', '1');
    if (this.model.actionType === ActionTypeEneum.edit) {
      params = params.set('repackingPlanningId', '' + this.model.parentId);
    }
    params = params.set('-1', '');
    for (const item of this.model.getTblData()) {
      params = params.append('' + item.repackingPlanningDetail.productPacking.id, item.repackingPlanningDetail.expireDate);
    }
    return params;
  }

  getDate(strDate: any) {
    return new Date(strDate);
  }

  getColumnType(type: any) {
    if (type === undefined) {
      return 0;
    }
    return type;
  }

  onAddEvent(event: any) {
    // const data = {
    //   type: ActionTypeEneum.PP,
    //   params: this.getExceptId(),
    //   ldCols: [{name: 'productPacking.code', type: 'view', label: 'common.packing.code'},
    //     {name: 'productPacking.product.name', type: 'view', label: 'common.product.name'},
    //     {name: 'productPacking.packingType.quantity', type: 'view', label: 'common.packing.block'},
    //     {name: 'expireDate', type: 'view', inputType: 'date', label: 'common.expireDate'},
    //     {name: 'quantity', type: 'view', label: 'common.quantity', inputType: 'number'},
    //   ],
    //   entity: 'repacking',
    //   rowIdName: 'id',
    //   multiSel: true,
    //   fromComponent: 'repackingPlanning',
    //   searchUrl: '/pallets/product-packing-group-by-expire-date'
    // };
    //
    // const dialogRef = this.dialog.open(LDDialogSearchComponent, {
    //   disableClose: false,
    //   width: '70%',
    //   maxHeight: '90vh',
    //   data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   this.model.addTableData(result);
    // });

    const productPackingIdExpireDate: { productPackingId: number, expireDate: string }[] = [{
      productPackingId: -1,
      expireDate: ''
    }];
    for (const item of this.model.getTblData()) {
      productPackingIdExpireDate.push({
        productPackingId: item.repackingPlanningDetail.productPacking.id,
        expireDate: item.repackingPlanningDetail.expireDate
      });
    }
    const dialogRef = this.dialog.open(PalletsProductPackingGroupByExpireDateComponent, {
      disableClose: false,
      width: '90%',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        storeId: this.model.formData.store ? this.model.formData.store.id : 0,
        distributorId: this.model.formData.distributor ? this.model.formData.distributor.id : 0,
        repackingPlanningId: this.model.actionType === ActionTypeEneum.edit ? this.model.parentId : 0,
        palletSteps: 1,
        productPackingIdExpireDate
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.model.addTableData(result);
    });

  }

  ngOnInit() {
    this.model.getDataBinding();
  }

  refresh(repackingPlanning: any) {
    if (repackingPlanning.status === this.repackingStatus.NEW && this.model.actionType !== ActionTypeEneum.view) {
      const params = new HttpParams()
        .set('type', ClaimEnum.REPACKING_PLANNING)
        .set('referenceId', '' + repackingPlanning.id);
      this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', params)
        .subscribe((claimDetails: ClaimDetailModel[]) => {
            if (claimDetails.length > 0) {
              this.isApprovedClaim = true;
              if (repackingPlanning.status === RepackingStatusEnum.NEW &&
                this.model.actionType !== ActionTypeEneum.view) {
                this.model.showWarningMsg('common.claim.approved.not.update');
              }
              if (this.isFirstTime) {
                // this.model.cols[this.model.cols.indexOf(this.model.quantityCol)].type = 'view';
                this.model.cols.splice(this.model.cols.indexOf(this.model.addCol), 1);
                this.model.setCols(this.model.cols);
                this.isFirstTime = false;
              }
            }
          },
          (error: any) => {
            this.isApprovedClaim = false;
          });
    }
  }


  onSave() {
    this.model.onSavePlan();
  }


  isShowButton(event: any) {

    if (this.model.actionType === ActionTypeEneum.view || this.model.actionType === ActionTypeEneum.view_from_claim) {
      event.isShow = false;
      return;
    }

    event.isShow = (!this.model.formData.status || this.model.formData.status === RepackingStatusEnum.NEW)
      && !this.isApprovedClaim;
    return;
  }

  onCellEvent(event: any) {
    if (event.act === ActionTypeEneum.del) {
      this.model.tsTable.removeRows(event.ind, 1);
    }
  }

  isEnableNext(): boolean {
    if (isNaN(Number(this.model.formData.status))) {
      return false;
    }
    return true;
  }
}
