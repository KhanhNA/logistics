import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TsUtilsService} from '../../../../base/Utils/ts-utils.service';

import {LDBaseLayout} from '../../../../base/l-d-base.layout';
import {ToastrService} from 'ngx-toastr';
import {FieldConfigExt} from '../../../../base/field/field.interface';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {ActionTypeEneum, RepackingStatusEnum} from '../../../../_models/action-type';
import {NumberValidators} from '../../../../base/field/number-validators';
import {ProductDescriptionsUtils} from '../../../../_utils/product.descriptions.utils';
import {AsyncDataUtils} from '../../../../_utils/async.data.utils';
import {ApiService, AuthoritiesService, UtilsService} from '@next-solutions/next-solutions-base';
import {HttpParams} from '@angular/common/http';
import {RepackingPlanDetailModel} from "../../../../_models/repacking/repacking.plan.detail.model";
import {ProductPackingModel} from "../../../../_models/product.packing.model";
import {ProductModel} from "../../../../_models/product.model";
import {ProductPackingPriceModel} from "../../../../_models/product.packing.price.model";
import {CommonUtils} from "../../../../_utils/common.utils";
import {RepackingPlanDetailPalletModel} from "../../../../_models/repacking/repacking.plan.detail.pallet.model";
import {StoreModel} from "../../../../_models/store.model";
import {RepackingPlanningModel} from "../../../../_models/repacking.planning.model";
import {DistributorModel} from "../../../../_models/distributor.model";

// @Directive()
export class PlanLayout extends LDBaseLayout {
  storesCollection: any[] = [];
  distributorCollection: any[] = [];

  tempDetails: RepackingPlanDetailModel[] = [];

  addCol = new FieldConfigExt({
    type: 'btnGrp',
    inputType: 'text',
    name: 'add',
    value: ['delete'],
    authorities: {
      add: ['get/pallets/product-packing-group-by-expire-date'],
      delete: ['post/repacking-plannings', 'patch/repacking-plannings/{id}']
    },
    titles: {
      add: 'common.title.add',
      delete: 'common.title.delete',
    }
  });

  changeQuantity = (row: any) => {
    console.log(row);
    this.showMaxQuantityWarning(row, 'common.packing.max.quantity');
    row.obj.total = Number(row.obj.quantity) * row.obj.repackingPlanningDetail.productPacking.packingType.quantity;
    // row.obj.inventory = row.obj.totalPack - Number(row.obj.quantity);
  };
  quantityCol = new FieldConfigExt({
    type: this.actionType === ActionTypeEneum.view ? 'view' : 'input',
    label: 'common.packing.quantity',
    inputType: 'number',
    name: 'quantity',
    emit: this.changeQuantity,
    validations: [
      {
        name: 'integerNumber',
        validator: NumberValidators.integerValidation(),
        message: 'validation.integer'
      },
      {
        name: 'minQuantity',
        validator: NumberValidators.minQuantityValidation(0),
        message: 'validation.minQuantity'
      },
    ],
  });
  palletCol = new FieldConfigExt({
    type: 'view',
    label: 'common.pallet',
    inputType: 'text',
    name: 'palletDetail.pallet.displayName',
  });

  constructor(tsuService: TsUtilsService,
              private userService: ApiService,
              fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              transService: TranslateService,
              private utilsService: UtilsService,
              toastr: ToastrService,
              location: Location,
              public dialog: MatDialog,
              protected router: Router,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    this.getFormDataBinding();
  }

  getFormDataBinding() {
    this.getAsyncStoresAndDistributorsData().then(data => {
      data.stores.then(response => {
        this.storesCollection.push(...response.content);
      });
      data.distributors.then(distributors => {
        this.distributorCollection.push(...distributors);
      });
    }).then(() => {
      if (this.actionType === ActionTypeEneum.new) {
        const palletColIndex = this.cols.indexOf(this.palletCol);
        if (palletColIndex >= 0) {
          this.cols.splice(palletColIndex, 1);
          this.setCols(this.cols);
        }
      }
      this.setCols(this.cols);
      this.init(this.inputFields);
      this.formData = {
        code: '',
        store: this.storesCollection[0],
        distributor: this.distributorCollection[0],
        description: ''
      };
    });
  }

  async getAsyncStoresAndDistributorsData() {
    return AsyncDataUtils.getAsyncDataStoresAndDistributors(this.tsuService, this.tsuService);
  }

  changeFormValue = (data: any) => {
    this.tsTable.removeRows(0, this.getTblData().length);
  };

  convertToRows(result: any) {
    const rows = [];
    if (result) {
      for (const item of Object.keys(result)) {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(result[item].productPacking.product);
        ProductModel.reduceCircleReference(result[item].product);
        result[item].productPackingPrices?.forEach((price: any) => {
          ProductPackingPriceModel.reduceCircleReference(price);
        });

        rows.push({
          repackingPlanningDetail: {
            productPacking: result[item].productPacking,
            expireDate: result[item].expireDate,
          },
          maxQuantity: result[item].quantity,
          quantity: result[item].quantity
        });
        // if (this.actionType === ActionTypeEneum.new && this.formData.repackingPlanningDetails) {
        //   this.formData.repackingPlanningDetails.push({
        //     productPacking: result[item].productPacking,
        //     expireDate: result[item].expireDate,
        //     maxQuantity: result[item].quantity,
        //     quantity: result[item].quantity
        //   });
        // }
      }
    }
    return rows;
  }

  getDataBinding() {
    if (this.actionType === ActionTypeEneum.new) {
      return;
    }
    this.userService.get(`/repacking-plannings/${this.parentId}`, new HttpParams())
      .subscribe((plan: any) => {
        this.updateData(plan);
      });
  }

  updateData(plan: any) {
    if (!plan) {
      return;
    }
    if (this.actionType === ActionTypeEneum.edit && plan.status === RepackingStatusEnum.NEW) {
      const palletColIndex = this.cols.indexOf(this.palletCol);
      if (palletColIndex >= 0) {
        this.cols.splice(palletColIndex, 1);
        this.setCols(this.cols);
      }

    }
    if (plan.status !== RepackingStatusEnum.NEW) {
      this.cols.forEach(col => {
        if (col.type === 'input') {
          col.type = 'view';
        }
      });
    }

    this.formData = plan;
    plan.content = plan.repackingPlanningDetails;


    let params = new HttpParams()
      .set('storeId', this.formData.store ? this.formData.store.id : '')
      .set('distributorId', this.formData.distributor ? this.formData.distributor.id : '')
      .set('text', '')
      .set('pageNumber', '1')
      .set('pageSize', '20')
      .set('isIncludeProductPackingIdExpireDate', 'true')
      .set('palletSteps', '1');
    if (this.actionType === ActionTypeEneum.edit) {
      params = params.set('repackingPlanningId', '' + this.parentId);
    }
    params = params.set('-1', '');
    for (const item of plan.repackingPlanningDetails) {
      params = params.append('' + item.productPacking.id, item.expireDate);
    }

    this.userService.get('/pallets/product-packing-group-by-expire-date', params)
      .subscribe((inventoryResponse: any) => {
        const inventoryMap = new Map<string, any>();
        inventoryResponse.content.forEach((inventory: any) => {
          inventoryMap.set('' + inventory.expireDate + inventory.productPacking.id, inventory);
        });
        this.initTableData(plan, inventoryMap);
      });


  };

  initTableData(plan: any, inventoryMap: Map<string, any>) {
    const dataTable: any[] = [];
    if (plan.status !== RepackingStatusEnum.NEW || this.actionType === ActionTypeEneum.view) {
      plan.repackingPlanningDetails.forEach((repackingDetail: RepackingPlanDetailModel) => {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(repackingDetail.productPacking?.product);
        if (repackingDetail.repackingPlanningDetailPallets.length > 0) {
          repackingDetail.repackingPlanningDetailPallets.forEach((detailPallet: RepackingPlanDetailPalletModel) => {
            // if (!repackingDetail.inventory) {
            //   repackingDetail.inventory = 0;
            // }
            // repackingDetail.totalPack = Number(repackingDetail.inventory) + Number(repackingDetail.quantity);
            if (detailPallet.palletDetail?.pallet?.store?.id) {
              detailPallet.palletDetail.pallet.store = new StoreModel(detailPallet.palletDetail.pallet.store.id);
            }

            dataTable.push(detailPallet);
          });
        } else {
          repackingDetail.repackingPlanning = repackingDetail.repackingPlanning?.id
            ? new RepackingPlanningModel(repackingDetail.repackingPlanning.id) : undefined;
          dataTable.push(
            {
              repackingPlanningDetail: repackingDetail,
              quantity: repackingDetail.quantity
            });

        }
      });
    } else {
      let key = '';
      plan.repackingPlanningDetails.forEach((planningDetail: RepackingPlanDetailModel) => {
        planningDetail.repackingPlanning = planningDetail.repackingPlanning?.id
          ? new RepackingPlanningModel(planningDetail.repackingPlanning.id) : undefined;
        planningDetail.repackingPlanningDetail = planningDetail;
        key = '' + planningDetail.expireDate + planningDetail.productPacking?.id;
        if (inventoryMap.has(key)) {
          planningDetail.maxQuantity = inventoryMap.get(key).quantity;
        } else {
          planningDetail.maxQuantity = planningDetail.quantity;
        }
      });

      dataTable.push(...plan.repackingPlanningDetails);
    }
    plan.content = dataTable;

    this.updateTable(dataTable);
  }

  callApi = () => {
    this.formData.repackingPlanningDetails = [];
    this.getTblData().forEach((row: RepackingPlanDetailModel) => {
      if (row.repackingPlanningDetail?.productPacking?.uom) {
        // row.repackingPlanningDetail.productPacking = row.repackingPlanningDetail.productPacking
        //   ? new ProductPackingModel(row.repackingPlanningDetail.productPacking.id) : undefined;
        delete row.repackingPlanningDetail.productPacking.uom;
      }
      // (row.palletDetail && !row.palletDetail.pallet.displayName)
      if (this.actionType === ActionTypeEneum.new || this.actionType === ActionTypeEneum.edit) {
        this.formData.repackingPlanningDetails.push({
          ...row.repackingPlanningDetail,
          quantity: row.quantity
        });
      } else {
        this.showErrorMsg('action.invalid');
        // row.repackingPlanningDetail.quantity = 0;
        // for (const detailPallet of row.repackingPlanningDetail.repackingPlanningDetailPallets) {
        //   row.repackingPlanningDetail.quantity = Number(row.repackingPlanningDetail.quantity) + Number(detailPallet.quantity);
        // }
        // this.formData.repackingPlanningDetails.push(row.repackingPlanningDetail);
      }
    });

    console.log(this.formData)
    // this.tempDetails = JSON.parse(JSON.stringify(this.formData.repackingPlanningDetails));
    // CommonUtils.reduceCircleReferenceObject(this.formData);
    let method;
    // const copiedObj = {}
    // CommonUtils.deepCopyAndReduceReferenceManual(this.formData, copiedObj, ['parent']);
    const repackingPlan = {
      id: this.formData.id,
      code: this.formData.code,
      description: this.formData.description,
      distributor: isNaN(Number(this.formData.distributor.id)) ? undefined : new DistributorModel(Number(this.formData.distributor.id)),
      store: isNaN(Number(this.formData.store.id)) ? undefined : new StoreModel(Number(this.formData.store.id)),
      repackingPlanningDetails: this.formData.repackingPlanningDetails.map((detail: any) => {
        return {
          quantity: detail.quantity,
          productPacking: new ProductPackingModel(detail.productPacking?.id),
          expireDate: detail.expireDate
        } as RepackingPlanDetailModel;
      }),
      status: isNaN(Number(this.formData.status)) ? 0 : Number(this.formData.status)
    };
    if (this.actionType === ActionTypeEneum.new) {
      method = this.userService.post('/repacking-plannings', [repackingPlan]);
    } else {
      method = this.userService.patch(`/repacking-plannings/${this.parentId}`, repackingPlan);
    }
    return method;
  }

  errFunc = (err: string) => {
    this.utilsService.showErrorToarst(err);
    // this.formData.repackingPlanningDetails = this.tempDetails;
  }

  onSavePlan() {
    // this.formData.repackingPlanningDetails = this.getTblData();
    this.utilsService.executeWithErrorHandle(this.callApi, this.saveSuccess1, '.add.sucess', 'common.confirmSave', ['repacking.plan'], '', [],
      this.errFunc);
  }

  saveSuccess1 = (data: any) => {
    const msg = this.translateService.instant(this.entity + '.' + this.actionType + '.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
    // this.formData.repackingPlanningDetails = this.tempDetails;
    if (data[0] && data[0].status === RepackingStatusEnum.NEW) {
      this.router.navigate(['/app-repacking/edit/' + data[0].id]).then();
    } else if (!data[0]) {
      this.getDataBinding();
    }
  };

  isViewNewRepackingPlanning(repackingPlan: any) {
    return repackingPlan.status === RepackingStatusEnum.NEW && this.actionType === ActionTypeEneum.view;
  }

  inputFields = [
    new FieldConfigExt({
      type: 'input',
      label: 'repacking.ae.code',
      inputType: 'text',
      name: 'code',
      value: '',
      readonly: /*this.actionType === ActionTypeEneum.view ? */'true'/* : 'false'*/,
      binding: 'code',
    }),
    new FieldConfigExt({
      type: 'autocomplete',
      label: 'pallet.ae.store.name',
      inputType: 'text',
      name: 'store',
      value: '',
      options: ['code', 'name'],
      collections: this.storesCollection,
      emit: this.changeFormValue,
      binding: 'store',
      require: 'true',
      // readonly: this.actionType === ActionTypeEneum.view ? 'true' : 'false',
    }),
    new FieldConfigExt({
      type: 'autocomplete',
      label: 'po.ae.distributor.code',
      inputType: 'text',
      name: 'distributor.code',
      value: '',
      options: ['code', 'name'],
      collections: this.distributorCollection,
      emit: this.changeFormValue,
      binding: 'distributor',
      require: 'true',
      // readonly: this.actionType === ActionTypeEneum.view ? 'true' : 'false',
    }),
    new FieldConfigExt({
      type: 'input',
      label: 'repacking.ae.description',
      inputType: 'text',
      value: '',
      name: 'description',
      binding: 'description',
      readonly: this.actionType === ActionTypeEneum.view ? 'true' : 'false',
    })
  ];
  cols = [
    new FieldConfigExt({
      type: 'index',
      // label: 'ld.no',
      // inputType: 'text',
      name: 'stt',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'common.packing.code',
      inputType: 'text',
      name: 'repackingPlanningDetail.productPacking.code',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'common.product.name',
      inputType: 'text',
      name: 'repackingPlanningDetail.productPacking.product.name',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'common.packingType',
      inputType: 'number',
      name: 'repackingPlanningDetail.productPacking.packingType.quantity',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'common.expireDate',
      inputType: 'date',
      name: 'repackingPlanningDetail.expireDate',
    }),
    // new FieldConfigExt({
    //   type: 'view',
    //   label: 'common.inStock',
    //   inputType: 'number',
    //   name: 'inventory',
    // }),
    this.palletCol,
    this.quantityCol,
    new FieldConfigExt({
      type: 'view',
      label: 'common.totalQuan',
      inputType: 'number',
      name: 'total',
    }),
    this.addCol
  ];
}
