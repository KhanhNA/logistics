import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {LDBaseLayout} from '../../base/l-d-base.layout';
import {ToastrService} from 'ngx-toastr';
import {FieldConfigExt} from '../../base/field/field.interface';
import {Search} from '../../_models/Search';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, Location} from '@angular/common';
import {RepackingStatusEnum} from '../../_models/action-type';
import {AsyncDataUtils} from '../../_utils/async.data.utils';
import {DateUtils} from '../../base/Utils/date.utils';
import {environment} from '../../../environments/environment';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {Directive} from '@angular/core';

// @Directive()
export class LDRepackingLayout extends LDBaseLayout {
  inputFields: FieldConfigExt[] | undefined;
  cols: FieldConfigExt[] | undefined;

  printRepackId?: number;
  storesCollection: any[] = [];
  // distributorCollection: any[] = [];
  hasState = false;
  statusOptions = {
    '-1': 'common.status.cancel', 0: 'common.status.new', 3: 'common.status.planned',
    1: 'common.status.repacked', 2: 'common.status.imported'
  };

  constructor(tsuService: TsUtilsService, private userService: ApiService, fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              transService: TranslateService, toastr: ToastrService, location: Location,
              protected authoritiesService: AuthoritiesService
  ) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);

    this.getFormDataBinding();
  }

  getFormDataBinding() {
    this.getAsyncData().then(data => {
      // this.distributorCollection.push({
      //   code: '- ' + this.translateService.instant('common.status.all'),
      //   name: '',
      //   id: ''
      // });
      data.stores.then(response => {
        this.storesCollection.push(...response.content);
      });
      // data.distributors.then(distributors => {
      //   this.distributorCollection.push(...distributors);
      // });
    }).then(() => {
      this.cols = [
        new FieldConfigExt({
          type: 'index',
          label: 'ld.no',
          inputType: 'text',
          name: 'STT',
          value: '',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'repacking.ld.code',
          inputType: 'text',
          name: 'code',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'repacking.ae.description',
          inputType: 'text',
          name: 'description',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'common.status',
          inputType: 'select',
          name: 'statusString',
          options: this.statusOptions
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'repacking.ae.createUser',
          inputType: 'text',
          name: 'createUserObj.name',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'repacking.ae.planningDate',
          inputType: 'date',
          name: 'planningDate',
        }),
        new FieldConfigExt({
          type: 'btnGrp',
          label: 'add',
          inputType: 'text',
          name: 'add',
          require: 'true',
          value: ['edit', 'clear', 'dashboard'],
          authorities: {
            add: ['post/repacking-plannings'],
            edit: ['patch/repacking-plannings/{id}', 'post/import-statements/from-repacking-planning'],
            clear: ['post/repacking-plannings/cancel-repack/{id}', 'post/repacking-plannings/cancel-plan/{id}'],
            dashboard: ['get/repacking-plannings/{id}', '/import-statements/repacking-planning/{id}'],
          },
          titles: {
            add: 'common.title.add',
            edit: 'common.title.edit',
            clear: 'common.title.cancel',
            dashboard: 'common.title.dashboard',
          }
        })
      ];
      this.inputFields = [
        new FieldConfigExt({
          type: 'input',
          label: 'common.text',
          inputType: 'text',
          name: 'text',
          value: '',
          binding: 'text'
        }),
        new FieldConfigExt({
          type: 'autocomplete',
          label: 'pallet.ae.store.name',
          inputType: 'text',
          name: 'store',
          value: '',
          options: ['code', 'name'],
          collections: this.storesCollection,
          binding: 'store',
          require: 'true'
        }),
        // new FieldConfigExt({
        //   type: 'autocomplete',
        //   label: 'po.ae.distributor.code',
        //   inputType: 'text',
        //   name: 'distributor.code',
        //   value: '',
        //   options: ['code', 'name'],
        //   collections: this.distributorCollection,
        //   binding: 'distributor',
        // }),
        new FieldConfigExt({
          type: 'select',
          label: 'common.status',
          inputType: 'text',
          name: 'status',
          binding: 'status',
          options: this.statusOptions
        }),
        new FieldConfigExt({
          type: 'select',
          label: 'common.dateType',
          inputType: 'text',
          name: 'dateType',
          binding: 'dateType',
          options: this.statusOptions
        }),
        new FieldConfigExt({
          type: 'date',
          label: 'common.fromDate',
          inputType: 'date',
          name: 'fromDate',
          value: DateUtils.getDayBeforeOneMonth(),
          binding: 'fromDate',
          require: 'true',
          options: {max: ['toDate']}
        }), new FieldConfigExt({
          type: 'date',
          label: 'common.toDate',
          inputType: 'date',
          name: 'toDate',
          value: DateUtils.getCurrentDate(),
          binding: 'toDate',
          require: 'true',
          options: {min: ['fromDate'], max: ['curDate']}
        }),
        // new FieldConfigExt({
        //   type: 'button',
        //   label: 'common.search',
        //   name: 'search',
        //   emit: this.searchDataBtn,
        //   inputType: 'submit'
        // })
      ];
      this.setCols(this.cols);
      this.init(this.inputFields);
      if (!this.hasState) {
        this.formData = {
          text: '',
          store: this.storesCollection[0] ? this.storesCollection[0] : '',
          // distributor: this.distributorCollection[0] ? this.distributorCollection[0] : '',
          fromDate: DateUtils.getDayBeforeOneMonth(),
          toDate: DateUtils.getCurrentDate(),
          status: '0',
          dateType: '0'
        };
        this.onSearchData();
      }


    });
  }

  async getAsyncData() {
    return AsyncDataUtils.getAsyncDataStoresAndDistributors(this.tsuService, this.tsuService);
  }

  searchDataBtn = () => {
    this.onSearchData();
    return false;
  }

  convertToRows(data: any) {
    const rows = [];
    let row;
    if (data) {
      for (const item of Object.keys(data)) {
        row = {
          quantity: 0,
          amount: 0,
          packingProduct: {
            packingProductId: data[item].packingProductId,
            code: data[item].code,
            name: data[item].name
          }
        };
        rows.push(row);
      }
    }
    return rows;
  }

  updateSearchInfo(search: Search) {
    const params = new HttpParams()
      .set('storeId', this.formData.store ? this.formData.store.id : '')
      // .set('distributorId', this.formData.distributor ? this.formData.distributor.id : '')
      .set('text', this.formData.text ? this.formData.text : '')
      .set('fromDate', this.formData.fromDate ? '' + this.datePipe.transform(this.formData.fromDate, environment.API_DATE_FORMAT) :
        (this.datePipe.transform(DateUtils.getDayBeforeOneMonth(), environment.API_DATE_FORMAT))
          ? '' + this.datePipe.transform(DateUtils.getDayBeforeOneMonth(), environment.API_DATE_FORMAT) : '')
      .set('toDate', this.formData.toDate ? '' + this.datePipe.transform(this.formData.toDate, environment.API_DATE_FORMAT) :
        '' + this.datePipe.transform(DateUtils.getCurrentDate(), environment.API_DATE_FORMAT))
      .set('dateType', this.formData.dateType ? this.formData.dateType : 0)
      .set('status', this.formData.status ? this.formData.status : 0)
      .set('pageNumber', '' + (search.pageNumber + 1))
      .set('pageSize', '' + search.pageSize);
    search.method = this.userService.get('/repacking-plannings', params);
  }

  onCancelPlan(data: any) {
    let method;
    switch (data.row.status) {
      case RepackingStatusEnum.REPACKED:
        method = this.userService.post(`/repacking-plannings/cancel-repack/${data.row.id}`, null);
        break;
      case RepackingStatusEnum.NEW:
        method = this.userService.post(`/repacking-plannings/cancel-plan/${data.row.id}`, null);
        break;
    }
    this.tsuService.execute6(method, this.saveSuccess, 'common.confirmCancel', ['common.repack']);
  }

  saveSuccess = () => {
    const msg = this.translateService.instant(this.entity + '.cancel.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);

    this.onSearchData();
  }

  beforeUpdate = (data: any) => {
    data.content.forEach((repackingPlanning: any) => {
      repackingPlanning.statusString = this.translateService.instant(this.statusOptions[repackingPlanning.status]);
      repackingPlanning.createUserObj.name = repackingPlanning.createUserObj.firstName + ' '
        + repackingPlanning.createUserObj.lastName;
    });
  }
}
