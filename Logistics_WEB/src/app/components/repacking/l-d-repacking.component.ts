import {CookieService} from 'ngx-cookie-service';
import {FormBuilder} from '@angular/forms';

import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {CustomDateAdapter, MY_FORMATS_1} from '../../base/field/custom-date-adapter';
import {CommunicationService} from '../../communication-service';
import {ToastrService} from 'ngx-toastr';
import {LDRepackingLayout} from './l-d-repacking.layout';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginatorIntl } from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {MultilanguagePanigator} from '../../_helpers/multilanguage.paginator';
import {TranslateService} from '@ngx-translate/core';
import {ActionTypeEneum, RepackingStatusConst, RepackingStatusEnum} from '../../_models/action-type';
import {NavService} from '../../_services/nav.service';
import {Subject} from 'rxjs';
import {UiStateService} from '../../_services/ui.state.service';
import {takeUntil} from 'rxjs/operators';
import {DateUtils} from '../../base/Utils/date.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';


@Component({
  selector: 'app-pallet', /* dinh nghia id*/
  templateUrl: './l-d-repacking.html',
  styleUrls: ['../form.layout.scss', '../table.layout.scss', './l-d-repacking.scss'],
  providers: [
    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator},
    DatePipe, {
      provide: DateAdapter, useClass: CustomDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: MY_FORMATS_1
    }
  ]
})
export class LDRepackingComponent implements OnInit, AfterViewInit, OnDestroy {

  packingStatus = new RepackingStatusConst();
  btnSaveLbl = 'common.btn.save';
  model: LDRepackingLayout;

  private unsubscribe$ = new Subject();

  formDataMap = new Map();
  url = '';
  isFirstTime = true;

  constructor(fb: FormBuilder,
              private router: Router,
              private userService: ApiService,
              cookieService: CookieService,
              private toastr: ToastrService,
              private datePipe: DatePipe,
              private serv: CommunicationService,
              tranService: TranslateService,
              public dialog: MatDialog,
              tsuService: TsUtilsService,
              route: ActivatedRoute,
              location: Location,
              private navService: NavService,
              private uiStateService: UiStateService,
              protected authoritiesService: AuthoritiesService
  ) {
    this.model = new LDRepackingLayout(tsuService, userService, fb, route,
      this.datePipe, tranService, toastr, location, authoritiesService);
  }

  ngOnInit() {
    this.uiStateService.uiState$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(state => {
        console.log(state);
        if (this.isFirstTime) {
          this.url = this.router.url;
        }
        if (state && this.isFirstTime) {
          this.isFirstTime = false;
          if (state.formDataMap && state.formDataMap.has(this.url)) {
            const formData = state.formDataMap.get(this.url);
            formData.fromDate = DateUtils.getDateFromString(formData.fromDate);
            formData.toDate = DateUtils.getDateFromString(formData.toDate);
            this.model.formData = formData;
            this.model.hasState = true;

            this.formDataMap = state.formDataMap;
            this.model.onSearchData();
          }

        }
      });

  }

  ngOnDestroy(): void {
    if (!this.formDataMap.has(this.url)) {
      this.formDataMap.set(this.url, this.model.formData);
    }
    this.uiStateService.setUiState$({formDataMap: this.formDataMap});
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngAfterViewInit(): void {
    // this.navService.title = 'repacking.ld.title';
  }

  onAddData(event: any): void {
    this.router.navigate([this.router.url, ActionTypeEneum.new]);

  }

  onEditEvent(event: any) {

  }

  onCellEvent(event: any) {
    console.log('viewwwwwwwwwwwwww', event);

    if (event.act === ActionTypeEneum.clear) {
      this.model.onCancelPlan(event);
      return;
    }

    this.router.navigate([this.router.url, event.act, event.row.id]);
  }

  showButtonEvent(event: any) {
    const row = event.row;
    const act = event.act;
    // console.log('rowwwwwwwwwwwwwwwwwwwwwww', event);
    // event.show = ['edit', 'clear'].includes(act);
    if (event.act === ActionTypeEneum.new) {
      event.isShow = true;
      return;
    }

    switch (event.row.status) {
      case RepackingStatusEnum.IMPORTED:
        event.isShow = [ActionTypeEneum.view].includes(act);
        return;
      case RepackingStatusEnum.CANCEL:
        event.isShow = act === ActionTypeEneum.view;
        return;
      default:
        event.isShow = ['edit', 'clear', ActionTypeEneum.view].includes(act);
    }

  }

  onRowClick(event: any) {
    // console.log(event, this.ld);
    // this.selPallet = event.row;
    // const detail = this.ld.last;
    // detail.setTableData(event.row.palletDetails);
  }

  isShowButton(btn: string, action: string) {
    return true;
    // switch (action) {
    //   case this.ACTION_TYPE.new:
    //     return ['add', 'delete', 'save'].includes(btn);
    //   case this.ACTION_TYPE.editPo:
    //     return ['add', 'delete', 'save', 'printPO'].includes(btn);
    //   case this.ACTION_TYPE.GR:
    //     return ['GR'].includes(btn);
    //   case this.ACTION_TYPE.Pal:
    //     return ['pallet'].includes(btn);
    //   default:
    //     return false;
    //
    // }
  }
}
