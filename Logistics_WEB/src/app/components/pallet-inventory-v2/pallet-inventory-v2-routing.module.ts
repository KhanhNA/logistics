import {RouterModule, Routes} from '@angular/router';
import {AuthoritiesResolverService} from '@next-solutions/next-solutions-base';
import {NgModule} from '@angular/core';
import {PalletInventoryV2Component} from './pallet-inventory-v2.component';


const routes: Routes = [
  {
    pathMatch: 'full',
    path: '',
    resolve: {me: AuthoritiesResolverService},
    component: PalletInventoryV2Component
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PalletInventoryV2RoutingModule {

}
