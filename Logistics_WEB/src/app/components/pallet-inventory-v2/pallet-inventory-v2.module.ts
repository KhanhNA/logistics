import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PalletInventoryV2Component} from './pallet-inventory-v2.component';
import {HttpClient} from '@angular/common/http';
import {MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {SharedModule} from '../../modules/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {PalletInventoryV2RoutingModule} from './pallet-inventory-v2-routing.module';

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/pallet/', suffix: '.json'},
    {prefix: './assets/i18n/', suffix: '.json'},

  ]);
}

@NgModule({
  declarations: [PalletInventoryV2Component],
  imports: [
    SharedModule,
    CommonModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      isolate: true,
    }),
    PalletInventoryV2RoutingModule,
  ]
})
export class PalletInventoryV2Module { }
