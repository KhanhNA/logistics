import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PalletInventoryV2Component } from './pallet-inventory-v2.component';

describe('PalletInventoryV2Component', () => {
  let component: PalletInventoryV2Component;
  let fixture: ComponentFixture<PalletInventoryV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PalletInventoryV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PalletInventoryV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
