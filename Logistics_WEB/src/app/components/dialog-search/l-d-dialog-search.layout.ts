import {FormBuilder, FormControl} from '@angular/forms';
import {HttpParams} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {LDBaseLayout} from '../../base/l-d-base.layout';
import {FieldConfigExt} from '../../base/field/field.interface';
import {Search} from '../../_models/Search';
import {ColumnTypeConstant} from '../../_models/Column.type';
import {Directive, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {ProductDescriptionsUtils} from '../../_utils/product.descriptions.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';


@Directive()
export class LDDialogSearchLayout extends LDBaseLayout implements OnInit {

  selCol = new FieldConfigExt({
    type: 'checkboxAll',
    label: 'add',
    inputType: 'all',
    name: 'add',
  });

  readonly columnType = new ColumnTypeConstant();
  urlSearch: string;
  params: HttpParams;
  method: any;

  lastSearch = '';

  constructor(tsuService: TsUtilsService, private userService: ApiService, fb: FormBuilder, private row: any,
              route: ActivatedRoute, transService: TranslateService, toastr: ToastrService, location: Location,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    this.row.ldCols.push(this.selCol);


    this.setCols(this.row.ldCols);
    this.tsTable.rowIdName = row.rowIdName;
    this.urlSearch = row.searchUrl;
    this.params = row.params;
    this.method = row.method;
    this.setSelection(row.multiSel);
    this.form.addControl('text', new FormControl());
    // console.log(this.row, this.getCols);
  }

  private initByParams() {
    let ctrl;
    let value;
    for (const f of this.getCols) {
      if (f.name) {
        ctrl = this.form.get([f.name]);
        value = this.params.get(f.name);
      }
      if (ctrl && value) {
        ctrl.setValue(value);
      }
    }
  }

  get selecteds() {
    return this.tsTable.allSelect;
  }

  beforeUpdate = (data: any) => {
    for (const productPacking of data.content) {
      if (productPacking.productPackingPrices && !productPacking.productPackingPrices[0]) {
        productPacking.productPackingPrices[0] = {
          price: 0,
          currency: {}
        };
      }
      if (productPacking.product) {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(productPacking.product);
      } else {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(productPacking.productPacking.product);
      }

    }
  }

  onSearchData(page?: any) {
    console.log('abc', page, this.search);
    const search = this.search;
    if (page !== undefined) {
      search.pageNumber = page.pageIndex;
      search.pageSize = page.pageSize;
    }
    this.updateSearchInfo(this.search);

    if (this.search.method !== undefined) {

      this.tsuService.execute6(this.search.method, this.updateTable, '', []);
    }
  }

  updateTable = (data: any) => {
    this.lastSearch = this.search.text;
    this.beforeUpdate(data);
    this.tsTable.updatePaging(data);
    this.afterSearch(data);
  }

  updateSearchInfo(search: Search) {
    // const params = new HttpParams()
    //   .set('page', '' + search.pageNumber)
    //   .set('entityName', 'incentive.IncentivePackingGroupPacking');
    // const objSearch = {incentivePackingGroupId: this.ictPackingGrpId};
    const text = (search.text !== null && search.text !== undefined) ? search.text : '';
    const params = this.params.set('text', text)
      .set('pageNumber', '' + (search.pageNumber + 1))
      .set('pageSize', '' + search.pageSize);

    console.log(params);

    if (this.method) {
      console.log('method:', this.method);
      search.method = this.method;
    } else {
      search.method = this.userService.get(this.urlSearch, params);
    }

  }

  ngOnInit(): void {
    // this.initByParams();
    // for (const item: this.this.cols) {
    //
    // }
  }

  getExceptId() {
    const packingIds = [-1];
    this.formData.poDetails.forEach((packing: any) => {
      if (packing.productPacking) {
        packingIds.push(packing.productPacking.id);
      }

    });
    console.log('excepid', this.formData, packingIds);
    let params = new HttpParams();
    const textSearch = this.search.text;


    const url = '/product-packings';
    // if (this.row.type === 'PPE') {
    //   url = url + '/group-by-expire-date';
    // }
    let mId = -1;
    if (this.formData.manufacturer) {
      mId = this.formData.manufacturer.id;
    }
    params = params.set('text', textSearch);
    params = params.set('manufacturerId', mId.toString());

    params = params.set('exceptId', packingIds.toString());
    params = params.set('pageNumber', '1');
    params = params.set('pageSize', '20');
    return params;
  }

}
