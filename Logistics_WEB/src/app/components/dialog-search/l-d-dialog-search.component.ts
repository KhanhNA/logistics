import {CookieService} from 'ngx-cookie-service';
import {FormBuilder} from '@angular/forms';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {ToastrService} from 'ngx-toastr';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatPaginatorIntl } from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {MultilanguagePanigator} from '../../_helpers/multilanguage.paginator';
import {Component, Inject, OnInit} from '@angular/core';
import {LDDialogSearchLayout} from './l-d-dialog-search.layout';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  templateUrl: './l-d-dialog-search.html',
  styleUrls: ['./l-d-dialog-search.scss'],
  providers: [
    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator}

  ]
})
export class LDDialogSearchComponent implements OnInit {
  layoutModel: LDDialogSearchLayout;
  lastSearchKey = '';

  constructor(private fb: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, toastr: ToastrService,
              private tsuService: TsUtilsService,
              public dialogRef: MatDialogRef<LDDialogSearchComponent>,
              @Inject(MAT_DIALOG_DATA) protected row: any,
              private route: ActivatedRoute,
              transService: TranslateService, location: Location,
              protected authoritiesService: AuthoritiesService) {

    // super(formBuilder, Category, cookieService, userService);

    this.layoutModel = new LDDialogSearchLayout(tsuService, this.userService, fb,
      row, route, transService, toastr, location, authoritiesService);
  }

  onAddEvent(event: any) {
    this.router.navigate([this.router.url, 'new']);
    // this.dialog.open(AECategoryComponent, {disableClose: false, width: '500px', data: event});
  }

  onEditEvent(event: any) {
    // console.log('event:', event);
    this.router.navigate([this.router.url, 'edit', event.row.id]);

  }

  onPutPacking() {
    console.log(this.layoutModel.selecteds);
    this.dialogRef.close(this.layoutModel.selecteds);
  }

  ngOnInit(): void {
    this.layoutModel.onSearchData();
  }

  onSearchData(pageData?: any) {
    this.layoutModel.search.text = this.layoutModel.form.controls.text.value;
    const page = pageData ? pageData : {
      pageIndex: this.layoutModel.search.pageNumber,
      pageSize: this.layoutModel.search.pageSize
    };
    if (this.layoutModel.search.text !== this.layoutModel.lastSearch) {
      page.pageIndex = 0;
      this.layoutModel.tsTable.paginator?.firstPage();
    }
    this.layoutModel.onSearchData(page);
  }

  onPage(page: any) {
    this.layoutModel.tsTable.selectedRowIndex = 0;
    this.onSearchData(page);
  }

}
