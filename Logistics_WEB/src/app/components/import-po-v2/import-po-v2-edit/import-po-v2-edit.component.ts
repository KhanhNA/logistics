import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  DateUtilService,
  IconTypeEnum,
  LoaderService,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {CommonUtils} from '../../../_utils/common.utils';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {CommonHttpService} from '../../../_services/http/common/common.http.service';
import {ImportPoAuthoritiesService} from '../../../_services/authority/import.po.authorities.service';
import {ImportPoModel} from '../../../_models/import-po/import.po.model';
import {PalletModel} from '../../../_models/pallet/pallet.model';
import {ImportPoDetailPalletModel} from '../../../_models/import-po/import.po.detail.pallet.model';
import {ActionTypeEneum, ClaimEnum, ImportPoStatus} from '../../../_models/action-type';
import {importPoStatusAction} from '../../../_models/const/import.po.const';
import {ClaimService} from '../../../_services/http/claim/claim.service';
import {ImportPoDetailModel} from '../../../_models/import-po/import.po.detail.model';
import {DividePackingComponent} from '../../divide-packing/divide-packing.component';
import {ClaimAuthoritiesService} from "../../../_services/authority/claim.authorities.service";

@Component({
  selector: 'app-import-po-v2-edit',
  templateUrl: './import-po-v2-edit.component.html',
  styleUrls: ['./import-po-v2-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ImportPoV2EditComponent extends BaseAddEditLayout implements OnInit {

  moduleName = 'import.po';
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  palletOptions: SelectModel[] = [];
  isView = false;
  importPo: ImportPoModel | undefined;
  hasClaim = false;
  claimError = false;
  claimId: number | undefined;

  constructor(protected activatedRoute: ActivatedRoute,
              protected location: Location,
              protected translateService: TranslateService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              private formBuilder: FormBuilder,
              private commonHttpService: CommonHttpService,
              private dialog: MatDialog,
              private router: Router,
              private apiService: ApiService,
              private claimService: ClaimService,
              public claimAuthoritiesService: ClaimAuthoritiesService,
              public importPoAuthoritiesService: ImportPoAuthoritiesService,
              private dateUtilService: DateUtilService,
              private loaderService: LoaderService
  ) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    activatedRoute.data.subscribe(data => {
      this.isView = !!data.isView;
    });
    this.columns = [
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${CommonUtils.getPosition(e, this.addEditForm.value?.importPoDetails)}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${CommonUtils.getPosition(e, this.addEditForm.value?.importPoDetails)}`,
        isShowHeader: true
      },
      {
        columnDef: 'packingCode',
        header: () => 'common.packing.code',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.productPacking?.code}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.productPacking?.code}`,
        isShowHeader: true
      },
      {
        columnDef: 'productName',
        header: () => 'common.product.name',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking?.product)}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking?.product)}`,
        isShowHeader: true
      },
      {
        columnDef: 'uom',
        header: () => 'common.packing.uom',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.productPacking?.uom}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.productPacking?.uom}`,
        isShowHeader: true
      },
      {
        columnDef: 'block',
        header: () => 'common.packing.block',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.productPacking?.packingType?.quantity}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.productPacking?.packingType?.quantity}`,
        isShowHeader: true
      },
      {
        columnDef: 'expireDate',
        header: 'expireDate',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.expireDate ? dateUtilService.convertDateToDisplayServerTime(e.expireDate) : ''}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.expireDate ? dateUtilService.convertDateToDisplayServerTime(e.expireDate) : ''}`,
        isShowHeader: true
      },
      {
        columnDef: 'orgQuantity',
        header: 'orgQuantity',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.orgQuantity}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.orgQuantity}`,
        display: () => !!this.hasClaim
      },
      {
        columnDef: 'claimQuantity',
        header: 'claimQuantity',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.claimQuantity}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.claimQuantity}`,
        display: () => !!this.hasClaim
      },
      {
        columnDef: 'quantity',
        header: () => this.hasClaim ? 'import.po.table.header.realQuantity' : 'common.quantity',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.quantity ? e.quantity.toLocaleString('en-US') : '0'}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.quantity ? e.quantity.toLocaleString('en-US') : '0'}`,
        isShowHeader: true
      },
      {
        columnDef: 'price',
        header: () => 'common.price',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.productPackingPrice?.price}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.productPackingPrice?.price}`,
        isShowHeader: true
      },
      {
        columnDef: 'total',
        header: () => 'common.total',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${Number(e.quantity) * (100 + Number(e.productPacking?.vat ? e.productPacking?.vat : 0)) *
        Number(e.productPackingPrice?.price) / 100}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${Number(e.quantity) * (100 + Number(e.productPacking?.vat ? e.productPacking?.vat : 0)) *
        Number(e.productPackingPrice?.price) / 100}`,
        isShowHeader: true
      },
      {
        columnDef: 'palletId',
        columnType: () => this.isView ? ColumnTypes.VIEW : ColumnTypes.MULTI_SELECT_AUTOCOMPLETE,
        optionValues: () => this.palletOptions,
        isRequired: () => !this.isView,
        header: () => 'common.pallet',
        title: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.pallet?.displayName}`,
        cell: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => `${e.pallet?.displayName}`,
        display: () => !this.isView || (this.isView && !importPoStatusAction.isNewStatus(this.importPo)),
        disabled: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !!this.isView || this.claimError,
        onCellValueChange: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => {
          console.log(e);
          if (!e.pallet || e.pallet.id !== e.palletId) {
            e.pallet = this.palletOptions.find(p => p.value === e.palletId)?.rawData;
          }
        }
      },

    ];

    this.buttons = [
      {
        columnDef: 'add_box',
        title: 'common.title.add_box',
        icon: 'fa fa-plus-square',
        iconType: IconTypeEnum.FONT_AWESOME,
        header: 'common.action',
        className: 'primary',
        click: 'dividePackingEvent',
        display: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !this.isView &&
          this.importPoAuthoritiesService.hasUpdatePalletImportPoRole(),
        disabled: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !this.isShowTblButton(e, 'add_box')
      },
      {
        columnDef: 'clear',
        title: 'common.title.clear',
        icon: 'fa fa-times',
        iconType: IconTypeEnum.FONT_AWESOME,
        header: 'common.action',
        className: 'danger',
        click: 'removePackingEvent',
        display: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !this.isView &&
          this.importPoAuthoritiesService.hasUpdatePalletImportPoRole(),
        disabled: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !this.isShowTblButton(e, 'clear')
      },
    ];

  }

  async ngOnInit() {
    super.ngOnInit();
    this.addEditForm = this.formBuilder.group({
      manufacturer: [''],
      distributor: [''],
      description: [''],
      importPoDetails: ['']
    });

    // const manufacturerPromise = this.commonHttpService.createGetManufacturerObservable().toPromise();
    // const distributorPromise = this.commonHttpService.createGetAllDistributorObservable().toPromise();
    const importPoPromise = this.apiService.get('/import-pos/' + this.id, new HttpParams()).toPromise();
    // const manufacturerRes = await manufacturerPromise as Page;
    // const distributorRes = await distributorPromise;
    this.importPo = await importPoPromise as ImportPoModel;
    const palletRes = await this.commonHttpService.createGetPalletObs(this.importPo.store?.id, 1, true).toPromise() as Page;
    this.palletOptions = (palletRes.content as PalletModel[]).map(pallet => {
      return new SelectModel(pallet.id, pallet.displayName + '', false, pallet);
    });

    await this.updateData();
    this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, this.importPo));
    this.addEditForm.patchValue({
      manufacturer: this.importPo.manufacturer?.code + ' - '
        + this.importPo.manufacturer?.manufacturerDescriptions.find(des =>
          des.language && des.language.code === this.translateService.currentLang)?.name,
      distributor: this.importPo.distributor?.code + ' - ' + this.importPo.distributor?.name,
    });
  }

  isShowTblButton(e: ImportPoDetailModel | ImportPoDetailPalletModel | any, buttonName: 'add_box' | 'clear') {
    if (this.claimError) {
      return false;
    }
    const importPoDetails = this.addEditForm.value?.importPoDetails ? this.addEditForm.value.importPoDetails : [];
    const ind = importPoDetails.indexOf(e);
    if ((importPoDetails[ind - 1] && importPoDetails[ind - 1].id === e?.id) ||
      (importPoDetails[ind + 1] && importPoDetails[ind + 1].id === e?.id)) {
      if (e.quantity <= 1) {
        return ['clear'].includes(buttonName);
      }
      return true;
    } else {
      if (e.quantity <= 1) {
        return false;
      }
      return ['add_box'].includes(buttonName);
    }
  }

  isViewNewImportPo() {
    return importPoStatusAction.isNewStatus(this.importPo) && this.isView;
  }

  async getClaimDetail(claimMap: Map<string, any>) {
    let key = '';
    let claimDetails;
    if (!this.isViewNewImportPo()) {
      claimDetails = await this.claimService.getClaimByReferenceObs(this.id, ClaimEnum.IMPORT_PO_UPDATE_PALLET).toPromise()
        .catch((error: any) => {
          this.claimError = true;
          this.utilsService.showErrorToarst(error);
        });
    }
    if (claimDetails && claimDetails[0]) {
      this.hasClaim = true;
      this.claimId = claimDetails[0].claim.id;
      claimDetails.forEach((claimDetail: any) => {
        key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
        claimDetail.orgQuantity = claimDetail.quantity;
        claimDetail.claimQuantity = claimDetail.quantity;
        claimDetail.quantity = 0;
        claimMap.set(key, claimDetail);
      });
    }

    if (!importPoStatusAction.isOutOfPalletStatus(this.importPo)) {
      this.importPo?.importPoDetails.forEach((importPoDetail: any) => {
        key = '' + importPoDetail.expireDate + importPoDetail.productPacking.id;
        if (claimMap.has(key) && !this.isViewNewImportPo()) {
          importPoDetail.orgQuantity = Number(importPoDetail.quantity);
          importPoDetail.claimQuantity = Number(claimMap.get(key).claimQuantity);
          importPoDetail.quantity = importPoDetail.orgQuantity - importPoDetail.claimQuantity;
        } else {
          importPoDetail.orgQuantity = importPoDetail.quantity;
          importPoDetail.claimQuantity = 0;
        }
      });
    }
  }

  async updateData() {
    if (!this.importPo) {
      return;
    }
    const claimMap = new Map<string, any>();
    await this.getClaimDetail(claimMap).then();
    let dataTable = [];

    const map: Map<string, any> = new Map();
    let key = '';
    let productPackingId: number | undefined;
    let expireDate: string | undefined = '';
    let claimDetail;
    let claimKey;
    if (importPoStatusAction.isInPalletStatus(this.importPo) || importPoStatusAction.isOutOfPalletStatus(this.importPo)) {
      for (const importPoDetail of this.importPo.importPoDetails) {
        for (const importPoDetailPallet of importPoDetail.importPoDetailPallets) {
          importPoDetailPallet.expireDate = importPoDetail.expireDate;
          importPoDetailPallet.productPackingPrice = importPoDetail.productPackingPrice;
          importPoDetailPallet.productPacking = importPoDetail.productPacking;
          importPoDetailPallet.maxQuantity = importPoDetailPallet.quantity;
          importPoDetailPallet.palletId = importPoDetailPallet.pallet?.id ? importPoDetailPallet.pallet?.id : undefined;

          key = '' + importPoDetailPallet.pallet?.id + importPoDetailPallet.expireDate + importPoDetailPallet.productPacking?.id;
          if (map.has(key)) {
            const detailPallet = map.get(key);
            if (detailPallet) {
              detailPallet.quantity = Number(detailPallet.quantity) + (importPoDetailPallet.quantity ? importPoDetailPallet.quantity : 0);
              detailPallet.maxQuantity = Number(detailPallet.maxQuantity)
                + (importPoDetailPallet.maxQuantity ? importPoDetailPallet.maxQuantity : 0);
            }
            importPoDetailPallet.orgQuantity = (importPoDetailPallet.quantity ? importPoDetailPallet.quantity : 0);
            importPoDetailPallet.claimQuantity = 0;
          } else {
            claimKey = '' + importPoDetailPallet.expireDate + importPoDetailPallet.productPacking?.id;
            if (claimMap.has(claimKey)) {
              claimDetail = claimMap.get('' + importPoDetailPallet.expireDate + importPoDetailPallet.productPacking?.id);
              importPoDetailPallet.orgQuantity = Number(importPoDetailPallet.quantity) + Number(claimDetail.orgQuantity);
              importPoDetailPallet.claimQuantity = Number(claimDetail.orgQuantity);
              claimMap.delete(claimKey);
            } else {
              importPoDetailPallet.orgQuantity = importPoDetailPallet.quantity;
              importPoDetailPallet.claimQuantity = 0;
            }
            map.set(key, importPoDetailPallet);
          }
          ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(importPoDetailPallet.productPacking?.product);
        }
      }
      dataTable.push(...claimMap.values());
      dataTable.push(...map.values());
    } else if (this.importPo.status === ImportPoStatus.NEW) {
      this.importPo.importPoDetails.forEach((detail: ImportPoDetailModel) => {
        productPackingId = detail.productPacking?.id;
        expireDate = detail.expireDate;
        detail.maxQuantity = detail.quantity;
        key = '' + expireDate + productPackingId;
        if (map.has(key)) {
          const importPODetail = map.get(key);
          if (importPODetail) {
            importPODetail.quantity = Number(importPODetail.quantity) + (detail.quantity ? detail.quantity : 0);
            importPODetail.maxQuantity = Number(importPODetail.maxQuantity) + Number(detail.maxQuantity);
          }
        } else {
          map.set(key, detail);
        }
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(detail.productPacking?.product);
      });
      dataTable = [...map.values()];
    }

    this.importPo.importPoDetails = dataTable;
  }

  dividePackingEvent(row: ImportPoDetailModel, index: number) {
    const shallowCopy = {...row};
    this.loaderService.isLoading.next(true);
    this.dialog.open(DividePackingComponent, {
      disableClose: false,
      data: {
        packing: shallowCopy, type: ActionTypeEneum.Pal, store: this.importPo?.store,
        nameCol: {name: 'productPacking.product.name'}
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        this.dividePacking(response.data, {ind: index, row});
      }
    });
  }

  removePackingEvent(row: ImportPoDetailModel, index: number) {
    this.removeDividedPacking({ind: index, row});
  }

  dividePacking(dividedPacking: ImportPoDetailModel, originPacking: { ind: number, row: ImportPoDetailModel }) {
    const tblData = this.addEditForm.value?.importPoDetails;
    const length = tblData.length;
    const dataTable = [];
    dividedPacking.palletId = dividedPacking.pallet?.id ? dividedPacking.pallet?.id : undefined;
    for (let i = 0; i < length; i++) {
      dataTable.push(tblData[i]);
      if (i === originPacking.ind) {
        tblData[i].quantity = Number(tblData[i].quantity) - Number(dividedPacking.quantity);
        dividedPacking.maxQuantity = tblData[i].maxQuantity - tblData[i].quantity;
        dividedPacking.orgQuantity = dividedPacking.quantity;
        dividedPacking.claimQuantity = 0;
        tblData[i].maxQuantity = tblData[i].quantity;
        tblData[i].orgQuantity = Number(tblData[i].orgQuantity) - Number(dividedPacking.quantity);
        dataTable.push(dividedPacking);
      }
    }
    console.log(dataTable);
    this.addEditForm.patchValue({
      importPoDetails: dataTable
    });
  }

  removeDividedPacking(removeObj: { ind: number, row: ImportPoDetailModel }) {
    const tblData = this.addEditForm.value?.importPoDetails ? this.addEditForm.value?.importPoDetails : [];
    const beforeObj = tblData[removeObj.ind - 1];
    const afterObj = tblData[removeObj.ind + 1];
    if ((removeObj.ind === 0 && afterObj.id !== removeObj.row.id) ||
      (removeObj.ind === tblData.length - 1 && beforeObj.id !== removeObj.row.id) ||
      (beforeObj && beforeObj.id !== removeObj.row.id && afterObj.id !== removeObj.row.id)) {
      this.utilsService.showWarningToarst('common.packing.remove.not.divide');
      return;
    }
    if (beforeObj && beforeObj.id === removeObj.row.id) {
      beforeObj.quantity = Number(removeObj.row.quantity) + Number(beforeObj.quantity);
      beforeObj.maxQuantity = Number(removeObj.row.maxQuantity) + Number(beforeObj.maxQuantity);
      beforeObj.orgQuantity = Number(beforeObj.orgQuantity) + Number(removeObj.row.orgQuantity);
      beforeObj.claimQuantity = Number(beforeObj.claimQuantity) + Number(removeObj.row.claimQuantity);
      beforeObj.total = Number(beforeObj.quantity) * (100 + Number(beforeObj.productPacking.vat)) *
        Number(beforeObj.productPackingPrice.price) / 100;
    } else if (afterObj.id === removeObj.row.id) {
      afterObj.quantity = Number(removeObj.row.quantity) + Number(afterObj.quantity);
      afterObj.maxQuantity = Number(removeObj.row.maxQuantity) + Number(afterObj.maxQuantity);
      afterObj.orgQuantity = Number(afterObj.orgQuantity) + Number(removeObj.row.orgQuantity);
      afterObj.claimQuantity = Number(afterObj.claimQuantity) + Number(removeObj.row.claimQuantity);
      afterObj.total = Number(afterObj.quantity) * (100 + Number(afterObj.productPacking.vat)) *
        Number(afterObj.productPackingPrice.price) / 100;
    }

    tblData.splice(removeObj.ind, 1);
    this.addEditForm.patchValue({
      importPoDetails: tblData
    });
  }

  onUpdateImportPOPallet() {
    console.log(this.addEditForm.value);
    const {importPoDetails} = this.addEditForm.value;
    let count = 0;
    const map: Map<string, ImportPoDetailPalletModel> = new Map<string, ImportPoDetailPalletModel>();
    let key = '';
    let item: ImportPoDetailModel | ImportPoDetailPalletModel | any;
    for (item of (importPoDetails ? importPoDetails : [])) {
      count++;
      if (!item.palletId) {
        const msg = this.translateService.instant('notice.update.pallet') + count;
        this.utilsService.showWarningToarst(msg);
        return;
      }
      key = '' + item.productPacking?.id + item.expireDate + item.palletId;
      if (map.has(key)) {
        const detailPallet = map.get(key);
        if (detailPallet) {
          detailPallet.quantity = Number(detailPallet.quantity) + Number(item.quantity);
        }
      } else {
        map.set(key, new ImportPoDetailPalletModel({
          id: item.palletDetail ? item.id : undefined,
          importPoId: item.importPo?.id,
          importPoDetailId: item.importPoDetail?.id,
          pallet: item.palletId,
          quantity: item.quantity,
          palletDetailId: item.palletDetail?.id
        }));
      }
    }
    const method = this.apiService.post('/import-pos/update-pallet/' + this.id, [...map.values()]);
    this.utilsService.execute(method, this.onSuccessFunc, '.Pal.success', 'import.po.confirm.update.pallet', []);
  }

  onBack() {
    this.location.back();
  }

  onClaimDetail() {
    if (this.claimId) {
      this.router.navigate(['/claim/dashboard/' + this.claimId]);
    } else {
      this.utilsService.showErrorToarst('claim.wait.approve.please');
    }
  }

  updateLabel(event: any) {
    switch (event.label) {
      case 'import.po.label.edit':
      case 'import.po.label.view':
        event.label = this.importPo?.code;
        break;
      case 'claim.label.action':
        event.label = 'common.label.view';
        break;
    }
  }
}
