import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout, ButtonFields,
  ColumnFields,
  DateUtilService,
  FormStateService, IconTypeEnum, Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {NavService} from '../../_services/nav.service';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {map} from 'rxjs/internal/operators';
import {HttpParams} from '@angular/common/http';
import {ImportStatementModel} from "../../_models/import-statement/import.statement.model";
import {PoModel} from "../../_models/po/po.model";
import {ImportPoAuthoritiesService} from "../../_services/authority/import.po.authorities.service";

@Component({
  selector: 'app-import-po-v2',
  templateUrl: './import-po-v2.component.html',
  styleUrls: ['./import-po-v2.component.css']
})
export class ImportPoV2Component extends BaseSearchLayout implements OnInit {

  statusSelect: SelectModel[] = [
    {value: 0, displayValue: 'import-po.status.new', disabled: false, rawData: []},
    {value: 1, displayValue: 'import-po.status.in_pallet', disabled: false, rawData: []},
    {value: 2, displayValue: 'import-po.status.out_of_pallet', disabled: false, rawData: []}
  ];
  storeIdSelect: SelectModel[] = [];
  isPending = true;

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  moduleName = 'import.po';

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              private tsUtilsService: TsUtilsService,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              public importPoAuthoritiesService: ImportPoAuthoritiesService,
              private dateUtilService: DateUtilService,
              private navService: NavService) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        status: [''],
        storeId: ['']
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'manufacturerCode',
        header: 'manufacturerCode',
        title: (e: any) => `${e.manufacturer.code ? e.manufacturer.code : ''}`,
        cell: (e: any) => `${e.manufacturer.code ? e.manufacturer.code : ''}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-manufacturerCode'
      },
      {
        columnDef: 'manufacturerName',
        header: 'manufacturerName',
        title: (e: any) => `${e.manufacturer.manufacturerDescriptions[0].name}`,
        cell: (e: any) => `${e.manufacturer.manufacturerDescriptions[0].name}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-manufacturerName'
      },
      {
        columnDef: 'poCode',
        header: 'poCode',
        title: (e: any) => `${e.code}`,
        cell: (e: any) => `${e.code}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-poCode'
      },
      {
        columnDef: 'description',
        header: 'description',
        title: (e: any) => `${e.description}`,
        cell: (e: any) => `${e.description}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-description'
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: any) => `${this.translateService.instant(this.statusSelect.find((a: any) => a.value === e.status)?.displayValue + '')}`,
        cell: (e: any) => `${this.translateService.instant(this.statusSelect.find((a: any) => a.value === e.status)?.displayValue + '')}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-status'
      },
      {
        columnDef: 'approveDate',
        header: 'approveDate',
        title: (e: any) => `${this.dateUtilService.convertDateToDisplayGMT0(e.po.approveDate)}`,
        cell: (e: any) => `${this.dateUtilService.convertDateToDisplayGMT0(e.po.approveDate)}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-approveDate'
      },
      {
        columnDef: 'approveUser',
        header: 'approveUser',
        title: (e: any) => `${e.po.approveUserObj.firstName + ' ' + e.po.approveUserObj.lastName}`,
        cell: (e: any) => `${e.po.approveUserObj.firstName + ' ' + e.po.approveUserObj.lastName}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-approveUser'
      },
    );
    this.buttons.push(
      {
        columnDef: 'edit',
        icon: 'fa fa-pen',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'primary',
        click: 'addOrEdit',
        title: 'common.title.edit',
        display: (e: any) => !!e && importPoAuthoritiesService.hasUpdatePalletImportPoRole(),
        header: 'common.action',
      },
      {
        columnDef: 'view',
        title: 'common.title.dashboard',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'onView',
        className: 'info',
        display: () => importPoAuthoritiesService.hasGetImportPoRole(),
      }
    );
  }

  async ngOnInit() {
    const next = await this.tsUtilsService.getFCStores().toPromise() as Page;
    this.storeIdSelect = [];
    next.content.forEach((m: any) => {
      this.storeIdSelect.push(new SelectModel(m.id, m.code));
    });
    this.isPending = false;
    if (this.storeIdSelect.length) {
      this.searchForm.patchValue({
        storeId: this.storeIdSelect[0].value
      });
    }
    if (this.statusSelect.length) {
      this.searchForm.patchValue({
        status: this.statusSelect[0].value
      });
    }
    super.ngOnInit();
    super.onSubmit();
  }

  search(): void {
    super.search();
    const params = new HttpParams()
      .set('storeId', `${this.searchForm.value.storeId}`)
      // .set('distributorId', this.formData.distributor ? this.formData.distributor.id : '')
      .set('text', `${this.searchForm.value.text}`)
      .set('status', `${this.searchForm.value.status}`);
    this._fillData('/import-pos', params);

  }

  onView(model: PoModel, index: number) {
    this.router.navigate(['import-po', 'view', model.id]);
  }

  addOrEdit(e: any) {
    this.router.navigate(['edit', e.id], {relativeTo: this.activatedRoute});
  }
}
