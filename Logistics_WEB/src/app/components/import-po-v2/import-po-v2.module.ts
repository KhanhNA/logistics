import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ImportPoV2Component} from './import-po-v2.component';
import {AuthoritiesResolverService, MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {SharedModule} from '../../modules/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {ImportPoV2EditComponent} from './import-po-v2-edit/import-po-v2-edit.component';
import {ImportPoAuthoritiesService} from "../../_services/authority/import.po.authorities.service";
import {ClaimService} from "../../_services/http/claim/claim.service";
import {ClaimAuthoritiesService} from "../../_services/authority/claim.authorities.service";
import {ClaimSharedComponent} from "../../modules/claim.shared.component";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/', suffix: '.json'},
    {prefix: './assets/i18n/import-po/', suffix: '.json'},
  ]);
}

const routes: Routes = [
  {
    path: '',
    component: ImportPoV2Component,
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'edit/:id',
    component: ImportPoV2EditComponent,
    data: {breadcrumb: 'import.po.label.edit'},
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'view/:id',
    component: ImportPoV2EditComponent,
    data: {breadcrumb: 'import.po.label.view', isView: true},
    resolve: {me: AuthoritiesResolverService}
  }
];

@NgModule({
  declarations: [ImportPoV2Component],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      isolate: true,
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ImportPoAuthoritiesService,
    ClaimAuthoritiesService,
    ClaimService
  ]
})
export class ImportPoV2Module {
}
