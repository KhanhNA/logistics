import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  DateUtilService,
  FormStateService,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {ExchangeRateModel} from '../../_models/exchangeRateModel';
import {DateUtils} from '../../base/Utils/date.utils';
import {CurrencyModel} from '../../_models/currency.model';
import {ActionTypeEneum} from '../../_models/action-type';

@Component({
  selector: 'app-exchange-rate',
  templateUrl: './exchange-rate.component.html',
  styleUrls: []
})
export class ExchangeRateComponent extends BaseSearchLayout implements OnInit {
  static readonly OPTION_ALL = '_';
  moduleName = 'exchangeRate';
  buttons: ButtonFields[] = [];
  fromCurrencyOptions: SelectModel[] = [];
  toCurrencyOptions: SelectModel[] = [];
  currencyArr: CurrencyModel[] = [];

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private dateUtilService: DateUtilService) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        fromCurrency: [ExchangeRateComponent.OPTION_ALL],
        toCurrency: [ExchangeRateComponent.OPTION_ALL],
        fromDate: [dateUtilService.convertDateToStringGMT0(DateUtils.getCurrentDate())],
        toDate: ['']
      }));
    this.columns.push(...[{
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
        {
          columnDef: 'exchangeRate.from.currency',
          header: 'exchangeRate.from.currency',
          title: (e: ExchangeRateModel) => `${e.fromCurrency?.name}`,
          cell: (e: ExchangeRateModel) => `${e.fromCurrency?.name}`,
          align: AlignEnum.CENTER
        },
        {
          columnDef: 'exchangeRate.to.currency',
          header: 'exchangeRate.to.currency',
          title: (e: ExchangeRateModel) => `${e.toCurrency?.name}`,
          cell: (e: ExchangeRateModel) => `${e.toCurrency?.name}`,
          align: AlignEnum.CENTER
        },
        {
          columnDef: 'exchangeRate',
          header: 'exchangeRate',
          title: (e: ExchangeRateModel) => `${e.exchangeRate}`,
          cell: (e: ExchangeRateModel) => `${e.exchangeRate}`,
          align: AlignEnum.CENTER
        },
        {
          columnDef: 'fromDate',
          header: 'fromDate',
          title: (e: ExchangeRateModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.fromDate ? e.fromDate : '')}`,
          cell: (e: ExchangeRateModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.fromDate ? e.fromDate : '')}`,
          align: AlignEnum.CENTER
        }]
    );

    this.buttons.push(...[
      {
        columnDef: 'edit',
        color: 'warn',
        icon: 'edit',
        click: 'addOrEdit',
        title: 'common.title.edit',
        isShowHeader: true,
        display: (e: ExchangeRateModel) => e && this.authoritiesService.hasAuthority('patch/currency-exchange-rate/{id}'),
        disabled: (e: any) => !e,
        header: {
          columnDef: 'add',
          color: 'warn',
          icon: 'add',
          click: 'addOrEdit',
          title: 'common.title.add',
          display: (e: ExchangeRateModel) => !e && this.authoritiesService.hasAuthority('post/currency-exchange-rate'),
          disabled: (e: any) => e
        },
      }]);
  }

  async ngOnInit() {
    this.currencyArr = await this.apiService.get('/currencies/all', new HttpParams()).toPromise() as CurrencyModel[];
    this.fromCurrencyOptions = this.currencyArr.map(currency => new SelectModel(currency.id, currency.name + ''));
    this.toCurrencyOptions = this.currencyArr.map(currency => new SelectModel(currency.id, currency.name + ''));

    this.fromCurrencyOptions.splice(0, 0,
      new SelectModel(ExchangeRateComponent.OPTION_ALL, this.translateService.instant('common.status.all')));
    this.toCurrencyOptions.splice(0, 0,
      new SelectModel(ExchangeRateComponent.OPTION_ALL, this.translateService.instant('common.status.all')));
    super.onSubmit();
  }

  search() {
    const params = new HttpParams()
      .set('fromCurrencyId', this.searchForm.get('fromCurrency')?.value !== ExchangeRateComponent.OPTION_ALL
        ? this.searchForm.get('fromCurrency')?.value.toString() : '')
      .set('toCurrencyId', this.searchForm.get('toCurrency')?.value !== ExchangeRateComponent.OPTION_ALL
        ? this.searchForm.get('toCurrency')?.value.toString() : '')
      .set('fromDate', this.searchForm.get('fromDate')?.value)
      .set('toDate', this.searchForm.get('toDate')?.value);
    this._fillData('/currency-exchange-rate', params);
  }

  changeFromCurrency(value: any) {
    this.toCurrencyOptions = this.currencyArr.filter(currency => currency.id !== value)
      .map(currency => new SelectModel(currency.id, currency.name + ''));
    this.toCurrencyOptions.splice(0, 0,
      new SelectModel(ExchangeRateComponent.OPTION_ALL, this.translateService.instant('common.status.all')));
    if (!this.toCurrencyOptions.map(option => option.value).includes(this.searchForm.get('toCurrency')?.value)) {
      this.searchForm.get('toCurrency')?.setValue(ExchangeRateComponent.OPTION_ALL);
    }
  }

  addOrEdit(row: ExchangeRateModel) {
    const url = '/exchange-rate';
    if (row) {
      this.router.navigate([url, ActionTypeEneum.edit, row.id]).then();
    } else {
      this.router.navigate([url, ActionTypeEneum.new]).then();
    }

  }
}
