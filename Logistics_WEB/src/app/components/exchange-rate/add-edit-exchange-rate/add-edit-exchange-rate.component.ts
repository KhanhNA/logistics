import {Component, OnInit} from '@angular/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {FormBuilder, Validators} from '@angular/forms';
import {CurrencyModel} from '../../../_models/currency.model';
import {ExchangeRateModel} from '../../../_models/exchangeRateModel';
import {ActionTypeEneum} from '../../../_models/action-type';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-add-edit-exchange-rate',
  templateUrl: './add-edit-exchange-rate.component.html',
  styleUrls: ['./add-edit-exchange-rate.component.scss']
})
export class AddEditExchangeRateComponent extends BaseAddEditLayout implements OnInit {
  parentId: number;
  actionType: string | null;
  currencyArr: CurrencyModel[] = [];
  fromCurrencyOptions: SelectModel[] = [];
  toCurrencyOptions: SelectModel[] = [];
  exchangeRate: ExchangeRateModel | undefined;
  errorMessage = new Map().set('pattern', () => this.translateService.instant('pattern.required.number'));
  exchangeRatePattern = '^[+]?([1-9][0-9]*(?:[\\.][0-9]*)?|0*\\.0*[1-9][0-9]*)(?:[eE][+-][0-9]+)?$';
  minFromDate = () => this.exchangeRate?.fromDate && new Date(this.exchangeRate.fromDate) < new Date() ?
    new Date(this.exchangeRate.fromDate) : new Date();
  minToDate = () => this.exchangeRate?.toDate && new Date(this.exchangeRate.toDate) < this.minFromDate() ?
    new Date(this.exchangeRate.toDate) : this.minFromDate();

  constructor(protected activatedRoute: ActivatedRoute,
              protected location: Location,
              protected translateService: TranslateService,
              protected authoritiesService: AuthoritiesService,
              private fb: FormBuilder,
              private apiService: ApiService,
              protected utilsService: UtilsService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.parentId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.actionType = this.activatedRoute.snapshot.paramMap.get('actType');

  }

  async ngOnInit() {
    super.ngOnInit();

    this.addEditForm = this.fb.group({
      id: [''],
      fromCurrency: ['', [Validators.required]],
      toCurrency: ['', [Validators.required]],
      exchangeRate: ['', Validators.compose([Validators.required, Validators.pattern(this.exchangeRatePattern)])],
      fromDate: ['', [Validators.required]],
      toDate: ['']
    });

    this.currencyArr = await this.apiService.get('/currencies/all', new HttpParams()).toPromise() as CurrencyModel[];
    this.fromCurrencyOptions = this.currencyArr.map(currency => new SelectModel(currency.id, currency.name + ''));
    this.toCurrencyOptions = this.currencyArr.map(currency => new SelectModel(currency.id, currency.name + ''));

    if (this.isEdit) {
      this.exchangeRate = await this.apiService.get('/currency-exchange-rate/' + this.parentId, new HttpParams())
        .toPromise() as ExchangeRateModel;

      this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, this.exchangeRate));
      this.addEditForm.markAllAsTouched();
    }
  }

  changeFromCurrency(value: any) {
    this.toCurrencyOptions = this.currencyArr.filter(currency => currency.id !== value)
      .map(currency => new SelectModel(currency.id, currency.name + ''));
  }

  onSubmit() {
    console.log(this.addEditForm);
    console.log(new ExchangeRateModel(this.addEditForm));
    if (this.actionType === ActionTypeEneum.new) {
      const method = this.apiService.post('/currency-exchange-rate', [new ExchangeRateModel(this.addEditForm)]);
      this.utilsService.execute(method, this.onSuccessFunc, '.add.success', 'common.confirmSave',
        ['common.exchange.rate.param']);
    } else if (this.actionType === ActionTypeEneum.edit) {
      const method = this.apiService.patch('/currency-exchange-rate/' + this.parentId, new ExchangeRateModel(this.addEditForm));
      this.utilsService.execute(method, this.onSuccessFunc, '.edit.success', 'common.confirmSave',
        ['common.exchange.rate.param'])
    }
  }
}
