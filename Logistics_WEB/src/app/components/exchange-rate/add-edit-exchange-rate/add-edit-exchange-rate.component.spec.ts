import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditExchangeRateComponent } from './add-edit-exchange-rate.component';

describe('AddEditExchangeRateComponent', () => {
  let component: AddEditExchangeRateComponent;
  let fixture: ComponentFixture<AddEditExchangeRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditExchangeRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditExchangeRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
