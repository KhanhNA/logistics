import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Pov2Component} from './pov2.component';
import {HttpClient} from '@angular/common/http';
import {MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {SharedModule} from '../../modules/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {Pov2RoutingModule} from './pov2-routing.module';
import {Pov2Service} from './pov2.service';
import {PoAuthoritiesService} from '../../_services/authority/po.authorities.service';


export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/', suffix: '.json'},
    {prefix: './assets/i18n/po/', suffix: '.json'},

  ]);
}

@NgModule({
  declarations: [Pov2Component],
  imports: [
    SharedModule,
    CommonModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      isolate: true,
    }),
    Pov2RoutingModule,
  ],
  providers: [Pov2Service, PoAuthoritiesService]
})
export class Pov2Module {
}
