import {Injectable} from '@angular/core';
import {ApiService, DateUtilService, Page, SelectModel} from '@next-solutions/next-solutions-base';
import {HttpParams} from '@angular/common/http';
import {DistributorModel} from '../../_models/distributor.model';
import {ManufacturerModel} from "../../_models/manufacturer.model";
import {ManufacturerDescriptionModel} from "../../_models/manufacturer.description.model";
import {CurrencyModel} from "../../_models/currency.model";
import {DateUtils} from "../../base/Utils/date.utils";
import {BehaviorSubject, Observable} from "rxjs";
import {ProductPackingModel} from "../../_models/product.packing.model";
import {CommonHttpService} from "../../_services/http/common/common.http.service";
import {CommonUtils} from "../../_utils/common.utils";


@Injectable()
export class Pov2Service {

  isNotPrice = new BehaviorSubject(false);

  productPackingMap: Map<string, ProductPackingModel> = new Map<string, ProductPackingModel>();

  distributorOptions?: SelectModel[] = undefined;
  storeOptions?: SelectModel[] = undefined;
  manufacturerOptions?: SelectModel[] = undefined;
  currencyOptions?: SelectModel[] = undefined;

  constructor(private apiService: ApiService, private dateUtilService: DateUtilService,
              private commonHttpService: CommonHttpService) {
  }

  createGetManufacturerObservable(): Observable<any> {
    return this.commonHttpService.createGetManufacturerObservable();
  }

  getManufacture(currentLangCode: string, successFunc: any) {
    if (this.manufacturerOptions) {
      successFunc(this.manufacturerOptions);
      return;
    }
    this.commonHttpService.createGetManufacturerObservable().subscribe((next: any) => {
      let description;
      this.manufacturerOptions = next.content.map((manufacturer: ManufacturerModel) => {
        description = manufacturer.manufacturerDescriptions?.filter(manufacturerDescriptionModel =>
          manufacturerDescriptionModel.language?.code === currentLangCode)[0] as ManufacturerDescriptionModel;
        return new SelectModel(manufacturer.id, manufacturer.code + ' - ' + description.name);
      });
      successFunc(this.manufacturerOptions);
    });
  }

  createGetAllDistributorObservable(): Observable<any> {
    return this.commonHttpService.createGetAllDistributorObservable();
  }

  getAllDistributor(successFunc: any, isAwaitPromise?: boolean): Promise<any> | any {
    if (this.distributorOptions) {
      console.log('get from cache');
      successFunc(this.distributorOptions);

      return;
    }
    if (isAwaitPromise) {
      return this.commonHttpService.createGetAllDistributorObservable().toPromise();
    }
    return this.commonHttpService.createGetAllDistributorObservable().subscribe(next => {
      // successFunc(next);
      const distributor = next as DistributorModel[];
      this.distributorOptions = distributor?.map(v => {
        return new SelectModel(v.id, '' + v.name, false, v);
      });
      successFunc(this.distributorOptions);
    });
  }

  createGetAllCurrenciesObservable(): Observable<any> {
    return this.apiService.get('/currencies/all', new HttpParams());
  }

  getAllCurrencies(successFunc: any) {
    if (this.currencyOptions) {
      console.log('get from cache');
      successFunc(this.currencyOptions);

      return;
    }
    return this.createGetAllCurrenciesObservable().subscribe(next => {
      // successFunc(next);
      const currencies = next as CurrencyModel[];
      this.currencyOptions = currencies?.map(v => {
        return new SelectModel(v.id, '' + v.name, false, v);
      });
      successFunc(this.currencyOptions);
    });
  }

  createGetStoreObservable(): Observable<any> {
    return this.commonHttpService.createGetFCStoreAvailableObservable();
  }

  getStore(successFunc: any, isAwaitPromise?: boolean): Promise<any> | any {
    if (this.storeOptions) {
      successFunc(this.storeOptions);
      return;
    }
    // stores?status=true&text=&exceptIds=-1&ignoreCheckPermission=false&is_DC=false&pageNumber=1&pageSize=9999
    if (!!isAwaitPromise) {
      return this.commonHttpService.createGetFCStoreAvailableObservable().toPromise();
    }
    this.commonHttpService.createGetFCStoreAvailableObservable().subscribe((next: any) => {
      this.storeOptions = next.content?.map((v: any) => {
        return new SelectModel(v.id, '' + v.code, false, v);
      });
      successFunc(this.storeOptions);
    });
  }

  searchPo(query: any) {
    // http://demo.aggregatoricapaci.com:8888/pos?storeId=1&distributorId=&text=&fromDate=2020-11-29%2007:00:00.000Z&toDate=2020-12-29%2007:00:00.000Z&status=NEW&dateType=NEW&pageNumber=1&pageSize=10&orderBy=

  }

  createGetCurrencyExchangeRateObservable(fromCurrency: any, toCurrency: any): Observable<any> {
    const params = new HttpParams()
      .set('fromCurrencyId', fromCurrency)
      .set('toCurrencyId', toCurrency)
      .set('fromDate', '' + this.dateUtilService.convertDateToStringGMT0(DateUtils.getCurrentDate()))
      .set('toDate', '' + this.dateUtilService.convertDateToStringGMT0(DateUtils.getDayBeforeNow(-1)))
      .set('pageSize', CommonUtils.MAX_PAGE_SIZE + '')
      .set('pageNumber', '1');
    return this.apiService.get('/currency-exchange-rate', params)
  }

  getCurrencyExchangeRate(fromCurrency: any, toCurrency: any, successFunc: any) {
    if (fromCurrency === toCurrency) {
      successFunc(1);
    } else {
      this.createGetCurrencyExchangeRateObservable(fromCurrency, toCurrency).subscribe((next: any) => {
        successFunc(next.content[0].exchangeRate);
      });
      // if (page.content.length > 0) {
      //   this.exchangeRate = page.content[0];
      // } else {
      //   this.exchangeRate = undefined;
      //   this.showErrorMsg('common.exchange.rate.not.found');
      // }
      // this.addEditForm.get('exchangeRate')?.setValue(this.exchangeRate ? this.exchangeRate.exchangeRate : 0);
    }
  }

  patchArrDate(uri: string, id: number, body: any) {
    return this.apiService.patch('/pos/' + id + uri, body);
  }


}
