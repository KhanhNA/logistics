import {Component, Injector, OnInit} from '@angular/core';
import {LogisticBaseSearchLayout} from '../../base/field/logistic-base-search-layout';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  DateRangePickerModel,
  DateUtilService,
  FormStateService,
  IconTypeEnum,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {PhoneInputFormat} from '../../phone-input-format';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {PoModel} from '../../_models/po/po.model';
import {HttpParams} from '@angular/common/http';
import {Pov2Service} from './pov2.service';
import {ModelUtils} from '../../_models/ModelUtils';
import {poConst, poStatusAction} from '../../_models/po/po.const';
import {ActionTypeEneum, PoStatus} from '../../_models/action-type';
import {PoAuthoritiesService} from "../../_services/authority/po.authorities.service";
import {forkJoin, Observable} from "rxjs";
import {DistributorModel} from "../../_models/distributor.model";
import {environment} from "../../../environments/environment";
import {ManufacturerModel} from "../../_models/manufacturer.model";
import {StoreModel} from "../../_models/store.model";

@Component({
  selector: 'app-pov2',
  templateUrl: './pov2.component.html',
  styleUrls: ['./pov2.component.css'],

})

export class Pov2Component extends LogisticBaseSearchLayout implements OnInit {
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  moduleName = 'pov2';
  statusOptions: SelectModel[] = [];
  distributorOptions: SelectModel[] = [];
  manufacturerOptions: SelectModel[] = [];
  storeOptions: SelectModel[] = [];
  expandButton = environment.EXPAND_HEADER_BUTTON;

  phoneNumberControl: any;
  public formatFun: PhoneInputFormat = new PhoneInputFormat();

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected poService: Pov2Service,
              protected dateUtilService: DateUtilService,
              protected authoritiesService: AuthoritiesService,
              public poAuthoritiesService: PoAuthoritiesService
  ) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        storeId: [''],
        distributorId: [''],
        manufacturerId: [''],
        status: [''],
        searchDate: [''],
        dateType: [''],
        fromDate: [''],
        toDate: [''],
      }));

    if (this.searchForm.get('phoneNumber') !== null) {
      this.phoneNumberControl = this.searchForm.get('phoneNumber');
    }
    this.columns.push(...[
      {
        columnDef: 'checked',
        header: 'checked',
        title: (e: PoModel) => `true`,
        cell: (e: PoModel) => `true`,
        display: (_: PoModel) => true,
        columnType: ColumnTypes.CHECKBOX,
        align: AlignEnum.CENTER,
      },
      {
        columnDef: 'stt', header: 'number',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
      },
      {
        columnDef: 'po.ae.code', header: 'po.ae.code',
        title: (po: PoModel) => `${po.code ? po.code : ''}`,
        cell: (po: PoModel) => `${po.code ? po.code : ''}`,
        className: 'mat-column-code',
      },
      {
        columnDef: 'po.ae.manufacturer.code', header: 'po.ae.manufacturer.code',
        title: (po: PoModel) => `${po.manufacturer?.code + ''}`,
        cell: (po: PoModel) => `${po.manufacturer?.code + ''}`,
        className: 'mat-column-manufacturer',
      },
      {
        columnDef: 'po.ae.description', header: 'po.ae.description',
        title: (po: PoModel) => `${po.description + ''}`,
        cell: (po: PoModel) => `${po.description + ''}`,
        className: 'mat-column-description',
      },
      {
        columnDef: 'common.total', header: 'common.total',
        title: (po: PoModel) => `${Number(po.total).toLocaleString('en-US') + ''}`,
        cell: (po: PoModel) => `${Number(po.total).toLocaleString('en-US') + ''}`,
        className: 'mat-column-total',
      },
      {
        columnDef: 'common.status', header: 'common.status',
        title: (po: PoModel) => `${this.getPoStatus(po)}`,
        cell: (po: PoModel) => `${this.getPoStatus(po)}`,
        className: (po: PoModel) => `${this.getPoStatusStyle(po)}`
      },
      {
        columnDef: 'common.createDate', header: 'common.createDate',
        title: (po: PoModel) => `${!!po ? (po.createDateString + ' - ' +
          po.createUserObj?.firstName + ' ' + po.createUserObj?.lastName) : ''}`,
        cell: (po: PoModel) => `${!!po ? (po.createDateString + ' - ' +
          po.createUserObj?.firstName + ' ' + po.createUserObj?.lastName) : ''}`,
        className: 'mat-column-createDate',
      },
      {
        columnDef: 'common.approveDate', header: 'common.approveDate',
        title: (po: PoModel) => `${this.getApproveInfo(po)}`,
        cell: (po: PoModel) => `${this.getApproveInfo(po)}`,
        className: 'mat-column-approveDate',

        display: (po: PoModel) => true,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'common.arrivedVietnamPortDate', header: 'common.arrivedVietnamPortDate',
        title: (po: PoModel) => `${this.getArrivedVietnamPortDate(po)}`,
        cell: (po: PoModel) => `${this.getArrivedVietnamPortDate(po)}`,
        className: 'mat-column-arrivedVietnamPortDate',
        display: (po: PoModel) => true,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'common.arrivedMyanmarPortDate', header: 'common.arrivedMyanmarPortDate',
        title: (po: PoModel) => `${this.getArrivedMyanmarPortDate(po)}`,
        cell: (po: PoModel) => `${this.getArrivedMyanmarPortDate(po)}`,
        className: 'mat-column-arrivedMyanmarPortDate',
        display: (po: PoModel) => true,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'common.arrivedFcDate', header: 'common.arrivedFcDate',
        title: (po: PoModel) => `${this.getArrivedFcDate(po)}`,
        cell: (po: PoModel) => `${this.getArrivedFcDate(po)}`,
        className: 'mat-column-arrivedFcDate',
        display: (po: PoModel) => true,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'common.importDate', header: 'common.importDate',
        title: (po: PoModel) => `${this.getImportDate(po)}`,
        cell: (po: PoModel) => `${this.getImportDate(po)}`,
        className: 'mat-column-importDate',
        display: (po: PoModel) => true,
        isExpandOptionColumn: () => true
      },
    ]);
    this.buttons.push(...[
      {
        columnDef: 'edit',
        color: 'warn',
        icon: 'fa fa-pen',
        title: 'common.title.edit',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'onEditPO',
        isShowHeader: true,
        header: 'common.action',
        className: 'secondary',
        display: (po: PoModel) => poStatusAction.isNewStatus(po) && this.poAuthoritiesService.hasUpdatePoRole()

        // display: (po: PoModel) => e && this.distributorAuthoritiesUtils.hasGetPackageRole(),
        // disabled: (po: PoModel) => !e || DistributorAuthoritiesUtils.isAcceptedStatus(e)
      },
      {
        columnDef: 'GoodsReceive',
        color: 'approve',
        icon: 'fa fa-warehouse',
        title: 'common.title.gr',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'receiveGoods',
        className: 'info',
        isShowHeader: true,
        display: (po: PoModel) => this.poAuthoritiesService.hasGoodReceivePoRole() && poStatusAction.isArrivedMyanmarPortStatus(po),
        // disabled: (po: PoModel) => !(e.status !== null && e.status !== undefined && (e.status.toString() === '0'))?true: false
      },
      {
        columnDef: 'deliveryDate',
        color: 'approve',
        icon: 'fa fa-calendar-alt',
        title: 'common.title.deliveryDate',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'deliveryDate',
        className: 'info',
        isShowHeader: true,
        display: (po: PoModel) => this.poAuthoritiesService.hasUpdateDeleveryDate() &&
          (poStatusAction.isApprovedStatus(po) || poStatusAction.isArrivedMyanmarPortStatus(po) || poStatusAction.isArrivedVietnamPortStatus(po)),
        // disabled: (po: PoModel) => !(e.status !== null && e.status !== undefined && (e.status.toString() === '0'))?true: false
      },


      {
        columnDef: 'printPDF',
        title: 'common.title.picture_as_pdf',
        icon: 'fa fa-download',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'primary',
        click: 'onPrintPDF',
        isShowHeader: true,
        display: () => this.poAuthoritiesService.hasPrintPDFPoRole(),
        // disabled: (po: PoModel) => !(e.status !== null && e.status !== undefined && (e.status.toString() === '0'))?true: false
      },
      {
        columnDef: 'view',
        title: 'common.title.dashboard',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'onView',
        className: 'info',
        display: () => true,
      }
    ]);


    this.searchForm.get('status')?.setValue(this.modelUtils.getSelectAllValue());
  }

  getImportDate(po: PoModel) {
    if (!po || !po.importUserObj || !po.importDate) {
      return '';
    }
    return this.dateUtilService.convertDateToDisplayGMT0(po.importDate) + ' - ' +
      po.importUserObj.firstName + ' ' + po.importUserObj.lastName;
  }

  getArrivedFcDate(po: PoModel) {
    if (!po || !po.arrivedFcUserObj) {
      return '';
    }
    return po.arrivedFcDateString + ' - ' +
      po.arrivedFcUserObj.firstName + ' ' + po.arrivedFcUserObj.lastName;
  }

  getArrivedMyanmarPortDate(po: PoModel) {
    if (!po || !po.arrivedMyanmarPortUserObj) {
      return '';
    }
    return po.arrivedMyanmarPortDateString + ' - ' +
      po.arrivedMyanmarPortUserObj.firstName + ' ' + po.arrivedMyanmarPortUserObj.lastName;
  }

  getArrivedVietnamPortDate(po: PoModel) {
    if (!po || !po.arrivedVietnamPortUserObj) {
      return '';
    }
    return po.arrivedVietnamPortDateString + ' - ' +
      po.arrivedVietnamPortUserObj.firstName + ' ' + po.arrivedVietnamPortUserObj.lastName;
  }

  getApproveInfo(po: PoModel) {
    if (!po || !po.approveUserObj || !po.approveDate) {
      return '';
    }
    return this.dateUtilService.convertDateToDisplayGMT0(po.approveDate) + ' - ' +
      po.approveUserObj.firstName + ' ' + po.approveUserObj.lastName;
  }

  hasAuthorityApproved() {
    return this.authoritiesService.hasAuthority('distributorsApproved');
  }

  // log(e: any): any {
  //   console.log('abccccccccccccccccccccccccccccc', e, e.status, mapStyles.STATUS[e.status]);
  //
  //   return e.status ? (mapStyles.STATUS[e.status].title) : ''
  // }

  async ngOnInit() {
    const distributorObs: Observable<any> = this.poService.createGetAllDistributorObservable();
    const manufacturerObs: Observable<Page> = this.poService.createGetManufacturerObservable();
    const storeObs: Observable<any> = this.poService.createGetStoreObservable();

    const response = await forkJoin({
      distributors: distributorObs,
      manufacturers: manufacturerObs,
      stores: storeObs,
    }).toPromise();

    this.distributorOptions = (response.distributors as DistributorModel[]).map((d: DistributorModel) => {
      return new SelectModel(d.id, '' + d.name, false, d);
    });

    this.manufacturerOptions = response.manufacturers.content.map((m: ManufacturerModel) => {
      return new SelectModel(m.id, '' + m.code + ' - ' + m.manufacturerDescriptions.filter(des => des.language && des.language.code === this.translateService.currentLang)[0]?.name, false, m);
    });

    this.storeOptions = (response.stores as Page).content?.map((s: StoreModel) => {
      return new SelectModel(s.id, '' + s.code, false, s);
    });

    this.statusOptions = ModelUtils.getInstance().getSelectModel(poConst);
    this.searchForm.patchValue({
      distributorId: this.distributorOptions.length > 0 ? this.distributorOptions[0].value : '',
      manufacturerId: this.manufacturerOptions.length > 0 ? this.manufacturerOptions[0].value : '',
      storeId: this.storeOptions.length > 0 ? this.storeOptions[0].value : '',
      status: poConst.NEW.code,
      dateType: poConst.NEW.code
    });
    this.setUpDefaultTime();
    if (this.poAuthoritiesService.hasGoodReceivePoRole()){
      this.searchForm.patchValue({
        status: poConst.ARRIVED_MYANMAR_PORT.code
      });
    }
    super.ngOnInit();
    this.onSubmit();
    // this.formStateService.uiState$
    //   .subscribe((mapState: Map<string, FormGroup>) => {
    //     if (!mapState.has(this.moduleName)) {
    //       this.searchForm.patchValue({
    //         distributorId: this.distributorOptions.length > 0 ? this.distributorOptions[0].value : '',
    //         manufacturerId: this.manufacturerOptions.length > 0 ? this.manufacturerOptions[0].value : '',
    //         storeId: this.storeOptions.length > 0 ? this.storeOptions[0].value : '',
    //         status: poConst.NEW.code,
    //         dateType: poConst.NEW.code
    //       });
    //       this.setUpDefaultTime();
    //     }
    //     this.onSubmit();
    //   });


  }

  setUpDefaultTime() {
    const now = new Date();
    const defaultFromDate = new Date(now.getFullYear(), now.getMonth() - 1, now.getDate());
    const defaultToDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    this.searchForm.patchValue({
      searchDate: new DateRangePickerModel(defaultFromDate, defaultToDate)
    });
  }

  search() {

    // http://demo.aggregatoricapaci.com:8888/pos?storeId=1&distributorId=&text=&fromDate=2020-11-29%2007:00:00.000Z&toDate=2020-12-29%2007:00:00.000Z&status=NEW&dateType=NEW&pageNumber=1&pageSize=10&orderBy=
    const {storeId, distributorId, manufacturerId, text, fromDate, toDate, status, dateType} = this.searchForm.value;
    const params = new HttpParams()
      .set('storeId', storeId ? storeId + '' : '')
      .set('distributorId', distributorId ? distributorId + '' : '')
      .set('manufacturerId', manufacturerId ? manufacturerId + '' : '')
      .set('text', text ? text : '')
      .set('fromDate', fromDate ? fromDate : '')
      .set('toDate', toDate ? toDate : '')
      .set('status', this.getSelectValue(status))
      .set('dateType', this.getSelectValue(dateType))
      .set('orderBy', '');

    this._fillData('/pos', params);
  }

  onAddNewPO() {
    this.router.navigate(['po', 'add']);
  }

  hasAuthorityCreate() {
    return this.authoritiesService.hasAuthority('distributorsCreate');
  }

  onEditPO(model: PoModel, index: number) {
    this.router.navigate(['po', 'edit', model.id]);

  }

  onView(model: PoModel, index: number) {
    this.router.navigate(['po', 'dashboard', model.id]);

  }

  receiveGoods(model: PoModel, index: number) {
    this.router.navigate(['po', 'gr', model.id]);
  }

  deliveryDate(model: PoModel, index: number) {
    this.router.navigate(['po', ActionTypeEneum.deliveryDate, model.id]);
  }

  onPrintPDF(model: PoModel, index: number) {
    this.utilsService.showConfirmDialog('common.confirmPrintPdf', ['common.PO'])
      .afterClosed().subscribe(result => {
      if (result && result.value) {
        this.apiService.saveFile('/pos/print-pdf/' + model.id, null, {
          headers: undefined,
          params: undefined
        });

      }
    });


  }

  printPOs() {
    const ids = this.results.data.filter((row: any) => row.checked)
      .map((row: any) => {
        return row.id;
      });
    if (ids.length === 0) {
      this.utilsService.showErrorToarst('notice.checkbox.nocheck');
      return;
    }

    this.apiService.saveFile(`/pos/print`, ids, {
      headers: undefined,
      params: undefined
    });
  }

  onExportReport() {
    this.utilsService.showConfirmDialog('common.confirmPrint', ['common.PO'])
      .afterClosed().subscribe(result => {
      if (result && result.value) {

        this.printPOs();

      }
    });


  }


  clearSearch() {
    this.resetSearchFormValue();
    this.searchForm.patchValue({
      distributorId: this.distributorOptions.length > 0 ? this.distributorOptions[0].value : '',
      manufacturerId: this.manufacturerOptions.length > 0 ? this.manufacturerOptions[0].value : '',
      storeId: this.storeOptions.length > 0 ? this.storeOptions[0].value : '',
      status: poConst.NEW.code,
      dateType: poConst.NEW.code
    });
    this.setUpDefaultTime();
    this.onSubmit();
  }


  private getPoStatus(po: PoModel) {
    if (!po || !po.status) {
      return '';
    }
    return this.translateService.instant(poConst[po.status].label);
  }

  private getPoStatusStyle(po: PoModel) {
    if (!po || !po.status) {
      return '';
    }
    return poConst[po.status].styleTable;
  }

}

