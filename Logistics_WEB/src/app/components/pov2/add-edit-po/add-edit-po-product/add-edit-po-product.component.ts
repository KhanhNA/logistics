import {Component, Inject, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  DateUtilService, LoaderService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {Pov2Service} from '../../pov2.service';
import {PoDetailModel} from '../../../../_models/po/po.detail.model';
import {ActionTypeEneum, PoStatus} from '../../../../_models/action-type';
import {DateUtils} from '../../../../base/Utils/date.utils';
import {PoModel} from '../../../../_models/po/po.model';
import {ProductPackingsComponent} from '../../../dialog-select-product-packing/product-packings.component';
import {MatDialog} from '@angular/material/dialog';
import {ProductDescriptionsUtils} from '../../../../_utils/product.descriptions.utils';
import {PoAuthoritiesService} from "../../../../_services/authority/po.authorities.service";
import {poStatusAction} from "../../../../_models/po/po.const";
import {DividePackingComponent} from "../../../divide-packing/divide-packing.component";
import {ProductModel} from "../../../../_models/product.model";
import {ProductPackingPriceModel} from "../../../../_models/product.packing.price.model";
import {CommonHttpService} from "../../../../_services/http/common/common.http.service";
import {ProductPackingModel} from "../../../../_models/product.packing.model";
import {HttpParams} from "@angular/common/http";

const readXlsxFile = require('read-excel-file');

@Component({
  selector: 'app-add-edit-po-product',
  templateUrl: './add-edit-po-product.component.html',
  styleUrls: ['./add-edit-po-product.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddEditPoProductComponent extends BaseAddEditLayout implements OnInit {
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  @Input() form?: FormGroup;
  @Input() actionType?: string;
  currentLangCode: string;
  @Input() hasClaim = false;
  @Input() po?: PoModel;
  @Input() isPendingApprove = false;
  @Input() isReadOnly = false;

  get poStatusAction() {
    return poStatusAction;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              @Inject(Pov2Service) protected poService: Pov2Service,
              protected dateUtilService: DateUtilService,
              private dialog: MatDialog,
              public commonHttpService: CommonHttpService,
              public loaderService: LoaderService,
              public poAuthoritiesService: PoAuthoritiesService
  ) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.currentLangCode = this.translateService.currentLang ?
      this.translateService.currentLang : this.translateService.defaultLang;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.defineColumnTable();
  }

  onAddDetail() {

    let mId = -1;
    if (this.form?.get('manufacturer')) {
      mId = this.form?.get('manufacturer')?.value;
    }
    let distributorId = -1;
    if (this.form?.get('distributor')) {
      distributorId = this.form?.get('distributor')?.value;
    }
    const dialogRef = this.dialog.open(ProductPackingsComponent, {
      panelClass: 'choose-product-packings',
      disableClose: false,
      width: '90%',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        manufacturerId: mId,
        distributorId,
        exceptId: this.getExceptProductPackingIds().join(', ')
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.addTableData(result);

    });
  }


  addTableData(data: any[]) {
    // const rows = this.convertToRows(data);
    if (data) {
      const rows = this.convertToRows(data);
      const details = this.form?.get('poDetails')?.value as (PoDetailModel | any)[];
      details.push(...rows);
      this.form?.get('poDetails')?.setValue(details);
    }
  }

  convertToRows(data: any) {
    console.log(data);
    const rows = [];
    let row;
    const dontHavePrice: number[] = [];
    let indexRow = this.form?.get('poDetails')?.value.length;
    if (data) {
      for (const item of Object.keys(data)) {
        indexRow++;
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(data[item].product);

        row = {
          quantity: isNaN(Number(data[item].quantity)) ? 1 : Number(data[item].quantity),
          total: 0,
          vat: data[item].vat,
          productPacking: {
            id: data[item].id, code: data[item].code, name: data[item].name,
            uom: data[item].uom,
            packingType: {quantity: data[item].packingType.quantity}
          },
          product: data[item].product,
          productPackingPrice: {},
          // {
          //     id: data[item].productPackingPrices[0].id,
          //     price: data[item].productPackingPrices[0].price,
          //     currency: data[item].productPackingPrices[0].currency,
          //   },
          productPackingPrices: data[item].productPackingPrices,
        };
        let hasPrice = false;
        let control;
        for (const packingPrice of row.productPackingPrices) {
          control = this.form?.get('fromCurrency');
          if (control && packingPrice.currency.id === control.value) {
            row.productPackingPrice = packingPrice;
            hasPrice = true;
            // row.total = packingPrice.price * data[item].vat / 100;
            break;
          }
        }
        ProductModel.reduceCircleReference(row.product);
        row.productPackingPrices?.forEach((price: any) => {
          ProductPackingPriceModel.reduceCircleReference(price);
        });
        if (!hasPrice) {
          dontHavePrice.push(indexRow);
        }
        rows.push(row);
      }
    }
    if (dontHavePrice.length > 0) {
      this.poService.isNotPrice.next(true);
      this.showErrorMsg('packing.not.have.price', dontHavePrice, ['USD']);
    }
    return rows;
  }

  getExceptProductPackingIds() {
    const packingIds = [-1];
    if (this.form?.get('poDetails')?.value.length > 0) {
      this.form?.get('poDetails')?.value.forEach((packing: any) => {
        if (packing.productPacking) {
          packingIds.push(packing.productPacking.id);
        }
      });
    }

    return packingIds;
  }

  showErrorMsg = (msgKey: string, rowIndex?: any[], msgParams?: any[]) => {
    console.log('errrrrorrrrrrrrrrrrrrrrrrrrrrr', msgKey);
    let msg = this.translateService.instant(msgKey);
    if (msgParams) {
      msg = this.utilsService.strFormat(msgKey, msgParams);
    }

    if (rowIndex && rowIndex.length > 0) {
      msg = '' + msg + rowIndex.join(', ');
    }
    this.utilsService.showErrorToarst(msg);
  }

  onDeleteDetail(row: PoDetailModel, index: number) {
    const details = this.form?.get('poDetails')?.value as PoDetailModel[];
    details.splice(index, 1);
    this.poService.isNotPrice.next(details.findIndex(detail => !detail.productPackingPrice?.id) > -1)
    this.form?.get('poDetails')?.setValue(details);
  }

  defineColumnTable() {
    this.columns.push(...[
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${(this.form?.get('poDetails')?.value?.indexOf(e) + 1).toLocaleString('en-US')}`,
        cell: (e: any) => `${(this.form?.get('poDetails')?.value?.indexOf(e) + 1).toLocaleString('en-US')}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
        isShowHeader: true
      },
      {
        columnDef: 'productPacking.code',
        header: 'productPacking.code',
        title: (e: PoDetailModel) => `${e.productPacking?.code}`,
        cell: (e: PoDetailModel) => `${e.productPacking?.code}`,
        isShowHeader: true
      },
      {
        columnDef: 'product.name',
        header: 'product.name',
        title: (e: PoDetailModel) => `${e ? e.product?.productDescriptions?.filter(description =>
          description.language?.code === this.currentLangCode)[0].name : ''}`,
        cell: (e: PoDetailModel) => `${e ? e.product?.productDescriptions?.filter(description =>
          description.language?.code === this.currentLangCode)[0].name : ''}`,
        isShowHeader: true
      },
      {
        columnDef: 'productPacking.uom',
        header: 'productPacking.uom',
        title: (e: PoDetailModel) => `${e.productPacking?.uom}`,
        cell: (e: PoDetailModel) => `${e.productPacking?.uom}`,
        isShowHeader: true
      },
      {
        columnDef: 'productPacking.packingType.quantity',
        header: 'productPacking.packingType.quantity',
        title: (e: PoDetailModel) => `${e.productPacking?.packingType?.quantity?.toLocaleString('en-US')}`,
        cell: (e: PoDetailModel) => `${e.productPacking?.packingType?.quantity?.toLocaleString('en-US')}`,
        isShowHeader: true
      },
      {
        columnDef: 'orgQuantity',
        header: 'orgQuantity',
        columnType: ColumnTypes.VIEW,
        title: (e: any) => `${Number(e.orgQuantity).toLocaleString('en-US')}`,
        cell: (e: any) => `${Number(e.orgQuantity).toLocaleString('en-US')}`,
        display: (e: any) => this.hasClaim,
        isShowHeader: this.hasClaim,
      },
      {
        columnDef: 'claimQuantity',
        header: 'claimQuantity',
        columnType: ColumnTypes.VIEW,
        title: (e: any) => `${Number(e.claimQuantity).toLocaleString('en-US')}`,
        cell: (e: any) => `${Number(e.claimQuantity).toLocaleString('en-US')}`,
        display: (e: any) => this.hasClaim,
        isShowHeader: this.hasClaim,
      },
      {
        columnDef: 'quantity',
        header: (e: any) => this.hasClaim ? this.translateService.instant('common.quantity.real.import')
          : this.translateService.instant('common.quantity'),
        columnType: (this.actionType === ActionTypeEneum.new || this.actionType === ActionTypeEneum.edit) ? ColumnTypes.INPUT_COUNTER
          : ColumnTypes.VIEW,
        title: (e: PoDetailModel) => `${Number(e.quantity).toLocaleString('en-US')}`,
        cell: (e: PoDetailModel) => `${Number(e.quantity).toLocaleString('en-US')}`,
        disabled: (e: any) => this.isReadOnly,
        isShowHeader: true,
        min: (e: any) => 1,
        isRequired: () => (this.actionType === ActionTypeEneum.new || this.actionType === ActionTypeEneum.edit) ? true : false,
        errorMessage: new Map<string, () => string>()
          .set('min', () => 'validation.minQuantity')
          .set('pattern', () => 'validation.quantity.pattern.number')
          .set('required', () => this.translateService.instant('common.required')),
        onCellValueChange: (e: any) => {
          // console.log(this.form?);
        },
        validate: (e: any) => {
          // if (e.quantity <=0){
          //   return {min: {min: 1, actual: e.quantity}};
          // }
          return null;
        }
      },
      {
        columnDef: 'productPackingPrice.price',
        header: 'productPackingPrice.price',
        title: (e: PoDetailModel) => `${e.productPackingPrice?.price ? e.productPackingPrice.price.toLocaleString('en-US') : '0'}`,
        cell: (e: PoDetailModel) => `${e.productPackingPrice?.price ? e.productPackingPrice.price.toLocaleString('en-US') : '0'}`,
        isShowHeader: true,
      },
      {
        columnDef: 'productPackingPrice.currency.name',
        header: 'productPackingPrice.currency.name',
        title: (e: PoDetailModel) => `${e.productPackingPrice?.currency ? e.productPackingPrice.currency.name : ''}`,
        cell: (e: PoDetailModel) => `${e.productPackingPrice?.currency ? e.productPackingPrice.currency.name : ''}`,
        isShowHeader: true
      },
      {
        columnDef: 'total',
        header: 'total',
        title: (e: any) => e.productPackingPrice && e.productPackingPrice.productPacking ?
          ((e.productPackingPrice.price /* * (100 + e.productPackingPrice.productPacking.vat)*/ * Number(e.quantity)) /*/ 100*/)
            .toLocaleString('en-US')
          : '0',
        cell: (e: any) => e.productPackingPrice && e.productPackingPrice.productPacking ?
          ((e.productPackingPrice.price /* * (100 + e.productPackingPrice.productPacking.vat) */ * Number(e.quantity))/* / 100*/)
            .toLocaleString('en-US')
          : '0',
        isShowHeader: true
      },
      {
        columnDef: 'expireDate',
        header: 'expireDate',
        columnType: this.actionType === ActionTypeEneum.GR ? ColumnTypes.DATE_PICKER : ColumnTypes.VIEW,
        min: (e: any) => this.onDisabledExpiredDate(e) ? null
          : (e.isClaimAll && e.expireDate) ? (DateUtils.getDayBeforeNow(-1) < new Date(e.expireDate)
            ? DateUtils.getDayBeforeNow(-1) : new Date(e.expireDate)) : DateUtils.getDayBeforeNow(-1),
        max: (e: any) => this.onDisabledExpiredDate(e) ? new Date(9999, 12, 31)
          : this.dateUtilService.addDays(DateUtils.getCurrentDate_0H(), e.product.lifecycle),
        disabled: (e: any) => {
          return this.onDisabledExpiredDate(e);
        },
        errorMessage: new Map<string, () => string>()
          .set('required', () => this.translateService.instant('common.required'))
          .set('min', () => this.translateService.instant('common.error.date.min')),
        title: (e: any) => e.expireDate ? this.dateUtilService.convertDateToDisplayGMT0(e.expireDate) + '' : '',
        cell: (e: any) => e.expireDate ? this.dateUtilService.convertDateToDisplayGMT0(e.expireDate) + '' : '',
        display: (e: any) => poStatusAction.isImportedStatus(this.po) || this.actionType === ActionTypeEneum.GR,
        onCellValueChange: (e: any) => {
          // const curDate = new Date();
          // const expireDate = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate()
          //   + e.product.lifecycle, 0, 0, 0);
          // if (new Date('' + e.expireDate) > expireDate) {
          //   this.showWarningMsg('date.pick.not.gather.than.life.cycle');
          //   e.expireDate = '';
          // }
        },
        validate: (e: any) => {
          return null;
        },
        isRequired: this.actionType === ActionTypeEneum.GR
      }
    ]);

    this.buttons.push(...[
      {
        columnDef: 'delete',
        color: 'warn',
        icon: 'delete',
        click: 'onDeleteDetail',
        header: 'common.action',
        display: (e: any) => e && this.authoritiesService.hasAuthority('post/pos')
          && ![ActionTypeEneum.GR.toString(), ActionTypeEneum.view.toString()].includes(this.actionType + '')
          && this.isShowOrDisabledTblButton({act: 'delete', row: e}),

      },
      {
        columnDef: 'add_box',
        color: 'warn',
        icon: 'add_box',
        click: 'divideDetail',
        isShowHeader: false,
        display: (e: any) => e && this.poAuthoritiesService.hasGoodReceivePoRole()
          && this.actionType === ActionTypeEneum.GR,
        disabled: (e: any) => !this.isShowOrDisabledTblButton({act: 'add_box', row: e}) || this.isPendingApprove
      },
      {
        columnDef: 'clear',
        color: 'warn',
        icon: 'clear',
        click: 'removeDetail',
        isShowHeader: false,
        display: (e: any) => e && this.authoritiesService.hasAuthority('post/import-pos')
          && this.actionType === ActionTypeEneum.GR,
        disabled: (e: any) => !this.isShowOrDisabledTblButton({act: 'clear', row: e}) || this.isPendingApprove
      }
    ]);
  }

  onDisabledExpiredDate(e: any | PoDetailModel) {
    return (poStatusAction.isImportedStatus(this.po)
      || this.actionType === ActionTypeEneum.view) || e.isClaimAll || this.isPendingApprove;
  }

  isShowOrDisabledTblButton(event: { act: any, row: any }): boolean {
    if (!this.form?.get('manufacturer')?.value) {
      return false;
    }

    if (this.actionType === ActionTypeEneum.edit && poStatusAction.isApprovedStatus(this.po)) {
      return false;
    }

    if (this.actionType === ActionTypeEneum.new) {
      return ['delete', 'add'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.GR) {
      if (event.act === 'add') {
        return false;
      }

      if (event.act === 'clear' && this.form?.get('poDetails')) {
        const ind = this.form?.get('poDetails')?.value.indexOf(event.row);
        if ((this.form?.get('poDetails')?.value[ind - 1] && this.form?.get('poDetails')?.value[ind - 1].id === event.row.id) ||
          (this.form?.get('poDetails')?.value[ind + 1] && this.form?.get('poDetails')?.value[ind + 1].id === event.row.id)) {
          return true;
        } else {
          return false;
        }
      }

      if (event.row.quantity <= 1) {
        return ['clear'].includes(event.act);
      }
      return ['add_box', 'clear'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.editPo) {
      return !['add_box', 'clear'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.Pal
      || this.actionType === ActionTypeEneum.view) {
      return false;
    }
    return false;
  }

  divideDetail(row: PoDetailModel, index: number) {
    const shallowCopy = {...row};
    this.dialog.open(DividePackingComponent, {
      disableClose: false,
      height: '38%',
      width: '40vw',
      data: {packing: shallowCopy, type: ActionTypeEneum.GR}
    }).afterClosed().subscribe(response => {
      if (response) {
        this.dividePacking(response.data, {ind: index, row});
      }
    });
  }

  dividePacking(dividedPacking: any, originPackingRow: any) {
    const tblData = this.form?.get('poDetails')?.value;
    const length = tblData.length;
    const dataTable = [];
    for (let i = 0; i < length; i++) {
      if (tblData[i].expireDate) {
        // const arr = tblData[i].expireDate.split(' ');
        // console.log(tblData[i].expireDate, new Date(tblData[i].expireDate));
        // tblData[i].expireDate = DateUtils.getDateFromString_0H(tblData[i].expireDate);
        // tblData[i].expireDate =
        // this.datePipe.transform(DateUtils.getDateFromString_0H(tblData[i].expireDate), environment.API_DATE_FORMAT);
        tblData[i].expireDate = tblData[i].expireDate.replace(' ', 'T');
        // console.log(tblData[i].expireDate);
      }
      dataTable.push(tblData[i]);
      if (i === originPackingRow.ind) {
        if (dividedPacking.expireDate) {
          dividedPacking.expireDate = dividedPacking.expireDate.replace(' ', 'T');
        }
        tblData[i].quantity = Number(tblData[i].quantity) - Number(dividedPacking.quantity);
        dividedPacking.maxQuantity = tblData[i].maxQuantity - tblData[i].quantity;
        dividedPacking.orgQuantity = dividedPacking.quantity;
        dividedPacking.claimQuantity = 0;
        tblData[i].maxQuantity = tblData[i].quantity;
        tblData[i].orgQuantity = Number(tblData[i].orgQuantity) - Number(dividedPacking.quantity);
        dataTable.push(dividedPacking);
      }
    }
    this.form?.get('poDetails')?.setValue(dataTable);
  }

  removeDetail(row: any, index: number) {
    const beforeObj = this.form?.get('poDetails')?.value[index - 1];
    const afterObj = this.form?.get('poDetails')?.value[index + 1];
    if ((index === 0 && afterObj.id !== row.id) ||
      (index === this.form?.get('poDetails')?.value.length - 1 && beforeObj.id !== row.id) ||
      (beforeObj && beforeObj.id !== row.id && afterObj.id !== row.id)) {
      this.utilsService.showWarningToarst('common.packing.remove.not.divide');
      return;
    }
    if (beforeObj && beforeObj.id === row.id) {
      beforeObj.quantity = Number(row.quantity) + Number(beforeObj.quantity);
      beforeObj.maxQuantity = Number(row.maxQuantity) + Number(beforeObj.maxQuantity);
      beforeObj.orgQuantity = Number(beforeObj.orgQuantity) + Number(row.orgQuantity);
      beforeObj.claimQuantity = Number(beforeObj.claimQuantity) + Number(row.claimQuantity);
      beforeObj.total = Number(beforeObj.quantity) * (100 + Number(beforeObj.vat)) *
        Number(beforeObj.productPackingPrice.price) / 100;
    } else if (afterObj.id === row.id) {
      afterObj.quantity = Number(row.quantity) + Number(afterObj.quantity);
      afterObj.maxQuantity = Number(row.maxQuantity) + Number(afterObj.maxQuantity);
      afterObj.orgQuantity = Number(afterObj.orgQuantity) + Number(row.orgQuantity);
      afterObj.claimQuantity = Number(afterObj.claimQuantity) + Number(row.claimQuantity);
      afterObj.total = Number(afterObj.quantity) * (100 + Number(afterObj.vat)) *
        Number(afterObj.productPackingPrice.price) / 100;
    }
    this.form?.get('poDetails')?.value.forEach((packing: any) => {
      if (packing.expireDate) {
        packing.expireDate = packing.expireDate.replace(' ', 'T');
      }
    });
    const tblData = this.form?.get('poDetails')?.value;
    tblData.splice(index, 1);
    this.form?.get('poDetails')?.setValue(tblData);
  }

  onAddDetailFromExcel(inputRef: HTMLInputElement) {
    console.log(inputRef.files)
    if (inputRef.files && inputRef.files[0]) {
      const schema = {
        // 'No.': {
        //   prop: 'stt',
        //   type: Number
        // },
        // 'productPacking': {
        //   prop: 'productPacking',
        //   type: {
        'Package code': {
          prop: 'code',
          type: String
        },
        // 'Product Name': {
        //   prop: 'name',
        //   type: String
        // },
        // 'UOM': {
        //   prop: 'uom',
        //   type: String
        // },
        // 'Pack size': {
        //   prop: 'packingType',
        //   type: Number
        // },
        'Quantity': {
          prop: 'quantity',
          type: Number
        },
        // 'Price': {
        //   prop: 'price',
        //   type: Number
        // },
        // 'Currency': {
        //   prop: 'currency',
        //   type: String
        // },
        // 'Amount': {
        //   prop: 'total',
        //   type: Number
        // },
        // }
        // }
      }
      this.loaderService.isLoading.next(true);
      readXlsxFile.default(inputRef.files[0], {schema}).then((result: { rows: ProductPackingModel[], errors: any }) => {
        console.log(result);
        let includeCheck;
        let pp;
        let tempPP;
        const importedPPMap: Map<string, ProductPackingModel> = new Map<string, ProductPackingModel>();
        if (result.rows.length) {
          this.form?.patchValue({
            poDetails: []
          });
          result.rows.forEach(importPP => {
            includeCheck = this.poService.productPackingMap.has(importPP.code + '');
            if (includeCheck) {
              pp = this.poService.productPackingMap.get(importPP.code + '') as ProductPackingModel;
              tempPP = importedPPMap.get(pp.code + '');
              if (tempPP) {
                tempPP.quantity = (tempPP.quantity ? tempPP.quantity : 0)
                  + (isNaN(Number(importPP.quantity)) ? 0 : Number(importPP.quantity));
                importedPPMap.set(tempPP.code + '', tempPP);
              } else {
                pp.quantity = importPP.quantity;
                importedPPMap.set(pp.code + '', pp);
              }
            }
          });

          this.addTableData([...importedPPMap.values()]);
        } else {
          this.utilsService.showErrorToarst('common.list.empty', ['common.product.packing']);
          return;
        }

      }).then(() => {
        console.log(inputRef)
        inputRef.value = '';
      }).finally(() => {
        this.loaderService.isLoading.next(false);
      });
    }

  }

  async onImportByExcelOnClient(importFromExcel: HTMLInputElement) {
    if (!this.poService.productPackingMap.size) {
      const {manufacturer, distributor} = this.form?.value;
      const data = await this.commonHttpService.createFindAllProductPackingInPoObs({
        manufacturerId: manufacturer,
        distributorId: distributor
      }).toPromise();
      console.log(data);
      if (!data.content.length) {
        this.utilsService.showErrorToarst('common.list.empty', ['common.product.packing']);
        return;
      }
      (data.content as ProductPackingModel[]).forEach(pp => {
        this.poService.productPackingMap.set(pp.code + '', pp);
      });
    }
    if (importFromExcel) {
      importFromExcel.click();
    }
  }

  downloadTemplate(){
    const {manufacturer, distributor} = this.form?.value;
    const params = new HttpParams()
      .set('distributorId', distributor + '')
      .set('manufacturerId', manufacturer  + '')
      .set('exceptId', this.getExceptProductPackingIds().join(', '))
    this.apiService.saveFile('/pos/download-template', undefined, {params});
  }
}
