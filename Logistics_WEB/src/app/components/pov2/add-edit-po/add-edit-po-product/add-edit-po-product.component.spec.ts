import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPoProductComponent } from './add-edit-po-product.component';

describe('AddEditPoProductComponent', () => {
  let component: AddEditPoProductComponent;
  let fixture: ComponentFixture<AddEditPoProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditPoProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPoProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
