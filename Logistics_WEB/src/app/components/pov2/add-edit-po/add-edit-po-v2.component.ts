import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ButtonFields,
  ColumnFields,
  DateUtilService, DialogTypeEnum, NsCustomDialogDataConfig,
  Page,
  SelectModel, UploadModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ManufacturerModel} from '../../../_models/manufacturer.model';
import {ManufacturerDescriptionModel} from '../../../_models/manufacturer.description.model';
import {StoreModel} from '../../../_models/store.model';
import {DistributorModel} from '../../../_models/distributor.model';
import {CurrencyModel} from '../../../_models/currency.model';
import {PoModel} from '../../../_models/po/po.model';
import {PoDetailModel} from '../../../_models/po/po.detail.model';
import {ActionTypeEneum, ClaimEnum, ImportPoStatus} from '../../../_models/action-type';
import {ProductPackingsComponent} from '../../dialog-select-product-packing/product-packings.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {ToastrService} from 'ngx-toastr';
import {DateUtils} from '../../../base/Utils/date.utils';
import {ImportPoModel} from '../../../_models/import-po/import.po.model';
import {ClaimDetailModel} from '../../../_models/claim.detail.model';
import {ExchangeRateModel} from '../../../_models/exchangeRateModel';
import {Pov2Service} from '../pov2.service';
import {poConst, poStatusAction} from '../../../_models/po/po.const';
import {PoAuthoritiesService} from '../../../_services/authority/po.authorities.service';
import {environment} from "../../../../environments/environment";
import {Observable, of} from "rxjs";
import {ProductModel} from "../../../_models/product.model";
import {CommonUtils} from "../../../_utils/common.utils";
import {ProductPackingPriceModel} from "../../../_models/product.packing.price.model";


@Component({
  selector: 'app-add-edit-po-v2',
  templateUrl: './add-edit-po-v2.component.html',
  styleUrls: ['./add-edit-po-v2.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddEditPoV2Component extends BaseAddEditLayout implements AfterViewInit, OnInit {
  moduleName = 'po.ae';

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  manufacturerOptions: SelectModel[] = [];
  storeOptions: SelectModel[] = [];
  distributorOptions: SelectModel[] = [];
  currencyOptions: SelectModel[] = [];
  toCurrencyOptions: SelectModel[] = [];

  exchangeRate: ExchangeRateModel | undefined;
  po: PoModel | undefined;
  parentId: string | null;
  currentLangCode: string;
  actionType: string | null | ActionTypeEneum;

  isPendingApprove = false;
  hasClaim = false;
  claimId: number | undefined;
  tempPoDetailsValue: any;

  errorDeliveryDateMessages = new Map().set('min', () => this.translateService.instant('common.error.date.min'))
    .set('required', () => this.translateService.instant('common.required'));
  errorDeliveryAddressMessages = new Map().set('required', () => this.translateService.instant('common.required'));
  errorExchangeRateMessages = new Map().set('pattern', () => this.translateService.instant('validation.minExchangeRate'))
    .set('required', () => this.translateService.instant('common.required'));

  get poStatusAction() {
    return poStatusAction;
  }

  get ActionType() {
    return ActionTypeEneum;
  }
  get CommonUtils() {
    return CommonUtils;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              public authoritiesService: AuthoritiesService,
              private dialog: MatDialog,
              private router: Router,
              private cdr: ChangeDetectorRef,
              public poService: Pov2Service,
              private dateUtilService: DateUtilService,
              public poAuthoritiesService: PoAuthoritiesService,
              private toastr: ToastrService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.addEditForm = this.formBuilder.group({
      manufacturer: ['', [Validators.required]],
      store: ['', [Validators.required]],
      distributor: ['', [Validators.required]],
      arriveVietnamPortDate: [''],
      arrivedVietnamPortDate: [''],
      arriveMyanmarPortDate: [''],
      arrivedMyanmarPortDate: [''],
      arriveFcDate: [''],
      importDate: [''],
      deliveryAddress: ['', [Validators.required]],
      fromCurrency: ['1', [Validators.required]], // fix from currency la usd -> id = 1
      exchangeRate: ['',
        [Validators.pattern('^(0*[1-9][0-9]*(\\.[0-9]+)?|0+\\.[0-9]*[1-9][0-9]*)$')]],
      toCurrency: ['3', [Validators.required]], // fix tocurrency la MMK -> id = 3
      description: [''],
      rejectReason: '',
      poDetails: [''],
      resultQC: ['']
    });
    this.parentId = this.activatedRoute.snapshot.paramMap.get('id');
    this.actionType = this.activatedRoute.snapshot.paramMap.get('actType');
    this.currentLangCode = this.translateService.currentLang ?
      this.translateService.currentLang : this.translateService.defaultLang;
    // this.defineColumnTable();
    this.poService.getCurrencyExchangeRate(this.addEditForm.get('fromCurrency')?.value,
      this.addEditForm.get('toCurrency')?.value, (value: any) => {
        this.addEditForm.get('exchangeRate')?.setValue(value);
      });
  }


  onDisabledExpiredDate(e: any | PoDetailModel) {
    return (poStatusAction.isImportedStatus(this.po)
      || this.actionType === ActionTypeEneum.view) || e.isClaimAll || this.isPendingApprove;
  }

  isShowOrDisabledTblButton(event: { act: any, row: any }): boolean {
    if (!this.addEditForm.get('manufacturer')?.value) {
      return false;
    }

    if (this.actionType === ActionTypeEneum.edit && poStatusAction.isApprovedStatus(this.po)) {
      return false;
    }

    if (this.actionType === ActionTypeEneum.new) {
      return ['delete', 'add'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.GR) {
      if (event.act === 'add') {
        return false;
      }

      if (event.act === 'clear' && this.addEditForm.get('poDetails')) {
        const ind = this.addEditForm.get('poDetails')?.value.indexOf(event.row);
        if ((this.addEditForm.get('poDetails')?.value[ind - 1] && this.addEditForm.get('poDetails')?.value[ind - 1].id === event.row.id) ||
          (this.addEditForm.get('poDetails')?.value[ind + 1] && this.addEditForm.get('poDetails')?.value[ind + 1].id === event.row.id)) {
          return true;
        } else {
          return false;
        }
      }

      if (event.row.quantity <= 1) {
        return ['clear'].includes(event.act);
      }
      return ['add_box', 'clear'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.editPo) {
      return !['add_box', 'clear'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.Pal
      || this.actionType === ActionTypeEneum.view) {
      return false;
    }
    return false;
  }

  updateAfterGetDetail = (data: any) => {
    if (((poStatusAction.isArrivedMyanmarPortStatus(data) && this.actionType === ActionTypeEneum.GR))
      || poStatusAction.isImportedStatus(data)) {
      this.isPendingApprove = false;
      if (this.onDisplayWithAuthority('get/claims/by-reference')) {
        const params = new HttpParams()
          .set('type', ClaimEnum.IMPORT_PO)
          .set('referenceId', '' + data.id);
        this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', params)
          .subscribe((claimDetails: ClaimDetailModel[]) => {
              this.getClaimSuccess(claimDetails);
            },
            (error: any) => {
              this.isPendingApprove = true;
              this.showErrorMsg(error);
              this.bindingData(data);
            });
      } else {
        this.bindingData(data);
      }

    } else {
      this.bindingData(data);
    }
  }

  getClaimSuccess = (claimDetails: ClaimDetailModel[]) => {
    const claimMap = new Map<number, any>();
    const claimMapForImportPo = new Map<string, ClaimDetailModel>();
    if (claimDetails.length > 0) {
      this.claimId = claimDetails[0].claim?.id;
      this.hasClaim = true;
      let key = '';
      claimDetails.forEach((claimDetail: ClaimDetailModel) => {
        if (claimDetail.productPacking && claimDetail.productPacking.id) {
          if (claimMap.has(claimDetail.productPacking.id)) {
            const detail = claimMap.get(claimDetail.productPacking.id);
            detail.quantity = +detail.quantity
              + Number(claimDetail.quantity);
          } else {
            claimMap.set(claimDetail.productPacking.id, claimDetail);
          }

          key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
          if (claimMapForImportPo.has(key)) {
            const claimDetailMap = claimMapForImportPo.get(key);
            if (claimDetailMap) {
              claimDetailMap.quantity = Number(claimDetailMap.quantity) + Number(claimDetail.quantity);
              claimMapForImportPo.set(key, claimDetailMap);
            }
          } else {
            claimMapForImportPo.set(key, claimDetail);
          }
        }

      });
    }

    this.po?.poDetails?.forEach((poDetail: any) => {
      if (claimMap.has(poDetail.productPacking.id)) {
        poDetail.orgQuantity = poDetail.quantity;
        poDetail.claimQuantity = Number(claimMap.get(poDetail.productPacking.id).quantity);
        poDetail.quantity = Number(poDetail.quantity) - Number(claimMap.get(poDetail.productPacking.id).quantity);
        if (poDetail.quantity === 0) {
          poDetail.expireDate = claimMap.get(poDetail.productPacking.id).expireDate;
          poDetail.isClaimAll = true;
        }
      } else {
        poDetail.orgQuantity = poDetail.quantity;
        poDetail.claimQuantity = 0;
      }
    });

    this.bindingData(this.po, claimMapForImportPo);
    // this.tsuService.hideLoading();
  }

  bindingData(data: any, claimMapForImport?: Map<string, any>) {

    if (poStatusAction.isImportedStatus(data)) {
      const map = new Map();
      let key = '';
      data.importPo.importPoDetails.forEach((importPoDetail: any) => {
        key = '' + importPoDetail.expireDate + importPoDetail.productPacking.id;
        if (map.has(key)) {
          const detail = map.get(key);
          detail.quantity = Number(detail.quantity) + Number(importPoDetail.quantity);
          detail.orgQuantity = Number(detail.orgQuantity) + Number(importPoDetail.quantity);
        } else {
          if (claimMapForImport) {
            if (claimMapForImport.has(key)) {
              importPoDetail.claimQuantity = claimMapForImport.get(key).quantity;
              claimMapForImport.delete(key);
            } else {
              importPoDetail.claimQuantity = 0;
            }
            importPoDetail.orgQuantity = importPoDetail.quantity + importPoDetail.claimQuantity;
          }
          map.set(key, importPoDetail);
        }
      });
      const dataTable = [];
      if (claimMapForImport) {
        for (const claim of claimMapForImport.values()) {
          claim.orgQuantity = claim.quantity;
          claim.claimQuantity = claim.quantity;
          claim.quantity = claim.orgQuantity - claim.claimQuantity;
          claim.product = claim.productPacking.product;
        }
        dataTable.push(...claimMapForImport.values());
      }
      dataTable.push(...map.values());
      this.addEditForm.get('poDetails')?.setValue(dataTable);
      return;
    }
    data.poDetails.forEach((poDetail: PoDetailModel) => {
      // ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(poDetail.product);
      poDetail.poDetail = {id: poDetail.id, quantity: poDetail.quantity};
      poDetail.maxQuantity = poDetail.quantity;
      poDetail.productPackingPrices = poDetail.productPacking?.productPackingPrices;
      ProductModel.reduceCircleReference(poDetail.product);
      PoDetailModel.reduceCircleReference(poDetail);
      poDetail.productPackingPrices?.forEach((price: any) => {
        ProductPackingPriceModel.reduceCircleReference(price);
      });
    });
    this.addEditForm.get('poDetails')?.setValue(data.poDetails);
  }

  async ngOnInit() {
    super.ngOnInit();


    await this.getFormSourceData(true);
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
    // if (this.isEdit) {
    //   this.addEditForm.markAllAsTouched();
    // }
  }

  // onAddDetail() {
  //
  //   let mId = -1;
  //   if (this.addEditForm.get('manufacturer')) {
  //     mId = this.addEditForm.get('manufacturer')?.value;
  //   }
  //   let distributorId = -1;
  //   if (this.addEditForm.get('distributor')) {
  //     distributorId = this.addEditForm.get('distributor')?.value;
  //   }
  //   const dialogRef = this.dialog.open(ProductPackingsComponent, {
  //     disableClose: false,
  //     width: '90%',
  //     maxHeight: '90vh',
  //     data: {
  //       manufacturerId: mId,
  //       distributorId,
  //       exceptId: this.getExceptProductPackingIds().join(', ')
  //     }
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     this.addTableData(result);
  //
  //   });
  // }

  // addTableData(data: any[]) {
  //   // const rows = this.convertToRows(data);
  //   if (data) {
  //     const rows = this.convertToRows(data);
  //     const details = this.addEditForm.get('poDetails')?.value as (PoDetailModel | any)[];
  //     details.push(...rows);
  //     this.addEditForm.get('poDetails')?.setValue(details);
  //   }
  // }

  // convertToRows(data: any) {
  //   // console.log(data);
  //   const rows = [];
  //   let row;
  //   const dontHavePrice: number[] = [];
  //   let indexRow = this.addEditForm.get('poDetails')?.value.length;
  //   if (data) {
  //     for (const item of Object.keys(data)) {
  //       indexRow++;
  //       ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(data[item].product);
  //       ProductModel.reduceCircleReference(data[item].product);
  //       row = {
  //         quantity: 1,
  //         total: 0,
  //         vat: data[item].vat,
  //         productPacking: {
  //           id: data[item].id, code: data[item].code, name: data[item].name,
  //           uom: data[item].uom,
  //           packingType: {quantity: data[item].packingType.quantity}
  //         },
  //         product: data[item].product,
  //         productPackingPrice: {},
  //         // {
  //         //     id: data[item].productPackingPrices[0].id,
  //         //     price: data[item].productPackingPrices[0].price,
  //         //     currency: data[item].productPackingPrices[0].currency,
  //         //   },
  //         productPackingPrices: data[item].productPackingPrices,
  //       };
  //       let hasPrice = false;
  //       for (const packingPrice of data[item].productPackingPrices) {
  //         if (packingPrice.currency.id === this.addEditForm.get('fromCurrency')?.value) {
  //           row.productPackingPrice = packingPrice;
  //           hasPrice = true;
  //           // row.total = packingPrice.price * data[item].vat / 100;
  //           break;
  //         }
  //       }
  //       if (!hasPrice) {
  //         dontHavePrice.push(indexRow);
  //       }
  //       rows.push(row);
  //     }
  //   }
  //   if (dontHavePrice.length > 0) {
  //     this.showErrorMsg('packing.not.have.price', dontHavePrice);
  //   }
  //   return rows;
  // }

  showErrorMsg = (msgKey: string, rowIndex?: any[], msgParams?: any[]) => {
    let msg = this.translateService.instant(msgKey);
    if (msgParams) {
      msg = this.strFormat(msgKey, msgParams);
    }

    if (rowIndex && rowIndex.length > 0) {
      msg = '' + msg + rowIndex.join(', ');
    }
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.error(msg);
  }

  public strFormat(str: string, replacement: string[]) {
    let a = this.translateService.instant(str);
    if (replacement === null || replacement === undefined || replacement.length === 0) {
      return a;
    }

    for (const k of Object.keys(replacement)) {
      a = a.replace('{' + k + '}', this.translateService.instant(replacement[k]));
    }
    return a;
  }

  // getExceptProductPackingIds() {
  //   const packingIds = [-1];
  //   if (this.addEditForm.get('poDetails')?.value.length > 0) {
  //     this.addEditForm.get('poDetails')?.value.forEach((packing: any) => {
  //       if (packing.productPacking) {
  //         packingIds.push(packing.productPacking.id);
  //       }
  //     });
  //   }
  //
  //   return packingIds;
  // }

  async getFormSourceData(isInit?: boolean) {
    // manufacturer form control
    const manufacturerParams = new HttpParams()
      .set('text', '')
      .set('status', '1')
      .set('pageNumber', '1')
      .set('pageSize', '9999');
    const manufacturerPagingPromise = this.apiService.get('/manufacturers', manufacturerParams).toPromise();
    const storeParams = new HttpParams()
      .set('text', '')
      .set('status', '1')
      .set('is_DC', 'false')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('pageNumber', '1')
      .set('pageSize', '9999');
    const storePagingPromise = this.apiService.get('/stores', storeParams).toPromise();
    const distributorAllPromise = this.apiService.get('/distributors/all', new HttpParams()).toPromise();
    const currencyAllPromise = this.apiService.get('/currencies/all', new HttpParams()).toPromise();

    const manufacturerPaging = await manufacturerPagingPromise as Page;
    const storePaging = await storePagingPromise as Page;
    const distributorAll = await distributorAllPromise as DistributorModel[];
    const currencyAll = await currencyAllPromise as CurrencyModel[];

    // manufacturer
    let description: ManufacturerDescriptionModel;
    this.manufacturerOptions = manufacturerPaging.content.map((manufacturer: ManufacturerModel) => {
      description = manufacturer.manufacturerDescriptions?.filter(manufacturerDescriptionModel =>
        manufacturerDescriptionModel.language?.code === this.currentLangCode)[0] as ManufacturerDescriptionModel;
      return new SelectModel(manufacturer.id, manufacturer.code + ' - ' + description.name);
    });

    // store
    this.storeOptions = storePaging.content.map((store: StoreModel) =>
      new SelectModel(store.id, store.code + ' - ' + store.name)
    );

    // distributor
    this.distributorOptions = distributorAll.map(distributor =>
      new SelectModel(distributor.id, distributor.code + ' - ' + distributor.name)
    );

    // currency
    this.currencyOptions = currencyAll.map(currency =>
      new SelectModel(currency.id, currency.code + ' - ' + currency.name)
    );

    this.toCurrencyOptions = currencyAll.map(currency =>
      new SelectModel(currency.id, currency.code + ' - ' + currency.name)
    );
    this.addEditForm.get('toCurrency')?.setValue(currencyAll[currencyAll.map(currency => currency.code).indexOf('MMK')].id);
    this.addEditForm.get('fromCurrency')?.setValue(currencyAll[currencyAll.map(currency => currency.code).indexOf('USD')].id);

    if (isInit) {
      if (this.isEdit) {
        this.po = await this.apiService.get('/pos/' + this.parentId, new HttpParams()).toPromise() as PoModel;
        this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, this.po));
        if (this.po?.importPo?.importPoQcs && this.po.importPo.importPoQcs.length > 0) {
          const value = this.po.importPo.importPoQcs[0].url;
          const name = UploadModel.getFileName(value);
          const type = UploadModel.getFileType(value);
          const url = environment.BASE_FILE_URL + value;
          this.addEditForm.patchValue({
            resultQC: new UploadModel(name, null, url, type)
          });
        }
        if (this.po.poDetails && this.po.poDetails.length > 0) {
          const poDetailCurrencyId = this.po.poDetails[0].productPackingPrice?.currency?.id;
          this.addEditForm.get('fromCurrency')?.setValue(poDetailCurrencyId);

          this.updateAfterGetDetail(this.po);
          // this.addEditForm.markAllAsTouched();
          // this.po.poDetails.forEach((poDetail: any) => {
          //   // ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(poDetail.product);
          //   poDetail.poDetail = {id: poDetail.id, quantity: poDetail.quantity};
          //   poDetail.maxQuantity = poDetail.quantity;
          //   poDetail.productPackingPrices = poDetail.productPacking.productPackingPrices;
          // });
        }
      } else {
        this.addEditForm.patchValue({
          manufacturer: this.manufacturerOptions[0] ? this.manufacturerOptions[0].value : '',
          store: this.storeOptions[0] ? this.storeOptions[0].value : '',
          distributor: this.distributorOptions[0] ? this.distributorOptions[0].value : ''
        });
      }
    }
  }

  formSelectChange() {
    this.addEditForm.get('poDetails')?.setValue([]);
  }

  toCurrencyChange() {
    this.getCurrencyExchangeRate().then();
    // this.addEditForm.markAllAsTouched();
  }

  hasAuthority(): boolean {
    return this.authoritiesService.hasAuthority('get/currency-exchange-rate');
  }

  async getCurrencyExchangeRate() {
    if (this.addEditForm.get('fromCurrency')?.value === this.addEditForm.get('toCurrency')?.value) {
      this.addEditForm.get('exchangeRate')?.setValue(1);
    } else if (this.addEditForm.get('fromCurrency')?.value && this.addEditForm.get('toCurrency')?.value
      && this.hasAuthority()) {
      const params = new HttpParams()
        .set('fromCurrencyId', this.addEditForm.get('fromCurrency')?.value.toString())
        .set('toCurrencyId', this.addEditForm.get('toCurrency')?.value.toString())
        .set('fromDate', '' + this.dateUtilService.convertDateToStringGMT0(DateUtils.getCurrentDate()))
        .set('toDate', '' + this.dateUtilService.convertDateToStringGMT0(DateUtils.getDayBeforeNow(-1)))
        .set('pageSize', '10')
        .set('pageNumber', '1');
      const page = await this.apiService.get('/currency-exchange-rate', params).toPromise() as Page;
      if (page.content.length > 0) {
        this.exchangeRate = page.content[0];
      } else {
        this.exchangeRate = undefined;
        this.showErrorMsg('common.exchange.rate.not.found');
      }
      this.addEditForm.get('exchangeRate')?.setValue(this.exchangeRate ? this.exchangeRate.exchangeRate : 0);
    }
  }

  async fromCurrencyChange() {
    this.getCurrencyExchangeRate().then();
    const dataTable = this.addEditForm.get('poDetails')?.value as any[];
    let index = 0;
    const dontHavePrice: number[] = [];
    dataTable.forEach(row => {
      index++;
      let isExist = false;
      if (row.productPackingPrices) {
        for (const packingPrice of row.productPackingPrices) {
          if (packingPrice.currency.id === this.addEditForm.get('fromCurrency')?.value) {
            row.productPackingPrice = packingPrice;
            isExist = true;
            break;
          } else {
            row.productPackingPrice = {price: '', currency: {name: ''}};
          }
        }
      } else {
        row.productPackingPrice = {price: '', currency: {name: ''}};
      }
      if (!isExist) {
        dontHavePrice.push(index);
      }

    });
    // this.showWarningMsg('currency.not.exist');
    if (dontHavePrice.length > 0) {
      this.showErrorMsg('packing.not.have.price', dontHavePrice);
    }

    this.addEditForm.get('poDetails')?.setValue(dataTable);
  }

  apiCall = (): Observable<any> => {

    // this.addEditForm.addControl('status', new FormControl(this.po ? this.po.status : PoStatus.NEW));
    console.log(this.addEditForm.get('poDetails')?.value)
    this.tempPoDetailsValue = JSON.parse(JSON.stringify(this.addEditForm.get('poDetails')?.value));
    const tempPo = new PoModel(this.addEditForm);
    CommonUtils.reduceCircleReferenceObject(tempPo);
    // this.formData.deliveryDate = this.formData.deliveryDate.replace('T', ' ');
    if (this.actionType === ActionTypeEneum.new) {
      return this.apiService.post('/pos', [tempPo]);
    } else if (this.actionType === ActionTypeEneum.editPo) {
      return this.apiService.patch('/pos/' + this.parentId, tempPo);
    }

    return of(null);
  }

  onErrorFunc = (err: any) => {
    this.utilsService.showErrorToarst(err);
    this.addEditForm.patchValue({
      poDetails: this.tempPoDetailsValue
    });
  }

  onSuccessFunc = (data: any, onSuccessMessage?: string) => {
    this.utilsService.onSuccessFunc(onSuccessMessage);
    this.addEditForm.patchValue({
      poDetails: this.tempPoDetailsValue
    });
    setTimeout(() => {
      this.location.back();
    }, 500);
  };

  onSave() {
    this.utilsService.executeWithErrorHandle(this.apiCall, this.onSuccessFunc, '.add.success', 'common.confirmSave', ['common.PO'], '', [], this.onErrorFunc);
  }

  onDisplayWithAuthority(authorities: any): boolean {
    if (typeof (authorities) === 'string') {
      return this.authoritiesService.hasAuthority(authorities);
    }
    if (typeof (authorities) === 'object') {
      return this.authoritiesService.hasAuthorities(authorities);
      // let hasAuthority = false;
      // for (const authority of authorities) {
      //   hasAuthority = hasAuthority || this.authoritiesService.hasAuthority(authority);
      //   if (hasAuthority) {
      //     return hasAuthority;
      //   }
      // }
    }
    return false;
  }

  isShowButton(btn: string, action: string | null) {
    if (action) {
      switch (action) {
        case this.ActionType.new:
          return [ActionTypeEneum.new, ActionTypeEneum.del, 'save'].includes(btn);
        case this.ActionType.editPo:
          if (poStatusAction.isNewStatus(this.po)) {
            return ['add', 'delete', 'save', 'approve', 'rejectApprove', 'printPO'].includes(btn);
          } else if (poStatusAction.isApprovedStatus(this.po)) {
            return ['rejectApprove'].includes(btn);
          }
          return false;
        case this.ActionType.GR:
          return ['GR', 'printPO'].includes(btn) && poStatusAction.isArrivedMyanmarPortStatus(this.po);
        default:
          return false;

      }
    }
  }

  onApprove() {
    if (!poStatusAction.isNewStatus(this.po) || this.actionType !== ActionTypeEneum.view) {
      return;
    }
    let method;
    method = this.apiService.patch('/pos/approve/' + this.parentId, {});

    this.utilsService.execute(method, this.onSuccessFunc, '.add.success', 'common.confirmApprove', ['common.PO']);

  }

  onRejectApprove() {
    if ((!poStatusAction.isNewStatus(this.po)
      && !poStatusAction.isApprovedStatus(this.po)
      && !poStatusAction.isArrivedMyanmarPortStatus(this.po)
      && !poStatusAction.isArrivedVietnamPortStatus(this.po))
      || this.actionType !== ActionTypeEneum.view) {
      return;
    }
    const textMsg = 'reject.reason';

    const strOk = this.translateService.instant('common.OK');
    const strCancel = this.translateService.instant('common.Cancel');
    const dialogConfig: MatDialogConfig<any> = {
      data: {
        customClass: 'reject_reason_dialog',
        msg: this.translateService.instant('reject.reason'),
        msgDetail: '',
        type: DialogTypeEnum.INPUT_CONFIRM,
        btnOKString: strOk,
        btnCancelString: strCancel,
        hideIconButton: true
      } as NsCustomDialogDataConfig
    };
    this.utilsService.showConfirmInputDialog('reject.reason', [], dialogConfig)
      .afterClosed().subscribe((result: any) => {
      if (result?.value) {
        this.rejectFunction(result.value).subscribe((response: any) => {
          this.onSuccessFunc(response, this.poStatusAction.isNewStatus(this.po) ? 'po.status.approved.refuse.success' : 'po.status.cancelled.success');
        }, (errorMsg: any) => {
          this.showError(errorMsg);
        });
      }
    });
    // this.showInputConfirmDialog(textMsg, this.rejectFunction,
    //   this.onSuccessFunc, 'po.status.approved.refuse.success', this.showError);

  }

  rejectFunction = (rejectReason: any) => {
    return this.apiService.patch('/pos/reject/' + this.parentId, {rejectReason});
  }

  public showError = (err: string): void => {
    this.utilsService.showError(err);
  }


  printPO = (data: any) => {
    this.apiService.saveFile(`/pos/print/${this.parentId}`, null, {
      headers: undefined,
      params: undefined
    });
  }

  onPrintPO() {
    this.utilsService.execute(null, this.printPO, '', 'common.confirmPrint', ['common.PO']);
  }

  onGRPO() {

    const map = new Map();
    let key = '';

    this.addEditForm.addControl('po', new FormControl(this.po?.id));
    this.addEditForm.addControl('status', new FormControl(ImportPoStatus.NEW));
    // this.addEditForm.addControl('importPoDetail', new FormControl(this.po.poDetails));
    const importPo = new ImportPoModel(this.addEditForm);
    const warningThresholdRowIndexArr: any[] = [];
    let dateOfManufacture;
    let expireDateChoose;
    let warningThresholdDate;
    let rowIndex = 0;
    this.addEditForm.get('poDetails')?.value.forEach((detail: any) => {
      // if (!detail.expireDate) {
      //   this.showWarningMsg('po.ae.expireDate.require');
      // }
      rowIndex++;
      expireDateChoose = DateUtils.getDateFromString_0H(detail.expireDate);
      dateOfManufacture = DateUtils.getDayBeforeDate(expireDateChoose.toString(), Number(detail.product.lifecycle));
      warningThresholdDate = DateUtils.getDayAfterDate(dateOfManufacture.toString(), Number(detail.product.warningThreshold));

      if (DateUtils.getCurrentDate_0H() >= warningThresholdDate && expireDateChoose > DateUtils.getCurrentDate_0H()) {
        warningThresholdRowIndexArr.push(rowIndex);
      }


      const expireDate = detail.expireDate.split(' ')[0];

      key = '' + expireDate + detail.productPacking.id;

      if (map.has(key)) {
        const detailExpireDate = map.get(key);
        detailExpireDate.quantity = Number(detailExpireDate.quantity) + Number(detail.quantity);
      } else {
        map.set(key, {
          expireDate: detail.expireDate && detail.expireDate.includes('T') ? detail.expireDate.replace('T', ' ')
            : detail.expireDate,
          product: {id: detail.product.id},
          productPacking: {id: detail.productPacking.id},
          productPackingPrice: {id: detail.productPackingPrice.id},
          poDetail: {id: detail.id},
          quantity: detail.quantity
        });
      }
    });
    console.log(map.values());
    let msg = 'po.confirm.gr';
    const msgParam = [];
    if (warningThresholdRowIndexArr.length > 0) {
      msg = 'po.confirm.gr.with.warning.threshold';
      if (warningThresholdRowIndexArr.length > 3) {
        msgParam.push(warningThresholdRowIndexArr.slice(0, 2).join(', '));
      } else {
        msgParam.push(warningThresholdRowIndexArr.join(', '));
      }
    }

    importPo.importPoDetails = [...map.values()];
    CommonUtils.reduceCircleReferenceObject(importPo);
    const fileResult = this.addEditForm.get('resultQC')?.value;
    const formData = new FormData();
    formData.append('source', this.utilsService.toBlobJon(importPo));
    formData.append('files', fileResult[0] ? fileResult[0].binary : fileResult.binary);
    const method = this.apiService.post('/import-pos', formData);

    this.utilsService.execute(method, this.onSuccessFunc, '.gr.success', msg, msgParam);

  }

  onClaimDetail() {
    if (this.claimId) {
      this.router.navigate(['/claim/dashboard/' + this.claimId]).then();
    } else {
      this.showErrorMsg('claim.wait.approve.please');
    }
  }

  showWarningMsg = (msgKey: string) => {
    const msg = this.translateService.instant(msgKey);
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.warning(msg);
  }
  minDeliveryDate = () => {
    return (this.actionType === ActionTypeEneum.new || (this.actionType === ActionTypeEneum.edit &&
      !(this.actionType === ActionTypeEneum.edit && poStatusAction.isApprovedStatus(this.po))))
      ? DateUtils.getDayBeforeNow(-1) : null;
  }
  minVietnamPortDate = () => (poStatusAction.isApprovedStatus(this.po) && !this.disabledArrivedPort('vietnamPort'))
    ? DateUtils.getDayBeforeNow(-1) : null
  minMyanmarPortDate = () => this.po &&
  (poStatusAction.isApprovedStatus(this.po) || poStatusAction.isArrivedVietnamPortStatus(this.po)) && !this.disabledArrivedPort('myanmarPort')
    ? DateUtils.getDayBeforeNow(-1) : null
  minFCArrivedDate = () => this.po &&
  (poStatusAction.isArrivedVietnamPortStatus(this.po) || poStatusAction.isArrivedMyanmarPortStatus(this.po)) && !this.disabledArrivedPort('fc')
    ? DateUtils.getDayBeforeNow(-1) : null

  showInputConfirmDialog(titleMsg: string, method: any,
                         onSuccessFunc: (data: any, onSuccessMessage: string) => void, messageSuccess: string,
                         onErrorFunc: (msg: any) => void) {
    this.utilsService.showConfirmInputDialog(titleMsg ? titleMsg : '', [])
      .afterClosed().subscribe((result: any) => {
      if (result?.value) {
        method(result.value).subscribe((response: any) => {
          onSuccessFunc(response, messageSuccess);
        }, (errorMsg: any) => {
          onErrorFunc(errorMsg);
        });
      }
    });
  }

  updateArrivedDate() {
    let method;
    if (this.authoritiesService.hasAuthority('patch/pos/{id}/arrived-vietnam-port')) {
      method = this.apiService.patch(`/pos/${this.parentId}/arrived-vietnam-port`,
        {arrivedVietnamPortDate: this.addEditForm.get('arrivedVietnamPortDate')?.value});
    } else if (this.authoritiesService.hasAuthority('patch/pos/{id}/arrived-myanmar-port')) {
      method = this.apiService.patch(`/pos/${this.parentId}/arrived-myanmar-port`,
        {arrivedMyanmarPortDate: this.addEditForm.get('arrivedMyanmarPortDate')?.value});
    } else if (this.authoritiesService.hasAuthority('patch/pos/{id}/arrived-fc')) {
      method = this.apiService.patch(`/pos/${this.parentId}/arrived-fc`,
        {arrivedFcDate: this.addEditForm.get('arrivedFcDate')?.value});
    }

    this.utilsService.execute(method, this.onSuccessFunc,
      '.edit.success', 'common.confirmSave', ['common.arrived.date.param']);
  }

  onDisplayArrivedDate() {
    const hasUpdate = (this.onDisplayWithAuthority('patch/pos/{id}/arrived-vietnam-port')
      && poStatusAction.isApprovedStatus(this.po))
      || (this.onDisplayWithAuthority('patch/pos/{id}/arrived-myanmar-port')
        && (poStatusAction.isApprovedStatus(this.po) || poStatusAction.isArrivedVietnamPortStatus(this.po)))
      || (this.onDisplayWithAuthority('patch/pos/{id}/arrived-fc')
        && (poStatusAction.isArrivedVietnamPortStatus(this.po) || poStatusAction.isArrivedMyanmarPortStatus(this.po)));
    return hasUpdate;
  }

  disabledArrivedPort(location: 'vietnamPort' | 'myanmarPort' | 'fc') {
    let disabled;
    switch (location) {
      case 'vietnamPort': {
        disabled = (!this.onDisplayWithAuthority('patch/pos/{id}/arrived-vietnam-port')
          || this.actionType !== ActionTypeEneum.deliveryDate
          || !poStatusAction.isApprovedStatus(this.po));

        if (!disabled && this.addEditForm) {
          this.addEditForm.get('arrivedVietnamPortDate')?.setValidators(Validators.required);
          this.addEditForm.get('arrivedVietnamPortDate')?.updateValueAndValidity();
        } else {
          this.addEditForm.get('arrivedVietnamPortDate')?.setValidators(null);
        }
        break;
      }
      case 'myanmarPort':
        disabled = !this.onDisplayWithAuthority('patch/pos/{id}/arrived-myanmar-port')
          || !this.addEditForm.get('arrivedVietnamPortDate')?.value
          || this.actionType !== ActionTypeEneum.deliveryDate
          || (!poStatusAction.isApprovedStatus(this.po) && !poStatusAction.isArrivedVietnamPortStatus(this.po));
        if (!disabled && this.addEditForm) {
          this.addEditForm.get('arrivedMyanmarPortDate')?.setValidators(Validators.required);
          this.addEditForm.get('arrivedMyanmarPortDate')?.updateValueAndValidity();
        } else {
          this.addEditForm.get('arrivedMyanmarPortDate')?.setValidators(null);
        }
        break;
      case 'fc':
        disabled = !this.onDisplayWithAuthority('patch/pos/{id}/arrived-fc')
          || !this.addEditForm.get('arrivedMyanmarPortDate')?.value
          || this.actionType !== ActionTypeEneum.deliveryDate
          || (!poStatusAction.isArrivedVietnamPortStatus(this.po) && !poStatusAction.isArrivedMyanmarPortStatus(this.po));
        if (!disabled && this.addEditForm) {
          this.addEditForm.get('arrivedFcDate')?.setValidators(Validators.required);
          this.addEditForm.get('arrivedFcDate')?.updateValueAndValidity();
        } else {
          this.addEditForm.get('arrivedFcDate')?.setValidators(null);
        }
        break;
    }

    return disabled;
  }

  get poConst() {
    return poConst;
  }

  isEmptyDetails() {
    return this.addEditForm.get('poDetails')?.value ? this.addEditForm.get('poDetails')?.value.length === 0 : false;
  }

  updateBreadcrumb(event: any) {
    switch (event.label) {
      case 'pov2.label.update':
        switch (this.actionType + '') {
          case ActionTypeEneum.editPo.toString():
            event.label = this.translateService.instant('pov2.label.update');
            break;
          case ActionTypeEneum.GR.toString():
            event.label = this.translateService.instant('pov2.label.gr');
            break;
          case ActionTypeEneum.view.toString():
            event.label = this.translateService.instant('pov2.label.detail');
            break;
          case ActionTypeEneum.view_from_claim.toString():
            event.label = this.translateService.instant('pov2.label.detail');
            break;
          case ActionTypeEneum.deliveryDate.toString():
            event.label = this.translateService.instant('pov2.label.deliveryDate');
            break;
        }
        break;
    }
  }

  onBack() {
    this.location.back();
  }

  getReadOnly() {
    return (this.isEdit && (!poStatusAction.isNewStatus(this.po) || !this.poAuthoritiesService.hasCreateOrUpdatePoRole()))
      || this.actionType === ActionTypeEneum.view;
  }
}
