import {Component, Inject, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  DateUtilService,
  FileTypes,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {PoModel} from '../../../../_models/po/po.model';
import {ActionTypeEneum, PoStatus} from '../../../../_models/action-type';
import {Pov2Service} from '../../pov2.service';
import {poConst, poStatusAction} from 'src/app/_models/po/po.const';
import {PoAuthoritiesService} from '../../../../_services/authority/po.authorities.service';
import {CommonUtils} from '../../../../_utils/common.utils';

@Component({
  selector: 'app-add-edit-po-manufacturer',
  templateUrl: './add-edit-po-manufacture.component.html',
  styleUrls: ['./add-edit-po-manufacture.component.css']
})
export class AddEditPoManufactureComponent extends BaseAddEditLayout implements OnInit, OnChanges {

  @Input() po?: PoModel;
  parentId: string | null;
  currentLangCode: string;
  actionType: string | null | ActionTypeEneum;
  manufacturerOptions?: SelectModel[] = [];
  storeOptions?: SelectModel[] = [];
  distributorOptions?: SelectModel[] = [];
  currencyOptions: SelectModel[] = [];
  toCurrencyOptions: SelectModel[] = [];
  @Input() form?: FormGroup;
  @Input() isReadOnly = false;
  @Input() hasClaim = false;

  get poStatusAction() {
    return poStatusAction;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              private dateUtilService: DateUtilService,
              protected authoritiesService: AuthoritiesService,
              public poAuthoritiesService: PoAuthoritiesService,
              @Inject(Pov2Service) protected poService: Pov2Service,
  ) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);

    // this.poService.getAllDistributor();
    this.parentId = this.activatedRoute.snapshot.paramMap.get('id');
    this.actionType = this.activatedRoute.snapshot.paramMap.get('actType');
    this.currentLangCode = this.translateService.currentLang ?
      this.translateService.currentLang : this.translateService.defaultLang;
  }

  minArriveVietnamPortDate = () => {
    const approvedDate = this.dateUtilService.convertToDateIgnoreTimeZone(this.po?.approveDate);
    const now = this.dateUtilService.getDateNow();
    const minDate = approvedDate ? now.getTime() > approvedDate.getTime() ? now : approvedDate : now;
    return (this.po && this.po.status === PoStatus.APPROVED && !this.disabledArrivedPort('vietnamPort'))
      ? this.dateUtilService.addDays(minDate, 1) : null;
  }

  minArrivedVietnamPortDate = () => {
    const approvedDate = this.dateUtilService.convertToDateIgnoreTimeZone(this.po?.approveDate);
    return (this.po && this.po.status === PoStatus.APPROVED && !this.disabledArrivedPort('vietnamPort')) && approvedDate
      ? this.dateUtilService.addDays(approvedDate, 1) : null;
  }

  maxArrivedVietnamPortDate = () => this.dateUtilService.getDateNow();
  maxArrivedMyanmarPortDate = () => this.dateUtilService.getDateNow();

  minArrivedMyanmarPort = () => {
    const arrivedVNPort = this.po?.arrivedVietnamPortDate ?
      this.dateUtilService.convertToDateIgnoreTimeZone(this.po.arrivedVietnamPortDate) : null;
    return this.po &&
    (this.po.status === PoStatus.APPROVED || this.po.status === PoStatus.ARRIVED_VIETNAM_PORT) && !this.disabledArrivedPort('myanmarPort')
      ? (arrivedVNPort ? this.dateUtilService.addDays(arrivedVNPort, 1) : null) : null;
  }

  minArriveMyanmarPortDate = () => {
    const approvedDate = this.dateUtilService.convertToDateIgnoreTimeZone(this.po?.approveDate);
    const now = this.dateUtilService.getDateNow();
    const minDate = approvedDate ? now.getTime() > approvedDate.getTime() ? now : approvedDate : now;
    return this.po &&
    (this.po.status === PoStatus.APPROVED || this.po.status === PoStatus.ARRIVED_VIETNAM_PORT)
    && !this.disabledArrivedPort('myanmarPort')
      ? this.dateUtilService.addDays(minDate, 1) : null;
  }


  minArriveFCDate = () => this.po &&
  (this.po.status === PoStatus.ARRIVED_VIETNAM_PORT || this.po.status === PoStatus.ARRIVED_MYANMAR_PORT)
  && !this.disabledArrivedPort('fc')
    ? this.dateUtilService.addDays(this.dateUtilService.getDateNow(), 1) : null

  // minFCArrivedDate = () => this.po &&
  // (this.po.status === PoStatus.ARRIVED_VIETNAM_PORT || this.po.status === PoStatus.ARRIVED_MYANMAR_PORT) && !this.disabledArrivedPort('fc')
  //   ? DateUtils.getDayBeforeNow(-1) : null

  isUpdateDeliveryDate() {
    return this.isEdit && this.actionType === ActionTypeEneum.deliveryDate;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.poService.getManufacture(this.currentLangCode, (value: any) => {
      this.manufacturerOptions = value;
    });

    this.poService.getStore((value: any) => {
      this.storeOptions = value;
    });
    this.poService.getAllDistributor((value: any) => {
      this.distributorOptions = value;
    });
    this.poService.getAllCurrencies((value: any) => {
      this.currencyOptions = value;
      this.toCurrencyOptions = value;
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('hasClaim' in changes) {
      this.hasClaim = changes.hasClaim.currentValue;
    }
    if ('isReadOnly' in changes) {
      this.isReadOnly = changes.isReadOnly.currentValue;
    }
  }

  updateArriveDate(uri: string) {
    if (!this.po || !this.po.id || !this.form) {
      return;
    }
    let obj = {};
    if (uri === poConst.ARRIVED_VIETNAM_PORT.expect_uri) {
      obj = {arriveVietnamPortDate: this.form.value.arriveVietnamPortDate};
    } else if (uri === poConst.ARRIVED_VIETNAM_PORT.actual_uri) {
      obj = {arrivedVietnamPortDate: this.form.value.arrivedVietnamPortDate};
    } else if (uri === poConst.ARRIVED_MYANMAR_PORT.expect_uri) {
      obj = {arriveMyanmarPortDate: this.form.value.arriveMyanmarPortDate};
    } else if (uri === poConst.ARRIVED_MYANMAR_PORT.actual_uri) {
      obj = {arrivedMyanmarPortDate: this.form.value.arrivedMyanmarPortDate};
    } else if (uri === poConst.IMPORTED.expect_uri) {
      obj = {arriveFcDate: this.form.value.arriveFcDate};
    }

    // const tempPo = new PoModel(this.form);
    // CommonUtils.reduceCircleReferenceObject(tempPo);
    const method = this.poService.patchArrDate(uri, this.po.id, obj);
    this.utilsService.execute(method, this.onSuccessFunc,
      '.edit.success', 'common.confirmSave', ['common.arrived.date.param']);
  }

  get ActionType() {
    return ActionTypeEneum;
  }

  disabledArrivedPort(location: 'vietnamPort' | 'myanmarPort' | 'fc') {
    let disabled;
    switch (location) {
      case 'vietnamPort': {
        disabled = (!this.onDisplayWithAuthority('patch/pos/{id}/arrived-vietnam-port')
          || this.actionType !== ActionTypeEneum.deliveryDate
          || (this.po && this.po.status !== PoStatus.APPROVED));

        if (!disabled && this.form) {
          this.form?.get('arrivedVietnamPortDate')?.setValidators(Validators.required);
          this.form?.get('arrivedVietnamPortDate')?.updateValueAndValidity();
        } else {
          this.form?.get('arrivedVietnamPortDate')?.setValidators(null);
        }
        break;
      }
      case 'myanmarPort':
        disabled = !this.onDisplayWithAuthority('patch/pos/{id}/arrived-myanmar-port')
          || !this.form?.get('arrivedVietnamPortDate')?.value
          || this.actionType !== ActionTypeEneum.deliveryDate
          || (this.po && this.po.status !== PoStatus.APPROVED && this.po.status !== PoStatus.ARRIVED_VIETNAM_PORT);
        if (!disabled && this.form) {
          this.form?.get('arrivedMyanmarPortDate')?.setValidators(Validators.required);
          this.form?.get('arrivedMyanmarPortDate')?.updateValueAndValidity();
        } else {
          this.form?.get('arrivedMyanmarPortDate')?.setValidators(null);
        }
        break;
      case 'fc':
        disabled = !this.onDisplayWithAuthority('patch/pos/{id}/arrive-fc')
          || !this.form?.get('arrivedMyanmarPortDate')?.value
          || this.actionType !== ActionTypeEneum.deliveryDate
          || (this.po && this.po.status !== PoStatus.ARRIVED_VIETNAM_PORT && this.po.status !== PoStatus.ARRIVED_MYANMAR_PORT);
        if (!disabled && this.form) {
          this.form?.get('arrivedFcDate')?.setValidators(Validators.required);
          this.form?.get('arrivedFcDate')?.updateValueAndValidity();
        } else {
          this.form?.get('arrivedFcDate')?.setValidators(null);
        }
        break;
    }

    return disabled;
  }

  onDisplayWithAuthority(authorities: any): boolean {
    if (typeof (authorities) === 'string') {
      return this.authoritiesService.hasAuthority(authorities);
    }
    if (typeof (authorities) === 'object') {
      let hasAuthority = false;
      for (const authority of authorities) {
        hasAuthority = hasAuthority || this.authoritiesService.hasAuthority(authority);
        if (hasAuthority) {
          return hasAuthority;
        }
      }
    }
    return false;
  }

  hasUpdateDateRole(name: string) {
    if (this.po && this.po[name]) {
      return true;
    }
    return false;
  }

  test(po?: PoModel) {
    console.log('poooooooooooooooooo', po, !!po);

  }

  get poConst() {
    return poConst;
  }

  get PoStatus() {
    return PoStatus;
  }

  get FileTypes() {
    return FileTypes;
  }

  onManufacturerChange($event: any) {
    this.poService.productPackingMap.clear();
    this.form?.patchValue({
      poDetails: []
    });
  }

  onDistributorChange($event: any) {
    this.poService.productPackingMap.clear();
    this.form?.patchValue({
      poDetails: []
    });
  }
}
