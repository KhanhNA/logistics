import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPoManufactureComponent } from './add-edit-po-manufacture.component';

describe('AddEditPoManufactureComponent', () => {
  let component: AddEditPoManufactureComponent;
  let fixture: ComponentFixture<AddEditPoManufactureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditPoManufactureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPoManufactureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
