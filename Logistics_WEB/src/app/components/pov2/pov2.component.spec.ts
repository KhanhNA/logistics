import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pov2Component } from './pov2.component';

describe('Pov2Component', () => {
  let component: Pov2Component;
  let fixture: ComponentFixture<Pov2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pov2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pov2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
