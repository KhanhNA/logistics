import {RouterModule, Routes} from '@angular/router';
import {AuthoritiesResolverService} from '@next-solutions/next-solutions-base';
import {NgModule} from '@angular/core';

import {Pov2Component} from './pov2.component';
import {AddEditPoV2Component} from './add-edit-po/add-edit-po-v2.component';


const routes: Routes = [
  {
    pathMatch: 'full',
    path: '',
    resolve: {me: AuthoritiesResolverService},
    component: Pov2Component
  },
  {

    path: ':actType',
    data: {breadcrumb: 'pov2.label.create'},
    resolve: {me: AuthoritiesResolverService},
    component: AddEditPoV2Component
  },
  {

    path: ':actType/:id',
    resolve: {me: AuthoritiesResolverService},
    data: {breadcrumb: 'pov2.label.update'},
    component: AddEditPoV2Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Pov2RoutingModule {

}
