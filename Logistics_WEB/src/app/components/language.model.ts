export class Language {
  id: number | undefined;
  code: string | undefined;
  name: string | undefined;
}
