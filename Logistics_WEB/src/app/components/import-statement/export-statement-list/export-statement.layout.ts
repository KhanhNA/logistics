import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {HttpParams} from '@angular/common/http';
import {ExportStatementStatus} from '../../../_models/action-type';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

// @Directive()
export class ExportStatementLayout extends LDBaseLayout {

  tblCols: FieldConfigExt[] = [
    new FieldConfigExt({
      name: 'stt',
      type: 'index',
      label: 'ld.no',
      inputType: 'input'
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'code',
      label: 'common.export.code',
      inputType: 'input'
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'fromStore.name',
      label: 'common.fromStore',
      inputType: 'input',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'toStore.name',
      label: 'common.toStore',
      inputType: 'input',
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'description',
      label: 'common.description',
      inputType: 'input',
    }),

    new FieldConfigExt({
      type: 'btnGrp',
      name: 'add',
      value: ['dashboard'],
      authorities: {
        dashboard: ['post/import-statements']
      },
      titles: {
        dashboard: 'common.title.dashboard'
      }
    })
  ];

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, translateService, toastr, location, authoritiesService);
    this.setCols(this.tblCols);
  }

  updateSearchInfo(search?: any) {
    const params = new HttpParams()
      .set('code', '')
      .set('toStoreId', '')
      .set('createDate', '')
      .set('status', ExportStatementStatus.RELEASED.toString())
      .set('pageNumber', (search.pageNumber + 1).toString())
      .set('pageSize', search.pageSize.toString())
      .set('type', 'to');
    search.method = this.apiService.get('/export-statements', params);
  }

  beforeUpdate = (data: any) => {
    for (const exportStatement of data.content) {
      exportStatement.fromStore = {
        name: exportStatement.fromStore.code + ' - ' + exportStatement.fromStore.name
      };
      if (exportStatement.toStore === null || exportStatement.toStore === undefined) {
        exportStatement.toStore = {
          name: ''
        };
      } else {
        exportStatement.toStore = {
          name: exportStatement.toStore.code + ' - ' + exportStatement.toStore.name
        };
      }
    }
  }


}
