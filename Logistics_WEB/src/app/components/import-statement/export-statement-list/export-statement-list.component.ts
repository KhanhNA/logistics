import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import {NavService} from '../../../_services/nav.service';
import {ActionTypeEneum} from '../../../_models/action-type';
import {ExportStatementLayout} from './export-statement.layout';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-export-statement-list',
  templateUrl: './export-statement-list.component.html',
  styleUrls: ['./export-statement-list.component.scss', '../../table.layout.scss']
})
export class ExportStatementListComponent implements OnInit, AfterViewInit {
  model: ExportStatementLayout;


  exportStatement: any;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService, private dialog: MatDialog,
              private navService: NavService,
              private router: Router,
              protected authoritiesService: AuthoritiesService) {
    this.model = new ExportStatementLayout(tsuService, fb, route, translateService, toastr, location, apiService, authoritiesService);
  }

  ngOnInit() {
    this.model.onSearchData();
    this.model.actionType = ActionTypeEneum.new;

  }

  ngAfterViewInit(): void {
    this.navService.title = 'menu.import.statement.create.from.export';
  }

  onRowClick(event: any) {
    this.exportStatement = event.row;
  }

  onPage(page?: any) {
    this.model.onPage(page);
  }

  onCellEvent(event: any) {

    this.router.navigate(['/import-statement', 'add', event.row.id]);
  }
}
