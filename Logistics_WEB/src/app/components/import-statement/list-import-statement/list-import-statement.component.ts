import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  DateUtilService,
  FormStateService,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ImportStatementModel} from '../../../_models/import-statement/import.statement.model';
import {ImportStatementStatusEnum} from '../../../_models/enums/ImportStatementStatusEnum';
import {HttpParams} from '@angular/common/http';
import {NavService} from '../../../_services/nav.service';
import {ActionTypeEneum} from '../../../_models/action-type';

@Component({
  selector: 'app-list-import-statement',
  templateUrl: './list-import-statement.component.html',
})
export class ListImportStatementComponent extends BaseSearchLayout implements OnInit{

  moduleName = 'import.statement';

  buttons: ButtonFields[] = [];

  statusValues: SelectModel[] = [];

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private dateUtilService: DateUtilService,
              private navService: NavService) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        status: ['_0'],
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code',
        header: 'code',
        title: (e: ImportStatementModel) => `${e.code}`,
        cell: (e: ImportStatementModel) => `${e.code}`,
      },
      {
        columnDef: 'totalQuantity',
        header: 'totalQuantity',
        title: (e: ImportStatementModel) => `${e.totalQuantity ? e.totalQuantity.toLocaleString('en-US') : ''}`,
        cell: (e: ImportStatementModel) => `${e.totalQuantity ? e.totalQuantity.toLocaleString('en-US') : ''}`,
        align: AlignEnum.RIGHT
      },
      {
        columnDef: 'totalVolume',
        header: 'totalVolume',
        title: (e: ImportStatementModel) => `${e.totalVolume ? (Number(e.totalVolume) / 1000).toLocaleString('en-US') : 0}`,
        cell: (e: ImportStatementModel) => `${e.totalVolume ? (Number(e.totalVolume) / 1000).toLocaleString('en-US') : 0}`,
        align: AlignEnum.RIGHT
      },
      {
        columnDef: 'estimatedTimeOfArrival',
        header: 'estimatedTimeOfArrival',
        title: (e: ImportStatementModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.estimatedTimeOfArrival ? e.estimatedTimeOfArrival : '')}`,
        cell: (e: ImportStatementModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.estimatedTimeOfArrival ? e.estimatedTimeOfArrival : '')}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'description',
        header: 'description',
        title: (e: ImportStatementModel) => `${e.description ? e.description : ''}`,
        cell: (e: ImportStatementModel) => `${e.description ? e.description : ''}`,
        display: (e: ImportStatementModel) => !!e.description
      },
      {
        columnDef: 'exportStatement.code',
        header: 'exportStatement.code',
        title: (e: ImportStatementModel) => `${e.exportStatement ? e.exportStatement.code : ''}`,
        cell: (e: ImportStatementModel) => `${e.exportStatement ? e.exportStatement.code : ''}`,
      },
      {
        columnDef: 'repackingPlanning.code',
        header: 'repackingPlanning.code',
        title: (e: ImportStatementModel) => `${e.repackingPlanning ? e.repackingPlanning.code : ''}`,
        cell: (e: ImportStatementModel) => `${e.repackingPlanning ? e.repackingPlanning.code : ''}`,
        display: (e: ImportStatementModel) => {
          return e.repackingPlanning ? true : false;
        }
      }
    );
  }

  async ngOnInit() {
    this.navService.title = 'menu.import.statement.list';
    Object.keys(ImportStatementStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(ImportStatementStatusEnum, key.replace('_', ''));
      this.statusValues.push(new SelectModel(key, value));
    });

    super.ngOnInit();
    this.onSubmit();
  }

  search() {
    const status = this.searchForm.get('status')?.value;
    const params = new HttpParams()
      .set('code', this.searchForm.get('text')?.value)
      .set('status', `${status}`.replace('_', ''))
      .set('type', 'to');
    this._fillData('/import-statements/find', params);
  }

  onRowClick(row: ImportStatementModel) {
    this.router.navigate(['/import-statement', ActionTypeEneum.view, row.id]);
  }
}
