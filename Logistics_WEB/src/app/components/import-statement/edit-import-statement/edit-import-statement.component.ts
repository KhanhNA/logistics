import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import {NavService} from '../../../_services/nav.service';
import {EditImportStatementDetailLayout} from './edit-import-statement-detail.layout';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-edit-import-statement',
  templateUrl: './edit-import-statement.component.html',
  styleUrls: ['./edit-import-statement.component.scss', '../../table.layout.scss'
    , '../../form.layout.scss']
})
export class EditImportStatementComponent implements OnInit, AfterViewInit {

  model: EditImportStatementDetailLayout;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService, private dialog: MatDialog,
              private navService: NavService,
              private router: Router,
              protected authoritiesService: AuthoritiesService) {
    this.model = new EditImportStatementDetailLayout(tsuService, fb, route,
      translateService, toastr, location, apiService, router, authoritiesService);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.model.importStatementId = params.id;
    });
  }

  ngAfterViewInit(): void {
    // if (this.model.actionType === ActionTypeEneum.edit) {
      this.navService.title = '';
    // } else if (this.model.actionType === ActionTypeEneum.view) {
    //   this.navService.title = 'menu.import.statement';
    // }
  }

  onUpdate() {
    this.model.onUpdate();
  }


  onPage(page?: any) {
    this.model.tsTable.removeRows(0, this.model.getTblData().length);
  }

  onCellEvent(event: any) {
    const tblData = this.model.getTblData();
    if (event.act === 'delete') {
      this.model.tsTable.removeRows(tblData.indexOf(event.row), 1);
    }
  }
}
