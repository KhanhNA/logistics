import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {ActionTypeEneum, ClaimEnum, ImportStatementStatus} from '../../../_models/action-type';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {HttpParams} from '@angular/common/http';
import {ClaimDetailModel} from '../../../_models/claim.detail.model';

// @Directive()
export class EditImportStatementDetailLayout extends LDBaseLayout {
  private orgQuantityCol = new FieldConfigExt({
    type: 'view',
    name: 'orgQuantity',
    label: 'common.quantity.org',
    inputType: 'number',
  });
  private claimDeliveryQuantityCol = new FieldConfigExt({
    type: 'view',
    name: 'claimDeliveryQuantity',
    label: 'common.quantity.claim.delivery',
    inputType: 'number',
  });

  private claimImportQuantityCol = new FieldConfigExt({
    type: 'view',
    name: 'claimImportQuantity',
    label: 'common.quantity.claim.import',
    inputType: 'number',
  });

  importStatementId: any;
  importStatement: any;
  title: any;
  private importStatementDetailPallets?: any[];
  private exportStatementDescriptionCol = new FieldConfigExt({
    type: 'input',
    label: 'common.export.description',
    inputType: 'text',
    name: 'exportStatement.description',
    require: 'false',
    binding: 'exportStatement.description'
  });
  hasClaimDelivery = false;
  hasClaimImport = false;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected activatedRoute: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService,
              private router: Router,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, activatedRoute, translateService, toastr, location, authoritiesService);
    this.setFormData();
    this.setCols(this.tblCols);
    this.init(this.inputFields);
    this.title = 'importStatement.detail';
  }

  inputFields = [
    new FieldConfigExt({
      type: 'input',
      binding: 'code',
      name: 'code',
      label: 'common.code',
      inputType: 'input',
      require: 'true',
      readonly: 'true'
    }),
    this.exportStatementDescriptionCol,
    new FieldConfigExt({
      type: 'input',
      label: 'common.import.description',
      inputType: 'text',
      name: 'description',
      binding: 'description'
    }),
    new FieldConfigExt({
      type: 'input',
      label: 'common.fromStore',
      inputType: 'text',
      name: 'fromStore.name',
      binding: 'fromStore.name',
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'fromStoreUsername',
      name: 'fromStoreUsername',
      value: '',
      label: 'store.user',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'fromStoreUserTel',
      name: 'fromStoreUserTel',
      label: 'store.phone',
      value: '',
      inputType: 'input',
      readonly: 'true'
    }),

    new FieldConfigExt({
      type: 'input',
      label: 'common.toStore',
      inputType: 'text',
      name: 'toStore.name',
      binding: 'toStore.name',
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'toStoreUsername',
      name: 'toStoreUsername',
      value: '',
      label: 'store.user',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'toStoreUserTel',
      name: 'toStoreUserTel',
      label: 'store.phone',
      value: '',
      inputType: 'input',
      readonly: 'true'
    }),

  ];

  tblCols: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'index',
      name: 'stt',
      label: 'ld.no',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.code',
      label: 'common.packing.code',
      inputType: 'input'
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'storeProductPackingDetail.qRCode',
      label: 'qr-code',
      inputType: 'fileImgBase64',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.product.name',
      label: 'common.product.name',
      inputType: 'input'
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'expireDate',
      label: 'common.expireDate',
      inputType: 'date'
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.packingType.quantity',
      label: 'productPacking.packingType.quantity',
      inputType: 'input'
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'quantity',
      label: 'common.quantity',
      inputType: 'number',
      // emit: this.changeQuantity,
      // validations: [
      //   {
      //     name: 'minQuantity',
      //     validator: NumberValidators.minQuantityValidation(-1),
      //     message: 'validation.>=0'
      //   },
      // ],
    }),
  ];

  setFormData() {
    let type = '';
    if (this.actionType === ActionTypeEneum.view) {
      this.activatedRoute.paramMap.subscribe((paramMap: any) => {
        type = paramMap.params.claimType;
      });

      const claimDeliveryMap = new Map<string, any>();
      const claimImportMap = new Map<string, any>();

      this.apiService.get('/import-statements/' + this.parentId, new HttpParams())
        .subscribe((importStatement: any) => {
          this.importStatement = importStatement;
          let key;
          if (!this.isHideClaimView()) {
            if (!this.isImportStatementFromRepackingPlanning()) {
              this.getClaimAsyncFromExport().then(response => {
                response.claimDelivery.then((claimDeliveryDetails: any) => {
                  if (claimDeliveryDetails && claimDeliveryDetails.length > 0) {
                    this.hasClaimDelivery = true;
                    claimDeliveryDetails.forEach((claimDetail: any) => {
                      key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
                      claimDetail.orgQuantity = claimDetail.quantity;
                      claimDetail.claimDeliveryQuantity = claimDetail.quantity;
                      claimDetail.claimImportQuantity = 0;
                      claimDetail.quantity = 0;
                      claimDetail.storeProductPackingDetail = {
                        qRCode: ''
                      };
                      claimDeliveryMap.set(key, claimDetail);
                    });
                  }
                });

                response.claimImport.then((claimImportDetails: any) => {
                  if (claimImportDetails && claimImportDetails.length > 0) {
                    this.hasClaimImport = true;
                    claimImportDetails.forEach((claimDetail: any) => {
                      key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
                      claimDetail.orgQuantity = claimDetail.quantity;
                      claimDetail.claimDeliveryQuantity = 0;
                      claimDetail.claimImportQuantity = claimDetail.quantity;
                      claimDetail.quantity = 0;
                      claimDetail.storeProductPackingDetail = {
                        qRCode: ''
                      };
                      claimImportMap.set(key, claimDetail);
                    });
                  }
                });
              }).then(() => {
                this.updateData(importStatement, claimDeliveryMap, claimImportMap);
              });
            } else {
              this.getClaimImportFromRepack().then((result: any) => {
                if (result.claimImportFromRepack.length > 0) {
                  this.hasClaimImport = true;
                  result.claimImportFromRepack.forEach((claimDetail: any) => {
                    key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
                    claimDetail.orgQuantity = claimDetail.quantity;
                    claimDetail.claimDeliveryQuantity = 0;
                    claimDetail.claimImportQuantity = claimDetail.quantity;
                    claimDetail.quantity = 0;
                    claimDetail.storeProductPackingDetail = {
                      qRCode: ''
                    };
                    claimImportMap.set(key, claimDetail);
                  });
                }

              }).then(() => {
                this.updateData(importStatement, new Map<string, number>(), claimImportMap);
              });
            }
          } else {
            this.updateData(importStatement, claimDeliveryMap, claimImportMap);
          }
        });


    }
  }

  isImportStatementFromRepackingPlanning(): boolean {
    return this.importStatement.fromStore.id === this.importStatement.toStore.id;
  }

  isHideClaimView(): boolean {
    return this.importStatement.status === ImportStatementStatus.NOT_UPDATE_INVENTORY
      && this.actionType === ActionTypeEneum.view;
  }

  async getClaimAsyncFromExport() {
    let hasErrorCallback = false;
    const paramsDelivery = new HttpParams()
      .set('type', ClaimEnum.DELIVERY)
      .set('referenceId', '' + this.parentId);

    const claimDelivery = this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', paramsDelivery).toPromise()
      .catch((errMsg: any) => {
        if (!hasErrorCallback) {
          // this.showErrorMsg(errMsg);
          hasErrorCallback = !hasErrorCallback;
        }
      });

    const paramsImport = new HttpParams()
      .set('type', ClaimEnum.IMPORT_STATEMENT_EXPORT_STATEMENT)
      .set('referenceId', '' + this.parentId);
    const claimImport = this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', paramsImport).toPromise()
      .catch((errMsg: any) => {
        if (!hasErrorCallback) {
          // this.showErrorMsg(errMsg);
          hasErrorCallback = !hasErrorCallback;
        }
      });

    await claimDelivery;
    await claimImport;

    return {claimDelivery, claimImport};
  }

  async getClaimImportFromRepack() {
    const paramsDelivery = new HttpParams()
      .set('type', ClaimEnum.IMPORT_STATEMENT_REPACKING_PLANNING)
      .set('referenceId', '' + this.parentId);

    const claimImportFromRepack = await this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', paramsDelivery).toPromise();
    return {claimImportFromRepack};
  }

  updateColsByAction() {
    const colsDisplay = [];
    for (const col of this.tblCols) {
      if (col.name === 'quantity') {
        if (this.hasClaimImport && this.hasClaimDelivery && !this.isHideClaimView()) {
          col.label = 'common.quantity.real.import';
          col.require = 'false';
          colsDisplay.push(
            this.orgQuantityCol,
            this.claimDeliveryQuantityCol,
            this.claimImportQuantityCol,
          );
        } else if (this.hasClaimDelivery && !this.isHideClaimView()) {
          col.label = 'common.quantity.real.import';
          col.require = 'false';
          colsDisplay.push(
            this.orgQuantityCol,
            this.claimDeliveryQuantityCol,
          );
        } else if (this.hasClaimImport && !this.isHideClaimView()) {
          col.label = 'common.quantity.real.import';
          col.require = 'false';
          colsDisplay.push(
            this.orgQuantityCol,
            this.claimImportQuantityCol,
          );
        }
      }
      colsDisplay.push(col);
    }
    this.tblCols = colsDisplay;
  }

  updateData(importStatement: any, claimDeliveryMap: Map<string, any>, claimImportMap: Map<string, any>) {
    if (!importStatement.exportStatement) {
      this.inputFields.splice(this.inputFields.indexOf(this.exportStatementDescriptionCol), 1);
      this.init(this.inputFields);
    }

    this.updateColsByAction();
    this.setCols(this.tblCols);

    this.formData = importStatement;

    importStatement.fromStore.name = importStatement.fromStore.code + ' - ' + importStatement.fromStore.name;
    importStatement.toStore.name = importStatement.toStore.code + ' - ' + importStatement.toStore.name;
    importStatement.fromStoreUsername = importStatement.fromStore.user ? importStatement.fromStore.user.username : '';
    importStatement.fromStoreUserTel = importStatement.fromStore.user ? importStatement.fromStore.user.tel : '';
    importStatement.toStoreUsername = importStatement.toStore.user ? importStatement.toStore.user.username : '';
    importStatement.toStoreUserTel = importStatement.toStore.user ? importStatement.toStore.user.tel : '';

    importStatement.content = importStatement.importStatementDetails;
    if (importStatement.status === ImportStatementStatus.NOT_UPDATE_INVENTORY) {

      let productPackingIdImport;
      let expireDateImport;
      let productPackingIdExport;
      let expireDateExport;
      const importStatementMap = new Map<string, any>();
      importStatement.content.forEach((importDetail: any) => {
        productPackingIdImport = importDetail.productPacking.id;
        expireDateImport = importDetail.expireDate;
        for (const exportStatementDetail of importStatement.exportStatement.exportStatementDetails) {
          productPackingIdExport = exportStatementDetail.productPacking.id;
          expireDateExport = exportStatementDetail.storeProductPackingDetail.storeProductPacking.expireDate;
          if (productPackingIdExport === productPackingIdImport
            && new Date(expireDateExport).getTime() === new Date(expireDateImport).getTime()) {
            importDetail.storeProductPackingDetail = {
              qRCode: exportStatementDetail.storeProductPackingDetail.qRCode
            };
          }
        }
        const key = '' + importDetail.expireDate.toString().concat(importDetail.productPacking.id);

        if (importStatementMap.has(key)) {
          const detail = importStatementMap.get(key);
          detail.quantity = Number(detail.quantity) + Number(importDetail.quantity);
          detail.orgQuantity = Number(detail.orgQuantity) + Number(importDetail.quantity);
        } else {
          importDetail.orgQuantity = Number(importDetail.quantity);
          importStatementMap.set(key, importDetail);
        }
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(importDetail.productPacking.product);
      });

      for (const importDetail of importStatementMap.values()) {
        const key = '' + importDetail.expireDate.toString().concat(importDetail.productPacking.id);
        importDetail.orgQuantity = Number(importDetail.quantity);
        if (claimDeliveryMap.has(key)) {
          importDetail.claimDeliveryQuantity = claimDeliveryMap.get(key).claimDeliveryQuantity;
          importDetail.quantity = Number(importDetail.quantity) - importDetail.claimDeliveryQuantity;
          claimDeliveryMap.delete(key);
        } else {
          importDetail.claimDeliveryQuantity = 0;
        }
        if (claimImportMap.has(key)) {
          importDetail.claimImportQuantity = claimImportMap.get(key).claimImportQuantity;
          importDetail.quantity = Number(importDetail.quantity) - importDetail.claimImportQuantity;
          claimImportMap.delete(key);
        } else {
          importDetail.claimImportQuantity = 0;
        }
      }

      importStatement.content = [...importStatementMap.values()];

      this.updateTable(importStatement);
    } else {
      if (!this.importStatement.toStore.dc) {
        this.tblCols.push(new FieldConfigExt({
          type: 'view',
          name: 'pallet.displayName',
          label: 'common.pallet',
          inputType: 'text'
        }));
        this.setCols(this.tblCols);
      }

      const map = new Map();
      let palletId = '';
      let key = '';
      let claimKey = '';
      let packingId = '';
      let expireDate = '';
      this.importStatementDetailPallets = importStatement.importStatementDetailPallets;
      importStatement.importStatementDetailPallets.forEach((detail: any) => {
        console.log(detail);
        if (detail.palletDetail) {
          detail.productPacking = detail.palletDetail.productPacking;
        } else {
          detail.productPacking = detail.storeProductPackingDetail.storeProductPacking.productPacking;
        }
        packingId = detail.productPacking.id;
        expireDate = detail.expireDate;
        palletId = detail.pallet ? detail.pallet.id : '';

        key = palletId + ' ' + expireDate + packingId;
        claimKey = '' + expireDate + packingId;
        if (map.has(key)) {
          const packingDetail = map.get(key);
          packingDetail.quantity = Number(packingDetail.quantity) + Number(detail.quantity);
          packingDetail.orgQuantity = Number(packingDetail.orgQuantity) + Number(detail.quantity);
        } else {
          detail.orgQuantity = Number(detail.quantity);
          detail.claimDeliveryQuantity = 0;
          detail.claimImportQuantity = 0;
          if (claimDeliveryMap.has(claimKey)) {
            const claimDeliveryDetailQuantity = claimDeliveryMap.get(claimKey).claimDeliveryQuantity;
            detail.orgQuantity = Number(detail.orgQuantity) + claimDeliveryDetailQuantity;
            detail.claimDeliveryQuantity = claimDeliveryDetailQuantity;

            claimDeliveryMap.delete(claimKey);
          }
          if (claimImportMap.has(claimKey)) {
            const claimImportDetailQuantity = claimImportMap.get(claimKey).claimImportQuantity;
            detail.orgQuantity = Number(detail.orgQuantity) + claimImportDetailQuantity;
            detail.claimImportQuantity = claimImportDetailQuantity;

            claimImportMap.delete(claimKey);
          }
          map.set(key, detail);
        }
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(detail.productPacking.product);
      });
      const dataTable = [];
      dataTable.push(...claimDeliveryMap.values(), ...claimImportMap.values());
      dataTable.push(...map.values());
      this.updateTable(dataTable);
    }

  }

  onUpdate() {
    const obj = {
      id: this.formData.id,
      description: this.formData.importDescription,
      exportStatementId: this.formData.id,
      fromStoreId: this.formData.fromStore.id,
      fromStoreCode: this.formData.fromStore.code,
      toStoreId: this.formData.toStore.id,
      toStoreCode: this.formData.toStore.code,
      status: ImportStatementStatus.NOT_UPDATE_INVENTORY,
      importStatementDetails: [] as any[]
    };
    for (const row of this.getTblData()) {
      obj.importStatementDetails.push({
        expireDate: row.expireDate,
        productPackingCode: row.productPacking.code,
        productPackingId: row.productPacking.id,
        quantity: row.quantity
      });
    }
    const method = this.apiService.patch('/import-statements/' + this.formData.id, obj);
    this.tsuService.execute6(method, this.saveSuccess, 'import-statement.msg.update', []);
  }

  saveSuccess = () => {
    const msg = this.translateService.instant(this.entity + '.' + this.actionType + '.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
    this.router.navigate(['/import-statement']);
  };

  changeQuantity = (row: any) => {
    console.log(row);
    this.showMaxQuantityWarning(row, 'common.packing.max.quantity.from.export');
  };
}
