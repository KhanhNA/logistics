import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {ImportStatementStatus} from '../../../_models/action-type';
import {NumberValidators} from '../../../base/field/number-validators';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {HttpParams} from "@angular/common/http";

// @Directive()
export class ImportStatementDetailLayout extends LDBaseLayout {

  exportStatementId: any;
  title: any;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected activatedRoute: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService,
              private router: Router,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, activatedRoute, translateService, toastr, location, authoritiesService);
    this.setCols(this.tblCols);
    this.init(this.inputFields);
    this.title = 'importStatement.detail';
  }

  updateSearchInfo(search: any) {
    search.method = this.apiService.get('/export-statements/' + this.exportStatementId, new HttpParams());
  }


  beforeUpdate = (data?: any) => {
    const dataTable = [];
    const exportStatementDetails = data.exportStatementDetails;
    for (let index = 0; index < exportStatementDetails.length; index++) {
      const detail = exportStatementDetails[index];
      const detailId = detail.storeProductPackingDetail.storeProductPacking.productPacking.id;
      const detailExpiredDate = new Date(detail.storeProductPackingDetail.storeProductPacking.expireDate);

      for (let compareIndex = 0; compareIndex < exportStatementDetails.length; compareIndex++) {
        const compareDetail = exportStatementDetails[compareIndex];
        const compareDetailId = compareDetail.storeProductPackingDetail.storeProductPacking.productPacking.id;
        const compareDetailExpiredDate = new Date(compareDetail.storeProductPackingDetail.storeProductPacking.expireDate);
        if (index !== compareIndex && detailId === compareDetailId && detailExpiredDate.getTime() === compareDetailExpiredDate.getTime()) {
          detail.quantity += compareDetail.quantity;
          exportStatementDetails.splice(compareIndex, 1);
          if (compareIndex < index) {
            index--;
          }
          compareIndex--;
        }
      }
      ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(
        detail.storeProductPackingDetail.storeProductPacking.productPacking.product);

      detail.maxQuantity = detail.quantity;
      dataTable.push(detail);
    }
    data.content = dataTable;

    data.fromStore.name = data.fromStore.code + ' - ' + data.fromStore.name;
    data.toStore.name = data.toStore.code + ' - ' + data.toStore.name;
    this.formData = data;
  }

  onSave() {
    const obj = {
      description: this.formData.importDescription,
      exportStatementId: this.formData.id,
      fromStoreId: this.formData.fromStore.id,
      fromStoreCode: this.formData.fromStore.code,
      toStoreId: this.formData.toStore.id,
      toStoreCode: this.formData.toStore.code,
      status: ImportStatementStatus.NOT_UPDATE_INVENTORY,
      importStatementDetails: [] as any[]
    };
    for (const row of this.getTblData()) {
      obj.importStatementDetails.push({
        expireDate: row.storeProductPackingDetail.storeProductPacking.expireDate,
        productPackingCode: row.storeProductPackingDetail.storeProductPacking.productPacking.code,
        productPackingId: row.storeProductPackingDetail.storeProductPacking.productPacking.id,
        quantity: row.quantity
      });
    }
    const method = this.apiService.post('/import-statements/from-export-statement', obj, undefined);
    this.tsuService.execute6(method, this.saveSuccess, 'import-statement.msg.create', []);
  }

  saveSuccess = () => {
    const msg = this.translateService.instant(this.entity + '.' + this.actionType + '.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
    this.router.navigate(['/import-statement']);
  }

  changeQuantity = (row: any) => {
    this.showMaxQuantityWarning(row, 'common.packing.max.quantity');
  }

  inputFields = [
    new FieldConfigExt({
      type: 'input',
      label: 'common.fromStore',
      inputType: 'text',
      name: 'fromStore.name',
      binding: 'fromStore.name',
    }),

    new FieldConfigExt({
      type: 'input',
      label: 'common.toStore',
      inputType: 'text',
      name: 'toStore.name',
      binding: 'toStore.name',
    }),
    new FieldConfigExt({
      type: 'input',
      label: 'common.export.description',
      inputType: 'text',
      name: 'description',
      require: 'false',
      value: '',
      binding: 'description'
    }),
    new FieldConfigExt({
      type: 'input',
      label: 'common.import.description',
      inputType: 'text',
      name: 'importDescription',
      binding: 'importDescription'
    })
  ];

  tblCols: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'index',
      name: 'stt',
      label: 'ld.no',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'storeProductPackingDetail.storeProductPacking.productPacking.code',
      label: 'common.packing.code',
      inputType: 'input'
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'storeProductPackingDetail.storeProductPacking.productPacking.product.name',
      label: 'common.product.name',
      inputType: 'input'
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'storeProductPackingDetail.storeProductPacking.expireDate',
      label: 'common.expireDate',
      inputType: 'date'
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'storeProductPackingDetail.storeProductPacking.productPacking.packingType.quantity',
      label: 'productPacking.packingType.quantity',
      inputType: 'number',
    }),

    new FieldConfigExt({
      type: 'input',
      name: 'quantity',
      label: 'common.quantity',
      inputType: 'number',
      emit: this.changeQuantity,
      validations: [
        {
          name: 'minQuantity',
          validator: NumberValidators.minQuantityValidation(-1),
          message: 'validation.minQuantity'
        },
      ],
    }),
  ];
}
