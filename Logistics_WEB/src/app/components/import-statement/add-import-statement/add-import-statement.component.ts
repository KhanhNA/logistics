import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import {NavService} from '../../../_services/nav.service';
import {ImportStatementDetailLayout} from './import-statement-detail.layout';
import {ActionTypeEneum} from '../../../_models/action-type';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-add-import-statement',
  templateUrl: './add-import-statement.component.html',
  styleUrls: ['./add-import-statement.component.scss', '../../table.layout.scss'
    , '../../form.layout.scss']
})
export class AddImportStatementComponent implements OnInit, AfterViewInit {


  modelDetail: ImportStatementDetailLayout;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService, private dialog: MatDialog,
              private navService: NavService,
              private router: Router,
              protected authoritiesService: AuthoritiesService) {
    this.modelDetail = new ImportStatementDetailLayout(tsuService, fb, route,
      translateService, toastr, location, apiService, router, authoritiesService);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.modelDetail.exportStatementId = params.id;
    });
    this.modelDetail.actionType = ActionTypeEneum.new;
    this.modelDetail.onSearchData();
  }

  ngAfterViewInit(): void {
    this.navService.title = 'menu.import.statement.create.from.export';
  }

  onSave() {
    this.modelDetail.onSave();
  }


  onPage(page?: any) {
    this.modelDetail.tsTable.removeRows(0, this.modelDetail.getTblData().length);

  }

  onCellEvent(event: any) {
    const tblData = this.modelDetail.getTblData();
    if (event.act === 'delete') {
      this.modelDetail.tsTable.removeRows(tblData.indexOf(event.row), 1);
    }
  }
}
