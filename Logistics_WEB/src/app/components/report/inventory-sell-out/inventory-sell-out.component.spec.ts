import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InventorySellOutComponent } from './inventory-sell-out.component';

describe('InventorySellOutComponent', () => {
  let component: InventorySellOutComponent;
  let fixture: ComponentFixture<InventorySellOutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InventorySellOutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InventorySellOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
