import {Component, Injector, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnTypes, DateRangePickerModel,
  FormStateService,
  IconTypeEnum,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {DatePipe} from '@angular/common';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {ExpireDateColorsUtils} from '../../../base/Utils/ExpireDateColorsUtils';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {InventorySellOutModel} from '../../../_models/report/inventory.sell.out.model';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {environment} from '../../../../environments/environment';
import {CommonHttpService} from '../../../_services/http/common/common.http.service';
import {StoreModel} from '../../../_models/store.model';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-inventory-sell-out',
  templateUrl: './inventory-sell-out.component.html',
  styleUrls: ['./inventory-sell-out.component.css']
})
export class InventorySellOutComponent extends BaseSearchLayout implements OnInit {
  moduleName = 'inventory-sell-out';
  buttons: ButtonFields[] = [];
  storeOptions: SelectModel[] = [];

  constructor(private snackBar: MatSnackBar,
              private formBuilder: FormBuilder,
              public dialog: MatDialog,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              protected uiStateService: FormStateService,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              protected commonHttpService: CommonHttpService,
              protected injector: Injector) {

    super(router, apiService, utilsService, uiStateService, translateService,
      injector, activatedRoute, authoritiesService, formBuilder.group({
        storeIds: [''],
        fromDate: [''],
        toDate: [''],
        range: ['']
      }));

    this.columns.push(...[
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'storeCodeName',
        header: 'storeCodeName',
        title: (e: any) => `${e.storeCode + ' - ' + e.storeName}`,
        cell: (e: any) => `${e.storeCode + ' - ' + e.storeName}`,
        className: 'mat-column-store-code-name'
      },
      {
        columnDef: 'packCode',
        header: 'packCode',
        title: (e: InventorySellOutModel) => `${e.packCode}`,
        cell: (e: InventorySellOutModel) => `${e.packCode}`,
      },
      {
        columnDef: 'productName',
        header: 'productName',
        title: (e: InventorySellOutModel) => `${e.productName}`,
        cell: (e: InventorySellOutModel) => `${e.productName}`,
        className: 'mat-column-productName'
      },
      {
        columnDef: 'uom',
        header: 'uom',
        title: (e: InventorySellOutModel) => `${e.uom}`,
        cell: (e: InventorySellOutModel) => `${e.uom}`,
        className: 'mat-column-uom'
      },
      {
        columnDef: 'packSize',
        header: 'packSize',
        title: (e: InventorySellOutModel) => `${e.packSize}`,
        cell: (e: InventorySellOutModel) => `${e.packSize}`,
        className: 'mat-column-packSize'
      },
      {
        columnDef: 'sellQuantity',
        header: 'sellQuantity',
        title: (e: InventorySellOutModel) => `${e.sellQuantity ? e.sellQuantity.toLocaleString('en-US') : 0}`,
        cell: (e: InventorySellOutModel) => `${e.sellQuantity ? e.sellQuantity.toLocaleString('en-US') : 0}`,
        className: 'mat-column-sellQuantity'
      },
      {
        columnDef: 'inventory',
        header: 'inventory',
        title: (e: InventorySellOutModel) => `${e.inventory ? e.inventory.toLocaleString('en-US') : 0}`,
        cell: (e: InventorySellOutModel) => `${e.inventory ? e.inventory.toLocaleString('en-US') : 0}`,
        className: 'mat-column-inventory'
      },
    ]);
  }

  async ngOnInit() {
    const storeRes = await this.commonHttpService.createGetAllDCStoreManagementObservable().toPromise() as Page;
    this.storeOptions = storeRes.content?.map((s: StoreModel) => {
      return new SelectModel(s.id, s.code + ' - ' + s.name, false, s);
    });
    this.setUpDefaultTime();
    super.ngOnInit();
    this.onSubmit();
  }

  setUpDefaultTime() {
    const now = new Date();
    const defaultFromDate = new Date(now.getFullYear(), now.getMonth() - 1, now.getDate());
    const defaultToDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    this.searchForm.patchValue({
      range: new DateRangePickerModel(defaultFromDate, defaultToDate)
    });
  }

  search(): void {
    const {storeIds, fromDate, toDate} = this.searchForm.value;
    const params = new HttpParams()
      .set('storeIds', storeIds ? storeIds.join(',') : '')
      .set('fromDate', fromDate)
      .set('toDate', toDate);
    this._fillData('/report/inventory-and-sell-out', params);
  }

  clearSearch() {
    this.searchForm.patchValue({
      storeIds: []
    });
    this.setUpDefaultTime();
  }

  onExportExcel() {
    const {storeIds, fromDate, toDate} = this.searchForm.value;
    const obj = {
      fromDate,
      toDate
    };
    if (storeIds && storeIds.length) {
      obj['storeIds'] = storeIds;
    }
    this.apiService.saveFile('/report/inventory-and-sell-out/export-excel', obj, {});
  }
}
