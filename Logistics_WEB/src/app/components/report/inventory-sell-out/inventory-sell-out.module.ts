import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InventorySellOutComponent} from './inventory-sell-out.component';
import {HttpClient} from "@angular/common/http";
import {AuthoritiesResolverService, MultiTranslateHttpLoader} from "@next-solutions/next-solutions-base";
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../modules/shared.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/inventory-sell-out/', suffix: '.json'},
    {prefix: './assets/i18n/', suffix: '.json'},
  ]);
}

const routes: Routes = [
  {
    pathMatch: 'full',
    path: '',
    resolve: {me: AuthoritiesResolverService},
    component: InventorySellOutComponent
  },

];

@NgModule({
  declarations: [InventorySellOutComponent],
  imports: [
    SharedModule,
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      isolate: true,
    }),
  ],
  providers: []
})
export class InventorySellOutModule { }
