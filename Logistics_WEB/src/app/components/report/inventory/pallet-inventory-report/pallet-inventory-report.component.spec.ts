import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PalletInventoryReportComponent } from './pallet-inventory-report.component';

describe('PalletInventoryReportComponent', () => {
  let component: PalletInventoryReportComponent;
  let fixture: ComponentFixture<PalletInventoryReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PalletInventoryReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PalletInventoryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
