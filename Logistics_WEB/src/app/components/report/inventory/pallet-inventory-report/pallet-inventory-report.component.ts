import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService, AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields, DateUtilService, FormStateService, LoaderService,
  SelectModel, UtilsService
} from "@next-solutions/next-solutions-base";
import {ModelUtils} from "../../../../_models/ModelUtils";
import {palletConst, palletStep} from "../../../../_models/pallet/pallet.const";
import {FormBuilder} from "@angular/forms";
import {NavService} from "../../../../_services/nav.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {TsUtilsService} from "../../../../base/Utils/ts-utils.service";
import {PalletDetailModel} from "../../../../_models/pallet/pallet.detail.model";
import {AsyncDataUtils} from "../../../../_utils/async.data.utils";
import {partnerStatus} from "../../../../_models/partner/partnerStatus";
import {HttpParams} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {CommonUtils} from "../../../../_utils/common.utils";

@Component({
  selector: 'app-pallet-inventory-report',
  templateUrl: './pallet-inventory-report.component.html',
  styleUrls: ['./pallet-inventory-report.component.css']
})
export class PalletInventoryReportComponent extends BaseSearchLayout implements OnInit {

  moduleName = 'pallet-inventory';
  moduleNameJson = 'palletv2';
  statusOptions: SelectModel[] = [];
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  stepOptions: SelectModel[] = ModelUtils.getInstance().getSelectModel(palletStep);
  storesCollection: SelectModel[] = [];
  distributorCollection: SelectModel[] = [];
  palletCollection: SelectModel[] = [];
  expireDateColors: SelectModel[] = ModelUtils.getInstance().getSelectModel(palletConst);
  expandButton = { // ButtonFields
    columnDef: 'expandColumn',
    color: 'warn',
    icon: 'filter_list',
    click: '',
    title: 'common.title.expand.column',
  };
  errorMsg = new Map<string, () => string>()
    .set('min', () => '')
    .set('max', () => '')
    .set('pattern', () => '')
    .set('required', () => '');

  get environment() {
    return environment;
  }


  constructor(protected formBuilder: FormBuilder,
              public navService: NavService,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              public dateServiceUtil: DateUtilService,
              protected tsuService: TsUtilsService,
              public loaderService: LoaderService,
  ) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        palletSteps: ['1'],
        storeId: [''],
        distributorId: [''],
        palletId: ['_'],
        productPackingCode: [''],
        productPackingName: [''],
        expireDateColor: ['WARNING']
      }));
    // this.initExpireDateColors();
    this.columns.push({
        columnDef: 'stt', header: 'number',
        title: (e: PalletDetailModel) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: PalletDetailModel) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
      },
      // {
      //   columnDef: 'code', header: 'code',
      //   title: (e: PalletDetailModel) => `${e.code ? e.c : ''}`,
      //   cell: (e: PalletDetailModel) => `${e.code ? e.code : ''}`,
      //   className: 'mat-column-code',
      // },
      {
        columnDef: 'pallet.step', header: 'pallet.step',
        title: (e: PalletDetailModel) => `${this.getStepAreaName(e)}`,
        cell: (e: PalletDetailModel) => `${this.getStepAreaName(e)}`,
        className: 'mat-column-pallet.step',
      },
      {
        columnDef: 'pallet.name', header: 'pallet.name',
        title: (e: PalletDetailModel) => `${!!e && e.pallet?.name ? e.pallet.name : ''}`,
        cell: (e: PalletDetailModel) => `${!!e && e.pallet?.name ? e.pallet.name : ''}`,
        className: 'mat-column-pallet.name',
      },
      {
        columnDef: 'pallet.code', header: 'pallet.code',
        title: (e: PalletDetailModel) => `${!!e && e.pallet?.code ? e.pallet.code : ''}`,
        cell: (e: PalletDetailModel) => `${!!e && e.pallet?.code ? e.pallet.code : ''}`,
        className: 'mat-column-pallet.code',
      },
      {
        columnDef: 'productPacking.code', header: 'productPacking.code',
        title: (e: PalletDetailModel) => `${!!e && e.productPacking?.code ? e.productPacking.code : ''}`,
        cell: (e: PalletDetailModel) => `${!!e && e.productPacking?.code ? e.productPacking.code : ''}`,
        className: 'mat-column-productPacking.code',
      },
      {
        columnDef: 'productPacking.name',
        header: 'productPacking.name',
        title: (e: PalletDetailModel) => `${!!e && e.productPacking?.product?.productDescriptions ? e.productPacking?.product?.productDescriptions.filter(des => des.language && des.language.code === translateService.currentLang)[0]?.name : ''}`,
        cell: (e: PalletDetailModel) => `${!!e && e.productPacking?.product?.productDescriptions ? e.productPacking?.product?.productDescriptions.filter(des => des.language && des.language.code === translateService.currentLang)[0]?.name : ''}`,
        className: 'mat-column-productPacking.name',
      },
      {
        columnDef: 'productPacking.packingType.quantity',
        header: 'productPacking.packingType.quantity',
        title: (e: PalletDetailModel) => `${!!e && e.productPacking?.packingType?.quantity ? e.productPacking.packingType.quantity : ''}`,
        cell: (e: PalletDetailModel) => `${!!e && e.productPacking?.packingType?.quantity ? e.productPacking.packingType.quantity : ''}`,
        className: 'mat-column-productPacking.packingType.quantity',
      },
      {
        columnDef: 'productPacking.uom',
        header: 'productPacking.uom',
        title: (e: PalletDetailModel) => `${!!e && e.productPacking?.uom ? e.productPacking.uom : ''}`,
        cell: (e: PalletDetailModel) => `${!!e && e.productPacking?.uom ? e.productPacking.uom : ''}`,
        className: 'mat-column-productPacking.uom',
      },
      {
        columnDef: 'productPacking.product.manufacturer.name',
        header: 'productPacking.product.manufacturer.name',
        title: (e: PalletDetailModel) => `${this.getManufacturerDisplayValue(e)}`,
        cell: (e: PalletDetailModel) => `${this.getManufacturerDisplayValue(e)}`,
        className: 'mat-column-productPacking.product.manufacturer.name',
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'expireDate',
        header: 'expireDate',
        title: (e: PalletDetailModel) => `${this.getExpireDateDisplay(e)}`,
        cell: (e: PalletDetailModel) => `${this.getExpireDateDisplay(e)}`,
        className: 'mat-column-expireDate',
      },
      {
        columnDef: 'quantity',
        header: 'quantity',
        title: (e: PalletDetailModel) => `${!!e && e.quantity ? ModelUtils.getInstance().formatNumber('' + e.quantity) : ''}`,
        cell: (e: PalletDetailModel) => `${!!e && e.quantity ? ModelUtils.getInstance().formatNumber('' + e.quantity) : ''}`,
        className: 'mat-column-quantity',
        align: AlignEnum.CENTER
      },
    );
    // this.buttons.push(
    //   {
    //     columnDef: 'edit',
    //     color: 'warn',
    //     icon: 'fa fa-pen',
    //     iconType: IconTypeEnum.FONT_AWESOME,
    //     click: 'editPartner',
    //     isShowHeader: true,
    //     header: 'common.action',
    //     className: 'secondary',
    //     display: (e: PalletDetailModel) => true
    //
    //
    //     // display: (e: PalletDetailModel) => e && this.distributorAuthoritiesUtils.hasGetPackageRole(),
    //     // disabled: (e: PalletDetailModel) => !e || DistributorAuthoritiesUtils.isAcceptedStatus(e)
    //   },
    // );

  }

  getExpireDateDisplay(pallet: PalletDetailModel) {
    if (!pallet || !pallet.expireDate) {
      return '';
    }
    return this.dateServiceUtil.convertDateToDisplayGMT0(pallet.expireDate);
  }

  getStepAreaName(pallet: PalletDetailModel) {
    const step = pallet?.pallet?.step;
    if (!step) {
      return '';
    }
    return this.translateService.instant(palletStep[step].label);
  }

  private getManufacturerDisplayValue(pallet: PalletDetailModel) {
    const manufacturer = pallet.productPacking?.product?.manufacturer;
    if (!manufacturer) {
      return '';
    }
    return manufacturer.manufacturerDescriptions?.filter(description =>
      description.language?.code === sessionStorage.getItem('lang' + environment.CLIENT_ID))[0].name;
  }

  async ngOnInit() {
    this.getFormDataBinding();
    // this.onSubmit();
  }


  getFormDataBinding() {
    this.getAsyncData().then(data => {
      // this.distributorCollection.push({
      //   code: '- ' + this.translateService.instant('common.status.all'),
      //   name: '',
      //   id: ''
      // });
      data.stores.then(response => {
        if (response && response.content) {
          this.storesCollection = response.content.map((v: any) => {
            return new SelectModel(v.id, v.code, false, v);
          });
          this.searchForm.patchValue({
            storeId: this.storesCollection[0] ? this.storesCollection[0].value : ''
          });
        }

      });
      data.distributors.then(distributors => {
        if (distributors) {
          this.distributorCollection = distributors.map((v: any) => {
            return new SelectModel(v.id, v.code + '-' + v.name, false, v);
          });
          this.searchForm.patchValue({
            distributorId: this.distributorCollection[0] ? this.distributorCollection[0].value : ''
          });
        }
      });
    }).then(() => {
      super.ngOnInit();
      this.onSubmit();
    });
  }

  async reloadPallet() {
    const storeId = this.searchForm.get('storeId')?.value;
    const step = this.searchForm.get('palletSteps')?.value;
    if (!storeId || !step) {
      return;
    }
    this.searchForm.patchValue({
      palletId: '_'
    });
    AsyncDataUtils.getPalletAsyncData(this.tsuService, storeId, step).then(data => {
      if (data && data.pallets && data.pallets.content) {
        this.palletCollection = [new SelectModel('_', 'common.all')];
        this.palletCollection.push(...data.pallets.content.map((v: any) => {
          return new SelectModel(v.id, v.displayName, false, v);
        }));
      }
      this.onSubmit();
    });
  }

  async getAsyncData() {
    return AsyncDataUtils.getAsyncDataStoresAndDistributors(this.tsuService, this.tsuService);
  }

  getStatus(e: any) {
    return this.translateService.instant(partnerStatus['' + e.status].label);
  }

  getStyle(e: any) {
    return partnerStatus['' + e.status].styleTable;
  }

  // Override
  onSubmit(): void {
    this.isResetPaging = true;

    this.search();

  }

  search() {
    const {
      palletSteps, storeId, distributorId, palletId,
      productPackingCode, productPackingName, expireDateColor
    } = this.searchForm.value;
    const params = new HttpParams()
      .set('-1', '')
      .set('palletSteps', palletSteps)
      .set('storeId', storeId)
      .set('distributorId', distributorId)
      .set('palletId', palletId === '_' ? (palletId + '').replace('_', '') : palletId)
      .set('productPackingCode', productPackingCode)
      .set('productPackingName', productPackingName)
      .set('expireDateColor', expireDateColor);

    this._fillData('/pallets/group-by-pallet-product-packing-expire-date/', params);
  }


  addNewPartner() {
    this.router.navigate(['partner/add']).then();
  }

  editPartner(row: any, ind: number) {
    if (!row) {
      return;
    }
    this.router.navigate(['partner/edit', row.id]).then();
  }

  onReset() {
    this.searchForm.patchValue({
      palletSteps: '1',
      storeId: this.storesCollection[0] ? this.storesCollection[0].value : '',
      distributorId: this.distributorCollection[0] ? this.distributorCollection[0].value : '',
      palletId: '_',
      productPackingCode: '',
      productPackingName: '',
      expireDateColor: ''
    });
    // this.onSubmit();
  }

  exportExcel() {
    const {
      palletSteps, storeId, distributorId, palletId,
      productPackingCode, productPackingName, expireDateColor
    } = this.searchForm.value;
    const obj = {
      distributorId,
      palletSteps: [palletSteps],
      storeId,
      expireDateColor: expireDateColor ? (expireDateColor + '').trim() : '',
    };
    if (!CommonUtils.isNullOrUndefinedOrBlankValue(productPackingName)) {
      obj['productPackingName'] = productPackingName;
    }
    if (!CommonUtils.isNullOrUndefinedOrBlankValue(productPackingCode)) {
      obj['productPackingCode'] = productPackingCode;
    }
    if (palletId !== '_') {
      obj['palletId'] = palletId;
    }
    this.apiService.saveFile('/report/pallet-inventory/export-excel', obj, {});
  }
}
