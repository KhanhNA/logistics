import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PalletInventoryReportComponent } from './pallet-inventory-report.component';
import {HttpClient} from "@angular/common/http";
import {AuthoritiesResolverService, MultiTranslateHttpLoader} from "@next-solutions/next-solutions-base";
import {RouterModule, Routes} from "@angular/router";
import {InventoryListComponent} from "../inventory-list/inventory-list.component";
import {SharedModule} from "../../../../modules/shared.module";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/pallet/', suffix: '.json'},
    {prefix: './assets/i18n/', suffix: '.json'},

  ]);
}

const routes: Routes = [
  {
    pathMatch: 'full',
    path: '',
    resolve: {me: AuthoritiesResolverService},
    component: PalletInventoryReportComponent
  },

];

@NgModule({
  declarations: [PalletInventoryReportComponent],
  imports: [
    SharedModule,
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      isolate: true,
    }),
  ]
})
export class PalletInventoryReportModule { }
