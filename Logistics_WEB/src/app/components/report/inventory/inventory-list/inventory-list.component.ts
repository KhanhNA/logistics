import {AfterViewInit, Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  FormStateService,
  IconTypeEnum,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {PrintUtils} from '../../../../base/Utils/print.utils';
import {DatePipe} from '@angular/common';
import {TsUtilsService} from '../../../../base/Utils/ts-utils.service';
import {ExpireDateColorsUtils} from '../../../../base/Utils/ExpireDateColorsUtils';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {NavService} from '../../../../_services/nav.service';
import {ProductDescriptionsUtils} from '../../../../_utils/product.descriptions.utils';
import {environment} from '../../../../../environments/environment';
import {DistributorModel} from '../../../../_models/distributor.model';
import {StoreModel} from '../../../../_models/store.model';
import {HttpParams} from '@angular/common/http';
import {ManufacturerModel} from '../../../../_models/manufacturer.model';
import {StoreInventoryPalletDetailsComponent} from '../../../store/inventory/pallet-details/store-inventory-pallet-details.component';
import {CommonUtils} from '../../../../_utils/common.utils';
import {StoreInventoryReportModel} from '../../../../_models/report/store.inventory.report.model';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InventoryListComponent extends BaseSearchLayout implements OnInit, AfterViewInit {
  moduleName = 'store.inventory';
  stores: SelectModel[] = [];
  distributors: SelectModel[] = [];
  manufacturers: SelectModel[] = [];
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  defaultCurrencyCode = 'MMK';

  expandColumnButton: ButtonFields = environment.EXPAND_HEADER_BUTTON;

  expireDateColorOptions = [
    // new SelectModel(' ', this.translateService.instant('common.status.all')),
    new SelectModel('STILL_EXPIRY_DATE', this.translateService.instant('common.still.expiry.date')),
    new SelectModel('WARNING', this.translateService.instant('common.warning')),
    new SelectModel('OUT_OF_DATE', this.translateService.instant('common.out.of.date'))
  ];

  constructor(private snackBar: MatSnackBar,
              private formBuilder: FormBuilder,
              public dialog: MatDialog,
              protected router: Router,
              protected apiService: ApiService,
              private datePipe: DatePipe,
              private tsService: TsUtilsService,
              private expireDateColorsUtils: ExpireDateColorsUtils,
              protected utilsService: UtilsService,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              private navService: NavService,
              protected uiStateService: FormStateService,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              protected injector: Injector) {

    super(router, apiService, utilsService, uiStateService, translateService,
      injector, activatedRoute, authoritiesService, formBuilder.group({
        text: [''],
        store: [''],
        distributor: [' '],
        manufacturer: [' '],
        expireDateColor: ['STILL_EXPIRY_DATE'],
        productPackingCode: [''],
        productPackingName: ['']
      }));

    this.columns.push(
      // {
      //   columnDef: 'checked',
      //   header: 'checked',
      //   title: (e: any) => `${e.checked}`,
      //   cell: (e: any) => `${e.checked}`,
      //   columnType: ColumnTypes.CHECKBOX,
      //   align: AlignEnum.CENTER
      // },
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      // {
      //   columnDef: 'qRCode',
      //   header: 'qRCode',
      //   title: () => '',
      //   cell: (e: any) => `${e.qRCode}`,
      //   columnType: ColumnTypes.BASE64,
      //   isExpandOptionColumn: () => true
      // },
      // {
      //   columnDef: 'store.code', header: 'store.code',
      //   title: (e: any) => `${e.store.code}`,
      //   cell: (e: any) => `${e.store.code}`,
      //   className: 'mat-column-store-code'
      // },
      {
        columnDef: 'store.label',
        header: 'store.label',
        title: (e: StoreInventoryReportModel) => `${e.storeCode + ' - ' + e.storeName}`,
        cell: (e: StoreInventoryReportModel) => `${e.storeCode + ' - ' + e.storeName}`,
        className: 'mat-column-store-name'
      },
      // {
      //   columnDef: 'palletType',
      //   header: 'palletType',
      //   title: (e: StoreInventoryReportModel) => `${e.productPacking?.barCode ? e.productPacking?.barCode : ''}`,
      //   cell: (e: StoreInventoryReportModel) => `${e.productPacking?.barCode ? e.productPacking?.barCode : ''}`,
      //   isExpandOptionColumn: () => true
      // },
      {
        columnDef: 'productPacking.code',
        header: 'productPacking.code',
        title: (e: StoreInventoryReportModel) => `${e.packCode}`,
        cell: (e: StoreInventoryReportModel) => `${e.packCode}`,
        className: 'mat-column-productPacking-code'
      },
      {
        columnDef: 'barCode',
        header: 'barCode',
        title: (e: StoreInventoryReportModel) => `${e.barcode ? e.barcode : ''}`,
        cell: (e: StoreInventoryReportModel) => `${e.barcode ? e.barcode : ''}`,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'productPacking.product.name',
        header: 'product.name',
        title: (e: StoreInventoryReportModel) => `${e.productName}`,
        cell: (e: StoreInventoryReportModel) => `${e.productName}`,
        className: 'mat-column-product-name'
      },
      {
        columnDef: 'productPacking.packingType.quantity',
        header: 'packingType',
        title: (e: StoreInventoryReportModel) => `${e.packSize}`,
        cell: (e: StoreInventoryReportModel) => `${e.packSize}`,
        className: 'mat-column-packingType'
      },
      {
        columnDef: 'productPacking.uom',
        header: 'uom',
        title: (e: any) => `${e.uom}`,
        cell: (e: any) => `${e.uom}`,
        className: 'mat-column-uom'
      },
      // {
      //   columnDef: 'product.manufacturer.name',
      //   header: 'manufacturer',
      //   title: (e: StoreInventoryModel) => `${e.product?.manufacturer?.code + ' - ' +
      //   e.product?.manufacturer?.manufacturerDescriptions?.filter(des => des.language?.code === this.translateService.currentLang)[0]?.name}`,
      //   cell: (e: StoreInventoryModel) => `${e.product?.manufacturer?.code + ' - ' +
      //   e.product?.manufacturer?.manufacturerDescriptions?.filter(des => des.language?.code === this.translateService.currentLang)[0]?.name}`,
      //   className: 'mat-column-manufacturer'
      // },
      {
        columnDef: 'expireDate',
        header: 'expireDate',
        title: (e: StoreInventoryReportModel) => `${e.expiredDate}`,
        cell: (e: StoreInventoryReportModel) => `${e.expiredDate}`,
        className: 'mat-column-cell-expireDate'
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: StoreInventoryReportModel) => `${e.status}`,
        cell: (e: StoreInventoryReportModel) => `${e.status}`,
        className: (e: any) => `mat-column-status`
      },
      {
        columnDef: 'totalQuantity',
        header: 'totalQuantity',
        title: (e: StoreInventoryReportModel) => `${e.quantity ? e.quantity.toLocaleString('en-US') : 0}`,
        cell: (e: StoreInventoryReportModel) => `${e.quantity ? e.quantity.toLocaleString('en-US') : 0}`,
        className: 'mat-column-totalQuantity'
      },
      {
        columnDef: 'productPacking.price',
        header: 'productPacking.price',
        title: (e: StoreInventoryReportModel) => `${Number(e.importPrice).toLocaleString('en-US')}`,
        cell: (e: StoreInventoryReportModel) => `${Number(e.importPrice).toLocaleString('en-US')}`,
        className: 'mat-column-totalQuantity'
      },
      {
        columnDef: 'productPacking.currency',
        header: 'productPacking.currency',
        title: (e: StoreInventoryReportModel) => `${e.currency ? e.currency : ''}`,
        cell: (e: StoreInventoryReportModel) => `${e.currency ? e.currency : ''}`,
        className: 'mat-column-currency'
      },
      {
        columnDef: 'amount',
        header: 'amount',
        title: (e: StoreInventoryReportModel) => {
          const price = e.importPrice;
          return (price && e.quantity ? Number(price * e.quantity).toLocaleString('en-US') : '0') + '';
        },
        cell: (e: StoreInventoryReportModel) => {
          const price = e.importPrice;
          return (price && e.quantity ? Number(price * e.quantity).toLocaleString('en-US') : '0') + '';
        },
        className: 'mat-column-amount'
      },
    );
    this.buttons.push(
      // {
      //   columnDef: 'pallet-details',
      //   color: 'warn',
      //   header: 'common.action',
      //   icon: 'fa fa-eye',
      //   iconType: IconTypeEnum.FONT_AWESOME,
      //   className: 'info',
      //   click: 'palletDetails',
      //   title: `common.title.detail`,
      //   disabled: (e: any) => e && !this.authoritiesService.hasAuthority('get/stores/inventory/pallet-details') && !e.store.dc,
      //   display: (e: any) => true
      // },
    );
  }

  ngAfterViewInit(): void {
    this.navService.title = 'menu.store.inventory';
  }

  async ngOnInit() {
    this.getFormDataFromApi().then(response => {
      console.log(response);
      response.distributor.then((distributors: DistributorModel[]) => {
        this.distributors = [];
        this.distributors.push(new SelectModel(' ', this.translateService.instant('common.status.all')));
        distributors.forEach(distributor => {
          this.distributors.push(new SelectModel(distributor.id, distributor.code + ' - ' + distributor.name));
        });
      });

      response.store.then((stores: Page) => {
        const storeModels = stores.content as StoreModel[];
        const storeIds: (number | null)[] = [];
        this.stores = [];
        storeModels.forEach(store => {
          storeIds.push(store.id);
          this.stores.push(new SelectModel(store.id, store.code + ' - ' + store.name));
        });

        const par = new HttpParams().append('storeIds', storeIds.join(', '));
        console.log(par);
        this.apiService.get<StoreModel[]>('/stores/by-provide-store', par).subscribe((provideStores: StoreModel[]) => {
          this.stores = [...this.stores, ...provideStores.map(store => new SelectModel(store.id, store.code + ' - ' + store.name))];
          this.searchForm.get('store')?.setValue(this.stores[0] ? this.stores[0].value : '');
          this.onSubmit();
        });
      });

      response.manufacturer.then((manufacturers: Page) => {
        this.manufacturers = [];
        this.manufacturers.push(new SelectModel(' ', this.translateService.instant('common.status.all')));
        manufacturers.content.forEach((manufacturer: ManufacturerModel) => {
          this.manufacturers.push(new SelectModel(manufacturer.id,
            manufacturer.code + ' - ' + manufacturer.manufacturerDescriptions?.filter(des => des.language?.code === this.translateService.currentLang)[0]?.name));
        });
      });
    });

  }

  createObjSearch() {
    const {store, productPackingCode, productPackingName, expireDateColor, manufacturer} = this.searchForm.value;
    const obj = {
      acceptLanguage: this.translateService.currentLang,
      exceptProductPackingIds: [-1],
      expireDateColor: expireDateColor ? (expireDateColor + '').trim() : '',
      storeId: store ? store : ''
    };
    if (!CommonUtils.isNullOrUndefinedOrBlankValue(productPackingName)) {
      obj['productPackingName'] = productPackingName;
    }
    if (!CommonUtils.isNullOrUndefinedOrBlankValue(productPackingCode)) {
      obj['productPackingCode'] = productPackingCode;
    }
    if (manufacturer && (manufacturer + '').trim()) {
      obj['manufacturerId'] = manufacturer;
    }
    return obj;
  }

  search() {
    if (!this.searchForm.get('store')?.value) {
      return;
    }
    this._fillDataByPostMethod('/report/inventory', this.createObjSearch(), {params: new HttpParams()});
  }

  onDistributorChange(value: any) {
    this.search();
  }

  onManufacturerChange(value: any) {
    this.search();
  }

  onExpireDateColorChange(value: any) {
    this.search();
  }

  onStoreChange(value: any) {
    this.search();
  }

  async getFormDataFromApi() {
    const params = new HttpParams()
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('status', 'true')
      .set('text', '')
      .set('pageNumber', '1')
      .set('pageSize', '69');
    const store = this.apiService.get<Page>('/stores', params).toPromise();
    const distributor = this.tsService.getDistributors().toPromise();
    const manufacturer = this.tsService.getAllManufacturers().toPromise();
    await store;
    await distributor;
    await manufacturer;
    return {store, distributor, manufacturer};
  }

  palletDetails(storeInventory: StoreInventoryReportModel) {
    this.dialog.open(StoreInventoryPalletDetailsComponent, {
      disableClose: false,
      data: {
        value: storeInventory
      }
    });
  }

  getPackingPrice(inventory: StoreInventoryReportModel) {
    // return inventory.productPacking?.productPackingPrices[0]?.price
    //   ? inventory.productPacking?.productPackingPrices[0].price : 0;
  }

  exportExcel() {

    this.apiService.saveFile('/report/inventory/export-excel', this.createObjSearch(), {});
  }
}
