import {Component, Inject, Injector, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields,
  DateUtilService,
  FormStateService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {StoreModel} from "../../../_models/store.model";


@Component({
  selector: 'app-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.scss', '../../table.layout.scss', '../../form.layout.scss']
})
export class MerchantComponent extends BaseSearchLayout implements OnInit {
  columns: ColumnFields[] = [];

  buttons: ButtonFields[] = [];

  moduleName = 'merchant.order';

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private dateUtilService: DateUtilService,
              @Inject(MAT_DIALOG_DATA) public data: StoreModel | any) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        date: [''],
        fromDate: [''],
        tpDate: [''],
      }));
    this.columns.push(
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'merchantCode',
        header: 'merchantCode',
        title: (e: any) => `${e.merchantCode}`,
        cell: (e: any) => `${e.merchantCode}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-merchantCode'
      },
      {
        columnDef: 'merchantName',
        header: 'merchantName',
        title: (e: any) => `${e.fullName}`,
        cell: (e: any) => `${e.fullName}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-merchantName'
      },
      {
        columnDef: 'registerDate',
        header: 'registerDate',
        title: (e: any) => `${e.activeDate ? this.dateUtilService.convertDateToDisplayGMT0(e.activeDate) : ''}`,
        cell: (e: any) => `${e.activeDate ? this.dateUtilService.convertDateToDisplayGMT0(e.activeDate) : ''}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-registerDate'
      }
    );
  }

  ngOnInit() {
    super.ngOnInit();
    super.onSubmit();
  }

  search(): void {
    const locationCharges: string[] = this.data.storeLocationCharges.map((lc: any) => lc.countryLocation.code);
    const obj = {
      villageCodes: locationCharges,
      keyword: this.searchForm.value.text,
      registerFromDate: this.searchForm.value.fromDate,
      registerToDate: this.searchForm.value.toDate,
      // offset: ''
    };
    this._fillDataByPostMethod('/api/v1/merchant/get-warehouse', obj, {
      params: new HttpParams()
    }, environment.DCOMM_BASE_URL);
  }

}
