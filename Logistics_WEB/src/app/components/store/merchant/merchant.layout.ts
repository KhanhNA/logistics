import { OnInit, Directive } from '@angular/core';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {DatePipe, Location} from '@angular/common';
import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {Search} from '../../../_models/Search';
import {HttpParams} from '@angular/common/http';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {environment} from '../../../../environments/environment';

@Directive()
export class MerchantLayout extends LDBaseLayout implements OnInit {
  inputFields: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'input',
      inputType: 'input',
      name: 'text',
      binding: 'text',
      label: 'common.search'
    }),

    new FieldConfigExt({
      type: 'date',
      inputType: 'input',
      name: 'registerFromDate',
      binding: 'registerFromDate',
      label: 'common.fromDate',
      require: 'true'
    }),

    new FieldConfigExt({
      type: 'date',
      inputType: 'input',
      name: 'registerToDate',
      binding: 'registerToDate',
      label: 'common.toDate',
      require: 'true',
      options: {min: ['registerFromDate']}
    }),
  ];

  tblCols: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'index',
      name: 'STT',
      label: 'ld.no'
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'merchantCode',
      label: 'merchant.order.table.header.merchantCode'
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'fullName',
      label: 'merchant.order.table.header.merchantName'
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'activeDate',
      inputType: 'date',
      label: 'merchant.order.table.header.registerDate'
    })
  ];

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService,
              private store: any, private datePipe: DatePipe,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, translateService, toastr, location, authoritiesService);
    this.setCols(this.tblCols);

    this.init(this.inputFields, this.languages);
    const current = new Date();
    const fromDate = new Date();
    fromDate.setDate(fromDate.getDate() - 30);
    this.formData = {
      text: '',
      registerFromDate: fromDate,
      registerToDate: current
    };
  }

  ngOnInit() {
  }

  updateSearchInfo(search: Search) {
    const params = new HttpParams()
      .set('keyword', this.fv.text)
      .set('offset', '')
      .set('page', search.pageNumber.toString())
      .set('size', search.pageSize.toString())
      .set('registerFromDate', '' + this.datePipe.transform(this.formData.registerFromDate, 'yyyy-MM-dd', '-0'))
      .set('registerToDate', '' + this.datePipe.transform(this.formData.registerToDate, 'yyyy-MM-dd', '-0'))
      .set('storeCode', this.store.code)
      .set('unpaged', '')
      .set('sort.sorted', '')
      .set('sort.sorted', '')
      .set('paged', '');

    search.method = this.apiService.get('/api/v1/merchant/get-warehouse', params, environment.DCOMM_BASE_URL);
  }
}
