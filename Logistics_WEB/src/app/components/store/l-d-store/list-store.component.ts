import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  DateUtilService, FlatTreeNode,
  FormStateService, IconTypeEnum,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {NavService} from '../../../_services/nav.service';
import {HttpParams} from '@angular/common/http';
import {StoreStatusEnum} from '../../../_models/enums/StoreStatusEnum';
import {StoreModel} from '../../../_models/store.model';
import {MerchantComponent} from '../merchant/merchant.component';
import {MatDialog} from '@angular/material/dialog';
import {ActionTypeEneum} from '../../../_models/action-type';
import {CountryModel} from "../../../_models/country.model";
import {StoreAuthoritiesService} from "../../../_services/authority/store.authorities.service";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-list-store',
  templateUrl: './list-store.component.html',
})
export class ListStoreComponent extends BaseSearchLayout implements OnInit {

  moduleName = 'store';
  buttons: ButtonFields[] = [];

  statusValues: SelectModel[] = [];
  regionOptions: SelectModel[] = [];
  regionArr: any[] = [];
  districtArr: any[] = [];
  townshipArr: any[] = [];
  districtOptions: SelectModel[] = [];
  townshipOptions: SelectModel[] = [];
  locationTree: any[] | undefined;
  countryOptions: SelectModel[] = [];
  selectedCountry: any;
  selectedRegion: any;
  selectedDistrict: any;
  storeTypeOptions: SelectModel[] = [];

  get expandHeaderButton() {
    return environment.EXPAND_HEADER_BUTTON;
  }


  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private navService: NavService,
              protected dateUtilService: DateUtilService,
              public storeAuthoritiesService: StoreAuthoritiesService,
              public dialog: MatDialog) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        status: ['_true'],
        region: [''],
        country: [''],
        township: [''],
        district: [''],
        is_DC: ['_']
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code',
        header: 'code',
        title: (e: StoreModel) => `${e.code}`,
        cell: (e: StoreModel) => `${e.code}`,
      },
      {
        columnDef: 'name',
        header: 'name',
        title: (e: StoreModel) => `${e.name}`,
        cell: (e: StoreModel) => `${e.name}`,
      },
      // {
      //   columnDef: 'domainName',
      //   header: 'domainName',
      //   title: (e: StoreModel) => `${e.domainName ? e.domainName : ''}`,
      //   cell: (e: StoreModel) => `${e.domainName ? e.domainName : ''}`,
      // },
      {
        columnDef: 'address',
        header: 'address',
        title: (e: StoreModel) => `${e.address}`,
        cell: (e: StoreModel) => `${e.address}`,
      },
      {
        columnDef: 'region',
        header: 'region',
        title: (e: StoreModel) => `${e.region?.name}`,
        cell: (e: StoreModel) => `${e.region?.name}`,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'phone',
        header: 'phone',
        title: (e: StoreModel) => `${e.phone}`,
        cell: (e: StoreModel) => `${e.phone}`,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'inBusinessSince',
        header: 'inBusinessSince',
        title: (e: StoreModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.inBusinessSince ? e.inBusinessSince : '')}`,
        cell: (e: StoreModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.inBusinessSince ? e.inBusinessSince : '')}`,
      },
      {
        columnDef: 'email',
        header: 'email',
        title: (e: StoreModel) => `${e.email}`,
        cell: (e: StoreModel) => `${e.email}`,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: StoreModel) => `${this.translateService.instant(UtilsService.getEnumValue(StoreStatusEnum, e.status))}`,
        cell: (e: StoreModel) => `${this.translateService.instant(UtilsService.getEnumValue(StoreStatusEnum, e.status))}`,
        className: (e: StoreModel) => {
          switch (!!e.status) {
            case true: return 'approved';
            case false: return 'rejected';
          }
        }
      },
    );
    this.buttons.push(...[{
      columnDef: 'visibility',
      color: 'warn',
      icon: 'visibility',
      click: 'getMerchantOfStore',
      title: 'common.title.visibility',
      display: (e: StoreModel) => e && this.authoritiesService.hasAuthority('get/stores/{id}'),
      disabled: (e: any) => !e || !e.dc
    },
      {
        columnDef: 'done_all',
        color: 'warn',
        icon: 'done_all',
        click: 'active',
        title: 'common.title.activate',
        display: (e: StoreModel) => e && this.authoritiesService.hasAuthority('patch/stores/activate/{id}'),
        disabled: (e: any) => e && e.status
      },
      {
        columnDef: 'edit',
        color: 'warn',
        icon: 'edit',
        click: 'addOrEdit',
        title: 'common.title.edit',
        isShowHeader: true,
        display: (e: StoreModel) => e && this.authoritiesService.hasAuthority('get/stores/{id}') && this.authoritiesService.hasAuthority('patch/stores/{id}'),
        disabled: (e: any) => !e,
        header: 'common.action'/* {
          columnDef: 'add',
          color: 'warn',
          icon: 'add',
          click: 'addOrEdit',
          title: 'common.title.add',
          display: (e: StoreModel) => !e && this.authoritiesService.hasAuthority('post/stores'),
          disabled: (e: any) => e
        }*/,
      },
      {
        columnDef: 'clear',
        color: 'warn',
        icon: 'clear',
        click: 'deactivate',
        title: 'common.store.deactivate',
        display: (e: StoreModel) => e && this.authoritiesService.hasAuthority('patch/stores/deactivate/{id}'),
        disabled: (e: any) => e && !e.status
      },
      {
        columnDef: 'local_shipping',
        color: 'warn',
        icon: 'local_shipping',
        click: 'localShipping',
        title: 'common.title.delivery',
        display: (e: StoreModel) => e
          && this.authoritiesService.hasAuthority('post/store-delivery-waits')
          && this.authoritiesService.hasAuthority('patch/store-delivery-waits/{id}'),
        disabled: (e: any) => !e
      },
      {
        columnDef: 'view',
        title: 'common.title.dashboard',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'onView',
        className: 'info',
        display: () => this.authoritiesService.hasAuthority('get/stores/{id}'),
      }
    ]);
  }

  async ngOnInit() {
    this.navService.title = 'menu.store';

    this.storeTypeOptions = [
      new SelectModel('_', this.translateService.instant('common.all')),
      new SelectModel('_1', 'DC'),
      new SelectModel('_0', 'FC')
    ];

    Object.keys(StoreStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(StoreStatusEnum, key.replace('_', ''));
      if (key.replace('_', '')){
        this.statusValues.push(new SelectModel(key, value));
      }

    });
    this.countryOptions = [];
    this.locationTree = await this.apiService.get('/countries/locations/tree', new HttpParams()).toPromise() as any[];
    this.countryOptions = this.locationTree.map(country => new SelectModel(country.id, country.code, false, country));
    this.locationTree.forEach(country => this.regionArr.push(...country.children));
    this.regionArr.forEach(region => this.districtArr.push(...region.children));
    this.districtArr.forEach(district => this.townshipArr.push(...district.children));

    this.searchForm.patchValue({
      country: this.countryOptions.find(country => country.rawData.code === 'MY')?.value
    });

    super.ngOnInit();
    this.onSubmit();
  }

  search() {
    const {status, text, is_DC, country, region, district, township} = this.searchForm.value;
    let params = new HttpParams()
      .set('text', text)
      .set('status', `${status}`.replace('_', ''))
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'true')
      .set('is_DC', is_DC.toString().replace('_', ''));
    if (country) {
      params = params.set('country_id', country);
    }
    if (region) {
      params = params.set('region_id', region);
    }
    if (township) {
      params = params.set('township_id', township);
    }
    if (district) {
      params = params.set('district_id', district);
    }
    this._fillData('/stores', params);
  }

  getMerchantOfStore(row: StoreModel) {
    this.dialog.open(MerchantComponent, {
      disableClose: false,
      width: '70vw',
      height: '70%',
      data: row
    });
  }

  addOrEdit(row: StoreModel) {
    const url = '/store';
    if (row) {
      this.router.navigate([url, ActionTypeEneum.edit, row.id]);
    } else {
      this.router.navigate([url, ActionTypeEneum.new]);
    }

  }

  onView(row: StoreModel){
    this.router.navigate(['/store', ActionTypeEneum.view, row.id]);
  }

  localShipping(row: StoreModel) {
    this.router.navigate(['/store/edit/max-delivery', row.id], {state: {fromStore: row}});
  }

  active(row: StoreModel) {
    const apiCall = this.apiService.patch('/stores/activate/' + row.id, null);
    this.utilsService.execute(apiCall, this.onSuccessFunc, '.done_all.success', 'store.message.active', ['store.activate']);
  }

  deactivate(row: StoreModel) {
    const apiCall = this.apiService.patch('/stores/deactivate/' + row.id, null);
    this.utilsService.execute(apiCall, this.onSuccessFunc, '.clear.success', 'store.message.active', ['common.store.deactivate.param']);
  }

  changeCountry(countries: SelectModel[]) {
    if (countries[0]?.rawData) {
      this.selectedCountry = countries[0].rawData;
      const children = this.selectedCountry.children;
      if (children) {
        this.searchForm.patchValue({
          region: '',
          district: '',
          township: ''
        });
        this.regionOptions = children.map((child: any) => new SelectModel(child.id, child.name, false, child));
      }
    }
  }

  onRegionChange(regions: SelectModel[]) {
    if (regions[0]?.rawData) {
      this.selectedRegion = regions[0].rawData;
      const children = this.selectedRegion.children;
      if (children) {
        this.searchForm.patchValue({
          district: '',
          township: ''
        });
        this.districtOptions = children.map((child: any) => new SelectModel(child.id, child.name, false, child));
      }
    }
  }

  onDistrictChange(districts: SelectModel[]) {
    if (districts[0]?.rawData) {
      this.selectedDistrict = districts[0].rawData as FlatTreeNode;
      const children = this.selectedDistrict.children;
      if (children) {
        this.searchForm.patchValue({
          township: ''
        });
        this.townshipOptions = children.map((child: any) => new SelectModel(child.id, child.name, false, child));
      }
    }
  }

  createStore() {
    this.router.navigate(['/store', ActionTypeEneum.new]).then();
  }

  onResetForm() {
    this.searchForm.reset();
    this.searchForm.patchValue({
      text: '',
      status: '_true',
      region: '',
      country: '',
      township: '',
      district: '',
      is_DC: '_'
    });

    // this.setUpDefaultTime();
  }
}
