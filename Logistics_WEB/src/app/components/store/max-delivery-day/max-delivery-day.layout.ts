import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {NumberValidators} from '../../../base/field/number-validators';
import {ActionTypeEneum} from '../../../_models/action-type';
import {StoreModel} from '../../../_models/store.model';
import {ApiService, AuthoritiesService, Page} from '@next-solutions/next-solutions-base';
import {HttpParams} from '@angular/common/http';
import { Directive } from "@angular/core";

// @Directive()
export class MaxDeliveryDayLayout extends LDBaseLayout {

  storesMap = new Map();
  stores: any[] = [];
  fromStore: StoreModel | undefined;

  isCreateAll = true;

  constructor(protected tsuService: TsUtilsService,
              fb: FormBuilder,
              protected route: ActivatedRoute,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              protected location: Location,
              private apiService: ApiService,
              private storeId: any,
              protected authoritiesService: AuthoritiesService
  ) {
    super(tsuService, fb, route, translateService, toastr, location, authoritiesService);

    this.getStoreList();
    this.setCols(this.cols);
  }

  getStoreList() {
    this.apiService.get<StoreModel>('/stores/' + this.storeId, new HttpParams()).subscribe((store: StoreModel) => {
      this.fromStore = store;
      this.fromStore.name = this.fromStore.code + ' - ' + this.fromStore.name;
      const dataTable: any[] = [];
      this.getAsyncData().then((resolve: any) => {
        if (resolve.existedFromStore.length > 0) {
          this.isCreateAll = false;
        }
        resolve.existedFromStore.forEach((obj: any) => {
          this.storesMap.set(obj.toStore.id, obj);
        });
        this.stores = resolve.stores.content;
        this.updateTableData(dataTable, true);

      });
    });

  }

  updateTableData(dataTable: any[], isConcat: any) {
    let isDone = false;
    this.stores.forEach(store => {
      if (this.fromStore?.id !== store.id) {
        if (!this.storesMap.has(store.id)) {
          if (isConcat) {
            store.name = store.code + ' - ' + store.name;
          }
          dataTable.push({
            fromStore: this.fromStore,
            toStore: store,
            maxDayToWait: 0
          });
        } else {
          const maxDelivery = this.storesMap.get(store.id);
          if (!isDone) {
            maxDelivery.fromStore.name = maxDelivery.fromStore.code + ' - ' + maxDelivery.fromStore.name;
            isDone = true;
          }
          maxDelivery.toStore.name = maxDelivery.toStore.code + ' - ' + maxDelivery.toStore.name;
          dataTable.push(maxDelivery);
        }
      }
    });
    this.updateTable(dataTable);
  }

  async getAsyncData() {
    const existedFromStore = await this.apiService.get(`/store-delivery-waits/from-store/${this.fromStore?.id}`,
      new HttpParams()).toPromise();
    const params = new HttpParams()
      .set('text', '')
      .set('status', 'true')
      .set('exceptIds', `${this.fromStore?.id}`)
      .set('ignoreCheckPermission', 'true')
      .set('isDc', '')
      .set('pageNumber', '1')
      .set('pageSize', '9999');
    const stores = await this.apiService.get<Page>('/stores', params).toPromise();
    return {existedFromStore, stores};
  }

  async getAsyncMaxDeliveryData() {
    const existedFromStore = await this.apiService.get(`/store-delivery-waits/from-store/${this.fromStore?.id}`,
      new HttpParams()).toPromise();
    return {existedFromStore};
  }

  updateStoreList() {
    const dataTable: any[] = [];
    this.getAsyncMaxDeliveryData().then((resolve: any) => {
      resolve.existedFromStore.forEach((obj: any) => {
        this.storesMap.set(obj.toStore.id, obj);
      });
      this.updateTableData(dataTable, false);
    });
  }


  onSave(maxDeliveryWait: any[]) {
    const method = this.apiService.post('/store-delivery-waits', maxDeliveryWait);
    this.tsuService.execute6(method, this.saveSuccess2, 'common.confirmSave', [' ']);
  }


  onUpdate(maxDeliveryWait: any) {
    this.actionType = ActionTypeEneum.edit;
    const method = this.apiService.patch(`/store-delivery-waits/${maxDeliveryWait.id}`, maxDeliveryWait);
    this.tsuService.execute6(method, this.saveSuccess, 'common.confirmSave', [' ']);
  }

  saveSuccess2 = (data: any) => {
    const msg = this.translateService.instant('.add.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
    this.getStoreList();
  };


  cols: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'index',
      name: 'STT',
      label: 'ld.no',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'fromStore.name',
      label: 'common.fromStore',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'toStore.name',
      label: 'common.toStore'
    }),
    new FieldConfigExt({
      type: 'input',
      name: 'maxDayToWait',
      label: 'common.maxDay.toWait',
      inputType: 'number',
      value: '',
      require: 'true',
      validations: [
        {
          name: 'minQuantity',
          validator: NumberValidators.minQuantityValidation(0),
          message: 'validation.minDayNumber'
        },
        {
          name: 'integerNumber',
          validator: NumberValidators.integerValidation(),
          message: 'validation.day.integer'
        },
      ]
    }),
    new FieldConfigExt({
      type: 'btnGrp',
      name: 'add',
      value: ['save', 'edit'],
      authorities: {
        save: ['post/store-delivery-waits'],
        edit: ['patch/store-delivery-waits/{id}']
      },
      titles: {
        save: 'common.title.save',
        edit: 'common.title.update'
      }
    })
  ];

}
