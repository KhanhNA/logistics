import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormArray, FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {MaxDeliveryDayLayout} from './max-delivery-day.layout';
import {NavService} from '../../../_services/nav.service';
import {ApiService, AuthoritiesService, UtilsService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-max-delivery-day',
  templateUrl: './max-delivery-day.component.html',
  styleUrls: ['../../table.layout.scss', './max-delivery-day.component.scss']
})
export class MaxDeliveryDayComponent implements OnInit, AfterViewInit {

  model: MaxDeliveryDayLayout;
  storeId: any;

  constructor(protected tsuService: TsUtilsService,
              protected fb: FormBuilder,
              protected route: ActivatedRoute,
              private router: Router,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              protected location: Location,
              private apiService: ApiService,
              private navService: NavService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService) {

    this.route.paramMap.subscribe(params => {
      this.storeId = params.get('id');
    });
    this.model = new MaxDeliveryDayLayout(this.tsuService, this.fb, this.route, this.translateService,
      this.toastr, this.location, this.apiService, this.storeId, this.authoritiesService);

  }


  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.navService.title = 'common.store.delivery.maxtime';
    this.model.form.markAllAsTouched();
  }

  isShowBtn(event: any) {
    if (this.model.isCreateAll) {
      event.isShow = false;
      return;
    }
    if (event.act === 'add') {
      event.isShow = false;
      return;
    }
    const index = this.model.tsTable.dataSrc.data.indexOf(event.row);
    const formArray = this.model.form.controls.table as FormArray;
    if (!event.row.maxDayToWait || event.row.maxDayToWait <= 0
      || (index >= 0 && formArray.controls[index].invalid)) {
      event.isShow = false;
      return;
    }

    if (event.row.id) {
      event.isShow = 'edit' === event.act;
      return;
    } else {
      event.isShow = 'save' === event.act;
      return;
    }
  }

  onCellEvent(event: any) {
    if (event.row.id) {
      this.model.onUpdate(event.row);
    } else {
      this.model.onSave([event.row]);
    }

  }

  onSaveAll() {
    this.model.onSave(this.model.getTblData());
  }


  onDownloadTemplate() {
    this.apiService.saveFile('/store-delivery-waits/export-from-store/' + this.storeId, null, {});
  }

  onFileChangeAction(event: Event) {
    const target = event.target as HTMLInputElement;
    console.log('change file', target , target.files);
    if (target && target.files && target.files.length > 0) {
      const f: File = target.files[0];
      const formData = new FormData();
      formData.append('file', f, f.name);
      target.value = '';
      const apiCall = this.apiService.post('/store-delivery-waits/import-from-store/' + this.storeId, formData);
      this.utilsService.execute(apiCall, this.onSuccessFunc, '.add.success', '');
    }
  }
  onSuccessFunc = (data: any, onSuccessMessage?: string): void => {
    this.utilsService.onSuccessFunc(onSuccessMessage ? onSuccessMessage : 'common.default.success');
    this.model.getStoreList();
  }
}
