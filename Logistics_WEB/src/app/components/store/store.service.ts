import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  selectedTabIndex = new BehaviorSubject(0);

  constructor() { }
}
