import {FieldConfigExt} from '../../../base/field/field.interface';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {ActionTypeEneum} from '../../../_models/action-type';
import {EmailValidator} from '../../../base/field/email-validator';
import {delay} from 'rxjs/operators';
import {HttpParams} from '@angular/common/http';
import {ApiService, AuthoritiesService, DateUtilService} from '@next-solutions/next-solutions-base';
import {NumberValidators} from '../../../base/field/number-validators';
import {ConfigEnum} from '../../../_models/enums/config.enum';
import {Directive} from "@angular/core";

// @Directive()
export class AEStoreLayout extends LDBaseLayout {

  languageOptions: string[] = [];
  currencyOptions: string[] = [];
  districtOptions = {};
  countriesArr: any[] = [];

  countryOptions = {};
  regionArr: any[] = [];
  districtArr: any[] = [];
  townshipArr: any[] = [];
  regionOptions = {};
  townshipOptions = {};
  locationsTree: any[] = [];


  languagesMap: { [key: string]: any[] } = {};
  currencyMap: any[] = [];

  isDC = true;

  storeUsers: any[] = [];

  enableUpdate?: boolean;

  inputFields: FieldConfigExt[] | undefined;
  storeCodeCol = new FieldConfigExt({
    type: 'input',
    name: 'code',
    inputType: 'input',
    binding: 'code',
    label: 'store.code',
    readonly: 'true',
    fieldSet: 'systemInfo'
  });

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected activatedRoute: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService,
              protected dateUtilService: DateUtilService,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, activatedRoute, translateService, toastr, location, authoritiesService);
    if (this.actionType === ActionTypeEneum.new) {
      this.getOptions();
    } else {
      this.getOptions(this.parentId);
    }
  }

  onSave() {
    console.log(this.formData);
    let method;
    if (!this.formData.status) {
      this.formData.status = false;
    }
    const formData = this.formData;
    if (formData.inBusinessSince) {
      formData.inBusinessSince = formData.inBusinessSince.replace('T', ' ');
    }
    if (formData.outBusinessSince) {
      formData.outBusinessSince = null;
    }
    if (!formData.dc) {
      formData.provideStore = null;
    }

    formData.language = {id: this.languagesMap.en};
    const successMethod = this.saveSuccess;
    if (this.actionType === ActionTypeEneum.new) {
      method = this.apiService.post('/stores', [formData]);
    } else if (this.actionType === ActionTypeEneum.edit) {
      method = this.apiService.patch('/stores/' + this.parentId, formData);
    }
    this.tsuService.execute6(method, successMethod, 'common.confirmSave', ['common.store']);
  }

  saveSuccess = (data: any) => {
    const msg = this.translateService.instant(this.entity + '.' + this.actionType + '.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);

    delay(500);
    this.location.back();
  };

  getLangOptions() {
    return this.apiService.get('/languages', new HttpParams());
  }

  getCurrencyOptions() {
    return this.apiService.get('/currencies/all', new HttpParams());
  }

  getStoreUsers() {
    return this.apiService.get('/stores/users/' + this.parentId, new HttpParams());
  }

  getUsersList() {
    const params = new HttpParams()
      .set('pageNumber', String(1))
      .set('pageSize', String(20))
      .set('status', 'true')
      .set('text', '');
    return this.apiService.get('/users', params);
  }

  getOptions(id ?: number) {
    this.loadAllFromApi()
      .then(resolve => {
        resolve.languageOptions.then((languages: any) => {
          for (const lang of languages) {
            this.languageOptions.push(lang.code);
            this.languagesMap[lang.code] = lang.id;
          }
        });

        resolve.currencyOptions.then((currencies: any) => {
          for (const currency of currencies) {
            this.currencyOptions.push(currency.code);
            this.currencyMap[currency.code] = currency.id;
          }
        });
        resolve.storeUsesApi.then((storeUsers: any) => {
          if (this.actionType === ActionTypeEneum.new) {
            this.storeUsers = storeUsers.content;
          } else {
            this.storeUsers = storeUsers;
            if (storeUsers.length === 0) {
              this.showWarningMsg('store.user.not.exist');
            }
          }
        });
        resolve.locationTree.then((tree: any) => {
          console.log(tree);
          this.locationsTree = tree;
          this.countriesArr = [...this.locationsTree];
          this.countriesArr.forEach(country => {
            this.countryOptions[country.id] = country.isocode;
          });

          this.countriesArr.forEach(country => this.regionArr.push(...country.children));
          this.regionArr.forEach(region => {
            this.regionOptions[region.id] = region.name;
          });

          // set data districtArr
          this.regionArr.forEach(region => this.districtArr.push(...region.children));
          this.districtArr.forEach(district => {
            this.districtOptions[district.id] = district.name;
          });
          // set data township
          this.districtArr.forEach(district => this.townshipArr.push(...district.children));
          this.townshipArr.forEach(township => {
            this.townshipOptions[township.id] = township.name;
          });
        });
        resolve.enableUpdatePromise.then((enable: any) => {
          this.enableUpdate = Number(enable.value) !== 0;

        })
      })
      .then(() => {
        this.inputFields = [
          this.storeCodeCol,
          new FieldConfigExt({
            type: 'input',
            name: 'name',
            inputType: 'input',
            binding: 'name',
            label: 'store.name',
            require: 'true',
            fieldSet: 'systemInfo'
          }),
          new FieldConfigExt({
            type: 'date',
            label: 'store.inBusinessSince',
            name: 'inBusinessSince',
            inputType: 'date',
            value: '',
            binding: 'inBusinessSince',
            require: 'true',
            fieldSet: 'systemInfo'
          }),
          new FieldConfigExt({
            type: 'input',
            label: 'store.outBusinessSince',
            name: 'outBusinessSince',
            inputType: 'input',
            value: '',
            binding: 'outBusinessSince',
            readonly: 'true',
            fieldSet: 'systemInfo'
          }),
          new FieldConfigExt({
            type: 'radiobutton',
            name: 'dc',
            inputType: 'input',
            binding: 'dc',
            value: true,
            label: 'store.isDC',
            emit: this.onChangeDc,
            options: [{value: true, label: 'DC'}, {value: false, label: 'FC'}],
            fieldSet: 'systemInfo'
          }),
          new FieldConfigExt({
            type: 'autocomplete',
            label: 'store.provideStore',
            inputType: 'text',
            name: 'provideStore',
            value: '',
            options: ['code', 'name'],
            collections: this.getStores,
            binding: 'provideStore',
            fieldSet: 'systemInfo'
          }),
          new FieldConfigExt({
            type: 'select',
            name: 'currency',
            label: 'store.currency',
            inputType: 'input',
            binding: 'currencyForm',
            options: this.currencyOptions,
            fieldSet: 'systemInfo',
            readonly: 'true'
          }),
          new FieldConfigExt({
            type: 'checkbox',
            name: 'status',
            inputType: 'input',
            binding: 'status',
            label: 'store.active',
            require: 'true',
            fieldSet: 'systemInfo'
          }),
          new FieldConfigExt({
            type: 'select',
            name: 'country',
            label: 'store.country',
            inputType: 'input',
            binding: 'countryForm',
            options: this.countryOptions,
            require: 'true',
            fieldSet: 'locationInfo'
          }),
          new FieldConfigExt({
            type: 'select',
            name: 'region',
            inputType: 'input',
            binding: 'region',
            label: 'store.region',
            require: 'true',
            fieldSet: 'locationInfo',
            options: this.regionOptions,
            emit: this.regionChange,
          }),
          new FieldConfigExt({
            type: 'select',
            name: 'district',
            binding: 'district',
            label: 'store.district',
            require: 'true',
            fieldSet: 'locationInfo',
            options: this.districtOptions,
            emit: this.onChangeDistrict
          }),
          new FieldConfigExt({
            type: 'select',
            name: 'township.id',
            inputType: 'input',
            binding: 'township',
            label: 'store.township',
            require: 'true',
            fieldSet: 'locationInfo',
            options: this.townshipOptions
          }),
          new FieldConfigExt({
            type: 'input',
            name: 'address',
            inputType: 'input',
            binding: 'address',
            label: 'store.address',
            require: 'true',
            fieldSet: 'locationInfo'
          }),
          new FieldConfigExt({
            type: 'input',
            label: 'store.floor',
            inputType: 'number',
            name: 'floorNo',
            value: '',
            binding: 'floorNo',
            fieldSet: 'locationInfo',
            validations: [
              {
                name: 'integerNumber',
                validator: NumberValidators.integerValidation(),
                message: 'validation.floor.integer'
              },
              {
                name: 'minQuantity',
                validator: NumberValidators.minQuantityValidation(0),
                message: 'validation.floor'
              },
            ]
          }),
          new FieldConfigExt({
            type: 'input',
            name: 'area',
            inputType: 'number',
            binding: 'area',
            label: 'store.area',
            require: 'true',
            fieldSet: 'locationInfo',
            validations: [
              {
                name: 'minQuantity',
                validator: NumberValidators.minQuantityValidation(0),
                message: 'validation.area'
              },
            ]
          }),

          new FieldConfigExt({
            type: 'input',
            name: 'lat',
            inputType: 'input',
            binding: 'lat',
            label: 'store.lat',
            require: 'true',
            fieldSet: 'locationInfo'
          }),
          new FieldConfigExt({
            type: 'input',
            name: 'lng',
            inputType: 'input',
            binding: 'lng',
            label: 'store.lng',
            require: 'true',
            fieldSet: 'locationInfo'
          }),


          new FieldConfigExt({
            type: 'input',
            name: 'email',
            inputType: 'input',
            binding: 'email',
            label: 'common.email',
            require: 'true',
            validations: [{
              name: 'emailValidation',
              validator: EmailValidator.emailValidation(),
              message: 'validation.email',
              type: 'custom',
              options: 'email'
            }],
            fieldSet: 'contactInfo'
          }),
          new FieldConfigExt({
            type: 'input',
            name: 'phone',
            inputType: 'input',
            binding: 'phone',
            label: 'store.phone',
            require: 'true',
            fieldSet: 'contactInfo'
          }),
          new FieldConfigExt({
            type: 'input',
            name: 'postalCode',
            inputType: 'input',
            binding: 'postalCode',
            label: 'store.postalCode',
            // require: 'true',
            fieldSet: 'contactInfo'
          }),
          new FieldConfigExt({
            type: 'input',
            name: 'domainName',
            inputType: 'input',
            binding: 'domainName',
            label: 'store.domainName',
            fieldSet: 'contactInfo'
            // require: 'true'
          }),
          new FieldConfigExt({
            type: 'autocomplete',
            name: 'user.username',
            inputType: 'input',
            value: '',
            binding: 'user',
            label: 'store.user',
            options: ['username', 'firstName', 'lastName'],
            collections: this.storeUsers,
            require: 'true',
            fieldSet: 'contactInfo'
          }),
        ];
        if (this.actionType === ActionTypeEneum.new) {
          this.formData.currencyForm = '' + this.currencyOptions.indexOf('MMK');
          this.formData.countryForm = '' + this.countriesArr.filter(country => country.isocode === 'MY')[0].id;
          this.storeCodeCol.label = 'store.code.auto.create';
          this.formData.dc = true;
        }
        this.init(this.inputFields, this.languages);
      })
      .then(() => {
        if (id) {
          const method = this.apiService.get('/stores/' + id, new HttpParams());
          this.tsuService.execute6(method, this.bindingData, '', ['']);
        }
      })
      .catch(error => console.log(error));
  }

  bindingData = (data: any) => {
    if (data.inBusinessSince) {
      data.inBusinessSince = data.inBusinessSince.replace(' ', 'T');
    }
    if (data.outBusinessSince) {
      data.outBusinessSince = this.dateUtilService.convertDateToDisplayGMT0(data.outBusinessSince);
      console.log(data.outBusinessSince);
    }
    data.languageForm = this.languageOptions.indexOf(data.language.code) + '';
    data.currencyForm = this.currencyOptions.indexOf(data.currency.code) + '';

    // data.language = this.languageOptions.indexOf(data.language.code) + '';
    // data.country = this.countriesOptions.indexOf(data.country.isocode) + '';
    // data.currency = this.currencyOptions.indexOf(data.currency.code) + '';

    this.formData = data;
    const index = this.inputFields?.findIndex((x: FieldConfigExt) => x.name === 'provideStore');
    if ((index || index === 0) && index >= 0 && !this.form.get('dc')?.value) {
      this.onChangeDc(this.form.get('dc'), this.inputFields ? this.inputFields[index] : undefined);
    }
    this.form.markAllAsTouched();
  }

  async loadAllFromApi() {
    try {
      const languageOptions = this.getLangOptions().toPromise();
      // const countryOptions = this.getCountryOptions().toPromise();
      const currencyOptions = this.getCurrencyOptions().toPromise();
      const locationTree = this.apiService.get('/countries/locations/tree', new HttpParams()).toPromise();
      const enableUpdatePromise = this.apiService.get('/configs', new HttpParams().set('code', ConfigEnum.ENABLE_UPDATE_STORE)).toPromise();
      let storeUsesApi;
      if (this.actionType !== ActionTypeEneum.new) {
        storeUsesApi = this.getStoreUsers().toPromise();
      } else {
        storeUsesApi = this.getUsersList().toPromise();
      }

      await languageOptions;
      // await countryOptions;
      await currencyOptions;
      await storeUsesApi;
      await locationTree;
      await enableUpdatePromise;
      return {languageOptions, currencyOptions, storeUsesApi, locationTree, enableUpdatePromise};
    } catch (e) {
      throw e;
    }
  }

  getStores = () => {
    return this.tsuService.getFCStores();
  };

  onChangeDc = (event: any, field: FieldConfigExt | undefined) => {
    this.isDC = event.value;
    if (!this.isDC && field && field.bindingRef) {
      field.form?.get('provideStore')?.setValue(null);
      field.bindingRef[0].provideStore = null;
    }
  }

  onChangeDistrict = (event?: any) => {
    console.log(event)
    // this.locationsTree.filter()
    const tempData = this.formData;
    if (this.townshipOptions) {
      for (const variableKey of Object.keys(this.townshipOptions)) {
        delete this.townshipOptions[variableKey];
      }
    }
    this.townshipArr.forEach((township: any) => {
      if (township.parent && township.parent.id === Number(event.value)) {
        this.townshipOptions[township.id] = township.name;
      }
    });
  }

  regionChange = (event?: any) => {
    console.log(event)
    const temp = {};
    this.districtArr.forEach((district: any) => {
      if (district.parent && district.parent.id === Number(event.value))
        temp[district.id] = district.name;
    });

    this.districtOptions = temp;
    // this.inputFields[10].options = this.districtOptions;
  }
}
