import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AEStoreLayout} from './a-e-store.layout';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {DatePipe, Location} from '@angular/common';
import {ActionTypeEneum} from '../../../_models/action-type';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {NavService} from '../../../_services/nav.service';
import {ApiService, AuthoritiesService, DateUtilService} from '@next-solutions/next-solutions-base';
import {DateUtils} from '../../../base/Utils/date.utils';


@Component({
  selector: 'app-a-e-store',
  templateUrl: './a-e-store.component.html',
  styleUrls: ['../../table.layout.scss', '../../form.layout.scss', './a-e-store.component.scss'],
  providers: [
    DatePipe
  ]
})
export class AEStoreComponent implements OnInit, AfterViewInit {

  model: AEStoreLayout;
  params: any;

  lat = 20.9174528;
  lng = 105.6997376;

  title: any = '';

  get DateUtils(){
    return DateUtils;
  }

  constructor(protected tsuService: TsUtilsService,
              fb: FormBuilder,
              protected route: ActivatedRoute,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              protected location: Location,
              protected apiService: ApiService,
              protected datePipe: DatePipe,
              protected navService: NavService,
              protected dateUtilService: DateUtilService,
              protected authoritiesService: AuthoritiesService
  ) {
    this.model = new AEStoreLayout(tsuService, fb, route, translateService, toastr, location, apiService, dateUtilService,
      authoritiesService);
    navigator.geolocation.getCurrentPosition((position) => {
      this.lat = position.coords.latitude;
      this.lng = position.coords.longitude;
    });
  }

  ngOnInit() {
    // set location default
    this.model.formData.lat = this.lat;
    this.model.formData.lng = this.lng;
    this.title = 'store.add';
    if (this.model.actionType === ActionTypeEneum.edit) {
      this.title = 'store.edit';
    }
    if (this.model.actionType === ActionTypeEneum.new) {
      this.model.formData.inBusinessSince = DateUtils.getCurrentDate_0H();
    }
  }

  ngAfterViewInit(): void {
    this.navService.title = 'store.add';
    if (this.model.actionType === ActionTypeEneum.edit) {
      this.navService.title = 'store.edit';
    }
  }


  // isShowButton(act: string) {
  //   console.log(this.model.actionType);
  //   if (act === this.model.actionType) {
  //     return true;
  //   }
  // }

  isReadonly(field: FieldConfigExt) {
    if ('provideStore' === field.name && !this.model.isDC) {
      field.readonly = 'true';
      return true;
    }
    if ('provideStore' === field.name && this.model.isDC) {
      field.readonly = 'false';
    }
    return false;
  }

  onSave() {
    const formData = this.model.formData;
    formData.lat = this.lat;
    formData.lng = this.lng;

    formData.language = {id: this.convertCodeToId(this.model.languagesMap, this.model.languageOptions[formData.languageForm])};
    formData.currency = {id: this.convertCodeToId(this.model.currencyMap, this.model.currencyOptions[formData.currencyForm])};
    // if (this.storeId != null) {
    //   formData.user = {id: this.convertCodeToId(this.model.userMap, this.model.usersOptions[formData.userForm])};
    // }
    this.model.onSave();
  }

  convertCodeToId(map: any, code: any) {
    return map[code];
  }

  // private getDetail(id: any) {
  //   const method = this.apiService.get('/stores/' + id, null);
  //   this.tsuService.execute6(method, this.bindingData, '', ['']);
  // }
  //
  // bindingData = (data) => {
  //   this.model.getOptions(data);
  // };
}
