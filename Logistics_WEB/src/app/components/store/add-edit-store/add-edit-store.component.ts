import {AfterContentInit, AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  DateUtilService, FileTypes, FlatTreeConfigConvertObject, FlatTreeNode,
  SelectModel, UploadModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ToastrService} from 'ngx-toastr';
import {ActionTypeEneum} from '../../../_models/action-type';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {HttpParams} from '@angular/common/http';
import {ConfigEnum} from '../../../_models/enums/config.enum';
import {CurrencyModel} from '../../../_models/currency.model';
import {StoreModel} from '../../../_models/store.model';
import {SharedService} from '../../../_services/shared.service';
import {ModelUtils} from '../../../_models/ModelUtils';
import {environment} from '../../../../environments/environment';
import {StoreService} from "../store.service";
import {CommonUtils} from "../../../_utils/common.utils";
import {LocationTreeModel} from "../../../_models/LocationTreeModel";
import {StoreLocationChargeModel} from "../../../_models/store-location-charge.model";

@Component({
  selector: 'app-add-edit-store',
  templateUrl: './add-edit-store.component.html',
  styleUrls: ['./add-edit-store.component.scss'],
  providers: [SharedService]
})
export class AddEditStoreComponent extends BaseAddEditLayout implements OnInit, OnDestroy {
  parentId?: string | null;
  actionType?: string | null;
  enableUpdate?: boolean;

  currencyOptions = [];

  provideStores = [];

  regionOptions: SelectModel[] = [];
  regionArr: any[] = [];
  districtArr: any[] = [];
  townshipArr: any[] = [];
  districtOptions: SelectModel[] = [];
  townshipOptions: SelectModel[] = [];
  locationTree: any[] = [];

  isView = false;

  // flatLocationNodes: FlatTreeNode[] = [];
  countryOptions: SelectModel[] = [];

  lat = 20.9174528;
  lng = 105.6997376;
  countryArr: any[] = [];
  currencyArr: any[] = [];

  errorMessage = new Map().set('pattern', () => this.translateService.instant('pattern.required.number'));
  errorMessageEmail = new Map().set('pattern', () => this.translateService.instant('pattern.required.email'));
  readonly emailPattern = '^[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$';
  readonly areaPattern = '^(\\d+\\.?\\d*|\\.\\d+)$';
  languageOptions: any[] = [];
  languagesMap = {};
  isFC = true;
  store?: StoreModel;
  selectedCharges?: number[] = [];
  initData = true;
  locationMap: Map<any, any> = new Map<any, any>();

  get ActionTypeEnum() {
    return ActionTypeEneum;
  }

  get FileType() {
    return FileTypes;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected authoritiesService: AuthoritiesService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected tsuService: TsUtilsService,
              private dateUtilService: DateUtilService,
              protected sharedService: SharedService,
              private router: Router,
              public storeService: StoreService,
              private toastr: ToastrService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.parentId = this.activatedRoute.snapshot.paramMap.get('id');
    this.actionType = this.activatedRoute.snapshot.paramMap.get('actType');
    this.isView = this.actionType === ActionTypeEneum.view;
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.isNewStore !== null && params.isNewStore !== undefined) {
        storeService.selectedTabIndex.next(!!params.isNewStore ? 1 : 0);
      }
    });
  }

  onRequiredProvideStore = () => !this.isFC;


  disableUpdateCheck() {
    return (this.actionType === this.ActionTypeEnum.edit && !this.enableUpdate) || this.isView;
  }

  async ngOnInit() {
    super.ngOnInit();
    this.addEditForm = this.formBuilder.group({
      id: [''],
      language: [''],
      code: [''],
      name: ['', [Validators.required]],
      inBusinessSince: [this.actionType === ActionTypeEneum.new ?
        this.dateUtilService.convertDateToStringGMT0(new Date()) : '', [Validators.required]],
      outBusinessSince: [''],
      dc: '0',
      provideStore: [''],
      currency: [''],
      status: [''],
      country: ['', [Validators.required]],
      region: ['', [Validators.required]],
      district: ['', [Validators.required]],
      township: ['', [Validators.required]],
      address: ['', [Validators.required]],
      floorNo: '',
      area: ['', [Validators.compose([Validators.pattern(this.areaPattern), Validators.required])]],
      lat: [this.lat, [Validators.required]],
      lng: [this.lng, [Validators.required]],
      email: ['', Validators.compose([Validators.pattern(this.emailPattern), Validators.required])],
      phone: ['', [Validators.required]],
      firstName: '',
      lastName: '',
      userTel: '',
      userEmail: '',
      user: [''],
      locationCharges: [''],
      contractFiles: [''],
      width: [''],
      length: [''],
      height: [''],
      deliveryFrequency: ['']
    });
    // this.flatLocationNodes = await this.sharedService.getLocationTree();
    navigator.geolocation.getCurrentPosition((position) => {
      this.lat = position.coords.latitude;
      this.lng = position.coords.longitude;
    });
    if (this.actionType === ActionTypeEneum.new) {
      this.getOptionsAndData();
    } else {
      this.getOptionsAndData(Number(this.parentId));
    }

  }

  ngOnDestroy(): void {
    this.storeService.selectedTabIndex.next(0);
  }

  onDisplayWithAuthority(authorities: any): boolean {
    if (typeof (authorities) === 'string') {
      return this.authoritiesService.hasAuthority(authorities);
    }
    if (typeof (authorities) === 'object') {
      let hasAuthority = false;
      for (const authority of authorities) {
        hasAuthority = hasAuthority || this.authoritiesService.hasAuthority(authority);
        if (hasAuthority) {
          return hasAuthority;
        }
      }
    }
    return false;
  }

  onSuccessFunc = (data: any, onSuccessMessage?: string): void => {
    this.utilsService.onSuccessFunc(onSuccessMessage ? onSuccessMessage : 'common.default.success');
    if (this.isEdit) {
      setTimeout(() => {
        this.back();
      }, 1000);
    } else {
      this.store = data[0] ? data[0] : data;
      this.router.navigate(['/store/edit/' + (data[0] ? data[0].id : data.id)],
        {queryParams: {isNewStore: true}});
    }
  }

  onSave() {
    // if (this.addEditForm.get('inBusinessSince')) {
    //   this.addEditForm.get('inBusinessSince')?.setValue(this.addEditForm.get('inBusinessSince').value.replace('T', ' '));
    // }
    // if (this.addEditForm.get('outBusinessSince')) {
    //   this.addEditForm.get('outBusinessSince')?.setValue('');
    // }
    const {locationCharges} = this.addEditForm.value;
    let method;
    this.addEditForm.get('language')?.setValue(this.languagesMap['en']);
    const formData = new FormData();
    const storeModel = new StoreModel(this.addEditForm);
    storeModel.storeLocationCharges = locationCharges ? locationCharges.map((locationCode: any) => {
      return {
        countryLocation: {id: this.locationMap.get(locationCode)?.id} as LocationTreeModel,
        store: this.id ? new StoreModel(this.id) : undefined
      } as StoreLocationChargeModel;
    }) : [];
    const contractFiles = this.addEditForm.get('contractFiles');
    if (contractFiles) {

      const ids = ModelUtils.getInstance().getFileList(contractFiles, 'files', formData);
      const removeIds: number[] = [];
      this.store?.storeFiles?.forEach(value => {
        if (value.id && (!ids || !ids.includes(value.id))) {
          removeIds.push(value.id);
        }
      });
      if (removeIds && removeIds.length > 0) {
        formData.set('removeFileIds', this.utilsService.toBlobJon(removeIds));
      }
    }

    formData.set('source', this.utilsService.toBlobJon(storeModel));

    if (this.actionType === ActionTypeEneum.new) {
      method = this.apiService.post('/stores', formData);
    } else if (this.actionType === ActionTypeEneum.edit) {
      method = this.apiService.patch('/stores/' + this.parentId, formData);
    }
    this.utilsService.execute(method, this.onSuccessFunc, this.translateService.instant('.' + this.actionType + '.success'),
      'common.confirmSave', ['common.store']);
  }

  onChangeDc(event: any) {
    this.isFC = event.value === '0';
    this.addEditForm.get('provideStore')?.setValue('');
  }

  getStores = () => {
    return this.tsuService.getFCStores();
  }

  getLangOptions() {
    return this.apiService.get('/languages', new HttpParams());
  }

  getCurrencyOptions() {
    return this.apiService.get('/currencies/all', new HttpParams());
  }

  getStoreUsers() {
    return this.apiService.get('/stores/users/' + this.parentId, new HttpParams());
  }

  getUsersList() {
    const params = new HttpParams()
      .set('pageNumber', String(1))
      .set('pageSize', String(20))
      .set('status', 'true')
      .set('text', '');
    return this.apiService.get('/users', params);
  }

  async loadAllFromApi() {
    try {
      const provideStorePromise = this.getStores().toPromise();
      const languageOptions = this.getLangOptions().toPromise();
      // const countryOptions = this.getCountryOptions().toPromise();
      const currencyOptions = this.getCurrencyOptions().toPromise();
      const locationTree = this.apiService.get('/countries/locations/tree', new HttpParams()).toPromise();
      let storeUsesApi;
      if (this.actionType !== ActionTypeEneum.new) {
        storeUsesApi = this.getStoreUsers().toPromise();
      } else {
        storeUsesApi = this.getUsersList().toPromise();
      }

      await provideStorePromise;
      await languageOptions;
      // await countryOptions;
      await currencyOptions;
      await storeUsesApi;
      await locationTree;
      return {provideStorePromise, languageOptions, currencyOptions, storeUsesApi, locationTree};
    } catch (e) {
      throw e;
    }
  }

  getOptionsAndData(id ?: number) {
    if (this.actionType === ActionTypeEneum.edit) {
      this.apiService.get('/configs',
        new HttpParams().set('code', ConfigEnum.ENABLE_UPDATE_STORE))
        .subscribe((enableUpdateValue: any) => {
          this.enableUpdate = Number(enableUpdateValue.value) !== 0;
        });
    }
    this.loadAllFromApi()
      .then(resolve => {
        resolve.provideStorePromise.then((stores: any) => {
          this.provideStores = stores.content.map((store: StoreModel) =>
            new SelectModel(store.id, store.code + ' - ' + store.name));
        });
        resolve.languageOptions.then((languages: any) => {

          for (const lang of languages) {
            this.languageOptions.push(lang.code);
            this.languagesMap[lang.code] = lang.id;
          }
        });

        resolve.currencyOptions.then((currencies: any) => {
          this.currencyOptions = currencies.map((currency: CurrencyModel) => new SelectModel(currency.id, currency.code + ''));
          this.currencyArr = currencies;
        });
        resolve.storeUsesApi.then((storeUsers: any) => {
            if (this.actionType === ActionTypeEneum.edit) {
              // this.addEditForm.get = storeUsers;
              // if (storeUsers.length === 0) {
              //   this.showWarningMsg('store.user.not.exist');
              // }
            }
          }
        );
        resolve.locationTree.then((tree: any) => {
          console.log(tree);
          this.locationMap.clear();
          this.convertTreeToFlatMap(tree, this.locationMap);
          this.locationTree = tree;


          this.countryArr = [...tree];
          this.countryOptions = this.locationTree.map(country => new SelectModel(country.id, country.code));

          this.locationTree.forEach(country => this.regionArr.push(...country.children));
          this.regionArr.forEach(region => this.districtArr.push(...region.children));

          this.districtArr.forEach(district => this.townshipArr.push(...district.children));
        });
      })
      .then(() => {
        if (this.actionType === ActionTypeEneum.new) {
          this.addEditForm.get('currency')?.setValue(this.currencyArr.filter((currency: CurrencyModel) => currency.code === 'MMK')[0].id);
          this.addEditForm.get('country')?.setValue(this.countryArr.filter(country => country.code === 'MY')[0].id);
          this.changeCountry(this.addEditForm.get('country')?.value);
          this.initData = false;
        }
      })
      .then(() => {
        if (id) {
          const method = this.apiService.get('/stores/' + id, new HttpParams());
          this.utilsService.execute(method, this.bindingData, '', '', ['']);
        }
      })
      .catch(error => console.log(error));
  }

  convertTreeToFlatMap(list: (FlatTreeNode | any)[], flatMap: Map<any, any>) {
    if (!list) {
      return;
    }
    for (const node of list) {
      node.value = node.code;
      node.displayValue = node.code + (node.name ? (' - ' + node.name) : '');
      if (node.children && node.children.length > 0) {
        this.convertTreeToFlatMap(node.children, flatMap);
      } else {
        flatMap.set(node.value, node);
      }
    }
  }

  bindingData = (data: StoreModel | any) => {
    this.isFC = !data.dc;
    if (data.inBusinessSince) {
      data.inBusinessSince = data.inBusinessSince.replace(' ', 'T');
    }
    if (data.outBusinessSince) {
      data.outBusinessSince = data.outBusinessSince.replace(' ', 'T');
    }
    data.firstName = data.user ? data.user.firstName : '';
    data.lastName = data.user ? data.user.lastName : '';
    data.userTel = data.user ? data.user.tel : '';
    data.userEmail = data.user ? data.user.email : '';

    this.store = data;
    if (this.store && this.store.storeLocationCharges) {
      this.store.locationCharges = this.store.storeLocationCharges.map(value => value.countryLocation?.code) as string[];
    }
    this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, data));

    this.bindingContractFile();
    // giá trị bằng '0' checkbox vẫn đánh dấu, set lại rỗng
    this.addEditForm.get('status')?.setValue(this.addEditForm.get('status')?.value === '1' ? '1' : '');
    this.addEditForm.updateValueAndValidity();
    this.addEditForm.markAllAsTouched();
  }

  bindingContractFile() {
    const files = this.store?.storeFiles;
    if (files && files.length > 0) {
      const urls = files.map(value => {
        if (value && value.url) {
          const name = UploadModel.getFileName(value.url);
          const type = UploadModel.getFileType(value.url);
          const id = value.id ? value.id : undefined;
          const url = environment.BASE_FILE_URL + value.url;
          return new UploadModel(name, null, url, type, id);
        }
      });
      // console.log('urlsssssssssssssssssssss', urls);
      this.addEditForm.get('contractFiles')?.setValue(urls);
    }
  }

  changeCountry(countryId: any) {
    if (this.locationTree && this.locationTree.length > 0 && countryId) {
      this.regionOptions = this.locationTree.filter(country => country.id === countryId)[0].children
        .map((region: any) => new SelectModel(region.id, region.name));
      if (!this.initData) {
        this.addEditForm.patchValue({
          region: '',
          district: '',
          township: ''
        });
      }
    }
  }

  changeDistrict(districtId: any) {
    this.townshipOptions = this.townshipArr.filter(township => township.parent.id === districtId)
      .map(township => new SelectModel(township.id, township.name));
  }

  changeRegion(regionId: any) {
    this.districtOptions = this.districtArr.filter(district => district.parent.id === regionId)
      .map(district => new SelectModel(district.id, district.name));
  }

  changeSelectionTownship(event: any) {
    console.log('seleccccccccccccccccccccct', event);
  }

  onBack() {
    window.history.back();
  }

  updateScrumb(event: any) {
    switch (event.label) {
      case 'menu.store.edit':
        event.label = this.store?.code;
        break;
    }
  }

  onChangeTab(tabIndex: number) {
    if (tabIndex === 0) {
      this.storeService.selectedTabIndex.next(0);
    }
  }
}
