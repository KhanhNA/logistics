import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {HttpParams} from '@angular/common/http';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ColumnFields,
  ColumnTypes,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-store-inventory-pallet-details',
  templateUrl: './store-inventory-pallet-details.component.html',
  styleUrls: ['./store-inventory-pallet-details.component.scss']
})
export class StoreInventoryPalletDetailsComponent extends BaseAddEditLayout implements OnInit {
  moduleName = 'store.inventory.pallet.details';
  columns: ColumnFields[];

  constructor(private apiService: ApiService, @Inject(MAT_DIALOG_DATA) public data: any,
              activatedRoute: ActivatedRoute,
              location: Location,
              translateService: TranslateService,
              formBuilder: FormBuilder,
              protected authoritiesService: AuthoritiesService,
              utilsService: UtilsService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.addEditForm = formBuilder.group({
      detailPallet: ''
    });

    this.columns = [
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${this.getPosition(e, this.addEditForm.get('detailPallet')?.value)}`,
        cell: (e: any) => `${this.getPosition(e, this.addEditForm.get('detailPallet')?.value)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'pallet.code', header: 'pallet.code', title: (e: any) => `${e.pallet.code}`,
        cell: (e: any) => `${e.pallet.code}`,
        className: 'mat-column-pallet-code'
      },
      {
        columnDef: 'pallet.name', header: 'pallet.name', title: (e: any) => `${e.pallet.name}`,
        cell: (e: any) => `${e.pallet.name}`,
        className: 'mat-column-pallet-name'
      },
      {
        columnDef: 'quantity', header: 'quantity', title: (e: any) => `${Number(e.quantity).toLocaleString('en-US')}`,
        cell: (e: any) => `${Number(e.quantity).toLocaleString('en-US')}`,
        className: 'mat-column-quantity',
        columnType: ColumnTypes.VIEW // inputType: 'number'
      },
    ];
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = new HttpParams()
      .set('storeId', `${this.data.value.store.id}`)
      .set('productPackingId', `${this.data.value.productPackingId}`)
      .set('expireDate', `${this.data.value.expireDate}`);
    this.apiService.get('/stores/inventory/pallet-details', params).subscribe((data: any) => {
      this.addEditForm.get('detailPallet')?.setValue(data);
    });
  }

  getPosition(e: any, arr: any[]) {
    return arr && arr.length > 0 ? (arr.indexOf(e) + 1).toString() : '';
  }
}
