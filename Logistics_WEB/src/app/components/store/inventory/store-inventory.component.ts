import {AfterViewInit, Component, Injector, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {StoreModel} from '../../../_models/store.model';
import {NavService} from '../../../_services/nav.service';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {DistributorModel} from '../../../_models/distributor.model';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {StoreInventoryPalletDetailsComponent} from './pallet-details/store-inventory-pallet-details.component';
import {StoreInventoryModel} from '../../../_models/store.inventory.model';
import {ExpireDateColorsUtils} from '../../../base/Utils/ExpireDateColorsUtils';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../../../../environments/environment';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  FormStateService, IconTypeEnum,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ToastrService} from 'ngx-toastr';
import {PrintUtils} from '../../../base/Utils/print.utils';
import {ManufacturerModel} from "../../../_models/manufacturer.model";
import {ExportStatementAuthoritiesService} from "../../../_services/authority/export.statement.authorities.service";

@Component({
  selector: 'app-store-inventory',
  templateUrl: './store-inventory.component.html',
  styleUrls: ['./store-inventory.component.scss'],
  providers: [DatePipe, PrintUtils, ExportStatementAuthoritiesService]
})
export class StoreInventoryComponent extends BaseSearchLayout implements OnInit, AfterViewInit {
  moduleName = 'store.inventory';
  stores: SelectModel[] = [];
  distributors: SelectModel[] = [];
  manufacturers: SelectModel[] = [];
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  defaultCurrencyCode = 'MMK';

  expandColumnButton: ButtonFields = {
    columnDef: 'expandColumn',
    icon: 'filter_list',
    header: '',
    click: '',
    display: () => true
  };

  expireDateColorOptions = [
    new SelectModel(' ', this.translateService.instant('common.status.all')),
    new SelectModel('STILL_EXPIRY_DATE', this.translateService.instant('common.still.expiry.date')),
    new SelectModel('WARNING', this.translateService.instant('common.warning')),
    new SelectModel('OUT_OF_DATE', this.translateService.instant('common.out.of.date'))
  ];

  constructor(private snackBar: MatSnackBar,
              private formBuilder: FormBuilder,
              public dialog: MatDialog,
              protected router: Router,
              private printUtils: PrintUtils,
              protected apiService: ApiService,
              private datePipe: DatePipe,
              private tsService: TsUtilsService,
              private expireDateColorsUtils: ExpireDateColorsUtils,
              protected utilsService: UtilsService,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              private navService: NavService,
              protected uiStateService: FormStateService,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              protected exportStatementAuthoritiesService: ExportStatementAuthoritiesService,
              protected injector: Injector) {

    super(router, apiService, utilsService, uiStateService, translateService,
      injector, activatedRoute, authoritiesService, formBuilder.group({
        text: [''],
        store: [''],
        distributor: [' '],
        manufacturer: [' '],
        expireDateColor: [' '],
        productPackingCode: [''],
        productPackingName: ['']
      }));

    this.columns.push(
      {
        columnDef: 'checked',
        header: 'checked',
        title: (e: any) => `${e.checked}`,
        cell: (e: any) => `${e.checked}`,
        columnType: ColumnTypes.CHECKBOX,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'className',
        header: 'status',
        title: (e: any) => `${this.expireDateColorsUtils.getTranslated(e.className)}`,
        cell: (e: any) => `${this.expireDateColorsUtils.getIcon(e.className)}`,
        className: (e: any) => `mat-column-expired-class material-icons ${e.className}`
      },
      // {
      //   columnDef: 'qRCode',
      //   header: 'qRCode',
      //   title: () => '',
      //   cell: (e: any) => `${e.qRCode}`,
      //   columnType: ColumnTypes.BASE64,
      //   isExpandOptionColumn: () => true
      // },
      // {
      //   columnDef: 'store.code', header: 'store.code',
      //   title: (e: any) => `${e.store.code}`,
      //   cell: (e: any) => `${e.store.code}`,
      //   className: 'mat-column-store-code'
      // },
      {
        columnDef: 'store.label',
        header: 'store.label',
        title: (e: any) => `${e.store.code + ' - ' + e.store.name}`,
        cell: (e: any) => `${e.store.code + ' - ' + e.store.name}`,
        className: 'mat-column-store-name'
      },
      {
        columnDef: 'distributorName',
        header: 'distributorName',
        title: (e: StoreInventoryModel) => `${e.productPacking?.distributor?.code + ' - ' + e.productPacking?.distributor?.name}`,
        cell: (e: StoreInventoryModel) => `${e.productPacking?.distributor?.code + ' - ' + e.productPacking?.distributor?.name}`,
        isExpandOptionColumn: () => true
      },
      // {
      //   columnDef: 'palletType',
      //   header: 'palletType',
      //   title: (e: StoreInventoryModel) => `${e.productPacking?.barCode ? e.productPacking?.barCode : ''}`,
      //   cell: (e: StoreInventoryModel) => `${e.productPacking?.barCode ? e.productPacking?.barCode : ''}`,
      //   isExpandOptionColumn: () => true
      // },
      {
        columnDef: 'productPacking.code', header: 'productPacking.code', title: (e: any) => `${e.productPacking.code}`,
        cell: (e: any) => `${e.productPacking.code}`,
        className: 'mat-column-productPacking-code'
      },
      {
        columnDef: 'barCode',
        header: 'barCode',
        title: (e: StoreInventoryModel) => `${e.productPacking?.barcode ? e.productPacking?.barcode : ''}`,
        cell: (e: StoreInventoryModel) => `${e.productPacking?.barcode ? e.productPacking?.barcode : ''}`,
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'productPacking.product.name',
        header: 'product.name',
        title: (e: any) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking.product)}`,
        cell: (e: any) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking.product)}`,
        className: 'mat-column-product-name'
      },
      {
        columnDef: 'productPacking.packingType.quantity',
        header: 'packingType', title: (e: any) => `${e.productPacking.packingType.quantity}`,
        cell: (e: any) => `${e.productPacking.packingType.quantity}`,
        className: 'mat-column-packingType'
      },
      {
        columnDef: 'productPacking.uom',
        header: 'uom',
        title: (e: any) => `${e.productPacking.uom}`,
        cell: (e: any) => `${e.productPacking.uom}`,
        className: 'mat-column-uom'
      },
      {
        columnDef: 'product.manufacturer.name',
        header: 'manufacturer',
        title: (e: StoreInventoryModel) => `${e.product?.manufacturer?.code + ' - ' +
        e.product?.manufacturer?.manufacturerDescriptions?.filter(des => des.language?.code === this.translateService.currentLang)[0]?.name}`,
        cell: (e: StoreInventoryModel) => `${e.product?.manufacturer?.code + ' - ' +
        e.product?.manufacturer?.manufacturerDescriptions?.filter(des => des.language?.code === this.translateService.currentLang)[0]?.name}`,
        className: 'mat-column-manufacturer',
        isExpandOptionColumn: () => true
      },
      {
        columnDef: 'expireDate',
        header: 'expireDate',
        title: (e: any) => `${this.datePipe.transform(e.expireDate, environment.DIS_DATE_FORMAT, '-0')}`,
        cell: (e: any) => `${this.datePipe.transform(e.expireDate, environment.DIS_DATE_FORMAT, '-0')}`,
        className: 'mat-column-cell-expireDate'
      },
      {
        columnDef: 'totalQuantity',
        header: 'totalQuantity',
        title: (e: StoreInventoryModel) => `${Number(e.totalQuantity).toLocaleString('en-US')}`,
        cell: (e: StoreInventoryModel) => `${Number(e.totalQuantity).toLocaleString('en-US')}`,
        className: 'mat-column-totalQuantity'
      },
      // {
      //   columnDef: 'productPacking.price',
      //   header: 'productPacking.price',
      //   title: (e: StoreInventoryModel) => `${Number(this.getPackingPrice(e)).toLocaleString('en-US')}`,
      //   cell: (e: StoreInventoryModel) => `${Number(this.getPackingPrice(e)).toLocaleString('en-US')}`,
      //   className: 'mat-column-totalQuantity'
      // },
      // {
      //   columnDef: 'productPacking.currency',
      //   header: 'productPacking.currency',
      //   title: (e: StoreInventoryModel) => `${this.defaultCurrencyCode}`,
      //   cell: (e: StoreInventoryModel) => `${this.defaultCurrencyCode}`,
      //   className: 'mat-column-currency'
      // },
      // {
      //   columnDef: 'amount',
      //   header: 'amount',
      //   title: (e: StoreInventoryModel) => {
      //     const price = this.getPackingPrice(e);
      //     return (price && e.totalQuantity ? Number(price * e.totalQuantity).toLocaleString('en-US') : '') + '';
      //   },
      //   cell: (e: StoreInventoryModel) => {
      //     const price = this.getPackingPrice(e);
      //     return (price && e.totalQuantity ? Number(price * e.totalQuantity).toLocaleString('en-US') : '') + '';
      //   },
      //   className: 'mat-column-amount'
      // },
    );
    this.buttons.push(
      {
        columnDef: 'pallet-details',
        color: 'warn',
        header: 'common.action',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'info',
        click: 'palletDetails',
        title: `common.title.detail`,
        disabled: (e: any) => e && e.store.dc,
        display: (e: any) => this.authoritiesService.hasAuthority('get/stores/inventory/pallet-details')
      },
    );
  }

  ngAfterViewInit(): void {
    this.navService.title = 'menu.store.inventory';
  }

  async ngOnInit() {
    this.getFormDataFromApi().then(response => {
      console.log(response);
      response.distributor.then((distributors: DistributorModel[]) => {
        this.distributors = [];
        this.distributors.push(new SelectModel(' ', this.translateService.instant('common.status.all')));
        distributors.forEach(distributor => {
          this.distributors.push(new SelectModel(distributor.id, distributor.code + ' - ' + distributor.name));
        });
      });

      response.store.then((stores: Page) => {
        const storeModels = stores.content as StoreModel[];
        const storeIds: (number | null)[] = [];
        this.stores = [];
        storeModels.forEach(store => {
          storeIds.push(store.id);
          this.stores.push(new SelectModel(store.id, store.code + ' - ' + store.name));
        });

        if (this.exportStatementAuthoritiesService.hasCreateExportStatementRole()) {
          const par = new HttpParams().append('storeIds', storeIds.join(', '));
          this.apiService.get<StoreModel[]>('/stores/by-provide-store', par).subscribe((provideStores: StoreModel[]) => {
            this.stores = [...this.stores, ...provideStores.map(store => new SelectModel(store.id, store.code + ' - ' + store.name))];
            this.searchForm.get('store')?.setValue(this.stores[0] ? this.stores[0].value : '');
            this.onSubmit();
          });
        } else {
          this.searchForm.get('store')?.setValue(this.stores[0] ? this.stores[0].value : '');
          this.onSubmit();
        }
      });

      response.manufacturer.then((manufacturers: Page) => {
        this.manufacturers = [];
        this.manufacturers.push(new SelectModel(' ', this.translateService.instant('common.status.all')));
        manufacturers.content.forEach((manufacturer: ManufacturerModel) => {
          this.manufacturers.push(new SelectModel(manufacturer.id,
            manufacturer.code + ' - ' + manufacturer.manufacturerDescriptions?.filter(des => des.language?.code === this.translateService.currentLang)[0]?.name));
        });
      });
    });

  }

  search() {
    if (!this.searchForm.get('store')?.value) {
      return;
    }
    const params = new HttpParams()
      .set('text', this.searchForm.get('text')?.value)
      .set('storeId', this.searchForm.get('store')?.value ? this.searchForm.get('store')?.value.toString() : '')
      .set('exceptProductPackingIds', '-1')
      .set('productPackingCode', this.searchForm.get('productPackingCode')?.value)
      .set('productPackingName', this.searchForm.get('productPackingName')?.value)
      .set('expireDateColor', this.searchForm.get('expireDateColor')?.value ?
        this.searchForm.get('expireDateColor')?.value.toString().trim() : '')
      .set('manufacturerId', this.searchForm.get('manufacturer')?.value ?
        this.searchForm.get('manufacturer')?.value.toString().trim() : '');
    this._fillData('/stores/inventory', params);
  }

  onDistributorChange(value: any) {
    this.search();
  }

  onManufacturerChange(value: any) {
    this.search();
  }

  onExpireDateColorChange(value: any) {
    this.search();
  }

  onStoreChange(value: any) {
    this.search();
  }

  async getFormDataFromApi() {
    const params = new HttpParams()
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('status', 'true')
      .set('text', '')
      .set('pageNumber', '1')
      .set('pageSize', '69');
    const store = this.apiService.get<Page>('/stores', params).toPromise();
    const distributor = this.tsService.getDistributors().toPromise();
    const manufacturer = this.tsService.getAllManufacturers().toPromise();
    await store;
    await distributor;
    await manufacturer;
    return {store, distributor, manufacturer};
  }

  palletDetails(storeInventory: StoreInventoryModel) {
    this.dialog.open(StoreInventoryPalletDetailsComponent, {
      disableClose: false,
      data: {
        value: storeInventory
      }
    });
  }

  printQR() {
    console.log(this.results.data);
    const checkedArr = this.results.data.filter(inventory => inventory.checked);
    this.printUtils.printQRInventory(checkedArr);
  }

  getPackingPrice(inventory: StoreInventoryModel) {
    return inventory.productPacking?.productPackingPrices?.filter(price => price.currency?.code === this.defaultCurrencyCode)[0]
      ? inventory.productPacking?.productPackingPrices?.filter(price => price.currency?.code === this.defaultCurrencyCode)[0].price : '';
  }
}
