import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {LDBaseLayout} from '../../base/l-d-base.layout';
import {FieldConfigExt} from '../../base/field/field.interface';
import {Search} from '../../_models/Search';
import {DatePipe, Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {AfterViewInit, Directive} from '@angular/core';
import {AsyncDataUtils} from '../../_utils/async.data.utils';
import {DateUtils} from '../../base/Utils/date.utils';
import {CookieService} from 'ngx-cookie-service';
import {environment} from '../../../environments/environment';
import {ApiService, AuthoritiesService, BaseTableLayout, DateUtilService} from '@next-solutions/next-solutions-base';
import {PoStatus, PoStatusDescription} from '../../_models/action-type';
import {PoModel} from '../../_models/po/po.model';


@Directive()
export class LDPoLayout extends LDBaseLayout implements AfterViewInit {
  readonly orderByMap: any = {
    createDate: 'create_date', createUser: 'create_user', 'manufacturer.code': 'manufacturer.code',
    'manufacturer.name': 'manufacturer.name', code: 'code', description: 'description',
    status: 'status', approveDate: 'approve_Date', approveUser: 'approve_User'
  };
  inputFields: FieldConfigExt[] | undefined;
  cols: FieldConfigExt[] | undefined;
  storesCollection: any[] = [];
  distributorCollection: any = [];
  hasState = false;
  private approveDateCol = new FieldConfigExt({
    type: 'view',
    label: 'common.approveDate',
    inputType: 'text',
    name: 'approveInfo'
  });
  private importDateCol = new FieldConfigExt({
    type: 'view',
    label: 'common.importDate',
    inputType: 'text',
    name: 'importInfo'
  });
  poStatusOptions = {};
  private arrivedVietnamPortCol = new FieldConfigExt({
    type: 'view',
    label: 'common.arrivedVietnamPortDate',
    inputType: 'text',
    name: 'arrivedVietnamPortDateString'
  });
  arrivedMyanmarPortCol =
    new FieldConfigExt({
      type: 'view',
      label: 'common.arrivedMyanmarPortDate',
      inputType: 'text',
      name: 'arrivedMyanmarPortDateString'
    });
  arrivedFcDateCol = new FieldConfigExt({
    type: 'view',
    label: 'common.arrivedFcDate',
    inputType: 'text',
    name: 'arrivedFcDateString'
  });

  constructor(tsuService: TsUtilsService, private userService: ApiService, fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              transService: TranslateService,
              toastr: ToastrService,
              location: Location,
              private cookieService: CookieService,
              private dateUtilService: DateUtilService,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    Object.keys(PoStatus).forEach(key => this.poStatusOptions[key] = PoStatusDescription[key]);
    console.log(this.poStatusOptions);
    this.getFormDataBinding();
  }

  getFormDataBinding() {
    this.getAsyncData().then(data => {
      this.distributorCollection.push({
        code: '- ' + this.translateService.instant('common.status.all'),
        name: '',
        id: ''
      });
      data.stores.then(response => {
        this.storesCollection.push(...response.content);
      });
      data.distributors.then(distributors => {
        this.distributorCollection.push(...distributors);
      });
    }).then(() => {
      this.inputFields = [
        new FieldConfigExt({
          type: 'input',
          label: 'common.text',
          inputType: 'text',
          name: 'text',
          value: '',
          binding: 'text'
        }),
        new FieldConfigExt({
          type: 'autocomplete',
          label: 'pallet.ae.store.name',
          inputType: 'text',
          name: 'store',
          value: '',
          options: ['code', 'name'],
          collections: this.storesCollection,
          binding: 'store',
          require: 'true'
        }),
        new FieldConfigExt({
          type: 'autocomplete',
          label: 'po.ae.distributor.code',
          inputType: 'text',
          name: 'distributor.code',
          value: '',
          options: ['code', 'name'],
          collections: this.distributorCollection,
          binding: 'distributor',
        }),
        new FieldConfigExt({
          type: 'select',
          name: 'status',
          label: 'common.status',
          inputType: 'input',
          binding: 'status',
          options: this.poStatusOptions
        }),
        new FieldConfigExt({
          type: 'select',
          name: 'dateType',
          label: 'common.dateType',
          inputType: 'input',
          binding: 'dateType',
          options: this.poStatusOptions
        }),
        new FieldConfigExt({
          type: 'date',
          label: 'common.fromDate',
          inputType: 'date',
          name: 'fromDate',
          binding: 'fromDate',
          options: {max: ['toDate']},
          require: 'true'

        }),
        new FieldConfigExt({
          type: 'date',
          label: 'common.toDate',
          inputType: 'date',
          name: 'toDate',
          binding: 'toDate',
          options: {min: ['fromDate'], max: ['curDate']},
          require: 'true'
        })
      ];
      this.cols = [
        new FieldConfigExt({
          type: 'checkboxAll',
          inputType: 'all',
          name: 'chkbox'
        }),
        new FieldConfigExt({
          type: 'index',
          label: 'ld.no',
          inputType: 'text',
          name: 'STT',
          value: '',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'po.ae.code',
          inputType: 'text',
          name: 'code',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'po.ae.manufacturer.code',
          inputType: 'text',
          name: 'manufacturer.code',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'po.ae.description',
          inputType: 'text',
          name: 'description',

        }),
        new FieldConfigExt({
          type: 'view',
          label: 'common.total',
          inputType: 'number',
          name: 'total',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'common.status',
          inputType: 'select',
          name: 'statusString',
          options: this.poStatusOptions
        }),

        new FieldConfigExt({
          type: 'view',
          label: 'common.createDate',
          inputType: 'text',
          name: 'createInfo'
        }),
        this.approveDateCol,
        this.arrivedVietnamPortCol,
        this.arrivedMyanmarPortCol,
        this.arrivedFcDateCol,

        this.importDateCol,
        new FieldConfigExt({
          type: 'btnGrp',
          label: 'add',
          inputType: 'text',
          name: 'add',
          require: 'true',
          value: ['edit', 'add_shopping_cart', 'picture_as_pdf', 'compare_arrows'],
          authorities: {
            add: ['post/pos'],
            edit: ['patch/pos/{id}'],
            add_shopping_cart: ['post/import-pos'],
            picture_as_pdf: ['post/pos/print-pdf/{id}'],
            compare_arrows: ['patch/pos/{id}/arrived-fc', 'patch/pos/{id}/arrived-myanmar-port', 'patch/pos/{id}/arrived-vietnam-port']
          },
          titles: {
            add: 'common.title.add',
            edit: 'common.title.edit',
            add_shopping_cart: 'common.title.gr',
            picture_as_pdf: 'common.title.picture_as_pdf',
            compare_arrows: 'common.title.compare_arrows'
          }

        })
      ];
      this.setCols(this.cols);
      this.setSelection(true);
      this.init(this.inputFields);

      if (!this.hasState) {
        this.formData = {
          store: this.storesCollection[0] ? this.storesCollection[0] : '',
          distributor: this.distributorCollection[0] ? this.distributorCollection[0] : '',
          text: '',
          fromDate: DateUtils.getDayBeforeOneMonth(),
          toDate: DateUtils.getCurrentDate(),
          status: this.onDisplayWithAuthority('post/import-pos') ? PoStatus.ARRIVED_MYANMAR_PORT :
            this.onDisplayWithAuthority('patch/pos/{id}/arrived-fc') ? PoStatus.ARRIVED_VIETNAM_PORT :
              this.onDisplayWithAuthority(['patch/pos/{id}/arrived-vietnam-port', 'patch/pos/{id}/arrived-myanmar-port'])
                ? PoStatus.APPROVED : PoStatus.NEW,
          dateType: PoStatus.NEW
        };
        this.onSearchData();

      }
    });
  }

  async getAsyncData() {
    return AsyncDataUtils.getAsyncDataStoresAndDistributors(this.tsuService, this.tsuService);
  }

  convertToRows(data: any) {
    const rows = [];
    let row;
    if (data) {
      for (const item of Object.keys(data)) {
        row = {
          quantity: 0,
          amount: 0,
          packingProduct: {
            packingProductId: data[item].packingProductId,
            code: data[item].code,
            name: data[item].name
          }
        };
        rows.push(row);
      }
    }
    return rows;
  }

  getOrderBy() {
    const orderBy = this.orderBy;
    if (!orderBy || !orderBy.direction) {
      return '';
    }
    return this.orderByMap[orderBy.active] + ',' + orderBy.direction.toUpperCase();
  }

  updateSearchInfo(search: Search) {
    const fromDate = this.datePipe.transform(this.formData.fromDate, environment.API_DATE_FORMAT, '-0');
    const toDate = this.datePipe.transform(this.formData.toDate, environment.API_DATE_FORMAT, '-0');
    if (fromDate !== null && toDate != null) {
      const params = new HttpParams()
        .set('storeId', this.formData.store ? this.formData.store.id : '')
        .set('distributorId', this.formData.distributor ? this.formData.distributor.id : '')
        .set('text', this.formData.text ? this.formData.text : '')
        .set('fromDate', fromDate)
        .set('toDate', toDate)
        .set('status', this.formData.status ? this.formData.status : PoStatus.NEW)
        .set('dateType', this.formData.dateType ? this.formData.dateType : 0)
        .set('pageNumber', '' + (search.pageNumber + 1))
        .set('pageSize', '' + search.pageSize)
        .set('orderBy', '' + this.getOrderBy());
      search.method = this.userService.get('/pos', params);
      return true;
    } else {
      this.tsTable.removeRows(0, this.tsTable.dataSrc.data.length);
      return false;
    }
  }

  beforeUpdate = (data: any) => {
    this.search.pageSize = data.size;
    this.search.pageNumber = data.number;
    this.search.totalElements = data.totalElements;
    const status = data.content[0] ? data.content[0].status : '';
    data.content.forEach((po: PoModel | any) => {
      if (this.poStatusOptions[po.status]) {
        po.statusString = this.translateService.instant(this.poStatusOptions[po.status]);
      }

      po.createInfo = po.createDateString + ' - ' +
        po.createUserObj.firstName + ' ' + po.createUserObj.lastName;

      if (po.approveUserObj) {
        po.approveInfo = this.dateUtilService.convertDateToDisplayGMT0(po.approveDate) + ' - ' +
          po.approveUserObj.firstName + ' ' + po.approveUserObj.lastName;
      } else {
        po.approveInfo = '';
      }

      if (po.arrivedVietnamPortUserObj) {
        po.arrivedVietnamPortDateString = po.arrivedVietnamPortDateString + ' - ' +
          po.arrivedVietnamPortUserObj.firstName + ' ' + po.arrivedVietnamPortUserObj.lastName;
      }
      if (po.arrivedMyanmarPortUserObj) {
        po.arrivedMyanmarPortDateString = po.arrivedMyanmarPortDateString + ' - ' +
          po.arrivedMyanmarPortUserObj.firstName + ' ' + po.arrivedMyanmarPortUserObj.lastName;
      }
      if (po.arrivedFcUserObj) {
        po.arrivedFcDateString = po.arrivedFcDateString + ' - ' +
          po.arrivedFcUserObj.firstName + ' ' + po.arrivedFcUserObj.lastName;
      }

      if (po.importUserObj) {
        po.importInfo = this.dateUtilService.convertDateToDisplayGMT0(po.importDate) + ' - ' +
          po.importUserObj.firstName + ' ' + po.importUserObj.lastName;
      } else {
        po.importInfo = '';
      }

    });
    this.updateColByStatus(status);
  };

  updateColByStatus(status: PoStatus) {
    if (!this.cols) {
      return;
    }
    let displayCol = [...this.cols];
    if (status === PoStatus.NEW || status === PoStatus.REJECT) {
      displayCol.splice(this.cols.indexOf(this.approveDateCol), 5);
    } else if (status === PoStatus.APPROVED) {
      displayCol.splice(this.cols.indexOf(this.arrivedVietnamPortCol), 4);
    } else {
      displayCol = this.cols;
    }

    this.setCols(displayCol);

  }

  printPOs = (data: any) => {
    const ids = [];
    if (!this.tsTable.allSelect) {
      return;
    }
    if (this.tsTable.allSelect) {
      for (const key of Object.keys(this.tsTable.allSelect)) {
        ids.push(this.tsTable.allSelect[key].id);
      }
    }
    if (ids.length === 0) {
      return;
    }

    this.userService.saveFile(`/pos/print`, ids, {
      headers: undefined,
      params: undefined
    });
  }

  onPrintPOs = (data: any) => {
    const ids = [];
    if (this.tsTable.allSelect) {
      for (const key of Object.keys(this.tsTable.allSelect)) {
        ids.push(this.tsTable.allSelect[key].id);
      }
    }
    if (ids.length === 0) {
      this.showWarningMsg('notice.checkbox.nocheck');
      return;
    }
    this.tsuService.execute6(null, this.printPOs, 'common.confirmPrint', ['common.PO']);
  };

  ngAfterViewInit(): void {
    console.log(this.formData.store);
  }
}
