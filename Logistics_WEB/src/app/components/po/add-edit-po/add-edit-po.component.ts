import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  DateUtilService,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ManufacturerModel} from '../../../_models/manufacturer.model';
import {ManufacturerDescriptionModel} from '../../../_models/manufacturer.description.model';
import {StoreModel} from '../../../_models/store.model';
import {DistributorModel} from '../../../_models/distributor.model';
import {CurrencyModel} from '../../../_models/currency.model';
import {PoModel} from '../../../_models/po/po.model';
import {PoDetailModel} from '../../../_models/po/po.detail.model';
import {ActionTypeEneum, ClaimEnum, ImportPoStatus, PoStatus} from '../../../_models/action-type';
import {ProductPackingsComponent} from '../../dialog-select-product-packing/product-packings.component';
import {MatDialog} from '@angular/material/dialog';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {ToastrService} from 'ngx-toastr';
import {DateUtils} from '../../../base/Utils/date.utils';
import {ImportPoModel} from '../../../_models/import-po/import.po.model';
import {DividePackingComponent} from '../../divide-packing/divide-packing.component';
import {ClaimDetailModel} from '../../../_models/claim.detail.model';
import {ExchangeRateModel} from '../../../_models/exchangeRateModel';


@Component({
  selector: 'app-add-edit-po',
  templateUrl: './add-edit-po.component.html',
  styleUrls: ['./add-edit-po.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddEditPoComponent extends BaseAddEditLayout implements AfterViewInit, OnInit {
  moduleName = 'po.ae';

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  manufacturerOptions: SelectModel[] = [];
  storeOptions: SelectModel[] = [];
  distributorOptions: SelectModel[] = [];
  currencyOptions: SelectModel[] = [];
  toCurrencyOptions: SelectModel[] = [];

  exchangeRate: ExchangeRateModel | undefined;
  po: PoModel | undefined;
  parentId: string | null;
  currentLangCode: string;
  actionType: string | null | ActionTypeEneum;

  isPendingApprove = false;
  hasClaim = false;
  claimId: number | undefined;

  errorDeliveryDateMessages = new Map().set('min', () => this.translateService.instant('common.error.date.min'))
    .set('required', () => this.translateService.instant('common.required'));
  errorDeliveryAddressMessages = new Map().set('required', () => this.translateService.instant('common.required'));
  errorExchangeRateMessages = new Map().set('pattern', () => this.translateService.instant('validation.minExchangeRate'))
    .set('required', () => this.translateService.instant('common.required'));

  get PoStatus() {
    return PoStatus;
  }

  get ActionType() {
    return ActionTypeEneum;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              private dialog: MatDialog,
              private router: Router,
              private dateUtilService: DateUtilService,
              private toastr: ToastrService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.parentId = this.activatedRoute.snapshot.paramMap.get('id');
    this.actionType = this.activatedRoute.snapshot.paramMap.get('actType');
    this.currentLangCode = this.translateService.currentLang ?
      this.translateService.currentLang : this.translateService.defaultLang;
    this.defineColumnTable();

  }

  defineColumnTable() {
    this.columns.push(...[
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${(this.addEditForm.get('poDetails')?.value.indexOf(e) + 1).toLocaleString('en-US')}`,
        cell: (e: any) => `${(this.addEditForm.get('poDetails')?.value.indexOf(e) + 1).toLocaleString('en-US')}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
        isShowHeader: true
      },
      {
        columnDef: 'productPacking.code',
        header: 'productPacking.code',
        title: (e: PoDetailModel) => `${e.productPacking?.code}`,
        cell: (e: PoDetailModel) => `${e.productPacking?.code}`,
        isShowHeader: true
      },
      {
        columnDef: 'product.name',
        header: 'product.name',
        title: (e: PoDetailModel) => `${e ? e.product?.productDescriptions?.filter(description =>
          description.language?.code === this.currentLangCode)[0].name : ''}`,
        cell: (e: PoDetailModel) => `${e ? e.product?.productDescriptions?.filter(description =>
          description.language?.code === this.currentLangCode)[0].name : ''}`,
        isShowHeader: true
      },
      {
        columnDef: 'productPacking.uom',
        header: 'productPacking.uom',
        title: (e: PoDetailModel) => `${e.productPacking?.uom}`,
        cell: (e: PoDetailModel) => `${e.productPacking?.uom}`,
        isShowHeader: true
      },
      {
        columnDef: 'productPacking.packingType.quantity',
        header: 'productPacking.packingType.quantity',
        title: (e: PoDetailModel) => `${e.productPacking?.packingType?.quantity?.toLocaleString('en-US')}`,
        cell: (e: PoDetailModel) => `${e.productPacking?.packingType?.quantity?.toLocaleString('en-US')}`,
        isShowHeader: true
      },
      {
        columnDef: 'orgQuantity',
        header: 'orgQuantity',
        columnType: ColumnTypes.VIEW,
        title: (e: any) => `${Number(e.orgQuantity).toLocaleString('en-US')}`,
        cell: (e: any) => `${Number(e.orgQuantity).toLocaleString('en-US')}`,
        display: (e: any) => this.hasClaim,
        isShowHeader: this.hasClaim,
      },
      {
        columnDef: 'claimQuantity',
        header: 'claimQuantity',
        columnType: ColumnTypes.VIEW,
        title: (e: any) => `${Number(e.claimQuantity).toLocaleString('en-US')}`,
        cell: (e: any) => `${Number(e.claimQuantity).toLocaleString('en-US')}`,
        display: (e: any) => this.hasClaim,
        isShowHeader: this.hasClaim,
      },
      {
        columnDef: 'quantity',
        header: (e: any) => this.hasClaim ? this.translateService.instant('common.quantity.real.import')
          : this.translateService.instant('common.quantity'),
        columnType: (this.actionType === ActionTypeEneum.new || this.actionType === ActionTypeEneum.edit) ? ColumnTypes.INPUT_COUNTER
          : ColumnTypes.VIEW,
        title: (e: PoDetailModel) => `${Number(e.quantity).toLocaleString('en-US')}`,
        cell: (e: PoDetailModel) => `${Number(e.quantity).toLocaleString('en-US')}`,
        disabled: (e: any) => this.actionType !== ActionTypeEneum.edit && this.actionType !== ActionTypeEneum.new
          || (!!this.po && this.po.status === PoStatus.APPROVED.toString() && this.actionType === ActionTypeEneum.edit),
        isShowHeader: true,
        min: (e: any) => 1,
        isRequired: !(this.actionType !== ActionTypeEneum.edit && this.actionType !== ActionTypeEneum.new
          || (this.po && this.po.status === PoStatus.APPROVED.toString() && this.actionType === ActionTypeEneum.edit)),
        errorMessage: new Map<string, () => string>()
          .set('min', () => 'validation.minQuantity')
          .set('pattern', () => 'validation.quantity.pattern.number')
          .set('required', () => this.translateService.instant('common.required')),
        onCellValueChange: (e: any) => {
          console.log(this.addEditForm);
        },
        validate: (e: any) => {
          // if (e.quantity <=0){
          //   return {min: {min: 1, actual: e.quantity}};
          // }
          return null;
        }
      },
      {
        columnDef: 'productPackingPrice.price',
        header: 'productPackingPrice.price',
        title: (e: PoDetailModel) => `${e.productPackingPrice?.price ? e.productPackingPrice.price.toLocaleString('en-US') : ''}`,
        cell: (e: PoDetailModel) => `${e.productPackingPrice?.price ? e.productPackingPrice.price.toLocaleString('en-US') : ''}`,
        isShowHeader: true,
      },
      {
        columnDef: 'productPackingPrice.currency.name',
        header: 'productPackingPrice.currency.name',
        title: (e: PoDetailModel) => `${e.productPackingPrice?.currency ? e.productPackingPrice.currency.name : ''}`,
        cell: (e: PoDetailModel) => `${e.productPackingPrice?.currency ? e.productPackingPrice.currency.name : ''}`,
        isShowHeader: true
      },
      {
        columnDef: 'total',
        header: 'total',
        title: (e: any) => e.productPackingPrice && e.productPackingPrice.productPacking ?
          ((e.productPackingPrice.price * (100 + e.productPackingPrice.productPacking.vat) * Number(e.quantity)) / 100).toLocaleString('en-US')
          : '',
        cell: (e: any) => e.productPackingPrice && e.productPackingPrice.productPacking ?
          ((e.productPackingPrice.price * (100 + e.productPackingPrice.productPacking.vat) * Number(e.quantity)) / 100).toLocaleString('en-US')
          : '',
        isShowHeader: true
      },
      {
        columnDef: 'expireDate',
        header: 'expireDate',
        columnType: this.actionType === ActionTypeEneum.GR ? ColumnTypes.DATE_PICKER : ColumnTypes.VIEW,
        min: (e: any) => this.onDisabledExpiredDate(e) ? null
          : (e.isClaimAll && e.expireDate) ? (DateUtils.getDayBeforeNow(-1) < new Date(e.expireDate)
            ? DateUtils.getDayBeforeNow(-1) : new Date(e.expireDate)) : DateUtils.getDayBeforeNow(-1),
        max: (e: any) => this.onDisabledExpiredDate(e) ? new Date(9999, 12, 31)
          : this.dateUtilService.addDays(DateUtils.getCurrentDate_0H(), e.product.lifecycle),
        disabled: (e: any) => {
          return this.onDisabledExpiredDate(e);
        },
        errorMessage: new Map<string, () => string>()
          .set('required', () => this.translateService.instant('common.required'))
          .set('min', () => this.translateService.instant('common.error.date.min')),
        title: (e: any) => e.expireDate ? this.dateUtilService.convertDateToDisplayGMT0(e.expireDate) + '' : '',
        cell: (e: any) => e.expireDate ? this.dateUtilService.convertDateToDisplayGMT0(e.expireDate) + '' : '',
        display: (e: any) => (this.po && this.po.status === PoStatus.IMPORTED)
          || this.actionType === ActionTypeEneum.GR,
        onCellValueChange: (e: any) => {
          // const curDate = new Date();
          // const expireDate = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate()
          //   + e.product.lifecycle, 0, 0, 0);
          // if (new Date('' + e.expireDate) > expireDate) {
          //   this.showWarningMsg('date.pick.not.gather.than.life.cycle');
          //   e.expireDate = '';
          // }
        },
        validate: (e: any) => {
          return null;
        },
        isRequired: this.actionType === ActionTypeEneum.GR
      }
    ]);

    this.buttons.push(...[
      {
        columnDef: 'delete',
        color: 'warn',
        icon: 'fa fa-times',
        click: 'onDeleteDetail',

        display: (e: any) => e && this.authoritiesService.hasAuthority('post/pos')
          && ![ActionTypeEneum.GR.toString(), ActionTypeEneum.view.toString()].includes(this.actionType + '')
          && this.isShowOrDisabledTblButton({act: 'delete', row: e}),
        header: {
          columnDef: 'add',
          color: 'warn',
          icon: 'add',
          click: 'onAddDetail',
          display: (e: any) => (!e || this.hasAuthority()) && this.authoritiesService.hasAuthority('get/product-packings')
            && ![ActionTypeEneum.GR.toString(), ActionTypeEneum.view.toString()].includes(this.actionType + '')
            && this.isShowOrDisabledTblButton({act: 'add', row: e}),
          disabled: (e: any) => {
            return !this.addEditForm.get('manufacturer')?.value;
          }
        },
      },
      {
        columnDef: 'add_box',
        color: 'warn',
        icon: 'add_box',
        click: 'divideDetail',
        isShowHeader: false,
        display: (e: any) => e && this.authoritiesService.hasAuthority('post/import-pos')
          && this.actionType === ActionTypeEneum.GR,
        disabled: (e: any) => !this.isShowOrDisabledTblButton({act: 'add_box', row: e}) || this.isPendingApprove
      },
      {
        columnDef: 'clear',
        color: 'warn',
        icon: 'clear',
        click: 'removeDetail',
        isShowHeader: false,
        display: (e: any) => e && this.authoritiesService.hasAuthority('post/import-pos')
          && this.actionType === ActionTypeEneum.GR,
        disabled: (e: any) => !this.isShowOrDisabledTblButton({act: 'clear', row: e}) || this.isPendingApprove
      }
    ]);
  }

  onDisabledExpiredDate(e: any | PoDetailModel) {
    return ((this.po && this.po.status === PoStatus.IMPORTED)
      || this.actionType === ActionTypeEneum.view) || e.isClaimAll || this.isPendingApprove;
  }

  isShowOrDisabledTblButton(event: { act: any, row: any }): boolean {
    if (!this.addEditForm.get('manufacturer')?.value) {
      return false;
    }

    if (this.actionType === ActionTypeEneum.edit && this.po?.status === PoStatus.APPROVED.toString()) {
      return false;
    }

    if (this.actionType === ActionTypeEneum.new) {
      return ['delete', 'add'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.GR) {
      if (event.act === 'add') {
        return false;
      }

      if (event.act === 'clear' && this.addEditForm.get('poDetails')) {
        const ind = this.addEditForm.get('poDetails')?.value.indexOf(event.row);
        if ((this.addEditForm.get('poDetails')?.value[ind - 1] && this.addEditForm.get('poDetails')?.value[ind - 1].id === event.row.id) ||
          (this.addEditForm.get('poDetails')?.value[ind + 1] && this.addEditForm.get('poDetails')?.value[ind + 1].id === event.row.id)) {
          return true;
        } else {
          return false;
        }
      }

      if (event.row.quantity <= 1) {
        return ['clear'].includes(event.act);
      }
      return ['add_box', 'clear'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.editPo) {
      return !['add_box', 'clear'].includes(event.act);
    }

    if (this.actionType === ActionTypeEneum.Pal
      || this.actionType === ActionTypeEneum.view) {
      return false;
    }
    return false;
  }

  updateAfterGetDetail = (data: any) => {
    if (((data.status === PoStatus.ARRIVED_MYANMAR_PORT.toString() && this.actionType === ActionTypeEneum.GR))
      || data.status === PoStatus.IMPORTED.toString()) {
      this.isPendingApprove = false;
      if (this.onDisplayWithAuthority('get/claims/by-reference')) {
        const params = new HttpParams()
          .set('type', ClaimEnum.IMPORT_PO)
          .set('referenceId', '' + data.id);
        this.apiService.get<ClaimDetailModel[]>('/claims/by-reference', params)
          .subscribe((claimDetails: ClaimDetailModel[]) => {
              this.getClaimSuccess(claimDetails);
            },
            (error: any) => {
              this.isPendingApprove = true;
              this.showErrorMsg(error);
              this.bindingData(data);
            });
      } else {
        this.bindingData(data);
      }

    } else {
      this.bindingData(data);
    }
  }

  getClaimSuccess = (claimDetails: ClaimDetailModel[]) => {
    const claimMap = new Map<number, any>();
    const claimMapForImportPo = new Map<string, ClaimDetailModel>();
    if (claimDetails.length > 0) {
      this.claimId = claimDetails[0].claim?.id;
      this.hasClaim = true;
      let key = '';
      claimDetails.forEach((claimDetail: ClaimDetailModel) => {
        if (claimDetail.productPacking && claimDetail.productPacking.id) {
          if (claimMap.has(claimDetail.productPacking.id)) {
            const detail = claimMap.get(claimDetail.productPacking.id);
            detail.quantity = +detail.quantity
              + Number(claimDetail.quantity);
          } else {
            claimMap.set(claimDetail.productPacking.id, claimDetail);
          }

          key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
          if (claimMapForImportPo.has(key)) {
            const claimDetailMap = claimMapForImportPo.get(key);
            if (claimDetailMap) {
              claimDetailMap.quantity = Number(claimDetailMap.quantity) + Number(claimDetail.quantity);
              claimMapForImportPo.set(key, claimDetailMap);
            }
          } else {
            claimMapForImportPo.set(key, claimDetail);
          }
        }

      });
    }

    this.po?.poDetails?.forEach((poDetail: any) => {
      if (claimMap.has(poDetail.productPacking.id)) {
        poDetail.orgQuantity = poDetail.quantity;
        poDetail.claimQuantity = Number(claimMap.get(poDetail.productPacking.id).quantity);
        poDetail.quantity = Number(poDetail.quantity) - Number(claimMap.get(poDetail.productPacking.id).quantity);
        if (poDetail.quantity === 0) {
          poDetail.expireDate = claimMap.get(poDetail.productPacking.id).expireDate;
          poDetail.isClaimAll = true;
        }
      } else {
        poDetail.orgQuantity = poDetail.quantity;
        poDetail.claimQuantity = 0;
      }
    });

    this.bindingData(this.po, claimMapForImportPo);
    // this.tsuService.hideLoading();
  }

  bindingData(data: any, claimMapForImport?: Map<string, any>) {

    if (data.status === PoStatus.IMPORTED) {
      const map = new Map();
      let key = '';
      data.importPo.importPoDetails.forEach((importPoDetail: any) => {
        key = '' + importPoDetail.expireDate + importPoDetail.productPacking.id;
        if (map.has(key)) {
          const detail = map.get(key);
          detail.quantity = Number(detail.quantity) + Number(importPoDetail.quantity);
          detail.orgQuantity = Number(detail.orgQuantity) + Number(importPoDetail.quantity);
        } else {
          if (claimMapForImport) {
            if (claimMapForImport.has(key)) {
              importPoDetail.claimQuantity = claimMapForImport.get(key).quantity;
              claimMapForImport.delete(key);
            } else {
              importPoDetail.claimQuantity = 0;
            }
            importPoDetail.orgQuantity = importPoDetail.quantity + importPoDetail.claimQuantity;
          }
          map.set(key, importPoDetail);
        }
      });
      const dataTable = [];
      if (claimMapForImport) {
        for (const claim of claimMapForImport.values()) {
          claim.orgQuantity = claim.quantity;
          claim.claimQuantity = claim.quantity;
          claim.quantity = claim.orgQuantity - claim.claimQuantity;
          claim.product = claim.productPacking.product;
        }
        dataTable.push(...claimMapForImport.values());
      }
      dataTable.push(...map.values());
      this.addEditForm.get('poDetails')?.setValue(dataTable);
      return;
    }
    data.poDetails.forEach((poDetail: any) => {
      // ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(poDetail.product);
      poDetail.poDetail = {id: poDetail.id, quantity: poDetail.quantity};
      poDetail.maxQuantity = poDetail.quantity;
      poDetail.productPackingPrices = poDetail.productPacking.productPackingPrices;

    });
    this.addEditForm.get('poDetails')?.setValue(data.poDetails);
  }

  async ngOnInit() {
    super.ngOnInit();
    this.addEditForm = this.formBuilder.group({
      manufacturer: ['', [Validators.required]],
      store: ['', [Validators.required]],
      distributor: ['', [Validators.required]],
      arrivedVietnamPortDate: [''],
      arrivedMyanmarPortDate: [''],
      arrivedFcDate: [''],
      deliveryAddress: ['', [Validators.required]],
      fromCurrency: ['', [Validators.required]],
      exchangeRate: ['',
        [Validators.pattern('^(0*[1-9][0-9]*(\\.[0-9]+)?|0+\\.[0-9]*[1-9][0-9]*)$')]],
      toCurrency: ['', [Validators.required]],
      description: [''],
      rejectReason: '',
      poDetails: ['']
    });

    this.getFormSourceData().then();

    if (this.isEdit) {
      this.po = await this.apiService.get('/pos/' + this.parentId, new HttpParams()).toPromise() as PoModel;
      this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, this.po));
      if (this.po.poDetails && this.po.poDetails.length > 0) {
        const poDetailCurrencyId = this.po.poDetails[0].productPackingPrice?.currency?.id;
        this.addEditForm.get('fromCurrency')?.setValue(poDetailCurrencyId);

        this.updateAfterGetDetail(this.po);
        // this.addEditForm.markAllAsTouched();
        // this.po.poDetails.forEach((poDetail: any) => {
        //   // ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(poDetail.product);
        //   poDetail.poDetail = {id: poDetail.id, quantity: poDetail.quantity};
        //   poDetail.maxQuantity = poDetail.quantity;
        //   poDetail.productPackingPrices = poDetail.productPacking.productPackingPrices;
        // });
      }
    }
  }

  ngAfterViewInit(): void {
    // if (this.isEdit) {
    //   this.addEditForm.markAllAsTouched();
    // }
  }

  divideDetail(row: PoDetailModel, index: number) {
    const shallowCopy = {...row};
    this.dialog.open(DividePackingComponent, {
      disableClose: false,
      height: '38%',
      width: '40vw',
      data: {packing: shallowCopy, type: ActionTypeEneum.GR}
    }).afterClosed().subscribe(response => {
      if (response) {
        this.dividePacking(response.data, {ind: index, row});
      }
    });
  }

  dividePacking(dividedPacking: any, originPackingRow: any) {
    const tblData = this.addEditForm.get('poDetails')?.value;
    const length = tblData.length;
    const dataTable = [];
    for (let i = 0; i < length; i++) {
      if (tblData[i].expireDate) {
        // const arr = tblData[i].expireDate.split(' ');
        // console.log(tblData[i].expireDate, new Date(tblData[i].expireDate));
        // tblData[i].expireDate = DateUtils.getDateFromString_0H(tblData[i].expireDate);
        // tblData[i].expireDate =
        // this.datePipe.transform(DateUtils.getDateFromString_0H(tblData[i].expireDate), environment.API_DATE_FORMAT);
        tblData[i].expireDate = tblData[i].expireDate.replace(' ', 'T');
        // console.log(tblData[i].expireDate);
      }
      dataTable.push(tblData[i]);
      if (i === originPackingRow.ind) {
        if (dividedPacking.expireDate) {
          dividedPacking.expireDate = dividedPacking.expireDate.replace(' ', 'T');
        }
        tblData[i].quantity = Number(tblData[i].quantity) - Number(dividedPacking.quantity);
        dividedPacking.maxQuantity = tblData[i].maxQuantity - tblData[i].quantity;
        dividedPacking.orgQuantity = dividedPacking.quantity;
        dividedPacking.claimQuantity = 0;
        tblData[i].maxQuantity = tblData[i].quantity;
        tblData[i].orgQuantity = Number(tblData[i].orgQuantity) - Number(dividedPacking.quantity);
        dataTable.push(dividedPacking);
      }
    }
    this.addEditForm.get('poDetails')?.setValue(dataTable);
  }

  removeDetail(row: any, index: number) {
    const beforeObj = this.addEditForm.get('poDetails')?.value[index - 1];
    const afterObj = this.addEditForm.get('poDetails')?.value[index + 1];
    if ((index === 0 && afterObj.id !== row.id) ||
      (index === this.addEditForm.get('poDetails')?.value.length - 1 && beforeObj.id !== row.id) ||
      (beforeObj && beforeObj.id !== row.id && afterObj.id !== row.id)) {
      this.showWarningMsg('common.packing.remove.not.divide');
      return;
    }
    if (beforeObj && beforeObj.id === row.id) {
      beforeObj.quantity = Number(row.quantity) + Number(beforeObj.quantity);
      beforeObj.maxQuantity = Number(row.maxQuantity) + Number(beforeObj.maxQuantity);
      beforeObj.orgQuantity = Number(beforeObj.orgQuantity) + Number(row.orgQuantity);
      beforeObj.claimQuantity = Number(beforeObj.claimQuantity) + Number(row.claimQuantity);
      beforeObj.total = Number(beforeObj.quantity) * (100 + Number(beforeObj.vat)) *
        Number(beforeObj.productPackingPrice.price) / 100;
    } else if (afterObj.id === row.id) {
      afterObj.quantity = Number(row.quantity) + Number(afterObj.quantity);
      afterObj.maxQuantity = Number(row.maxQuantity) + Number(afterObj.maxQuantity);
      afterObj.orgQuantity = Number(afterObj.orgQuantity) + Number(row.orgQuantity);
      afterObj.claimQuantity = Number(afterObj.claimQuantity) + Number(row.claimQuantity);
      afterObj.total = Number(afterObj.quantity) * (100 + Number(afterObj.vat)) *
        Number(afterObj.productPackingPrice.price) / 100;
    }
    this.addEditForm.get('poDetails')?.value.forEach((packing: any) => {
      if (packing.expireDate) {
        packing.expireDate = packing.expireDate.replace(' ', 'T');
      }
    });
    const tblData = this.addEditForm.get('poDetails')?.value;
    tblData.splice(index, 1);
    this.addEditForm.get('poDetails')?.setValue(tblData);
  }

  onDeleteDetail(row: PoDetailModel, index: number) {
    const details = this.addEditForm.get('poDetails')?.value as PoDetailModel[];
    details.splice(index, 1);
    this.addEditForm.get('poDetails')?.setValue(details);
  }

  onAddDetail() {

    let mId = -1;
    if (this.addEditForm.get('manufacturer')) {
      mId = this.addEditForm.get('manufacturer')?.value;
    }
    let distributorId = -1;
    if (this.addEditForm.get('distributor')) {
      distributorId = this.addEditForm.get('distributor')?.value;
    }
    const dialogRef = this.dialog.open(ProductPackingsComponent, {
      disableClose: false,
      width: '90%',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        manufacturerId: mId,
        distributorId,
        exceptId: this.getExceptProductPackingIds().join(', ')
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.addTableData(result);

    });
  }

  addTableData(data: any[]) {
    // const rows = this.convertToRows(data);
    if (data) {
      const rows = this.convertToRows(data);
      const details = this.addEditForm.get('poDetails')?.value as (PoDetailModel | any)[];
      details.push(...rows);
      this.addEditForm.get('poDetails')?.setValue(details);
    }
  }

  convertToRows(data: any) {
    // console.log(data);
    const rows = [];
    let row;
    const dontHavePrice: number[] = [];
    let indexRow = this.addEditForm.get('poDetails')?.value.length;
    if (data) {
      for (const item of Object.keys(data)) {
        indexRow++;
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(data[item].product);

        row = {
          quantity: 1,
          total: 0,
          vat: data[item].vat,
          productPacking: {
            id: data[item].id, code: data[item].code, name: data[item].name,
            uom: data[item].uom,
            packingType: {quantity: data[item].packingType.quantity}
          },
          product: data[item].product,
          productPackingPrice: {},
          // {
          //     id: data[item].productPackingPrices[0].id,
          //     price: data[item].productPackingPrices[0].price,
          //     currency: data[item].productPackingPrices[0].currency,
          //   },
          productPackingPrices: data[item].productPackingPrices,
        };
        let hasPrice = false;
        for (const packingPrice of data[item].productPackingPrices) {
          if (packingPrice.currency.id === this.addEditForm.get('fromCurrency')?.value) {
            row.productPackingPrice = packingPrice;
            hasPrice = true;
            // row.total = packingPrice.price * data[item].vat / 100;
            break;
          }
        }
        if (!hasPrice) {
          dontHavePrice.push(indexRow);
        }
        rows.push(row);
      }
    }
    if (dontHavePrice.length > 0) {
      this.showErrorMsg('packing.not.have.price', dontHavePrice);
    }
    return rows;
  }

  showErrorMsg = (msgKey: string, rowIndex?: any[], msgParams?: any[]) => {
    let msg = this.translateService.instant(msgKey);
    if (msgParams) {
      msg = this.strFormat(msgKey, msgParams);
    }

    if (rowIndex && rowIndex.length > 0) {
      msg = '' + msg + rowIndex.join(', ');
    }
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.error(msg);
  }

  public strFormat(str: string, replacement: string[]) {
    let a = this.translateService.instant(str);
    if (replacement === null || replacement === undefined || replacement.length === 0) {
      return a;
    }

    for (const k of Object.keys(replacement)) {
      a = a.replace('{' + k + '}', this.translateService.instant(replacement[k]));
    }
    return a;
  }

  getExceptProductPackingIds() {
    const packingIds = [-1];
    if (this.addEditForm.get('poDetails')?.value.length > 0) {
      this.addEditForm.get('poDetails')?.value.forEach((packing: any) => {
        if (packing.productPacking) {
          packingIds.push(packing.productPacking.id);
        }
      });
    }

    return packingIds;
  }

  async getFormSourceData() {
    // manufacturer form control
    const manufacturerParams = new HttpParams()
      .set('text', '')
      .set('status', '1')
      .set('pageNumber', '1')
      .set('pageSize', '9999');
    const manufacturerPagingPromise = this.apiService.get('/manufacturers', manufacturerParams).toPromise();
    const storeParams = new HttpParams()
      .set('text', '')
      .set('status', '1')
      .set('is_DC', 'false')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('pageNumber', '1')
      .set('pageSize', '9999');
    const storePagingPromise = this.apiService.get('/stores', storeParams).toPromise();
    const distributorAllPromise = this.apiService.get('/distributors/all', new HttpParams()).toPromise();
    const currencyAllPromise = this.apiService.get('/currencies/all', new HttpParams()).toPromise();

    const manufacturerPaging = await manufacturerPagingPromise as Page;
    const storePaging = await storePagingPromise as Page;
    const distributorAll = await distributorAllPromise as DistributorModel[];
    const currencyAll = await currencyAllPromise as CurrencyModel[];

    // manufacturer
    let description: ManufacturerDescriptionModel;
    this.manufacturerOptions = manufacturerPaging.content.map((manufacturer: ManufacturerModel) => {
      description = manufacturer.manufacturerDescriptions?.filter(manufacturerDescriptionModel =>
        manufacturerDescriptionModel.language?.code === this.currentLangCode)[0] as ManufacturerDescriptionModel;
      return new SelectModel(manufacturer.id, manufacturer.code + ' - ' + description.name);
    });

    // store
    this.storeOptions = storePaging.content.map((store: StoreModel) =>
      new SelectModel(store.id, store.code + ' - ' + store.name)
    );

    // distributor
    this.distributorOptions = distributorAll.map(distributor =>
      new SelectModel(distributor.id, distributor.code + ' - ' + distributor.name)
    );

    // currency
    this.currencyOptions = currencyAll.map(currency =>
      new SelectModel(currency.id, currency.code + ' - ' + currency.name)
    );

    this.toCurrencyOptions = currencyAll.map(currency =>
      new SelectModel(currency.id, currency.code + ' - ' + currency.name)
    );
    this.addEditForm.get('toCurrency')?.setValue(currencyAll[currencyAll.map(currency => currency.code).indexOf('MMK')].id);
    this.addEditForm.get('fromCurrency')?.setValue(currencyAll[currencyAll.map(currency => currency.code).indexOf('USD')].id);
  }

  formSelectChange() {
    this.addEditForm.get('poDetails')?.setValue([]);
  }

  toCurrencyChange() {
    this.getCurrencyExchangeRate().then();
    // this.addEditForm.markAllAsTouched();
  }

  hasAuthority(): boolean {
    return this.authoritiesService.hasAuthority('get/currency-exchange-rate');
  }

  async getCurrencyExchangeRate() {
    if (this.addEditForm.get('fromCurrency')?.value === this.addEditForm.get('toCurrency')?.value) {
      this.addEditForm.get('exchangeRate')?.setValue(1);
    } else if (this.addEditForm.get('fromCurrency')?.value && this.addEditForm.get('toCurrency')?.value
      && this.hasAuthority()) {
      const params = new HttpParams()
        .set('fromCurrencyId', this.addEditForm.get('fromCurrency')?.value.toString())
        .set('toCurrencyId', this.addEditForm.get('toCurrency')?.value.toString())
        .set('fromDate', '' + this.dateUtilService.convertDateToStringGMT0(DateUtils.getCurrentDate()))
        .set('toDate', '' + this.dateUtilService.convertDateToStringGMT0(DateUtils.getDayBeforeNow(-1)))
        .set('pageSize', '10')
        .set('pageNumber', '1');
      const page = await this.apiService.get('/currency-exchange-rate', params).toPromise() as Page;
      if (page.content.length > 0) {
        this.exchangeRate = page.content[0];
      } else {
        this.exchangeRate = undefined;
        this.showErrorMsg('common.exchange.rate.not.found');
      }
      this.addEditForm.get('exchangeRate')?.setValue(this.exchangeRate ? this.exchangeRate.exchangeRate : 0);
    }
  }

  async fromCurrencyChange() {
    this.getCurrencyExchangeRate().then();
    const dataTable = this.addEditForm.get('poDetails')?.value as any[];
    let index = 0;
    const dontHavePrice: number[] = [];
    dataTable.forEach(row => {
      index++;
      let isExist = false;
      if (row.productPackingPrices) {
        for (const packingPrice of row.productPackingPrices) {
          if (packingPrice.currency.id === this.addEditForm.get('fromCurrency')?.value) {
            row.productPackingPrice = packingPrice;
            isExist = true;
            break;
          } else {
            row.productPackingPrice = {price: '', currency: {name: ''}};
          }
        }
      } else {
        row.productPackingPrice = {price: '', currency: {name: ''}};
      }
      if (!isExist) {
        dontHavePrice.push(index);
      }

    });
    // this.showWarningMsg('currency.not.exist');
    if (dontHavePrice.length > 0) {
      this.showErrorMsg('packing.not.have.price', dontHavePrice);
    }

    this.addEditForm.get('poDetails')?.setValue(dataTable);
  }

  onSave() {
    console.log('aaaaaaa');
    let method;

    this.addEditForm.addControl('status', new FormControl(this.po ? this.po.status : PoStatus.NEW));

    const tempPo = new PoModel(this.addEditForm);
    // this.formData.deliveryDate = this.formData.deliveryDate.replace('T', ' ');
    if (this.actionType === ActionTypeEneum.new) {
      method = this.apiService.post('/pos', [tempPo]);
    } else if (this.actionType === ActionTypeEneum.editPo) {
      method = this.apiService.patch('/pos/' + this.parentId, tempPo);
    }
    this.utilsService.execute(method, this.onSuccessFunc, '.add.success', 'common.confirmSave', ['common.PO']);
  }

  onDisplayWithAuthority(authorities: any): boolean {
    if (typeof (authorities) === 'string') {
      return this.authoritiesService.hasAuthority(authorities);
    }
    if (typeof (authorities) === 'object') {
      let hasAuthority = false;
      for (const authority of authorities) {
        hasAuthority = hasAuthority || this.authoritiesService.hasAuthority(authority);
        if (hasAuthority) {
          return hasAuthority;
        }
      }
    }
    return false;
  }

  isShowButton(btn: string, action: string | null) {
    if (action) {
      switch (action) {
        case this.ActionType.new:
          return [ActionTypeEneum.new, ActionTypeEneum.del, 'save'].includes(btn);
        case this.ActionType.editPo:
          if (this.po && this.po.status === PoStatus.NEW) {
            return ['add', 'delete', 'save', 'approve', 'rejectApprove', 'printPO'].includes(btn);
          } else if (this.po && this.po.status === PoStatus.APPROVED) {
            return ['rejectApprove'].includes(btn);
          }
          return false;
        case this.ActionType.GR:
          return ['GR', 'printPO'].includes(btn) && this.po && this.po.status === PoStatus.ARRIVED_MYANMAR_PORT;
        default:
          return false;

      }
    }
  }

  onApprove() {
    if (this.po?.status !== PoStatus.NEW || this.actionType !== ActionTypeEneum.editPo) {
      return;
    }
    let method;
    method = this.apiService.patch('/pos/approve/' + this.parentId, {});

    this.utilsService.execute(method, this.onSuccessFunc, '.add.success', 'common.confirmApprove', ['common.PO']);

  }

  onRejectApprove() {
    if ((this.po?.status !== PoStatus.NEW && this.po?.status !== PoStatus.APPROVED)
      || this.actionType !== ActionTypeEneum.editPo) {
      return;
    }
    const textMsg = 'reject.reason';

    this.showInputConfirmDialog(textMsg, this.rejectFunction,
      this.onSuccessFunc, 'po.status.approved.refuse.success', this.showError);

  }

  rejectFunction = (rejectReason: any) => {
    return this.apiService.patch('/pos/reject/' + this.parentId, {rejectReason});
  }

  public showError = (err: string): void => {
    this.utilsService.showError(err);
  }


  printPO = (data: any) => {
    this.apiService.saveFile(`/pos/print/${this.parentId}`, null, {
      headers: undefined,
      params: undefined
    });
  }

  onPrintPO() {
    this.utilsService.execute(null, this.printPO, '', 'common.confirmPrint', ['common.PO']);
  }

  onGRPO() {

    const map = new Map();
    let key = '';

    this.addEditForm.addControl('po', new FormControl(this.po?.id));
    this.addEditForm.addControl('status', new FormControl(ImportPoStatus.NEW));
    // this.addEditForm.addControl('importPoDetail', new FormControl(this.po.poDetails));
    const importPo = new ImportPoModel(this.addEditForm);
    const warningThresholdRowIndexArr: any[] = [];
    let dateOfManufacture;
    let expireDateChoose;
    let warningThresholdDate;
    let rowIndex = 0;
    this.addEditForm.get('poDetails')?.value.forEach((detail: any) => {
      // if (!detail.expireDate) {
      //   this.showWarningMsg('po.ae.expireDate.require');
      // }
      rowIndex++;
      expireDateChoose = DateUtils.getDateFromString_0H(detail.expireDate);
      dateOfManufacture = DateUtils.getDayBeforeDate(expireDateChoose.toString(), Number(detail.product.lifecycle));
      warningThresholdDate = DateUtils.getDayAfterDate(dateOfManufacture.toString(), Number(detail.product.warningThreshold));

      if (DateUtils.getCurrentDate_0H() >= warningThresholdDate && expireDateChoose > DateUtils.getCurrentDate_0H()) {
        warningThresholdRowIndexArr.push(rowIndex);
      }


      const expireDate = detail.expireDate.split(' ')[0];

      key = '' + expireDate + detail.productPacking.id;

      if (map.has(key)) {
        const detailExpireDate = map.get(key);
        detailExpireDate.quantity = Number(detailExpireDate.quantity) + Number(detail.quantity);
      } else {
        map.set(key, {
          expireDate: detail.expireDate && detail.expireDate.includes('T') ? detail.expireDate.replace('T', ' ')
            : detail.expireDate,
          product: {id: detail.product.id},
          productPacking: {id: detail.productPacking.id},
          productPackingPrice: {id: detail.productPackingPrice.id},
          poDetail: detail,
          quantity: detail.quantity
        });
      }
    });
    console.log(map.values());
    let msg = 'po.confirm.gr';
    const msgParam = [];
    if (warningThresholdRowIndexArr.length > 0) {
      msg = 'po.confirm.gr.with.warning.threshold';
      if (warningThresholdRowIndexArr.length > 3) {
        msgParam.push(warningThresholdRowIndexArr.slice(0, 2).join(', '));
      } else {
        msgParam.push(warningThresholdRowIndexArr.join(', '));
      }
    }

    importPo.importPoDetails = [...map.values()];
    const method = this.apiService.post('/import-pos', [importPo]);

    this.utilsService.execute(method, this.onSuccessFunc, '.gr.success', msg, msgParam);

  }

  onClaimDetail() {
    if (this.claimId) {
      this.router.navigate(['/claim/dashboard/' + this.claimId]).then();
    } else {
      this.showErrorMsg('claim.wait.approve.please');
    }
  }

  showWarningMsg = (msgKey: string) => {
    const msg = this.translateService.instant(msgKey);
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.warning(msg);
  }
  minDeliveryDate = () => {
    return (this.actionType === ActionTypeEneum.new || (this.actionType === ActionTypeEneum.edit &&
      !(this.actionType === ActionTypeEneum.edit && this.po && this.po.status === PoStatus.APPROVED)))
      ? DateUtils.getDayBeforeNow(-1) : null;
  }
  minVietnamPortDate = () => (this.po && this.po.status === PoStatus.APPROVED && !this.disabledArrivedPort('vietnamPort'))
    ? DateUtils.getDayBeforeNow(-1) : null
  minMyanmarPortDate = () => this.po &&
  (this.po.status === PoStatus.APPROVED || this.po.status === PoStatus.ARRIVED_VIETNAM_PORT) && !this.disabledArrivedPort('myanmarPort')
    ? DateUtils.getDayBeforeNow(-1) : null
  minFCArrivedDate = () => this.po &&
  (this.po.status === PoStatus.ARRIVED_VIETNAM_PORT || this.po.status === PoStatus.ARRIVED_MYANMAR_PORT) && !this.disabledArrivedPort('fc')
    ? DateUtils.getDayBeforeNow(-1) : null

  showInputConfirmDialog(titleMsg: string, method: any,
                         onSuccessFunc: (data: any, onSuccessMessage: string) => void, messageSuccess: string,
                         onErrorFunc: (msg: any) => void) {
    this.utilsService.showConfirmInputDialog(titleMsg ? titleMsg : '', [])
      .afterClosed().subscribe((result: any) => {
      if (result.value) {
        method(result.value).subscribe((response: any) => {
          onSuccessFunc(response, messageSuccess);
        }, (errorMsg: any) => {
          onErrorFunc(errorMsg);
        });
      }
    });
  }

  updateArrivedDate() {
    let method;
    if (this.authoritiesService.hasAuthority('patch/pos/{id}/arrived-vietnam-port')) {
      method = this.apiService.patch(`/pos/${this.parentId}/arrived-vietnam-port`,
        {arrivedVietnamPortDate: this.addEditForm.get('arrivedVietnamPortDate')?.value});
    } else if (this.authoritiesService.hasAuthority('patch/pos/{id}/arrived-myanmar-port')) {
      method = this.apiService.patch(`/pos/${this.parentId}/arrived-myanmar-port`,
        {arrivedMyanmarPortDate: this.addEditForm.get('arrivedMyanmarPortDate')?.value});
    } else if (this.authoritiesService.hasAuthority('patch/pos/{id}/arrived-fc')) {
      method = this.apiService.patch(`/pos/${this.parentId}/arrived-fc`,
        {arrivedFcDate: this.addEditForm.get('arrivedFcDate')?.value});
    }

    this.utilsService.execute(method, this.onSuccessFunc,
      '.edit.success', 'common.confirmSave', ['common.arrived.date.param']);
  }

  onDisplayArrivedDate() {
    const hasUpdate = (this.onDisplayWithAuthority('patch/pos/{id}/arrived-vietnam-port')
      && this.po && (this.po.status === PoStatus.APPROVED))
      || (this.onDisplayWithAuthority('patch/pos/{id}/arrived-myanmar-port')
        && this.po && (this.po.status === PoStatus.APPROVED || this.po.status === PoStatus.ARRIVED_VIETNAM_PORT))
      || (this.onDisplayWithAuthority('patch/pos/{id}/arrived-fc')
        && this.po && (this.po.status === PoStatus.ARRIVED_VIETNAM_PORT || this.po.status === PoStatus.ARRIVED_MYANMAR_PORT));
    return hasUpdate;
  }

  disabledArrivedPort(location: 'vietnamPort' | 'myanmarPort' | 'fc') {
    let disabled;
    switch (location) {
      case 'vietnamPort': {
        disabled = (!this.onDisplayWithAuthority('patch/pos/{id}/arrived-vietnam-port')
          || this.actionType !== ActionTypeEneum.deliveryDate
          || (this.po && this.po.status !== PoStatus.APPROVED));

        if (!disabled && this.addEditForm) {
          this.addEditForm.get('arrivedVietnamPortDate')?.setValidators(Validators.required);
          this.addEditForm.get('arrivedVietnamPortDate')?.updateValueAndValidity();
        } else {
          this.addEditForm.get('arrivedVietnamPortDate')?.setValidators(null);
        }
        break;
      }
      case 'myanmarPort':
        disabled = !this.onDisplayWithAuthority('patch/pos/{id}/arrived-myanmar-port')
          || !this.addEditForm.get('arrivedVietnamPortDate')?.value
          || this.actionType !== ActionTypeEneum.deliveryDate
          || (this.po && this.po.status !== PoStatus.APPROVED && this.po.status !== PoStatus.ARRIVED_VIETNAM_PORT);
        if (!disabled && this.addEditForm) {
          this.addEditForm.get('arrivedMyanmarPortDate')?.setValidators(Validators.required);
          this.addEditForm.get('arrivedMyanmarPortDate')?.updateValueAndValidity();
        } else {
          this.addEditForm.get('arrivedMyanmarPortDate')?.setValidators(null);
        }
        break;
      case 'fc':
        disabled = !this.onDisplayWithAuthority('patch/pos/{id}/arrived-fc')
          || !this.addEditForm.get('arrivedMyanmarPortDate')?.value
          || this.actionType !== ActionTypeEneum.deliveryDate
          || (this.po && this.po.status !== PoStatus.ARRIVED_VIETNAM_PORT && this.po.status !== PoStatus.ARRIVED_MYANMAR_PORT);
        if (!disabled && this.addEditForm) {
          this.addEditForm.get('arrivedFcDate')?.setValidators(Validators.required);
          this.addEditForm.get('arrivedFcDate')?.updateValueAndValidity();
        } else {
          this.addEditForm.get('arrivedFcDate')?.setValidators(null);
        }
        break;
    }

    return disabled;
  }

  isEmptyDetails() {
    return this.addEditForm.get('poDetails') ? this.addEditForm.get('poDetails')?.value.length === 0 : false;
  }
}
