import {CookieService} from 'ngx-cookie-service';
import {FormBuilder} from '@angular/forms';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {CustomDateAdapter, MY_FORMATS_1} from '../../base/field/custom-date-adapter';
import {CommunicationService} from '../../communication-service';
import {ToastrService} from 'ngx-toastr';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {LDPoLayout} from './l-d-po.layout';
import {DatePipe, Location} from '@angular/common';
import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {MultilanguagePanigator} from '../../_helpers/multilanguage.paginator';
import {TranslateService} from '@ngx-translate/core';
import {NavService} from '../../_services/nav.service';
import {ActionTypeConstant, ActionTypeEneum, PoStatus} from '../../_models/action-type';
import {takeUntil} from 'rxjs/operators';
import {UiStateService} from '../../_services/ui.state.service';
import {Subject} from 'rxjs';
import {DateUtils} from '../../base/Utils/date.utils';
import {ApiService, AuthoritiesService, DateUtilService} from '@next-solutions/next-solutions-base';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'list-po', /* dinh nghia id*/
  templateUrl: './l-d-po.html',
  styleUrls: ['../form.layout.scss', '../table.layout.scss', './l-d-po.scss'],
  providers: [
    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator},
    DatePipe, {
      provide: DateAdapter, useClass: CustomDateAdapter
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_1}
  ]
})
export class LDPoComponent implements OnInit, AfterViewInit, OnDestroy {
  readonly ACTION_TYPE = new ActionTypeConstant();
  model: LDPoLayout;
  private unsubscribe$ = new Subject();

  printPdfId?: number;
  formDataMap = new Map();
  url = '';
  isFirstTime = true;

  constructor(fb: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService,
              toastr: ToastrService,
              private datePipe: DatePipe,
              private serv: CommunicationService,
              tranService: TranslateService,
              private tsuService: TsUtilsService,
              private route: ActivatedRoute,
              location: Location,
              protected dateUtilService: DateUtilService,
              private navService: NavService,
              private uiStateService: UiStateService,
              protected authoritiesService: AuthoritiesService) {
    this.model = new LDPoLayout(tsuService, userService, fb,
      this.route, this.datePipe, tranService, toastr, location, cookieService, dateUtilService, authoritiesService);
  }

  ngOnInit() {
    if (this.isFirstTime) {
      this.url = this.router.url;
    }
    this.uiStateService.uiState$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(state => {
        if (state) {
          this.isFirstTime = false;
          if (state.formDataMap && state.formDataMap.has(this.url)) {

            const formData = state.formDataMap.get(this.url);
            formData.fromDate = DateUtils.getDateFromString(formData.fromDate);
            formData.toDate = DateUtils.getDateFromString(formData.toDate);
            this.model.formData = formData;
            this.model.hasState = true;
            this.formDataMap = state.formDataMap;
            this.model.onSearchData();
          }

        }

      });
  }

  ngOnDestroy(): void {

    if (!this.formDataMap.has(this.url)) {
      this.formDataMap.set(this.url, this.model.formData);
    }

    this.uiStateService.setUiState$({formDataMap: this.formDataMap});
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngAfterViewInit(): void {
    this.navService.title = 'po.ld.title';
  }

  showButtonEvent(event: any) {
    const row = event.row;
    const act = event.act;
    switch (row.status) {
      case PoStatus.NEW:
        event.isShow = ['edit', 'delete'].includes(act);
        return;
      case PoStatus.APPROVED:
        event.isShow = ['edit', 'picture_as_pdf', 'compare_arrows'].includes(act);
        return;
      case PoStatus.ARRIVED_VIETNAM_PORT:
        event.isShow = ['compare_arrows', 'picture_as_pdf'].includes(act);
        return;
      case PoStatus.ARRIVED_MYANMAR_PORT:
        event.isShow = ['add_shopping_cart', 'picture_as_pdf', 'compare_arrows'].includes(act);
        return;
      case PoStatus.IMPORTED:
        event.isShow = ['picture_as_pdf'].includes(act);
        return;
      case PoStatus.REJECT:
        event.isShow = false;
        return;
      default:
        event.isShow = true;
        return;
    }

  }

  onRowClick(event: any) {
    this.router.navigate([this.router.url, ActionTypeEneum.view, event.row.id]);
  }

  onAddEvent(event: any) {
    this.router.navigate([this.router.url, ActionTypeEneum.new]);
  }

  onCellEvent(event: any) {
    console.log('event:', event);
    if (event.act === ActionTypeEneum.editPo) {
      this.router.navigate([this.router.url, this.ACTION_TYPE.editPo, event.row.id]);
    } else if (event.act === 'add_shopping_cart') {
      this.router.navigate([this.router.url, this.ACTION_TYPE.GR, event.row.id]);
    } else if (event.act === 'dashboard') {
      this.router.navigate([this.router.url, this.ACTION_TYPE.Pal, event.row.importPo.id]);
    } else if (event.act === 'picture_as_pdf') {
      this.printPdfId = event.row.id;
      this.tsuService.execute6(null, this.printPdf, 'common.confirmPrintPdf', ['common.PO']);
    } else if (event.act === 'compare_arrows') {
      this.router.navigate([this.router.url, ActionTypeEneum.deliveryDate, event.row.id]).then();
    }
  }

  printPdf = (data: any) => {
    this.userService.saveFile('/pos/print-pdf/' + this.printPdfId, null, {
      headers: undefined,
      params: undefined
    });
  }

  printPOs() {
    this.model.onPrintPOs({});
  }

  onSearchData() {
    this.model.tsTable.allSelect = {};
    // if (!this.model.formData.fromDate) {
    //   this.model.formData.fromDate = this.datePipe.transform(DateUtils.getStartDay1970(), AppSettings.API_DATE_FORMAT, '-0');
    // }
    // if (!this.model.formData.toDate) {
    //   this.model.formData.toDate = this.datePipe.transform(DateUtils.getCurrentDate(), AppSettings.API_DATE_FORMAT, '-0');
    // }
    this.model.onSearchData();
  }
}
