import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportStatementV2Component } from './import-statement-v2.component';
import { ImportStatementV2AddEditComponent } from './import-statement-v2-add-edit/import-statement-v2-add-edit.component';
import {RouterModule, Routes} from '@angular/router';
import {AuthoritiesResolverService, MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {SharedModule} from '../../modules/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {ClaimService} from "../../_services/http/claim/claim.service";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/', suffix: '.json'},
    {prefix: './assets/i18n/import-statement/', suffix: '.json'},
  ]);
}

const routes: Routes = [
  {
    path: '',
    component: ImportStatementV2Component,
    pathMatch: 'full',
    data: {breadcrumb: 'menu.import.statement.list'},
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: ':id',
    component: ImportStatementV2AddEditComponent,
    data: {breadcrumb: 'menu.import.statement.dashboard'},
    resolve: {me: AuthoritiesResolverService}
  }
];

@NgModule({
  declarations: [ImportStatementV2Component, ImportStatementV2AddEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      isolate: true,
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
   providers: [
     ClaimService,
   ]
})
export class ImportStatementV2Module { }
