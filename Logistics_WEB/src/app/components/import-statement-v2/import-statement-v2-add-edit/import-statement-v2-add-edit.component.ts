import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseTableLayout,
  ButtonFields, ColumnFields,
  ColumnTypes,
  DateUtilService,
  FormStateService,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NavService} from '../../../_services/nav.service';
import {ImportStatementTable} from '../../../_models/import-statement/import.statement.table';
import {HttpParams} from '@angular/common/http';
import {ClaimEnum, ImportStatementStatus} from '../../../_models/action-type';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {ClaimDetailModel} from '../../../_models/claim.detail.model';
import {ClaimService} from "../../../_services/http/claim/claim.service";

@Component({
  selector: 'app-import-statement-v2-add-edit',
  templateUrl: './import-statement-v2-add-edit.component.html',
  styleUrls: ['./import-statement-v2-add-edit.component.css']
})
export class ImportStatementV2AddEditComponent extends BaseTableLayout implements OnInit {

  moduleName = 'import.statement.packing-product';

  buttons: ButtonFields[] = [];

  columns: ColumnFields[] = [];

  statusValues: SelectModel[] = [];

  form?: FormGroup;

  id: number | null | undefined;

  importStatement: any;
  hasClaimDelivery = false;
  hasClaimImport = false;

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private dateUtilService: DateUtilService,
              private claimService: ClaimService
  ) {
    super(activatedRoute, authoritiesService);
    this.columns.push(...[
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${this.getPosition(e, this.form?.get('table')?.value)}`,
        cell: (e: any) => `${this.getPosition(e, this.form?.get('table')?.value)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'packingCode',
        header: 'packingCode',
        title: (e: ImportStatementTable) => `${e.packageCode ? e.packageCode : ''}`,
        cell: (e: ImportStatementTable) => `${e.packageCode ? e.packageCode : ''}`,
        display: (e: ImportStatementTable) => true,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'qrCode',
        header: 'qrCode',
        columnType: ColumnTypes.BASE64,
        title: (e: ImportStatementTable) => `${e.qrCode ? e.qrCode : ''}`,
        cell: (e: ImportStatementTable) => `${e.qrCode ? e.qrCode : ''}`,
        display: (e: ImportStatementTable) => true,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'productName',
        header: 'productName',
        title: (e: ImportStatementTable) => `${e.productName}`,
        cell: (e: ImportStatementTable) => `${e.productName}`,
        display: (e: ImportStatementTable) => true,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'expiredDate',
        header: 'expiredDate',
        title: (e: ImportStatementTable) => `${this.dateUtilService.convertDateToDisplayGMT0(e.expireDate + '')}`,
        cell: (e: ImportStatementTable) => `${this.dateUtilService.convertDateToDisplayGMT0(e.expireDate + '')}`,
        display: (e: ImportStatementTable) => true,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'packingType',
        header: 'packingType',
        title: (e: ImportStatementTable) => `${e.packingType}`,
        cell: (e: ImportStatementTable) => `${e.packingType}`,
        display: (e: ImportStatementTable) => true,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'quantity',
        header: 'quantity',
        title: (e: ImportStatementTable) => `${e.quantity}`,
        cell: (e: ImportStatementTable) => `${e.quantity}`,
        display: (e: ImportStatementTable) => true,
        align: AlignEnum.RIGHT
      },
      {
        columnDef: 'pallet',
        header: 'pallet',
        title: (e: ImportStatementTable) => `${e.pallet}`,
        cell: (e: ImportStatementTable) => `${e.pallet}`,
        display: (e: ImportStatementTable) => this.importStatement.status === ImportStatementStatus.UPDATED_INVENTORY,
        align: AlignEnum.CENTER
      }
    ]);
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      code: [''],
      exportDescription: [''],
      importDescription: [''],
      fromStore: [''],
      fromStoreUsername: [''],
      fromStoreUserTel: [''],
      toStoreName: [''],
      toStoreUsername: [''],
      toStoreUserTel: [''],
      table: ['']
    });
    const claimDeliveryMap = new Map<string, any>();
    const claimImportMap = new Map<string, any>();
    this.apiService.get('/import-statements/' + this.id, new HttpParams()).subscribe(next => {
      this.importStatement = next;
      let key;
      if (!this.isHideClaimView()) {
        if (!this.isImportStatementFromRepackingPlanning()) {
          this.getClaimAsyncFromExport().then(response => {
            response?.claimDelivery.then((claimDeliveryDetails: any) => {
              if (claimDeliveryDetails && claimDeliveryDetails.length > 0) {
                this.hasClaimDelivery = true;
                claimDeliveryDetails.forEach((claimDetail: any) => {
                  key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
                  claimDetail.orgQuantity = claimDetail.quantity;
                  claimDetail.claimDeliveryQuantity = claimDetail.quantity;
                  claimDetail.claimImportQuantity = 0;
                  claimDetail.quantity = 0;
                  claimDetail.storeProductPackingDetail = {
                    qRCode: ''
                  };
                  claimDeliveryMap.set(key, claimDetail);
                });
              }
            });

            response?.claimImport.then((claimImportDetails: any) => {
              if (claimImportDetails && claimImportDetails.length > 0) {
                this.hasClaimImport = true;
                claimImportDetails.forEach((claimDetail: any) => {
                  key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
                  claimDetail.orgQuantity = claimDetail.quantity;
                  claimDetail.claimDeliveryQuantity = 0;
                  claimDetail.claimImportQuantity = claimDetail.quantity;
                  claimDetail.quantity = 0;
                  claimDetail.storeProductPackingDetail = {
                    qRCode: ''
                  };
                  claimImportMap.set(key, claimDetail);
                });
              }
            });
          }).then(() => {
            this.fillData(next, claimDeliveryMap, claimImportMap);
          });
        } else {
          this.getClaimImportFromRepack().then((result: any) => {
            if (result.claimImportFromRepack.length > 0) {
              this.hasClaimImport = true;
              result.claimImportFromRepack.forEach((claimDetail: any) => {
                key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
                claimDetail.orgQuantity = claimDetail.quantity;
                claimDetail.claimDeliveryQuantity = 0;
                claimDetail.claimImportQuantity = claimDetail.quantity;
                claimDetail.quantity = 0;
                claimDetail.storeProductPackingDetail = {
                  qRCode: ''
                };
                claimImportMap.set(key, claimDetail);
              });
            }

          }).then(() => {
            this.fillData(next, new Map<string, number>(), claimImportMap);
          });
        }
      } else {
        this.fillData(next, claimDeliveryMap, claimImportMap);
      }
    });
  }

  getPosition(e: any, arr: any[]) {
    return !!arr && arr.length > 0 ? (arr.indexOf(e) + 1).toString() : '';
  }

  fillData(importStatement: any, claimDeliveryMap?: Map<string, any>, claimImportMap?: Map<string, any>) {
    this.form?.patchValue({
      code: importStatement.code,
      exportDescription: importStatement?.exportStatement?.description,
      importDescription: importStatement.description,
      fromStore: importStatement.fromStore.code + ' - ' + importStatement.fromStore.name,
      fromStoreUsername: importStatement.fromStore.user ? importStatement.fromStore.user.username : '',
      fromStoreUserTel: importStatement.fromStore.user ? importStatement.fromStore.user.tel : '',
      toStoreName: importStatement.toStore.code + ' - ' + importStatement.toStore.name,
      toStoreUsername: importStatement.toStore.user ? importStatement.toStore.user.username : '',
      toStoreUserTel: importStatement.toStore.user ? importStatement.toStore.user.tel : ''
    });

    if (importStatement.status === ImportStatementStatus.NOT_UPDATE_INVENTORY) {
      let productPackingIdImport;
      let expireDateImport;
      let productPackingIdExport;
      let expireDateExport;
      const importStatementMap = new Map<string, any>();
      importStatement.importStatementDetails.forEach((importDetail: any) => {
        productPackingIdImport = importDetail.productPacking.id;
        expireDateImport = importDetail.expireDate;
        for (const exportStatementDetail of importStatement.exportStatement.exportStatementDetails) {
          productPackingIdExport = exportStatementDetail.productPacking.id;
          expireDateExport = exportStatementDetail.storeProductPackingDetail.storeProductPacking.expireDate;
          if (productPackingIdExport === productPackingIdImport
            && new Date(expireDateExport).getTime() === new Date(expireDateImport).getTime()) {
            importDetail.storeProductPackingDetail = {
              qRCode: exportStatementDetail.storeProductPackingDetail.qRCode
            };
          }
        }
        const key = '' + importDetail.expireDate.toString().concat(importDetail.productPacking.id);

        if (importStatementMap.has(key)) {
          const detail = importStatementMap.get(key);
          detail.quantity = Number(detail.quantity) + Number(importDetail.quantity);
          detail.orgQuantity = Number(detail.orgQuantity) + Number(importDetail.quantity);
        } else {
          importDetail.orgQuantity = Number(importDetail.quantity);
          importStatementMap.set(key, importDetail);
        }
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(importDetail.productPacking.product);
      });

      for (const importDetail of importStatementMap.values()) {
        const key = '' + importDetail.expireDate.toString().concat(importDetail.productPacking.id);
        importDetail.orgQuantity = Number(importDetail.quantity);
        if (claimDeliveryMap?.has(key)) {
          importDetail.claimDeliveryQuantity = claimDeliveryMap.get(key).claimDeliveryQuantity;
          importDetail.quantity = Number(importDetail.quantity) - importDetail.claimDeliveryQuantity;
          claimDeliveryMap.delete(key);
        } else {
          importDetail.claimDeliveryQuantity = 0;
        }
        if (claimImportMap?.has(key)) {
          importDetail.claimImportQuantity = claimImportMap.get(key).claimImportQuantity;
          importDetail.quantity = Number(importDetail.quantity) - importDetail.claimImportQuantity;
          claimImportMap.delete(key);
        } else {
          importDetail.claimImportQuantity = 0;
        }
      }
      const importStatementTable: ImportStatementTable[] = [];
      importStatementMap.forEach((e: any) => {
        importStatementTable.push({
          quantity: e.quantity,
          expireDate: e.expireDate,
          packingType: e.productPacking.packingType.quantity,
          productName: e.productPacking.product.name,
          qrCode: e.storeProductPackingDetail.qRCode,
          packageCode: e.productPacking.code,
          pallet: '',
        });
      });
      this.form?.get('table')?.patchValue(importStatementTable);
    } else {
      const map = new Map();
      let palletId = '';
      let key = '';
      let claimKey = '';
      let packingId = '';
      let expireDate = '';
      importStatement.importStatementDetailPallets.forEach((detail: any) => {
        if (detail.palletDetail) {
          detail.productPacking = detail.palletDetail.productPacking;
        } else {
          detail.productPacking = detail.storeProductPackingDetail.storeProductPacking.productPacking;
        }
        packingId = detail.productPacking.id;
        expireDate = detail.expireDate;
        palletId = detail.pallet ? detail.pallet.id : '';

        key = palletId + ' ' + expireDate + packingId;
        claimKey = '' + expireDate + packingId;
        if (map.has(key)) {
          const packingDetail = map.get(key);
          packingDetail.quantity = Number(packingDetail.quantity) + Number(detail.quantity);
          packingDetail.orgQuantity = Number(packingDetail.orgQuantity) + Number(detail.quantity);
        } else {
          detail.orgQuantity = Number(detail.quantity);
          detail.claimDeliveryQuantity = 0;
          detail.claimImportQuantity = 0;
          if (claimDeliveryMap?.has(claimKey)) {
            const claimDeliveryDetailQuantity = claimDeliveryMap.get(claimKey).claimDeliveryQuantity;
            detail.orgQuantity = Number(detail.orgQuantity) + claimDeliveryDetailQuantity;
            detail.claimDeliveryQuantity = claimDeliveryDetailQuantity;

            claimDeliveryMap.delete(claimKey);
          }
          if (claimImportMap?.has(claimKey)) {
            const claimImportDetailQuantity = claimImportMap.get(claimKey).claimImportQuantity;
            detail.orgQuantity = Number(detail.orgQuantity) + claimImportDetailQuantity;
            detail.claimImportQuantity = claimImportDetailQuantity;

            claimImportMap.delete(claimKey);
          }
          map.set(key, detail);
        }
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(detail.productPacking.product);
      });
      const dataTable = [];
      dataTable.push(...claimDeliveryMap?.values(), ...claimImportMap?.values());
      dataTable.push(...map.values());
      const importStatementTable: ImportStatementTable[] = [];
      dataTable.forEach((e: any) => {
        importStatementTable.push({
          quantity: e.quantity,
          expireDate: e.expireDate,
          packingType: e.productPacking.packingType.quantity,
          productName: e.productPacking.product.name,
          qrCode: e.storeProductPackingDetail.qRCode,
          packageCode: e.productPacking.code,
          pallet: e.pallet ? e.pallet.displayName : '',
        });
      });
      this.form?.get('table')?.patchValue(importStatementTable);
    }
  }

  isHideClaimView(): boolean {
    return this.importStatement.status === ImportStatementStatus.NOT_UPDATE_INVENTORY;
  }

  isImportStatementFromRepackingPlanning(): boolean {
    return this.importStatement.fromStore.id === this.importStatement.toStore.id;
  }

  async getClaimAsyncFromExport() {
    if (!this.id) {
      return;
    }
    let hasErrorCallback = false;
    const claimDelivery = this.claimService.getClaimByReferenceObs(this.id, ClaimEnum.DELIVERY).toPromise()
      .catch((errMsg: any) => {
        if (!hasErrorCallback) {
          // this.showErrorMsg(errMsg);
          hasErrorCallback = !hasErrorCallback;
        }
      });

    const claimImport = this.claimService.getClaimByReferenceObs(this.id, ClaimEnum.IMPORT_STATEMENT_EXPORT_STATEMENT).toPromise()
      .catch((errMsg: any) => {
        if (!hasErrorCallback) {
          // this.showErrorMsg(errMsg);
          hasErrorCallback = !hasErrorCallback;
        }
      });

    await claimDelivery;
    await claimImport;

    return {claimDelivery, claimImport};
  }

  async getClaimImportFromRepack() {
    if (!this.id){
      return;
    }
    const claimImportFromRepack = await this.claimService.getClaimByReferenceObs(this.id, ClaimEnum.IMPORT_STATEMENT_REPACKING_PLANNING).toPromise();
    return {claimImportFromRepack};
  }
}
