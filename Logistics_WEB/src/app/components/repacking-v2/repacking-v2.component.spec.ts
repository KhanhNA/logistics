import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepackingV2Component } from './repacking-v2.component';

describe('RepackingV2Component', () => {
  let component: RepackingV2Component;
  let fixture: ComponentFixture<RepackingV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepackingV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepackingV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
