import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepackV2Component } from './repack-v2.component';

describe('RepackV2Component', () => {
  let component: RepackV2Component;
  let fixture: ComponentFixture<RepackV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepackV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepackV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
