import {ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {DatePipe, Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  DateUtilService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {RepackingAuthoritiesService} from '../../../../_services/authority/repacking.authorities.service';
import {CommonHttpService} from '../../../../_services/http/common/common.http.service';
import {RepackingV2Service} from '../../repacking-v2.service';
import {ClaimService} from '../../../../_services/http/claim/claim.service';
import {MatDialog} from '@angular/material/dialog';
import {RepackingPlanningModel} from '../../../../_models/repacking.planning.model';
import {repackingStatusAction} from '../../../../_models/const/repacking.const';
import {ClaimEnum} from '../../../../_models/action-type';
import {ClaimDetailModel} from '../../../../_models/claim.detail.model';
import {ProductDescriptionsUtils} from '../../../../_utils/product.descriptions.utils';
import {RepackingPlanDetailModel} from '../../../../_models/repacking/repacking.plan.detail.model';
import {ProductPackingModel} from '../../../../_models/product.packing.model';
import {CommonUtils} from '../../../../_utils/common.utils';
import {RepackingPlanDetailRepackedModel} from "../../../../_models/repacking/repacking.plan.detail.repacked.model";
import {HttpParams} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";

class PackingQuantityRepack {
  productPacking: ProductPackingModel | undefined;
  quantity = 0;
  expireDate: string | undefined;
  repackingDetail: RepackingPlanDetailModel | undefined;
}

@Component({
  selector: 'app-repack-v2',
  templateUrl: './repack-v2.component.html',
  styleUrls: ['./repack-v2.component.scss'],
})
export class RepackV2Component extends BaseAddEditLayout implements OnInit, OnChanges {

  @Input() moduleName = 'repacking';
  @Input() isView = false;
  @Input() repackingPlan: RepackingPlanningModel | undefined;
  claimPendingApprove = false;
  claimDetails: ClaimDetailModel[] = [];
  claimId: number | undefined;
  isPrint = false;
  isPreparedData = false;
  repackedFailed = false;

  repackedPackingMap: Map<string, PackingQuantityRepack[]> = new Map<string, PackingQuantityRepack[]>();

  get repackingStatusAction() {
    return repackingStatusAction;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private dateUtilService: DateUtilService,
              public repackingAuthoritiesService: RepackingAuthoritiesService,
              protected commonHttpService: CommonHttpService,
              protected repackingV2Service: RepackingV2Service,
              protected claimService: ClaimService,
              private dialog: MatDialog,
              private datePipe: DatePipe
  ) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);

  }

  ngOnChanges(changes: SimpleChanges) {
    if ('repackingPlan' in changes) {
      this.repackingPlan = changes.repackingPlan.currentValue;
      if (this.repackingV2Service.selectedTabIndex.value === 1 && !!this.repackingPlan) {
        this.refresh(this.repackingPlan);
      }
    }
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  isViewNewRepackingPlanning(repackingPlan: RepackingPlanningModel) {
    return repackingStatusAction.isNewStatus(repackingPlan) && this.isView;
  }

  updateDetail(map?: Map<string, number>) {
    if (map) {
      let checkKey = '';
      let value: any;
      this.repackingPlan?.repackingPlanningDetails.forEach((repackingDetail: RepackingPlanDetailModel) => {
        checkKey = '' + repackingDetail.expireDate + repackingDetail.productPacking?.id;
        value = map.get(checkKey) ? map.get(checkKey) : 0;
        if (value) {
          repackingDetail.orgQuantity = Number(repackingDetail.quantity);
          repackingDetail.claimQuantity = value;
          repackingDetail.quantity = Number(repackingDetail.quantity) - value;
        } else {
          repackingDetail.orgQuantity = Number(repackingDetail.quantity);
          repackingDetail.claimQuantity = 0;
        }
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(repackingDetail.productPacking?.product);
        this.updatePack1Quantity(repackingDetail);
      });
    } else {
      this.repackingPlan?.repackingPlanningDetails.forEach((repackingDetail: RepackingPlanDetailModel) => {
        this.updatePack1Quantity(repackingDetail);
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(repackingDetail.productPacking?.product);
      });
    }

    this.isPreparedData = true;
    this.isPrint = false;
  }

  // update về pack 1 để đóng gói lại
  updatePack1Quantity(repackingDetail: RepackingPlanDetailModel) {
    repackingDetail.totalQuantity = (repackingDetail.quantity && repackingDetail.productPacking?.packingType?.quantity)
      ? Number(repackingDetail.quantity) * Number(repackingDetail.productPacking?.packingType?.quantity) : 0;

    repackingDetail.orgQuantity = (repackingDetail.orgQuantity && repackingDetail.productPacking?.packingType?.quantity) ?
      repackingDetail.orgQuantity * repackingDetail.productPacking?.packingType?.quantity
      : repackingDetail.totalQuantity;

    repackingDetail.claimQuantity = (repackingDetail.claimQuantity && repackingDetail.productPacking?.packingType?.quantity) ?
      repackingDetail.claimQuantity * repackingDetail.productPacking.packingType.quantity : 0;

    if (CommonUtils.isEmptyList(repackingDetail.repackingPlanningDetailRepackeds)) {
      this.repackedPackingMap.set(repackingDetail.productPacking?.product?.code + '',
        repackingDetail.productPacking?.product?.productPackings
          ? repackingDetail.productPacking.product.productPackings.map(packing => {
            return {
              repackingDetail,
              productPacking: packing,
              quantity: 0,
              expireDate: repackingDetail.expireDate + ''
            };
          })
          : []);
    } else {

      this.repackedPackingMap.set(repackingDetail.productPacking?.product?.code + '',
        repackingDetail.repackingPlanningDetailRepackeds.map(repacked => {
          return {
            repackingDetail,
            productPacking: repacked.productPacking,
            quantity: repacked.quantity ? repacked.quantity : 0,
            expireDate: repacked.expireDate + ''
          };
        })
      );
    }
  }

  async refresh(repackingPlan: RepackingPlanningModel | undefined) {
    this.repackedPackingMap = new Map<string, PackingQuantityRepack[]>();
    if (!repackingPlan?.id) {
      return;
    }
    this.claimPendingApprove = false;
    this.repackingPlan = repackingPlan;

    const claimMap = new Map<string, number>();
    let key = '';
    if (!repackingStatusAction.isCanceledStatus(repackingPlan) && !this.isViewNewRepackingPlanning(repackingPlan)) {
      const claimDetails: ClaimDetailModel[] =
        await this.claimService.getClaimByReferenceObs(repackingPlan.id, ClaimEnum.REPACKING_PLANNING).toPromise()
          .catch((err: any) => {
            this.utilsService.showErrorToarst(err);
            this.claimPendingApprove = true;
            this.updateDetail();
          });
      if (this.claimPendingApprove) {
        return;
      }
      this.claimDetails = claimDetails;
      if (this.claimDetails.length > 0) {
        if (repackingStatusAction.isRepackedStatus(repackingPlan) && !this.isView) {
          this.utilsService.showWarningToarst('claim.approve.not.repack');
        }
        this.claimId = this.claimDetails[0].claim?.id;
        this.claimDetails.forEach((detail: ClaimDetailModel) => {
          key = '' + detail.expireDate + detail.productPacking?.id;
          claimMap.set(key, detail.quantity ? detail.quantity : 0);
        });
      }

      this.updateDetail(claimMap);
    } else {
      this.updateDetail();
    }

    this.cdr.detectChanges();
  }

  saveSuccess = (data: any, msgSuccess: string | undefined) => {
    const msg = this.utilsService.onSuccessFunc(msgSuccess);
    this.isPrint = true;

    this.apiService.get(`/repacking-plannings/${this.id}`, new HttpParams())
      .subscribe((repackingPlan: RepackingPlanningModel | any) => {
        this.refresh(repackingPlan);
      });
  }

  onSaveRepacking() {
    const data: RepackingPlanDetailRepackedModel[] = [];
    this.repackedPackingMap.forEach((packingQuantityList, productCode) => {
      data.push(...packingQuantityList.map(packingQuantity => {
        return {
          expireDate: packingQuantity.expireDate,
          repackingPlanningDetail: {id: packingQuantity.repackingDetail?.id},
          productPacking: {id: packingQuantity.productPacking?.id},
          quantity: packingQuantity.quantity
        } as RepackingPlanDetailRepackedModel;
      }));
    })
    const method = this.apiService.post(`/repacking-plannings/repack/${this.id}`, data);
    this.utilsService.execute(method, this.saveSuccess, '.repack.success', 'common.confirmSave', ['repacking.param.plan']);
  }

  checkRepackedCorrect() {
    this.repackedFailed = this.repackingPlan ? this.repackingPlan.repackingPlanningDetails.some(detail => detail.totalQuantity !== 0) : true;
  }

  onRepackedQuantityChange(repackedQuantity: number, repackingDetail: RepackingPlanDetailModel,
                           packingRepackAndQuantity: PackingQuantityRepack,
                           packingQuantityRepacks: PackingQuantityRepack[] | undefined) {
    packingRepackAndQuantity.quantity = repackedQuantity;
    repackingDetail.totalQuantity = repackingDetail.orgQuantity - repackingDetail.claimQuantity
      - (packingQuantityRepacks ? packingQuantityRepacks.reduce((total, packingQuantityRepack) => {
        return total + (packingQuantityRepack.quantity * (packingQuantityRepack.productPacking?.packingType?.quantity
          ? packingQuantityRepack.productPacking?.packingType?.quantity : 0));
      }, 0) : 0);
    this.checkRepackedCorrect();
  }

  printQRCode = (data: any) => {
    this.apiService.saveFile(`/repacking-plannings/print-qrcode/${this.id}`, null, {
      headers: undefined,
      params: undefined
    });
  }

  onExportQrCode() {
    // const method = this.userService.post(`/repacking-plannings/print-qrcode/${this.parentId}`, null);
    this.utilsService.execute(null, this.printQRCode, '', 'common.confirmExportExcel', ['common.QRCode']);
  }


  onPrintQrCode() {
    console.log(this.repackedPackingMap, this.repackingPlan);
    const divContents = [];
    const store = this.repackingPlan?.store?.code;
    const hotline = this.repackingPlan?.store?.phone;
    const repackeDate = this.datePipe.transform(this.repackingPlan?.repackedDate, 'HH:mm dd-MM-yyyy', '-0');
    let expiredDate;
    let repackingPlanningDetailRepackeds;
    let packingCode;
    let qrBase64;
    let quantityPackingType;
    let packingName;
    let content;
    for (const repackingDetail of (this.repackingPlan?.repackingPlanningDetails ? this.repackingPlan?.repackingPlanningDetails : [])) {
      expiredDate = this.datePipe.transform(repackingDetail.expireDate, environment.DIS_DATE_FORMAT, '-0');
      repackingPlanningDetailRepackeds = repackingDetail.repackingPlanningDetailRepackeds;
      for (const entityRepacked of repackingPlanningDetailRepackeds) {
        packingCode = entityRepacked.productPacking?.code;
        qrBase64 = entityRepacked.qRCode;
        quantityPackingType = entityRepacked.productPacking?.packingType?.quantity;
        packingName = entityRepacked.productPacking?.name;

        content = RepackingV2Service.createContentPrint(packingCode, qrBase64, expiredDate, store,
          packingName, quantityPackingType, repackeDate, hotline);
        for (let i = 0; i < (entityRepacked.quantity ? entityRepacked.quantity : 0); i++) {
          divContents.push(content);
        }

      }
    }

    RepackingV2Service.openWindow(divContents);
  }

  onClaimDetail() {
    if (this.claimId) {
      this.router.navigate(['/claim/dashboard/' + this.claimId]).then();
    }
  }
}
