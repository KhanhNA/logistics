import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  DateUtilService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute, Router} from '@angular/router';
import {ControlValueAccessor, FormBuilder, FormControl} from '@angular/forms';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {RepackingAuthoritiesService} from '../../../_services/authority/repacking.authorities.service';
import {CommonHttpService} from '../../../_services/http/common/common.http.service';
import {RepackingPlanningModel} from '../../../_models/repacking.planning.model';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {RepackingV2Service} from '../repacking-v2.service';
import {MatStepper} from '@angular/material/stepper';
import {repackingStatusAction} from '../../../_models/const/repacking.const';

@Component({
  selector: 'app-add-edit-repacking-v2',
  templateUrl: './add-edit-repacking-v2.component.html',
  styleUrls: ['./add-edit-repacking-v2.component.css']
})
export class AddEditRepackingV2Component extends BaseAddEditLayout implements OnInit {
  moduleName = 'repacking.ae';
  repackingPlan: RepackingPlanningModel | undefined;
  isIgnore = false;
  isView = false;

  @ViewChild('stepper') stepper: MatStepper | undefined;

  get repackingStatusAction() {
    return repackingStatusAction;
  }

  tempControl = new FormControl('');

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private dateUtilService: DateUtilService,
              public repackingAuthoritiesService: RepackingAuthoritiesService,
              protected commonHttpService: CommonHttpService,
              protected repackingV2Service: RepackingV2Service,
  ) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    activatedRoute.data.subscribe(data => {
      this.isView = !!data.isView;
    });
  }

  async ngOnInit() {
    super.ngOnInit();
    if (this.isEdit) {
      this.repackingPlan = await this.repackingV2Service.createGetRepackingPlanObs(this.id).toPromise();
      this.setStepper();
    }
  }

  setStepper() {
    if (this.stepper) {
      if (repackingStatusAction.isNewStatus(this.repackingPlan)) {
        // this.planComp?.refresh(this.repackingPlan);
      } else if (repackingStatusAction.isPlannedStatus(this.repackingPlan) || repackingStatusAction.isRepackedStatus(this.repackingPlan)) {
        this.stepper.selectedIndex = 1;
      } else if (repackingStatusAction.isImportedStatus(this.repackingPlan)) {
        this.isIgnore = true;
        this.stepper.selectedIndex = 1;
        this.isIgnore = false;
        this.stepper.selectedIndex = 2;
      }
    }
  }

  async onSelectionChange(event: StepperSelectionEvent) {
    if (!this.isIgnore) {
      this.repackingV2Service.selectedTabIndex.next(event.selectedIndex);
      this.repackingPlan = await this.repackingV2Service.createGetRepackingPlanObs(this.id).toPromise();
      // switch (event.selectedIndex) {
      //   case 0:
      //     this.planComp?.refresh(this.repackingPlan);
      //     break;
      //   case 1:
      //     this.repackComp?.refresh(this.repackingPlan, this.actionType, this.parentId);
      //     break;
      //   case 2:
      //     this.importStock?.refresh(this.repackingPlan, this.actionType, this.parentId);
      //     break;
      //
      // }
    }
  }

  updateLabel(event: any) {
    switch (event.label) {
      case 'menu.repacking.edit':
      case 'menu.repacking.view':
        event.label = this.repackingPlan?.code;
        break;
    }
  }
}
