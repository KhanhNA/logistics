import {ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {DatePipe, Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  DateUtilService,
  IconTypeEnum,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {RepackingAuthoritiesService} from '../../../../_services/authority/repacking.authorities.service';
import {CommonHttpService} from '../../../../_services/http/common/common.http.service';
import {RepackingV2Service} from '../../repacking-v2.service';
import {ClaimService} from '../../../../_services/http/claim/claim.service';
import {MatDialog} from '@angular/material/dialog';
import {CommonUtils} from '../../../../_utils/common.utils';
import {RepackingPlanningModel} from '../../../../_models/repacking.planning.model';
import {RepackingPlanDetailRepackedModel} from '../../../../_models/repacking/repacking.plan.detail.repacked.model';
import {ClaimEnum, ImportStatementStatus} from "../../../../_models/action-type";
import {ClaimDetailModel} from "../../../../_models/claim.detail.model";
import {repackingStatusAction} from "../../../../_models/const/repacking.const";
import {ImportPoDetailModel} from "../../../../_models/import-po/import.po.detail.model";
import {ImportPoDetailPalletModel} from "../../../../_models/import-po/import.po.detail.pallet.model";
import {ProductDescriptionsUtils} from "../../../../_utils/product.descriptions.utils";
import {ClaimAuthoritiesService} from "../../../../_services/authority/claim.authorities.service";
import {DividePackingComponent} from "../../../divide-packing/divide-packing.component";
import {ImportStatementModel} from "../../../../_models/import-statement/import.statement.model";
import {ImportStatementDetailPalletModel} from "../../../../_models/import-statement/import.statement.detail.pallet.model";

@Component({
  selector: 'app-import-stock-v2',
  templateUrl: './import-stock-v2.component.html',
  styleUrls: ['./import-stock-v2.component.scss']
})
export class ImportStockV2Component extends BaseAddEditLayout implements OnInit, OnChanges {
  @Input() moduleName = 'repacking.ae';
  @Input() isView = false;
  @Input() repackingPlan: RepackingPlanningModel | undefined;
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  repackedDetailsFormControl = new FormControl([]);
  palletOptions: SelectModel[] = [];
  isApprovePending = false;
  claimId: number | undefined;
  importStatementDetails: { productPackingId: any; quantity: any; productPackingCode: any; expireDate: any }[] = [];

  get repackingStatusAction() {
    return repackingStatusAction;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private dateUtilService: DateUtilService,
              public repackingAuthoritiesService: RepackingAuthoritiesService,
              protected commonHttpService: CommonHttpService,
              protected repackingV2Service: RepackingV2Service,
              protected claimService: ClaimService,
              public claimAuthoritiesService: ClaimAuthoritiesService,
              private dialog: MatDialog,
              private datePipe: DatePipe
  ) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.columns = [
      {
        columnDef: 'checked', header: 'checked',
        title: (e: RepackingPlanDetailRepackedModel) => `${e.checked}`,
        cell: (e: RepackingPlanDetailRepackedModel) => `${e.checked}`,
        className: 'mat-column-stt',
        columnType: ColumnTypes.CHECKBOX,
        display: () => repackingStatusAction.isImportedStatus(this.repackingPlan),
        align: AlignEnum.CENTER,
      },
      {
        columnDef: 'stt', header: 'stt',
        title: (e: any) => `${CommonUtils.getPosition(e, this.repackedDetailsFormControl.value)}`,
        cell: (e: any) => `${CommonUtils.getPosition(e, this.repackedDetailsFormControl.value)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
      },
      {
        columnDef: 'packageCode', header: 'packingCode',
        title: (e: RepackingPlanDetailRepackedModel) => `${e.productPacking?.code + ''}`,
        cell: (e: RepackingPlanDetailRepackedModel) => `${e.productPacking?.code + ''}`,
        className: 'mat-column-packing-code',
      },
      {
        columnDef: 'packageName', header: 'packingName',
        title: (e: RepackingPlanDetailRepackedModel) => `${e.productPacking?.product?.productDescriptions.find(des => des.language && des.language.code === translateService.currentLang)?.name + ''}`,
        cell: (e: RepackingPlanDetailRepackedModel) => `${e.productPacking?.product?.productDescriptions.find(des => des.language && des.language.code === translateService.currentLang)?.name + ''}`,
        className: 'mat-column-packing-name',
      },
      {
        columnDef: 'packingType', header: 'packingType',
        title: (e: RepackingPlanDetailRepackedModel) => `${e.productPacking?.packingType?.quantity + ''}`,
        cell: (e: RepackingPlanDetailRepackedModel) => `${e.productPacking?.packingType?.quantity + ''}`,
        className: 'mat-column-packing-type',
        align: AlignEnum.CENTER,
        alignHeader: AlignEnum.CENTER
      },
      {
        columnDef: 'uom', header: 'uom',
        title: (e: RepackingPlanDetailRepackedModel) => `${e.productPacking?.uom + ''}`,
        cell: (e: RepackingPlanDetailRepackedModel) => `${e.productPacking?.uom + ''}`,
        className: 'mat-column-uom'
      },
      {
        columnDef: 'expireDate', header: 'expireDate',
        title: (e: RepackingPlanDetailRepackedModel) => `${dateUtilService.convertDateToDisplayServerTime(e.expireDate + '')}`,
        cell: (e: RepackingPlanDetailRepackedModel) => `${dateUtilService.convertDateToDisplayServerTime(e.expireDate + '')}`,
        className: 'mat-column-expireDate',
      },
      {
        columnDef: 'quantity', header: 'quantity',
        title: (e: RepackingPlanDetailRepackedModel) => `${e.quantity ? e.quantity : 0}`,
        cell: (e: RepackingPlanDetailRepackedModel) => `${e.quantity ? e.quantity : 0}`,
        className: 'mat-column-quantity',
      },
      {
        columnDef: 'palletId',
        columnType: () => this.isView || repackingStatusAction.isImportedStatus(this.repackingPlan)
          ? ColumnTypes.VIEW : ColumnTypes.MULTI_SELECT_AUTOCOMPLETE,
        optionValues: () => this.palletOptions,
        isRequired: () => !this.isView && !repackingStatusAction.isImportedStatus(this.repackingPlan),
        header: () => 'common.pallet',
        title: (e: RepackingPlanDetailRepackedModel) => `${e.pallet?.displayName}`,
        cell: (e: RepackingPlanDetailRepackedModel) => `${e.pallet?.displayName}`,
        disabled: (e: RepackingPlanDetailRepackedModel) => !!this.isView || !!this.isApprovePending,
        onCellValueChange: (e: RepackingPlanDetailRepackedModel) => {
          if (!e.pallet || e.pallet.id !== e.palletId) {
            e.pallet = this.palletOptions.find(p => p.value === e.palletId)?.rawData;
          }
        },
        alignHeader: AlignEnum.CENTER
      },
    ];
    this.buttons = [
      {
        columnDef: 'add_box',
        title: 'common.title.add_box',
        icon: 'fa fa-plus-square',
        iconType: IconTypeEnum.FONT_AWESOME,
        header: 'common.action',
        className: 'primary',
        click: 'dividePackingEvent',
        display: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !this.isView &&
          this.repackingAuthoritiesService.hasCreateImportStatementFromRepackingRole()
          && repackingStatusAction.isRepackedStatus(this.repackingPlan),
        disabled: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !this.isEnableBtn(e, 'add_box')
      },
      {
        columnDef: 'clear',
        title: 'common.title.clear',
        icon: 'fa fa-times',
        iconType: IconTypeEnum.FONT_AWESOME,
        header: 'common.action',
        className: 'danger',
        click: 'removePackingEvent',
        display: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !this.isView &&
          this.repackingAuthoritiesService.hasCreateImportStatementFromRepackingRole()
          && repackingStatusAction.isRepackedStatus(this.repackingPlan),
        disabled: (e: ImportPoDetailModel | ImportPoDetailPalletModel | any) => !this.isEnableBtn(e, 'clear')
      },
    ];
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('repackingPlan' in changes) {
      this.repackingPlan = changes.repackingPlan.currentValue;
      if (this.repackingV2Service.selectedTabIndex.value === 2) {
        this.refresh(this.repackingPlan);
      }
    }
  }

  async ngOnInit() {
    super.ngOnInit();
    this.addEditForm = this.formBuilder.group({
      description: [''],
    });

    // const palletRes = await this.commonHttpService.createGetPalletObs(this.repackingPlan?.store?.id, 1, true).toPromise() as Page;
    // this.palletOptions = (palletRes.content as PalletModel[]).map(pallet => {
    //   return new SelectModel(pallet.id, pallet.displayName + '', false, pallet);
    // });
  }

  isEnableBtn(e: RepackingPlanDetailRepackedModel, buttonName: 'add_box' | 'clear') {
    if (this.isApprovePending || this.isView) {
      return false;
    }
    const repackedDetails = this.repackedDetailsFormControl.value ? this.repackedDetailsFormControl.value : [];
    const ind = repackedDetails.indexOf(e);
    if ((repackedDetails[ind - 1] && repackedDetails[ind - 1].id === e?.id) ||
      (repackedDetails[ind + 1] && repackedDetails[ind + 1].id === e?.id)) {
      if (Number(e.quantity) <= 1) {
        return ['clear'].includes(buttonName);
      }
      return true;
    } else {
      if (Number(e.quantity) <= 1) {
        return false;
      }
      return ['add_box'].includes(buttonName);
    }
  }

  dividePackingEvent(row: RepackingPlanDetailRepackedModel, index: number) {
    const shallowCopy = {...row};
    this.dialog.open(DividePackingComponent, {
      disableClose: false,
      height: '38%',
      width: '40vw',
      data: {
        packing: shallowCopy, type: 'from-repack', store: this.repackingPlan?.store,
        nameCol: {name: 'productPacking.product.name'}
      }
    }).afterClosed().subscribe(response => {
      if (response) {
        this.dividePacking(response.data, {ind: index, row});
      }
    });
  }

  removePackingEvent(row: RepackingPlanDetailRepackedModel, index: number) {
    this.removeDividedPacking({ind: index, row});
  }

  dividePacking(dividedPacking: any, originPacking: { ind: number, row: RepackingPlanDetailRepackedModel }) {
    const tblData = this.repackedDetailsFormControl.value as RepackingPlanDetailRepackedModel[];
    const length = tblData.length;
    const dataTable = [];
    dividedPacking.palletId = dividedPacking.pallet?.id ? dividedPacking.pallet?.id : undefined;
    for (let i = 0; i < length; i++) {
      dataTable.push(tblData[i]);
      if (i === originPacking.ind) {
        tblData[i].quantity = Number(tblData[i].quantity) - Number(dividedPacking.quantity);
        dividedPacking.maxQuantity = Number(tblData[i].maxQuantity) - Number(tblData[i].quantity);
        dividedPacking.orgQuantity = dividedPacking.quantity;
        dividedPacking.claimQuantity = 0;
        tblData[i].maxQuantity = tblData[i].quantity;
        tblData[i].orgQuantity = Number(tblData[i].orgQuantity) - Number(dividedPacking.quantity);
        dataTable.push(dividedPacking);
      }
    }
    this.repackedDetailsFormControl.setValue(dataTable);
  }

  removeDividedPacking(removeObj: { ind: number, row: RepackingPlanDetailRepackedModel }) {
    const tblData = this.repackedDetailsFormControl.value as RepackingPlanDetailRepackedModel[];
    const beforeObj = tblData[removeObj.ind - 1];
    const afterObj = tblData[removeObj.ind + 1];
    if ((removeObj.ind === 0 && afterObj.id !== removeObj.row.id) ||
      (removeObj.ind === tblData.length - 1 && beforeObj.id !== removeObj.row.id) ||
      (beforeObj && beforeObj.id !== removeObj.row.id && afterObj.id !== removeObj.row.id)) {
      this.utilsService.showWarningToarst('common.packing.remove.not.divide');
      return;
    }
    if (beforeObj && beforeObj.id === removeObj.row.id) {
      beforeObj.quantity = Number(removeObj.row.quantity) + Number(beforeObj.quantity);
      beforeObj.maxQuantity = Number(removeObj.row.maxQuantity) + Number(beforeObj.maxQuantity);
      beforeObj.orgQuantity = Number(beforeObj.orgQuantity) + Number(removeObj.row.orgQuantity);
      beforeObj.claimQuantity = Number(beforeObj.claimQuantity) + Number(removeObj.row.claimQuantity);
    } else if (afterObj.id === removeObj.row.id) {
      afterObj.quantity = Number(removeObj.row.quantity) + Number(afterObj.quantity);
      afterObj.maxQuantity = Number(removeObj.row.maxQuantity) + Number(afterObj.maxQuantity);
      afterObj.orgQuantity = Number(afterObj.orgQuantity) + Number(removeObj.row.orgQuantity);
      afterObj.claimQuantity = Number(afterObj.claimQuantity) + Number(removeObj.row.claimQuantity);
    }
    tblData.splice(removeObj.ind, 1);
    this.repackedDetailsFormControl.setValue(tblData);
  }

  async refresh(repackingPlan: RepackingPlanningModel | undefined) {
    if (!repackingPlan?.id) {
      return;
    }
    this.repackingPlan = repackingPlan;
    if (CommonUtils.isEmptyList(this.palletOptions)) {
      const palletRes = await this.commonHttpService.createGetPalletObs(this.repackingPlan.store?.id, 2, true).toPromise() as Page;
      this.palletOptions = palletRes.content.map(p => {
        return new SelectModel(p.id, p.displayName + '', false, p);
      });
    }
    this.getDataAndSetData();
  }

  initTableData(claimMap: Map<string, number>, data: RepackingPlanDetailRepackedModel[]) {
    const map = new Map();
    const claimAllRepackingDetailIds = data.filter((repacked: RepackingPlanDetailRepackedModel) => {
      return claimMap.has(repacked.productPacking?.code + '') && repacked.quantity === 0;
    }).map(repacked => repacked.id);
    for (const repackingPlanDetail of data) {
      const repackingPlanDetailPackingId = repackingPlanDetail.productPacking?.id;
      const repackingPlanDetailExpireDate = repackingPlanDetail.repackingPlanningDetail?.expireDate;
      repackingPlanDetail.expireDate = repackingPlanDetail.repackingPlanningDetail?.expireDate;
      const key = '' + repackingPlanDetailExpireDate + repackingPlanDetailPackingId;
      if (map.has(key)) {
        const detail = map.get(key);
        detail.quantity = Number(repackingPlanDetail.quantity) + Number(detail.quantity);
      } else {
        if (Number(repackingPlanDetail.quantity) > 0 || claimAllRepackingDetailIds.includes(repackingPlanDetail.id)) {
          if (claimMap.has(key)) {
            repackingPlanDetail.orgQuantity = repackingPlanDetail.quantity;
            repackingPlanDetail.claimQuantity = Number(claimMap.get(key));
            repackingPlanDetail.quantity = Number(repackingPlanDetail.quantity) - Number(claimMap.get(key));
          } else {
            repackingPlanDetail.orgQuantity = repackingPlanDetail.quantity;
            repackingPlanDetail.claimQuantity = 0;
          }
          map.set(key, repackingPlanDetail);
        }
      }
      ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(repackingPlanDetail.productPacking?.product);
      repackingPlanDetail.maxQuantity = repackingPlanDetail.quantity;
    }

    this.importStatementDetails = Array.from(map.values()).map(detail => {
      return {
        productPackingId: detail.productPacking.id,
        productPackingCode: detail.productPacking.code,
        quantity: detail.quantity,
        expireDate: detail.expireDate
      };
    });

    this.repackedDetailsFormControl.setValue([...map.values()]);
  }

  async getDataAndSetData() {
    if (repackingStatusAction.isRepackedStatus(this.repackingPlan)) {
      // this.tsuService.showLoading();
      const claimMap = new Map();
      const data: RepackingPlanDetailRepackedModel[] = [];
      this.repackingPlan?.repackingPlanningDetails.forEach(detail => {
        data.push(...detail.repackingPlanningDetailRepackeds);
      });
      // const repackedPromise = this.repackingV2Service.createGetRepackedDetailsObs(this.id).toPromise();
      const claimPromise = this.claimService.getClaimByReferenceObs(this.id, ClaimEnum.IMPORT_STATEMENT_REPACKING_PLANNING).toPromise()
        .catch((error: any) => {
          this.isApprovePending = true;
          this.utilsService.showErrorToarst(error);
          this.initTableData(claimMap, data);
        });
      // const data = await repackedPromise;

      const claimReference = await claimPromise;
      if (this.isApprovePending) {
        return;
      }

      let key = '';
      if (claimReference.length > 0) {
        this.claimId = claimReference[0].claim?.id;
        claimReference.forEach((detail: ClaimDetailModel) => {
          key = '' + detail.expireDate + detail.productPacking?.id;
          claimMap.set(key, detail.quantity);
        });
      }
      this.initTableData(claimMap, data);
    } else if (repackingStatusAction.isImportedStatus(this.repackingPlan)) {
      this.addEditForm.patchValue({description: this.repackingPlan?.importStatement?.description});
      const claimMap = new Map();
      this.claimService.getClaimByReferenceObs(this.id, ClaimEnum.IMPORT_STATEMENT_REPACKING_PLANNING)
        .subscribe((claimReference: ClaimDetailModel[]) => {
          let key = '';
          if (claimReference.length > 0) {
            this.claimId = claimReference[0].claim?.id;
            claimReference.forEach((detail: any) => {
              key = '' + detail.expireDate + detail.productPacking.id;
              detail.orgQuantity = detail.quantity;
              detail.claimQuantity = detail.quantity;
              detail.quantity = Number(detail.orgQuantity) - Number(detail.claimQuantity);
              claimMap.set(key, detail);
            });
          }

          let claimKey = '';
          this.repackingPlan?.importStatement?.importStatementDetailPallets.forEach((detailPallet: ImportStatementDetailPalletModel) => {
            claimKey = '' + detailPallet.expireDate + detailPallet.productPackingId;
            detailPallet.productPacking = detailPallet.palletDetail?.productPacking;
            detailPallet.orgQuantity = detailPallet.quantity;
            detailPallet.claimQuantity = 0;
            detailPallet.palletId = detailPallet.pallet?.id ? detailPallet.pallet?.id : undefined;
            detailPallet.pallet = detailPallet.pallet;
            if (claimMap.has(claimKey)) {
              detailPallet.claimQuantity = claimMap.get(claimKey).orgQuantity;
              detailPallet.orgQuantity = Number(detailPallet.orgQuantity) + Number(detailPallet.claimQuantity);
              claimMap.delete(claimKey);
            }
          });

          this.repackedDetailsFormControl.setValue(this.repackingPlan?.importStatement?.importStatementDetailPallets);
        });
    }
  }

  onSave() {
    let count = 0;
    const map = new Map();
    let key = '';
    const tableData = this.repackedDetailsFormControl.value as RepackingPlanDetailRepackedModel[];
    for (const item of tableData) {
      count++;
      if (!item.pallet || !item.pallet.id) {
        const msg = this.translateService.instant('notice.update.pallet') + count;
        this.utilsService.showWarningToarst(msg);
        return;
      }
      key = '' + item.productPacking?.id + item.repackingPlanningDetail?.expireDate + item.pallet.id;
      if (map.has(key)) {
        const detailPallet = map.get(key);
        detailPallet.quantity = Number(detailPallet.quantity) + Number(item.quantity);
      } else {
        map.set(key, {
          pallet: item.pallet,
          quantity: item.quantity,
          productPackingId: item.productPacking?.id,
          expireDate: item.repackingPlanningDetail?.expireDate,
        });
      }
    }
    const data = {
      toStoreId: this.repackingPlan?.store ? this.repackingPlan.store.id : '',
      toStoreCode: this.repackingPlan?.store ? this.repackingPlan.store.code : '',
      fromStoreId: this.repackingPlan?.store ? this.repackingPlan.store.id : '',
      fromStoreCode: this.repackingPlan?.store ? this.repackingPlan.store.code : '',
      repackingPlanningId: this.id,
      description: this.addEditForm.value.description,
      status: ImportStatementStatus.NOT_UPDATE_INVENTORY,
      importStatementDetails: this.importStatementDetails,
      importStatementDetailPallets: [...map.values()]
    };


    let method;
    method = this.apiService.post('/import-statements/from-repacking-planning', data);
    this.utilsService.execute(method, this.onSuccessFunc, '.edit.success', 'common.confirmImport', ['common.product.packing.param']);
  }


  onPrintQrCode() {
    const checkedData = (this.repackedDetailsFormControl.value as ImportStatementDetailPalletModel[]).filter(row => row.checked);

    if (CommonUtils.isEmptyList(checkedData)) {
      this.utilsService.showWarningToarst('notice.checkbox.nocheck');
      return;
    }
    const divContents = [];
    const store = this.repackingPlan?.store?.code + '';
    const hotline = this.repackingPlan?.store?.phone + '';
    const repackedDate = this.datePipe.transform(this.repackingPlan?.repackedDate, 'HH:mm dd-MM-yyyy', '-0');
    for (const importStatementDetail of checkedData) {
      const expiredDate = this.dateUtilService.convertDateToDisplayServerTime(importStatementDetail.expireDate + '');
      const packingCode = importStatementDetail.productPacking?.code;
      const qrBase64 = importStatementDetail.storeProductPackingDetail?.qRCode;
      const quantityPackingType = importStatementDetail.productPacking?.packingType?.quantity;
      const packingName = importStatementDetail.productPacking?.name;

      const content = this.createContentPrint(packingCode, qrBase64, expiredDate, store,
        packingName, quantityPackingType, repackedDate, hotline);
      divContents.push(content);
    }

    this.openWindow(divContents);
  }

  createContentPrint(code: any, base64: any, expireDate: any, store: any, name: any,
                     quantity: any, repackDate: any, hotline: any) {
    const divContent = ' <div class="invoiceForm" style="display: flex;\n' +
      '  height: 1.94in;\n' +
      '  width: 3.14in;\n' +
      '  font-size: 15px;">\n' +
      '    <div class="qrCode" style="    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: start;\n' +
      '    align-items: center;\n' +
      '    margin: 0.15in 0.2in 0 0.3in;">\n' +
      '      <div class="code">\n' +
      '        <label>Code: </label>\n' +
      '        <label style="word-break: break-all">' + code + '</label>\n' +
      '      </div>\n' +
      '      <div id="img">\n' +
      '        <img src="data:image/png;base64, ' + base64 + '" style="width: 1in;height: 1in"/>\n' +
      '      </div>\n' +
      '      <div class="expiredDate" style="display: flex;\n' +
      '       flex-direction: column;' +
      '       margin-top: 0.025in">\n' +
      '        <label>Expire date: </label>\n' +
      '        <label>' + expireDate + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '    <div class="information" style="    margin: 0.15in 0 0.25in 0;\n' +
      '    width: calc(100% - 1.5in);\n' +
      '    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: space-between;\n' +
      '    font-size: 14px;">\n' +
      '      <div class="store">\n' +
      '        <label>Store: </label>\n' +
      '        <label style="word-break: break-all">' + store + '</label>\n' +
      '      </div>\n' +
      '      <div class="name">\n' +
      '        <label>Name: </label>\n' +
      '        <label>' + name + '</label>\n' +
      '      </div>\n' +
      '      <div class="block">\n' +
      '        <label>Block: </label>\n' +
      '        <label>' + quantity + '</label>\n' +
      '      </div>\n' +
      '      <div class="repackDate">\n' +
      '        <label>Repacked date: </label>\n' +
      '        <label style="height: 0.17in">' + repackDate + '</label>\n' +
      '      </div>\n' +
      '      <div class="hotline">\n' +
      '        <label>Hotline:</label>\n' +
      '        <label>' + hotline + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '  </div>\n';

    return divContent;
  }

  openWindow(contents: any) {
    const a = window.open('', '');
    if (a) {
      a.document.write('<html><head><style>div{' +
        'margin-bottom: 0.05in}\n' +
        'div:last-child { margin-bottom: 0;}\n@page {\n' +
        '  size: 3.15in 1.97in;\n' +
        '  margin: 0;\n' +
        '}</style></head>');
      a.document.write('<body style="margin: 0;padding: 0; ">');
      for (const content of contents) {
        a.document.write(content);
      }
      a.document.write('</body></html>');
      a.document.close();
      setTimeout(() => {
        a.print();
        a.close();
      }, 250);
    }
  }

  onClaimDetail() {
    if (this.claimId) {
      this.router.navigate(['/claim/dashboard/' + this.claimId]).then();
    } else {
      this.utilsService.showErrorToarst('claim.wait.approve.please');
    }
  }
}
