import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportStockV2Component } from './import-stock-v2.component';

describe('ImportStockV2Component', () => {
  let component: ImportStockV2Component;
  let fixture: ComponentFixture<ImportStockV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportStockV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportStockV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
