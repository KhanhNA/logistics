import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditRepackingV2Component } from './add-edit-repacking-v2.component';

describe('AddEditRepackingV2Component', () => {
  let component: AddEditRepackingV2Component;
  let fixture: ComponentFixture<AddEditRepackingV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditRepackingV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditRepackingV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
