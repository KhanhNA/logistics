import {ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  DateUtilService,
  IconTypeEnum,
  Page,
  SelectModel,
  UtilsService
} from "@next-solutions/next-solutions-base";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormControl} from "@angular/forms";
import {Location} from "@angular/common";
import {TranslateService} from "@ngx-translate/core";
import {RepackingAuthoritiesService} from "../../../../_services/authority/repacking.authorities.service";
import {CommonHttpService} from "../../../../_services/http/common/common.http.service";
import {RepackingV2Service} from "../../repacking-v2.service";
import {StoreModel} from "../../../../_models/store.model";
import {RepackingPlanningModel} from "../../../../_models/repacking.planning.model";
import {CommonUtils} from "../../../../_utils/common.utils";
import {RepackingPlanDetailModel} from "../../../../_models/repacking/repacking.plan.detail.model";
import {repackingStatusAction} from "../../../../_models/const/repacking.const";
import {
  PalletsProductPackingGroupByExpireDateComponent,
  ProductPackingIdExpireDateModel
} from "../../../dialog-select-product-packing/pallets-product-packing-group-by-expire-date.component";
import {MatDialog} from "@angular/material/dialog";
import {ProductPackingModel} from "../../../../_models/product.packing.model";
import {ProductDescriptionsUtils} from "../../../../_utils/product.descriptions.utils";
import {ProductModel} from "../../../../_models/product.model";
import {ProductPackingPriceModel} from "../../../../_models/product.packing.price.model";
import {ProductPackingExpireDateModel} from "../../../../_models/repacking/product.packing.expire.date.model";
import {RepackingPlanDetailPalletModel} from "../../../../_models/repacking/repacking.plan.detail.pallet.model";
import {ActionTypeEneum, ClaimEnum, RepackingStatusEnum} from "../../../../_models/action-type";
import {HttpParams} from "@angular/common/http";
import {ClaimDetailModel} from "../../../../_models/claim.detail.model";
import {ClaimService} from "../../../../_services/http/claim/claim.service";

@Component({
  selector: 'app-plan-v2',
  templateUrl: './plan-v2.component.html',
  styleUrls: ['./plan-v2.component.css']
})
export class PlanV2Component extends BaseAddEditLayout implements OnInit, OnChanges {

  @Input() moduleName = 'repacking';
  @Input() isView = false;
  @Input() currentTabIndex = 0;
  @Input() repackingPlan: RepackingPlanningModel | undefined;
  storeOptions: SelectModel[] = [];
  distributorOptions: SelectModel[] = [];

  columns: ColumnFields[] = [];
  viewColumns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  viewTableControl: FormControl = new FormControl([]);


  get repackingStatusAction() {
    return repackingStatusAction;
  }

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private dateUtilService: DateUtilService,
              public repackingAuthoritiesService: RepackingAuthoritiesService,
              protected commonHttpService: CommonHttpService,
              protected repackingV2Service: RepackingV2Service,
              protected claimService: ClaimService,
              private dialog: MatDialog
  ) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);

    this.columns = [
      {
        columnDef: 'stt', header: 'stt',
        title: (e: any) => `${CommonUtils.getPosition(e, this.addEditForm.value.repackingPlanningDetails)}`,
        cell: (e: any) => `${CommonUtils.getPosition(e, this.addEditForm.value.repackingPlanningDetails)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
      },
      {
        columnDef: 'packageCode', header: 'packingCode',
        title: (e: RepackingPlanDetailModel) => `${e.productPacking?.code + ''}`,
        cell: (e: RepackingPlanDetailModel) => `${e.productPacking?.code + ''}`,
        className: 'mat-column-packing-code',
      },
      {
        columnDef: 'packageName', header: 'packingName',
        title: (e: RepackingPlanDetailModel) => `${e.productPacking?.product?.productDescriptions.find(des => des.language && des.language.code === translateService.currentLang)?.name + ''}`,
        cell: (e: RepackingPlanDetailModel) => `${e.productPacking?.product?.productDescriptions.find(des => des.language && des.language.code === translateService.currentLang)?.name + ''}`,
        className: 'mat-column-packing-name',
      },
      {
        columnDef: 'packingType', header: 'packingType',
        title: (e: RepackingPlanDetailModel) => `${e.productPacking?.packingType?.quantity + ''}`,
        cell: (e: RepackingPlanDetailModel) => `${e.productPacking?.packingType?.quantity + ''}`,
        className: 'mat-column-packing-type',
      },
      {
        columnDef: 'uom', header: 'uom',
        title: (e: RepackingPlanDetailModel) => `${e.productPacking?.uom + ''}`,
        cell: (e: RepackingPlanDetailModel) => `${e.productPacking?.uom + ''}`,
        className: 'mat-column-uom',
      },
      {
        columnDef: 'expireDate', header: 'expireDate',
        title: (e: RepackingPlanDetailModel) => `${dateUtilService.convertDateToDisplayServerTime(e.expireDate + '')}`,
        cell: (e: RepackingPlanDetailModel) => `${dateUtilService.convertDateToDisplayServerTime(e.expireDate + '')}`,
        className: 'mat-column-expireDate',
      },
      {
        columnDef: 'stock', header: 'stock',
        title: (e: RepackingPlanDetailModel) => `${e.maxQuantity ? e.maxQuantity.toLocaleString('en-US') : 0}`,
        cell: (e: RepackingPlanDetailModel) => `${e.maxQuantity ? e.maxQuantity.toLocaleString('en-US') : 0}`,
        className: 'mat-column-stock',
        display: () => !this.isEdit || (this.isEdit && repackingStatusAction.isNewStatus(this.repackingPlan))
      },
      {
        columnDef: 'quantity', header: 'quantity',
        columnType: ColumnTypes.INPUT_COUNTER,
        isRequired: true,
        min: (e) => 1,
        max: (e: RepackingPlanDetailModel) => (!this.isEdit || (this.isEdit && repackingStatusAction.isNewStatus(this.repackingPlan)))
          ? e.maxQuantity ? e.maxQuantity : 0 : e.quantity,
        title: (e: RepackingPlanDetailModel) => `${e.quantity ? e.quantity : 0}`,
        cell: (e: RepackingPlanDetailModel) => `${e.quantity ? e.quantity : 0}`,
        disabled: (e: RepackingPlanDetailModel) => this.isEdit && !repackingStatusAction.isNewStatus(this.repackingPlan),
        className: 'mat-column-quantity',
        alignHeader: AlignEnum.RIGHT
      },
      {
        columnDef: 'totalQuantity', header: 'totalQuantity',
        title: (e: RepackingPlanDetailModel) => `${this.getTotalQuantity(e).toLocaleString('en-US')}`,
        cell: (e: RepackingPlanDetailModel) => `${this.getTotalQuantity(e).toLocaleString('en-US')}`,
        className: 'mat-column-quantity',
        align: AlignEnum.RIGHT,
        alignHeader: AlignEnum.RIGHT
      },
    ];
    this.viewColumns = [
      {
        columnDef: 'stt', header: 'stt',
        title: (e: any) => `${CommonUtils.getPosition(e, this.viewTableControl.value)}`,
        cell: (e: any) => `${CommonUtils.getPosition(e, this.viewTableControl.value)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
      },
      {
        columnDef: 'packageCode', header: 'packingCode',
        title: (e: RepackingPlanDetailPalletModel) => `${e.repackingPlanningDetail?.productPacking?.code + ''}`,
        cell: (e: RepackingPlanDetailPalletModel) => `${e.repackingPlanningDetail?.productPacking?.code + ''}`,
        className: 'mat-column-packing-code',
      },
      {
        columnDef: 'packageName', header: 'packingName',
        title: (e: RepackingPlanDetailPalletModel) => `${e.repackingPlanningDetail?.productPacking?.product?.productDescriptions.find(des => des.language && des.language.code === translateService.currentLang)?.name + ''}`,
        cell: (e: RepackingPlanDetailPalletModel) => `${e.repackingPlanningDetail?.productPacking?.product?.productDescriptions.find(des => des.language && des.language.code === translateService.currentLang)?.name + ''}`,
        className: 'mat-column-packing-name',
      },
      {
        columnDef: 'packingType', header: 'packingType',
        title: (e: RepackingPlanDetailPalletModel) => `${e.repackingPlanningDetail?.productPacking?.packingType?.quantity + ''}`,
        cell: (e: RepackingPlanDetailPalletModel) => `${e.repackingPlanningDetail?.productPacking?.packingType?.quantity + ''}`,
        className: 'mat-column-packing-type',
        alignHeader: AlignEnum.CENTER,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'uom', header: 'uom',
        title: (e: RepackingPlanDetailModel) => `${e.repackingPlanningDetail?.productPacking?.uom + ''}`,
        cell: (e: RepackingPlanDetailModel) => `${e.repackingPlanningDetail?.productPacking?.uom + ''}`,
        className: 'mat-column-uom',
      },
      {
        columnDef: 'expireDate', header: 'expireDate',
        title: (e: RepackingPlanDetailPalletModel) => `${dateUtilService.convertDateToDisplayServerTime(e.repackingPlanningDetail?.expireDate + '')}`,
        cell: (e: RepackingPlanDetailPalletModel) => `${dateUtilService.convertDateToDisplayServerTime(e.repackingPlanningDetail?.expireDate + '')}`,
        className: 'mat-column-expireDate',
      },
      {
        columnDef: 'pallet', header: 'pallet',
        title: (e: RepackingPlanDetailPalletModel) => `${e.palletDetail?.pallet?.displayName + ''}`,
        cell: (e: RepackingPlanDetailPalletModel) => `${e.palletDetail?.pallet?.displayName + ''}`,
        className: 'mat-column-pallet',
      },
      {
        columnDef: 'quantity', header: 'quantity',
        title: (e: RepackingPlanDetailPalletModel) => `${e.quantity ? e.quantity.toLocaleString('en-US') : 0}`,
        cell: (e: RepackingPlanDetailPalletModel) => `${e.quantity ? e.quantity.toLocaleString('en-US') : 0}`,
        className: 'mat-column-quantity',
        alignHeader: AlignEnum.CENTER,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'totalQuantity', header: 'totalQuantity',
        title: (e: RepackingPlanDetailPalletModel) => `${this.getTotalQuantityPalletDetail(e).toLocaleString('en-US')}`,
        cell: (e: RepackingPlanDetailPalletModel) => `${this.getTotalQuantityPalletDetail(e).toLocaleString('en-US')}`,
        className: 'mat-column-quantity',
        alignHeader: AlignEnum.CENTER,
        align: AlignEnum.CENTER
      },
    ];

    this.buttons = [
      {
        columnDef: 'delete',
        header: 'common.action',
        click: 'onDeleteDetail',
        title: 'common.title.delete',
        iconType: IconTypeEnum.FONT_AWESOME,
        icon: 'fa fa-trash',
        className: 'danger',
        display: (e: RepackingPlanningModel) => !this.isEdit || repackingStatusAction.isNewStatus(this.repackingPlan),
        color: 'danger'
      },
    ];
  }

  getTotalQuantity(detail: RepackingPlanDetailModel | undefined) {
    if (!detail?.quantity || !detail?.productPacking?.packingType?.quantity) {
      return 0;
    }
    return detail.quantity * detail.productPacking?.packingType.quantity;
  }

  getTotalQuantityPalletDetail(detail: RepackingPlanDetailPalletModel | undefined) {
    if (!detail?.quantity || !detail?.repackingPlanningDetail?.productPacking?.packingType?.quantity) {
      return 0;
    }
    return detail.quantity * detail.repackingPlanningDetail?.productPacking?.packingType.quantity;
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('repackingPlan' in changes) {
      this.repackingPlan = changes.repackingPlan.currentValue;
      if (this.repackingV2Service.selectedTabIndex.value === 0) {
        this.refresh(this.repackingPlan);
      }
    }
  }

  async ngOnInit() {
    super.ngOnInit();

    this.addEditForm = this.formBuilder.group({
      id: [''],
      code: [''],
      store: [''],
      distributor: [''],
      description: [''],
      repackingPlanningDetails: [''],
      status: [0] // NEW
    });

    const storePromise = this.commonHttpService.createGetFCStoreAvailableObservable().toPromise();
    const distributorPromise = this.commonHttpService.createGetAllDistributorObservable().toPromise();
    const storeRes = await storePromise;
    const distributorRes = await distributorPromise;
    this.storeOptions = (storeRes.content as StoreModel[]).map(s => new SelectModel(s.id, s.code + ' - ' + s.name, false, s));
    this.distributorOptions = distributorRes.map(d => new SelectModel(d.id, d.code + ' - ' + d.name, false, d));
    this.addEditForm.patchValue({
      store: CommonUtils.isEmptyList(this.storeOptions) ? '' : this.storeOptions[0].value,
      distributor: CommonUtils.isEmptyList(this.distributorOptions) ? '' : this.distributorOptions[0].value,
    });
    if (this.isEdit && this.repackingPlan) {
      if (this.isView) {
        const detailPallets: RepackingPlanDetailPalletModel[] = [];
        this.repackingPlan.repackingPlanningDetails.forEach(detail => {
          detailPallets.push(...detail.repackingPlanningDetailPallets);
        });
        this.viewTableControl.setValue(detailPallets);
      } else if (repackingStatusAction.isNewStatus(this.repackingPlan)) {
        await this.getTotalQuantityExpireDate();
      }
      this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, this.repackingPlan));
    }
  }

  async refresh(repackingPlanning: RepackingPlanningModel | undefined) {
    if (!repackingPlanning?.id) {
      return;
    }
    if (repackingStatusAction.isNewStatus(repackingPlanning) && !this.isView) {
      const claimDetails: ClaimDetailModel[] =
        await this.claimService.getClaimByReferenceObs(repackingPlanning.id, ClaimEnum.REPACKING_PLANNING).toPromise()
          .catch(() => {
            this.repackingV2Service.isApprovedClaim.next(false);
          });
      if (claimDetails.length > 0) {
        this.repackingV2Service.isApprovedClaim.next(true);
        if (repackingStatusAction.isNewStatus(repackingPlanning) && !this.isView) {
          this.utilsService.showWarningToarst('common.claim.approved.not.update');
        }
      }
    }
    this.cdr.detectChanges();
  }

  async getTotalQuantityExpireDate() {
    const inventoryRes = await this.commonHttpService.createGetQuantityPackingExpireDateObs(this.repackingPlan).toPromise() as Page;
    const inventoryMap: Map<string, number> = new Map();
    (inventoryRes.content as ProductPackingExpireDateModel[]).forEach(pE => {
      inventoryMap.set(pE.productPacking?.id + '_' + pE.expireDate, pE.quantity ? pE.quantity : 0);
    });
    let key = '';
    this.repackingPlan?.repackingPlanningDetails.forEach(detail => {
      key = detail.productPacking?.id + '_' + detail.expireDate;
      if (inventoryMap.has(key)) {
        detail.maxQuantity = inventoryMap.get(key);
      } else {
        detail.maxQuantity = detail.quantity;
      }
    });
  }

  onStoreChange($event: any
  ) {
    this.addEditForm.patchValue({
      repackingPlanningDetails: []
    });
  }

  onDistributorChange($event: any
  ) {
    this.addEditForm.patchValue({
      repackingPlanningDetails: []
    });
  }

  convertToRows(result: any
  ) {
    const rows = [];
    if (result) {
      for (const item of Object.keys(result)) {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(result[item].productPacking.product);
        ProductModel.reduceCircleReference(result[item].product);
        result[item].productPackingPrices?.forEach((price: any) => {
          ProductPackingPriceModel.reduceCircleReference(price);
        });

        rows.push({
          productPacking: result[item].productPacking,
          expireDate: result[item].expireDate,
          maxQuantity: result[item].quantity,
          quantity: result[item].quantity
        });
        // if (this.actionType === ActionTypeEneum.new && this.formData.repackingPlanningDetails) {
        //   this.formData.repackingPlanningDetails.push({
        //     productPacking: result[item].productPacking,
        //     expireDate: result[item].expireDate,
        //     maxQuantity: result[item].quantity,
        //     quantity: result[item].quantity
        //   });
        // }
      }
    }
    return rows;
  }

  addTableData(ppList: ProductPackingModel[]
  ) {
    const rows = this.convertToRows(ppList);
    const {repackingPlanningDetails} = this.addEditForm.value;
    this.addEditForm.patchValue({
      repackingPlanningDetails: [...repackingPlanningDetails, ...rows]
    });
  }

  onAddDetail() {
    const {distributor, store, repackingPlanningDetails} = this.addEditForm.value;
    const productPackingIdExpireDate: ProductPackingIdExpireDateModel[] = [{
      productPackingId: -1,
      expireDate: ''
    }];
    if (Array.isArray(repackingPlanningDetails)) {
      for (const item of repackingPlanningDetails) {
        productPackingIdExpireDate.push({
          productPackingId: item.productPacking.id,
          expireDate: item.expireDate
        });
      }
    }

    const dialogRef = this.dialog.open(PalletsProductPackingGroupByExpireDateComponent, {
      disableClose: false,
      width: '90%',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        storeId: store ? store : 0,
        distributorId: distributor ? distributor : 0,
        repackingPlanningId: this.isEdit ? this.repackingPlan?.id : 0,
        palletSteps: 1,
        productPackingIdExpireDate
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.addTableData(result);
    });
  }

  onDeleteDetail(row: RepackingPlanDetailModel, index: number) {
    const {repackingPlanningDetails} = this.addEditForm.value;
    (repackingPlanningDetails as RepackingPlanDetailModel[]).splice(index, 1);
    this.addEditForm.patchValue({
      repackingPlanningDetails
    });
  }

  callApi = () => {
    let method;
    const repackingPlan = new RepackingPlanningModel(this.addEditForm);
    // repackingPlan = CommonUtils.deletePropertyValueOfObject(['uom'], repackingPlan, []);
    if (!this.isEdit) {
      method = this.apiService.post('/repacking-plannings', [repackingPlan]);
    } else {
      method = this.apiService.patch(`/repacking-plannings/${this.id}`, repackingPlan);
    }
    return method;
  }

  errFunc = (err: string) => {
    this.utilsService.showErrorToarst(err);
    // this.formData.repackingPlanningDetails = this.tempDetails;
  }

  onSavePlan() {
    // this.formData.repackingPlanningDetails = this.getTblData();
    this.utilsService.executeWithErrorHandle(this.callApi, this.saveSuccess1,
      this.isEdit ? '.edit.success' : '.add.success', 'common.confirmSave', ['repacking.param.plan'], '', [],
      this.errFunc);
  }

  saveSuccess1 = (data: RepackingPlanningModel | RepackingPlanningModel[], onSuccessMsg: string | undefined) => {
    this.utilsService.onSuccessFunc(onSuccessMsg);
    // this.formData.repackingPlanningDetails = this.tempDetails;
    if (Array.isArray(data) && data[0].status === RepackingStatusEnum.NEW) {
      this.router.navigate(['/repacking/edit/' + data[0].id]).then();
    } else if (!data[0]) {
      // this.getDataBinding();
      this.refresh(data[0] ? data[0] : data);
    }
  }

  onBack() {
    this.location.back();
  }
}
