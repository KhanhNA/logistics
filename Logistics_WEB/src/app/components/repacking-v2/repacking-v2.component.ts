import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnTypes, DateRangePickerModel,
  DateUtilService,
  FormStateService,
  IconTypeEnum, SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {RepackingPlanningModel} from '../../_models/repacking.planning.model';
import {repackingStatus, repackingStatusAction} from "../../_models/const/repacking.const";
import {RepackingAuthoritiesService} from "../../_services/authority/repacking.authorities.service";
import {CommonHttpService} from "../../_services/http/common/common.http.service";
import {StoreModel} from "../../_models/store.model";
import {HttpParams} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {DateUtils} from "../../base/Utils/date.utils";
import {ActionTypeEneum, RepackingStatusEnum} from "../../_models/action-type";
import {CommonUtils} from "../../_utils/common.utils";

@Component({
  selector: 'app-repacking-v2',
  templateUrl: './repacking-v2.component.html',
  styleUrls: ['./repacking-v2.component.css']
})
export class RepackingV2Component extends BaseSearchLayout implements OnInit {
  moduleName = 'repacking'
  buttons: ButtonFields[];
  storeOptions: SelectModel[] = [];
  statusOptions: SelectModel[] = [];
  dateTypeOptions: SelectModel[] = [];

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected dateUtilService: DateUtilService,
              protected authoritiesService: AuthoritiesService,
              public repackingAuthoritiesService: RepackingAuthoritiesService,
              protected commonHttpService: CommonHttpService,
  ) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        storeId: [''],
        // distributor: this.distributorCollection[0] ? this.distributorCollection[0] : '',
        rangeDate: [
          new DateRangePickerModel(dateUtilService.convertDateToStringCurrentGMT(
            dateUtilService.addDays(dateUtilService.getDateNow(), -30)),
            dateUtilService.convertDateToStringCurrentGMT(dateUtilService.getDateNow()))],
        fromDate: [''],
        toDate: [''],
        status: [0], // NEW
        dateType: [0], // NEW
      }));
    this.columns = [
      // {
      //   columnDef: 'checked',
      //   header: 'checked',
      //   title: (e: RepackingPlanningModel) => `true`,
      //   cell: (e: RepackingPlanningModel) => `true`,
      //   display: (_: RepackingPlanningModel) => true,
      //   columnType: ColumnTypes.CHECKBOX,
      //   align: AlignEnum.CENTER,
      // },
      {
        columnDef: 'stt', header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER,
      },
      {
        columnDef: 'code',
        header: () => 'repacking.ld.code',
        title: (e: RepackingPlanningModel) => `${e.code ? e.code : ''}`,
        cell: (e: RepackingPlanningModel) => `${e.code ? e.code : ''}`,
        display: (_: RepackingPlanningModel) => true,
      },
      {
        columnDef: 'description',
        header: () => 'repacking.ae.description',
        title: (e: RepackingPlanningModel) => `${e.description ? e.description : ''}`,
        cell: (e: RepackingPlanningModel) => `${e.description ? e.description : ''}`,
        display: (_: RepackingPlanningModel) => true,
      },
      {
        columnDef: 'status',
        header: () => 'common.status',
        title: (e: RepackingPlanningModel) => `${translateService.instant(repackingStatusAction.getStatusLabelKey(e) + '')}`,
        cell: (e: RepackingPlanningModel) => `${translateService.instant(repackingStatusAction.getStatusLabelKey(e) + '')}`,
        display: (_: RepackingPlanningModel) => true,
      },
      {
        columnDef: 'createUserObj',
        header: () => 'repacking.ae.createUser',
        title: (e: RepackingPlanningModel) => `${e.createUserObj?.firstName + ' '
        + e.createUserObj?.lastName + ' - ' + this.dateUtilService.convertDateToDisplayServerTime(e.planningDate + '')}`,
        cell: (e: RepackingPlanningModel) => `${e.createUserObj?.firstName + ' '
        + e.createUserObj?.lastName + ' - ' + this.dateUtilService.convertDateToDisplayServerTime(e.planningDate + '')}`,
        display: (_: RepackingPlanningModel) => true,
      },
    ];
    this.buttons = [
      {
        columnDef: 'edit',
        header: 'common.action',
        click: 'onEditRepacking',
        title: 'common.title.edit',
        iconType: IconTypeEnum.FONT_AWESOME,
        icon: 'fa fa-edit',
        className: 'primary',
        display: (e: RepackingPlanningModel) => (this.repackingAuthoritiesService.hasUpdateRepackingRole()
          || this.repackingAuthoritiesService.hasCreateImportStatementFromRepackingRole())
          && !(repackingStatusAction.isImportedStatus(e) || repackingStatusAction.isCanceledStatus(e)),
        color: 'primary'
      },
      {
        columnDef: 'cancel',
        header: 'common.action',
        title: 'common.title.cancel',
        click: 'onCancelRepacking',
        iconType: IconTypeEnum.FONT_AWESOME,
        icon: 'fa fa-times',
        className: 'danger',
        display: (e: RepackingPlanningModel) => this.repackingAuthoritiesService.hasCancelRepackingRole()
          && (repackingStatusAction.isNewStatus(e) || repackingStatusAction.isPlannedStatus(e)),
        color: 'danger'
      },
      {
        columnDef: 'excel',
        header: 'common.action',
        title: 'common.title.print',
        click: 'onPrintRepacking',
        iconType: IconTypeEnum.FONT_AWESOME,
        icon: 'fa fa-file-excel',
        className: 'info',
        display: (e: RepackingPlanningModel) => this.repackingAuthoritiesService.hasPrintRepackingRole(),
        color: 'info'
      },
      {
        columnDef: 'view',
        header: 'common.action',
        title: 'common.title.dashboard',
        click: 'onViewRepacking',
        iconType: IconTypeEnum.FONT_AWESOME,
        icon: 'fa fa-eye',
        className: 'info',
        display: (e: RepackingPlanningModel) => this.repackingAuthoritiesService.hasViewRepackingRole(),
        color: 'info'
      }
    ];
  }

  async ngOnInit() {
    this.statusOptions = Object.keys(repackingStatus).map(key => {
      return new SelectModel(repackingStatus[key].code, this.translateService.instant(repackingStatus[key].label));
    });
    this.dateTypeOptions = Object.keys(repackingStatus).map(key => {
      return new SelectModel(repackingStatus[key].code, this.translateService.instant(repackingStatus[key].label));
    });
    const storeRes = await this.commonHttpService.createGetFCStoreAvailableObservable().toPromise();
    this.storeOptions = (storeRes.content as StoreModel[]).map(s => new SelectModel(s.id, s.code + ' - ' + s.name, false, s));
    this.searchForm.patchValue({
      storeId: !CommonUtils.isEmptyList(this.storeOptions) ? this.storeOptions[0].value : '',
      status: this.statusOptions[0].value,
      dateType: this.dateTypeOptions[0].value,
    });
    super.ngOnInit();
    this.onSubmit();
  }

  search(): void {
    const {storeId, text, status, dateType, fromDate, toDate} = this.searchForm.value;
    const params = new HttpParams()
      .set('storeId', storeId ? storeId : '')
      // .set('distributorId', this.formData.distributor ? this.formData.distributor.id : '')
      .set('text', text ? text : '')
      .set('fromDate', fromDate ? fromDate : '')
      .set('toDate', toDate ? toDate : '')
      .set('dateType', (dateType ? dateType : 0) + '')
      .set('status', (status ? status : 0) + '');
    this._fillData('/repacking-plannings', params);
  }

  onCancelRepacking(row: RepackingPlanningModel, index: number) {
    let method;
    if (repackingStatusAction.isNewStatus(row)) {
      method = this.apiService.post(`/repacking-plannings/cancel-plan/${row.id}`, null);
    } else if (repackingStatusAction.isPlannedStatus(row)) {
      method = this.apiService.post(`/repacking-plannings/cancel-repack/${row.id}`, null);
    }
    this.utilsService.execute(method, this.onSuccessFunc, '.cancel.success', 'common.confirmCancel', ['common.repack']);
  }

  onPrintRepacking(repacking: RepackingPlanningModel) {

    this.utilsService.showConfirmDialog('common.confirmPrint', ['repacking.param.plan'])
      .afterClosed().subscribe(result => {
      if (result && result.value) {
        this.apiService.saveFile('/repacking-plannings/print/' + repacking.id, undefined, {});
      }
    });
  }


  onCreateRepacking() {
    this.router.navigate([this.router.url, ActionTypeEneum.new]);
  }

  onEditRepacking(row: RepackingPlanningModel, index: number) {
    this.router.navigate(['/repacking/edit/' + row.id]);
  }

  onViewRepacking(row: RepackingPlanningModel, index: number) {
    this.router.navigate(['/repacking/view/' + row.id]);
  }

  clearSearch() {
    this.searchForm.patchValue({
      text: '',
      storeId: this.storeOptions[0]?.value,
      // distributor: this.distributorCollection[0] ? this.distributorCollection[0] : '',
      rangeDate: [
        new DateRangePickerModel(this.dateUtilService.convertDateToStringCurrentGMT(
          this.dateUtilService.addDays(this.dateUtilService.getDateNow(), -30)),
          this.dateUtilService.convertDateToStringCurrentGMT(this.dateUtilService.getDateNow()))],
      status: 0,
      dateType: 0,
    });
  }
}
