import {Injectable} from '@angular/core';
import {ApiService} from "@next-solutions/next-solutions-base";
import {HttpParams} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {RepackingPlanningModel} from "../../_models/repacking.planning.model";
import {RepackingPlanDetailRepackedModel} from "../../_models/repacking/repacking.plan.detail.repacked.model";
import {ImportStatementModel} from "../../_models/import-statement/import.statement.model";

@Injectable()
export class RepackingV2Service {
  isApprovedClaim = new BehaviorSubject(true);
  selectedTabIndex = new BehaviorSubject(0);

  constructor(private apiService: ApiService) {
  }

  static createContentPrint(code: any, base64: any, expireDate: any, store: any, name: any,
                            quantity: any, repackDate: any, hotline: any) {
    const divContent = ' <div class="invoiceForm" style="display: flex;\n' +
      '  height: 1.94in;\n' +
      '  width: 3.14in;\n' +
      '  font-size: 15px;">\n' +
      '    <div class="qrCode" style="    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: start;\n' +
      '    align-items: center;\n' +
      '    margin: 0.15in 0.2in 0 0.3in;">\n' +
      '      <div class="code">\n' +
      '        <label>Code: </label>\n' +
      '        <label style="word-break: break-all">' + code + '</label>\n' +
      '      </div>\n' +
      '      <div id="img">\n' +
      '        <img src="data:image/png;base64, ' + base64 + '" style="width: 1in;height: 1in"/>\n' +
      '      </div>\n' +
      '      <div class="expiredDate" style="display: flex;\n' +
      '       flex-direction: column;' +
      '       margin-top: 0.025in">\n' +
      '        <label>Expire date: </label>\n' +
      '        <label>' + expireDate + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '    <div class="information" style="    margin: 0.15in 0 0.25in 0;\n' +
      '    width: calc(100% - 1.5in);\n' +
      '    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: space-between;\n' +
      '    font-size: 14px;">\n' +
      '      <div class="store">\n' +
      '        <label>Store: </label>\n' +
      '        <label style="word-break: break-all">' + store + '</label>\n' +
      '      </div>\n' +
      '      <div class="name">\n' +
      '        <label>Name: </label>\n' +
      '        <label>' + name + '</label>\n' +
      '      </div>\n' +
      '      <div class="block">\n' +
      '        <label>Block: </label>\n' +
      '        <label>' + quantity + '</label>\n' +
      '      </div>\n' +
      '      <div class="repackDate">\n' +
      '        <label>Repacked date: </label>\n' +
      '        <label style="height: 0.17in">' + repackDate + '</label>\n' +
      '      </div>\n' +
      '      <div class="hotline">\n' +
      '        <label>Hotline:</label>\n' +
      '        <label>' + hotline + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '  </div>\n';

    return divContent;
  }

  static openWindow(contents: any) {
    const a = window.open('', '');
    if (a) {
      a.document.write('<html><head><style>div{' +
        'margin-bottom: 0.05in}\n' +
        'div:last-child { margin-bottom: 0;}\n@page {\n' +
        '  size: 3.15in 1.97in;\n' +
        '  margin: 0;\n' +
        '}</style></head>');
      a.document.write('<body style="margin: 0;padding: 0; ">');
      for (const content of contents) {
        a.document.write(content);
      }
      a.document.write('</body></html>');
      a.document.close();
      setTimeout(() => {
        a.print();
        a.close();
      }, 250);
    }
  }

  createGetRepackingPlanObs(repackingPlanId: number): Observable<RepackingPlanningModel> {
    return this.apiService.get('/repacking-plannings/' + repackingPlanId, new HttpParams());
  }

  createGetRepackedDetailsObs(repackingPlanId: number): Observable<RepackingPlanDetailRepackedModel[]> {
    return this.apiService.get(`/repacking-plannings/detail-repacked/${repackingPlanId}`, new HttpParams());
  }

  createGetImportStatementFromRepackingObs(repackingPlanId: number): Observable<ImportStatementModel> {
    return this.apiService.get('/import-statements/repacking-planning/' + repackingPlanId, new HttpParams());
  }
}
