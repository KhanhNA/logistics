import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RepackingV2Component} from './repacking-v2.component';
import {SharedModule} from '../../modules/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {AuthoritiesResolverService, MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {RepackingAuthoritiesService} from '../../_services/authority/repacking.authorities.service';
import {AddEditRepackingV2Component} from './add-edit-repacking-v2/add-edit-repacking-v2.component';
import {RepackV2Component} from './add-edit-repacking-v2/repack-v2/repack-v2.component';
import {ImportStockV2Component} from './add-edit-repacking-v2/import-stock-v2/import-stock-v2.component';
import {PlanV2Component} from './add-edit-repacking-v2/plan-v2/plan-v2.component';
import {RepackingV2Service} from "./repacking-v2.service";
import {ClaimService} from "../../_services/http/claim/claim.service";
import {ClaimAuthoritiesService} from "../../_services/authority/claim.authorities.service";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/', suffix: '.json'},
    {prefix: './assets/i18n/repacking/', suffix: '.json'},

  ]);
}

const routes: Routes = [
  {
    pathMatch: 'full',
    path: '',
    resolve: {me: AuthoritiesResolverService},
    component: RepackingV2Component,
  },
  {
    path: 'add',
    resolve: {me: AuthoritiesResolverService},
    data: {breadcrumb: 'menu.repacking.add'},
    component: AddEditRepackingV2Component,
  },
  {
    path: 'edit/:id',
    resolve: {me: AuthoritiesResolverService},
    data: {breadcrumb: 'menu.repacking.edit'},
    component: AddEditRepackingV2Component,
  },
  {
    path: 'view/:id',
    resolve: {me: AuthoritiesResolverService},
    data: {breadcrumb: 'menu.repacking.view', isView: true},
    component: AddEditRepackingV2Component,
  },

];

@NgModule({
  declarations: [
    RepackingV2Component,
    AddEditRepackingV2Component,
    RepackV2Component,
    ImportStockV2Component,
    PlanV2Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      isolate: true,
    }),
  ],
  providers: [
    RepackingV2Service,
    ClaimService,
    ClaimAuthoritiesService,
    RepackingAuthoritiesService,
    DatePipe
  ]
})
export class RepackingV2Module {
}
