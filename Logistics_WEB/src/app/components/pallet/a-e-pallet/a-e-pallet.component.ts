import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ToastrService} from 'ngx-toastr';
import {DatePipe, Location} from '@angular/common';
import {CommunicationService} from '../../../communication-service';
import {TranslateService} from '@ngx-translate/core';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {NavService} from '../../../_services/nav.service';
import {AEPalletLayout} from './a-e-pallet.layout';
import {ActionTypeEneum} from '../../../_models/action-type';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-a-e-pallet',
  templateUrl: './a-e-pallet.component.html',
  styleUrls: ['../../form.layout.scss', './a-e-pallet.component.scss'],
  providers: [DatePipe]
})
export class AEPalletComponent implements OnInit, AfterViewInit {

  model: AEPalletLayout;
  title?: string;

  constructor(fb: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, private toastr: ToastrService, private datePipe: DatePipe,
              private serv: CommunicationService, tranService: TranslateService,
              tsuService: TsUtilsService, private route: ActivatedRoute,
              location: Location, private navService: NavService,
              protected authoritiesService: AuthoritiesService) {
    this.model = new AEPalletLayout(tsuService, userService, fb, route, this.datePipe, tranService, toastr, location, authoritiesService);
    route.queryParams.subscribe(query => {
      this.model.selectedStoreId = Number(query.storeId);
    });
  }

  ngOnInit() {
    if (this.model.actionType === ActionTypeEneum.edit) {
      this.title = 'common.pallet.edit';

      this.model.getById(this.model.parentId);
    } else if (this.model.actionType === ActionTypeEneum.new) {
      this.title = 'common.pallet.add';
    }
  }

  ngAfterViewInit(): void {
    this.navService.title = this.title;
  }

  onSave() {
    this.model.onSave();
  }
}
