import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {ActionTypeEneum} from '../../../_models/action-type';
import {HttpParams} from '@angular/common/http';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {NumberValidators} from '../../../base/field/number-validators';
import {StoreModel} from "../../../_models/store.model";

// @Directive()
export class AEPalletLayout extends LDBaseLayout {

  stores: any[] = [];
  selectedStore: StoreModel | undefined;
  selectedStoreId: number | undefined;
  inputFields: FieldConfigExt[] | undefined;

  constructor(tsuService: TsUtilsService, private apiService: ApiService, fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              transService: TranslateService, toastr: ToastrService, location: Location,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);

    this.getFormData();
  }

  getFormData() {
    this.getStoresApi().subscribe((response: any) => {
      this.stores = response.content;
      this.inputFields = [
        new FieldConfigExt({
          type: 'input',
          name: 'code',
          binding: 'code',
          label: 'common.code.auto.create',
          readonly: 'true'
        }),
        new FieldConfigExt({
          type: 'input',
          name: 'name',
          binding: 'name',
          label: 'common.pallet.name',
          require: 'true'
        }),
        new FieldConfigExt({
          type: 'select',
          name: 'step',
          binding: 'step',
          label: 'common.pallet.step',
          options: {1: 'pallet.step.one', 2: 'pallet.step.two'},
          require: 'true'
        }),
        new FieldConfigExt({
          type: 'input',
          name: 'length',
          binding: 'length',
          label: 'common.pallet.length',
          inputType: 'number',
          require: 'true',
          validations: [
            {
              name: 'integerNumber',
              validator: NumberValidators.integerValidation(),
              message: 'validation.length.integer'
            },
            {
              name: 'minQuantity',
              validator: NumberValidators.minQuantityValidation(0),
              message: 'validation.length'
            },
          ]
        }),

        new FieldConfigExt({
          type: 'input',
          name: 'width',
          binding: 'width',
          label: 'common.pallet.width',
          inputType: 'number',
          require: 'true',
          validations: [
            {
              name: 'integerNumber',
              validator: NumberValidators.integerValidation(),
              message: 'validation.width.integer'
            },
            {
              name: 'minQuantity',
              validator: NumberValidators.minQuantityValidation(0),
              message: 'validation.width'
            },
          ]
        }),
        new FieldConfigExt({
          type: 'input',
          name: 'height',
          binding: 'height',
          label: 'common.pallet.height',
          inputType: 'number',
          require: 'true',
          validations: [
            {
              name: 'integerNumber',
              validator: NumberValidators.integerValidation(),
              message: 'validation.height.integer'
            },
            {
              name: 'minQuantity',
              validator: NumberValidators.minQuantityValidation(0),
              message: 'validation.height'
            },
          ]
        }),
        new FieldConfigExt({
          type: 'autocomplete',
          name: 'store.name',
          binding: 'store',
          options: ['code', 'name'],
          collections: this.stores,
          label: 'menu.store',
          require: 'true',
          readonly: 'true'
        }),
        new FieldConfigExt({
          type: 'checkbox',
          name: 'status',
          binding: 'status',
          label: 'store.active',
          require: 'true'
        }),
      ];
      this.init(this.inputFields);
      if (this.actionType === ActionTypeEneum.new) {
        this.formData = {
          name: '',
          length: 1200,
          width: 120,
          height: 180,
          step: '1',
          status: true,
          store: this.selectedStoreId ? this.stores.filter(s => s.id === this.selectedStoreId)[0] : '',
        };
      }
      this.form.markAllAsTouched();
    });
  }

  getStoresApi() {
    const params = new HttpParams()
      .set('status', 'true')
      .set('text', '')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('is_DC', '')
      .set('pageNumber', '1')
      .set('pageSize', '' + 9999);

    return this.apiService.get('/stores', params);
  }

  getById(palletId: number | undefined) {
    if (palletId) {
      this.apiService.get('/pallets/' + palletId, new HttpParams())
        .subscribe((pallet: any) => {
          pallet.step = '' + pallet.step;
          this.formData = pallet;
        });
    }
  }

  saveSuccess = (data: any) => {
    console.log(this.entity + '.' + this.actionType + '.success');
    const msg = this.translateService.instant(this.entity + '.' + this.actionType + '.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
    setTimeout(() => {
      this.location.back();
    });
  }

  onSave() {
    let method;
    if (this.actionType === ActionTypeEneum.edit) {
      method = this.apiService.patch('/pallets/' + this.parentId, this.formData);
    } else if (this.actionType === ActionTypeEneum.new) {
      method = method = this.apiService.post('/pallets', [this.formData]);
    }
    this.tsuService.execute6(method, this.saveSuccess, 'common.confirmSave', ['common.pallet.content']);
  }
}
