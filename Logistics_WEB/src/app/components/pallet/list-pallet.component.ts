import {Component, Injector, Input, OnDestroy, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnTypes,
  FormStateService,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {PalletStatusEnum} from '../../_models/enums/PalletStatusEnum';
import {NavService} from '../../_services/nav.service';
import {PalletStepEnum} from '../../_models/enums/PalletStepEnum';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {StoreModel} from '../../_models/store.model';
import {HttpParams} from '@angular/common/http';
import {PalletModel} from '../../_models/pallet/pallet.model';
import {ActionTypeEneum} from '../../_models/action-type';
import {ToastrService} from 'ngx-toastr';
import {PalletAuthoritiesService} from "../../_services/authority/pallet.authorities.service";
import {StoreService} from "../store/store.service";

@Component({
  selector: 'app-list-pallet',
  templateUrl: './list-pallet.component.html',
  styleUrls: ['./list-pallet.component.scss'],
})
export class ListPalletComponent extends BaseSearchLayout implements OnInit {

  @Input() store?: StoreModel;

  storeId: number | undefined;
  moduleName = 'pallet';
  buttons: ButtonFields[] = [];

  storeValues: SelectModel[] = [];
  stepValues: SelectModel[] = [];
  statusValues: SelectModel[] = [];

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private toastr: ToastrService,
              public palletAuthoritiesService: PalletAuthoritiesService,
              private navService: NavService, private tsuService: TsUtilsService) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        store: [''],
        step: ['_1'],
        status: ['_true'],
      }));
    this.columns.push(
      {
        columnDef: 'checked',
        header: 'checked',
        title: (e: any) => `${e.checked}`,
        cell: (e: any) => `${e.checked}`,
        columnType: ColumnTypes.CHECKBOX,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'qrCode',
        header: 'qrCode',
        title: (e: PalletModel) => `${e.code}`,
        cell: (e: PalletModel) => `${e.qRCode}`,
        columnType: ColumnTypes.BASE64,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code',
        header: 'code',
        title: (e: PalletModel) => `${e.code}`,
        cell: (e: PalletModel) => `${e.code}`,
      },
      {
        columnDef: 'name',
        header: 'name',
        title: (e: PalletModel) => `${e.name}`,
        cell: (e: PalletModel) => `${e.name}`,
      }
    );
    this.buttons.push({
      columnDef: 'edit',
      color: 'warn',
      icon: 'edit',
      click: 'addOrEdit',
      title: 'common.title.edit',
      isShowHeader: true,
      display: (e: StoreModel) => e && this.authoritiesService.hasAuthority('patch/pallets/{id}'),
      disabled: (e: any) => !e,
      header: 'common.action' /*{
        columnDef: 'add',
        color: 'warn',
        icon: 'add',
        click: 'addOrEdit',
        title: 'common.title.add',
        display: (e: StoreModel) => !e && this.authoritiesService.hasAuthority('post/pallets'),
        disabled: (e: any) => e
      }*/,
    });
    activatedRoute.params.subscribe(param => {
      this.storeId = param.id ? Number(param.id) : undefined;
    });
  }

  async ngOnInit() {
    this.navService.title = 'pallet.ld.titleDetail';

    const pageStore = await this.tsuService.getFCStores().toPromise() as Page;
    this.storeValues = [];
    this.stepValues = [];
    this.statusValues = [];
    pageStore.content.forEach((store: StoreModel) => {
      this.storeValues.push(new SelectModel(store.id, store.code + ' - ' + store.name));
    });
    if (this.store) {
      this.searchForm.get('store')?.setValue(this.store.id);
    } else if (this.storeValues && this.storeValues.length > 0) {
      this.searchForm.get('store')?.setValue(this.storeId);
    }
    Object.keys(PalletStepEnum).forEach(key => {
      const value = UtilsService.getEnumValue(PalletStepEnum, key.replace('_', ''));
      this.stepValues.push(new SelectModel(key, value));
    });
    Object.keys(PalletStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(PalletStatusEnum, key.replace('_', ''));
      this.statusValues.push(new SelectModel(key, value));
    });

    this.onSubmit();
  }

  search() {
    const step = this.searchForm.get('step')?.value;
    const status = this.searchForm.get('status')?.value;
    const params = new HttpParams()
      .set('storeId', this.searchForm.get('store')?.value)
      .set('distributorId', '')
      .set('text', this.searchForm.get('text')?.value)
      .set('step', `${step}`.replace('_', ''))
      .set('status', `${status}`.replace('_', ''));
    this._fillData('/pallets', params);
  }

  addOrEdit(pallet: PalletModel) {
    if (pallet) {
      this.router.navigate(['store/edit/' + this.storeId + '/app-pallet', ActionTypeEneum.edit, pallet.id]).then();
    } else {
      this.router.navigate(['store/edit/' + this.storeId + '/app-pallet', ActionTypeEneum.new]).then();
    }
  }

  printQR() {
    console.log(this.results.data);
    const dataCheckedTable = this.results.data.filter((pallet: PalletModel | any) => pallet.checked) as PalletModel[];
    if (dataCheckedTable.length === 0) {
      const msg = this.translateService.instant('notice.checkbox.nocheck');
      this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
      this.toastr.warning(msg);
      return;
    }
    const divContents = [];
    let qrBase64: any = '';
    let code: any = '';
    for (const pallet of dataCheckedTable) {
      qrBase64 = pallet.qRCode;
      code = pallet.code;
      const content = this.createContentPrint(qrBase64, code);
      divContents.push(content);
    }
    this.openWindow(divContents);
  }

  createContentPrint(base64: any, code: string) {
    const divContent = ' <div class="palletForm" style="display: flex;place-content: center;align-items: center;\n' +
      '  height: calc(1/3 * 297mm);\n' +
      '  width: 50%;\n' +
      '  font-size: 1.5rem;">\n' +
      '    <div class="qrCode" style="    display: flex;\n' +
      '    flex-direction: column;\n' +
      '    place-content: start;\n' +
      '    align-items: center;\n' +
      '    margin: 0.15in 0.2in 0 0.3in;">\n' +
      '      <div id="img" style="display: flex;\n' +
      '    place-content: center;height: 80%;">\n' +
      '        <img src="data:image/png;base64, ' + base64 + '" style="height: 100%"/>\n' +
      '      </div>\n' +
      '      <div class="code" style="height: 20%;">\n' +
      '        <label>Code: </label>\n' +
      '        <label style="word-break: break-all">' + code + '</label>\n' +
      '      </div>\n' +
      '    </div>\n' +
      '  </div>\n';

    return divContent;
  }

  openWindow(contents: any) {
    const a = window.open('', '');
    if (a) {
      a.document.write('<html><head><style>' +
        'body{display: flex;flex-direction: row; flex-wrap: wrap}' +
        'div{flex-basis: calc(50%);' +
        'margin-bottom: 0.05in}\n' +
        'div:last-child { margin-bottom: 0;}\n@page {\n' +
        '  size: 210mm 297mm;\n' +
        '  margin: 0;\n' +
        '}</style></head>');
      a.document.write('<body style="margin: 0;padding: 0; ">');
      for (const content of contents) {
        a.document.write(content);
      }
      a.document.write('</body></html>');
      a.document.close();
      setTimeout(() => {
        a.print();
        a.close();
      }, 250);
    }
  }

  createPallet() {
    this.router.navigate(['store/edit/' + this.storeId + '/app-pallet', ActionTypeEneum.new], {queryParams: {storeId: this.store?.id}}).then();
  }
}
