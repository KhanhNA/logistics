import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPalletComponent } from './add-edit-pallet.component';

describe('AddEditPalletComponent', () => {
  let component: AddEditPalletComponent;
  let fixture: ComponentFixture<AddEditPalletComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditPalletComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
