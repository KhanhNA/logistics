import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout, Page,
  SelectModel,
  UtilsService
} from "@next-solutions/next-solutions-base";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {Location} from "@angular/common";
import {TranslateService} from "@ngx-translate/core";
import {PartnerService} from "../../partner/partner.service";
import {partnerStatusAction} from "../../../_models/partner/partnerStatus";
import {StoreModel} from "../../../_models/store.model";
import {CommonHttpService} from "../../../_services/http/common/common.http.service";
import {HttpParams} from "@angular/common/http";
import {PalletModel} from "../../../_models/pallet/pallet.model";
import {ActionTypeEneum} from "../../../_models/action-type";
import {StoreService} from "../../store/store.service";

@Component({
  selector: 'app-add-edit-pallet',
  templateUrl: './add-edit-pallet.component.html',
  styleUrls: ['./add-edit-pallet.component.css']
})
export class AddEditPalletComponent extends BaseAddEditLayout implements OnInit, OnDestroy {
  selectedStore: StoreModel | undefined;
  selectedStoreId: number | undefined;
  stepOptions: SelectModel[] = [
    new SelectModel(1, 'pallet.step.one'),
    new SelectModel(2, 'pallet.step.two'),
  ];
  storeOptions: SelectModel[] = [];
  pallet: PalletModel | undefined;

  constructor(protected activatedRoute: ActivatedRoute,
              protected formBuilder: FormBuilder,
              protected location: Location,
              private router: Router,
              protected translateService: TranslateService,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              private storeService: StoreService,
              protected commonHttpService: CommonHttpService,
              protected authoritiesService: AuthoritiesService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    activatedRoute.queryParams.subscribe(query => {
      this.selectedStoreId = Number(query.storeId);
    });
  }

  async ngOnInit() {
    // super.ngOnInit();
    this.id = this.activatedRoute.snapshot.paramMap.get('palletId');
    if (this.id) {
      this.isEdit = true;
    }
    this.addEditForm = this.formBuilder.group({
      id: [''],
      code: [''],
      name: [''],
      step: [1],
      length: [1200],
      width: [120],
      height: [180],
      store: [this.selectedStoreId ? this.selectedStoreId : ''],
      status: [true],
    });

    if (this.isEdit) {
      this.pallet = await this.apiService.get('/pallets/' + this.id, new HttpParams()).toPromise() as PalletModel;
      if (this.pallet) {
        this.selectedStoreId = this.pallet.store?.id ? this.pallet.store?.id : undefined;
      }
      this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, this.pallet));
      this.addEditForm.updateValueAndValidity();
    }

    const store = await this.apiService.get('/stores/' + this.selectedStoreId, new HttpParams()).toPromise() as StoreModel;
    this.storeOptions = [new SelectModel(store.id, store.code + ' - ' + store.name, false, store)];
  }

  ngOnDestroy(): void {
    this.storeService.selectedTabIndex.next(1);
  }

  onBack() {
    this.location.back();
  }

  onSuccessFunc = (data: any, onSuccessMessage?: string): void => {
    this.utilsService.onSuccessFunc(onSuccessMessage);
    if (!this.isEdit) {
      setTimeout(() => {
        this.router.navigate(['store/edit/' + this.addEditForm.value.store + '/app-pallet/edit/'
        + (data[0] ? data[0].id : data.id)]).then();
      }, 500);
    }

  };

  onSave() {
    let method;
    const successMsg = this.isEdit ? '.edit.success' : '.add.success';
    const pallet = new PalletModel(this.addEditForm);
    if (this.isEdit) {
      method = this.apiService.patch('/pallets/' + this.id, pallet);
    } else {
      method = method = this.apiService.post('/pallets', [pallet]);
    }
    this.utilsService.execute(method, this.onSuccessFunc, successMsg, 'common.confirmSave', ['common.pallet.content']);
  }

  updateLabel(event: any) {
    switch (event.label) {
      case 'menu.pallet.edit':
        event.label = this.pallet?.code;
        break;
      case 'menu.store.edit':
        event.label = this.storeOptions[0]?.rawData?.code;
        break;

    }
  }
}
