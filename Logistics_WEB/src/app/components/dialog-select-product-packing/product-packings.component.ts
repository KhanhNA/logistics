import {Component, Inject, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnTypes,
  FormStateService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpParams} from '@angular/common/http';
import {ProductDescriptionsUtils} from '../../_utils/product.descriptions.utils';
import {ProductPackingModel} from '../../_models/product.packing.model';

export class ProductPackingsComponentModel {
  manufacturerId?: number;
  distributorId?: number;
  exceptId?: string;
}

@Component({
  selector: 'app-product-packing',
  templateUrl: './dialog-select-product-packing.component.html',
})
export class ProductPackingsComponent extends BaseSearchLayout implements OnInit {

  moduleName = 'product.packing';
  buttons: ButtonFields[] = [];

  ignoreExpiredDate = false;

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              @Inject(MAT_DIALOG_DATA) public data: ProductPackingsComponentModel,
              public dialogRef: MatDialogRef<ProductPackingModel[]>,) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: ['']
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code',
        header: 'code',
        title: (e: ProductPackingModel) => `${e.code}`,
        cell: (e: ProductPackingModel) => `${e.code}`,
      },
      {
        columnDef: 'product.name',
        header: 'product.name',
        title: (e: ProductPackingModel) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.product)}`,
        cell: (e: ProductPackingModel) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.product)}`,
      },
      {
        columnDef: 'packingType.quantity',
        header: 'packingType.quantity',
        title: (e: ProductPackingModel) => `${e.packingType?.quantity}`,
        cell: (e: ProductPackingModel) => `${e.packingType?.quantity}`,
        align: AlignEnum.RIGHT
      },
      {
        columnDef: 'uom',
        header: 'uom',
        title: (e: ProductPackingModel) => `${e.uom}`,
        cell: (e: ProductPackingModel) => `${e.uom}`,
        align: AlignEnum.CENTER
      },
      // {
      //   columnDef: 'vat',
      //   header: 'vat',
      //   title: (e: ProductPackingModel) => `${e.vat}`,
      //   cell: (e: ProductPackingModel) => `${e.vat}`,
      //   align: AlignEnum.RIGHT
      // },
      {
        columnDef: 'checked',
        header: 'checked',
        title: (e: ProductPackingModel) => `${e.checked}`,
        cell: (e: ProductPackingModel) => `${e.checked}`,
        columnType: ColumnTypes.CHECKBOX,
        align: AlignEnum.CENTER
      },
    );
  }

  async ngOnInit() {
    super.onSubmit();
  }

  search() {
    const params = new HttpParams()
      .set('manufacturerId', '' + this.data.manufacturerId + '')
      .set('distributorId', '' + this.data.distributorId + '')
      .set('exceptId', '' + this.data.exceptId)
      .set('text', this.searchForm.get('text')?.value);
    this._fillData('/product-packings', params);
  }

  onCloseDialog() {
    const onCloseDialogData: ProductPackingModel[] = [];
    this.results.data.forEach((x: ProductPackingModel | any) => {
      if (x.checked) {
        onCloseDialogData.push(x);
      }
    });
    this.dialogRef.close(onCloseDialogData);
  }

  onRowClick(pp: ProductPackingModel) {
    console.log(pp);
  }
}
