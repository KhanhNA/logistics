import {Component, Inject, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnTypes,
  DateUtilService,
  FormStateService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpParams} from '@angular/common/http';
import {ProductDescriptionsUtils} from '../../_utils/product.descriptions.utils';
import {PalletDetailModel} from '../../_models/pallet/pallet.detail.model';

export class ProductPackingIdExpireDateModel {
  productPackingId?: number;
  expireDate?: string;
}

export class PalletsProductPackingGroupByExpireDateComponentModel {
  storeId?: number;
  distributorId?: number;
  repackingPlanningId?: number;
  palletSteps?: number;
  productPackingIdExpireDate?: ProductPackingIdExpireDateModel[];
}

@Component({
  selector: 'app-pallets-product-packing-group-by-expire-date',
  templateUrl: './dialog-select-product-packing.component.html',
})
export class PalletsProductPackingGroupByExpireDateComponent extends BaseSearchLayout implements OnInit {

  moduleName = 'pallets.product.packing.group.by.expire.date';
  buttons: ButtonFields[] = [];

  ignoreExpiredDate = false;

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              @Inject(MAT_DIALOG_DATA) public data: PalletsProductPackingGroupByExpireDateComponentModel,
              public dialogRef: MatDialogRef<PalletDetailModel[]>,
              private dateUtilService: DateUtilService) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: ['']
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'productPacking.code',
        header: 'productPacking.code',
        title: (e: PalletDetailModel) => `${e.productPacking?.code}`,
        cell: (e: PalletDetailModel) => `${e.productPacking?.code}`,
      },
      {
        columnDef: 'productPacking.product.name',
        header: 'productPacking.product.name',
        title: (e: PalletDetailModel) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking?.product)}`,
        cell: (e: PalletDetailModel) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking?.product)}`,
      },
      {
        columnDef: 'productPacking.packingType.quantity',
        header: 'productPacking.packingType.quantity',
        title: (e: PalletDetailModel) => `${e.productPacking?.packingType?.quantity}`,
        cell: (e: PalletDetailModel) => `${e.productPacking?.packingType?.quantity}`,
        align: AlignEnum.RIGHT
      },
      {
        columnDef: 'expireDate',
        header: 'expireDate',
        title: (e: PalletDetailModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.expireDate + '')}`,
        cell: (e: PalletDetailModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.expireDate + '')}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'quantity',
        header: 'quantity',
        title: (e: PalletDetailModel) => `${Number(e.quantity).toLocaleString('en-US') + ''}`,
        cell: (e: PalletDetailModel) => `${Number(e.quantity).toLocaleString('en-US') + ''}`,
        align: AlignEnum.RIGHT
      },
      {
        columnDef: 'checked',
        header: 'checked',
        title: (e: PalletDetailModel) => `${e.checked}`,
        cell: (e: PalletDetailModel) => `${e.checked}`,
        columnType: ColumnTypes.CHECKBOX,
        align: AlignEnum.CENTER
      },
    );
  }

  async ngOnInit() {
    super.onSubmit();
  }

  search() {
    let params = new HttpParams()
      .set('storeId', '' + this.data.storeId?.toString())
      .set('distributorId', '' + this.data.distributorId?.toString())
      .set('palletSteps', '' + this.data.palletSteps?.toString())
      .set('repackingPlanningId', '' + this.data.repackingPlanningId?.toString())
      .set('text', this.searchForm.get('text')?.value);
    for (const item of (this.data.productPackingIdExpireDate ? this.data.productPackingIdExpireDate : [])) {
      params = params.append('' + item.productPackingId?.toString(), '' + item.expireDate?.toString());
    }
    this._fillData('/pallets/product-packing-group-by-expire-date', params);
  }

  onCloseDialog() {
    const onCloseDialogData: PalletDetailModel[] = [];
    this.results.data.forEach((x: PalletDetailModel | any) => {
      if (x.checked) {
        onCloseDialogData.push(x);
      }
    });
    this.dialogRef.close(onCloseDialogData);
  }
}
