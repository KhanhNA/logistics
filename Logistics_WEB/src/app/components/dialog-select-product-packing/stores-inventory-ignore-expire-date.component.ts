import {Component, Inject, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnTypes,
  FormStateService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpParams} from '@angular/common/http';
import {StoreProductPackingModel} from '../../_models/store.product.packing.model';
import {ProductDescriptionsUtils} from '../../_utils/product.descriptions.utils';

export class StoresInventoryIgnoreExpireDateComponentModel {
  storeId?: number;
  exceptProductPackingIds?: string;
  exportStatementId?: number;
}

@Component({
  selector: 'app-stores-inventory-ignore-expire-date',
  templateUrl: './dialog-select-product-packing.component.html',
})
export class StoresInventoryIgnoreExpireDateComponent extends BaseSearchLayout implements OnInit{

  moduleName = 'stores.inventory.ignore.expire.date';
  buttons: ButtonFields[] = [];
  ignoreExpiredDate = true;

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              @Inject(MAT_DIALOG_DATA) public data: StoresInventoryIgnoreExpireDateComponentModel,
              public dialogRef: MatDialogRef<StoreProductPackingModel[]>) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        // text: [''],
        code: [''],
        name: ['']
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'productPacking.code',
        header: 'productPacking.code',
        title: (e: StoreProductPackingModel) => `${e.productPacking?.code}`,
        cell: (e: StoreProductPackingModel) => `${e.productPacking?.code}`,
      },
      {
        columnDef: 'product.name',
        header: 'product.name',
        title: (e: StoreProductPackingModel) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.product)}`,
        cell: (e: StoreProductPackingModel) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.product)}`,
      },
      {
        columnDef: 'productPacking.packingType.quantity',
        header: 'productPacking.packingType.quantity',
        title: (e: StoreProductPackingModel) => `${e.productPacking?.packingType?.quantity}`,
        cell: (e: StoreProductPackingModel) => `${e.productPacking?.packingType?.quantity}`,
        align: AlignEnum.RIGHT
      },
      {
        columnDef: 'totalQuantity',
        header: 'totalQuantity',
        title: (e: StoreProductPackingModel) => `${Number(e.totalQuantity).toLocaleString('en-US') + ''}`,
        cell: (e: StoreProductPackingModel) => `${Number(e.totalQuantity).toLocaleString('en-US') + ''}`,
        align: AlignEnum.RIGHT
      },
      {
        columnDef: 'checked',
        header: 'checked',
        title: (e: StoreProductPackingModel) => `${e.checked}`,
        cell: (e: StoreProductPackingModel) => `${e.checked}`,
        columnType: ColumnTypes.CHECKBOX,
        align: AlignEnum.CENTER
      },
    );
  }

  async ngOnInit() {
    super.onSubmit();
  }

  search() {
    const params = new HttpParams()
      .set('storeId', '' + this.data.storeId?.toString())
      .set('exportStatementId', this.data.exportStatementId + '')
      .set('exceptProductPackingIds', this.data.exceptProductPackingIds + '')
      .set('code', this.searchForm.get('code')?.value)
      .set('name', this.searchForm.get('name')?.value);
    this._fillData('/stores/inventory-ignore-expire-date', params);
  }

  onCloseDialog() {
    const onCloseDialogData: StoreProductPackingModel[] = [];
    this.results.data.forEach((x: StoreProductPackingModel | any) => {
      if (x.checked) {
        onCloseDialogData.push(x);
      }
    });
    this.dialogRef.close(onCloseDialogData);
  }
}
