import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExportStatementMerchantOrderComponent } from './export-statement-merchant-order.component';
import {SharedModule} from '../../modules/shared.module';
import {HttpClient} from '@angular/common/http';
import {AuthoritiesResolverService, MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {RouterModule, Routes} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ExportStatementV2Service} from '../export-statement-v2/export-statement-v2.service';
import {ExportStatementAuthoritiesService} from '../../_services/authority/export.statement.authorities.service';
import {ClaimService} from '../../_services/http/claim/claim.service';
import {AddEditExportStatementV2Component} from '../export-statement-v2/add-edit-export-statement-v2/add-edit-export-statement-v2.component';
import {ClaimAuthoritiesService} from "../../_services/authority/claim.authorities.service";
import {InventoryAuthoritiesService} from "../../_services/authority/inventory.authorities.service";
import {MerchantOrderHttpService} from "../../_services/http/merchant.order.http.service";
import {ShippingPartnerHttpService} from "../../_services/http/shipping.partner.http.service";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/', suffix: '.json'},
    {prefix: './assets/i18n/export-statement-merchant-order/', suffix: '.json'},
    {prefix: './assets/i18n/export-statement/', suffix: '.json'},
  ]);
}

const routes: Routes = [
  {
    path: '',
    component: ExportStatementMerchantOrderComponent,
    pathMatch: 'full',
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'dashboard/:id',
    component: AddEditExportStatementV2Component,
    data: {breadcrumb: 'menu.export.statement.view', isView: true, isSO: true},
    resolve: {me: AuthoritiesResolverService}
  },
];

@NgModule({
  declarations: [
    ExportStatementMerchantOrderComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      isolate: true,
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ExportStatementV2Service,
    ExportStatementAuthoritiesService,
    ClaimService,
    ClaimAuthoritiesService,
    InventoryAuthoritiesService,
    MerchantOrderHttpService,
    ShippingPartnerHttpService
  ]
})
export class ExportStatementMerchantOrderModule { }
