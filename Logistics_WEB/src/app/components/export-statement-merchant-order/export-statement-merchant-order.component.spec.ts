import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportStatementMerchantOrderComponent } from './export-statement-merchant-order.component';

describe('ExportStatementGoodsBonusComponent', () => {
  let component: ExportStatementMerchantOrderComponent;
  let fixture: ComponentFixture<ExportStatementMerchantOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExportStatementMerchantOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportStatementMerchantOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
