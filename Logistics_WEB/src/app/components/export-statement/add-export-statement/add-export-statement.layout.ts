import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {ExportStatementStatus} from '../../../_models/action-type';
import {NumberValidators} from 'src/app/base/field/number-validators';
import {NavService} from '../../../_services/nav.service';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {ConfigEnum} from '../../../_models/enums/config.enum';
import {ExportStatementService} from "../export-statement.service";

// @Directive()
export class AddExportStatementLayout extends LDBaseLayout {
  fromStoresOptions: any[] = [];
  fromStoresMapIds: number[] = [];
  fromStoresList: any[] = [];

  toStoresList: any[] = [];
  toStoresOptions: any[] = [];
  toStoresMapIds: number[] = [];

  totalQuantity = 1;

  merchantOrderId?: number;

  merchantOrderIds: any;

  merchantOrder: any;
  selectedToStore: number;

  maxAmountWarning?: number;
  currencyExportStatement?: string;

  isGoodBonus?: boolean;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService,
              protected router: Router, private navService: NavService,
              protected authoritiesService: AuthoritiesService,
              ) {
    super(tsuService, fb, route, translateService, toastr, location, authoritiesService);

    this.selectedToStore = 0;
    if (!this.router.getCurrentNavigation()?.extras.state) {
      this.getStoreOptions();
      this.navService.title = 'menu.export.statement.create';
    } else {
      if (this.router.getCurrentNavigation()?.extras.state?.merchantOrderId) {
        this.merchantOrderId = this.router.getCurrentNavigation()?.extras.state?.merchantOrderId;
        this.navService.title = 'menu.export.statement.create.from.merchant.order';
        this.getOptionsFromMerchantOrder(this.merchantOrderId);
      } else {
        // fromStore la store chuyen hang cua merchant order, khong phai la kho xuat hang di kho khac
        // console.log(this.router.getCurrentNavigation().extras.state.fromStore);
        if (this.router.getCurrentNavigation()?.extras.state?.fromStore) {
          this.navService.title = 'menu.export.statement.create.from.goods.bonus';
          const fromStoreForMerchantOrder = this.router.getCurrentNavigation()?.extras.state?.fromStore;
          this.merchantOrderIds = this.router.getCurrentNavigation()?.extras.state?.merchantOrderIds;
          this.isGoodBonus = true;
          this.getInforForGoodsBonus(fromStoreForMerchantOrder);
        }
      }
    }
    this.setCols(this.tblCols);
  }

  getConfigExportStatement() {
    this.apiService.get('/configs', new HttpParams().set('code', ConfigEnum.EXPORT_STATEMENT_WARNING))
      .subscribe((configs: any) => {
        if (configs && configs.value) {
          const arr = configs.value.split('|');
          this.currencyExportStatement = arr[0];
          this.maxAmountWarning = arr[1] ? arr[1] : 0;
        }
      });
  }

  getInforForGoodsBonus(fromStoreForMerchantOrder: any) {
    const storeIds = [];
    this.getStoresFromApi(null).then(resolve => {
      resolve.fromStores.then(response => {
        response.content.forEach((store: any) => {
          storeIds.push(store.id);
          if (fromStoreForMerchantOrder.id !== store.id) {
            this.fromStoresOptions.push(store.code + ' - ' + store.name);
            this.fromStoresMapIds.push(store.id);
            this.fromStoresList.push(store);
          }
        });
      }).then(() => {
        resolve.toStores.then((toStores: any) => {
          let hasToStore = false;
          for (const toStore of toStores.content) {
            if (toStore.id === fromStoreForMerchantOrder.id) {
              this.toStoresOptions.push(fromStoreForMerchantOrder.code + ' - ' + fromStoreForMerchantOrder.name);
              this.toStoresMapIds.push(fromStoreForMerchantOrder.id);
              this.toStoresList.push(fromStoreForMerchantOrder);
              this.selectedToStore = this.toStoresMapIds.indexOf(fromStoreForMerchantOrder.id);
              this.formData.toStore = this.selectedToStore.toString();
              hasToStore = true;
              break;
            }
          }
          if (!hasToStore) {
            const msg = this.translateService.instant('export.create.toStore.fail');
            this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
            this.toastr.error(msg);
            this.location.back();
            return;
          }
        });
      }).then(() => {
        this.getConfigExportStatement();
      }).then(() => {
        this.setupStore(fromStoreForMerchantOrder, true);
        this.getMerchantOrderFromGoodsBonus(this.merchantOrderIds);
      });
    });
  }

  async getInventory(packingCodes: any[], dataTable?: any[]) {
    if (this.onDisplayWithAuthority(['get/merchant-orders/get-inventory'])) {
      const params = new HttpParams()
        .set('productPackingCodes', packingCodes.join(', '))
        .set('storeCodes', [this.fromStoresList[this.formData.fromStore].code].join(', '));
      const inventoryData = await this.apiService.get('/merchant-orders/get-inventory', params).toPromise() as any[];
      const inventoryMap = new Map<number, number>();
      inventoryData.forEach(inventory => {
        inventoryMap.set(inventory.productPackingId, inventory.totalQuantity);
      });

      dataTable?.forEach(row => {
        row.totalQuantity = inventoryMap.get(row.productPacking.id) ? inventoryMap.get(row.productPacking.id) : 0;
        row.maxQuantity = row.totalQuantity;
      });

    }
  }

  getMerchantOrderFromGoodsBonus(merchantOrderIds: any) {
    const dataTable: any[] = [];
    let packingCodes: any;
    this.getMerchantOrderListFromApi(merchantOrderIds).then(resolve => {
      const map = new Map();
      for (const merchantOrder of resolve) {
        if (!map.has(merchantOrder.productPacking.id)) {
          map.set(merchantOrder.productPacking.id, merchantOrder);
        } else {
          const sumMerchantOrder = map.get(merchantOrder.productPacking.id);
          sumMerchantOrder.quantity += merchantOrder.quantity;
        }
      }

      for (const obj of map.values()) {
        if (obj.status === 0) {
          ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(obj.productPacking.product);
          dataTable.push(obj);
        }
      }

      packingCodes = dataTable.map(row => row.productPacking.code);

    }).then(() => {
      this.getInventory(packingCodes, dataTable).then();
      const data = {content: dataTable};
      this.updateTable(data);
    });
  }

  async getMerchantOrderListFromApi(merchantOrderIds: any) {
    const params = new HttpParams().set('merchantOrderIds', merchantOrderIds.join(','));
    const api: Observable<any> = this.apiService.get('/merchant-orders/details', params);
    return api.toPromise();
  }

  setupStore(fromStore: any, isGoodsBonus?: boolean) {
    if (!isGoodsBonus) {
      this.fromStoresOptions.push(fromStore.code + ' - ' + fromStore.name);
      this.fromStoresMapIds.push(fromStore.id);
      this.fromStoresList.push(fromStore);

      this.inputFields.forEach(field => {
        if (field.name === 'toStore') {
          this.inputFields.splice(this.inputFields.indexOf(field), 1);
        }
      });
    }

    for (let i = 0; i < this.tblCols.length; i++) {
      const col = this.tblCols[i];

      // if (col.name === 'totalQuantity') {
      //   this.tblCols.splice(this.tblCols.indexOf(col), 1);
      //   i--;
      // }

      // if (col.name === 'quantity') {
      //   col.type = 'view';
      // }

      if (col.name === 'add') {
        this.tblCols.splice(this.tblCols.indexOf(col), 1);
        i--;
      }
    }
    this.setFormUserInformation();
    this.init(this.inputFields);
    this.setCols(this.tblCols);
  }

  setFormUserInformation() {
    if (this.fromStoresList[0].user) {
      this.formData.fromStoreUsername = this.fromStoresList[0].user.username;
      this.formData.fromStoreUserTel = this.fromStoresList[0].user.tel;
    }

    if (this.toStoresList[this.formData.toStore].user) {
      this.formData.toStoreUsername = this.toStoresList[this.formData.toStore].user.username;
      this.formData.toStoreUserTel = this.toStoresList[this.formData.toStore].user.tel;
    }
  }

  async getMerchantOrderFromApi(id: number | undefined) {
    const api: Observable<any> = this.apiService.get('/merchant-orders/' + id, new HttpParams());
    return await api.toPromise();
  }

  getOptionsFromMerchantOrder(merchantOrderId: number | undefined) {
    this.getMerchantOrderFromApi(merchantOrderId).then(result => {
      this.merchantOrder = result;
      this.setupStore(result.fromStore, false);
      result.content = result.merchantOrderDetails;
      result.content.forEach((merchantOrder: any) => {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(merchantOrder.productPacking.product);
      });
      this.updateTable(result);
    }).then(() => {
      this.getConfigExportStatement();
    });
  }

  getExceptProductPackingIds() {
    const packingIds = [-1];
    if (this.getTblData()) {
      this.getTblData().forEach(storeProductPacking => {
        packingIds.push(storeProductPacking.productPacking.id);
      });
    }
    return packingIds;
  }

  getExceptIds() {
    const packingIds = this.getExceptProductPackingIds();
    let params = new HttpParams();
    params = params.set('storeId', this.fromStoresMapIds[this.formData.fromStore].toString());
    params = params.set('exceptId', packingIds.toString());
    params = params.set('pageNumber', '1');
    params = params.set('pageSize', '10');
    for (const id of packingIds) {
      params = params.append('exceptProductPackingIds', id + '');
    }
    return params;
  }

  convertToRows(data: any) {
    const rows = [];
    let row;
    if (data) {
      for (const item of Object.keys(data)) {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(data[item].productPacking.product);
        row = {
          quantity: 1,
          totalQuantity: data[item].totalQuantity,
          weight: ExportStatementService.getWeightProduct(data[item], data[item]).toLocaleString('en-US'),
          volume: ExportStatementService.getVolumeProduct(data[item], data[item]).toLocaleString('en-US'),
          maxQuantity: data[item].totalQuantity,
          expireDate: data[item].expireDate,
          id: data[item].id,
          productPacking: data[item].productPacking,
          product: data[item].product
        };
        rows.push(row);
      }
    }
    return rows;
  }

  deleteRow(data: any) {
    this.tsTable.removeRows(data.ind, 1);
  }

  getFromStores(): Observable<any> {
    const params = new HttpParams()
      .set('status', 'true')
      .set('text', '')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('is_DC', '')
      .set('pageNumber', '1')
      .set('pageSize', '' + 1000);
    return this.apiService.get('/stores', params);
  }

  getToStores(exceptId: any): Observable<any> {
    const ids = [];
    ids.push('-1');
    if (exceptId) {
      ids.push('' + exceptId);
    }
    let params = new HttpParams()
      .set('status', 'true')
      .set('text', '')
      .set('ignoreCheckPermission', 'true')
      .set('is_DC', '')
      .set('pageNumber', '1')
      .set('pageSize', '' + 9999);
    console.log(ids);
    ids.forEach(id => {
      params = params.append('exceptIds', id);
    });
    console.log(params);
    return this.apiService.get('/stores', params);
  }

  getStoreOptions() {
    this.getStoresFromApi(null)
      .then(resolve => {
        resolve.fromStores.then(data => {
          console.log(data);
          this.fromStoresList = [...data.content];
          for (const store of data.content) {
            this.fromStoresOptions.push(store.code + ' - ' + store.name);
            this.fromStoresMapIds.push(store.id);
          }
        }).then(() => {
          resolve.toStores.then(data => {
            this.toStoresOptions.length = 0;
            this.toStoresMapIds.length = 0;
            this.toStoresList.length = 0;
            for (const store of data.content) {
              if (store.id !== this.fromStoresMapIds[0]) {
                this.toStoresOptions.push(store.code + ' - ' + store.name);
                this.toStoresMapIds.push(store.id);
                this.toStoresList.push(store);
              }
            }
          });
        }).then(() => {
          this.getConfigExportStatement();
        }).then(() => {
          this.setFormUserInformation();
          this.init(this.inputFields, this.languages);
        });
      })
      // tslint:disable-next-line:no-shadowed-variable
      .catch(error => console.log(error));
  }

  async getStoresFromApi(exceptId: any) {
    const fromStores = this.getFromStores().toPromise();
    const toStores = this.getToStores(exceptId).toPromise();
    await fromStores;
    await toStores;
    return {fromStores, toStores};
  }

  selectedStore = (data: any) => {
    console.table([data.obj]);
    let selectedStoreId;
    if (data.obj.name === 'fromStore' && !this.merchantOrderIds) {
      selectedStoreId = this.fromStoresMapIds[data.index];
      this.formData.fromStoreUsername = this.fromStoresList[data.index].user ? this.fromStoresList[data.index].user.username : '';
      this.formData.fromStoreUserTel = this.fromStoresList[data.index].user ? this.fromStoresList[data.index].user.tel : '';
      this.filterStore('toStore', selectedStoreId);
      this.tsTable.removeRows(0, this.getTblData().length);
    }

    if (this.isGoodBonus) {
      const packingCodes = this.getTblData().map(row => row.productPacking.code);
      this.getInventory(packingCodes, this.getTblData()).then();
    }
  };

  filterStore(changeType: any, exceptId: any) {
    this.getToStores(exceptId).toPromise().then(storeList => {
      console.table(this.formData);
      if (changeType === 'toStore') {
        this.toStoresOptions.length = 0;
        this.toStoresMapIds.length = 0;
        this.toStoresList.length = 0;
        this.toStoresList = [...storeList.content];
        storeList.content.forEach((store: any) => {
          this.toStoresOptions.push(store.code + ' - ' + store.name);
          this.toStoresMapIds.push(store.id);
        });
        // this.inputFields[1].options = this.toStoresOptions;
      }
      this.updateToStoreInformation();
    });
  }

  private updateToStoreInformation() {
    const index = this.formData.toStore;
    this.formData.toStoreUsername = this.toStoresList[index].user ? this.toStoresList[index].user.username : '';
    this.formData.toStoreUserTel = this.toStoresList[index].user ? this.toStoresList[index].user.tel : '';
  }


  onSave() {
    const dataTable = this.getTblData();
    let method;
    let msg = 'export-statement.msg.create';
    const msgParams: any[] = [];
    const rowIndex = [];
    let notCurrency = false;
    let currentAmount = 0;
    let index = 0;
    let notPrice;
    if ((this.maxAmountWarning || this.maxAmountWarning === 0) && this.currencyExportStatement) {
      for (const detail of dataTable) {
        index++;
        detail.product = detail.product ? detail.product : detail.productPacking.product;
        if (detail.product.productPackingPrices && detail.product.productPackingPrices.length > 0) {
          for (const packingPrice of detail.product.productPackingPrices) {
            if (packingPrice.currency.code.toLowerCase() === this.currencyExportStatement.toLowerCase()) {
              currentAmount = Number(currentAmount)
                + Number(detail.quantity) * (Number(packingPrice.price) * Number(detail.productPacking.packingType.quantity));
              notPrice = false;
              break;
            } else {
              notPrice = true;
            }
          }
          if (notPrice) {
            rowIndex.push(index);
            notCurrency = true;
          }

        } else {
          rowIndex.push(index);
          notCurrency = true;
        }
      }

      if (notCurrency) {
        msgParams.push(this.currencyExportStatement);
        this.showErrorMsg('export.statement.not.currency', rowIndex, msgParams);
        return;
      }
      if (currentAmount > this.maxAmountWarning) {
        msg = 'export-statement.msg.create.warning.max.amount';
        msgParams.push(Number(this.maxAmountWarning).toLocaleString('en-US'), this.currencyExportStatement);
      }
    }
    const exportStatementDetails = [];
    for (const row of dataTable) {
      const exportStatementDetail = {
        price: row.price,
        productPackingId: row.productPacking.id,
        productPackingCode: row.productPacking.code,
        quantity: row.quantity,
      };
      exportStatementDetails.push(exportStatementDetail);
    }
    if (this.merchantOrderId) {
      const exportStatement = {
        fromStoreId: this.fromStoresMapIds[this.formData.fromStore],
        toStoreId: null,
        description: this.formData.description,
        merchantCode: this.merchantOrder.merchantCode,
        merchantOrderId: this.merchantOrder.id,
        merchantOrderIds: [] as number[],
        status: ExportStatementStatus.REQUESTED,
        exportStatementDetails,
        shippingPartner: {
          id: 7
        }
      };
      method = this.apiService.post('/export-statements/from-merchant-order', exportStatement);
    } else if (this.merchantOrderIds && this.merchantOrderIds.length > 0) {
      const exportStatement = {
        fromStoreId: this.fromStoresMapIds[this.formData.fromStore],
        toStoreId: this.toStoresMapIds[this.formData.toStore],
        description: this.formData.description,
        merchantCode: null,
        merchantOrderId: null,
        merchantOrderIds: this.merchantOrderIds,
        status: ExportStatementStatus.REQUESTED,
        exportStatementDetails,
        shippingPartner: {
          id: 7
        }
      };
      method = this.apiService.post('/export-statements/by-store-demand', exportStatement);
    } else {
      const exportStatement = {
        fromStoreId: this.fromStoresMapIds[this.formData.fromStore],
        toStoreId: this.toStoresMapIds[this.formData.toStore],
        description: this.formData.description,
        merchantCode: null,
        merchantOrderId: null,
        merchantOrderIds: [] as number[],
        status: ExportStatementStatus.REQUESTED,
        exportStatementDetails,
        shippingPartner: {
          id: 7
        }
      };
      method = this.apiService.post('/export-statements', [exportStatement]);
    }
    this.tsuService.execute6(method, this.saveSuccess1, msg, msgParams);
  }

  saveSuccess1 = (data: any) => {
    console.log(data);
    const msg = this.translateService.instant('export.create.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
    this.router.navigate(['/export-statement/edit/' + (data[0] ? data[0].id : data.id)]).then();
  };

  changeQuantity = (data: any) => {
    console.log(data);
    ExportStatementService.getVolumeProduct(data.obj, data.obj).toLocaleString('en-US');
    ExportStatementService.getWeightProduct(data.obj, data.obj).toLocaleString('en-US');
    this.showMaxQuantityWarning(data, 'common.packing.max.quantity');
    // this.tsTable.dataSrc._updateChangeSubscription();
  };

  changeToStore = (data: any) => {
    this.updateToStoreInformation();
  }

  inputFields: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'select',
      binding: 'fromStore',
      name: 'fromStore',
      label: 'common.fromStore',
      inputType: 'input',
      options: this.fromStoresOptions,
      emit: this.selectedStore,
      require: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'fromStoreUsername',
      name: 'fromStoreUsername',
      value: '',
      label: 'store.user',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'fromStoreUserTel',
      name: 'fromStoreUserTel',
      label: 'store.phone',
      value: '',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'select',
      binding: 'toStore',
      name: 'toStore',
      label: 'common.toStore',
      inputType: 'input',
      options: this.toStoresOptions,
      emit: this.changeToStore,
      require: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'toStoreUsername',
      name: 'toStoreUsername',
      value: '',
      label: 'store.user',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'toStoreUserTel',
      name: 'toStoreUserTel',
      label: 'store.phone',
      value: '',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'description',
      name: 'description',
      label: 'common.description',
      inputType: 'input',
    }),
  ];

  tblCols: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'index',
      label: 'ld.no',
      inputType: 'text',
      name: 'STT',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.code',
      label: 'common.packing.code',
      inputType: 'input',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.product.name',
      label: 'common.product.name',
      inputType: 'input',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.packingType.quantity',
      label: 'productPacking.packingType.quantity',
      inputType: 'input',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'totalQuantity',
      label: 'storeProductPacking.totalQuantity',
      inputType: 'number',
    }),
    new FieldConfigExt({
      type: 'input',
      name: 'quantity',
      label: 'common.quantity',
      inputType: 'number',
      binding: 'quantity',
      require: 'true',
      emit: this.changeQuantity,
      validations: [
        {
          name: 'integerNumber',
          validator: NumberValidators.integerValidation(),
          message: 'validation.integer'
        },
        {
          name: 'minQuantity',
          validator: NumberValidators.minQuantityValidation(0),
          message: 'validation.minQuantity'
        },

      ]
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'weight',
      label: 'export.statement.table.header.weight',
      inputType: 'number',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'volume',
      label: 'export.statement.table.header.volume',
      inputType: 'number',
    }),
    new FieldConfigExt({
      type: 'btnGrp',
      label: 'add',
      inputType: 'text',
      name: 'add',
      require: 'true',
      value: ['delete'],
      authorities: {
        add: ['get/stores/inventory-ignore-expire-date'],
        delete: ['post/export-statements']
      },
      titles: {
        add: 'common.title.add',
        delete: 'common.title.delete'
      }
    })
  ];
}
