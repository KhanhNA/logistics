import {Component, OnInit} from '@angular/core';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {AddExportStatementLayout} from './add-export-statement.layout';
import {ActionTypeEneum} from '../../../_models/action-type';
import {MatDialog} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {NavService} from '../../../_services/nav.service';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {LDDialogSearchComponent} from '../../dialog-search/l-d-dialog-search.component';
import {StoresInventoryIgnoreExpireDateComponent} from '../../dialog-select-product-packing/stores-inventory-ignore-expire-date.component';
import {HttpParams} from "@angular/common/http";
import {ExportStatementService} from "../export-statement.service";

@Component({
  selector: 'app-add-import-statement',
  templateUrl: './add-export-statement.component.html',
  styleUrls: ['./add-export-statement.component.scss', '../../form.layout.scss', '../../table.layout.scss']
})
export class AddExportStatementComponent implements OnInit {

  model: AddExportStatementLayout;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService, private dialog: MatDialog,
              private navService: NavService, protected router: Router,
              protected authoritiesService: AuthoritiesService) {
    this.model = new AddExportStatementLayout(tsuService, fb, route, translateService, toastr, location, apiService, router, navService, authoritiesService);
  }

  ngOnInit() {
    this.model.actionType = ActionTypeEneum.new;
    // load data trong layout

    const selectedToStoreIndex = this.model.selectedToStore.toString();
    this.model.formData.fromStore = '0';
    this.model.formData.toStore = selectedToStoreIndex;
  }

  onAddEvent(event: any) {
    const dialogRef = this.dialog.open(StoresInventoryIgnoreExpireDateComponent, {
      disableClose: false,
      width: '90%',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        storeId: this.model.fromStoresMapIds[this.model.formData.fromStore],
        exceptProductPackingIds: this.model.getExceptProductPackingIds().join(', '),
        exportStatementId: 0
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('tabledataadd:', result);
      this.model.addTableData(result);
    });
    // this.openDialog();
  }

  openDialog = () => {
    // let params = new HttpParams()
    //   .set('text', '')
    //   .set('storeId', this.model.fromStoresMapIds[this.model.formData.fromStore])
    //   .set('pageNumber', '' + 1)
    //   .set('pageSize', '' + 10);
    // const ids = this.model.getExceptIds();
    // for (const id of ids) {
    //   params = params.append('exceptProductPackingIds', id + '');
    // }
    const data = {
      type: ActionTypeEneum.new,
      params: this.model.getExceptIds(),
      ldCols: [{name: 'productPacking.code', label: 'common.packing.code'},
        {name: 'productPacking.product.name', label: 'common.product.name'},
        {name: 'productPacking.packingType.quantity', label: 'productPacking.packingType.quantity'},
        {name: 'totalQuantity', label: 'storeProductPacking.totalQuantity', inputType: 'number'},
      ],
      entity: 'storeProductPacking',
      rowIdName: 'id',
      parentId: this.model.fromStoresMapIds[this.model.formData.fromStore],
      multiSel: true,
      // params,
      searchUrl: '/stores/inventory-ignore-expire-date',
      // method: this.apiService.get('/stores/inventory', params),

    };

    const dialogRef = this.dialog.open(LDDialogSearchComponent, {
      disableClose: false,
      width: '70%',
      maxHeight: '90vh',
      data
    });
    const sub = dialogRef.afterClosed().subscribe(result => {
      console.log('tabledataadd:', result);
      this.model.addTableData(result);
    });
  }

  async getSelectedFromStoreFromApi(selectedId: number) {
    const store = await this.getSelectedFromStore(selectedId).toPromise();
    return store;
  }

  getSelectedFromStore(selectedStoreId: number): Observable<any> {
    return this.apiService.get('/stores/' + selectedStoreId, new HttpParams());
  }

  onCellEvent(event: any) {
    console.log(event);
    if (event.act === ActionTypeEneum.del) {
      this.model.deleteRow(event);
    }
  }

  onSave() {
    this.model.onSave();
  }

  getTotalWeight() {
    if (!this.model.getTblData()) {
      return 0;
    }
    const totalWeight = this.model.getTblData().reduce((total: any, value: any) => {
      return total + ExportStatementService.getWeightProduct(value.productPacking, value);
    }, 0);
    return totalWeight ? (totalWeight / 1000).toLocaleString('en-US') : 0;
  }

  getTotalVolume() {
    if (!this.model.getTblData()) {
      return 0;
    }
    const totalVolume = this.model.getTblData().reduce((total: any, value: any) => {
      return total + ExportStatementService.getVolumeProduct(value.productPacking, value);
    }, 0);

    return totalVolume ? (totalVolume / 1000000).toLocaleString('en-US') : 0;
  }
}
