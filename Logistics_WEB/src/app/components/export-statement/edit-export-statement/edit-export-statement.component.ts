import {Component, OnInit} from '@angular/core';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {NavService} from '../../../_services/nav.service';
import {ActionTypeEneum} from '../../../_models/action-type';
import {EditExportStatementLayout} from './edit-export-statement.layout';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {LDDialogSearchClientComponent} from '../dialog-search-client/l-d-dialog-search-client.component';
import {StoresInventoryIgnoreExpireDateComponent} from '../../dialog-select-product-packing/stores-inventory-ignore-expire-date.component';
import {ExportStatementService} from "../export-statement.service";

@Component({
  selector: 'app-edit-export-statement',
  templateUrl: './edit-export-statement.component.html',
  styleUrls: ['./edit-export-statement.component.scss', '../../form.layout.scss', '../../table.layout.scss']
})
export class EditExportStatementComponent implements OnInit {

  model: EditExportStatementLayout;
  title?: string;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService, private dialog: MatDialog,
              private navService: NavService, protected router: Router,
              protected authoritiesService: AuthoritiesService) {
    this.model = new EditExportStatementLayout(tsuService, fb, route, translateService,
      toastr, location, apiService, router, navService, authoritiesService);
  }

  ngOnInit() {
    if (this.model.isView()) {
      this.title = 'exportStatement.detail';
    } else if (this.model.actionType === ActionTypeEneum.edit) {
      this.title = 'menu.export.statement.update';
    }

  }

  onAddEvent(event: any) {
    const dialogRef = this.dialog.open(StoresInventoryIgnoreExpireDateComponent, {
      disableClose: false,
      width: '90%',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        storeId: this.model.formData.fromStore.id,
        exceptProductPackingIds: this.model.getExceptIds().join(', '),
        exportStatementId: this.model.parentId
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('tabledataadd:', result);
      this.model.addTableData(result);
    });
    // this.openDialog();
  }

  openDialog = () => {

    const data = {
        type: ActionTypeEneum.new,
        exportStatementId: this.model.parentId,
        exceptIds: this.model.getExceptIds(),
        ldCols: [{name: 'productPacking.code', label: 'common.packing.code'},
          {name: 'productPacking.product.name', label: 'common.product.name'},
          {name: 'productPacking.packingType.quantity', label: 'productPacking.packingType.quantity'},
          {name: 'totalQuantity', label: 'storeProductPacking.totalQuantity', inputType: 'number'},
        ],
        entity: 'storeProductPacking',
        rowIdName: 'id',
        parentId: this.model.formData.fromStore.id,
        multiSel: true,
        // params,
        urlSearch: '/stores/inventory-ignore-expire-date',
        // method: this.apiService.get('/stores/inventory', params),

      }
    ;

    const dialogRef = this.dialog.open(LDDialogSearchClientComponent, {
      disableClose: false,
      width: '70%',
      maxHeight: '90vh',
      data
    });
    const sub = dialogRef.afterClosed().subscribe(result => {
      this.model.addTableData(result);
    });
  };

  onCellEvent(event: any) {
    console.log(event);
    if (event.act === ActionTypeEneum.del) {
      this.model.deleteRow(event);
    }
  }

  onUpdate() {
    this.model.onUpdate();
  }

  onShowButtonEvent(event: any) {
    if (this.model.actionType === ActionTypeEneum.view || this.model.actionType === ActionTypeEneum.view_from_claim) {
      event.isShow = false;
      return;
    }
    if (this.model.exportStatement.merchantOrderFromDC &&
      this.model.exportStatement.merchantOrderFromDC.length > 0) {
      event.isShow = false;
      return;
    }
  }

  getTotalWeight() {
    if (!this.model.getTblData()) {
      return 0;
    }
    const totalWeight = this.model.getTblData().reduce((total: any, value: any) => {
      return total + ExportStatementService.getWeightProduct(value.productPacking, value);
    }, 0);
    return totalWeight ? totalWeight : 0;
  }

  getTotalVolume() {
    if (!this.model.getTblData()) {
      return 0;
    }
    const totalVolume = this.model.getTblData().reduce((total: any, value: any) => {
      return total + ExportStatementService.getVolumeProduct(value.productPacking, value);
    }, 0);

    return totalVolume ? totalVolume : 0;
  }
}
