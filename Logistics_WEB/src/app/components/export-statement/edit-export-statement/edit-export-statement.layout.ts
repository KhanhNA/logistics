import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {HttpParams} from '@angular/common/http';
import {NumberValidators} from 'src/app/base/field/number-validators';
import {NavService} from '../../../_services/nav.service';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {AfterViewInit, Directive} from '@angular/core';
import {ActionTypeEneum, ClaimEnum, ExportStatementStatus} from '../../../_models/action-type';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {ClaimDetailModel} from '../../../_models/claim.detail.model';
import {ConfigEnum} from '../../../_models/enums/config.enum';
import {ExportStatementService} from "../export-statement.service";

@Directive()
export class EditExportStatementLayout extends LDBaseLayout implements AfterViewInit {

  totalQuantity = 1;

  exportStatement: any;

  collectDeleteDetailMap = new Map();

  isFcFromStore = false;

  claimType: string | undefined;
  hasClaim = false;
  totalWeight = 0;
  totalVolume = 0;

  addCol = new FieldConfigExt({
    type: 'btnGrp',
    label: 'add',
    inputType: 'text',
    name: 'add',
    require: 'true',
    value: ['delete'],
    authorities: {
      delete: ['post/export-statements/cancel-detail/{id}'],
      add: ['get/stores/inventory-ignore-expire-date']
    },
    titles: {
      add: 'common.title.add',
      delete: 'common.title.delete'
    }
  });

  maxAmountWarning?: number;
  currencyExportStatement?: string;
  qRCodeCol = new FieldConfigExt({
    type: 'view',
    name: 'storeProductPackingDetail.qRCode',
    label: 'qr-code',
    inputType: 'fileImgBase64'
  });

  constructor(protected tsuService: TsUtilsService,
              fb: FormBuilder,
              protected route: ActivatedRoute,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              protected location: Location,
              private apiService: ApiService,
              protected router: Router,
              private navService: NavService,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, translateService, toastr, location, authoritiesService);
    this.apiService.get('/configs', new HttpParams().set('code', ConfigEnum.EXPORT_STATEMENT_WARNING))
      .subscribe((configs: any) => {
        if (configs && configs.value) {
          const arr = configs.value.split('|');
          this.currencyExportStatement = arr[0];
          this.maxAmountWarning = arr[1];
        }
        this.getStoreOptions();
      });

    if (this.isView()) {
      this.quantityCol.type = 'view';
    }
  }

  ngAfterViewInit(): void {
    this.navService.title = 'menu.export.statement.create';
  }

  getExceptIds() {
    const storeIds = [-1];
    if (this.getTblData()) {
      this.getTblData().forEach(storeProductPacking => {
        storeIds.push(storeProductPacking.productPacking.id);
      });
    } else {
      // storeCodes.push(-1);
    }

    console.log('excepid', this.tsTable.dataSrc.data, storeIds);

    return storeIds;
  }

  convertToRows(data: any) {
    const rows = [];
    let row;
    if (data) {
      for (const item of Object.keys(data)) {

        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(data[item].productPacking.product);

        row = {
          quantity: 1,
          totalQuantity: data[item].totalQuantity,
          maxQuantity: data[item].totalQuantity,
          expireDate: data[item].expireDate,
          weight: ExportStatementService.getWeightProduct(data[item], data[item]).toLocaleString('en-US'),
          volume: ExportStatementService.getVolumeProduct(data[item], data[item]).toLocaleString('en-US'),
          id: data[item].id,
          productPacking: {
            id: data[item].productPacking.id,
            status: data[item].productPacking.status,
            code: data[item].productPacking.code,
            name: data[item].productPacking.name,
            packingType: {
              quantity: data[item].productPacking.packingType.quantity,
            },
            product: data[item].productPacking.product
          }
        };
        rows.push(row);
      }
    }
    console.log('converttttttttttt');
    return rows;
  }

  deleteRow(data: any) {
    // if (data.row.exportStatement) {
    //   this.tsuService.showConfirmDialog('common.confirmDel', ['common.export.detail'],
    //     'common.OK', 'common.Cancel').then((result) => {
    //     if (result.value) {
    //       this.apiService.post(`/export-statements/cancel-detail/${data.row.exportStatement.id}`,
    //         this.collectDeleteDetailMap.get(data.row.id))
    //         .subscribe(res => {
    //           this.delSuccess();
    //           this.tsTable.removeRows(data.ind, 1);
    //         });
    //     }
    //   });
    // } else {
    //   this.tsuService.showConfirmDialog('common.confirmDel', ['common.export.detail'],
    //     'common.OK', 'common.Cancel').then((result) => {
    //     if (result.value) {
    //       this.delSuccess();
    //       this.tsTable.removeRows(data.ind, 1);
    //     }
    //   });
    // }
    this.tsTable.removeRows(data.ind, 1);
  }

  delSuccess = (data?: any) => {
    const msg = this.translateService.instant('.del.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);

  };

  updateColsByAction() {
    const colsDisplay = [];
    for (const col of this.tblCols) {
      if (col.name === 'quantity') {
        if (this.actionType === ActionTypeEneum.view && this.hasClaim && !this.isHideClaimView()) {
          col.label = 'common.quantity.real.export';
          col.require = 'false';
          colsDisplay.push(
            new FieldConfigExt({
              type: 'view',
              label: 'common.quantity.org',
              inputType: 'number',
              name: 'orgQuantity',
            }),
            new FieldConfigExt({
              type: 'view',
              label: 'common.quantity.claim',
              inputType: 'number',
              name: 'claimQuantity',
            })
          );
        }
      }
      colsDisplay.push(col);
    }
    this.tblCols = colsDisplay;
  }

  getStoreOptions() {
    this.apiService.get('/export-statements/' + this.parentId, new HttpParams())
      .subscribe((exportStatement: any) => {
        this.exportStatement = exportStatement;
        if (this.exportStatement.fromStore && !this.exportStatement.fromStore.dc) {
          this.isFcFromStore = true;
        }
        if (this.exportStatement.merchantOrderId) {
          this.showWarningMsg('export.cant.update');
          this.location.back();
        }

        this.exportStatement.fromStore.name = this.exportStatement.fromStore.code + ' - ' + this.exportStatement.fromStore.name;
        if (this.exportStatement.toStore) {
          this.exportStatement.toStore.name = this.exportStatement.toStore
            ? this.exportStatement.toStore.code + ' - ' + this.exportStatement.toStore.name : '';
        } else {
          this.exportStatement.toStore = {name: ''};
        }

        this.exportStatement.fromStoreUsername = this.exportStatement.fromStore.user ? this.exportStatement.fromStore.user.username : '';
        this.exportStatement.fromStoreUserTel = this.exportStatement.fromStore.user ? this.exportStatement.fromStore.user.tel : '';
        this.exportStatement.toStoreUsername = this.exportStatement.toStore.user ? this.exportStatement.toStore.user.username : '';
        this.exportStatement.toStoreUserTel = this.exportStatement.toStore.user ? this.exportStatement.toStore.user.tel : '';

        if (this.exportStatement.merchantOrderFromDC ? this.exportStatement.merchantOrderFromDC.length > 0 : false) {
          this.quantityCol.type = 'view';
          this.inputFields[this.inputFields.length - 1].readonly = 'true';
        }

        const claimMap = new Map<string, any>();
        if (this.isView() && !this.isHideClaimView()) {
          if (this.onDisplayWithAuthority(['get/claims/by-reference'])) {
            const params = new HttpParams()
              .set('type', ClaimEnum.EXPORT_STATEMENT)
              .set('referenceId', '' + this.parentId);
            let key = '';
            this.apiService.get('/claims/by-reference', params)
              .subscribe((claimDetails: ClaimDetailModel[] | any) => {
                  if (claimDetails.length > 0) {
                    this.hasClaim = true;
                    claimDetails.forEach((claimDetail: any) => {
                      if (this.isFcFromStore) {
                        key = claimDetail.palletDetail.pallet.id + ' ' + claimDetail.expireDate + claimDetail.productPacking.id;
                      } else {
                        key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
                      }
                      if (claimMap.has(key)) {
                        const detail = claimMap.get(key);
                        detail.orgQuantity = Number(detail.orgQuantity) + claimDetail.quantity;
                        detail.claimQuatity = detail.orgQuantity;
                        detail.quantity = 0;
                      } else {
                        claimDetail.orgQuantity = claimDetail.quantity;
                        claimDetail.claimQuantity = claimDetail.quantity;
                        claimDetail.quantity = 0;
                        claimMap.set(key, claimDetail);
                      }
                    });
                  }

                  this.initData(claimMap);
                },
                error => {
                  // this.showErrorMsg(error);
                  this.initData(claimMap);
                });
          } else {
            this.initData(claimMap);
          }
        } else {
          this.initData(claimMap);
        }

      });
  }

  initData(claimMap: Map<string, any>) {
    this.updateColsByAction();
    // this.setCols(this.tblCols);
    if (this.isView()) {
      this.tblCols.splice(this.tblCols.indexOf(this.totalQuantityCol), 1);
      this.tblCols.splice(this.tblCols.indexOf(this.addCol), 1);
      if (this.isFcFromStore) {
        this.tblCols.push(new FieldConfigExt({
          type: 'view',
          name: 'palletDetail.pallet.displayName',
          inputType: 'text',
          label: 'common.pallet'
        }));
      }
      this.tblCols.push(this.expireDateCol);
    }

    if (!this.isView()) {
      this.tblCols.splice(this.tblCols.indexOf(this.qRCodeCol), 1);
    }
    // }
    const dataTable = this.collectProductPacking(claimMap, this.exportStatement.exportStatementDetails);
    this.init(this.inputFields);
    this.setCols(this.tblCols);
    this.updateTable(dataTable);
    this.formData = this.exportStatement;
  }

  collectProductPacking(claimMap: Map<string, any>, exportStatementDetails: any) {
    const uniqueDetailMap = new Map();
    const packingCodes: any[] = [];
    let key = '';
    exportStatementDetails.forEach((detail: any) => {

      detail.product = detail.productPacking.product;

      if (this.isView()) {
        if (this.isFcFromStore) {
          key = detail.palletDetail.pallet.id + ' ' + detail.expireDate + detail.productPacking.id;
        } else {
          key = '' + detail.expireDate + detail.productPacking.id;
        }
      } else {
        key = detail.productPacking.id;
      }
      if (uniqueDetailMap.has(key)) {
        const uniqueDetail = uniqueDetailMap.get(key);
        uniqueDetail.quantity = Number(uniqueDetail.quantity) + Number(detail.quantity);
        this.collectDeleteDetailMap.get(uniqueDetail.id).push(detail.id);
      } else {
        detail.orgQuantity = detail.quantity;
        detail.claimQuantity = 0;
        uniqueDetailMap.set(key, detail);
        this.collectDeleteDetailMap.set(detail.id, [detail.id]);
        packingCodes.push(detail.productPacking.code);
      }
    });
    if (packingCodes.length > 0 && !this.isView()
      && this.onDisplayWithAuthority(['get/merchant-orders/get-inventory'])) {
      const params = new HttpParams()
        .set('productPackingCodes', packingCodes.join(', '))
        .set('storeCodes', [this.exportStatement.fromStore.code].join(', '));
      this.apiService.get('/merchant-orders/get-inventory', params)
        .subscribe((data: any) => {
          for (const inventory of data) {
            const detail = uniqueDetailMap.get(inventory.productPackingId);
            detail.totalQuantity = Number(detail.quantity) + Number(inventory.totalQuantity);
            detail.maxQuantity = detail.totalQuantity;
          }
        });
    }
    const dataTable = [...claimMap.values()];
    dataTable.push(...uniqueDetailMap.values());
    return dataTable;
  }

  isView() {
    return this.actionType === ActionTypeEneum.view;
  }

  isHideClaimView() {
    return this.exportStatement.status === ExportStatementStatus.CANCELED
      || this.exportStatement.status === ExportStatementStatus.REQUESTED;
  }

  onUpdate() {
    let method;
    let msg = 'export-statement.msg.update';
    const exportStatementDetails = [];
    const dataTable = this.getTblData();
    const msgParams: any[] = [];
    const rowIndex = [];
    let notCurrency = false;
    let currentAmount = 0;
    let index = 0;
    let notPrice;
    for (const detail of dataTable) {
      index++;
      if (detail.productPacking.product.productPackingPrices && detail.productPacking.product.productPackingPrices.length > 0) {
        for (const packingPrice of detail.productPacking.product.productPackingPrices) {
          if (packingPrice.currency.code.toLowerCase() === this.currencyExportStatement?.toLowerCase()) {
            currentAmount = Number(currentAmount)
              + Number(detail.quantity) * (Number(packingPrice.price) * Number(detail.productPacking.packingType.quantity));
            notPrice = false;
            break;
          } else {
            notPrice = true;
          }
        }

        if (notPrice) {
          rowIndex.push(index);
          notCurrency = true;
        }
      } else {
        rowIndex.push(index);
        notCurrency = true;
      }
    }

    if (notCurrency) {
      msgParams.push(this.currencyExportStatement);
      this.showErrorMsg('export.statement.not.currency', rowIndex, msgParams);
      return;
    }
    if (this.maxAmountWarning && currentAmount > this.maxAmountWarning) {
      msg = 'export-statement.msg.update.warning.max.amount';
      msgParams.push(this.maxAmountWarning, this.currencyExportStatement);
    }

    for (const row of this.getTblData()) {
      const exportStatementDetail = {
        price: row.price,
        productPackingId: row.productPacking.id,
        productPackingCode: row.productPacking.code,
        quantity: row.quantity,
      };
      exportStatementDetails.push(exportStatementDetail);
    }
    const exportStatement = {
      id: this.formData.id,
      code: this.formData.code,
      fromStoreId: this.formData.fromStore.id,
      fromStoreCode: this.formData.fromStore.code,

      toStoreId: this.formData.toStore.id,
      toStoreCode: this.formData.toStore.code,
      description: this.formData.description,
      merchantCode: this.formData.merchantCode,
      merchantOrderId: null,
      merchantOrderIds: this.formData.merchantOrderIds,
      status: this.formData.status,
      exportStatementDetails,
      shippingPartner: {
        id: 7
      }
    };
    method = this.apiService.patch('/export-statements/' + this.exportStatement.id, exportStatement);
    this.tsuService.execute6(method, this.saveSuccess1, msg, msgParams);

  }

  saveSuccess1 = (data: any) => {
    const msg = this.translateService.instant('export.update.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);
    this.router.navigate(['/export-statement']).then();
  };

  changeQuantity = (row: any) => {
    ExportStatementService.getVolumeProduct(row.obj, row.obj).toLocaleString('en-US');
    ExportStatementService.getWeightProduct(row.obj, row.obj).toLocaleString('en-US');
    this.showMaxQuantityWarning(row, 'common.packing.max.quantity');
  };

  expireDateCol = new FieldConfigExt({
    type: 'view',
    name: 'storeProductPackingDetail.storeProductPacking.expireDate',
    label: 'common.expireDate',
    inputType: 'date'
  });

  quantityCol = new FieldConfigExt({
    type: 'input',
    name: 'quantity',
    label: 'common.quantity',
    inputType: 'number',
    binding: 'quantity',
    require: this.actionType === ActionTypeEneum.view ? 'false' : 'true',
    emit: this.changeQuantity,
    validations: [
      {
        name: 'integerNumber',
        validator: NumberValidators.integerValidation(),
        message: 'validation.integer'
      },
      {
        name: 'minQuantity',
        validator: NumberValidators.minQuantityValidation(0),
        message: 'validation.minQuantity'
      },

    ]
  });
  totalQuantityCol = new FieldConfigExt({
    type: 'view',
    name: 'totalQuantity',
    label: 'storeProductPacking.totalQuantity',
    inputType: 'number',
  });

  inputFields: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'input',
      binding: 'code',
      name: 'code',
      label: 'common.code',
      inputType: 'input',
      require: 'true',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'description',
      name: 'description',
      label: 'common.description',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'fromStore.name',
      name: 'fromStore.name',
      label: 'common.fromStore',
      inputType: 'input',
      require: 'true',
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'fromStoreUsername',
      name: 'fromStoreUsername',
      value: '',
      label: 'store.user',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'fromStoreUserTel',
      name: 'fromStoreUserTel',
      label: 'store.phone',
      value: '',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'toStore.name',
      name: 'toStore.name',
      label: 'common.toStore',
      inputType: 'input',
      require: this.actionType === ActionTypeEneum.view ? 'false' : 'true',
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'toStoreUsername',
      name: 'toStoreUsername',
      value: '',
      label: 'store.user',
      inputType: 'input',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      binding: 'toStoreUserTel',
      name: 'toStoreUserTel',
      label: 'store.phone',
      value: '',
      inputType: 'input',
      readonly: 'true'
    }),
  ];


  tblCols: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'index',
      label: 'ld.no',
      inputType: 'text',
      name: 'STT',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.code',
      label: 'common.packing.code',
      inputType: 'input',
    }),
    this.qRCodeCol,
    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.product.name',
      label: 'common.product.name',
      inputType: 'input',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'productPacking.packingType.quantity',
      label: 'productPacking.packingType.quantity',
      inputType: 'number',
    }),
    this.totalQuantityCol,
    this.quantityCol,
    new FieldConfigExt({
      type: 'view',
      name: 'weight',
      label: 'export.statement.table.header.weight',
      inputType: 'number',
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'volume',
      label: 'export.statement.table.header.volume',
      inputType: 'number',
    }),
    this.addCol
  ];
}
