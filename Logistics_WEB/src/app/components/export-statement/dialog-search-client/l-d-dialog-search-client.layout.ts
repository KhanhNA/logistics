import {FormBuilder, FormControl} from '@angular/forms';
import {HttpParams} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {Search} from '../../../_models/Search';
import {ColumnTypeConstant} from '../../../_models/Column.type';
import {Directive, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Directive()
export class LDDialogSearchClientLayout extends LDBaseLayout implements OnInit {

  selCol = new FieldConfigExt({
    type: 'checkboxAll',
    label: 'add',
    inputType: 'all',
    name: 'add',
  });


  readonly columnType = new ColumnTypeConstant();
  exceptIds: [];
  parentId: any;


  data: any = [];
  urlSearch: string;
  exportStatementId: string;

  constructor(tsuService: TsUtilsService, private userService: ApiService, fb: FormBuilder, private row: any,
              route: ActivatedRoute, transService: TranslateService, toastr: ToastrService, location: Location,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    this.row.ldCols.push(this.selCol);

    // this.setLanguageForCodeAndName();

    // this.row.ldCols[0] = this.row.ldCols[0].;
    console.log(this.row.ldCols);
    this.urlSearch = row.urlSearch;
    this.parentId = row.parentId;
    this.exceptIds = row.exceptIds;
    this.exportStatementId = row.exportStatementId;

    this.setCols(this.row.ldCols);
    this.tsTable.rowIdName = row.rowIdName;
    this.setSelection(row.multiSel);
    this.form.addControl('text', new FormControl());
    this.tsTable.form = this.form;

    // this.filterExceptRow(row.exceptIds);
  }


  // setLanguageForCodeAndName() {
  //   for (const storeProductPacking of this.row.content) {
  //     const productDescriptions = storeProductPacking.product.productDescriptions;
  //     for (let i = 0; i < productDescriptions.length; i++) {
  //       if (productDescriptions[i].language.code === this.currLang.code) {
  //         this.row.ldCols[0].name = 'product.productDescriptions[' + i + '].code';
  //         this.row.ldCols[1].name = 'product.productDescriptions[' + i + '].name';
  //         break;
  //       }
  //     }
  //
  //   }
  // }

  // filterExceptRow(exceptIds: []) {
  //   const rowsData = this.row.content;
  //   const exceptRows = [];
  //   if (exceptIds.length > 0) {
  //     for (let i = 0; i < rowsData.length; i++) {
  //       for (const exceptId of exceptIds) {
  //         console.log(rowsData[i]);
  //         if (rowsData[i].id === exceptId) {
  //           rowsData.splice(i, 1);
  //           i--;
  //           break;
  //         }
  //       }
  //     }
  //   }
  //
  //   this.data = rowsData;
  // }


  get selectedRows() {
    return this.tsTable.allSelect;
  }

  // updateTableData = (data) => {
  //   data = this.beforeUpdate(data);
  //   this.tsTable.updatePaging(data);
  //   this.afterSearch(data);
  // };
  //
  beforeUpdate = (data: any) => {
    for (const productPacking of data.content) {
      if (productPacking.product) {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(productPacking.product);
      } else {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(productPacking.productPacking.product);
      }

    }
  }

  ngOnInit(): void {

  }

  updateSearchInfo(search: Search) {
    const text = (search.text !== null && search.text !== undefined) ? search.text : '';
    let params = new HttpParams()
      .set('text', text)
      .set('storeId', this.parentId)
      .set('exportStatementId', this.exportStatementId ? this.exportStatementId : '' )
      .set('pageNumber', '' + (search.pageNumber + 1))
      .set('pageSize', '' + search.pageSize);
    const ids = this.exceptIds;
    for (const id of ids) {
      params = params.append('exceptProductPackingIds', id + '');
    }


    search.method = this.userService.get(this.urlSearch, params);
  }

  // onSearchDataOnClient(page?: any) {
  //
  //   console.log('abc', page, this.search);
  //   const search = this.search;
  //   if (page !== undefined) {
  //     search.pageNumber = page.pageIndex;
  //     search.pageSize = page.pageSize;
  //   }
  //
  //   console.log(this.data, this.search);
  //   const filterData = [];
  //   let checkCode = -1;
  //   let checkName = -1;
  //   let checkPackingQuantity = -1;
  //   let checkTotalQuantity = -1;
  //   let checkExpireDate = -1;
  //
  //   if (this.search.text === '' && !page) {
  //     this.updateTableData(this.data);
  //   } else if (this.data !== null) {
  //     for (const storeProductPacking of this.data) {
  //       checkCode = storeProductPacking.productPacking.code.toString().toLowerCase().indexOf(this.search.text.toLowerCase());
  //       checkName = storeProductPacking.productPacking.name.toString().toLowerCase().indexOf(this.search.text.toLowerCase());
  //       checkPackingQuantity = ('' + storeProductPacking.productPacking.packingType.quantity).indexOf(this.search.text);
  //       checkTotalQuantity = ('' + storeProductPacking.totalQuantity).indexOf(this.search.text);
  //       checkExpireDate = storeProductPacking.expireDate.toString().indexOf(this.search.text);
  //
  //       if (checkCode !== -1 || checkName !== -1 ||
  //         checkPackingQuantity !== -1 || checkTotalQuantity !== -1 ||
  //         checkExpireDate !== -1) {
  //         filterData.push(storeProductPacking);
  //       }
  //     }
  //     let data = [];
  //     if (page) {
  //       for (let j = page.pageIndex * page.pageSize; j < (page.pageIndex * page.pageSize + page.pageSize); j++) {
  //         data.push(filterData[j]);
  //       }
  //     } else {
  //       data = filterData;
  //     }
  //     this.updateTableData(data);
  //   }
  //
  // }

}
