import {CookieService} from 'ngx-cookie-service';
import {FormBuilder} from '@angular/forms';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {ToastrService} from 'ngx-toastr';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatPaginatorIntl } from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {MultilanguagePanigator} from '../../../_helpers/multilanguage.paginator';
import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {LDDialogSearchClientLayout} from './l-d-dialog-search-client.layout';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  templateUrl: './l-d-dialog-search-client.html',
  styleUrls: ['./l-d-dialog-search-client.scss'],
  providers: [
    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator}

  ]
})
export class LDDialogSearchClientComponent implements OnInit {
  model: LDDialogSearchClientLayout;

  constructor(private fb: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, toastr: ToastrService,
              private tsuService: TsUtilsService,
              public dialogRef: MatDialogRef<LDDialogSearchClientComponent>,
              @Inject(MAT_DIALOG_DATA) protected row: any,
              private route: ActivatedRoute,
              transService: TranslateService, location: Location,
              protected authoritiesService: AuthoritiesService) {

    // super(formBuilder, Category, cookieService, userService);

    this.model = new LDDialogSearchClientLayout(tsuService, this.userService, fb,
      row, route, transService, toastr, location, authoritiesService);
  }

  onAddEvent(event: any) {
    this.router.navigate([this.router.url, 'new']);
    // this.dialog.open(AECategoryComponent, {disableClose: false, width: '500px', data: event});
  }

  onEditEvent(event: any) {
    console.log('event:', event);
    this.router.navigate([this.router.url, 'edit', event.row.id]);
  }

  ngOnInit(): void {
    this.model.onSearchData();
  }

  onPutPacking() {
    console.log(this.model.selectedRows);
    this.dialogRef.close(this.model.selectedRows);
  }

  onSearchData(pageData?: any) {
    this.model.search.text = this.model.form.controls.text.value;
    const page = pageData ? pageData : {
      pageIndex: this.model.search.pageNumber,
      pageSize: this.model.search.pageSize
    };
    if (this.model.search.text !== this.model.lastSearch) {
      page.pageIndex = 0;
      this.model.tsTable.paginator?.firstPage();
    }
    this.model.onSearchData(page);
  }

  onPage(page: any) {
    this.model.tsTable.selectedRowIndex = 0;
    this.onSearchData(page);
  }
}
