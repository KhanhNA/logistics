import {Injectable} from '@angular/core';
import {ApiService, Page, UtilsService} from "@next-solutions/next-solutions-base";
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";
import {ProductModel} from "../../_models/product.model";
import {ExportStatementDetailModel} from "../../_models/export-statement/export.statement.detail.model";
import {ConfigEnum} from "../../_models/enums/config.enum";
import {TranslateService} from "@ngx-translate/core";
import {ExportStatementModel} from "../../_models/export-statement/export.statement.model";
import {ProductPackingModel} from "../../_models/product.packing.model";

@Injectable({
  providedIn: 'root'
})
export class ExportStatementService {

  constructor(private apiService: ApiService, private translateService: TranslateService,
              private utilsService: UtilsService) {
  }

  static getVolumeProduct(packing: ProductModel | ProductPackingModel | undefined, e: ExportStatementDetailModel) {
    if (!packing || !packing.height || !packing.length || !packing.width) {
      e.volume = 0;
    } else {
      e.volume = packing.width * packing.length * packing.height * (e.quantity ? e.quantity : 1);
    }
    return e.volume;
  }

  static getWeightProduct(packing: ProductModel | ProductPackingModel | undefined, e: ExportStatementDetailModel) {
    if (!packing || !packing.weight) {
      e.weight = 0;
    } else {
      e.weight = packing.weight * (e.quantity ? e.quantity : 1);
    }
    return e.weight;
  }

  showErrorMsg = (msgKey: string, rowIndex?: any[], msgParams?: any[]) => {
    let msg = this.translateService.instant(msgKey);
    if (msgParams) {
      msg = this.utilsService.strFormat(msgKey, msgParams);
    }

    if (rowIndex && rowIndex.length > 0) {
      msg = '' + msg + rowIndex.join(', ');
    }
    this.utilsService.showErrorToarst(msg);
  }

  getExportStatement(id: number | undefined): Observable<ExportStatementModel> {
    return this.apiService.get('/export-statements/' + id, new HttpParams());
  }

  getConfigExportStatement(): Observable<any> {
    return this.apiService.get('/configs', new HttpParams().set('code', ConfigEnum.EXPORT_STATEMENT_WARNING));
  }

  createFromStoreObservable(): Observable<Page> {
    const params = new HttpParams()
      .set('status', 'true')
      .set('text', '')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      .set('is_DC', '')
      .set('pageNumber', '1')
      .set('pageSize', '' + 1000);
    return this.apiService.get<Page>('/stores', params);
  }

  createToStoreObservable(exceptId: any): Observable<Page> {
    const ids = [];
    ids.push('-1');
    if (exceptId) {
      ids.push('' + exceptId);
    }
    let params = new HttpParams()
      .set('status', 'true')
      .set('text', '')
      .set('ignoreCheckPermission', 'true')
      .set('is_DC', '')
      .set('pageNumber', '1')
      .set('pageSize', '' + 9999);
    console.log(ids);
    ids.forEach(id => {
      params = params.append('exceptIds', id);
    });
    console.log(params);
    return this.apiService.get<Page>('/stores', params);
  }
}
