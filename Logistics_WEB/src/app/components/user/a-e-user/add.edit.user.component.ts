import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {UserModel} from '../../../_models/user.model';
import {AuthenticationService} from '../../../_services/authentication.service';
import {HttpParams} from '@angular/common/http';
import {DistributorModel} from '../../../_models/distributor.model';
import {TranslateService} from '@ngx-translate/core';
import {StoreModel} from '../../../_models/store.model';
import {
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  Page,
  RoleModel,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {Location} from '@angular/common';
import {NavService} from '../../../_services/nav.service';
import {environment} from '../../../../environments/environment';
import {CommonUtils} from "../../../_utils/common.utils";

@Component({
  selector: 'app-add-edit-user',
  templateUrl: './add.edit.user.component.html',
  styleUrls: ['./add.edit.user.component.scss', '../../form.layout.scss']
})
export class AddEditUserComponent extends BaseAddEditLayout implements OnInit {
  moduleName = 'user.add.edit.';
  roleValues: SelectModel[] = [];
  distributorValues: SelectModel[] = [];
  storeValues: SelectModel[] = [];
  isView = false;
  storeMultiple = true;

  constructor(protected formBuilder: FormBuilder,
              protected apiService: ApiService,
              protected activatedRoute: ActivatedRoute,
              protected location: Location,
              protected translateService: TranslateService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              protected navService: NavService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    activatedRoute.data.subscribe(data => {
      this.isView = !!data.isView;
    });
  }

  get role() {
    return this.addEditForm.get('role')?.value;
  }

  get isExists() {
    return this.addEditForm.get('status')?.value == null ? false : true;
  }

  async ngOnInit() {
    const username = this.activatedRoute.snapshot.paramMap.get('username');
    if (username) {
      this.isEdit = true;
    }
    if (this.isEdit) {
      this.navService.title = 'user.edit.title';
    } else {
      this.navService.title = 'user.add.title';
    }

    this.addEditForm = this.formBuilder.group({
      username: [''],
      firstName: [''],
      lastName: [''],
      tel: [''],
      email: [''],
      status: false,
      role: [''],
      distributorId: [''],
      storeIds: ['']
    });

    const roleParams = new HttpParams().set('clientId', environment.CLIENT_ID).set('pageNumber', '1').set('pageSize', '8888');
    const storeParams = new HttpParams().set('status', 'true').set('text', '').set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'true').set('pageNumber', '1').set('pageSize', '' + 9999);
    Promise.all<DistributorModel[], Page, Page>([
      this.apiService.get<DistributorModel[]>('/distributors/all', new HttpParams()).toPromise<DistributorModel[]>(),
      this.apiService.get<Page>('/role/find', roleParams, environment.BASE_AUTHORIZATION_URL).toPromise<Page>(),
      this.apiService.get<Page>('/stores', storeParams).toPromise<Page>(),
    ]).then(([distributors, pageRole, pageStore]) => {
      this.distributorValues = [];
      this.distributorValues.push(new SelectModel(' ', this.translateService.instant('user.status.all')));
      if (distributors) {
        distributors.forEach((distributor: DistributorModel) => {
          this.distributorValues.push(new SelectModel(distributor.id, distributor.code + ' - ' + distributor.name));
          if (CommonUtils.isMFCDistributor([distributor.code + ''])) {
            this.addEditForm.patchValue({
              distributorId: distributor.id
            });
          }
        });
      }
      if (pageRole) {
        const listRole = pageRole.content as RoleModel[];
        this.roleValues = [];
        listRole.forEach(role => {
          this.roleValues.push(new SelectModel(role.id, role.roleName + '', false, role));
        });
      }
      if (pageStore) {
        const listStore = pageStore.content as StoreModel[];
        this.storeValues = [];
        listStore.forEach(store => {
            this.storeValues.push(new SelectModel(store.id, store.code + ' - ' + store.name));
          }
        );
      }
    }).then(() => {
      if (this.isEdit) {
        this.apiService.get('/users/' + encodeURIComponent(username ? username : ''), new HttpParams())
          .subscribe((user: UserModel | any) => {
            if (!user.distributorId) {
              this.addEditForm.get('distributorId')?.setValue(' ');
            }
            this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, user));
            // this.addEditForm.patchValue({
            //   storeIds: this.storeMultiple ? [user.storeIds[0]] : user.storeIds[0]
            // });
          });
      }
    });


  }

  onSubmit(): void {
    const apiCall = this.isEdit
      ? this.apiService.patch('/users', new UserModel(this.addEditForm))
      : this.apiService.post('/users', new UserModel(this.addEditForm));
    const action = this.isEdit ? 'edit' : 'add';
    this.utilsService.execute(apiCall, this.onSuccessFunc, this.moduleName + action + '.success',
      this.moduleName + action + '.confirm');
  }

  hasAuthority(): boolean {
    if ((this.isEdit && this.authoritiesService.hasAuthority('patch/users'))
      || (!this.isEdit && this.authoritiesService.hasAuthority('post/users'))) {
      return true;
    }
    return false;
  }

  roleChange() {
    // tpdh, cvdh co the gan voi nhieu kho
    // this.storeMultiple = this.addEditForm.value?.role &&
    //   (this.roleValues.find(r => r.value === this.addEditForm.value?.role)?.rawData as RoleModel)?.permissions.findIndex(p => p.url === 'post/export-statements' || p.url === 'post/claims') > -1;
    this.addEditForm.patchValue({
      storeIds: this.storeMultiple ? [] : ''
    });
  }
}
