import {Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {UserStatusEnum} from '../../../_models/enums/UserStatusEnum';
import {UserModel} from '../../../_models/user.model';
import {TranslateService} from '@ngx-translate/core';
import {NavService} from '../../../_services/nav.service';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields,
  FormStateService,
  IconTypeEnum,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {UserAuthoritiesService} from "../../../_services/authority/user.authorities.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserComponent extends BaseSearchLayout implements OnInit {

  moduleName = 'user';
  columns: ColumnFields[];

  buttons: ButtonFields[];

  selectValues: SelectModel[] = [];
  state: any;

  constructor(private snackBar: MatSnackBar,
              protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              public dialog: MatDialog,
              protected translateService: TranslateService,
              private navService: NavService,
              protected uiStateService: FormStateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              public userAuthoritiesService: UserAuthoritiesService,
              protected authoritiesService: AuthoritiesService) {
    super(router, apiService, utilsService, uiStateService, translateService,
      injector, activatedRoute, authoritiesService, formBuilder.group({
        text: [''],
        status: '_',
      }))
    ;
    this.columns = [
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'username', header: 'username', title: (e: UserModel) => `${e.username}`,
        cell: (e: UserModel) => `${e.username}`,
        className: 'mat-column-username'
      },
      {
        columnDef: 'firstName', header: 'firstName', title: (e: UserModel) => `${e.firstName}`,
        cell: (e: UserModel) => `${e.firstName}`,
        className: 'mat-column-firstName'
      },
      {
        columnDef: 'lastName', header: 'lastName', title: (e: UserModel) => `${e.lastName}`,
        cell: (e: UserModel) => `${e.lastName}`,
        className: 'mat-column-lastName'
      },
      {
        columnDef: 'tel', header: 'tel', title: (e: UserModel) => `${e.tel}`,
        cell: (e: UserModel) => `${e.tel}`,
        className: 'mat-column-tel'
      },
      {
        columnDef: 'email', header: 'email', title: (e: UserModel) => `${e.email}`,
        cell: (e: UserModel) => `${e.email}`,
        className: 'mat-column-email'
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: UserModel) => `${this.utilsService.getEnumValueTranslated(UserStatusEnum, e.status === true ? '1' : '0')}`,
        cell: (e: UserModel) => `${this.utilsService.getEnumValueTranslated(UserStatusEnum, e.status === true ? '1' : '0')}`,
        className: 'mat-column-status'
      }
    ];
    this.buttons = [
      {
        columnDef: 'reset-pass',
        color: 'warn',
        icon: 'loop',
        click: 'resetPass',
        title: `common.title.reset.password`,
        display: (e: UserModel) => e && this.userAuthoritiesService.hasResetPasswordUserRole()
      },
      {
        columnDef: 'active',
        color: 'warn',
        icon: 'done_all',
        click: 'active',
        title: `common.title.activate`,
        disabled: (e: UserModel) => UtilsService.getEnumValue(UserStatusEnum, e.status === true ? '1' : '0') !== UserStatusEnum._0,
        display: (e: UserModel) => e && this.userAuthoritiesService.hasActivateUserRole()
      },
      {
        columnDef: 'addEdit',
        color: 'warn',
        icon: 'edit',
        header: 'common.action' /*{
          icon: 'add',
          color: 'warn',
          click: 'addOrEdit',
          title: `common.title.add`,
          display: (e: UserModel) => !e && this.authoritiesService.hasAuthority('post/users')
        }*/,
        title: 'common.title.edit',
        click: 'addOrEdit',
        display: (e: UserModel) => e && this.userAuthoritiesService.hasUpdateUserRole()
      },
      {
        columnDef: 'deactive',
        color: 'warn',
        icon: 'clear',
        click: 'deactive',
        title: 'common.title.deactivate',
        disabled: (e: UserModel) => UtilsService.getEnumValue(UserStatusEnum, e.status === true ? '1' : '0') !== UserStatusEnum._1,
        display: (e: UserModel) => e && this.authoritiesService.hasAuthority('post/users/deactive')
      },
      {
        columnDef: 'view',
        title: 'common.title.dashboard',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'onView',
        className: 'info',
        display: () => this.userAuthoritiesService.hasGetUserRole(),
      }
    ];
    this.state = this.router.getCurrentNavigation()?.extras.state;
  }

  async ngOnInit() {
    this.navService.title = 'menu.users';
    Object.keys(UserStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(UserStatusEnum, key.replace('_', ''));
      this.selectValues.push(new SelectModel(key, value));
    });

    super.ngOnInit();
    super.onSubmit();
  }


  search() {
    this.paging.text = this.searchForm.get('text')?.value;
    const status = this.searchForm.get('status')?.value;
    const params = new HttpParams()
      .set('text', this.paging.text)
      .set('status', `${status}`.replace('_', ''));
    // .set('pageNumber', this.isResetPaging ? '1' : this.paging.pageNumber.toString())
    // .set('pageSize', this.paging.pageSize.toString());
    this._fillData('/users', params);
  }

  onView(user: UserModel, index: number) {
    this.router.navigate([this.router.url, 'view', user.username]);
  }


  addOrEdit(user: UserModel) {
    if (user) {
      this.router.navigate([this.router.url, 'edit', user.username], {
        state: {
          text: this.searchForm.get('text')?.value,
          status: this.searchForm.get('status')?.value
        }
      });
    } else {
      this.router.navigate([this.router.url, 'add'], {
        state: {
          text: this.searchForm.get('text')?.value,
          status: this.searchForm.get('status')?.value
        }
      });
    }
  }

  active(user: UserModel) {
    this.utilsService.execute(this.apiService.post('/users/active', user),
      this.onSuccessFunc, this.moduleName + '.active.success',
      this.moduleName + '.active.confirm');
  }

  deactive(user: UserModel) {
    this.utilsService.execute(this.apiService.post('/users/deactive', user),
      this.onSuccessFunc, this.moduleName + '.deactive.success',
      this.moduleName + '.deactive.confirm');
  }

  resetPass(user: UserModel) {
    console.log(user);
    this.utilsService.execute(this.apiService.post('/users/reset-pass', user),
      this.onSuccessFunc, this.moduleName + '.reset-pass.success',
      this.moduleName + '.reset-pass.confirm');
  }

  createUser() {
    this.router.navigate([this.router.url, 'add'], {
      state: {
        text: this.searchForm.get('text')?.value,
        status: this.searchForm.get('status')?.value
      }
    });
  }

  onResetForm() {
    this.searchForm.reset();
    this.searchForm.patchValue({
      text: '',
      status: '_',
    });
  }
}
