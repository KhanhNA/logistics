import {Component, Injector, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {HttpParams} from '@angular/common/http';
import {MerchantOrderStatusEnum} from '../../_models/enums/MerchantOrderStatusEnum';
import {MerchantOrderModel} from '../../_models/merchant.order.model';
import {NavService} from '../../_services/nav.service';
import {MerchantOrderDetailComponent} from './detail/merchant.order.detail.component';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  DateUtilService,
  FormStateService,
  IconTypeEnum,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-merchant-order',
  templateUrl: './merchant.order.component.html',
})
export class MerchantOrderComponent extends BaseSearchLayout implements OnInit{

  moduleName = 'merchant.order';
  columns: ColumnFields[];
  buttons: ButtonFields[];

  selectValues: SelectModel[] = [];
  state: any;

  constructor(private snackBar: MatSnackBar,
              protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService, public dialog: MatDialog,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private navService: NavService,
              private dateUtilService: DateUtilService,
              protected uiStateService: FormStateService,
              protected injector: Injector) {
    super(router, apiService, utilsService, uiStateService, translateService,
      injector, activatedRoute, authoritiesService, formBuilder.group({
        text: [''],
        status: '_',
        fromDate: [dateUtilService.convertDateToStringGMT0(
          dateUtilService.addDays(new Date(), -7)
        )],
        toDate: [dateUtilService.convertDateToStringGMT0(new Date())]
      }))
    ;

    this.columns = [
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code', header: 'code', title: (e: MerchantOrderModel) => `${e.code}`,
        cell: (e: MerchantOrderModel) => `${e.code}`,
        className: 'mat-column-code'
      },
      {
        columnDef: 'qRCode', header: 'qRCode', title: (e: MerchantOrderModel) => `${e.qRCode}`,
        cell: (e: MerchantOrderModel) => `${e.qRCode}`,
        className: 'mat-column-qRCode',
        columnType: ColumnTypes.BASE64,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'merchantCode', header: 'merchantCode', title: (e: MerchantOrderModel) => `${e.merchantCode}`,
        cell: (e: MerchantOrderModel) => `${e.merchantCode}`,
        className: 'mat-column-merchantCode'
      },
      {
        columnDef: 'merchantName', header: 'merchantName', title: (e: MerchantOrderModel) => `${e.merchantName}`,
        cell: (e: MerchantOrderModel) => `${e.merchantName}`,
        className: 'mat-column-merchantName'
      },
      {
        columnDef: 'orderDate', header: 'orderDate',
        title: (e: MerchantOrderModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.orderDate)}`,
        cell: (e: MerchantOrderModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.orderDate)}`,
        className: 'mat-column-orderDate'
      },
      {
        columnDef: 'orderCode', header: 'orderCode', title: (e: MerchantOrderModel) => `${e.orderCode}`,
        cell: (e: MerchantOrderModel) => `${e.orderCode}`,
        className: 'mat-column-orderCode'
      },
      {
        columnDef: 'fromStore',
        header: 'fromStore',
        title: (e: MerchantOrderModel) => `${e.fromStore.code} - ${e.fromStore.name}`,
        cell: (e: MerchantOrderModel) => `${e.fromStore.code} - ${e.fromStore.name}`,
        className: 'mat-column-fromStore'
      },
      {
        columnDef: 'amount', header: 'amount', title: (e: MerchantOrderModel) => `${e.amount}`,
        cell: (e: MerchantOrderModel) => `${Number(e.amount).toLocaleString()}`,
        className: 'mat-column-amount'
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: MerchantOrderModel) => `${this.utilsService.getEnumValueTranslated(MerchantOrderStatusEnum, e.status + '')}`,
        cell: (e: MerchantOrderModel) => `${this.utilsService.getEnumValueTranslated(MerchantOrderStatusEnum, e.status + '')}`,
        className: 'mat-column-status'
      }
    ];
    this.buttons = [
      {
        columnDef: 'detail',
        color: 'warn',
        header: 'common.action',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'info',
        click: 'detail',
        title: 'common.title.detail',
        display: (e: MerchantOrderModel) => e && this.authoritiesService.hasAuthority('get/merchant-orders/{id}')
      },
    ];
  }

  async ngOnInit() {
    this.navService.title = 'menu.merchant.orders';

    Object.keys(MerchantOrderStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(MerchantOrderStatusEnum, key.replace('_', ''));
      this.selectValues.push(new SelectModel(key, value));
    });

    super.ngOnInit();
    this.onSubmit();
  }

  onAuthority(authority: any) {
    return this.authoritiesService.hasAuthority(authority);
  }

  search() {
    const status = this.searchForm.get('status')?.value;
    const fromDate = this.searchForm.get('fromDate')?.value;
    const toDate = this.searchForm.get('toDate')?.value;
    const params = new HttpParams()
      .set('text', this.searchForm.get('text')?.value)
      .set('status', `${status}`.replace('_', ''))
      .set('fromDate', this.dateUtilService.convertDateToStringCurrentGMT(fromDate) + '')
      .set('toDate', this.dateUtilService.convertDateToStringCurrentGMT(toDate) + '');
    this._fillData('/merchant-orders', params);
  }

  detail(merchantOrder: MerchantOrderModel) {
    this.dialog.open(MerchantOrderDetailComponent, {
      disableClose: false,
      maxWidth: '90%',
      maxHeight: '90%',
      data: {
        value: merchantOrder
      }
    });
  }
}
