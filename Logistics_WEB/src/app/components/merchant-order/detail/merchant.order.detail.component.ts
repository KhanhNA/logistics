import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MerchantOrderModel} from '../../../_models/merchant.order.model';
import {MerchantOrderDetailModel} from '../../../_models/merchant.order.detail.model';
import {MerchantOrderDetailStatusEnum} from '../../../_models/enums/MerchantOrderDetailStatusEnum';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ColumnFields,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-merchant-order-detail',
  templateUrl: './merchant.order.detail.component.html',
  styleUrls: ['./merchant.order.detail.component.scss']
})
export class MerchantOrderDetailComponent extends BaseAddEditLayout implements OnInit {
  moduleName = 'merchant.order.detail';
  columns: ColumnFields[] = [];

  getPosition(e: any, arr: any[]) {
    return arr && arr.length > 0 ? (arr.indexOf(e) + 1).toString() : '';
  }

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected translateService: TranslateService,
              protected authoritiesService: AuthoritiesService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              protected activatedRoute: ActivatedRoute,
              protected location: Location) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.addEditForm = this.formBuilder.group({
      merchantOrderDetails: ''
    });
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${this.getPosition(e, this.addEditForm.get('merchantOrderDetails')?.value)}`,
        cell: (e: any) => `${this.getPosition(e, this.addEditForm.get('merchantOrderDetails')?.value)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'distributorCode',
        header: 'distributorCode',
        title: (e: MerchantOrderDetailModel) => `${e.distributor?.code}`,
        cell: (e: MerchantOrderDetailModel) => `${e.distributor?.code}`,
        className: 'mat-column-distributor-code'
      },
      {
        columnDef: 'distributorName',
        header: 'distributorName',
        title: (e: MerchantOrderDetailModel) => `${e.distributor?.name}`,
        cell: (e: MerchantOrderDetailModel) => `${e.distributor?.name}`,
        className: 'mat-column-distributor-name'
      },
      {
        columnDef: 'productPackingCode',
        header: 'productPackingCode',
        title: (e: MerchantOrderDetailModel) => `${e.productPacking?.code}`,
        cell: (e: MerchantOrderDetailModel) => `${e.productPacking?.code}`,
        className: 'mat-column-product-packing-code'
      },
      {
        columnDef: 'productPackingName',
        header: 'productName',
        title: (e: MerchantOrderDetailModel) =>
          `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking?.product)}`,
        cell: (e: MerchantOrderDetailModel) =>
          `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking?.product)}`,
        className: 'mat-column-product-packing-name'
      },
      {
        columnDef: 'packingType.quantity',
        header: 'packingType',
        title: (e: MerchantOrderDetailModel) => `${e.productPacking?.packingType?.quantity}`,
        cell: (e: MerchantOrderDetailModel) => `${e.productPacking?.packingType?.quantity}`,
        className: 'mat-column-price'
      },
      {
        columnDef: 'price',
        header: 'price',
        title: (e: MerchantOrderDetailModel) => `${e.price}`,
        cell: (e: MerchantOrderDetailModel) => `${Number(e.price).toLocaleString()}`,
        className: 'mat-column-price'
      },
      {
        columnDef: 'quantity',
        header: 'quantity',
        title: (e: MerchantOrderDetailModel) => `${Number(e.quantity).toLocaleString('en-US')}`,
        cell: (e: MerchantOrderDetailModel) => `${Number(e.quantity).toLocaleString('en-US')}`,
        className: 'mat-column-quantity',
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: MerchantOrderModel) => `${this.utilsService.getEnumValueTranslated(MerchantOrderDetailStatusEnum, e.status + '')}`,
        cell: (e: MerchantOrderModel) => `${this.utilsService.getEnumValueTranslated(MerchantOrderDetailStatusEnum, e.status + '')}`,
        className: 'mat-column-status'
      });
  }

  ngOnInit() {
    super.ngOnInit();
    this.search();
  }

  search() {
    this.apiService.get<MerchantOrderModel>('/merchant-orders/' + this.data.value.id, new HttpParams())
      .subscribe((merchantOrder: MerchantOrderModel) => {
        this.addEditForm.get('merchantOrderDetails')?.setValue(merchantOrder.merchantOrderDetails);
      });
  }
}
