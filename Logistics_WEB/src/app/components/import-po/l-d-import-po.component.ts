import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ToastrService} from 'ngx-toastr';
import {DatePipe, Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {NavService} from '../../_services/nav.service';
import {LDImportPoLayout} from './l-d-import-po.layout';
import {ActionTypeConstant, ActionTypeEneum, ImportPoStatus} from '../../_models/action-type';
import {MatDialog} from '@angular/material/dialog';
import {UiStateService} from '../../_services/ui.state.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-import-po',
  templateUrl: './l-d-import-po.component.html',
  styleUrls: ['./l-d-import-po.component.scss', '../table.layout.scss', '../form.layout.scss'],
})
export class LDImportPoComponent implements OnInit, AfterViewInit, OnDestroy {


  model: LDImportPoLayout;
  private unsubscribe$ = new Subject();
  readonly ACTION_TYPE = new ActionTypeConstant();
  formDataMap: any = new Map();
  url = '';
  isFirstTime = true;

  constructor(fb: FormBuilder,
              private router: Router,
              private apiService: ApiService,
              cookieService: CookieService,
              toastr: ToastrService,
              private datePipe: DatePipe,
              tranService: TranslateService,
              private tsuService: TsUtilsService,
              private route: ActivatedRoute,
              location: Location,
              private navService: NavService,
              private dialog: MatDialog,
              private uiStateService: UiStateService,
              protected authoritiesService: AuthoritiesService) {
    this.model = new LDImportPoLayout(tsuService, apiService, fb, this.route, this.datePipe,
      tranService, toastr, location, authoritiesService);
  }

  ngOnInit() {
    this.uiStateService.uiState$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(state => {
        console.log(state);
        if (this.isFirstTime) {
          this.url = this.router.url;
        }
        if (state && this.isFirstTime) {
          this.isFirstTime = false;
          this.formDataMap = state.formDataMap;
          if (state.formDataMap && state.formDataMap.has(this.url)) {
            this.model.formData = state.formDataMap.get(this.url);
            this.model.onSearchData();
          }
        }

      });

  }

  ngOnDestroy(): void {
    if (!this.formDataMap.has(this.url)) {
      this.formDataMap.set(this.url, this.model.formData);
    }
    this.uiStateService.setUiState$({formDataMap: this.formDataMap});
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngAfterViewInit(): void {
    this.navService.title = 'import-po.ld.title';
  }

  onCellEvent(event: any) {
    if (event.act === 'dashboard') {
      this.router.navigate([this.router.url, this.ACTION_TYPE.Pal, event.row.id]).then();
    }
  }

  showButtonEvent(event: any) {
    const row = event.row;
    const act = event.act;
    switch (row.status) {
      case ImportPoStatus.OUT_OF_PALLET:
        event.isShow = false;
        return;
      case ImportPoStatus.NEW:
      case ImportPoStatus.IN_PALLET:
        event.isShow = ['dashboard'].includes(act);
        return;
      default:
        return true;
    }
  }

  onRowClick(event: any) {
    this.router.navigate([this.router.url, ActionTypeEneum.view, event.row.id]).then();
  }

  onSearchData() {
    // this.model.onPage({pageIndex: 0, pageSize: this.model.search.pageSize, previousPageIndex: 1});
    this.model.onSearchData();
  }
}
