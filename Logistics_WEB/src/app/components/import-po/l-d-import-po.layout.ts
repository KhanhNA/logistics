import {LDBaseLayout} from '../../base/l-d-base.layout';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {FieldConfigExt} from '../../base/field/field.interface';
import {HttpParams} from '@angular/common/http';
import {AsyncDataUtils} from '../../_utils/async.data.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

// @Directive()
export class LDImportPoLayout extends LDBaseLayout {
  storesCollection: any[] = [];
  // distributorCollection: any[] = [];
  inputFields: FieldConfigExt[] | undefined;
  cols: FieldConfigExt[] | undefined;
  statusOptions = {0: 'import-po.status.new', 1: 'import-po.status.in_pallet', 2: 'import-po.status.out_of_pallet'};

  constructor(tsuService: TsUtilsService,
              private apiService: ApiService,
              fb: FormBuilder,
              route: ActivatedRoute,
              protected datePipe: DatePipe,
              transService: TranslateService,
              toastr: ToastrService,
              location: Location,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    this.getFormDataBinding();
  }

  getFormDataBinding() {
    this.getAsyncData().then(data => {
      // this.distributorCollection.push({
      //   code: '- ' + this.translateService.instant('common.status.all'),
      //   name: '',
      //   id: ''
      // });
      data.stores.then(response => {
        this.storesCollection.push(...response.content);
      });
      // data.distributors.then(distributors => {
      //   this.distributorCollection.push(...distributors);
      // });
    }).then(() => {
      this.inputFields = [
        new FieldConfigExt({
          type: 'input',
          label: 'common.text',
          inputType: 'text',
          name: 'text',
          binding: 'text'
        }),
        new FieldConfigExt({
          type: 'autocomplete',
          label: 'pallet.ae.store.name',
          inputType: 'text',
          name: 'store',
          // value: '',
          options: ['code', 'name'],
          collections: this.storesCollection,
          binding: 'store',
          require: 'true'
        }),

        // new FieldConfigExt({
        //   type: 'autocomplete',
        //   label: 'po.ae.distributor.code',
        //   inputType: 'text',
        //   name: 'distributor.code',
        //   options: ['code', 'name'],
        //   collections: this.distributorCollection,
        //   binding: 'distributor',
        // }),

        new FieldConfigExt({
          type: 'select',
          name: 'status',
          label: 'common.status',
          binding: 'status',
          options: {0: 'import-po.status.new', 1: 'import-po.status.in_pallet', 2: 'import-po.status.out_of_pallet'},
          inputType: 'input',
        }),
      ];
      this.cols = [
        new FieldConfigExt({
          type: 'index',
          label: 'ld.no',
          inputType: 'text',
          name: 'STT',
          value: '',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'po.ae.manufacturer.code',
          inputType: 'text',
          name: 'manufacturer.code',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'po.ae.manufacturer.name',
          inputType: 'text',
          name: 'manufacturer.name',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'po.ae.code',
          inputType: 'text',
          name: 'code',
        }),
        new FieldConfigExt({
          type: 'view',
          label: 'po.ae.description',
          inputType: 'text',
          name: 'description',

        }),
        new FieldConfigExt({
          type: 'view',
          label: 'common.status',
          inputType: 'select',
          name: 'statusString',
          options: this.statusOptions

        }),
        new FieldConfigExt({
          type: 'view',
          label: 'common.approveDate',
          inputType: 'date',
          name: 'po.approveDate'

        }),
        new FieldConfigExt({
          type: 'view',
          label: 'common.approveUser',
          inputType: 'text',
          name: 'po.approveUserObj.name'

        }),
        new FieldConfigExt({
          type: 'btnGrp',
          label: 'add',
          inputType: 'text',
          name: 'add',
          require: 'true',
          value: ['dashboard'],
          authorities: {
            dashboard: ['post/import-pos/update-pallet/{id}']
          },
          titles: {
            dashboard: 'common.title.pallet'
          }
        })
      ];
      this.setSelection(true);
      this.setCols(this.cols);
      this.init(this.inputFields);
      if (!this.formData.store) {
        this.formData = {
          text: '',
          store: this.storesCollection[0],
          // distributor: this.distributorCollection[0],
          status: '0'
        };
        this.onSearchData();
      }

    });
  }

  async getAsyncData() {
    return AsyncDataUtils.getAsyncDataStoresAndDistributors(this.tsuService, this.tsuService);
  }

  updateSearchInfo(search: any) {
    const params = new HttpParams()
      .set('storeId', this.formData.store ? this.formData.store.id : '')
      // .set('distributorId', this.formData.distributor ? this.formData.distributor.id : '')
      .set('text', this.formData.text ? this.formData.text : '')
      .set('status', this.formData.status ? this.formData.status : 0)
      .set('pageNumber', (search.pageNumber + 1).toString())
      .set('pageSize', search.pageSize.toString());
    search.method = this.apiService.get('/import-pos', params);
  }

  beforeUpdate = (data: any) => {
    const currentLaguage = this.translateService.currentLang ?
      this.translateService.currentLang : this.translateService.defaultLang;
    data.content.forEach((importPo: any) => {
      importPo.statusString = this.translateService.instant(this.statusOptions[importPo.status]);
      importPo.po.approveUserObj.name = importPo.po.approveUserObj.firstName + ' ' + importPo.po.approveUserObj.lastName;
      if (importPo.manufacturer) {
        for (const description of importPo.manufacturer.manufacturerDescriptions) {
          if (description.language.code === currentLaguage) {
            importPo.manufacturer.name = description.name;
            break;
          }
        }
      }
    });
  }
}
