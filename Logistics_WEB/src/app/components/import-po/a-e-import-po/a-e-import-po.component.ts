import {ChangeDetectorRef, Component, OnInit, Renderer2} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material/dialog';
import {CommunicationService} from '../../../communication-service';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, Location} from '@angular/common';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {NavService} from '../../../_services/nav.service';
import {ActionTypeConstant, ActionTypeEneum, PoStatus} from '../../../_models/action-type';
import {AEImportPoLayout} from './a-e-import-po.layout';
import {DividePackingComponent} from '../../divide-packing/divide-packing.component';
import {UiStateService} from '../../../_services/ui.state.service';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-a-e-import-po',
  templateUrl: './a-e-import-po.component.html',
  styleUrls: ['./a-e-import-po.component.scss', '../../form.layout.scss', '../../table.layout.scss']
})
export class AEImportPoComponent implements OnInit {

  model: AEImportPoLayout;
  readonly ACTION_TYPE = new ActionTypeConstant();
  title = 'import-po.ae.title';
  titleDetail = 'import-po.ae.titleDetail';

  constructor(formBuilder: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, private toastr: ToastrService,
              public dialog: MatDialog, private renderer: Renderer2,
              private comm: CommunicationService, private route: ActivatedRoute, private changeDetector: ChangeDetectorRef,
              transService: TranslateService, private datePipe: DatePipe,
              private tsuService: TsUtilsService, location: Location,
              private navService: NavService, private uiState: UiStateService,
              protected authoritiesService: AuthoritiesService) {
    this.model = new AEImportPoLayout(this.tsuService, userService, formBuilder,
      route, datePipe, toastr, transService, location, authoritiesService);

  }

  ngOnInit(): void {
    this.navService.title = '';
    this.model.btnSaveLbl = 'Save';
  }

  isShowTblButton(event: any) {
    if (this.model.claimError) {
      event.isShow = false;
      return;
    }
    if (this.model.actionType === ActionTypeEneum.Pal) {
      if (event.act === 'add') {
        event.isShow = false;
        return;
      }
      const ind = this.model.getTblData().indexOf(event.row);
      if ((this.model.getTblData()[ind - 1] && this.model.getTblData()[ind - 1].id === event.row.id) ||
        (this.model.getTblData()[ind + 1] && this.model.getTblData()[ind + 1].id === event.row.id)) {
        if (event.row.quantity <= 1) {
          event.isShow = ['clear'].includes(event.act);
          return;
        }
        event.isShow = true;
        return;
      } else {
        if (event.row.quantity <= 1) {
          event.isShow = false;
          return;
        }
        event.isShow = ['add_box'].includes(event.act);
        return;
      }
    } else {
      event.isShow = false;
      return;
    }
  }

  isShowButton(btn: string, action: string | undefined) {
    // if (this.model.claimError) {
    //   return false;
    // }
    if (action) {
      switch (action) {
        case this.ACTION_TYPE.new:
          return [ActionTypeEneum.new, ActionTypeEneum.del, 'save'].includes(btn);
        case this.ACTION_TYPE.editPo:
          if (this.model.formData.status !== PoStatus.NEW) {
            return false;
          }
          return ['add', 'delete', 'save', 'approve', 'printPO'].includes(btn);
        case this.ACTION_TYPE.GR:

          return ['GR'].includes(btn) && this.model.formData.status === PoStatus.APPROVED;
        case this.ACTION_TYPE.Pal:
          return ['pallet'].includes(btn);
        default:
          return false;
      }
    }
  }

  onCellEvent(data: any) {
    console.log('cell', data);
    if (data.act === ActionTypeEneum.del) {
      this.model.deleteRow(data);
    }
    if (data.act === 'add_box') {
      const shallowCopy = {...data.row};
      this.dialog.open(DividePackingComponent, {
        disableClose: false,
        data: {
          packing: shallowCopy, type: ActionTypeEneum.Pal, store: this.model.formData.store,
          nameCol: {name: 'productPacking.product.name'}
        }
      }).afterClosed().subscribe(response => {
        if (response) {
          this.model.dividePacking(response.data, data);
        }
      });
    }

    if (data.act === 'clear') {
      this.model.removeDividedPacking(data);
    }
  }

  onUpdateImportPOPallet(event: any) {
    this.model.onUpdateImportPOPallet(event);
  }

  onClaimDetail() {
    if (this.model.claimId) {
      this.router.navigate(['/claim/dashboard/' + this.model.claimId]);
    } else {
      this.model.showErrorMsg('claim.wait.approve.please');
    }
  }
}
