import {LDBaseLayout} from '../../../base/l-d-base.layout';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {FieldConfigExt} from '../../../base/field/field.interface';
import {ActionTypeEneum, ClaimEnum, ImportPoStatus, RepackingStatusEnum} from '../../../_models/action-type';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {NumberValidators} from '../../../base/field/number-validators';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiService, AuthoritiesService, Page} from '@next-solutions/next-solutions-base';
import {ImportPoDetailModel} from "../../../_models/import-po/import.po.detail.model";

// @Directive()
export class AEImportPoLayout extends LDBaseLayout {

  data: any;
  store: any;
  btnSaveLbl?: string;
  manufacturers: any[] = [];
  pallets: any[] = [];

  importPo: any;

  inputFields: FieldConfigExt[] | undefined;
  cols: FieldConfigExt[] | undefined;
  distributors: any[] = [];
  claimError = false;
  hasClaim = false;
  claimId?: number;

  constructor(tsuService: TsUtilsService, private apiService: ApiService, fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              toastr: ToastrService,
              transService: TranslateService, location: Location,
              protected authoritiesService: AuthoritiesService
  ) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    this.setFormData();
  }

  async setFormData(claimDetails?: any) {

    this.getDataFromApi()
      .then(response => {
        response.manufacturers.then((data: any) => {
          this.manufacturers = data.content;
        });

        // response.pallets.then((data: any) => {
        //   this.pallets = data.content;
        // });

        response.distributors.then((data: any) => {
          this.distributors = data;
        });

        response.importPO.then((importPo: any) => {
          this.importPo = importPo;


        });

      }).then(() => {
      this.getPallets().subscribe((data: any) => {
        this.pallets = data.content;
        this.inputFields = [
          new FieldConfigExt({
            type: 'input',
            label: 'po.ae.manufacturer.code',
            inputType: 'text',
            name: 'manufacturer.code',
            options: ['code', 'name'],
            collections: this.manufacturers,
            binding: 'manufacturer.code',
            require: 'true',
            readonly: 'true'
          }),
          new FieldConfigExt({
            type: 'autocomplete',
            label: 'po.ae.distributor.code',
            inputType: 'text',
            name: 'distributor.code',
            value: '',
            options: ['code', 'name'],
            collections: this.distributors,
            binding: 'distributor',
            require: 'true',
            readonly: 'true'
          }),
          new FieldConfigExt({
            type: 'input',
            label: 'common.description',
            inputType: 'text',
            name: 'description',
            require: 'false',
            value: '',
            binding: 'description',
            readonly: 'true'
          })
        ];
        this.cols = [
          new FieldConfigExt({
            type: 'index',
            label: 'ld.no',
            inputType: 'text',
            name: 'STT',
          }),
          new FieldConfigExt({
            type: 'view',
            label: 'common.packing.code',
            inputType: 'text',
            name: 'productPacking.code',
          }),
          new FieldConfigExt({
            type: 'view',
            label: 'common.product.name',
            inputType: 'text',
            name: 'productPacking.product.name',
          }),
          new FieldConfigExt({
            type: 'view',
            label: 'common.packing.uom',
            inputType: 'text',
            name: 'productPacking.uom',
          }),
          new FieldConfigExt({
            type: 'view',
            label: 'common.packing.block',
            inputType: 'number',
            name: 'productPacking.packingType.quantity',
          }),
          new FieldConfigExt({
            type: 'view',
            label: 'common.quantity',
            inputType: 'number',
            name: 'quantity',
            emit: this.changeQuantity,
            validations: [
              {
                name: 'integerNumber',
                validator: NumberValidators.integerValidation(),
                message: 'validation.integer'
              },
              {
                name: 'minQuantity',
                validator: NumberValidators.minQuantityValidation(-1),
                message: 'validation.>=0'
              },

            ]
          }),
          new FieldConfigExt({
            type: 'view',
            label: 'common.price',
            inputType: 'number',
            name: 'productPackingPrice.price',
          }),
          new FieldConfigExt({
            type: 'view',
            label: 'common.total',
            inputType: 'number',
            name: 'total',
          }),
          new FieldConfigExt({
            type: 'view',
            label: 'common.expireDate',
            inputType: 'date',
            name: 'expireDate',
          }),
          new FieldConfigExt({
            type: 'autocomplete',
            label: 'common.pallet',
            inputType: 'text',
            name: 'pallet.code',
            value: '',
            binding: 'pallet',
            options: ['code', 'name'],
            collections: this.pallets,
          }),
          new FieldConfigExt({
            type: 'btnGrp',
            inputType: 'text',
            name: 'add',
            value: ['add_box', 'clear'],
            authorities: {
              add_box: ['post/import-pos/update-pallet/{id}'],
              clear: ['post/import-pos/update-pallet/{id}']
            },
            titles: {
              add_box: 'common.title.add_box',
              clear: 'common.title.clear'
            }
          })
        ];
        this.updateData();
      });

    });
  }

  async getDataFromApi() {
    // const claimDetails = this.getClaimFromApi().toPromise();
    const importPO = this.apiService.get('/import-pos/' + this.parentId, new HttpParams()).toPromise();
    const manufacturers = this.getManufacturers().toPromise();
    const distributors = this.getAllDistributors().toPromise();
    // await claimDetails;
    await importPO;
    await manufacturers;
    await distributors;
    return {importPO, manufacturers, distributors};
  }

  getClaimFromApi(): Observable<any> {
    const params = new HttpParams()
      .set('type', ClaimEnum.IMPORT_PO_UPDATE_PALLET)
      .set('referenceId', '' + this.parentId);
    return this.apiService.get<Page>('/claims/by-reference', params);
  }

  async getClaimDetail(claimMap: Map<string, any>) {
    let key = '';
    let claimDetails;
    if (!this.isViewNewImportPo()) {
      claimDetails = await this.getClaimFromApi().toPromise()
        .catch((error: any) => {
          this.claimError = true;
          this.showErrorMsg(error);
        });
    }
    if (claimDetails && claimDetails[0]) {
      this.hasClaim = true;
      this.claimId = claimDetails[0].claim.id;
      claimDetails.forEach((claimDetail: any) => {
        key = '' + claimDetail.expireDate + claimDetail.productPacking.id;
        claimDetail.orgQuantity = claimDetail.quantity;
        claimDetail.claimQuantity = claimDetail.quantity;
        claimDetail.quantity = 0;
        claimMap.set(key, claimDetail);
      });
    }

    if (this.importPo.status !== ImportPoStatus.OUT_OF_PALLET) {
      this.importPo.importPoDetails.forEach((importPoDetail: any) => {
        key = '' + importPoDetail.expireDate + importPoDetail.productPacking.id;
        if (claimMap.has(key) && !this.isViewNewImportPo()) {
          importPoDetail.orgQuantity = Number(importPoDetail.quantity);
          importPoDetail.claimQuantity = Number(claimMap.get(key).claimQuantity);
          importPoDetail.quantity = importPoDetail.orgQuantity - importPoDetail.claimQuantity;
        } else {
          importPoDetail.orgQuantity = importPoDetail.quantity;
          importPoDetail.claimQuantity = 0;
        }
      });
    }
  }

  async updateData() {
    const claimMap = new Map<string, any>();
    await this.getClaimDetail(claimMap).then();
    this.updateColsByAction();
    this.init(this.inputFields, this.languages);
    this.setCols(this.cols);
    let dataTable = [];
    if (this.actionType === ActionTypeEneum.view && this.cols) {
      for (let ind = this.cols.length - 1; ind >= 0; ind--) {
        if (this.cols[ind].name === 'pallet.code') {
          this.cols[ind].type = 'view';
          this.cols[ind].binding = 'pallet.code';
        }
      }
    }

    const map = new Map();
    let key = '';
    let productPackingId: number;
    let expireDate = '';
    let claimDetail;
    let claimKey;
    if (this.importPo.status === ImportPoStatus.IN_PALLET || this.importPo.status === ImportPoStatus.OUT_OF_PALLET) {
      for (const importPoDetail of this.importPo.importPoDetails) {
        for (const importPoDetailPallet of importPoDetail.importPoDetailPallets) {
          importPoDetailPallet.expireDate = importPoDetail.expireDate;
          importPoDetailPallet.productPackingPrice = importPoDetail.productPackingPrice;
          importPoDetailPallet.productPacking = importPoDetail.productPacking;
          importPoDetailPallet.maxQuantity = importPoDetailPallet.quantity;

          key = '' + importPoDetailPallet.pallet.id + importPoDetailPallet.expireDate + importPoDetailPallet.productPacking.id;
          if (map.has(key)) {
            const detailPallet = map.get(key);
            detailPallet.quantity = Number(detailPallet.quantity) + importPoDetailPallet.quantity;
            detailPallet.maxQuantity = Number(detailPallet.maxQuantity) + importPoDetailPallet.maxQuantity;

            importPoDetailPallet.orgQuantity = importPoDetailPallet.quantity;
            importPoDetailPallet.claimQuantity = 0;
          } else {
            claimKey = '' + importPoDetailPallet.expireDate + importPoDetailPallet.productPacking.id;
            if (claimMap.has(claimKey)) {
              claimDetail = claimMap.get('' + importPoDetailPallet.expireDate + importPoDetailPallet.productPacking.id);
              importPoDetailPallet.orgQuantity = Number(importPoDetailPallet.quantity) + Number(claimDetail.orgQuantity);
              importPoDetailPallet.claimQuantity = Number(claimDetail.orgQuantity);
              claimMap.delete(claimKey);
            } else {
              importPoDetailPallet.orgQuantity = importPoDetailPallet.quantity;
              importPoDetailPallet.claimQuantity = 0;
            }
            map.set(key, importPoDetailPallet);
          }
          ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(importPoDetailPallet.productPacking.product);
        }
      }
      dataTable.push(...claimMap.values());
      dataTable.push(...map.values());
    } else if (this.importPo.status === ImportPoStatus.NEW) {
      this.importPo.importPoDetails.forEach((detail: any) => {
        productPackingId = detail.productPacking.id;
        expireDate = detail.expireDate;
        detail.maxQuantity = detail.quantity;
        key = '' + expireDate + productPackingId;
        if (map.has(key)) {
          const importPODetail = map.get(key);
          importPODetail.quantity = Number(importPODetail.quantity) + detail.quantity;
          importPODetail.maxQuantity = Number(importPODetail.maxQuantity) + Number(detail.maxQuantity);
        } else {
          map.set(key, detail);
        }
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(detail.productPacking.product);
      });
      dataTable = [...map.values()];
    }
    this.importPo.content = dataTable;
    this.formData = this.importPo;

    this.tsTable.updatePaging(this.importPo);

  }

  updateColsByAction() {
    const colsDisplay = [];
    if (this.cols)
      for (const col of this.cols) {
        if (col.name === 'importPoDetailPallets[0].pallet.code') {
          if (this.actionType === ActionTypeEneum.view) {
            col.type = 'view';
          } else if (this.actionType !== ActionTypeEneum.Pal) {
            col.type = 'autocomplete';
          }
        } else if (col.name === 'quantity') {
          if (this.importPo.status === ImportPoStatus.OUT_OF_PALLET ||
            this.actionType === ActionTypeEneum.view) {
            col.type = 'view';
          }

          if ((this.actionType === ActionTypeEneum.view ||
            this.importPo.status !== ImportPoStatus.OUT_OF_PALLET)
            && this.hasClaim) {

            // col.require = 'false';
            if (!this.isViewNewImportPo()) {
              col.label = 'common.quantity.real.import';
              colsDisplay.push(
                new FieldConfigExt({
                  type: 'view',
                  label: 'common.quantity.org',
                  inputType: 'number',
                  name: 'orgQuantity',
                }),
                new FieldConfigExt({
                  type: 'view',
                  label: 'common.quantity.claim',
                  inputType: 'number',
                  name: 'claimQuantity',
                })
              );
            }
          }
        }

        colsDisplay.push(col);
      }
    this.cols = colsDisplay;
  }

  isViewNewImportPo() {
    return this.importPo.status === ImportPoStatus.NEW && this.actionType === ActionTypeEneum.view;
  }

  deleteRow(data: any) {
    this.tsTable.removeRows(data.ind, 1);
  }

  onUpdateImportPOPallet(event: any) {
    console.log(this.getTblData());
    let count = 0;
    const map = new Map();
    let key = '';
    for (const item of this.getTblData()) {
      count++;
      if (!item.pallet || !item.pallet.id) {
        const msg = this.translateService.instant('notice.update.pallet') + count;
        this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
        this.toastr.warning(msg);
        return;
      }
      key = '' + item.productPacking.id + item.expireDate + item.pallet.id;
      if (map.has(key)) {
        const detailPallet = map.get(key);
        detailPallet.quantity = Number(detailPallet.quantity) + Number(item.quantity);
      } else {
        map.set(key,
          {
            importPo: {id: item.importPo.id},
            importPoDetail: {id: item.importPoDetail ? item.importPoDetail.id : item.id},
            pallet: {id: item.pallet.id},
            quantity: item.quantity
          });
      }
    }
    const id = this.formData.id;
    const method = this.apiService.post('/import-pos/update-pallet/' + id, [...map.values()]);
    this.tsuService.execute6(method, this.saveSuccess, 'po.update.pallet', []);


  }

  saveSuccess = (data: any) => {
    const msg = this.translateService.instant(this.entity + '.' + this.actionType + '.success');
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.success(msg);

    this.location.back();
  };

  getAllDistributors = () => {
    return this.tsuService.getDistributors2();
  };

  getManufacturers = (value?: any) => {
    return this.tsuService.getManufacturers(value);
  };

  getPallets = () => {
    return this.tsuService.getPallets(this.importPo.store.id, 1, true);
  };

  dividePacking(dividedPacking: any, originPacking: any) {
    const tblData = this.getTblData();
    const length = tblData.length;
    const dataTable = [];
    for (let i = 0; i < length; i++) {
      dataTable.push(tblData[i]);
      if (i === originPacking.ind) {
        tblData[i].quantity = Number(tblData[i].quantity) - Number(dividedPacking.quantity);
        dividedPacking.maxQuantity = tblData[i].maxQuantity - tblData[i].quantity;
        dividedPacking.orgQuantity = dividedPacking.quantity;
        dividedPacking.claimQuantity = 0;
        tblData[i].maxQuantity = tblData[i].quantity;
        tblData[i].orgQuantity = Number(tblData[i].orgQuantity) - Number(dividedPacking.quantity);
        dataTable.push(dividedPacking);
      }
    }
    console.log(dataTable);
    this.tsTable.removeRows(0, length);
    this.tsTable.addRows(dataTable);
    this.formData.poDetails = dataTable;
  }

  removeDividedPacking(removeObj: {ind: number, row: ImportPoDetailModel}) {
    const beforeObj = this.getTblData()[removeObj.ind - 1];
    const afterObj = this.getTblData()[removeObj.ind + 1];
    if ((removeObj.ind === 0 && afterObj.id !== removeObj.row.id) ||
      (removeObj.ind === this.getTblData().length - 1 && beforeObj.id !== removeObj.row.id) ||
      (beforeObj && beforeObj.id !== removeObj.row.id && afterObj.id !== removeObj.row.id)) {
      this.showWarningMsg('common.packing.remove.not.divide');
      return;
    }
    if (beforeObj && beforeObj.id === removeObj.row.id) {
      beforeObj.quantity = Number(removeObj.row.quantity) + Number(beforeObj.quantity);
      beforeObj.maxQuantity = Number(removeObj.row.maxQuantity) + Number(beforeObj.maxQuantity);
      beforeObj.orgQuantity = Number(beforeObj.orgQuantity) + Number(removeObj.row.orgQuantity);
      beforeObj.claimQuantity = Number(beforeObj.claimQuantity) + Number(removeObj.row.claimQuantity);
      beforeObj.total = Number(beforeObj.quantity) * (100 + Number(beforeObj.productPacking.vat)) *
        Number(beforeObj.productPackingPrice.price) / 100;
    } else if (afterObj.id === removeObj.row.id) {
      afterObj.quantity = Number(removeObj.row.quantity) + Number(afterObj.quantity);
      afterObj.maxQuantity = Number(removeObj.row.maxQuantity) + Number(afterObj.maxQuantity);
      afterObj.orgQuantity = Number(afterObj.orgQuantity) + Number(removeObj.row.orgQuantity);
      afterObj.claimQuantity = Number(afterObj.claimQuantity) + Number(removeObj.row.claimQuantity);
      afterObj.total = Number(afterObj.quantity) * (100 + Number(afterObj.productPacking.vat)) *
        Number(afterObj.productPackingPrice.price) / 100;
    }

    this.tsTable.removeRows(removeObj.ind, 1);
    this.formData.poDetails = this.getTblData();
  }

  changeQuantity = (row: any) => {
    console.log(row);

    this.showMaxQuantityWarning(row, 'common.packing.max.quantity');
    row.obj.total = Number(row.obj.quantity) * (100 + Number(row.obj.productPacking.vat)) *
      Number(row.obj.productPackingPrice.price) / 100;

    this.tsTable.dataSrc._updateChangeSubscription();
  };
}
