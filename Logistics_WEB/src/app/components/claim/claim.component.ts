import {Component, Injector, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {Claim} from '../../_models/claim.model';
import {ClaimStatusEnum} from '../../_models/enums/ClaimStatusEnum';
import {ClaimTypeEnum} from '../../_models/enums/ClaimTypeEnum';
import {ActionTypeEneum} from '../../_models/action-type';
import {StoreModel} from '../../_models/store.model';
import {DateUtils} from '../../base/Utils/date.utils';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields,
  DateUtilService,
  FormStateService, IconTypeEnum, Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {CommonHttpService} from "../../_services/http/common/common.http.service";

@Component({
  selector: 'app-claim',
  templateUrl: './claim.component.html',
  styleUrls: [],
})
export class ClaimComponent extends BaseSearchLayout implements OnInit {

  moduleName = 'claim';

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  statusValues: SelectModel[] = [];
  typeValues: SelectModel[] = [];
  storeValues: SelectModel[] = [];

  state: any;
  stores: any[] = [];
  printId: number | undefined;

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected formStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              protected commonHttpService: CommonHttpService,
              private dateUtilService: DateUtilService) {
    super(router, apiService, utilsService, formStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        amenable: [''],
        description: [''],
        type: ['_'],
        store: [''],
        status: ['_'],
        fromCreateDate: ['' + dateUtilService.convertDateToStringGMT0(DateUtils.getDayBeforeOneMonth())],
        toCreateDate: ['' + dateUtilService.convertDateToStringGMT0(DateUtils.getCurrentDate())],
        code: ['']
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code', header: 'code', title: (e: Claim) => `${e.code}`,
        cell: (e: Claim) => `${e.code}`,
        className: 'mat-column-code'
      },
      {
        columnDef: 'storeId', header: 'storeId', title: (e: Claim) => `${e.store.name}`,
        cell: (e: Claim) => `${e.store.name}`,
        className: 'mat-column-storeId'
      },
      {
        columnDef: 'type', header: 'type', title: (e: Claim) => `${e.type}`,
        cell: (e: Claim) => `${e.type}`,
        className: 'mat-column-type'
      },
      {
        columnDef: 'amenable', header: 'amenable', title: (e: Claim) => `${e.amenable}`,
        cell: (e: Claim) => `${e.amenable}`,
        className: 'mat-column-amenable'
      },
      {
        columnDef: 'description', header: 'description', title: (e: Claim) => `${e.description ? e.description : ''}`,
        cell: (e: Claim) => `${e.description ? e.description : ''}`,
        className: 'mat-column-description'
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: Claim) => `${this.utilsService.getEnumValueTranslated(ClaimStatusEnum, `${e.status}`)}`,
        cell: (e: Claim) => `${this.utilsService.getEnumValueTranslated(ClaimStatusEnum, `${e.status}`)}`,
        className: 'mat-column-status'
      });

    this.buttons.push({
        columnDef: 'print_pdf',
        color: 'warn',
        icon: 'picture_as_pdf',
        click: 'print_pdf',
        title: 'common.title.picture_as_pdf',
        display: (e: any) => e && this.authoritiesService.hasAuthority('post/claims/{id}/print-pdf'),
        disabled: (e: any) => e && e.status !== 1
      },
      {
        columnDef: 'dashboard',
        color: 'warn',
        header: 'common.action',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'info',
        click: 'dashboard',
        title: 'common.title.dashboard',
        display: (e: any) => e && this.authoritiesService.hasAuthority('get/claims/{id}'),
      });
  }

  async ngOnInit() {
    Object.keys(ClaimStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(ClaimStatusEnum, key.replace('_', ''));
      this.statusValues.push(new SelectModel(key, value));
    });

    Object.keys(ClaimTypeEnum).forEach(key => {
      const value = UtilsService.getEnumValue(ClaimTypeEnum, key.replace('_', ''));
      this.typeValues.push(new SelectModel(key, value));
    });

    await this.getDataSelectModel();

    super.ngOnInit();

    this.onSubmit();
  }

  async getDataSelectModel() {
    // const params = new HttpParams()
    //   .set('status', 'true')
    //   .set('text', '')
    //   .set('exceptIds', '-1')
    //   .set('ignoreCheckPermission', 'false')
    //   .set('is_DC', '')
    //   .set('pageNumber', '1')
    //   .set('pageSize', '' + 9999);
    // const response = await this.apiService.get('/stores', params).toPromise() as Page;
    const response = await this.commonHttpService.createGetAllStoreAvailableManagementObservable().toPromise() as Page;
    this.storeValues = [];
    this.stores.push(...response.content);
    const storeIds: number[] = [];
    this.stores.forEach((store: StoreModel) => {
      if (store && store.id) {
        storeIds.push(store.id);
      }
      this.storeValues.push(new SelectModel(store.id, store.code + ' - ' + store.name));
    });
    if (this.storeValues.length > 0) {
      this.searchForm.patchValue({
        store: this.storeValues[0].value
      });
    }
    // const par = new HttpParams().append('storeIds', storeIds.join(', '));
    // this.apiService.get<StoreModel[]>('/stores/by-provide-store', par).subscribe((provideStores: StoreModel[]) => {
    //   this.storeValues = [...this.storeValues];
    //   provideStores.forEach(store => {
    //     this.storeValues.push(new SelectModel(store.id, store.code + ' - ' + store.name));
    //   });
    //   if (this.storeValues.length > 0){
    //     this.searchForm.patchValue({
    //       store: this.storeValues[0].value
    //     });
    //   }
    //
    // });
  }

  search() {
    const status = this.searchForm.get('status')?.value;
    const type = this.searchForm.get('type')?.value;
    const params = new HttpParams()
      .set('code', this.searchForm.get('code')?.value)
      .set('fromCreateDate', this.searchForm.get('fromCreateDate')?.value)
      .set('toCreateDate', this.searchForm.get('toCreateDate')?.value)
      .set('amenable', this.searchForm.get('amenable')?.value)
      .set('description', this.searchForm.get('description')?.value)
      .set('storeId', this.searchForm.get('store')?.value)
      .set('type', `${type}`.replace('_', ''))
      .set('status', `${status}`.replace('_', ''));
    this._fillData('/claims', params);
  }

  dashboard(row: Claim) {
    this.router.navigate([this.router.url, ActionTypeEneum.view, row.id]).then();
  }

  print_pdf(row: Claim) {
    this.printId = row.id;
    this.apiService.saveFile('/claims/' + row.id + '/print-pdf', null, {
      headers: undefined,
      params: undefined
    });
  }

}
