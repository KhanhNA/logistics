import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {TsUtilsService} from '../../../base/Utils/ts-utils.service';
import {NavService} from '../../../_services/nav.service';
import {ClaimStatusEnum} from '../../../_models/enums/ClaimStatusEnum';
import {ClaimTypeEnum} from '../../../_models/enums/ClaimTypeEnum';
import {ClaimDetailModel} from '../../../_models/claim.detail.model';
import {Claim} from '../../../_models/claim.model';
import {ActionTypeEneum, ClaimEnum} from '../../../_models/action-type';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ButtonFields,
  ColumnFields,
  DateUtilService,
  DialogTypeEnum,
  NsCustomDialogDataConfig,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {Location} from '@angular/common';
import {environment} from '../../../../environments/environment';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-claim',
  templateUrl: './claim-info.component.html',
  styleUrls: ['./claim-info.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClaimInfoComponent extends BaseAddEditLayout implements AfterViewInit, OnInit {

  moduleName = 'claim';
  columns: ColumnFields[];
  actType: string | undefined;

  buttons: ButtonFields[] = [];

  statusValues: SelectModel[] = [];
  typeValues: SelectModel[] = [];

  claim: Claim | undefined;

  urlImageDetail = '';
  descriptionImageDetail = '';
  hadClickedViewImage = false;

  isRejectedClaim = false;
  currentLangCode: string;
  // image slide
  slideIndex = 1;

  constructor(private snackBar: MatSnackBar,
              private formBuilder: FormBuilder,
              private router: Router,
              private apiService: ApiService,
              private tsClaimvice: TsUtilsService,
              public dialog: MatDialog,
              protected translateService: TranslateService,
              protected toastr: ToastrService,
              protected authoritiesService: AuthoritiesService,
              private navService: NavService,
              private route: ActivatedRoute,
              private tsuService: TsUtilsService,
              private dateUtilService: DateUtilService,
              activatedRoute: ActivatedRoute,
              location: Location,
              utilsService: UtilsService) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    this.currentLangCode = this.translateService.currentLang ?
      this.translateService.currentLang : this.translateService.defaultLang;
    this.columns = [
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${this.getPosition(e, this.addEditForm.get('claimDetails')?.value)}`,
        cell: (e: any) => `${this.getPosition(e, this.addEditForm.get('claimDetails')?.value)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code', header: 'code',
        title: (e: ClaimDetailModel) => `${e.productPacking?.code}`,
        cell: (e: ClaimDetailModel) => `${e.productPacking?.code}`,
        className: 'mat-column-code'
      },
      {
        columnDef: 'name', header: 'name',
        title: (e: ClaimDetailModel) => `${e ? e.productPacking?.product?.productDescriptions?.filter(description =>
          description.language?.code === this.currentLangCode)[0].name : ''}`,
        cell: (e: ClaimDetailModel) => `${e ? e.productPacking?.product?.productDescriptions?.filter(description =>
          description.language?.code === this.currentLangCode)[0].name : ''}`,
        className: 'mat-column-name'
      },
      {
        columnDef: 'expireDate', header: 'expireDate',
        title: (e: ClaimDetailModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.expireDate ? e.expireDate : '')}`,
        cell: (e: ClaimDetailModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.expireDate ? e.expireDate : '')}`,
        className: 'mat-column-expireDate'
      },
      {
        columnDef: 'quantity', header: 'quantity',
        title: (e: ClaimDetailModel) => `${Number(e.quantity).toLocaleString('en-US')}`,
        cell: (e: ClaimDetailModel) => `${Number(e.quantity).toLocaleString('en-US')}`,
        className: 'mat-column-quantity'
      },
      {
        columnDef: 'price', header: 'price',
        title: (e: ClaimDetailModel) => `${Number(e.productPackingPrice.price).toLocaleString('en-US')}`,
        cell: (e: ClaimDetailModel) => `${Number(e.productPackingPrice.price).toLocaleString('en-US')}`,
        className: 'mat-column-price'
      },
      {
        columnDef: 'palletDetailId',
        header: 'palletDetailId',
        title: (e: ClaimDetailModel) => e && e.palletDetail && e.palletDetail.pallet ? `${e.palletDetail.pallet.displayName}` : '',
        cell: (e: ClaimDetailModel) => e && e.palletDetail && e.palletDetail.pallet ? `${e.palletDetail.pallet.displayName}` : '',
        className: 'mat-column-palletDetailId',
        display: (e: ClaimDetailModel) => {
          return !!(e && e.palletDetail && e.palletDetail.pallet && e.palletDetail.pallet.code);
        }
      }];
    this.addEditForm = this.formBuilder.group({
      amenable: [''],
      description: [''],
      type: [''],
      store: [''],
      status: [''],
      code: [''],
      referenceId: [''],
      rejectReason: [''],
      claimDetails: ['']
    });
  }

  ngAfterViewInit(): void {
    // this.showSlides(this.slideIndex)
  }

  async ngOnInit() {
    this.navService.title = 'menu.claim';

    this.getValueSelectModel();

    this.search().then();
  }


  async getValueSelectModel() {
    Object.keys(ClaimStatusEnum).forEach(key => {
      if (key === '_') {
        return;
      }
      const value = UtilsService.getEnumValue(ClaimStatusEnum, key.replace('_', ''));
      this.statusValues.push(new SelectModel(key.replace('_', ''), value));
    });

    Object.keys(ClaimTypeEnum).forEach(key => {
      if (key === '_') {
        return;
      }
      const value = UtilsService.getEnumValue(ClaimTypeEnum, key.replace('_', ''));
      this.typeValues.push(new SelectModel(key.replace('_', ''), value));
    });
  }

  async search() {
    this.route.params.subscribe(params => {
      this.actType = params.actType;
      this.id = params.claimId;
    });
    this.claim = await this.apiService.get('/claims/' + this.id, new HttpParams()).toPromise() as Claim;
    this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, this.claim));
    this.addEditForm.get('status')?.setValue(`${this.claim.status}`);
    this.addEditForm.get('store')?.setValue(this.claim.store.name);
    this.isRejectedClaim = this.claim.status === 2; // status === rejected
    this.addEditForm.get('claimDetails')?.setValue(this.claim.claimDetails);
  }

  onApprove(): void {
    this.utilsService.execute(this.apiService.patch('/claims/accept/' + this.id, null), this.onSuccessFunc,
      this.moduleName + '.' + 'active' + '.success', this.moduleName + '.' + 'active' + '.confirm', []);
  }

  // onReject(): void {
  //   const ;
  //   const textMsg = 'reject.reason';
  //   const requireMessage = 'reject.reason.required';
  //   this.tsuService.showInputConfirmDialog(msgConfirm, textMsg, requireMessage, this.rejectFunction,
  //     this.onSuccessFunc, this.moduleName + '.' + 'deactive' + '.success', this.showError);
  //
  // }

  onReject() {
    const strOk = this.translateService.instant('claim.deactive.confirm');
    const strCancel = this.translateService.instant('common.Cancel');
    const dialogConfig: MatDialogConfig<any> = {
      data: {
        customClass: 'reject_reason_dialog',
        msg: this.translateService.instant(this.moduleName + '.' + 'deactive' + '.confirm'),
        msgDetail: '',
        type: DialogTypeEnum.INPUT_CONFIRM,
        btnOKString: strOk,
        btnCancelString: strCancel,
        hideIconButton: true
      } as NsCustomDialogDataConfig
    };
    this.utilsService.showConfirmInputDialog('common.reject.reason', [], dialogConfig)
      .afterClosed().subscribe(result => {
      if (result && result.value) {
        this.apiService.patch('/claims/reject/' + this.id, {rejectReason: result.value})
          .subscribe(data => {
              this.onSuccessFunc(data, this.moduleName + '.' + 'deactive' + '.success');
            },
            error => {
              this.showError(error);
            });
      }
    });
  }


  public showError = (err: string): void => {
    this.utilsService.showError(err);
  };


  visibleBtn(): boolean {
    return this.addEditForm.get('status')?.value !== '0';
  }

  hasAuthorityDisplay(type: string): boolean {
    switch (type) {
      case 'approve':
        return this.authoritiesService.hasAuthority('patch/claims/accept/{id}');
      case 'reject':
        return this.authoritiesService.hasAuthority('patch/claims/reject/{id}');
      case 'get-parent':
        return this.authoritiesService.hasAuthority('get/pos/{id}')
          || this.authoritiesService.hasAuthority('get/import-pos/{id}')
          || this.authoritiesService.hasAuthority('get/repacking-plannings/{id}')
          || this.authoritiesService.hasAuthority('get/export-statements/{id}');
    }
    return false;
  }

  imageClick(url: string, description: string) {
    this.hadClickedViewImage = true;
    this.urlImageDetail = url;
    this.descriptionImageDetail = description;
  }

  cancelView() {
    this.hadClickedViewImage = false;
  }

  onViewParent() {
    let url = '';
    if (this.claim) {
      switch (this.claim.type) {
        case ClaimEnum.IMPORT_PO:
          url = '/po/' + ActionTypeEneum.view + '/' + this.claim.referenceId;
          break;
        case ClaimEnum.IMPORT_PO_UPDATE_PALLET:
          url = '/import-po/' + ActionTypeEneum.view + '/' + this.claim.referenceId;
          break;
        case ClaimEnum.REPACKING_PLANNING:
          url = '/app-repacking/' + ActionTypeEneum.view + '/' + this.claim.referenceId;
          break;
        case ClaimEnum.IMPORT_STATEMENT_REPACKING_PLANNING:
          url = '/app-repacking/' + ActionTypeEneum.view + '/' + this.claim.referenceId;
          break;
        case ClaimEnum.EXPORT_STATEMENT:
          url = '/export-statement/' + ActionTypeEneum.view + '/' + this.claim.referenceId;
          break;
        case ClaimEnum.DELIVERY:
          url = '/import-statement/' + ActionTypeEneum.view + '/' + this.claim.referenceId;
          break;
        case ClaimEnum.IMPORT_STATEMENT_EXPORT_STATEMENT:
          url = '/import-statement/' + ActionTypeEneum.view + '/' + this.claim.referenceId;
          break;
        default:
          break;
      }
    }
    this.router.navigate([this.router.url + url]).then();
  }

  getPosition(e: any, arr: any[]) {
    return arr && arr.length > 0 ? (arr.indexOf(e) + 1).toString() : '';
  }

  getImageUrl(nativeUrl: string): string {
    return environment.BASE_URL + '/files/' + nativeUrl;
  }

  changeSlide(n: any) {
    const dots = document.getElementsByClassName('item');
    const index = this.slideIndex + n;
    if (index > 0 && index <= dots.length) {
      this.slideIndex = index;
      this.showSlide(index);
    }
  }

  currentSlide(index: any) {
    this.slideIndex = index + 1;
    console.log(this.slideIndex)
    this.showSlide(this.slideIndex);
  }

  showSlide(index: any) {
    const numberText = document.getElementById('number_text');
    const slideImage = document.getElementById('slide_image');
    const captionText = document.getElementById('caption');
    const dots = document.getElementsByClassName('item');

    for (let i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(' active', '');
    }

    dots[index - 1].className += ' active';

    if (numberText) {
      numberText.innerHTML = index + '/' + dots.length;
    }

    if (slideImage) {
      slideImage.setAttribute('style', '' + dots[index - 1].getAttribute('style'));
    }

    if (captionText) {
      captionText.innerHTML = '' + dots[index - 1].getAttribute('data-description');
    }
  }

  onLoad(img: HTMLImageElement, i: any) {
    const parentElement = img.closest('.item') as HTMLElement;
    parentElement.setAttribute('style', 'background-image:url("' + img.src + '")');

    if (i == 0) {
      parentElement.classList.add('active');

      const slideImage = document.getElementById('slide_image');
      const captionText = document.getElementById('caption');

      if (slideImage) {
        slideImage.setAttribute('style', 'background-image:url("' + img.src + '")');
      }

      if (captionText) {
        captionText.innerHTML = '' + img.getAttribute('alt');
      }
    }
  }

  updateBreadCrumb(event: any) {
    if (event.label !== 'menu.claim'){
      event.label = this.translateService.instant('claim.label.view');
    }
    if (event.label === 'common.label.view'){
      event.label = this.claim?.code;
    }

  }
}
