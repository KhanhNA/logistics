import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../modules/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {AuthoritiesResolverService, MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {ImportPoV2EditComponent} from '../import-po-v2/import-po-v2-edit/import-po-v2-edit.component';
import {ClaimService} from '../../_services/http/claim/claim.service';
import {ClaimComponent} from './claim.component';
import {ClaimInfoComponent} from './claim-info/claim-info.component';
import {AERepackingComponent} from '../repacking/a-e-repacking/a-e-repacking.component';
import {EditImportStatementComponent} from '../import-statement/edit-import-statement/edit-import-statement.component';
import {AddEditPoV2Component} from '../pov2/add-edit-po/add-edit-po-v2.component';
import {AddEditExportStatementV2Component} from '../export-statement-v2/add-edit-export-statement-v2/add-edit-export-statement-v2.component';
import {ClaimAuthoritiesService} from "../../_services/authority/claim.authorities.service";
import {ImportPoAuthoritiesService} from "../../_services/authority/import.po.authorities.service";
import {Pov2Service} from "../pov2/pov2.service";
import {PoAuthoritiesService} from "../../_services/authority/po.authorities.service";
import {AddEditRepackingV2Component} from "../repacking-v2/add-edit-repacking-v2/add-edit-repacking-v2.component";
import {RepackingAuthoritiesService} from "../../_services/authority/repacking.authorities.service";
import {RepackingV2Service} from "../repacking-v2/repacking-v2.service";
import {ImportStatementV2AddEditComponent} from "../import-statement-v2/import-statement-v2-add-edit/import-statement-v2-add-edit.component";
import {ExportStatementV2Service} from "../export-statement-v2/export-statement-v2.service";
import {InventoryAuthoritiesService} from "../../_services/authority/inventory.authorities.service";
import {MerchantOrderHttpService} from "../../_services/http/merchant.order.http.service";
import {ShippingPartnerHttpService} from "../../_services/http/shipping.partner.http.service";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/', suffix: '.json'},
    {prefix: './assets/i18n/po/', suffix: '.json'},
    {prefix: './assets/i18n/claim/', suffix: '.json'},
    {prefix: './assets/i18n/import-po/', suffix: '.json'},
    {prefix: './assets/i18n/export-statement/', suffix: '.json'},
    {prefix: './assets/i18n/repacking/', suffix: '.json'},
    {prefix: './assets/i18n/import-statement/', suffix: '.json'},
  ]);
}

const routes: Routes = [
  {
    path: '',
    component: ClaimComponent,
    pathMatch: 'full',
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: ':actType/:claimId',
    data: {breadcrumb: 'common.label.view'},
    children: [
      {
        path: '',
        component: ClaimInfoComponent,
        pathMatch: 'full',
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: 'po/:actType/:id',
        component: AddEditPoV2Component,
        data: {breadcrumb: 'claim.reference.po', isView: true},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: 'import-po/:actType/:id',
        component: ImportPoV2EditComponent,
        data: {breadcrumb: 'claim.reference.importPo', isView: true},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: 'app-repacking/:actType/:id',
        component: AddEditRepackingV2Component,
        data: {breadcrumb: 'claim.reference.repacking', isView: true},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: 'export-statement/:actType/:id',
        component: AddEditExportStatementV2Component,
        data: {breadcrumb: 'claim.reference.exportStatement', isView: true},
        resolve: {me: AuthoritiesResolverService}
      },
      {
        path: 'import-statement/:actType/:id',
        component: ImportStatementV2AddEditComponent,
        data: {breadcrumb: 'claim.reference.importStatement', isView: true},
        resolve: {me: AuthoritiesResolverService}
      },
    ],
  },
];

@NgModule({
  declarations: [
    ClaimComponent,
    ClaimInfoComponent,

  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      isolate: true,
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ClaimService,
    ClaimAuthoritiesService,
    ImportPoAuthoritiesService,
    Pov2Service,
    PoAuthoritiesService,
    RepackingAuthoritiesService,
    RepackingV2Service,
    ExportStatementV2Service,
    InventoryAuthoritiesService,
    MerchantOrderHttpService,
    ShippingPartnerHttpService
  ]
})
export class ClaimModule {
}
