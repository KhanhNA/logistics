import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {NavService} from '../../_services/nav.service';
import {WarehouseListComponent} from './warehouse-list/warehouse-list.component';
import {WarehouseOrderComponent} from './warehouse-order/warehouse-order.component';

@Component({
  selector: 'app-orders-bonus',
  templateUrl: './goods-bonus.component.html',
  styleUrls: ['./goods-bonus.component.scss', '../table.layout.scss', '../form.layout.scss']
})
export class GoodsBonusComponent implements OnInit, AfterViewInit {

  @ViewChild(WarehouseListComponent) warehouseList?: WarehouseListComponent;
  @ViewChild(WarehouseOrderComponent) warehouseOrder?: WarehouseOrderComponent;

  constructor(
    protected route: ActivatedRoute,
    protected translateService: TranslateService,
    protected toastr: ToastrService,
    private navService: NavService,
    private router: Router
  ) {
  }

  ngOnInit() {

  }


  ngAfterViewInit(): void {
    this.navService.title = 'menu.goods.bonus';
  }

  createExportStatement() {
    let merchantOrderIds: any;

    merchantOrderIds = this.warehouseOrder?.results.filteredData.filter(((c: any) => c.checkbox)).map(a => a.id);

    if (merchantOrderIds.length === 0) {
      this.warnNoChx('notice.checkbox.nocheck');
      return;
    } else {
      const fromStore = this.warehouseList?.results.filteredData.filter(c => c.id === this.warehouseOrder?.storeId) as any;
      if (fromStore && fromStore[0].dc) {
        this.router.navigate(['export-statement', 'add'],
          {
            state: {
              fromStore: fromStore[0],
              merchantOrderIds
            }
          }).then();
      } else {
        const msg = this.translateService.instant('export.create.toStore.fail');
        this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
        this.toastr.error(msg);
        return;
      }

    }
  }

  warnNoChx(key: any) {
    const msg = this.translateService.instant(key);
    this.toastr.toastrConfig.positionClass = 'toast-bottom-right';
    this.toastr.warning(msg);
  }

  onRowClick(id: number) {
    this.warehouseOrder?.setStoreId(id);
    this.warehouseOrder?.onSubmit();
  }
}
