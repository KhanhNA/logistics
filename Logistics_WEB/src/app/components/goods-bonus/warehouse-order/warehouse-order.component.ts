import {Component, EventEmitter, Injector, OnInit, Output} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes, DateRangePickerModel,
  DateUtilService,
  FormStateService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {NavService} from '../../../_services/nav.service';
import {HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-warehouse-order',
  templateUrl: './warehouse-order.component.html',
  styleUrls: ['./warehouse-order.component.css']
})
export class WarehouseOrderComponent extends BaseSearchLayout implements OnInit {

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  moduleName = 'merchant.order';
  hasPermissionSearch = false;
  hasPermissionCreate = false;
  storeId = 2;
  @Output() createExport = new EventEmitter();
  statusOptions = {
    '-1': 'merchant.order.status.all',
    0: 'merchant.order.status.not.enough.quantity',
    1: 'merchant.order.status.enough.quantity',
    2: 'merchant.order.status.exported',
    3: 'merchant.order.status.returned',
    4: 'merchant.order.status.canceled'
  };

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private dateUtilService: DateUtilService,
              private navService: NavService) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        date: [new DateRangePickerModel(
          dateUtilService.convertDateToStringCurrentGMT(dateUtilService.addDays(dateUtilService.getDateNow(), -7)),
          dateUtilService.convertDateToStringCurrentGMT(dateUtilService.getDateNow())
        )],
        fromDate: [dateUtilService.convertDateToStringCurrentGMT(dateUtilService.addDays(dateUtilService.getDateNow(), -7))],
        toDate: [dateUtilService.convertDateToStringCurrentGMT(dateUtilService.getDateNow())]
      }));
    this.columns.push(
      {
        columnDef: 'checkbox',
        header: 'checkbox',
        title: (e: any) => `${''}`,
        cell: (e: any) => `${''}`,
        columnType: ColumnTypes.CHECKBOX,
      },
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code',
        header: 'code',
        title: (e: any) => `${e.code}`,
        cell: (e: any) => `${e.code}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-code'
      },
      {
        columnDef: 'qRCode',
        header: 'qRCode',
        title: (e: any) => `${e.qRCode}`,
        cell: (e: any) => `${e.qRCode}`,
        align: AlignEnum.CENTER,
        columnType: ColumnTypes.BASE64,
        className: 'mat-column-qRCode'
      },
      {
        columnDef: 'merchantCode',
        header: 'merchantCode',
        title: (e: any) => `${e.merchantCode}`,
        cell: (e: any) => `${e.merchantCode}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-merchantCode'
      },
      {
        columnDef: 'merchantName',
        header: 'merchantName',
        title: (e: any) => `${e.merchantName}`,
        cell: (e: any) => `${e.merchantName}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-merchantName'
      },
      {
        columnDef: 'orderDate',
        header: 'orderDate',
        title: (e: any) => `${this.dateUtilService.convertDateToDisplayGMT0(e.orderDate)}`,
        cell: (e: any) => `${this.dateUtilService.convertDateToDisplayGMT0(e.orderDate)}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-orderDate'
      },
      {
        columnDef: 'orderCode',
        header: 'orderCode',
        title: (e: any) => `${e.orderCode}`,
        cell: (e: any) => `${e.orderCode}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-orderCode'
      },
      {
        columnDef: 'fromStore',
        header: 'fromStore',
        title: (e: any) => `${e.fromStore.code + ' - ' + e.fromStore.name}`,
        cell: (e: any) => `${e.fromStore.code + ' - ' + e.fromStore.name}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-fromStore'
      },
      {
        columnDef: 'amount',
        header: 'amount',
        title: (e: any) => `${e.amount}`,
        cell: (e: any) => `${e.amount}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-amount'
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (e: any) => `${this.translateService.instant(this.statusOptions[e.status])}`,
        cell: (e: any) => `${this.translateService.instant(this.statusOptions[e.status])}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-amount'
      },
    );
  }

  ngOnInit(): void {
    this.hasPermissionSearch = this.authoritiesService.hasAuthority('get/merchant-orders');
    this.hasPermissionCreate = this.authoritiesService.hasAuthority('post/export-statements');
    super.ngOnInit();
    super.onSubmit();
  }

  createExportStatement() {
    this.createExport.emit();
  }

  search(): void {
    const params = new HttpParams()
      .set('text', `${this.searchForm.value.text + ''}`)
      .set('status', '0')
      .set('fromDate', `${this.searchForm.value.fromDate}`)
      .set('toDate', `${this.searchForm.value.toDate}`)
      .set('storeId', `${this.storeId}`);
    this._fillData('/merchant-orders', params);
  }

  setStoreId(id: number) {
    this.storeId = id;
    this.onSubmit();
  }
}
