import {Component, EventEmitter, Injector, OnInit, Output} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout, ButtonFields, ColumnFields, DateUtilService,
  FormStateService,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {NavService} from '../../../_services/nav.service';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-warehouse-list',
  templateUrl: './warehouse-list.component.html',
  styleUrls: ['./warehouse-list.component.css']
})
export class WarehouseListComponent extends BaseSearchLayout implements OnInit {

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  @Output() clickRow =  new EventEmitter<number>();

  moduleName = 'store';
  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected uiStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              private dateUtilService: DateUtilService,
              private navService: NavService) {
    super(router, apiService, utilsService, uiStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        text: [''],
        status: ['_0'],
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code',
        header: 'code',
        title: (e: any) => `${e.code}`,
        cell: (e: any) => `${e.code}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-code'
      },
      {
        columnDef: 'name',
        header: 'name',
        title: (e: any) => `${e.name}`,
        cell: (e: any) => `${e.name}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-name'
      },
      {
        columnDef: 'domainName',
        header: 'domainName',
        title: (e: any) => `${e.domainName}`,
        cell: (e: any) => `${e.domainName}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-domainName'
      },
      {
        columnDef: 'address',
        header: 'address',
        title: (e: any) => `${e.address}`,
        cell: (e: any) => `${e.address}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-address'
      },
      {
        columnDef: 'city',
        header: 'city',
        title: (e: any) => `${e.city ? e.city : ''}`,
        cell: (e: any) => `${e.city ? e.city : ''}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-city'
      },
      {
        columnDef: 'phone',
        header: 'phone',
        title: (e: any) => `${e.phone}`,
        cell: (e: any) => `${e.phone}`,
        align: AlignEnum.CENTER,
        className: 'mat-column-phone'
      },
    );
  }

  ngOnInit(): void {
    super.ngOnInit();
    super.onSubmit();
  }

  search(): void {
    super.search();
    this._fillData('/merchant-orders/store-exists-not-enough-quantity', new HttpParams());
  }

  onRowClickAction(data: any) {
    this.clickRow.emit(data.object.id);
  }
}
