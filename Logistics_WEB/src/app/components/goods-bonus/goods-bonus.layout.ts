import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {Location} from '@angular/common';
import {HttpParams} from '@angular/common/http';
import {LDBaseLayout} from '../../base/l-d-base.layout';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {FieldConfigExt} from '../../base/field/field.interface';
import {Search} from '../../_models/Search';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {Directive} from "@angular/core";

// @Directive()
export class GoodsBonusLayout extends LDBaseLayout {
  tblCols: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'index',
      name: 'STT',
      label: 'ld.no',
      inputType: 'text',
      value: ''
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'store.code',
      inputType: 'text',
      name: 'code',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'store.name',
      inputType: 'text',
      name: 'name',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'store.domainName',
      inputType: 'text',
      name: 'domainName',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'store.address',
      inputType: 'text',
      name: 'address',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'store.city',
      inputType: 'text',
      name: 'city',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'store.phone',
      inputType: 'text',
      name: 'phone',
    })
  ];

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, translateService, toastr, location, authoritiesService);
    this.setCols(this.tblCols);
  }

  updateSearchInfo(search: Search) {
    const params = new HttpParams()
      .set('pageNumber', '' + (search.pageNumber + 1))
      .set('pageSize', '' + search.pageSize);
    search.method = this.apiService.get('/merchant-orders/store-exists-not-enough-quantity', params);
  }

}
