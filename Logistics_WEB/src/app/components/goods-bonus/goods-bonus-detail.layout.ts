import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {DatePipe, Location} from '@angular/common';
import {HttpParams} from '@angular/common/http';
import {LDBaseLayout} from '../../base/l-d-base.layout';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {FieldConfigExt} from '../../base/field/field.interface';
import {Search} from '../../_models/Search';
import {environment} from '../../../environments/environment';
import {ApiService, AuthoritiesService, Page} from '@next-solutions/next-solutions-base';

// @Directive()
export class GoodsBonusDetailLayout extends LDBaseLayout {

  statusOptions = {
    '-1': 'merchant.order.status.all',
    0: 'merchant.order.status.not.enough.quantity',
    1: 'merchant.order.status.enough.quantity',
    2: 'merchant.order.status.exported',
    3: 'merchant.order.status.returned',
    4: 'merchant.order.status.canceled'
  };
  inputFields: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'input',
      name: 'text',
      label: 'common.search',
      binding: 'text',
      inputType: 'input'
    }),
    new FieldConfigExt({
      type: 'date',
      label: 'common.fromDate',
      inputType: 'date',
      name: 'fromDate',
      binding: 'fromDate',
      options: {max: ['toDate']},
      require: 'true',
    }),
    new FieldConfigExt({
      type: 'date',
      label: 'common.toDate',
      inputType: 'date',
      name: 'toDate',
      binding: 'toDate',
      options: {min: ['fromDate'], max: ['curDate']},
      require: 'true'
    }),
  ];
  tblCols: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'checkboxAll',
      inputType: 'all',
      name: 'chkbox'
    }),
    new FieldConfigExt({
      type: 'index',
      name: 'STT',
      label: 'ld.no',
      inputType: 'text',
      value: ''
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'code',
      label: 'common.code',
      inputType: 'text',
      value: ''
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'qRCode',
      label: 'merchant.order.table.header.qRCode',
      inputType: 'fileImgBase64',
      value: ''
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'merchantCode',
      label: 'merchant.order.table.header.merchantCode',
      inputType: 'text',
      value: ''
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'merchantName',
      label: 'merchant.order.table.header.merchantName',
      inputType: 'text',
      value: ''
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'orderDate',
      label: 'merchant.order.table.header.orderDate',
      inputType: 'date',
      value: ''
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'orderCode',
      label: 'merchant.order.table.header.orderCode',
      inputType: 'text',
      value: ''
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'fromStore.name',
      label: 'merchant.order.table.header.fromStore',
      inputType: 'text',
      value: ''
    }),
    new FieldConfigExt({
      type: 'view',
      name: 'amount',
      label: 'merchant.order.table.header.amount',
      inputType: 'number',
      value: ''
    }),

    new FieldConfigExt({
      type: 'view',
      name: 'status',
      label: 'common.status',
      inputType: 'select',

    }),

  ];
  title?: string;
  storeId?: number;

  constructor(protected tsuService: TsUtilsService, fb: FormBuilder, protected route: ActivatedRoute,
              protected translateService: TranslateService, protected toastr: ToastrService,
              protected location: Location, private apiService: ApiService,
              private datePipe: DatePipe,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, translateService, toastr, location, authoritiesService);
    this.init(this.inputFields);
    this.setCols(this.tblCols);
    this.setSelection(true);
    this.formData = this.formData.formDate ? this.formData : {
      text: '',
      fromDate: new Date(new Date().getFullYear(), (new Date()).getMonth() - 1, new Date().getDate(),
        0, 0, 0),
      toDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(),
        new Date().getHours() + (new Date().getTimezoneOffset() / 60), new Date().getMinutes(), new Date().getSeconds()),
    };
  }

  updateSearchInfo(search: Search) {
    const params = new HttpParams()
      .set('text', this.fv.text)
      .set('status', '0')
      .set('fromDate', this.datePipe.transform(this.formData.fromDate, environment.API_DATE_FORMAT) + '')
      .set('toDate', this.datePipe.transform(this.formData.toDate, environment.API_DATE_FORMAT) + '')
      .set('storeId', this.storeId + '')
      .set('pageNumber', (search.pageNumber + 1).toString())
      .set('pageSize', search.pageSize.toString());
    search.method = this.apiService.get('/merchant-orders', params);
  }

  beforeUpdate = (data: any) => {
    for (const merchantOrder of data.content) {
      merchantOrder.fromStore = {
        name: merchantOrder.fromStore.code + ' - ' + merchantOrder.fromStore.name
      };
    }
  }

  afterSearch = (data: Page) => {
    data.content.forEach(detail => {
      detail.status = this.translateService.instant(this.statusOptions[detail.status]);
    });
  }
}
