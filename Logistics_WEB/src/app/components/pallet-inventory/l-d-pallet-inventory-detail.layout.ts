import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {LDBaseLayout} from '../../base/l-d-base.layout';
import {ToastrService} from 'ngx-toastr';
import {FieldConfigExt} from '../../base/field/field.interface';
import {Search} from '../../_models/Search';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, Location} from '@angular/common';
import {HttpParams} from '@angular/common/http';
import {ProductDescriptionsUtils} from '../../_utils/product.descriptions.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';


// @Directive()
export class LDPalletInventoryDetailLayout extends LDBaseLayout {
  pallet: any;
  distributor: any;

  constructor(tsuService: TsUtilsService, private userService: ApiService, fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              transService: TranslateService, toastr: ToastrService, location: Location,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    this.setCols(this.detailCols);
  }


  private detailCols = [
    new FieldConfigExt({
      type: 'index',
      label: 'ld.no',
      inputType: 'text',
      name: 'STT',
      value: '',
    }),
    new FieldConfigExt({
      type: 'icon',
      inputType: 'text',
      name: 'icon',
      value: '',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'common.packing.code',
      inputType: 'text',
      name: 'productPacking.code',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'common.product.name',
      inputType: 'text',
      name: 'productPacking.product.name',
    }),

    new FieldConfigExt({
      type: 'view',
      label: 'common.packingType',
      inputType: 'number',
      name: 'productPacking.packingType.quantity',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'common.expireDate',
      inputType: 'date',
      name: 'expireDate',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'common.quantity',
      inputType: 'number',
      name: 'quantity',
    }),
  ];

  viewPallet(pallet: number, distributor: any) {
    this.pallet = pallet;
    this.distributor = distributor;
    this.onSearchData();
  }

  updateSearchInfo(search: Search) {
    const params = new HttpParams()
      .set('-1', '')
      .set('palletId', `${this.pallet.id}`)
      .set('distributorId', this.distributor ? this.distributor.id : '')
      .set('palletSteps', `${this.pallet.step}`)
      .set('storeId', `${this.pallet.store.id}`)
      .set('text', '')
      .set('pageNumber', '' + (search.pageNumber + 1))
      .set('pageSize', '' + search.pageSize);
    search.method = this.userService.get('/pallets/product-packing-group-by-expire-date', params);
  }

  beforeUpdate = (data: any) => {
    data.content.forEach((pallet: any) => {
      ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(pallet.productPacking.product);
    });
  }
}





