import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {LDBaseLayout} from '../../base/l-d-base.layout';
import {ToastrService} from 'ngx-toastr';
import {FieldConfigExt} from '../../base/field/field.interface';
import {Search} from '../../_models/Search';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, Location} from '@angular/common';
import {AsyncDataUtils} from '../../_utils/async.data.utils';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import { Directive } from "@angular/core";

// @Directive()
export class LDPalletInventoryLayout extends LDBaseLayout {
  stepOptions = ['Nhận hàng', 'Lưu trữ'];
  stepIds = ['1', '2'];
  cols = [
    new FieldConfigExt({
      type: 'index',
      label: 'ld.no',
      inputType: 'text',
      name: 'STT',
      value: '',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'qr-code',
      inputType: 'fileImgBase64',
      name: 'qRCode'
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'pallet.ae.code',
      inputType: 'text',
      name: 'code',
    }),
    new FieldConfigExt({
      type: 'view',
      label: 'pallet.ae.name',
      inputType: 'text',
      name: 'name',
    }),
  ];

  inputFields: FieldConfigExt[] | undefined;

  storesCollection: any[] = [];
  distributorCollection: any[] = [];

  constructor(tsuService: TsUtilsService, private userService: ApiService, fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              transService: TranslateService, toastr: ToastrService, location: Location,
              protected authoritiesService: AuthoritiesService) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    this.getFormDataBinding();
  }

  getFormDataBinding() {
    this.getAsyncData().then(data => {
      this.distributorCollection.push({
        code: '- ' + this.translateService.instant('common.status.all'),
        name: '',
        id: ''
      });
      data.stores.then(response => {
        this.storesCollection.push(...response.content);
      });
      data.distributors.then(distributors => {
        this.distributorCollection.push(...distributors);
      });
    }).then(() => {
      this.inputFields = [
        new FieldConfigExt({
          type: 'input',
          label: 'common.text',
          inputType: 'text',
          name: 'text',
          binding: 'text'
        }),
        new FieldConfigExt({
          type: 'autocomplete',
          label: 'pallet.ae.store.name',
          inputType: 'text',
          name: 'store',
          value: '',
          options: ['code', 'name'],
          collections: this.storesCollection,
          binding: 'store',
          require: 'true'
        }),
        new FieldConfigExt({
          type: 'autocomplete',
          label: 'po.ae.distributor.code',
          inputType: 'text',
          name: 'distributor.code',
          value: '',
          options: ['code', 'name'],
          collections: this.distributorCollection,
          binding: 'distributor',
        }),
        new FieldConfigExt({
          type: 'select',
          label: 'pallet.step',
          inputType: 'text',
          name: 'step',
          binding: 'step',
          options: this.stepOptions
        })
      ];

      this.setCols(this.cols);
      this.init(this.inputFields);
      this.formData = this.formData.store ? this.formData : {
        text: '',
        store: this.storesCollection[0],
        distributor: this.distributorCollection[0],
        step: '0'
      };
      this.onSearchData();
    });
  }

  async getAsyncData() {
    return AsyncDataUtils.getAsyncDataStoresAndDistributors(this.tsuService, this.tsuService);
  }


  updateSearchInfo(search: Search) {
    const params = new HttpParams()
      .set('storeId', this.formData.store ? this.formData.store.id : '')
      .set('distributorId', this.formData.distributor ? this.formData.distributor.id : '')
      .set('text', this.formData.text ? this.formData.text : '')
      .set('status', 'true')
      .set('step', this.stepIds[this.formData.step ? this.formData.step : 0])
      .set('pageNumber', '' + (search.pageNumber + 1))
      .set('pageSize', '' + search.pageSize);
    search.method = this.userService.get('/pallets', params);
  }

  beforeUpdate = (data?: any) => {
    if (data !== undefined && data.content !== undefined) {
      data.content.forEach((value: any) => {
        value.palletImgStatus = value.qRCode;
      });
    }
  }
}





