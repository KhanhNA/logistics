import {CookieService} from 'ngx-cookie-service';
import {FormBuilder} from '@angular/forms';
import {CommunicationService} from '../../communication-service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {MultilanguagePanigator} from '../../_helpers/multilanguage.paginator';
import {CustomDateAdapter, MY_FORMATS_1} from '../../base/field/custom-date-adapter';

import {LDPalletInventoryLayout} from './l-d-pallet-inventory.layout';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {LDPalletInventoryDetailLayout} from './l-d-pallet-inventory-detail.layout';
import {NavService} from '../../_services/nav.service';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';

@Component({
  selector: 'app-pallet-inventory', /* dinh nghia id*/
  templateUrl: './l-d-pallet-inventory.html',
  styleUrls: ['../form.layout.scss', '../table.layout.scss', './l-d-pallet-inventory.scss'],
  providers: [
    {provide: MatPaginatorIntl, useClass: MultilanguagePanigator},
    DatePipe, {
      provide: DateAdapter, useClass: CustomDateAdapter
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS_1}
  ]
})
export class LDPalletInventoryComponent implements OnInit, AfterViewInit {

  ldInfoDetail: any = {id: 'id', entity: 'pallet', showSearch: false};
  selPallet: any;
  model: LDPalletInventoryLayout;
  modelDetail: LDPalletInventoryDetailLayout;

  constructor(fb: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, private toastr: ToastrService, private datePipe: DatePipe,
              private serv: CommunicationService, tranService: TranslateService,
              tsuService: TsUtilsService, route: ActivatedRoute, location: Location, private navService: NavService,
              protected authoritiesService: AuthoritiesService) {
    this.model = new LDPalletInventoryLayout(tsuService, userService, fb, route, this.datePipe, tranService, toastr, location, authoritiesService);
    this.modelDetail = new LDPalletInventoryDetailLayout(tsuService, userService, fb, route, this.datePipe, tranService, toastr, location, authoritiesService);
  }

  onRowClick(event: any) {
    this.modelDetail.viewPallet(event.row, this.model.formData.distributor);
  }

  ngAfterViewInit(): void {
    this.navService.title = 'menu.pallet.inventory';
  }

  ngOnInit(): void {
    this.model.afterSearch = this.afterSearch;
    this.model.onSearchData();
  }

  afterSearch = (data: any) => {
    if (this.model.getTblData()[this.model.selectedRowInd]) {
      this.modelDetail.viewPallet(this.model.getTblData()[this.model.selectedRowInd], this.model.formData.distributor);
    } else {
      this.modelDetail.tsTable.search.totalElements = 0
    }
  }


  onPage(page: any) {
    this.modelDetail.tsTable.removeRows(0, this.modelDetail.getTblData().length);
    this.model.onPage(page);
  }

  onSearch() {
    this.modelDetail.tsTable.removeRows(0, this.modelDetail.getTblData().length);
    this.model.onSearchData();
  }
}
