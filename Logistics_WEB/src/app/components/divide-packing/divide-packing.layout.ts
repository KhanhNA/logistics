import {LDBaseLayout} from '../../base/l-d-base.layout';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {FieldConfigExt} from '../../base/field/field.interface';
import {NumberValidators} from '../../base/field/number-validators';
import {ActionTypeEneum} from '../../_models/action-type';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {Directive} from "@angular/core";

// @Directive()
export class DividePackingLayout extends LDBaseLayout {

  packing: any = {};

  getPallets = () => {
    return this.tsuService.getPallets(this.data.store.id, 1, true);
  };

  getPallets2 = () => {
    return this.tsuService.getPallets(this.data.store.id, 2, true);
  }

  palletCol: FieldConfigExt = new FieldConfigExt({
    type: 'autocomplete',
    label: 'common.pallet',
    inputType: 'text',
    name: 'pallet.code',
    value: '',
    binding: 'pallet',
    options: ['code', 'name'],
  });

  constructor(tsuService: TsUtilsService, private apiService: ApiService, fb: FormBuilder,
              route: ActivatedRoute,
              private datePipe: DatePipe,
              toastr: ToastrService,
              transService: TranslateService, location: Location, public data: any,
              protected authoritiesService: AuthoritiesService
  ) {
    super(tsuService, fb, route, transService, toastr, location, authoritiesService);
    if (data.nameCol) {
      this.inputFields.forEach(field => {
        if (field.name === 'product.name') {
          field.name = data.nameCol.name;
          field.binding = data.nameCol.name;
        }
      });
    }
    this.packing = data.packing;
    if (this.packing.expireDate && this.data.type === ActionTypeEneum.GR) {
      this.packing.expireDate = this.packing.expireDate.replace(' ', 'T');
    }

    this.displayCols();
    this.packing.quantity = this.packing.quantity - 1;
    this.init(this.inputFields);
    this.formData = this.packing;
  }

  displayCols() {
    if (this.data.type === ActionTypeEneum.Pal) {
      this.inputFields.forEach(field => {
        if (field.name === 'expireDate') {
          this.inputFields.splice(this.inputFields.indexOf(field), 1);
          this.palletCol.collections = this.getPallets;
          this.inputFields.push(this.palletCol);
          return;
        }
      });
    } else if (this.data.type === 'from-repack') {
      this.inputFields.forEach(field => {
        if (field.name === 'expireDate') {
          this.inputFields.splice(this.inputFields.indexOf(field), 1);
          this.palletCol.collections = this.getPallets2;
          this.inputFields.push(this.palletCol);
          return;
        }
      });


    }
  }

  checkLifeCircle = (obj: any) => {
    const curDate = new Date();
    const expireDate = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate()
      + obj.obj.product.lifecycle, 0, 0, 0);
    if (new Date('' + obj.value) > expireDate) {
      this.showWarningMsg('date.pick.not.gather.than.life.cycle');
      obj.field.bindingRef[0][obj.field.bindingRef[1]] = null;
      obj.field.form.get('expireDate')?.setValue(null);
    } else {
      if (obj.field) {
        obj.field.bindingRef[0][obj.field.bindingRef[1]] = obj.field.form.get('expireDate')?.value;
      }
    }
  }

  inputFields: FieldConfigExt[] = [
    new FieldConfigExt({
      type: 'input',
      inputType: 'text',
      name: 'productPacking.code',
      binding: 'productPacking.code',
      label: 'common.packing.code',
      readonly: 'true'
    }),
    new FieldConfigExt({
      type: 'input',
      inputType: 'text',
      readonly: 'true',
      name: 'product.name',
      binding: 'product.name',
      label: 'common.product.name'
    }),
    new FieldConfigExt({
      type: 'input',
      name: 'quantity',
      label: 'common.quantity',
      binding: 'quantity',
      inputType: 'number',
      require: 'true',
      validations: [
        {
          name: 'integerNumber',
          validator: NumberValidators.integerValidation(),
          message: 'validation.integer'
        },
        {
          name: 'maxQuantity',
          validator: NumberValidators.quantityValidation(this.data.packing.quantity - 1),
          message: 'validation.maxQuantity.lessthan'
        },
        {
          name: 'minQuantity',
          validator: NumberValidators.minQuantityValidation(0),
          message: 'validation.minQuantity'
        },

      ]
    }),
    new FieldConfigExt({
      type: 'date',
      label: 'common.expireDate',
      inputType: 'date',
      name: 'expireDate',
      binding: 'expireDate',
      options: {min: ['curDate'], max: ['devide']},
      emit: this.checkLifeCircle
    }),


  ];
}
