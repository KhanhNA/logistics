import {ChangeDetectorRef, Component, Inject, OnInit, Renderer2} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ToastrService} from 'ngx-toastr';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import {CommunicationService} from '../../communication-service';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe, Location} from '@angular/common';
import {TsUtilsService} from '../../base/Utils/ts-utils.service';
import {DividePackingLayout} from './divide-packing.layout';
import {ApiService, AuthoritiesService} from '@next-solutions/next-solutions-base';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-divide-packing',
  templateUrl: './divide-packing.component.html',
  styleUrls: ['./divide-packing.component.scss'],
  providers: [
    DatePipe
  ]
})
export class DividePackingComponent implements OnInit {
  model: DividePackingLayout;

  constructor(formBuilder: FormBuilder, private router: Router, private userService: ApiService,
              cookieService: CookieService, private toastr: ToastrService,
              public dialog: MatDialog, private renderer: Renderer2,
              private comm: CommunicationService, private route: ActivatedRoute, private changeDetector: ChangeDetectorRef,
              transService: TranslateService, private datePipe: DatePipe,
              private tsuService: TsUtilsService, location: Location,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<DividePackingComponent>,
              protected authoritiesService: AuthoritiesService) {
    this.model = new DividePackingLayout(this.tsuService, userService, formBuilder, route, datePipe, toastr, transService, location, data, authoritiesService);
  }

  ngOnInit() {
  }

  onAddDividePacking() {
    if (this.model.formData.expireDate) {
      const arr = this.model.formData.expireDate.split(' ');
      this.model.formData.expireDate = this.datePipe.transform(new Date(arr[0]), environment.API_DATE_FORMAT, '-0');
    }
    // console.log('divide', this.model.formData.expireDate);
    this.dialogRef.close({data: {...this.model.formData}});
  }
}
