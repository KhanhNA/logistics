import {Component, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseAddEditLayout,
  ButtonFields,
  ColumnFields,
  ColumnTypes,
  DateUtilService,
  IconTypeEnum,
  LoaderService,
  Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder} from '@angular/forms';
import {CommonUtils} from '../../../_utils/common.utils';
import {ExportStatementDetailModel} from '../../../_models/export-statement/export.statement.detail.model';
import {forkJoin} from 'rxjs';
import {StoreModel} from '../../../_models/store.model';
import {StoresInventoryIgnoreExpireDateComponent} from '../../dialog-select-product-packing/stores-inventory-ignore-expire-date.component';
import {MatDialog} from '@angular/material/dialog';
import {ProductDescriptionsUtils} from '../../../_utils/product.descriptions.utils';
import {StoreProductPackingModel} from '../../../_models/store.product.packing.model';
import {ClaimEnum, ExportStatementStatus} from '../../../_models/action-type';
import {ExportStatementModel} from '../../../_models/export-statement/export.statement.model';
import {ExportStatementV2Service} from '../export-statement-v2.service';
import {ClaimService} from '../../../_services/http/claim/claim.service';
import {ClaimAuthoritiesService} from '../../../_services/authority/claim.authorities.service';
import {ClaimDetailModel} from '../../../_models/claim.detail.model';
import {InventoryAuthoritiesService} from '../../../_services/authority/inventory.authorities.service';
import {MerchantOrderHttpService} from '../../../_services/http/merchant.order.http.service';
import {ShippingPartnerHttpService} from '../../../_services/http/shipping.partner.http.service';
import {PartnerModel} from '../../../_models/partner/partner.model';

@Component({
  selector: 'app-add-export-statement-v2',
  templateUrl: './add-edit-export-statement-v2.component.html',
  styleUrls: ['./add-edit-export-statement-v2.component.scss']
})
export class AddEditExportStatementV2Component extends BaseAddEditLayout implements OnInit {

  fromStoresOptions: SelectModel[] = [];
  toStoresOptions: SelectModel[] = [];
  shippingPartnerOptions: SelectModel[] = [];

  exportStatement: ExportStatementModel | undefined;
  claimDetailsByReference: ClaimDetailModel[] | undefined;

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  totalColSpan = 0;

  maxAmountWarning = 0;
  currencyExportStatement?: string;
  merchantOrderIds: any[] = [];
  isGoodBonus = false;
  fromStoreForMerchantOrder: StoreModel | undefined;
  isView = false;
  isSO = false;
  hasClaim = false;
  isFcFromStore = false;

  constructor(protected activatedRoute: ActivatedRoute,
              protected location: Location,
              protected translateService: TranslateService,
              protected utilsService: UtilsService,
              protected authoritiesService: AuthoritiesService,
              private formBuilder: FormBuilder,
              private exportStatementV2Service: ExportStatementV2Service,
              private claimService: ClaimService,
              private dialog: MatDialog,
              private router: Router,
              private claimAuthoritiesService: ClaimAuthoritiesService,
              private inventoryAuthoritiesService: InventoryAuthoritiesService,
              private merchantOrderHttpService: MerchantOrderHttpService,
              private shippingPartnerHttpService: ShippingPartnerHttpService,
              private apiService: ApiService,
              private dateUtilService: DateUtilService,
              private loaderService: LoaderService
  ) {
    super(activatedRoute, location, translateService, utilsService, authoritiesService);
    activatedRoute.data.subscribe(data => {
      this.isView = !!data.isView;
      this.isSO = !!data.isSO;
    });
    this.columns.push(...[
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: ExportStatementDetailModel) => `${CommonUtils.getPosition(e, this.addEditForm?.get('exportStatementDetails')?.value)}`,
        cell: (e: ExportStatementDetailModel) => `${CommonUtils.getPosition(e, this.addEditForm?.get('exportStatementDetails')?.value)}`,
        display: (e: ExportStatementDetailModel) => true,
      },
      {
        columnDef: 'qrCode',
        header: 'qrCode',
        columnType: ColumnTypes.BASE64,
        title: (e: ExportStatementDetailModel) => `${e.storeProductPackingDetail?.qRCode ? e.storeProductPackingDetail?.qRCode : ''}`,
        cell: (e: ExportStatementDetailModel) => `${e.storeProductPackingDetail?.qRCode ? e.storeProductPackingDetail.qRCode : ''}`,
        display: (e: ExportStatementDetailModel) => this.isView,
      },
      {
        columnDef: 'packingCode',
        header: 'packingCode',
        title: (e: ExportStatementDetailModel) => `${e.productPacking?.code ? e.productPacking?.code : ''}`,
        cell: (e: ExportStatementDetailModel) => `${e.productPacking?.code ? e.productPacking?.code : ''}`,
        display: (e: ExportStatementDetailModel) => true,
      },
      {
        columnDef: 'productName',
        header: 'name',
        title: (e: ExportStatementDetailModel) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking?.product)}`,
        cell: (e: ExportStatementDetailModel) => `${ProductDescriptionsUtils.getProductNameByCurrentLanguage(e.productPacking?.product)}`,
        display: (e: ExportStatementDetailModel) => true,
      },
      {
        columnDef: 'packingQuantity',
        header: 'packingQuantity',
        title: (e: ExportStatementDetailModel) => `${e.productPacking?.packingType?.quantity
          ? e.productPacking?.packingType?.quantity : 0}`,
        cell: (e: ExportStatementDetailModel) => `${e.productPacking?.packingType?.quantity
          ? e.productPacking?.packingType?.quantity : 0}`,
        display: (e: ExportStatementDetailModel) => true,
      },
      {
        columnDef: 'totalQuantity',
        header: 'totalQuantity',
        title: (e: ExportStatementDetailModel) => `${e.totalQuantity ? e.totalQuantity : 0}`,
        cell: (e: ExportStatementDetailModel) => `${e.totalQuantity ? e.totalQuantity : 0}`,
        display: (e: ExportStatementDetailModel) => !this.isView,
      },
      {
        columnDef: 'orgQuantity',
        header: 'orgQuantity',
        title: (e: ExportStatementDetailModel) => `${e.orgQuantity ? e.orgQuantity : 0}`,
        cell: (e: ExportStatementDetailModel) => `${e.orgQuantity ? e.orgQuantity : 0}`,
        display: (e: ExportStatementDetailModel) => this.hasClaim,
      },
      {
        columnDef: 'claimQuantity',
        header: 'claimQuantity',
        title: (e: ExportStatementDetailModel) => `${e.claimQuantity ? e.claimQuantity : 0}`,
        cell: (e: ExportStatementDetailModel) => `${e.claimQuantity ? e.claimQuantity : 0}`,
        display: (e: ExportStatementDetailModel) => this.hasClaim,
      },
      {
        columnDef: 'quantity',
        header: () => this.isView && this.hasClaim ? 'export.statement.table.header.realQuantity' : 'common.quantity',
        columnType: () => this.isView ? ColumnTypes.VIEW : ColumnTypes.INPUT_COUNTER,
        isRequired: () => this.isView ? false : true,
        min: () => 0,
        max: (e: ExportStatementDetailModel) => !this.isView && e.totalQuantity ? e.totalQuantity : 0,
        title: (e: ExportStatementDetailModel) => `${e.quantity ? e.quantity : 0}`,
        cell: (e: ExportStatementDetailModel) => `${e.quantity ? e.quantity : 0}`,
        display: (e: ExportStatementDetailModel) => true,
        footers: [
          {
            columnDef: 'totalColSpan',
            title: (e: ExportStatementDetailModel[]) => this.translateService.instant('export.statement.title.total.'),
            cell: (rows: ExportStatementDetailModel[]) => {
              return this.translateService.instant('export.statement.title.total.');
            },
            display: (e: ExportStatementDetailModel[]) => {
              return !!this.totalColSpan && !this.isView;
            },
            colspan: (e: ExportStatementDetailModel[]) => {
              return this.totalColSpan;
            },
            align: AlignEnum.RIGHT,
          },
        ],
      },
      {
        columnDef: 'expiryDate',
        header: 'expiryDate',
        title: (e: ExportStatementDetailModel) => `${e.storeProductPackingDetail?.storeProductPacking?.expireDate ? this.dateUtilService.convertDateToDisplayServerTime(e.storeProductPackingDetail.storeProductPacking.expireDate) : ''}`,
        cell: (e: ExportStatementDetailModel) => `${e.storeProductPackingDetail?.storeProductPacking?.expireDate ? this.dateUtilService.convertDateToDisplayServerTime(e.storeProductPackingDetail.storeProductPacking.expireDate) : ''}`,
        display: (e: ExportStatementDetailModel) => this.isView,
      },
      {
        columnDef: 'pallet',
        header: 'pallet',
        title: (e: ExportStatementDetailModel) => `${e.palletDetail?.pallet?.displayName ? e.palletDetail.pallet.displayName : ''}`,
        cell: (e: ExportStatementDetailModel) => `${e.palletDetail?.pallet?.displayName ? e.palletDetail.pallet.displayName : ''}`,
        display: (e: ExportStatementDetailModel) => this.isView && this.isFcFromStore,
        footers: [
          {
            columnDef: 'totalColSpan',
            title: (e: ExportStatementDetailModel[]) => this.translateService.instant('export.statement.title.total.'),
            cell: (rows: ExportStatementDetailModel[]) => {
              return this.translateService.instant('export.statement.title.total.');
            },
            display: (e: ExportStatementDetailModel[]) => {
              return !!this.totalColSpan && this.isView;
            },
            colspan: (e: ExportStatementDetailModel[]) => {
              return this.totalColSpan;
            },
            align: AlignEnum.RIGHT,
          },
        ],
      },
      {
        columnDef: 'weight',
        header: 'weight',
        title: (e: ExportStatementDetailModel) => `${ExportStatementV2Service.getWeightProduct(e.productPacking, e).toLocaleString('en-US')}`,
        cell: (e: ExportStatementDetailModel) =>
          `${ExportStatementV2Service.getWeightProduct(e.productPacking, e).toLocaleString('en-US')}`,
        display: (e: ExportStatementDetailModel) => true,
        isShowHeader: true,
        footers: [
          {
            columnDef: 'test',
            title: (e: ExportStatementDetailModel[]) => this.translateService.instant('export.statement.title.total.weight'),
            cell: (rows: ExportStatementDetailModel[]) => {
              return Number(rows.reduce((total, value) => {
                return total + (value.weight ? value.weight : 0);
              }, 0) / Math.pow(10, 3)).toLocaleString('en-US') + ' kg';
            },
            display: (e: ExportStatementDetailModel[]) => {
              return !!this.totalColSpan;
            },
            colspan: (e: ExportStatementDetailModel[]) => {
              return 1;
            },
            align: AlignEnum.LEFT,
          },
        ],
      },
      {
        columnDef: 'volume',
        header: 'volume',
        title: (e: ExportStatementDetailModel) => `${ExportStatementV2Service.getVolumeProduct(e.productPacking, e).toLocaleString('en-US')}`,
        cell: (e: ExportStatementDetailModel) =>
          `${ExportStatementV2Service.getVolumeProduct(e.productPacking, e).toLocaleString('en-US')}`,
        display: (e: ExportStatementDetailModel) => true,
        isShowHeader: true,
        footers: [
          {
            columnDef: 'test',
            title: (e: ExportStatementDetailModel[]) => this.translateService.instant('export.statement.title.total.volume'),
            cell: (rows: ExportStatementDetailModel[]) => {
              return Number(rows.reduce((total, value) => {
                return total + (value.volume ? value.volume : 0);
              }, 0) / Math.pow(10, 9)).toLocaleString('en-US') + ' m3';
            },
            display: (e: ExportStatementDetailModel[]) => {
              return !!this.totalColSpan;
            },
            colspan: (e: ExportStatementDetailModel[]) => {
              return 2;
            },
            align: AlignEnum.LEFT,
          },
        ],
      },
    ]);

    this.buttons.push(...[
      {
        columnDef: 'remove',
        header: 'common.action',
        className: 'danger',
        click: 'onRemoveDetail',
        icon: 'fa fa-trash',
        iconType: IconTypeEnum.FONT_AWESOME,
        title: 'common.title.delete',
        display: (e: ExportStatementDetailModel) => !this.isView
      }
    ]);

    if (this.router.getCurrentNavigation()?.extras.state?.fromStore) {
      this.fromStoreForMerchantOrder = this.router.getCurrentNavigation()?.extras.state?.fromStore;
      this.merchantOrderIds = this.router.getCurrentNavigation()?.extras.state?.merchantOrderIds;
      this.isGoodBonus = true;
    }
  }

  async ngOnInit() {
    super.ngOnInit();
    this.addEditForm = this.formBuilder.group({
      id: [''],
      code: [''],
      fromStore: [''],
      fromStoreId: [''],
      fromStoreUsername: [''],
      fromStoreUserTel: [''],
      merchantCode: null,
      merchantOrderId: null,
      merchantOrderIds: [''],
      toStore: [''],
      toStoreId: [''],
      toStoreUsername: [''],
      toStoreUserTel: [''],
      description: [''],
      exportStatementDetails: [''],
      status: [''],
      shippingPartner: ['']
    });

    const currencyConfigObs = this.exportStatementV2Service.getConfigExportStatement();
    const fromStoreObs = this.exportStatementV2Service.createFromStoreObservable();
    const toStoreObs = this.exportStatementV2Service.createToStoreObservable(this.isEdit ? this.id : null);
    const shippingPartnerObs = this.shippingPartnerHttpService.createGetShippingPartnerAvailableObs();

    const response = await forkJoin({
      fromStorePage: fromStoreObs,
      toStorePage: toStoreObs,
      currencyConfig: currencyConfigObs,
      shippingPartnerPage: shippingPartnerObs
    }).toPromise() as {
      fromStorePage: Page,
      toStorePage: Page,
      shippingPartnerPage: Page,
      currencyConfig: any
    };
    this.fromStoresOptions = (response.fromStorePage.content as StoreModel[]).map(store =>
      new SelectModel(store.id, store.code + ' - ' + store.name, false, store));
    if (this.fromStoreForMerchantOrder) {
      this.fromStoresOptions = this.fromStoresOptions.filter(model => model.value !== this.fromStoreForMerchantOrder?.id);
    }
    this.toStoresOptions = (response.toStorePage.content as StoreModel[]).map(store =>
      new SelectModel(store.id, store.code + ' - ' + store.name, false, store));
    this.shippingPartnerOptions = (response.shippingPartnerPage.content as PartnerModel[]).map(partner =>
      new SelectModel(partner.id, partner.name + '', false, partner));
    if (response.currencyConfig && response.currencyConfig.value) {
      const arr = response.currencyConfig.value.split('|');
      this.currencyExportStatement = arr[0];
      this.maxAmountWarning = arr[1];
    }

    if (this.fromStoreForMerchantOrder) {
      this.addEditForm.patchValue({
        fromStore: this.fromStoresOptions.length ? this.fromStoresOptions[0].value : '',

        // fromStore là đối với L1 đặt hàng, bổ sung hàng cho fromStore truyền vào
        toStore: this.fromStoreForMerchantOrder.id ? this.fromStoreForMerchantOrder.id : ''
      });
    }

    if (!this.isEdit) {
      if (this.fromStoreForMerchantOrder) {
        await this.getMerchantOrderFromGoodsBonus(this.merchantOrderIds);
      } else {
        // this.addEditForm.patchValue({
        //   fromStore: this.fromStoresOptions.length ? this.fromStoresOptions[0].value : '',
        //   toStore: this.toStoresOptions.length ? this.toStoresOptions[0].value : ''
        // });
      }
    } else {
      const exportStatementPromise = this.exportStatementV2Service.getExportStatement(this.id).toPromise();
      if (this.claimAuthoritiesService.hasGetClaimByReferenceRole() && this.isView && !this.isHideClaimView()) {
        const claimDetailsByReferencePromise = this.claimService.getClaimByReferenceObs(this.id, ClaimEnum.EXPORT_STATEMENT).toPromise()
          .catch(err => {
            this.utilsService.showErrorToarst('claim.wait.approve.please');
          });
        this.claimDetailsByReference = await claimDetailsByReferencePromise;
      }
      this.exportStatement = await exportStatementPromise as ExportStatementModel;
      this.exportStatement.fromStoreUsername = this.exportStatement.fromStore?.user
        ? this.exportStatement.fromStore?.user.username + ' - ' + this.exportStatement.fromStore?.user.firstName
        + ' ' + this.exportStatement.fromStore?.user.lastName : '';
      this.exportStatement.fromStoreUserTel = this.exportStatement.fromStore?.user ? this.exportStatement.fromStore?.user.tel : '';
      this.exportStatement.toStoreUsername = this.exportStatement.toStore?.user
        ? this.exportStatement.toStore?.user.username + ' - ' + this.exportStatement.toStore?.user.firstName
        + ' ' + this.exportStatement.toStore?.user.lastName : '';
      this.exportStatement.toStoreUserTel = this.exportStatement.toStore?.user ? this.exportStatement.toStore?.user.tel : '';
      this.combineClaimWithDetails();
    }
  }

  isHideClaimView() {
    return this.exportStatement && (this.exportStatement.status === ExportStatementStatus.CANCELED
      || this.exportStatement.status === ExportStatementStatus.REQUESTED);
  }

  async combineClaimWithDetails() {
    if (!this.exportStatement) {
      return;
    }
    if (this.exportStatement.fromStore && !this.exportStatement.fromStore.dc) {
      this.isFcFromStore = true;
    }
    if (this.exportStatement.merchantOrderId) {
      this.utilsService.showWarningToarst('export.cant.update');
      this.location.back();
    }
    const claimMap = new Map<string, any>();
    if (this.claimDetailsByReference && this.claimDetailsByReference.length > 0) {
      this.hasClaim = true;
      let key = '';
      this.claimDetailsByReference.forEach((claimDetail: ClaimDetailModel) => {
        if (this.isFcFromStore) {
          key = claimDetail.palletDetail?.pallet?.id + ' ' + claimDetail.expireDate + claimDetail.productPacking?.id;
        } else {
          key = '' + claimDetail.expireDate + claimDetail.productPacking?.id;
        }
        if (claimMap.has(key)) {
          const detail = claimMap.get(key);
          detail.orgQuantity = Number(detail.orgQuantity) + (claimDetail.quantity ? claimDetail.quantity : 0);
          detail.claimQuatity = detail.orgQuantity;
          detail.quantity = 0;
        } else {
          claimDetail.orgQuantity = claimDetail.quantity;
          claimDetail.claimQuantity = claimDetail.quantity;
          claimDetail.quantity = 0;
          claimMap.set(key, claimDetail);
        }
      });
    }
    await this.collectProductPacking(claimMap);
    this.addEditForm.setValue(UtilsService.reduceEntityAttributeForFormControl(this.addEditForm, this.exportStatement));
    this.addEditForm.updateValueAndValidity();
    this.totalColSpan = CommonUtils.getTotalFooterColspanDynamic(this.columns, this.addEditForm.get('exportStatementDetails')?.value,
      ['weight', 'volume']);
  }

  async collectProductPacking(claimMap: Map<string, any>) {
    const uniqueDetailMap = new Map();
    const packingCodes: any[] = [];
    let key;
    this.exportStatement?.exportStatementDetails.forEach((detail: ExportStatementDetailModel) => {
      detail.product = detail.productPacking?.product;
      if (this.isView) {
        if (this.isFcFromStore) {
          key = detail.palletDetail?.pallet?.id + ' ' + detail.expireDate + detail.productPacking?.id;
        } else {
          key = '' + detail.expireDate + detail.productPacking?.id;
        }
      } else {
        key = detail.productPacking?.id;
      }
      if (uniqueDetailMap.has(key)) {
        const uniqueDetail = uniqueDetailMap.get(key);
        uniqueDetail.quantity = Number(uniqueDetail.quantity) + Number(detail.quantity);
        // this.collectDeleteDetailMap.get(uniqueDetail.id).push(detail.id);
      } else {
        detail.orgQuantity = detail.quantity;
        detail.claimQuantity = 0;
        uniqueDetailMap.set(key, detail);
        // this.collectDeleteDetailMap.set(detail.id, [detail.id]);
        packingCodes.push(detail.productPacking?.code);
      }
    });
    if (packingCodes.length > 0 && !this.isView) {
      const data = await this.merchantOrderHttpService.createGetMerchantOrderInventoryObs(packingCodes,
        [this.exportStatement?.fromStore?.code + '']).toPromise() as any[];
      for (const inventory of data) {
        const detail = uniqueDetailMap.get(inventory.productPackingId);
        detail.totalQuantity = Number(detail.quantity) + Number(inventory.totalQuantity);
        detail.maxQuantity = detail.totalQuantity;
      }
    }
    const dataTable = [...claimMap.values()];
    dataTable.push(...uniqueDetailMap.values());
    if (this.exportStatement) {
      this.exportStatement.exportStatementDetails = dataTable;
    }
  }

  async getInventoryAndUpdateDetail(packingCodes: any[], dataTable?: any[]) {
    if (this.inventoryAuthoritiesService.hasGetMerchantOrderInventory()) {
      const chosenStoreId = this.isEdit ? this.exportStatement?.fromStore?.id : this.addEditForm.get('fromStore')?.value;
      const inventoryData = await this.merchantOrderHttpService.createGetMerchantOrderInventoryObs(packingCodes,
        [this.fromStoresOptions.find(store => store.value === chosenStoreId)?.rawData.code]).toPromise() as any[];
      const inventoryMap = new Map<number, number>();
      inventoryData.forEach(inventory => {
        inventoryMap.set(inventory.productPackingId, inventory.totalQuantity);
      });

      dataTable?.forEach(row => {
        row.totalQuantity = inventoryMap.get(row.productPacking.id) ? inventoryMap.get(row.productPacking.id) : 0;
        row.maxQuantity = row.totalQuantity;
      });

    }
    return dataTable ? dataTable : [];
  }

  async getMerchantOrderFromGoodsBonus(merchantOrderIds: any) {
    let dataTable: any[] = [];
    let packingCodes: any;
    const list = await this.exportStatementV2Service.getMerchantOrderListFromApi(merchantOrderIds).toPromise() as any[];
    const map = new Map();
    for (const merchantOrder of list) {
      if (!map.has(merchantOrder.productPacking.id)) {
        map.set(merchantOrder.productPacking.id, merchantOrder);
      } else {
        const sumMerchantOrder = map.get(merchantOrder.productPacking.id);
        sumMerchantOrder.quantity += merchantOrder.quantity;
      }
    }

    for (const obj of map.values()) {
      if (obj.status === 0) {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(obj.productPacking.product);
        dataTable.push(obj);
      }
    }

    packingCodes = dataTable.map(row => row.productPacking.code);

    dataTable = await this.getInventoryAndUpdateDetail(packingCodes, dataTable);
    this.totalColSpan = CommonUtils.getTotalFooterColspanDynamic(this.columns, dataTable,
      ['weight', 'volume']);
    this.addEditForm.get('exportStatementDetails')?.setValue(dataTable);
  }


  async fromStoreChange(storeModel: SelectModel[]) {
    const store = storeModel[0]?.rawData as StoreModel;
    this.addEditForm.patchValue({
      fromStoreUsername: store?.user ? store?.user?.username + ' - ' + store?.user?.firstName + ' ' + store?.user?.lastName : '',
      fromStoreUserTel: store?.user?.tel ? store?.user?.tel : '',
    });
    if (!this.isGoodBonus && !this.isEdit) {
      this.addEditForm.patchValue({
        exportStatementDetails: [],
      });
    } else {
      const {exportStatementDetails} = this.addEditForm.value;
      this.addEditForm.get('exportStatementDetails')?.setValue([]);
      this.addEditForm.updateValueAndValidity();
      let exportStatementDetailsList = exportStatementDetails ? exportStatementDetails : [];
      const packingCodes = (exportStatementDetailsList as ExportStatementDetailModel[]).map(row => row.productPacking?.code);
      if (packingCodes.length) {
        exportStatementDetailsList = await this.getInventoryAndUpdateDetail(packingCodes, exportStatementDetailsList);
        this.addEditForm.get('exportStatementDetails')?.setValue([...exportStatementDetailsList]);
        this.addEditForm.updateValueAndValidity();
      } else {
        this.addEditForm.get('exportStatementDetails')?.setValue(exportStatementDetailsList);
        this.addEditForm.updateValueAndValidity();
      }

    }
    if (store) {
      this.filterStore('toStore', store.id);
    }
  }

  filterStore(changeType: any, exceptId: any) {
    if (!this.fromStoreForMerchantOrder) {
      this.exportStatementV2Service.createToStoreObservable(exceptId).toPromise().then(storeList => {
        if (changeType === 'toStore') {
          this.toStoresOptions = storeList.content.map((store: StoreModel) => {
            return new SelectModel(store.id, store.code + ' - ' + store.name, false, store);
          });
          if (!this.isEdit) {
            this.resetToStoreInformation();
          }
        }
      });
    }
  }

  resetToStoreInformation() {
    this.addEditForm.patchValue({
      toStore: '',
      toStoreUsername: '',
      toStoreUserTel: ''
    });
  }

  toStoreChange(storeModel: SelectModel[]) {
    const store = storeModel[0]?.rawData as StoreModel;
    this.addEditForm.patchValue({
      toStoreUsername: store?.user ? store?.user?.username + ' - ' + store?.user?.firstName + ' ' + store?.user?.lastName : '',
      toStoreUserTel: store?.user?.tel ? store?.user?.tel : '',
    });
  }

  onAddDetail() {
    const {fromStore} = this.addEditForm.value;
    this.loaderService.isLoading.next(true);
    const dialogRef = this.dialog.open(StoresInventoryIgnoreExpireDateComponent, {
      disableClose: false,
      width: '90%',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        storeId: fromStore,
        exceptProductPackingIds: this.getExceptProductPackingIds().join(', '),
        exportStatementId: 0
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('tabledataadd:', result);
      const oldRows = this.addEditForm.get('exportStatementDetails')?.value;
      const rows = this.convertToRows(result);
      this.addEditForm.get('exportStatementDetails')?.setValue([...(oldRows ? oldRows : []), ...rows]);
      this.totalColSpan = CommonUtils.getTotalFooterColspanDynamic(this.columns, this.addEditForm.get('exportStatementDetails')?.value,
        ['weight', 'volume']);
    });
  }

  convertToRows(data: any) {
    const rows = [];
    let row;
    if (data) {
      for (const item of Object.keys(data)) {
        ProductDescriptionsUtils.parseDisplayProductDescriptionsToName(data[item].productPacking.product);
        row = {
          quantity: 1,
          totalQuantity: data[item].totalQuantity,
          maxQuantity: data[item].totalQuantity,
          expireDate: data[item].expireDate,
          id: data[item].id,
          productPacking: data[item].productPacking,
          product: data[item].product
        };
        rows.push(row);
      }
    }
    return rows;
  }

  getExceptProductPackingIds() {
    const {exportStatementDetails} = this.addEditForm.value;
    const packingIds = [-1];
    if (exportStatementDetails) {
      exportStatementDetails.forEach((storeProductPacking: StoreProductPackingModel) => {
        if (storeProductPacking.productPacking?.id) {
          packingIds.push(storeProductPacking.productPacking.id);
        }
      });
    }
    return packingIds;
  }

  onRemoveDetail(row: ExportStatementDetailModel, index: number) {
    let exportStatementDetails = this.addEditForm.get('exportStatementDetails')?.value as ExportStatementDetailModel[];
    exportStatementDetails = exportStatementDetails ? exportStatementDetails : [];
    exportStatementDetails.splice(exportStatementDetails.indexOf(row), 1);
    this.addEditForm.get('exportStatementDetails')?.setValue(exportStatementDetails);
  }


  addEditExportStatement() {
    const {
      exportStatementDetails, code,
      fromStore, toStore, description, status, shippingPartner
    } = this.addEditForm.value;
    const dataTable: any[] = exportStatementDetails ? exportStatementDetails : [];
    let method;
    let msgConfirm = '';
    let msgSuccess = '';
    let msgParams: any[] = [];
    const rowIndex = [];
    let notCurrency = false;
    let currentAmount = 0;
    let index = 0;
    let notPrice;
    if ((this.maxAmountWarning || this.maxAmountWarning === 0) && this.currencyExportStatement) {
      for (const detail of dataTable) {
        index++;
        detail.product = detail.product ? detail.product : detail.productPacking.product;
        if (detail.product.productPackingPrices && detail.product.productPackingPrices.length > 0) {
          for (const packingPrice of detail.product.productPackingPrices) {
            if (packingPrice.currency.code.toLowerCase() === this.currencyExportStatement.toLowerCase()) {
              currentAmount = Number(currentAmount)
                + Number(detail.quantity) * (Number(packingPrice.price) * Number(detail.productPacking.packingType.quantity));
              notPrice = false;
              break;
            } else {
              notPrice = true;
            }
          }
          if (notPrice) {
            rowIndex.push(index);
            notCurrency = true;
          }

        } else {
          rowIndex.push(index);
          notCurrency = true;
        }
      }

      if (notCurrency) {
        msgParams.push(this.currencyExportStatement);
        this.exportStatementV2Service.showErrorMsg('export.statement.not.currency', rowIndex, msgParams);
        return;
      }
      if (currentAmount > Number(this.maxAmountWarning)) {
        msgConfirm = 'export-statement.msg.create.warning.max.amount';
        msgParams.push(Number(this.maxAmountWarning).toLocaleString('en-US'), this.currencyExportStatement);
      }
    }
    const exportStatementDetailList = dataTable.map(row => {
      return {
        price: row.price,
        productPackingId: row.productPacking.id,
        productPackingCode: row.productPacking.code,
        quantity: row.quantity,
      };
    }) as ExportStatementDetailModel[];
    if (!this.isEdit) {
      msgSuccess = '.add.success';
      if (this.merchantOrderIds && this.merchantOrderIds.length > 0) {
        const exportStatement = {
          fromStoreId: fromStore,
          toStoreId: toStore,
          description: description ? description : '',
          merchantCode: null,
          merchantOrderId: null,
          merchantOrderIds: this.merchantOrderIds,
          status: ExportStatementStatus.REQUESTED,
          shippingPartner: new PartnerModel(shippingPartner),
          exportStatementDetails: exportStatementDetailList
        };
        method = this.apiService.post('/export-statements/by-store-demand', exportStatement);
      } else {
        const exportStatement: Partial<ExportStatementModel> = {
          code,
          fromStore: fromStore ? new StoreModel(fromStore) : undefined,
          fromStoreId: fromStore,
          toStore: toStore ? new StoreModel(toStore) : undefined,
          toStoreId: toStore,
          description: description ? description : '',
          merchantCode: null,
          merchantOrderId: null,
          merchantOrderIds: [] as number[],
          status: ExportStatementStatus.REQUESTED,
          shippingPartner: new PartnerModel(shippingPartner),
          exportStatementDetails: exportStatementDetailList
        };
        method = this.apiService.post('/export-statements', [exportStatement]);
      }
    } else {
      msgSuccess = '.edit.success';
      const exportStatement: ExportStatementModel = {
        code,
        fromStore: fromStore ? new StoreModel(fromStore) : undefined,
        fromStoreId: fromStore,
        // fromStoreCode: (this.fromStoresOptions.find(store => store.value === fromStore)?.rawData as StoreModel).code + '',
        toStore: toStore ? new StoreModel(toStore) : undefined,
        toStoreId: toStore,
        // toStoreCode: (this.toStoresOptions.find(store => store.value === toStore)?.rawData as StoreModel).code + '',
        description,
        merchantCode: null,
        merchantOrderId: null,
        merchantOrderIds: this.merchantOrderIds,
        status,
        shippingPartner: new PartnerModel(shippingPartner),
        exportStatementDetails: exportStatementDetailList
      };
      method = this.apiService.patch('/export-statements/' + this.exportStatement?.id, exportStatement);
    }
    msgParams = msgParams.length ? msgParams : ['export.statement.param'];
    msgConfirm = msgConfirm ? msgConfirm : 'common.confirmSave';
    this.utilsService.execute(method, this.saveSuccess1, msgSuccess, msgConfirm, msgParams);
  }

  saveSuccess1 = (data: any, msg: string | undefined) => {
    this.utilsService.onSuccessFunc(msg);
    if (!this.isEdit) {
      this.router.navigate(['/export-statement/edit/' + (data[0] ? data[0].id : data.id)]).then();
    }
  }

  onBack() {
    this.location.back();
  }

  updateLabel(event: any) {
    switch (event.label) {
      case 'menu.export.statement.update':
      case 'menu.export.statement.view':
        event.label = this.exportStatement?.code;
        break;
    }
  }
}
