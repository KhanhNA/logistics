import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService, AuthoritiesService, BaseSearchLayout,
  ButtonFields,
  ColumnFields, ColumnTypes, DateUtilService,
  FormStateService, IconTypeEnum, Page,
  SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ExportStatementModel} from '../../_models/export-statement/export.statement.model';
import {ExportStatementStatusEnum} from '../../_models/enums/ExportStatementStatusEnum';
import {HttpParams} from '@angular/common/http';
import {StoreModel} from '../../_models/store.model';
import {ExportStatementAuthoritiesService} from '../../_services/authority/export.statement.authorities.service';
import {ExportStatementV2Service} from "./export-statement-v2.service";

@Component({
  selector: 'app-export-statement-v2',
  templateUrl: './export-statement-v2.component.html',
  styleUrls: ['./export-statement-v2.component.scss']
})
export class ExportStatementV2Component extends BaseSearchLayout implements OnInit {
  moduleName = 'export.statement';
  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  toStoreValues: SelectModel[] = [];
  createDateMaxValue: Date = this.dateUtilService.getDateNow();
  statusValues: SelectModel[] = [];

  constructor(protected formBuilder: FormBuilder, protected router: Router, protected apiService: ApiService,
              protected utilsService: UtilsService, protected translateService: TranslateService,
              protected uiStateService: FormStateService, protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              public exportStatementAuthoritiesService: ExportStatementAuthoritiesService,
              public exportStatementV2Service: ExportStatementV2Service,
              private dateUtilService: DateUtilService) {
    super(router, apiService, utilsService, uiStateService, translateService,
      injector, activatedRoute, authoritiesService, formBuilder.group({
        code: [''],
        toStore: [''],
        createDate: [dateUtilService.convertDateToStringCurrentGMT(dateUtilService.getDateNow())],
        status: ['_0'],
      }));
    this.columns.push(
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'code',
        header: 'code',
        title: (e: ExportStatementModel) => `${e.code}`,
        cell: (e: ExportStatementModel) => `${e.code}`,
      },
      {
        columnDef: 'qrCode',
        header: 'qrCode',
        title: (e: ExportStatementModel) => `${e.code}`,
        cell: (e: ExportStatementModel) => `${e.qRCode}`,
        columnType: ColumnTypes.BASE64,
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'fromStore',
        header: 'fromStore',
        title: (e: ExportStatementModel) => `${e.fromStore?.code} - ${e.fromStore?.name}`,
        cell: (e: ExportStatementModel) => `${e.fromStore?.code} - ${e.fromStore?.name}`,
      },
      {
        columnDef: 'toStore',
        header: 'toStore',
        title: (e: ExportStatementModel) => e.toStore ? `${e.toStore.code} - ${e.toStore.name}` : '',
        cell: (e: ExportStatementModel) => e.toStore ? `${e.toStore.code} - ${e.toStore.name}` : '',
      },
      {
        columnDef: 'estimatedTimeOfArrival',
        header: 'estimatedTimeOfArrival',
        title: (e: ExportStatementModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.estimatedTimeOfArrival ? e.estimatedTimeOfArrival : '')}`,
        cell: (e: ExportStatementModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.estimatedTimeOfArrival ? e.estimatedTimeOfArrival : '')}`,
      },
      {
        columnDef: 'description',
        header: 'description',
        title: (e: ExportStatementModel) => `${e.description ? e.description : ''}`,
        cell: (e: ExportStatementModel) => `${e.description ? e.description : ''}`,
      },
      {
        columnDef: 'exportDate',
        header: 'exportDate',
        title: (e: ExportStatementModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.exportDate ? e.exportDate : '')}`,
        cell: (e: ExportStatementModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.exportDate ? e.exportDate : '')}`,
        display: (e: ExportStatementModel) => !!e && !!e.exportDate && this.authoritiesService.hasAuthority('post/import-pos')
      },
    );
    this.buttons.push(
      {
        columnDef: 'edit',
        icon: 'fa fa-edit',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'primary',
        click: 'addOrEdit',
        title: 'common.title.edit',
        isShowHeader: this.exportStatementAuthoritiesService.hasUpdateExportStatementRole(),
        display: (e: ExportStatementModel) => e && this.exportStatementAuthoritiesService.hasUpdateExportStatementRole(),
        disabled: (e: ExportStatementModel) => !e || !!e.merchantOrder || !!e.merchantCode
          || UtilsService.getEnumValue(ExportStatementStatusEnum, `${e.status}`) !== ExportStatementStatusEnum._0,
        header: 'common.action'/*{
          columnDef: 'add',
          color: 'warn',
          icon: 'add',
          click: 'addOrEdit',
          title: 'common.title.add',
          display: (e: ExportStatementModel) => !e && this.authoritiesService.hasAuthority('post/export-statements'),
          disabled: (e: ExportStatementModel) => !!e
        }*/,
      },
      {
        columnDef: 'print',
        icon: 'fa fa-print',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'info',
        click: 'print',
        title: 'common.title.print',
        header: 'common.action',
        display: (e: ExportStatementModel) => e && this.exportStatementAuthoritiesService.hasPrintExportStatementRole(),
        disabled: (e: ExportStatementModel) => !e ||
          UtilsService.getEnumValue(ExportStatementStatusEnum, `${e.status}`) !== ExportStatementStatusEnum._0
      },
      {
        columnDef: 'printPdf',
        icon: 'fa fa-file-pdf',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'info',
        click: 'printPdf',
        header: 'common.action',
        title: 'common.title.picture_as_pdf',
        display: (e: ExportStatementModel) => e && this.exportStatementAuthoritiesService.hasPrintPdfRole(),
        disabled: (e: ExportStatementModel) => !e ||
          UtilsService.getEnumValue(ExportStatementStatusEnum, `${e.status}`) !== ExportStatementStatusEnum._0
      },
      {
        columnDef: 'cancel',
        icon: 'fa fa-times',
        click: 'cancel',
        iconType: IconTypeEnum.FONT_AWESOME,
        className: 'danger',
        title: 'common.title.cancel',
        display: (e: ExportStatementModel) => e && this.exportStatementAuthoritiesService.hasCancelExportStatementRole(),
        disabled: (e: ExportStatementModel) => !e || !!e.merchantOrder || !!e.merchantCode
          || UtilsService.getEnumValue(ExportStatementStatusEnum, `${e.status}`) !== ExportStatementStatusEnum._0
      },
      {
        columnDef: 'view',
        title: 'common.title.dashboard',
        icon: 'fa fa-eye',
        iconType: IconTypeEnum.FONT_AWESOME,
        click: 'onView',
        className: 'info',
        display: () => true,
      }
    );
  }

  async ngOnInit() {
    Object.keys(ExportStatementStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(ExportStatementStatusEnum, key.replace('_', ''));
      this.statusValues.push(new SelectModel(key, value));
    });
    // const params = new HttpParams()
    //   .set('text', '').set('status', 'true')
    //   .set('exceptIds', '-1').set('is_DC', '')
    //   .set('ignoreCheckPermission', 'true')
    //   .set('pageNumber', '1').set('pageSize', '9999');
    // const stores = await this.apiService.get<Page>('/stores', params).toPromise()
    const fromStoresPromise = this.exportStatementV2Service.createFromStoreObservable().toPromise();
    const toStoresPromise = this.exportStatementV2Service.createToStoreObservable(undefined).toPromise();
    const fromStores = await fromStoresPromise;
    const toStores = await toStoresPromise;
    const fromStoreIds = fromStores.content.map(store => store.id);
    this.toStoreValues = [];
    const storeModels = toStores.content as StoreModel[];
    this.toStoreValues.push(new SelectModel(' ', 'common.status.all'));
    storeModels.forEach((store: StoreModel) => {
      if (fromStoreIds.length === 1) {
        if (fromStoreIds[0] !== store.id) {
          this.toStoreValues.push(new SelectModel(store.id, store.code + ' - ' + store.name));
        }
      } else {
        this.toStoreValues.push(new SelectModel(store.id, store.code + ' - ' + store.name));
      }

    });

    if (this.toStoreValues && this.toStoreValues.length > 1) {
      this.searchForm.get('toStore')?.setValue(this.toStoreValues[0].value);
    }

    super.ngOnInit();
    this.onSubmit();
  }

  search() {
    const status = this.searchForm.get('status')?.value;
    const params = new HttpParams()
      .set('code', this.searchForm.get('code')?.value)
      .set('createDate', this.searchForm.get('createDate')?.value)
      .set('toStoreId', this.searchForm.get('toStore')?.value.toString().trim())
      .set('type', 'from')
      .set('isExportSo', 'false')
      .set('status', `${status ? status : ''}`.replace('_', ''));
    this._fillData('/export-statements', params);
  }

  // onRowClick(row: ExportStatementModel) {
  //   this.router.navigate(['/export-statement/dashboard/', row.id]).then();
  // }
  onView(model: ExportStatementModel, index: number) {
    this.router.navigate(['/export-statement/dashboard/', model.id]).then();
  }

  addOrEdit(row: ExportStatementModel) {
    if (row && row.id) {
      this.router.navigate(['/export-statement/edit', row.id]).then();
    } else {
      this.router.navigate(['/export-statement/add']).then();
    }
  }

  print(row: ExportStatementModel) {
    this.apiService.saveFile('/export-statements/print/' + row.id, null, {
      headers: undefined,
      params: undefined
    });
  }

  printPdf(row: ExportStatementModel) {
    this.apiService.saveFile('/export-statements/print-pdf/' + row.id, null, {
      headers: undefined,
      params: undefined
    });
  }

  cancel(row: ExportStatementModel) {
    const apiCall = this.apiService.post(`/export-statements/cancel/${row.id}`, null);
    this.utilsService.execute(apiCall, this.onSuccessFunc, '.cancel.success', 'common.confirmCancel', ['common.export']);
  }

  onAddNewPO() {
    this.router.navigate(['/export-statement/add']).then();
  }

  onResetForm() {
    this.searchForm.reset();
    this.searchForm.patchValue({
      code: '',
      toStore: '',
      createDate: '',
      status: '_0',
    });

    // this.setUpDefaultTime();
  }
}
