import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportStatementV2Component } from './export-statement-v2.component';

describe('ExportStatementV2Component', () => {
  let component: ExportStatementV2Component;
  let fixture: ComponentFixture<ExportStatementV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExportStatementV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportStatementV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
