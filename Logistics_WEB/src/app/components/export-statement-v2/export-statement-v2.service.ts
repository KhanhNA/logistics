import { Injectable } from '@angular/core';
import {ApiService, Page, UtilsService} from "@next-solutions/next-solutions-base";
import {TranslateService} from "@ngx-translate/core";
import {ProductModel} from "../../_models/product.model";
import {ExportStatementDetailModel} from "../../_models/export-statement/export.statement.detail.model";
import {Observable} from "rxjs";
import {ExportStatementModel} from "../../_models/export-statement/export.statement.model";
import {HttpParams} from "@angular/common/http";
import {ConfigEnum} from "../../_models/enums/config.enum";
import {ProductPackingModel} from "../../_models/product.packing.model";
import {CommonUtils} from "../../_utils/common.utils";

@Injectable()
export class ExportStatementV2Service {

  constructor(private apiService: ApiService, private translateService: TranslateService,
              private utilsService: UtilsService) {
  }


  static getVolumeProduct(product: ProductModel | ProductPackingModel | undefined, e: ExportStatementDetailModel) {
    if (!product || !product.height || !product.length || !product.width) {
      e.volume = 0;
    } else {
      e.volume = product.width * product.length * product.height * (e.quantity ? e.quantity : 0);
    }
    return Number(e.volume);
  }

  static getWeightProduct(product: ProductModel | ProductPackingModel | undefined, e: ExportStatementDetailModel) {
    if (!product || !product.weight) {
      e.weight = 0;
    } else {
      e.weight = product.weight * (e.quantity ? e.quantity : 0);
    }
    return Number(e.weight);
  }

  showErrorMsg = (msgKey: string, rowIndex?: any[], msgParams?: any[]) => {
    let msg = this.translateService.instant(msgKey);
    if (msgParams) {
      msg = this.utilsService.strFormat(msgKey, msgParams);
    }

    if (rowIndex && rowIndex.length > 0) {
      msg = '' + msg + rowIndex.join(', ');
    }
    this.utilsService.showErrorToarst(msg);
  }

  getMerchantOrderListFromApi(merchantOrderIds: any): Observable<any> {
    const params = new HttpParams().set('merchantOrderIds', merchantOrderIds.join(','));
    return  this.apiService.get('/merchant-orders/details', params);
  }

  getExportStatement(id: number | undefined): Observable<ExportStatementModel> {
    return this.apiService.get('/export-statements/' + id, new HttpParams());
  }

  getConfigExportStatement(): Observable<any> {
    return this.apiService.get('/configs', new HttpParams().set('code', ConfigEnum.EXPORT_STATEMENT_WARNING));
  }

  createFromStoreObservable(): Observable<Page> {
    const params = new HttpParams()
      .set('status', 'true')
      // .set('text', '')
      .set('exceptIds', '-1')
      .set('ignoreCheckPermission', 'false')
      // .set('is_DC', '')
      .set('pageNumber', '1')
      .set('pageSize', '' + CommonUtils.MAX_PAGE_SIZE);
    return this.apiService.get<Page>('/stores', params);
  }

  createToStoreObservable(exceptId: any): Observable<Page> {
    const ids = [];
    ids.push('-1');
    if (exceptId) {
      ids.push('' + exceptId);
    }
    let params = new HttpParams()
      .set('status', 'true')
      .set('text', '')
      .set('ignoreCheckPermission', 'true')
      .set('is_DC', '')
      .set('pageNumber', '1')
      .set('pageSize', '' + CommonUtils.MAX_PAGE_SIZE);
    console.log(ids);
    ids.forEach(id => {
      params = params.append('exceptIds', id);
    });
    console.log(params);
    return this.apiService.get<Page>('/stores', params);
  }
}
