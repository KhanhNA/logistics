import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExportStatementV2Component} from './export-statement-v2.component';
import {ExportStatementV2Service} from './export-statement-v2.service';
import {SharedModule} from '../../modules/shared.module';
import {AuthoritiesResolverService, MultiTranslateHttpLoader} from '@next-solutions/next-solutions-base';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {RouterModule, Routes} from "@angular/router";
import {LDExportStatementComponent} from "../export-statement/list-export-statement/l-d-export-statement.component";
import {AddExportStatementComponent} from "../export-statement/add-export-statement/add-export-statement.component";
import {EditExportStatementComponent} from "../export-statement/edit-export-statement/edit-export-statement.component";
import {AddEditExportStatementV2Component} from "./add-edit-export-statement-v2/add-edit-export-statement-v2.component";
import {ExportStatementAuthoritiesService} from "../../_services/authority/export.statement.authorities.service";
import {ClaimService} from "../../_services/http/claim/claim.service";
import {ClaimAuthoritiesService} from "../../_services/authority/claim.authorities.service";
import {InventoryAuthoritiesService} from "../../_services/authority/inventory.authorities.service";
import {MerchantOrderHttpService} from "../../_services/http/merchant.order.http.service";
import {ShippingPartnerHttpService} from "../../_services/http/shipping.partner.http.service";
import {ClaimSharedComponent} from "../../modules/claim.shared.component";

export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/', suffix: '.json'},
    {prefix: './assets/i18n/export-statement/', suffix: '.json'},
  ]);
}

const routes: Routes = [
  {
    path: '',
    component: ExportStatementV2Component,
    pathMatch: 'full',
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'add',
    component: AddEditExportStatementV2Component,
    data: {breadcrumb: 'menu.export.statement.create'},
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'edit/:id',
    component: AddEditExportStatementV2Component,
    data: {breadcrumb: 'menu.export.statement.update'},
    resolve: {me: AuthoritiesResolverService}
  },
  {
    path: 'dashboard/:id',
    component: AddEditExportStatementV2Component,
    data: {breadcrumb: 'menu.export.statement.view', isView: true},
    resolve: {me: AuthoritiesResolverService}
  },
]

@NgModule({
  declarations: [
    ExportStatementV2Component,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      isolate: true,
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ExportStatementV2Service,
    ExportStatementAuthoritiesService,
    ClaimService,
    ClaimAuthoritiesService,
    InventoryAuthoritiesService,
    MerchantOrderHttpService,
    ShippingPartnerHttpService
  ]
})
export class ExportStatementV2Module {
}
