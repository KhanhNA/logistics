import { TestBed } from '@angular/core/testing';

import { ExportStatementV2Service } from './export-statement-v2.service';

describe('ExportStatementV2Service', () => {
  let service: ExportStatementV2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExportStatementV2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
