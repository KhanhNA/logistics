import {Component, Injector, OnInit} from '@angular/core';
import {
  AlignEnum,
  ApiService,
  AuthoritiesService,
  BaseSearchLayout, ButtonFields, ColumnFields, DateUtilService,
  FormStateService, SelectModel,
  UtilsService
} from '@next-solutions/next-solutions-base';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ProductListModel} from '../../_models/product.list.model';
import {ClaimTypeEnum} from '../../_models/enums/ClaimTypeEnum';
import {ProductListStatusEnum} from '../../_models/enums/product.list.status.enum';
import {HttpParams} from '@angular/common/http';
import {StoreModel} from '../../_models/store.model';
import {CommonUtils} from "../../_utils/common.utils";
import {CommonHttpService} from "../../_services/http/common/common.http.service";

@Component({
  selector: 'app-damaged-goods',
  templateUrl: './damaged-goods.component.html',
  styleUrls: ['./damaged-goods.component.css']
})
export class DamagedGoodsComponent extends BaseSearchLayout implements OnInit {

  moduleName = 'damaged-goods';

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];

  statusValues: SelectModel[] = [];
  typeValues: SelectModel[] = [];
  storeValues: SelectModel[] = [];
  stores: any[] = [];

  constructor(protected formBuilder: FormBuilder,
              protected router: Router,
              protected apiService: ApiService,
              protected utilsService: UtilsService,
              protected formStateService: FormStateService,
              protected translateService: TranslateService,
              protected injector: Injector,
              protected activatedRoute: ActivatedRoute,
              protected authoritiesService: AuthoritiesService,
              protected commonHttpService: CommonHttpService,
              private dateUtilService: DateUtilService) {
    super(router, apiService, utilsService, formStateService, translateService, injector, activatedRoute, authoritiesService,
      formBuilder.group({
        amenable: [''],
        type: ['_'],
        productPackingCode: [''],
        productName: [''],
        store: [''],
        status: ['_APPROVED'],
        fromCreateDate: [''],
        toCreateDate: [''],
      }));
    this.columns.push({
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
        align: AlignEnum.CENTER
      },
      {
        columnDef: 'storeId', header: 'storeId', title: (e: ProductListModel) => `${e.storeCode} - ${e.storeName}`,
        cell: (e: ProductListModel) => `${e.storeCode} - ${e.storeName}`,
        className: 'mat-column-storeId'
      },
      {
        columnDef: 'packingCode', header: 'packingCode', title: (e: ProductListModel) => `${e.productPackingCode}`,
        cell: (e: ProductListModel) => `${e.productPackingCode}`,
        className: 'mat-column-packingCode'
      },
      {
        columnDef: 'productName', header: 'productName', title: (e: ProductListModel) => `${e.productPackingName}`,
        cell: (e: ProductListModel) => `${e.productPackingName}`,
        className: 'mat-column-productName'
      },
      {
        columnDef: 'type', header: 'type', title: (e: ProductListModel) => `${e.claimType}`,
        cell: (e: ProductListModel) => `${e.claimType}`,
        className: 'mat-column-type',
        display: (_: any) => !!this.searchForm.value.type.replace('_', '')
      },
      {
        columnDef: 'amenable', header: 'amenable', title: (e: ProductListModel) => `${e.amenable}`,
        cell: (e: ProductListModel) => `${e.amenable}`,
        className: 'mat-column-amenable',
        display: (_: any) => {
          return !!this.searchForm.get('amenable')?.value;
        }
      },
      {
        columnDef: 'quantity', header: 'quantity', title: (e: ProductListModel) => `${e.quantity}`,
        cell: (e: ProductListModel) => `${e.quantity}`,
        className: 'mat-column-quantity'
      },
      {
        columnDef: 'expireDate',
        header: 'expireDate',
        title: (e: ProductListModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.expireDate + '')}`,
        cell: (e: ProductListModel) => `${this.dateUtilService.convertDateToDisplayGMT0(e.expireDate + '')}`,
        className: 'mat-column-expireDate'
      });
  }

  async ngOnInit() {

    Object.keys(ProductListStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(ProductListStatusEnum, key.replace('_', ''));
      this.statusValues.push(new SelectModel(key, value));
    });

    Object.keys(ClaimTypeEnum).forEach(key => {
      const value = UtilsService.getEnumValue(ClaimTypeEnum, key.replace('_', ''));
      this.typeValues.push(new SelectModel(key, value));
    });
    this.getDataSelectModel();
  }

  async getDataSelectModel() {
    // const params = new HttpParams()
    //   .set('status', 'true')
    //   .set('text', '')
    //   .set('exceptIds', '-1')
    //   .set('ignoreCheckPermission', 'false')
    //   .set('is_DC', '')
    //   .set('pageNumber', '1')
    //   .set('pageSize', '' + 9999);
    // this.apiService.get('/stores', params).subscribe((response: any) => {
    const response = await this.commonHttpService.createGetAllStoreAvailableManagementObservable().toPromise();
    this.storeValues = [];
    this.stores.push(...response.content);
    const storeIds: number[] = [];
    this.stores.forEach((store: StoreModel) => {
      if (store && store.id) {
        storeIds.push(store.id);
      }
      this.storeValues.push(new SelectModel(store.id, store.code + ' - ' + store.name));
    });

    if (!CommonUtils.isEmptyList(this.storeValues)) {
      this.searchForm.patchValue({
        store: this.storeValues[0].value
      });
    }
    super.ngOnInit();

    this.onSubmit();
    // const par = new HttpParams().append('storeIds', storeIds.join(', '));
    // this.apiService.get<StoreModel[]>('/stores/by-provide-store', par).subscribe((provideStores: StoreModel[]) => {
    //   this.storeValues = [...this.storeValues];
    //   provideStores.forEach(store => {
    //     this.storeValues.push(new SelectModel(store.id, store.code + ' - ' + store.name));
    //   });
    //   if (this.storeValues.length > 0) {
    //     this.searchForm.patchValue({
    //       store: this.storeValues[0].value
    //     });
    //   }
    //   super.ngOnInit();
    //
    //   this.onSubmit();
    // });
    // });
  }

  search(): void {
    const status = this.searchForm.get('status')?.value;
    const type = this.searchForm.get('type')?.value;
    const params = new HttpParams()
      .set('fromCreateDate', this.searchForm.get('fromCreateDate')?.value)
      .set('toCreateDate', this.searchForm.get('toCreateDate')?.value)
      .set('amenable', this.searchForm.get('amenable')?.value)
      .set('storeId', this.searchForm.get('store')?.value)
      .set('type', `${type}`.replace('_', ''))
      .set('productPackingCode', this.searchForm.get('productPackingCode')?.value)
      .set('productName', this.searchForm.get('productName')?.value)
      .set('status', `${status}`.replace('_', ''));
    this._fillData('/claims/list-damaged-goods', params);
  }

}
