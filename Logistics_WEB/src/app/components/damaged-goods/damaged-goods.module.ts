import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DamagedGoodsComponent} from './damaged-goods.component';
import {SharedModule} from '../../modules/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {createTranslateLoader} from '../export-statement-v2/export-statement-v2.module';
import {AuthoritiesResolverService} from '@next-solutions/next-solutions-base';

const routes: Routes = [
  {
    path: '',
    component: DamagedGoodsComponent,
    pathMatch: 'full',
    resolve: {me: AuthoritiesResolverService}
  },
];


@NgModule({
  declarations: [DamagedGoodsComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      isolate: true,
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ]
})
export class DamagedGoodsModule {
}
