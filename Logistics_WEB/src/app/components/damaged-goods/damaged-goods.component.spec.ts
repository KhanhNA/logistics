import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DamagedGoodsComponent } from './damaged-goods.component';

describe('ProductListComponent', () => {
  let component: DamagedGoodsComponent;
  let fixture: ComponentFixture<DamagedGoodsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DamagedGoodsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DamagedGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
