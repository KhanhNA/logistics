package com.tsolution._1entities.dcommerce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.collections4.map.HashedMap;

import com.tsolution._1entities.LanguageEntity;
import com.tsolution._1entities.ProductPackingUnitDescriptionEntity;
import com.tsolution._1entities.ProductPackingUnitEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution.excetions.BusinessException;
import com.tsolution.tasks.Convertable;
import com.tsolution.utils.StringUtils;

public class AppUnitDescPureDto extends FilterDto implements Serializable, Convertable<ProductPackingUnitEntity> {
	/**
	 *
	 */
	private static final long serialVersionUID = -3651228574472741136L;
	private Long id;
	private String name;
	private Long languageId;
	private Long appUnitId;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getLanguageId() {
		return this.languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	public Long getAppUnitId() {
		return this.appUnitId;
	}

	public void setAppUnitId(Long appUnitId) {
		this.appUnitId = appUnitId;
	}

	@Override
	public Long getTId() {
		return this.id;
	}

	private boolean isValid() {
		return !StringUtils.isNullOrEmpty(this.name) && (this.languageId != null) && (this.appUnitId != null);
	}

	@Override
	public ProductPackingUnitEntity convert(SuperEntity superEntity, EntityManager em) throws BusinessException {
		if (!this.isValid()) {
			return null;
		}
		ProductPackingUnitEntity productPackingUnit = new ProductPackingUnitEntity();
		if (superEntity == null) {
			String query = String.format("INSERT INTO product_packing_unit(id, create_date) VALUES (%d, SYSDATE())",
					this.appUnitId);
			BaseRepository.executeCUD(em, query, new HashedMap<>());
		}
		productPackingUnit.setId(superEntity == null ? this.appUnitId : superEntity.getId());

		ProductPackingUnitEntity temp = null;
		if (superEntity instanceof ProductPackingUnitEntity) {
			temp = (ProductPackingUnitEntity) superEntity;
		}
		productPackingUnit
				.setProductPackingUnitDescriptions(this.getProductPackingUnitDescriptions(temp, productPackingUnit));
		return productPackingUnit;
	}

	private List<ProductPackingUnitDescriptionEntity> getProductPackingUnitDescriptions(ProductPackingUnitEntity temp,
			ProductPackingUnitEntity productPackingUnit) {
		List<ProductPackingUnitDescriptionEntity> productPackingUnitDescriptions = temp == null ? new ArrayList<>()
				: temp.getProductPackingUnitDescriptions();
		boolean isExists = false;
		for (ProductPackingUnitDescriptionEntity productPackingUnitDescription : productPackingUnitDescriptions) {
			if (productPackingUnitDescription.getLanguage().getId().equals(this.languageId)) {
				isExists = true;
				productPackingUnitDescription.setName(this.name);
				break;
			}
		}
		if (!isExists) {
			ProductPackingUnitDescriptionEntity productPackingUnitDescription = new ProductPackingUnitDescriptionEntity();
			productPackingUnitDescription.setName(this.name);
			productPackingUnitDescription.setLanguage(new LanguageEntity(this.languageId));
			productPackingUnitDescription.setProductPackingUnit(temp == null ? productPackingUnit : temp);
			productPackingUnitDescriptions.add(productPackingUnitDescription);
		}
		return productPackingUnitDescriptions;
	}

	@Override
	public String getSql() {
		return String.format("SELECT ppu.* FROM product_packing_unit ppu WHERE ppu.id = %d ", this.appUnitId);
	}

}
