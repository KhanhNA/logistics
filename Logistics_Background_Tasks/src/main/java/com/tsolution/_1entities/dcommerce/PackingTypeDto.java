package com.tsolution._1entities.dcommerce;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.tsolution._1entities.PackingTypeEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.tasks.Convertable;

public class PackingTypeDto extends FilterDto implements Serializable, Convertable<PackingTypeEntity> {
	/**
	 *
	 */
	private static final long serialVersionUID = -2304785979842483967L;
	private Long packingTypeId;
	private String code;
	private Integer quantity;

	public Long getPackingTypeId() {
		return this.packingTypeId;
	}

	public void setPackingTypeId(Long packingTypeId) {
		this.packingTypeId = packingTypeId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public Long getTId() {
		return this.packingTypeId;
	}

	@Override
	public PackingTypeEntity convert(SuperEntity superEntity, EntityManager em) {
		PackingTypeEntity packingTypeEntity = new PackingTypeEntity();
		if ((superEntity != null) && (superEntity.getId() != null)) {
			packingTypeEntity.setId(superEntity.getId());
		}
		packingTypeEntity.setCode(this.code);
		packingTypeEntity.setQuantity(this.quantity);
		packingTypeEntity.setStatus(true);
		return packingTypeEntity;
	}

	@Override
	public String getSql() {
		return String.format("SELECT pt.* FROM packing_type pt WHERE pt.code = '%s'", this.code);
	}
}
