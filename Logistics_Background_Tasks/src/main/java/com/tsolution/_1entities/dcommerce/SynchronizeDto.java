package com.tsolution._1entities.dcommerce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SynchronizeDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 8107788975808005351L;
	private String entityName;
	private List<Long> ids;

	public SynchronizeDto() {
		this.ids = new ArrayList<>();
	}

	public String getEntityName() {
		return this.entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public List<Long> getIds() {
		return this.ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

}
