package com.tsolution._1entities.dcommerce;

import java.io.Serializable;

public class ProductDescriptionDto extends FilterDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -3511165444536461221L;
	/**
	 *
	 */
	private String productName;
	private Long languageId;

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getLanguageId() {
		return this.languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

}
