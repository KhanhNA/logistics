package com.tsolution._1entities.dcommerce;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution.excetions.BusinessException;
import com.tsolution.tasks.Convertable;
import com.tsolution.utils.Constants;
import com.tsolution.utils.DatetimeUtils;
import com.tsolution.utils.DcommerceDeserializeDateHandler;
import com.tsolution.utils.SerializeDateHandler;
import com.tsolution.utils.StringUtils;

public class ProductPackingPriceDto extends FilterDto implements Serializable, Convertable<ProductPackingPriceEntity> {

	private static final Logger log = LogManager.getLogger(ProductPackingPriceDto.class);
	private static final long serialVersionUID = -4271451498861590490L;
	private Long id;
	private Long packingProductId;
	private ProductPackingDto packingProduct;
	private BigDecimal retailPrice;
	private BigDecimal price;

	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonDeserialize(using = DcommerceDeserializeDateHandler.class)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	private LocalDate fromDate;

	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonDeserialize(using = DcommerceDeserializeDateHandler.class)
	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	private LocalDate toDate;

	private Integer priceType;
	private String currencyCode;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPackingProductId() {
		return this.packingProductId;
	}

	public void setPackingProductId(Long packingProductId) {
		this.packingProductId = packingProductId;
	}

	public ProductPackingDto getPackingProduct() {
		return this.packingProduct;
	}

	public void setPackingProduct(ProductPackingDto packingProduct) {
		this.packingProduct = packingProduct;
	}

	public BigDecimal getRetailPrice() {
		return this.retailPrice;
	}

	public void setRetailPrice(BigDecimal retailPrice) {
		this.retailPrice = retailPrice;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public LocalDate getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return this.toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public Integer getPriceType() {
		return this.priceType;
	}

	public void setPriceType(Integer priceType) {
		this.priceType = priceType;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	@Override
	public Long getTId() {
		return this.id;
	}

	@Override
	public ProductPackingPriceEntity convert(SuperEntity superEntity, EntityManager em) throws BusinessException {
		this.fromDate = this.getFromDate(em);
		if ((this.packingProduct == null) || StringUtils.isNullOrEmpty(this.packingProduct.getCode())
				|| (this.priceType == null) || (this.fromDate == null)
				|| StringUtils.isNullOrEmpty(this.currencyCode)) {
			ProductPackingPriceDto.log
					.info("this.packingProduct == null || StringUtils.isNullOrEmpty(this.packingProduct.getCode())");
			ProductPackingPriceDto.log.info("this.priceType == null || this.fromDate == null");
			ProductPackingPriceDto.log.info("StringUtils.isNullOrEmpty(this.currencyCode)");
			return null;
		}
		if (this.priceType != 1) {
			return null;
		}
		ProductPackingPriceEntity productPackingPrice = new ProductPackingPriceEntity();
		if ((superEntity != null) && (superEntity.getId() != null)) {
			productPackingPrice.setId(superEntity.getId());
		}

		ProductPackingPriceEntity temp = null;
		if (superEntity instanceof ProductPackingPriceEntity) {
			ProductPackingPriceDto.log.info("superEntity instanceof ProductPackingPriceEntity");
			temp = (ProductPackingPriceEntity) superEntity;
		}

		productPackingPrice.setPrice(this.getPriceByType());
		productPackingPrice.setSaleType(this.getSaleType());
		productPackingPrice.setFromDate(this.fromDate.atStartOfDay());
		productPackingPrice.setToDate(this.toDate == null ? null : this.toDate.atStartOfDay());
		productPackingPrice.setStatus(true);

		if ((temp == null) || !temp.getCurrency().getCode().equals(this.currencyCode)) {
			String query = String.format("SELECT c.* FROM currency c WHERE c.code = '%s'", this.currencyCode);
			CurrencyEntity currencyEntity = BaseRepository.getFirstResultNativeQuery(em, query,
					new HashMap<String, Object>(), CurrencyEntity.class);
			productPackingPrice.setCurrency(currencyEntity);
		} else {
			productPackingPrice.setCurrency(temp.getCurrency());
		}
		ProductPackingPriceDto.log.info("productPackingPrice.getCurrency() == null ({})",
				productPackingPrice.getCurrency() == null);
		if ((temp == null) || !temp.getProductPacking().getCode().equals(this.packingProduct.getCode())) {
			String query = String.format("SELECT pp.* FROM product_packing pp WHERE pp.code = '%s'",
					this.packingProduct.getCode());
			ProductPackingEntity productPackingEntity = BaseRepository.getFirstResultNativeQuery(em, query,
					new HashMap<String, Object>(), ProductPackingEntity.class);
			productPackingPrice.setProductPacking(productPackingEntity);
		} else {
			productPackingPrice.setProductPacking(temp.getProductPacking());
		}
		ProductPackingPriceDto.log.info("productPackingPrice.getProductPacking() == null ({})",
				productPackingPrice.getProductPacking() == null);
		if (productPackingPrice.getProductPacking() == null) {
			return null;
		}
		productPackingPrice.setProduct(productPackingPrice.getProductPacking().getProduct());
		productPackingPrice
				.setPackingTypeQuantity(productPackingPrice.getProductPacking().getPackingType().getQuantity());
		return productPackingPrice;
	}

	private LocalDate getFromDate(EntityManager em) {
		if (this.fromDate != null) {
			String fDate = this.fromDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			ProductPackingPriceDto.log.info(fDate);
			LocalDateTime sysdate = DatetimeUtils.timeStamp2LocalDateTime(
					BaseRepository.getScalarResult(em, "SELECT SYSDATE()", new HashMap<>(), Timestamp.class));
			if ((sysdate != null) && !this.fromDate.isAfter(sysdate.toLocalDate())) {
				return this.fromDate;
			}
		}
		return null;
	}

	private String getSaleType() {
		String result;
		switch (this.priceType) {
		case 1:
			result = "IN";
			break;
		case 2:
			result = "OUT";
			break;
		case 3:
			result = "PROMOTION";
			break;
		default:
			result = null;
			break;
		}
		return result;
	}

	private BigDecimal getPriceByType() {
		BigDecimal result;
		switch (this.priceType) {
		case 1:
			result = this.price;
			break;
		case 2:
			result = this.retailPrice;
			break;
		default:
			result = new BigDecimal(0);
			break;
		}
		return result;
	}

	@Override
	public String getSql() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ppp.* ");
		sb.append("FROM product_packing_price ppp ");
		sb.append("		JOIN product_packing pp ON ppp.product_packing_id = pp.id ");
		sb.append("WHERE (pp.code, ppp.sale_type, date(ppp.from_date)) = ('%s', '%s', STR_TO_DATE('%s', '%s'))");
		return String.format(sb.toString(), this.packingProduct.getCode(), this.getSaleType(),
				this.fromDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), "%d/%m/%Y");
	}
}
