package com.tsolution._1entities.dcommerce;

import java.io.Serializable;
import java.util.HashMap;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tsolution._1entities.PackingTypeEntity;
import com.tsolution._1entities.ProductEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution.excetions.BusinessException;
import com.tsolution.tasks.Convertable;
import com.tsolution.utils.StringUtils;

public class ProductPackingDto extends FilterDto implements Serializable, Convertable<ProductPackingEntity> {

	private static final Logger log = LogManager.getLogger(ProductPackingDto.class);
	private static final long serialVersionUID = -4828090671773300898L;
	private Long packingProductId;
	private String code;
	private String name;
	private Integer vat;
	private Long uom;
	private String barcode;
	private ProductDto product;
	private PackingTypeDto packingType;
	private DistributorDto distributor;
	private Boolean status;
	private Integer waningThreshold;
	private Integer lifecycle;
	private Double calculatedWeight;
	private Double calculatedWidth;
	private Double calculatedHeight;
	private Double calculatedLength;

	public Long getPackingProductId() {
		return this.packingProductId;
	}

	public void setPackingProductId(Long packingProductId) {
		this.packingProductId = packingProductId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVat() {
		return this.vat;
	}

	public void setVat(Integer vat) {
		this.vat = vat;
	}

	public Long getUom() {
		return this.uom;
	}

	public void setUom(Long uom) {
		this.uom = uom;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public ProductDto getProduct() {
		return this.product;
	}

	public void setProduct(ProductDto product) {
		this.product = product;
	}

	public PackingTypeDto getPackingType() {
		return this.packingType;
	}

	public void setPackingType(PackingTypeDto packingType) {
		this.packingType = packingType;
	}

	public DistributorDto getDistributor() {
		return this.distributor;
	}

	public void setDistributor(DistributorDto distributor) {
		this.distributor = distributor;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getWaningThreshold() {
		return this.waningThreshold == null ? 0 : this.waningThreshold;
	}

	public void setWaningThreshold(Integer waningThreshold) {
		this.waningThreshold = waningThreshold;
	}

	public Integer getLifecycle() {
		return this.lifecycle == null ? 0 : this.lifecycle;
	}

	public void setLifecycle(Integer lifecycle) {
		this.lifecycle = lifecycle;
	}

	public Double getCalculatedWeight() {
		return this.calculatedWeight == null ? 0 : this.calculatedWeight;
	}

	public void setCalculatedWeight(Double calculatedWeight) {
		this.calculatedWeight = calculatedWeight;
	}

	public Double getCalculatedWidth() {
		return this.calculatedWidth == null ? 0 : this.calculatedWidth;
	}

	public void setCalculatedWidth(Double calculatedWidth) {
		this.calculatedWidth = calculatedWidth;
	}

	public Double getCalculatedHeight() {
		return this.calculatedHeight == null ? 0 : this.calculatedHeight;
	}

	public void setCalculatedHeight(Double calculatedHeight) {
		this.calculatedHeight = calculatedHeight;
	}

	public Double getCalculatedLength() {
		return this.calculatedLength == null ? 0 : this.calculatedLength;
	}

	public void setCalculatedLength(Double calculatedLength) {
		this.calculatedLength = calculatedLength;
	}

	@Override
	public Long getTId() {
		return this.packingProductId;
	}

	@Override
	public ProductPackingEntity convert(SuperEntity superEntity, EntityManager em) throws BusinessException {
		ProductPackingEntity productPacking = new ProductPackingEntity();
		if ((this.product == null) || (this.product.getId() == null) || (this.packingType == null)
				|| (this.packingType.getPackingTypeId() == null)) {
			ProductPackingDto.log.info("this.product == null || this.product.getId() == null");
			ProductPackingDto.log.info("this.packingType == null || this.packingType.getPackingTypeId() == null");
			return null;
		}
		if ((superEntity != null) && (superEntity.getId() != null)) {
			productPacking.setId(superEntity.getId());
		}
		productPacking.setCode(this.code);
		productPacking.setName(this.name);
		productPacking.setUom(this.uom);
		productPacking.setBarcode(StringUtils.isNullOrEmpty(this.barcode) ? this.product.getBarcode() : this.barcode);
		productPacking.setStatus((this.status == null) || this.status);
		productPacking.setLength(this.getCalculatedLength());
		productPacking.setWidth(this.getCalculatedWidth());
		productPacking.setHeight(this.getCalculatedHeight());
		productPacking.setVolume(this.getCalculatedLength() * this.getCalculatedWidth() * this.getCalculatedHeight());
		productPacking.setWeight(this.getCalculatedWeight());

		ProductPackingEntity temp = null;
		if (superEntity instanceof ProductPackingEntity) {
			ProductPackingDto.log.info("superEntity instanceof ProductPackingEntity");
			temp = (ProductPackingEntity) superEntity;
		}
		if ((temp == null) || !temp.getPackingType().getCode().equals(this.packingType.getCode())) {
			String query = String.format("SELECT pt.* FROM packing_type pt WHERE pt.code = '%s'",
					this.packingType.getCode());
			PackingTypeEntity packingTypeEntity = BaseRepository.getFirstResultNativeQuery(em, query,
					new HashMap<String, Object>(), PackingTypeEntity.class);
			productPacking.setPackingType(packingTypeEntity);
		} else {
			productPacking.setPackingType(temp.getPackingType());
		}
		if (productPacking.getPackingType() == null) {
			ProductPackingDto.log.info("productPacking.getPackingType() == null");
			return null;
		}

		ProductEntity productEntity;
		if ((temp == null) || !temp.getProduct().getCode().equals(this.product.getCode())) {
			String query = String.format("SELECT p.* FROM product p WHERE p.code = '%s'", this.product.getCode());
			productEntity = BaseRepository.getFirstResultNativeQuery(em, query, new HashMap<String, Object>(),
					ProductEntity.class);
		} else {
			productEntity = temp.getProduct();
		}
		if (productEntity == null) {
			ProductPackingDto.log.info("productPacking.getProduct() == null");
			return null;
		}
		productPacking.setProduct(productEntity);
		productPacking.setVat(productEntity.getVat());
		productPacking.setWarningThreshold(productEntity.getWarningThreshold());
		productPacking.setLifecycle(productEntity.getLifecycle());
		productPacking.setDistributor(productEntity.getDistributor());

		return productPacking;
	}

	@Override
	public String getSql() {
		return String.format("SELECT pp.* FROM product_packing pp WHERE pp.code = '%s'", this.code);
	}
}
