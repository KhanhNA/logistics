package com.tsolution._1entities.dcommerce;

import java.io.Serializable;

public class FilterDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -6465131322786533541L;
	private Boolean isSynchronization;

	public Boolean getIsSynchronization() {
		return this.isSynchronization;
	}

	public void setIsSynchronization(Boolean isSynchronization) {
		this.isSynchronization = isSynchronization;
	}

}
