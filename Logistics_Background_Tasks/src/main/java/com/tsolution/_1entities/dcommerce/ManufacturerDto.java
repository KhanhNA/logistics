package com.tsolution._1entities.dcommerce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.tsolution._1entities.LanguageEntity;
import com.tsolution._1entities.ManufacturerDescriptionEntity;
import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.tasks.Convertable;

public class ManufacturerDto extends FilterDto implements Serializable, Convertable<ManufacturerEntity> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6337207573747793972L;
	private Long id;
	private String createdDate;
	private String modifiedDate;
	private String updtId;
	private String code;
	private String image;
	private String sortOrder;
	private Long merchantId;
	private String tel;
	private String email;
	private Boolean status;
	private List<ManufacturerDescriptionDto> descriptions;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getUpdtId() {
		return this.updtId;
	}

	public void setUpdtId(String updtId) {
		this.updtId = updtId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Long getMerchantId() {
		return this.merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<ManufacturerDescriptionDto> getDescriptions() {
		return this.descriptions;
	}

	public void setDescriptions(List<ManufacturerDescriptionDto> descriptions) {
		this.descriptions = descriptions;
	}

	@Override
	public Long getTId() {
		return this.id;
	}

	@Override
	public ManufacturerEntity convert(SuperEntity superEntity, EntityManager em) {
		ManufacturerEntity manufacturer = new ManufacturerEntity();
		if ((superEntity != null) && (superEntity.getId() != null)) {
			manufacturer.setId(superEntity.getId());
		}
		manufacturer.setCode(this.code);
		manufacturer.setTel(this.tel);
		manufacturer.setEmail(this.email);
		manufacturer.setStatus(this.status);

		ManufacturerEntity temp = null;
		if (superEntity instanceof ManufacturerEntity) {
			temp = (ManufacturerEntity) superEntity;
		}

		manufacturer.setManufacturerDescriptions(this.getManufacturerDescriptions(temp, manufacturer));

		return manufacturer;
	}

	private List<ManufacturerDescriptionEntity> getManufacturerDescriptions(ManufacturerEntity temp,
			ManufacturerEntity manufacturer) {
		if (this.descriptions == null) {
			return new ArrayList<>();
		}
		List<ManufacturerDescriptionEntity> manufacturerDescriptionEntities = temp == null ? new ArrayList<>()
				: temp.getManufacturerDescriptions();
		for (ManufacturerDescriptionDto manufacturerDescriptionDto : this.descriptions) {
			Optional<ManufacturerDescriptionEntity> oManufacturerDescriptionEntity = manufacturerDescriptionEntities
					.stream().filter(x -> x.getLanguage().getId().equals(manufacturerDescriptionDto.getLanguageId()))
					.findFirst();
			ManufacturerDescriptionEntity manufacturerDescriptionEntity;
			if (oManufacturerDescriptionEntity.isPresent()) {
				manufacturerDescriptionEntity = oManufacturerDescriptionEntity.get();
				manufacturerDescriptionEntity.setName(manufacturerDescriptionDto.getName());
				manufacturerDescriptionEntity.setAddress(manufacturerDescriptionDto.getAddress());
			} else {
				manufacturerDescriptionEntity = new ManufacturerDescriptionEntity();
				manufacturerDescriptionEntity
						.setLanguage(new LanguageEntity(manufacturerDescriptionDto.getLanguageId()));
				manufacturerDescriptionEntity.setManufacturer(temp == null ? manufacturer : temp);
				manufacturerDescriptionEntity.setName(manufacturerDescriptionDto.getName());
				manufacturerDescriptionEntity.setAddress(manufacturerDescriptionDto.getAddress());
				manufacturerDescriptionEntities.add(manufacturerDescriptionEntity);
			}
		}
		return manufacturerDescriptionEntities;
	}

	@Override
	public String getSql() {
		return String.format("SELECT m.* FROM manufacturer m WHERE m.code = '%s'", this.code);
	}
}
