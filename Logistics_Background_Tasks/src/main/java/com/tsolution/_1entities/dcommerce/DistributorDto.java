package com.tsolution._1entities.dcommerce;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.tasks.Convertable;

public class DistributorDto extends FilterDto implements Serializable, Convertable<DistributorEntity> {

	/**
	 *
	 */
	private static final long serialVersionUID = 3783085153286258939L;
	private Long id;
	private String code;
	private String name;
	private String address;
	private String phoneNumber;
	private String email;
	private String aboutUs;
	private Boolean enabled;
	private String taxCode;
	private Integer status;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAboutUs() {
		return this.aboutUs;
	}

	public void setAboutUs(String aboutUs) {
		this.aboutUs = aboutUs;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getTaxCode() {
		return this.taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public Long getTId() {
		return this.id;
	}

	@Override
	public DistributorEntity convert(SuperEntity superEntity, EntityManager em) {
		DistributorEntity distributorEntity = new DistributorEntity();
		if ((superEntity != null) && (superEntity.getId() != null)) {
			distributorEntity.setId(superEntity.getId());
		}
		distributorEntity.setCode(this.code);
		distributorEntity.setName(this.name);
		distributorEntity.setAddress(this.address);
		distributorEntity.setPhoneNumber(this.phoneNumber);
		distributorEntity.setEmail(this.email);
		distributorEntity.setAboutUs(this.aboutUs);
		distributorEntity.setEnabled(this.enabled);
		distributorEntity.setTin(this.taxCode);
		distributorEntity.setEnabled(this.status == 1);
		return distributorEntity;
	}

	@Override
	public String getSql() {
		return String.format("SELECT d.* FROM distributor d WHERE d.code = '%s'", this.code);
	}
}
