package com.tsolution._1entities.dcommerce;

import java.io.Serializable;
import java.util.List;

public class PageDto<T> implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -4866487357959317166L;
	private transient List<T> content;

	public List<T> getContent() {
		return this.content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

}
