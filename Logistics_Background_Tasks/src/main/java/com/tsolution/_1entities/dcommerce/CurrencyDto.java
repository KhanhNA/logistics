package com.tsolution._1entities.dcommerce;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.tasks.Convertable;

public class CurrencyDto extends FilterDto implements Serializable, Convertable<CurrencyEntity> {

	/**
	 *
	 */
	private static final long serialVersionUID = -2854225930646142739L;
	private Long id;
	private String code;
	private String name;
	private Boolean supported;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getSupported() {
		return this.supported;
	}

	public void setSupported(Boolean supported) {
		this.supported = supported;
	}

	@Override
	public Long getTId() {
		return this.id;
	}

	@Override
	public CurrencyEntity convert(SuperEntity superEntity, EntityManager em) {
		CurrencyEntity currency = new CurrencyEntity();
		if ((superEntity != null) && (superEntity.getId() != null)) {
			currency.setId(superEntity.getId());
		}
		currency.setCode(this.code);
		currency.setName(this.name);
		currency.setSupport(this.supported);
		return currency;
	}

	@Override
	public String getSql() {
		return String.format("SELECT c.* FROM currency c WHERE c.code = '%s'", this.code);
	}
}
