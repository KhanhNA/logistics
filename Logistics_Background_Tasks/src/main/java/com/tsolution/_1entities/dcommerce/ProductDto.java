package com.tsolution._1entities.dcommerce;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.LanguageEntity;
import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution._1entities.ProductDescriptionEntity;
import com.tsolution._1entities.ProductEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution.excetions.BusinessException;
import com.tsolution.tasks.Convertable;
import com.tsolution.utils.StringUtils;

public class ProductDto extends FilterDto implements Serializable, Convertable<ProductEntity> {

	private static final Logger log = LogManager.getLogger(ProductDto.class);
	private static final long serialVersionUID = -3279393313166518654L;

	private Long id;
	private String code;
	private String name;
	private String sku;
	private String barcode;
	private ProductTypeDto productType;
	private ManufacturerDto manufacturer;
	private DistributorDto distributor;
	private Long distributorId;
	private String distributorCode;
	private Double length;
	private Double width;
	private Double height;
	private Double weight;
	private Integer vat;
	private Integer warningThreshold;
	private Integer lifecycle;
	private Integer status;
	private List<ProductDescriptionDto> productDescriptions;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSku() {
		return this.sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public ProductTypeDto getProductType() {
		return this.productType;
	}

	public void setProductType(ProductTypeDto productType) {
		this.productType = productType;
	}

	public ManufacturerDto getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(ManufacturerDto manufacturer) {
		this.manufacturer = manufacturer;
	}

	public DistributorDto getDistributor() {
		return this.distributor;
	}

	public void setDistributor(DistributorDto distributor) {
		this.distributor = distributor;
	}

	public Long getDistributorId() {
		return this.distributorId;
	}

	public void setDistributorId(Long distributorId) {
		this.distributorId = distributorId;
	}

	public String getDistributorCode() {
		return this.distributorCode;
	}

	public void setDistributorCode(String distributorCode) {
		this.distributorCode = distributorCode;
	}

	public Double getLength() {
		return this.length == null ? 0 : this.length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Double getWidth() {
		return this.width == null ? 0 : this.width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return this.height == null ? 0 : this.height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWeight() {
		return this.weight == null ? 0 : this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Integer getVat() {
		return this.vat;
	}

	public void setVat(Integer vat) {
		this.vat = vat;
	}

	public Integer getWarningThreshold() {
		return this.warningThreshold == null ? 0 : this.warningThreshold;
	}

	public void setWarningThreshold(Integer warningThreshold) {
		this.warningThreshold = warningThreshold;
	}

	public Integer getLifecycle() {
		return this.lifecycle == null ? 0 : this.lifecycle;
	}

	public void setLifecycle(Integer lifecycle) {
		this.lifecycle = lifecycle;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<ProductDescriptionDto> getProductDescriptions() {
		return this.productDescriptions;
	}

	public void setProductDescriptions(List<ProductDescriptionDto> productDescriptions) {
		this.productDescriptions = productDescriptions;
	}

	@Override
	public Long getTId() {
		return this.id;
	}

	private boolean isValid() {
		if ((this.manufacturer == null) || (this.manufacturer.getId() == null)) {
			ProductDto.log.info("this.manufacturer == null || this.manufacturer.getId() == null");
			return false;
		}
		if (this.distributor == null) {
			ProductDto.log.info("this.distributor == null");
			if ((this.distributorId == null) || StringUtils.isNullOrEmpty(this.distributorCode)) {
				ProductDto.log.info("this.distributorId == null || StringUtils.isNullOrEmpty(this.distributorCode)");
				return false;
			}
		} else if (this.distributor.getId() == null) {
			ProductDto.log.info("this.distributor.getId() == null");
			return false;
		}
		return true;
	}

	@Override
	public ProductEntity convert(SuperEntity superEntity, EntityManager em) throws BusinessException {
		if (!this.isValid()) {
			return null;
		}
		ProductEntity product = new ProductEntity();
		if ((superEntity != null) && (superEntity.getId() != null)) {
			product.setId(superEntity.getId());
		}
		product.setCode(this.code);
		product.setName(this.name);
		product.setSku(this.sku);
		product.setBarcode(this.barcode);
		product.setAvailable(this.status == 1);
		product.setAvailableDate(LocalDateTime.of(2019, 06, 01, 0, 0));
		product.setLength(this.getLength());
		product.setWidth(this.getWidth());
		product.setHeight(this.getHeight());
		product.setWeight(this.getWeight());
		product.setVat(this.getVat());
		product.setLifecycle(this.getLifecycle());
		product.setWarningThreshold(this.getWarningThreshold());
		ProductEntity temp = null;
		if (superEntity instanceof ProductEntity) {
			ProductDto.log.info("superEntity instanceof ProductEntity");
			temp = (ProductEntity) superEntity;
		}
		this.setReferenceManufacturer(em, product, temp);
		this.setReferenceDistributor(em, product, temp);
		product.setProductPackingPrices(temp == null ? null : temp.getProductPackingPrices());

		product.setProductDescriptions(this.getProductDescriptions(temp, product));
		return product;
	}

	private void setReferenceManufacturer(EntityManager em, ProductEntity product, ProductEntity temp)
			throws BusinessException {
		if ((temp == null) || !temp.getManufacturer().getCode().equals(this.manufacturer.getCode())) {
			String query = String.format("SELECT m.* FROM manufacturer m WHERE m.code = '%s'",
					this.manufacturer.getCode());
			ManufacturerEntity manufacturerEntity = BaseRepository.getFirstResultNativeQuery(em, query,
					new HashMap<String, Object>(), ManufacturerEntity.class);
			product.setManufacturer(manufacturerEntity);
		} else {
			product.setManufacturer(temp.getManufacturer());
		}
		ProductDto.log.info("product.getManufacturer() == null ({})", product.getManufacturer() == null);
	}

	private void setReferenceDistributor(EntityManager em, ProductEntity product, ProductEntity temp)
			throws BusinessException {
		if ((temp == null) || !temp.getDistributor().getCode().equals(this.distributor.getCode())) {
			String query = String.format("SELECT d.* FROM distributor d WHERE d.code = '%s'",
					StringUtils.isNullOrEmpty(this.distributor.getCode()) ? this.distributorCode
							: this.distributor.getCode());
			DistributorEntity distributorEntity = BaseRepository.getFirstResultNativeQuery(em, query,
					new HashMap<String, Object>(), DistributorEntity.class);
			product.setDistributor(distributorEntity);
		} else {
			product.setDistributor(temp.getDistributor());
		}
		ProductDto.log.info("product.getDistributor() == null ({})", product.getDistributor() == null);
	}

	private List<ProductDescriptionEntity> getProductDescriptions(ProductEntity temp, ProductEntity product) {
		if (this.productDescriptions == null) {
			return new ArrayList<>();
		}
		List<ProductDescriptionEntity> productDescriptionEntities = temp == null ? new ArrayList<>()
				: temp.getProductDescriptions();
		for (ProductDescriptionDto productDescriptionDto : this.productDescriptions) {
			Optional<ProductDescriptionEntity> oProductDescriptionEntity = productDescriptionEntities.stream()
					.filter(x -> x.getLanguage().getId().equals(productDescriptionDto.getLanguageId())).findFirst();
			ProductDescriptionEntity productdescriptionEntity;
			if (oProductDescriptionEntity.isPresent()) {
				productdescriptionEntity = oProductDescriptionEntity.get();
				productdescriptionEntity.setCode(this.code);
				productdescriptionEntity.setName(productDescriptionDto.getProductName());
			} else {
				productdescriptionEntity = new ProductDescriptionEntity();
				productdescriptionEntity.setLanguage(new LanguageEntity(productDescriptionDto.getLanguageId()));
				productdescriptionEntity.setProduct(temp == null ? product : temp);
				productdescriptionEntity.setCode(this.code);
				productdescriptionEntity.setName(productDescriptionDto.getProductName());
				productDescriptionEntities.add(productdescriptionEntity);
			}
		}
		return productDescriptionEntities;
	}

	@Override
	public String getSql() {
		return String.format("SELECT p.* FROM product p WHERE p.code = '%s'", this.code);
	}
}
