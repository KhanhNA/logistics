package com.tsolution._1entities.dcommerce;

import java.io.Serializable;

public class ManufacturerDescriptionDto extends FilterDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 9041571205498569790L;
	private Long id;
	private String dateCreate;
	private String dateModified;
	private String updtId;
	private String description;
	private String name;
	private String title;
	private String dateLastClick;
	private String manufacturersUrl;
	private String urlClicked;
	private Long languageId;
	private Long manufacturerId;
	private String urlImage;
	private String address;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}

	public String getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}

	public String getUpdtId() {
		return this.updtId;
	}

	public void setUpdtId(String updtId) {
		this.updtId = updtId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDateLastClick() {
		return this.dateLastClick;
	}

	public void setDateLastClick(String dateLastClick) {
		this.dateLastClick = dateLastClick;
	}

	public String getManufacturersUrl() {
		return this.manufacturersUrl;
	}

	public void setManufacturersUrl(String manufacturersUrl) {
		this.manufacturersUrl = manufacturersUrl;
	}

	public String getUrlClicked() {
		return this.urlClicked;
	}

	public void setUrlClicked(String urlClicked) {
		this.urlClicked = urlClicked;
	}

	public Long getLanguageId() {
		return this.languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	public Long getManufacturerId() {
		return this.manufacturerId;
	}

	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public String getUrlImage() {
		return this.urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
