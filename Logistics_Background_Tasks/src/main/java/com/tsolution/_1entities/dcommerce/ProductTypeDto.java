package com.tsolution._1entities.dcommerce;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.tsolution._1entities.ProductTypeEntity;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.tasks.Convertable;

public class ProductTypeDto extends FilterDto implements Serializable, Convertable<ProductTypeEntity> {
	/**
	 *
	 */
	private static final long serialVersionUID = -2304785979842483967L;
	private Long id;
	private String code;
	private String name;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Long getTId() {
		return this.id;
	}

	@Override
	public ProductTypeEntity convert(SuperEntity superEntity, EntityManager em) {
		ProductTypeEntity productTypeEntity = new ProductTypeEntity();
		if ((superEntity != null) && (superEntity.getId() != null)) {
			productTypeEntity.setId(superEntity.getId());
		}
		productTypeEntity.setCode(this.code);
		productTypeEntity.setName(this.name);
		productTypeEntity.setStatus(true);
		return productTypeEntity;
	}

	@Override
	public String getSql() {
		return String.format("SELECT pt.* FROM product_type pt WHERE pt.code = '%s'", this.code);
	}
}
