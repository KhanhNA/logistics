package com.tsolution.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DcommerceDeserializeDateHandler extends StdDeserializer<LocalDate> {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(DcommerceDeserializeDateHandler.class);

	public DcommerceDeserializeDateHandler() {
		this(null);
	}

	public DcommerceDeserializeDateHandler(Class<Object> clazz) {
		super(clazz);
	}

	@Override
	public LocalDate deserialize(JsonParser jsonparser, DeserializationContext context) {
		try {
			String date = jsonparser.getText();
			SimpleDateFormat sdf = new SimpleDateFormat(Constants.DCOMMERCE_DATE_PATTERN);
			return sdf.parse(date).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} catch (Exception e) {
			DcommerceDeserializeDateHandler.logger.error(e.getMessage(), e);
			return null;
		}
	}
}