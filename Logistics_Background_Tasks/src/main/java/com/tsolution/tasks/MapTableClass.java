package com.tsolution.tasks;

import com.tsolution._1entities.base.SuperEntity;

public class MapTableClass {
	private Class<? extends Convertable<?>> clazzConvertable;
	private Class<? extends SuperEntity> clazzSuperEntity;

	public MapTableClass(Class<? extends Convertable<?>> clazzConvertable,
			Class<? extends SuperEntity> clazzSuperEntity) {
		this.clazzConvertable = clazzConvertable;
		this.clazzSuperEntity = clazzSuperEntity;
	}

	public Class<? extends Convertable<?>> getClazzConvertable() {
		return this.clazzConvertable;
	}

	public Class<? extends SuperEntity> getClazzSuperEntity() {
		return this.clazzSuperEntity;
	}

}
