package com.tsolution.tasks;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.tsolution._1entities.dcommerce.FilterDto;
import com.tsolution.excetions.BusinessException;

public interface ITableSync {
	public <T extends Convertable<?>> void syncTable(OAuth2AccessToken oAuth2AccessToken, String table,
			FilterDto filterDto, Class<T> clazz) throws BusinessException;
}
