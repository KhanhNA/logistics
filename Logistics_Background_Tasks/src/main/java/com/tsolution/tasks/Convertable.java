package com.tsolution.tasks;

import javax.persistence.EntityManager;

import com.tsolution._1entities.base.SuperEntity;
import com.tsolution.excetions.BusinessException;

public interface Convertable<T extends SuperEntity> {
	Long getTId();

	T convert(SuperEntity superEntity, EntityManager em) throws BusinessException;

	String getSql();
}
