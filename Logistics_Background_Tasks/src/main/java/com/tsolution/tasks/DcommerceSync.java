package com.tsolution.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Component;

import com.tsolution._1entities.dcommerce.FilterDto;
import com.tsolution.utils.CallApiUtils;
import com.tsolution.utils.StringUtils;

@Component
public class DcommerceSync {

	private static final Logger logger = LogManager.getLogger(DcommerceSync.class);

	@Value("${security.oauth2.client.accessTokenUri}")
	private String accessTokenUri;

	@Value("${security.oauth2.client.clientId}")
	private String clientId;

	@Value("${security.oauth2.client.clientSecret}")
	private String clientSecret;

	@Value("${scheduled.tasks.dcommerce.username}")
	private String username;

	@Value("${scheduled.tasks.dcommerce.password}")
	private String password;

	@Value("${scheduled.tasks.dcommerce.product.tables.sync}")
	private String tableSync;

	@Autowired
	private ITableSync iTableSync;

	@Scheduled(cron = "${scheduled.tasks.dcommerce.product.sync}")
	public void run() {
		if (StringUtils.isNullOrEmpty(this.tableSync)) {
			return;
		}
		OAuth2RestOperations oAuth2RestOperations = CallApiUtils.login(this.accessTokenUri, this.clientId,
				this.clientSecret, this.username, this.password);
		if (oAuth2RestOperations == null) {
			return;
		}
		String[] tables = this.tableSync.split("\\|");
		if ((tables == null) || (tables.length == 0)) {
			return;
		}
		DcommerceSync.logger.info("Start Sync!!!");
		for (String table : tables) {
			try {
				FilterDto filterDto = new FilterDto();
				filterDto.setIsSynchronization(false);
				this.iTableSync.syncTable(oAuth2RestOperations.getAccessToken(), table, filterDto,
						MapTable.getClass(table).getClazzConvertable());
			} catch (Exception e) {
				DcommerceSync.logger.error(e.getMessage(), e);
			}
		}
		DcommerceSync.logger.info("Stop Sync!!!");
	}

}
