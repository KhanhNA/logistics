package com.tsolution.tasks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.tsolution._1entities.CurrencyEntity;
import com.tsolution._1entities.DistributorEntity;
import com.tsolution._1entities.ManufacturerEntity;
import com.tsolution._1entities.PackingTypeEntity;
import com.tsolution._1entities.ProductEntity;
import com.tsolution._1entities.ProductPackingEntity;
import com.tsolution._1entities.ProductPackingPriceEntity;
import com.tsolution._1entities.ProductPackingUnitEntity;
import com.tsolution._1entities.ProductTypeEntity;
import com.tsolution._1entities.dcommerce.AppUnitDescPureDto;
import com.tsolution._1entities.dcommerce.CurrencyDto;
import com.tsolution._1entities.dcommerce.DistributorDto;
import com.tsolution._1entities.dcommerce.ManufacturerDto;
import com.tsolution._1entities.dcommerce.PackingTypeDto;
import com.tsolution._1entities.dcommerce.ProductDto;
import com.tsolution._1entities.dcommerce.ProductPackingDto;
import com.tsolution._1entities.dcommerce.ProductPackingPriceDto;
import com.tsolution._1entities.dcommerce.ProductTypeDto;

public class MapTable {
	private static final String PACKING_PRICE = "PackingPrice";
	private static final String PACKING_PRODUCT = "PackingProduct";
	private static final String PRODUCT = "Product";
	private static final String PACKING_TYPE = "PackingType";
	private static final String MANUFACTURER = "Manufacturer";
	private static final String PRODUCT_TYPE = "ProductType";
	private static final String CURRENCY = "Currency";
	private static final String DISTRIBUTOR = "Distributor";
	private static final String APP_UNIT = "AppUnitDescPure";

	private static final Map<String, MapTableClass> map = ImmutableMap.<String, MapTableClass>builder()
			.put(MapTable.CURRENCY, new MapTableClass(CurrencyDto.class, CurrencyEntity.class))
			.put(MapTable.DISTRIBUTOR, new MapTableClass(DistributorDto.class, DistributorEntity.class))
			.put(MapTable.MANUFACTURER, new MapTableClass(ManufacturerDto.class, ManufacturerEntity.class))
			.put(MapTable.PRODUCT_TYPE, new MapTableClass(ProductTypeDto.class, ProductTypeEntity.class))
			.put(MapTable.PACKING_TYPE, new MapTableClass(PackingTypeDto.class, PackingTypeEntity.class))
			.put(MapTable.PRODUCT, new MapTableClass(ProductDto.class, ProductEntity.class))
			.put(MapTable.APP_UNIT, new MapTableClass(AppUnitDescPureDto.class, ProductPackingUnitEntity.class))
			.put(MapTable.PACKING_PRODUCT, new MapTableClass(ProductPackingDto.class, ProductPackingEntity.class))
			.put(MapTable.PACKING_PRICE,
					new MapTableClass(ProductPackingPriceDto.class, ProductPackingPriceEntity.class))
			.build();

	private MapTable() {
	}

	public static MapTableClass getClass(String table) {
		return MapTable.map.get(table);
	}

}
