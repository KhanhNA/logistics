package com.tsolution.tasks;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.dcommerce.FilterDto;
import com.tsolution._1entities.dcommerce.PageDto;
import com.tsolution._1entities.dcommerce.SynchronizeDto;
import com.tsolution._2repositories.impl.BaseRepository;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CallApiUtils;

@Service
public class TableSync implements ITableSync {

	private static final Logger logger = LogManager.getLogger(TableSync.class);

	@Value("${scheduled.tasks.dcommerce.host}")
	protected String host;

	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public <T extends Convertable<?>> void syncTable(OAuth2AccessToken oAuth2AccessToken, String table,
			FilterDto filterDto, Class<T> clazz) throws BusinessException {
		try {
			String url = "/api/v1/common/list?maxResult=9999&entityName=" + table;
			TableSync.logger.info(url);
			ResponseEntity<?> result = CallApiUtils.post(this.host, oAuth2AccessToken, url, filterDto);
			if (!HttpStatus.OK.equals(result.getStatusCode())) {
				TableSync.logger.error("Code: {}", result.getStatusCodeValue());
			}
			ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
					false);
			String resultBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result.getBody());
			JavaType javaType = mapper.getTypeFactory().constructParametricType(PageDto.class, clazz);
			PageDto<T> page = mapper.readValue(resultBody, javaType);
			if ((page != null) && (page.getContent() != null) && !page.getContent().isEmpty()) {
				SynchronizeDto synchronizeDto = new SynchronizeDto();
				synchronizeDto.setEntityName(table);
				MapTableClass mapTableClass = MapTable.getClass(table);
				for (T t : page.getContent()) {
					String sql = t.getSql();
					TableSync.logger.info(sql);
					SuperEntity superEntity = BaseRepository.getFirstResultNativeQuery(this.em, sql,
							new HashMap<String, Object>(), mapTableClass.getClazzSuperEntity());
					SuperEntity obj = t.convert(superEntity, this.em);
					if (obj == null) {
						continue;
					}
					String jsonObj = mapper.writeValueAsString(obj);
					TableSync.logger.info(jsonObj);
					if (obj.getId() == null) {
						this.em.persist(obj);
					} else {
						this.em.merge(obj);
					}
					synchronizeDto.getIds().add(t.getTId());
				}
				this.updateSyncStatus(oAuth2AccessToken, mapper, synchronizeDto);
			}
		} catch (HttpStatusCodeException | IOException e) {
			TableSync.logger.error(e.getMessage(), e);
		}
	}

	private void updateSyncStatus(OAuth2AccessToken oAuth2AccessToken, ObjectMapper mapper,
			SynchronizeDto synchronizeDto) throws JsonProcessingException {
		if (!synchronizeDto.getIds().isEmpty()) {
			String jsonSynchronizeDto = mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(Collections.singleton(synchronizeDto));
			TableSync.logger.info(jsonSynchronizeDto);
			ResponseEntity<?> finalResult = CallApiUtils.post(this.host, oAuth2AccessToken, "/api/v1/synchronize",
					Collections.singleton(synchronizeDto));
			if (!HttpStatus.OK.equals(finalResult.getStatusCode())) {
				TableSync.logger.error("Code: {}", finalResult.getStatusCodeValue());
			}
			this.em.flush();
		}
	}

}
