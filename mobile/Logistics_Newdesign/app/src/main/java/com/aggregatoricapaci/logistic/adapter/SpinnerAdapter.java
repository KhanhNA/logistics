package com.aggregatoricapaci.logistic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.model.dto.LanguageSpinner;

import java.util.List;


public class SpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<LanguageSpinner> lstLanguage;

    public SpinnerAdapter(Context context, List<LanguageSpinner> lstLanguage) {
        this.context = context;
        this.lstLanguage = lstLanguage;
    }
    
    @Override
    public int getCount() {
        return lstLanguage.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.item_spinner, null);

        ImageView imageView = view.findViewById(R.id.spinnerImageView);
        TextView titleTextView = view.findViewById(R.id.title);

        LanguageSpinner foodCategoryModel = lstLanguage.get(i);
        imageView.setImageResource(foodCategoryModel.getIdRes());
        titleTextView.setText(foodCategoryModel.getLanguageName());

        return view;
    }
}
