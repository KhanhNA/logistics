package com.aggregatoricapaci.logistic.ui.dialog;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.databinding.DialogConfirmMstoreBinding;
import com.aggregatoricapaci.logistic.util.StringUtils;


public class DialogConfirmMstore extends DialogFragment {
    private String title;
    private String actionTitle;
    private String msg;
    private View.OnClickListener onClickListener;
    private Runnable onCancelListener;
    private boolean isCancelable;
    private boolean isCancelOnClickOutside = true;
    private boolean isHideCancelButton;
    private DialogConfirmMstoreBinding binding;

    public DialogConfirmMstore(){
        super();
    }

    public DialogConfirmMstore(@NonNull String title, String msg, boolean isCancelable, boolean isHideCancelButton, View.OnClickListener onClickConfirmListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickConfirmListener;
        this.isCancelable = isCancelable;
        this.isHideCancelButton = isHideCancelButton;
    }

    public DialogConfirmMstore(@NonNull String title, String msg, View.OnClickListener onClickListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickListener;
        this.isCancelable = true;
    }

    public DialogConfirmMstore(@NonNull String title, String msg, String actionTitle, View.OnClickListener onClickListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickListener;
        this.actionTitle = actionTitle;
        this.isCancelable = true;
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        if(this.onCancelListener!=null){
            this.onCancelListener.run();
        }
    }

    public DialogConfirmMstore onCancelListener(Runnable onClickListener){
        this.onCancelListener = onClickListener;
        return this;
    }

    public void setCancelOnClickOutside(boolean isEnable){
        isCancelOnClickOutside = isEnable;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_confirm_mstore, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setCancelable(isCancelOnClickOutside && isCancelable);
        binding.btnCancel.setVisibility(isCancelable ? View.VISIBLE : View.INVISIBLE);
        binding.btnDismiss.setOnClickListener(v -> {
            this.dismiss();
            if(this.onCancelListener!=null){
                this.onCancelListener.run();
            }
        });
        binding.txtTitle.setText(title);

        if (actionTitle != null) {
            binding.btnConfirm.setText(actionTitle);
        }
        if (StringUtils.isNullOrEmpty(msg)) {
            binding.txtMsg.setVisibility(View.GONE);
        } else {
            binding.txtMsg.setText(msg);
        }
        if (isHideCancelButton) {
            binding.btnDismiss.setVisibility(View.GONE);
        }

        binding.btnCancel.setOnClickListener(v -> {
            this.dismiss();
            if(this.onCancelListener!=null){
                this.onCancelListener.run();
            }
        });
        binding.btnConfirm.setOnClickListener(onClickListener);


        return binding.getRoot();
    }

    public void showLoading() {
        binding.getRoot().findViewById(R.id.spinKit).setVisibility(View.VISIBLE);
        binding.getRoot().findViewById(R.id.layoutContent).setVisibility(View.GONE);
    }

}