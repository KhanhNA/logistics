package com.aggregatoricapaci.logistic.util;

import android.app.Activity;
import android.os.Bundle;

public class ActivityUtils {
    public static final String CLOSE_ON_CREATE = "CLOSE_ON_CREATE";

    public static void closeActivityOnCreate(Bundle outState){
        outState.putBoolean(CLOSE_ON_CREATE, true);
    }

    public static void handleSavedInstanceState(Activity activity, Bundle savedInstanceState){
        if(activity!=null && savedInstanceState!=null) {
            boolean isClose = savedInstanceState.getBoolean(CLOSE_ON_CREATE, false);
            if (isClose) {
                activity.finish();
            }
        }
    }
}
