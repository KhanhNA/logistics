package com.aggregatoricapaci.logistic.service.listener;

public interface RefreshTokenListener {
    void refreshTokenSuccess();
}
