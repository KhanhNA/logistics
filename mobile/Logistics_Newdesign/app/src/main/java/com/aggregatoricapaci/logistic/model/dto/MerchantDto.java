package com.aggregatoricapaci.logistic.model.dto;

import androidx.annotation.Nullable;

import com.aggregatoricapaci.logistic.util.StringUtils;
import com.tsolution.base.BaseModel;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ts-client01
 * Create at 2019-06-25 09:13
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MerchantDto extends BaseModel implements Comparable<MerchantDto>, Cloneable {
    private Long merchantId;
    private String merchantCode;
    private String merchantName;
    private Long walletClientId;
    private Long parentMarchantId;
    private Long merchantTypeId;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String fullName;
    private Long defaultStoreId;
    private Double tax;
    private String merchantImgUrl;
    private String mobilePhone;
    private String activeCode;
    private Date activeDate;
    private Date registerDate;
    private Integer activeStatus;
    private Integer status;
    private String address;

    private Integer gender;
    private String paymentAccount;
    private String passWordSecondTime;
    //properties contact;
    MerchantDto parentMerchant;
    private boolean isRegister;
    private String email;
    //properties personal
    private Date dateBirth;
    private String tokenFirebase;
    private String discussionId;
    private boolean isCheckAccountChat;
    private Boolean existAccountPost;
    private String facebookUrl;
    private String provinceCode;
    private String districtCode;
    private String villageCode;
    private String packageCode;
    private Boolean defaultPassword;
    private Boolean defaultPasswordWallet;
    private String walletAuthenticationKey;
    private String levelCode;
    private Date pointExpireDate;
    private Integer notifySetting;

    public String getTaxStr() {
        return "" + (tax == null ? "" : tax);
    }

    public void setTaxStr(String tax) {
        if (StringUtils.isNullOrEmpty(tax)) {
            this.tax = 0D;
        } else {
            this.tax = Double.parseDouble(tax);
        }
    }


    @Override
    public int compareTo(MerchantDto other) {
        if (fullName != null && other.fullName != null) {
            return fullName.compareTo(other.fullName);
        }
        return 0;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null) {
            return this.mobilePhone.equals(((MerchantDto) obj).mobilePhone);
        }
        return false;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


}