package com.aggregatoricapaci.logistic.util;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.BindingAdapter;

import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.base.AppController;
import com.aggregatoricapaci.logistic.base.GlideApp;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CustomerBindingAdapter {

    @BindingAdapter("strikeTextView")
    public static void strikeTextView(TextView textView, String content) {
        textView.setText(content);
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        loadImageGlide(view, ImageUtils.getImgUrl(imageUrl));
    }

//    @BindingAdapter("imageHomeBanner")
//    public static void loadImageHomeBanner(ImageView view, String imageUrl) {
//        if (view != null && StringUtils.isNotNullAndNotEmpty(imageUrl)) {
//            GlideUrl glideUrl = new GlideUrl(ImageUtils.getBillingUrl(imageUrl),
//                    new LazyHeaders.Builder()
//                            .addHeader("Authorization", HttpHelper.TOKEN_DCOM)
//                            .build());
//            GlideApp.with(view.getContext())
//                    .load(glideUrl)
//                    .into(view);
//        }
//    }

//
//    @BindingAdapter("addChipGroupComment")
//    public static void addChipGroupComment(ChipGroup chipGroup, List<Integer> idCode) {
//        chipGroup.removeAllViews();
//        if (TsUtils.isNotNull(idCode)) {
//            for (Integer integer : idCode) {
//                Chip chip = new Chip(chipGroup.getContext());
//                chip.setTag(integer);
//                chip.setTextColor(chipGroup.getContext().getResources().getColor(R.color.primaryTextColor));
//                chip.setChipStrokeWidth(1);
//                chip.setChipStrokeColor(ColorStateList.valueOf(chipGroup.getContext().getResources().getColor(R.color.stroke_color)));
//                chip.setText(StringUtils.getTagComment(integer, chipGroup.getContext()));
//                chip.setChipBackgroundColorResource(R.color.white);
//                chip.setCloseIconVisible(false);
//                chip.setCheckable(false);
//                chipGroup.addView(chip);
//            }
//        }
//    }

    @BindingAdapter("imageBackground")
    public static void imageBackground(AppBarLayout view, String imageUrl) {
        if (StringUtils.isNotNullAndNotEmpty(imageUrl)) {
            GlideApp.with(view.getContext())
                    .load(ImageUtils.getImgUrl(imageUrl))
                    .into(new CustomTarget<Drawable>() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            view.setBackground(resource);

                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {

                        }
                    });
        }
    }


    public static void loadImageComment(ImageView view, String imageUrl) {
        loadImageGlide(view, ImageUtils.getImgUrl(imageUrl));
    }


    private static void previewImage(Context context, List<String> list, int position) {
        List<LocalMedia> mediaList = new ArrayList<>();
        if (TsUtils.isNotNull(list)) {
            for (String s : list) {
                LocalMedia localMedia = new LocalMedia();
                localMedia.setPath(s);
                mediaList.add(localMedia);
            }
        }
        try {
            PictureSelector.create(unwrap(context))
                    .themeStyle(R.style.picture_default_style)
                    .isNotPreviewDownload(true)
                    .loadImageEngine(GlideEngine.createGlideEngine())
                    .openExternalPreview(position, mediaList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Activity unwrap(Context context) {
        while (!(context instanceof Activity) && context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        return (Activity) context;
    }


    private static void loadImageGlide(ImageView view, String imageUrl) {
        GlideApp.with(view.getContext())
                .load(imageUrl)
                .error(R.drawable.ic_stub)
                .into(view);
    }

    @BindingAdapter({"underLine"})
    public static void spanTextBold(TextView view, String title) {
        if (StringUtils.isNotNullAndNotEmpty(title)) {
            SpannableStringBuilder spannable = new SpannableStringBuilder(title);
            spannable.setSpan(
                    new UnderlineSpan(), 0, // start
                    title.length(), // end
                    Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            );
            view.setText(spannable);
        }
    }

    @BindingAdapter("spanBold")
    public static void spanBold(TextView view, String result) {
        String s2 = view.getContext().getString(R.string.results);
        SpannableString spannable = new SpannableString(result + s2);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, result.length(), 0);
        spannable.setSpan(new StyleSpan(Typeface.NORMAL), result.length(), result.length() + s2.length(), 0);
        view.setText(spannable);
    }

//    @BindingAdapter({"msgExpirePoint", "pointExpireDate"})
//    public static void msgExpirePoint(TextView view, BigDecimal point, String pointExpireDate) {
//        if (point == null) return;
//        String msg = view.getResources().getString(R.string.msg_expire_point);
//        view.setText(String.format(msg, AppController.getInstance().formatDecimal(point), pointExpireDate));
//    }
//    @BindingAdapter("expireOn")
//    public static void expireOn(TextView view, String date) {
//        if (date == null) return;
//        String msg = view.getResources().getString(R.string.msg_expire_date);
//        view.setText(String.format(msg, date));
//    }

    @BindingAdapter("imageBase64")
    public static void loadImageBase64(ImageView imageView, String base64) {
        if (base64 != null) {
            GlideApp.with(imageView.getContext())
                    .load(Base64.decode(base64, Base64.DEFAULT))
                    .into(imageView);
        }
    }


    private static String StrRpl(String str) {
        if (StringUtils.isNullOrEmpty(str)) return "";

        char[] chars = str.toCharArray();
        for (int i = 0, j = 0; i < chars.length && j < chars.length - 3; i++) {
            char ch = chars[i];
            if (!Character.isWhitespace(ch)) {
                chars[i] = '*';
                j++;
            }
        }
        str = new String(chars);
        return str;
    }


//    @SuppressLint("UseCompatLoadingForDrawables")
//    @BindingAdapter("orderDetailStatus")
//    public static void orderDetailStatus(LinearLayout linearLayout, Integer status) {
//        //trong LinearLayout cần có 1 text view đặt id là orderStatus
//        TextView textView = linearLayout.findViewById(R.id.orderStatus);
//        Resources rs = textView.getResources();
//        if (status == null) return;
//        Drawable img = null;
//
//        switch (status) {
//            case OrderStatusType.PENDING:
//                textView.setText(R.string.order_pending);
//                linearLayout.setBackgroundColor(rs.getColor(R.color.color_pending));
//                img = rs.getDrawable(R.drawable.ic_order_pending);
//                break;
//            case OrderStatusType.PROCESSED:
//                textView.setText(R.string.CONFIRMED);
//                linearLayout.setBackgroundColor(rs.getColor(R.color.color_delivering));
//                img = rs.getDrawable(R.drawable.ic_order_success);
//                break;
//            case OrderStatusType.DELIVERING:
//                textView.setText(R.string.delivering);
//                linearLayout.setBackgroundColor(rs.getColor(R.color.color_delivered));
//                img = rs.getDrawable(R.drawable.ic_order_delivering);
//                break;
//            case OrderStatusType.DELIVERED:
//                textView.setText(R.string.delivered);
//                linearLayout.setBackgroundColor(rs.getColor(R.color.color_complete));
//                img = rs.getDrawable(R.drawable.ic_order_success);
//                break;
//            case OrderStatusType.COMPLETE:
//                textView.setText(R.string.ORDER_SUCCESS);
//                textView.setBackgroundColor(rs.getColor(R.color.color_complete));
//                img = rs.getDrawable(R.drawable.ic_order_success);
//                break;
//            case OrderStatusType.CANCELED:
//                textView.setText(R.string.CANCEL_ORDER);
//                linearLayout.setBackgroundColor(rs.getColor(R.color.color_price));
//                img = rs.getDrawable(R.drawable.ic_order_canceled);
//                break;
//            case OrderStatusType.RETURN:
//                textView.setText(R.string.return_order);
//                img = rs.getDrawable(R.drawable.ic_order_canceled);
//                linearLayout.setBackgroundColor(rs.getColor(R.color.color_price));
//                break;
//        }
//        textView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
//    }

}
