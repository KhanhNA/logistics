package com.aggregatoricapaci.logistic.base.http;


import android.util.Log;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * @author：PhamBien
 */
public class HttpLogger implements HttpLoggingInterceptor.Logger {
    private static String TAG = HttpLogger.class.getSimpleName();

    private StringBuilder mMessage = new StringBuilder();

    @Override
    public void log(String message) {
        if (isSkipMessage(message)) return;
        Log.i(TAG, message);
    }

    boolean isSkipMessage(String msg){
        if (msg.contains("{")
            || msg.contains("}")
            || msg.contains("[")
            || msg.contains("]")
            || msg.contains("-->")
            || msg.contains("<--")
            || msg.contains("message")
            || msg.contains("code")
        ){
            return false;
        }
        return true;
    }
}