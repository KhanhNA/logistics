package com.aggregatoricapaci.logistic.util;

import androidx.annotation.IntDef;

@IntDef({TypeSearch.SEARCH_DISTRIBUTOR,TypeSearch.SEARCH_MANUFACTURE,TypeSearch.SEARCH_CATEGORY,TypeSearch.SEARCH_SKU})
public @interface TypeSearch {
    int SEARCH_DISTRIBUTOR = 0;
    int SEARCH_MANUFACTURE = 1;
    int SEARCH_CATEGORY = 2;
    int SEARCH_SKU = 3;
}
