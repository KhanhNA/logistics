package com.aggregatoricapaci.logistic.ui.dialog;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.base.AppController;
import com.aggregatoricapaci.logistic.base.http.HttpHelper;
import com.aggregatoricapaci.logistic.databinding.DialogChangeConfigBinding;
import com.aggregatoricapaci.logistic.util.StringUtils;


@SuppressLint("ValidFragment")
public class DialogChangeConfig extends DialogFragment {

    public DialogChangeConfig(){
        super();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogChangeConfigBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_config, container, false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
        SharedPreferences.Editor editor = AppController.getInstance().getEditor();

        String urlLogin = sharedPreferences.getString("U_LOGIN", AppController.BASE_LOGIN_URL);
        String urlServer = sharedPreferences.getString("U_SERVER", AppController.MOBILE_SERVICE_URL);

        binding.edLogin.setText(urlLogin);
        binding.edServer.setText(urlServer);

        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        binding.btnCancel.setOnClickListener(v -> this.dismiss());
        binding.btnConfirm.setOnClickListener(v -> {
            if (StringUtils.isNotNullAndNotEmpty(binding.edLogin.getText().toString())) {
                editor.putString("U_LOGIN", binding.edLogin.getText().toString()).apply();
            }
            if (StringUtils.isNotNullAndNotEmpty(binding.edServer.getText().toString())) {
                editor.putString("U_SERVER", binding.edServer.getText().toString()).apply();
            }
            if (StringUtils.isNotNullAndNotEmpty(binding.edLogistic.getText().toString())) {
                editor.putString("U_LOGISTIC", binding.edLogistic.getText().toString()).apply();
            }
            AppController.BASE_LOGIN_URL = sharedPreferences.getString("U_LOGIN", AppController.BASE_LOGIN_URL);
            AppController.MOBILE_SERVICE_URL = sharedPreferences.getString("U_SERVER", AppController.MOBILE_SERVICE_URL);
            new HttpHelper.Builder(AppController.getInstance())
                    .initOkHttp()
                    .createRetrofit(AppController.MOBILE_SERVICE_URL)
                    .build();
            dismiss();
        });
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

}
