package com.aggregatoricapaci.logistic.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.aggregatoricapaci.logistic.MainActivity;
import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.adapter.SpinnerAdapter;
import com.aggregatoricapaci.logistic.base.AppController;
import com.aggregatoricapaci.logistic.databinding.ActivityLoginBinding;
import com.aggregatoricapaci.logistic.model.ApiResponse;
import com.aggregatoricapaci.logistic.model.dto.LanguageSpinner;
import com.aggregatoricapaci.logistic.service.listener.OnSingleClickListener;
import com.aggregatoricapaci.logistic.ui.dialog.DialogChangeConfig;
import com.aggregatoricapaci.logistic.util.ErrorCode;
import com.aggregatoricapaci.logistic.util.ImageUtils;
import com.aggregatoricapaci.logistic.util.IntentConstants;
import com.aggregatoricapaci.logistic.util.NetworkUtils;
import com.aggregatoricapaci.logistic.util.ToastUtils;
import com.aggregatoricapaci.logistic.viewmodel.LoginVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;
import com.tsolution.base.utils.LanguageUtils;
import com.workable.errorhandler.ErrorHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;


public class LoginActivity extends BaseActivity<ActivityLoginBinding> implements ActionsListener {
    private static final int FORGET_PASS_CODE = 111;
    private static final int REQUEST_INPUT_CODE = 222;
    private static final int REQUEST_CODE_ACTIVE = 0x9345;
    private LoginVM loginVM;
    private String inputCode = "";
    private CircularProgressButton btnLogin;
    private int check = 0;
    private final List<LanguageSpinner> lstLanguage = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginVM = (LoginVM) viewModel;
        btnLogin = binding.btnLogin;
        AppCompatSpinner spLanguage = binding.spin;
        loginVM.setMessageInputCodeFail(getString(R.string.INPUT_CODE_FAIL));
        loginVM.setMessageRequiteInviteCode(getString(R.string.requite_invite_code));
        loginVM.loginResponse().observe(this, this::consumeResponse);
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, lstLanguage);
        lstLanguage.add(LanguageSpinner.builder().idRes(R.drawable.ic_vietnam).languageName(R.string.VIETNAMMESE).build());
        lstLanguage.add(LanguageSpinner.builder().idRes(R.drawable.ic_united_kingdom).languageName(R.string.ENGLISH).build());
        lstLanguage.add(LanguageSpinner.builder().idRes(R.drawable.ic_myanmar).languageName(R.string.burmese).build());

        spLanguage.setAdapter(spinnerAdapter);
        spLanguage.setSelection(AppController.getInstance().getSharePre().getInt("current_language", 1));


        //
        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (++check > 1) {
                    try {
                        setLanguage(LanguageUtils.LANGUAGE_CODE[i]);
                        // LanguageUtils.changeLanguage(LoginActivity.this, LanguageUtils.LANGUAGE_CODE[i]);
                        AppController.getInstance().getSharePre().edit().putInt("current_language", i).apply();
                        //setLocale(LanguageUtils.LANGUAGE_CODE[i]);
                    } catch (Exception e) {
                        ErrorHandler.create().handle(e);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        TextWatcher loginTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String userName = binding.txtUserName.getText().toString().trim();
                String password = binding.txtPassword.getText().toString().trim();
                binding.btnLogin.setEnabled(!userName.isEmpty() && !password.isEmpty());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        binding.txtUserName.addTextChangedListener(loginTextWatcher);
        binding.txtPassword.addTextChangedListener(loginTextWatcher);
        binding.imgSettingServer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                DialogChangeConfig dialogChangeConfig = new DialogChangeConfig();
                dialogChangeConfig.show(getSupportFragmentManager(), "confirm_url");
            }
        });
    }

    private void setLocale(String s) {
        Locale myLocale = new Locale(s);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
//        Intent refresh = new Intent(this, LoginActivity.class);
//        finish();
//        startActivity(refresh);

        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        //setLanguage(s);
    }


    /**
     * Related to {@link LoginVM loginSuccess() }
     *
     * @param apiResponse
     */
    private void consumeResponse(ApiResponse apiResponse) {
        switch (apiResponse.status) {
            case LOADING:
//                progressDialog.show();
                break;

            case SUCCESS:
                AppController.getInstance().getEditor().putBoolean("chkRegister", false).apply();
                binding.txtLoginFail.setVisibility(View.GONE);
                hideKeyBoard();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
                break;

            case ERROR:
                String message = apiResponse.error == null ? "" : apiResponse.error.getMessage();
                if (ErrorCode.LOCKED.equals(message)) {
                    message = getResources().getString(R.string.not_permission);
                } else if (ErrorCode.USER_NOT_EXIST.equals(message)) {
                    // cannot find user
                    message = getResources().getString(R.string.userNotExistOrNotPermission);
                } else {
                    message = getResources().getString(R.string.login_fail);
                }
                binding.txtLoginFail.setText(message);
                binding.txtLoginFail.setVisibility(View.VISIBLE);
                btnLogin.doneLoadingAnimation(R.color.color_price, ImageUtils.drawableToBitmap(getResources().getDrawable(R.drawable.ic_progress_cancle)));
                new Handler().postDelayed(() -> btnLogin.revertAnimation(), 1000);
                break;
            case NOT_CONNECT:
                Toast.makeText(this, R.string.not_connect_server, Toast.LENGTH_SHORT).show();
                btnLogin.doneLoadingAnimation(R.color.color_price, ImageUtils.drawableToBitmap(getResources().getDrawable(R.drawable.ic_progress_cancle)));
                new Handler().postDelayed(() -> btnLogin.revertAnimation(), 1000);
                break;

            default:
                break;
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        switch (action) {
            case IntentConstants.INTENT_INPUT_FAIL:
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_login;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void action(Object... objects) {
        View v = (View) objects[0];
        if (R.id.btnLogin == v.getId()) {
            if (isValid()) {
                hideKeyBoard();
                if (!NetworkUtils.isNetworkConnected(this)) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                } else {
                    btnLogin.startAnimation();
                    new Handler().postDelayed(() -> loginVM.requestLogin(loginVM.getModelE().getUserName(), loginVM.getModelE().getPassWord()), 1000);
                }
            }
        }
        if (R.id.imgSettingServer == v.getId()) {
            DialogChangeConfig dialogChangeConfig = new DialogChangeConfig();
            dialogChangeConfig.show(getSupportFragmentManager(), "confirm_url");
        }
        if (R.id.back == v.getId()) {
            onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1111) {
            String result = data.getStringExtra("result_inputCode");
            String userNameParent = data.getStringExtra("result_inputCode_parentName");
        }
        if (requestCode == REQUEST_INPUT_CODE && resultCode == Activity.RESULT_OK) {
            inputCode = data.getStringExtra("inputCode");
        }
        if (requestCode == FORGET_PASS_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                binding.txtUserName.setText(data.getStringExtra("PHONE_NUMBER"));
            }
        }
    }


    private boolean isValid() {
        if (loginVM.getModelE().getUserName().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_username));
            return false;
        } else if (loginVM.getModelE().getPassWord().trim().isEmpty()) {
            ToastUtils.showToast(getResources().getString(R.string.enter_valid_password));
            return false;
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        btnLogin.dispose();
    }
}
