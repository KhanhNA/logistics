package com.aggregatoricapaci.logistic.util;

import android.util.Log;

public class TsLog {

    public static void d(String tag, String msg, Throwable tr){
            Log.d(tag, msg, tr);
    }

    public static void e(String tag, String msg, Throwable tr){
        Log.e(tag, msg, tr);
    }

    public static void e(String tag, String msg){
        Log.e(tag, msg);
    }


    public static void i(String tag, String msg, Throwable tr){
            Log.i(tag, msg, tr);
    }


    public static void v(String tag, String msg, Throwable tr){
            Log.v(tag, msg, tr);
    }


    public static void w(String tag, String msg, Throwable tr){
            Log.w(tag, msg, tr);
    }

    public static String printStackTrace(Throwable e) {
        String report = "";
        if (e != null) {
            report = e.toString() + "\n\n";
            StackTraceElement[] arr = e.getStackTrace();

            report += "--------- Stack trace ---------\n\n";
            for (int i = 0; i < arr.length; i++) {
                report += "    " + arr[i].toString() + "\n";
            }
            report += "-------------------------------\n\n";

            // If the exception was thrown in a background thread inside
            // AsyncTask, then the actual exception can be found with getCause
            report += "--------- Cause ---------\n\n";
            Throwable cause = e.getCause();
            if (cause != null) {
                report += cause.toString() + "\n\n";
                arr = cause.getStackTrace();
                for (int i = 0; i < arr.length; i++) {
                    report += "    " + arr[i].toString() + "\n";
                }
            }
            report += "-------------------------------\n\n";
        }
        return report;
    }
}
