package com.aggregatoricapaci.logistic.util;

import android.widget.ImageView;

import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.base.GlideApp;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.AlbumLoader;

/**
 * @author Good_Boy
 */
public class MediaLoader implements AlbumLoader {

    @Override
    public void load(ImageView imageView, AlbumFile albumFile) {
        load(imageView, albumFile.getPath());
    }

    @Override
    public void load(ImageView imageView, String url) {
        GlideApp.with(imageView.getContext())
                .load(url)
                .error(R.drawable.ic_stub)
                .placeholder(R.drawable.ic_stub)
                .transition(new DrawableTransitionOptions().crossFade())
                .into(imageView);
    }
}