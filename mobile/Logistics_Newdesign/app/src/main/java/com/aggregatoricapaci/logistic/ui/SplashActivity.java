package com.aggregatoricapaci.logistic.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.aggregatoricapaci.logistic.MainActivity;
import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.base.AppController;
import com.aggregatoricapaci.logistic.model.ApiResponse;
import com.aggregatoricapaci.logistic.util.Constants;
import com.aggregatoricapaci.logistic.util.StringUtils;
import com.aggregatoricapaci.logistic.viewmodel.LoginVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.utils.LanguageUtils;

public class SplashActivity extends BaseActivity {
    Bundle extras;
    LoginVM loginVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginVM = (LoginVM) viewModel;
        int defaultLanguage = AppController.getInstance().getSharePre().getInt("current_language", 1);
        LanguageUtils.changeLanguage(SplashActivity.this, LanguageUtils.LANGUAGE_CODE[defaultLanguage]);
        //
        if (getIntent() != null) {
            extras = getIntent().getExtras();
        }
        //
        SharedPreferences preferences = AppController.getInstance().getSharePre();
        if (preferences != null) {
            String userName = preferences.getString(Constants.USER_NAME, "");
            String pass = preferences.getString(Constants.MK, "");
            if (StringUtils.isNotNullAndNotEmpty(userName) && StringUtils.isNotNullAndNotEmpty(pass)) {
                loginVM.requestLogin(userName, pass);
            } else {
                startLoginGuest();
            }
        } else {
            startLoginGuest();
        }
        loginVM.loginResponse().observe(this, this::consumeResponse);

    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LanguageUtils.setLocale(SplashActivity.this);
    }

    private void startLoginGuest() {

        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();

        // close splash activity
    }

    private void consumeResponse(ApiResponse apiResponse) {
        Intent intent;
        switch (apiResponse.status) {
            case SUCCESS:
                AppController.getInstance().getEditor().putBoolean("chkRegister", false).apply();
                intent = new Intent(SplashActivity.this, MainActivity.class);
                if (extras != null) {
                    intent.putExtras(extras);
                }
                startActivity(intent);
                finish();
                break;
            case NOT_CONNECT:
            case ERROR:
                intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
