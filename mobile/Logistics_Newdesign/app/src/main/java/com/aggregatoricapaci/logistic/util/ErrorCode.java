package com.aggregatoricapaci.logistic.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorCode {
    public static final String SYSTEM_ERROR = "SYSTEM_ERROR";
    public static final String INVALID_INPUT = "INVALID_INPUT";
    public static final String INVALID_TOKEN = "INVALID_TOKEN";
    public static final String INPUT_OVER_MAXIMUM = "INPUT_OVER_MAXIMUM";
    public static final String USER_NOT_EXIST = "USER_NOT_EXIST";
    public static final String FILE_ALREADY_EXISTS = "FILE_ALREADY_EXISTS";
    public static final String FILE_NOT_EXISTS = "FILE_NOT_EXISTS";
    public static final String PRODUCT_NOT_FOUND = "PRODUCT_NOT_FOUND";
    public static final String DUPLICATE_COMMENT = "DUPLICATE_COMMENT";
    public static final String CONSTRAINT_VIOLATION = "CONSTRAINT_VIOLATION";
    public static final String MAX_COMMENT_IMAGES = "MAXIMUM_2_IMAGES";
    public static final String LOCKED = "Locked";
    // merchant
    public static final String MAX_GROUP_MERCHANT = "MAXIMUM_1_GROUP";
    public static final String MERCHANT_DOES_NOT_EXIST = "MERCHANT_DOES_NOT_EXIST";
    public static final String ALREADY_REGISTER_PHONE_NUMBER = "ALREADY_REGISTER_PHONE_NUMBER";
    public static final String WRONG_OTP_CODE = "WRONG_OTP_CODE";

    // Incentive
    public static final String TO_TIME_ARE_NOT_THE_SAME = "TO_TIME_ARE_NOT_THE_SAME";
    public static final String WRONG_TIME_RANGE = "WRONG_TIME_RANGE";

    // MytelPay
    public static final String PAYMENT_METHOD_MUST_BE_THE_SAME = "PAYMENT_METHOD_MUST_BE_THE_SAME";
    public static final String USER_NAME_MUST_BE_THE_SAME = "USER_NAME_MUST_BE_THE_SAME";
    public static final String ORDER_NOT_FOUND = "ORDER_NOT_FOUND";
    public static final String WRONG_HEADER_SIGNATURE = "WRONG_HEADER_SIGNATURE";
    public static final String PAYMENT_AMOUNT_NOT_MATCH = "PAYMENT_AMOUNT_NOT_MATCH";
    public static final String TRANSACTION_FAILED = "TRANSACTION_FAILED";
    public static final String USER_DOES_NOT_HAVE_PAYMENT_ACCOUNT = "USER_DOES_NOT_HAVE_PAYMENT_ACCOUNT";

    // Order
    public static final String ORDER_DELIVERED = "ORDER_DELIVERED";
    public static final String ORDER_CANCELED = "ORDER_CANCELED";
    public static final String ORDER_WAITING_PAYMENT = "ORDER_IS_WAITING_FOR_PAYMENT";
    public static final String COUPON_CODE_NOT_QUALIFIED = "COUPON_CODE_NOT_QUALIFIED";

}
