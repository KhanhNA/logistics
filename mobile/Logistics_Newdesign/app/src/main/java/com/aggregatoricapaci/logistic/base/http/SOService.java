package com.aggregatoricapaci.logistic.base.http;

import com.aggregatoricapaci.logistic.model.dto.MerchantDto;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SOService {
    @GET("merchant/user")
    Flowable<MerchantDto> getMerchantByUserName(@Query("userName") String userName);
}
