package com.aggregatoricapaci.logistic.util;

public enum OrderActionType {
    NO_ACTION(0),
    CANCEL_ORDER(1),
    RETURN_ORDER(2),
    REJECT_CANCEL(3),
    REJECT_RETURN_ORDER(4), PENDING(5),
    REJECT_MERCHANT_ORDER(6);

    private final int value;

    OrderActionType(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

}
