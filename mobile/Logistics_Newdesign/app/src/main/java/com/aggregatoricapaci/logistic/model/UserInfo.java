package com.aggregatoricapaci.logistic.model;

import com.tsolution.base.BaseModel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserInfo extends BaseModel {
    private String userName;
    private String passWord;
    private Boolean isSave;
    private String langCode;
    private Integer langId;
    public UserInfo() {
    }

    public UserInfo(String userName, String passWord, Boolean isSave, String lang, Integer langId) {
        this.userName = userName;
        this.passWord = passWord;
        this.isSave = isSave;
        this.langCode = lang;
        this.langId = langId;

    }
}
