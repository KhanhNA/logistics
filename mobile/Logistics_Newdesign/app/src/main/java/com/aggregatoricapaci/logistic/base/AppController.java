
package com.aggregatoricapaci.logistic.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.multidex.MultiDex;

import com.aggregatoricapaci.logistic.BuildConfig;
import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.model.dto.MerchantDto;
import com.aggregatoricapaci.logistic.util.Constants;
import com.aggregatoricapaci.logistic.util.MediaLoader;
import com.aggregatoricapaci.logistic.util.StringUtils;
import com.aggregatoricapaci.logistic.util.ToastUtils;
import com.google.gson.Gson;
import com.rohitss.uceh.UCEHandler;
import com.tsolution.base.RetrofitClient;
import com.tsolution.base.utils.LanguageUtils;
import com.tsolution.base.utils.locale.LocaleUtils;
import com.workable.errorhandler.ErrorHandler;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.tsolution.base.utils.LanguageUtils.LANGUAGE_CODE;

public class AppController extends Application {
    private static AppController mInstance;
    public static final String BASE_URL = "https://mfunctions.com";
    public static String MOBILE_SERVICE_URL = BASE_URL + ":8234/api/v1/";
    public static String BASE_LOGIN_URL = BASE_URL + ":9999";
    public static final String BASE_IMAGE = MOBILE_SERVICE_URL + "files/";
    public static String URL_PDF = MOBILE_SERVICE_URL + "filesPdf/";
    public static String URL_BILLING_FILE = BASE_URL + ":8688/files/";
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDate_v2 = new SimpleDateFormat(AppController.YYYY_MM_DD);
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat formatDate_v3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS'Z'");

    public static final String MERCHANT = "MERCHANT";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_PATTERN_GSON = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String FORMAT_CURRENCY = "#,###.##";
    Locale locale = new Locale("en", "UK");
    DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
    public static final String CURRENCY_UNIT = " MMK";
    public static long expiresTimeToken = 0L;
    private SharedPreferences sharedPreferences;
    private DecimalFormat formatter;
    HashMap<String, Object> clientCache;
    private SimpleDateFormat formatDate;
    private Bundle bundle;

    static {
        RetrofitClient.BASE_URL = MOBILE_SERVICE_URL;
        RetrofitClient.BASE_URL_OAUTH = BASE_LOGIN_URL;
        RetrofitClient.clientId = BuildConfig.CLIENT_ID;
        RetrofitClient.clientSecret = BuildConfig.CLIENT_SECRET;
        RetrofitClient.DATE_FORMAT = DATE_PATTERN_GSON;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        sharedPreferences = getSharedPreferences(BuildConfig.CLIENT_SECRET, Context.MODE_PRIVATE);
        //setLanguage();
        clientCache = new HashMap<>();
        /* Glide */
        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .setLocale(getSavedLocal())
                .build()
        );
        //
        // Handler exception
        new UCEHandler.Builder(this).setUCEHEnabled(true).build();
        ErrorHandler
                .defaultErrorHandler()
                // Always log to a crash/error reporting service
                .always((throwable, errorHandler) -> {
                    throwable.printStackTrace();
                    ToastUtils.showToast("error: " + throwable.getMessage());
                });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    public void setLanguage() {
        Locale savedLocal = getSavedLocal();
        LocaleUtils.setLocale(savedLocal);
    }
    public static void putCurrentMerchant(MerchantDto merchantDto) {
        AppController.getInstance().putCache(AppController.MERCHANT, merchantDto);
        // Save merchantDto to SharedPreferences
        Gson gson = new Gson();
        AppController.getInstance()
                .getSharePre().edit()
                .putString(
                        Constants.SAVED_MERCHANT_DTO,
                        gson.toJson(merchantDto, MerchantDto.class)
                ).apply();
    }
    public Locale getSavedLocal() {
        int savedLocal = sharedPreferences
                .getInt("current_language", 1);
        String localeStr = null;
        if (savedLocal < LANGUAGE_CODE.length) {
            localeStr = LANGUAGE_CODE[savedLocal];
        }
        Locale locale;
        if (StringUtils.isNullOrEmpty(localeStr)) {
            locale = new Locale("en");
        } else {
            String[] localeCode = localeStr.split("-");
            if (localeCode.length > 1) locale = new Locale(localeCode[0], localeCode[1]);
            else locale = new Locale(localeCode[0]);
        }
        return locale;
    }

    public String getLanguageCode() {
        return LanguageUtils.LANGUAGE_CODE[sharedPreferences.getInt("current_language", 1)];
    }



    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public SharedPreferences getSharePre() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(BuildConfig.CLIENT_SECRET, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences(BuildConfig.CLIENT_SECRET, Context.MODE_PRIVATE);
        }
        return sharedPreferences.edit();
    }

    /**
     * save object vao file
     *
     * @author: PhamBien
     * @return: void
     * @throws:
     */
    public void saveObject(String fileName, Object object) {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), fileName);
            if (!file.exists()) {
                file.mkdirs();
            }
            FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch (Exception e) {
            Log.e("Error: ", "Loi write file: " + e.getMessage());
        }
    }

    /**
     * Kiem tra file ton tai hay khong
     *
     * @author PhamBien
     */
    public static boolean isExistFile(String fileName) {
        try {
            if (!StringUtils.isNullOrEmpty(fileName)) {
                String[] s = AppController.getInstance()
                        .fileList();
                for (String s1 : s) {
                    if (fileName.equals(s1)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            Log.w("File", "" + e.getMessage());
        }
        return false;
    }

    public String formatInteger(Integer remain) {
        if (remain == null) {
            return "0";
        }
        if (formatter == null) {
            formatter = new DecimalFormat(FORMAT_CURRENCY, symbols);
        }

        return formatter.format(remain);
    }

    public String formatFloat(float remain) {
        if (remain == 0) {
            return "0";
        }
        if (formatter == null) {
            formatter = new DecimalFormat(FORMAT_CURRENCY, symbols);
        }

        return formatter.format(remain);
    }

    public String formatDecimal(BigDecimal money) {
        if (money == null) {
            return "0";
        }
        if (formatter == null) {
            formatter = new DecimalFormat(FORMAT_CURRENCY, symbols);
        }

        return formatter.format(money);
    }



    public String formatCurrency(BigDecimal money) {
        if (money == null) {
            return formatCurrency((Double) null);
        }
        return formatCurrency(money.doubleValue());
    }

    public String formatCurrency(Double money) {
        if (money == null) {
            return "0" + CURRENCY_UNIT;
        }
        if (formatter == null) {

            formatter = new DecimalFormat(FORMAT_CURRENCY, symbols);
        }
        return formatter.format(money) + CURRENCY_UNIT;
    }

    public String formatCurrency(BigDecimal money, String unit) {
        if (money == null) {
            return formatCurrency((Double) null);
        }
        return formatCurrency(money.doubleValue(), unit);
    }

    public String formatCurrency(Double money, String unit) {
        if (money == null) {
            return "0" + CURRENCY_UNIT;
        }
        if (formatter == null) {

            formatter = new DecimalFormat(FORMAT_CURRENCY, symbols);
        }
        return formatter.format(money) + unit;
    }

    public String formatCurrencyNotCurrency(Double money) {
        if (money == null) {
            return "0";
        }
        if (formatter == null) {

            formatter = new DecimalFormat(FORMAT_CURRENCY, symbols);
        }

        return formatter.format(money);
    }

    public String formatCurrencyNotCurrency(BigDecimal money) {
        if (money == null) {
            return formatCurrencyNotCurrency((Double) null);
        }
        return formatCurrencyNotCurrency(money.doubleValue());
    }


    public void putCache(String key, Object obj) {
        if (clientCache == null) {
            clientCache = new HashMap<>();
        }
        clientCache.put(key, obj);
    }

    public Object getFromCache(String key) {
        if (clientCache == null) {
            return null;
        }
        return clientCache.get(key);
    }

//    public static Long getMerchantId() {
//        return getCurrentMerchant() != null ? getCurrentMerchant().getMerchantId() : null;
//    }

//    public static MerchantDto getCurrentMerchant() {
//        MerchantDto merchantDto = (MerchantDto) com.ts.dcommerce.base.AppController.getInstance().getFromCache(com.ts.dcommerce.base.AppController.MERCHANT);
//        if (merchantDto == null) {
//            // jump to fallback case, try to get from SharedPreferences to prevent crash app
//            String json = com.ts.dcommerce.base.AppController.getInstance()
//                    .getSharePre().getString(Constants.SAVED_MERCHANT_DTO, null);
//            if (json != null && !json.trim().isEmpty()) {
//                Gson gson = new Gson();
//                merchantDto = gson.fromJson(json, MerchantDto.class);
//            }
//        }
//        return merchantDto;
//    }
//
//    public static void putCurrentMerchant(MerchantDto merchantDto) {
//        AppController.getInstance().putCache(com.ts.dcommerce.base.AppController.MERCHANT, merchantDto);
//        // Save merchantDto to SharedPreferences
//        Gson gson = new Gson();
//       AppController.getInstance()
//                .getSharePre().edit()
//                .putString(
//                        Constants.SAVED_MERCHANT_DTO,
//                        gson.toJson(merchantDto, MerchantDto.class)
//                ).apply();
//    }

    @SuppressLint("DefaultLocale")
    public static String formatNumber(double number) {
        if (number >= 1000000000) {
            return String.format("%.2fT", number / 1000000000.0);
        }

        if (number >= 1000000) {
            return String.format("%.2fB", number / 1000000.0);
        }

        if (number >= 100000) {
            return String.format("%.2fM", number / 100000.0);
        }

        if (number >= 1000) {
            return String.format("%.2fK", number / 1000.0);
        }
        return String.valueOf(number);
    }

    public void clearCache() {
        clientCache = new HashMap<>();
        // Delete user & pass
        AppController.getInstance().getSharePre().edit().putString(Constants.USER_NAME, "").apply();
        AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
        AppController.getInstance().getSharePre().edit().putString(Constants.SAVED_MERCHANT_DTO, "").apply();
    }

    public static InputFilter[] getFilter() {
        InputFilter EMOJI_FILTER = (source, start, end, dest, dstart, dend) -> {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE || type == Character.NON_SPACING_MARK
                        || type == Character.OTHER_SYMBOL) {
                    return "";
                }
            }
            return null;
        };
        return new InputFilter[]{EMOJI_FILTER};
    }


    public void setIsFirstTimeLaunch(boolean isFirstTimeLaunch) {
        getEditor().putBoolean(Constants.IS_FIRST_TIME_LAUNCH, isFirstTimeLaunch).apply();
    }

    public boolean getIsFirstTimeLaunch() {
        return sharedPreferences.getBoolean(Constants.IS_FIRST_TIME_LAUNCH, true);
    }

    public static boolean isCharAllowed(char c) {
        return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
    }

    public String getPasswordWallet() {
        return sharedPreferences.getString(Constants.MK_WALLET, "");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
