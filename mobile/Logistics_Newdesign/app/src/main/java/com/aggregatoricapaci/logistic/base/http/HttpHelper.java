package com.aggregatoricapaci.logistic.base.http;

import android.content.Context;

import androidx.annotation.NonNull;

import com.aggregatoricapaci.logistic.BuildConfig;
import com.aggregatoricapaci.logistic.base.AppController;
import com.aggregatoricapaci.logistic.service.listener.RefreshTokenListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.tsolution.base.listener.ResponseResult;
import com.tsolution.base.utils.LanguageUtils;
import com.workable.errorhandler.ErrorHandler;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.aggregatoricapaci.logistic.util.TsUtils.checkNotNull;


/**
 * @author：PhamBien
 */
public class HttpHelper {

    private static volatile HttpHelper mHttpHelper = null;

    private static Retrofit mRetrofit;

    private SOService soService;

    public static String TOKEN_DCOM = " ";
    public static String REFRESH_TOKEN = "";

    private HttpHelper() {
    }

    public static HttpHelper getInstance() {
        if (mHttpHelper == null) {
            synchronized (HttpHelper.class) {
                if (mHttpHelper == null) {
                    mHttpHelper = new HttpHelper();
                }
            }
        }
        return mHttpHelper;
    }

    private void build(Builder builder) {
        checkNotNull(builder);
        checkNotNull(builder.mBuilder);
        checkNotNull(builder.mOkHttpClient);
        checkNotNull(builder.mRetrofit);
        mRetrofit = builder.mRetrofit;
        soService = mRetrofit.create(SOService.class);
    }

    public SOService getApi() {
        checkNotNull(SOService.class);
        if (mRetrofit == null) {
            buildRetrofitInternal();
        }
        if (soService == null) {
            soService = mRetrofit.create(SOService.class);
        }
        return soService;
    }

    void buildRetrofitInternal() {
        new Builder(AppController.getInstance())
                .initOkHttp()
                .createRetrofit(AppController.MOBILE_SERVICE_URL)
                .build();
    }


    public static void requestLogin(String userName, String password, ResponseResult result) {
        String x = Credentials.basic(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET);

        FormBody.Builder builder = new FormBody.Builder();
        builder.add("grant_type", "password");
        builder.add("username", userName.trim());
        builder.add("password", password.trim());

        final Request request = new Request.Builder()
                .url(AppController.BASE_LOGIN_URL + "/oauth/token").addHeader("origin", "abc")
                .addHeader("Authorization", x)
                .addHeader("Accept", "application/json, text/plain, */*")
                .addHeader("Content-type", "application/x-www-form-urlencoded")
                .addHeader("Accept-Language", "" + LanguageUtils.languageId)
                .post(builder.build())
                .build();

        OkHttpClient.Builder builder1 = new OkHttpClient.Builder();

        builder1.hostnameVerifier((s, sslSession) -> true)
                .build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                result.onResponse(null, null, e, null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                String token = response.body() == null ? "" : response.body().string();
                Type type = new TypeToken<Map<String, String>>() {
                }.getType();
                Gson gson = new Gson();
                final Map<String, String> myMap;
                if (response.isSuccessful()) {
                    myMap = gson.fromJson(token, type);
                    TOKEN_DCOM = myMap.get("token_type") + " " + myMap.get("access_token");
                    REFRESH_TOKEN = myMap.get("refresh_token");
                    new Builder(AppController.getInstance())
                            .initOkHttp()
                            .createRetrofit(AppController.MOBILE_SERVICE_URL)
                            .build();
                    ErrorHandler.create().run(() -> {
                        // Luu lai thoi diem lay token
                        AppController.expiresTimeToken = Long.parseLong(Objects.requireNonNull(myMap.get("expires_in"))) + System.currentTimeMillis() / 1000;
                    });

                    result.onResponse(null, null, token, null);
                } else {
                    String errorDes = null;
                    try {
                        myMap = gson.fromJson(token, type);
                        errorDes = myMap.get("error_description");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    result.onResponse(null, null, null, errorDes == null ? null : new Throwable(errorDes));
                }
            }
        });
    }

    public static void refreshLogin(RefreshTokenListener refreshTokenListener) {
        String x = Credentials.basic(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET);

        FormBody.Builder builder = new FormBody.Builder();
        builder.add("grant_type", "refresh_token");
        builder.add("refresh_token", REFRESH_TOKEN);

        final Request request = new Request.Builder()
                .url(AppController.BASE_LOGIN_URL + "/oauth/token").addHeader("origin", "abc")
                .addHeader("Authorization", x)
                .addHeader("Accept", "application/json, text/plain, */*")
                .addHeader("Content-type", "application/x-www-form-urlencoded")
                .post(builder.build())
                .build();

        OkHttpClient.Builder builder1 = new OkHttpClient.Builder();

        builder1.hostnameVerifier((s, sslSession) -> true)
                .build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String token = response.body().string();
                    Type type = new TypeToken<Map<String, String>>() {
                    }.getType();
                    Map<String, String> myMap = new Gson().fromJson(token, type);
                    TOKEN_DCOM = myMap.get("token_type") + " " + myMap.get("access_token");
                    new Builder(AppController.getInstance())
                            .initOkHttp()
                            .createRetrofit(AppController.MOBILE_SERVICE_URL)
                            .build();
                    // Luu lai thoi diem lay token
                    AppController.expiresTimeToken = Long.parseLong(Objects.requireNonNull(myMap.get("expires_in"))) + System.currentTimeMillis() / 1000;
                    refreshTokenListener.refreshTokenSuccess();
                }

            }
        });
    }

    public static class Builder {
        private OkHttpClient mOkHttpClient;

        private OkHttpClient.Builder mBuilder;

        private Retrofit mRetrofit;
        private Retrofit mRetrofitLogistic;
        private Retrofit mRetrofitBilling;

        private Context mContext;

        public Builder(Context context) {
            this.mContext = context;
        }

        /**
         * create  OKHttpClient
         *
         * @return Builder
         */
        public Builder initOkHttp() {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLogger());
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            synchronized (HttpHelper.class) {
                if (mBuilder == null) {
                    Cache cache = new Cache(new File(mContext.getCacheDir(), "HttpCache"), 1024 * 1024 * 10);
                    mBuilder = new OkHttpClient.Builder()
                            .cache(cache)
                            .addInterceptor(interceptor)
                            .hostnameVerifier((s, sslSession) -> true)
                            .connectTimeout(30, TimeUnit.SECONDS)
                            .writeTimeout(30, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .addInterceptor(chain -> {
                                Request request = chain.request();
                                Request.Builder newRequest = request.newBuilder().addHeader("Authorization", TOKEN_DCOM)
                                        .addHeader("Content-Type", "application/json")
                                        .addHeader("Accept-Language", "" + LanguageUtils.languageId);
                                return chain.proceed(newRequest.build());
                            });
                }
            }
            return this;
        }


        /**
         * create retrofit
         *
         * @param baseUrl baseUrl
         * @return Builder
         */
        public Builder createRetrofit(String baseUrl) {
            checkNotNull(baseUrl);
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat(AppController.DATE_PATTERN_GSON);
            Retrofit.Builder builder = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .baseUrl(baseUrl);
            this.mOkHttpClient = mBuilder.build();
            this.mRetrofit = builder.client(mOkHttpClient).build();
            return this;
        }


        public void build() {
            HttpHelper.getInstance().build(this);
        }
    }
}
