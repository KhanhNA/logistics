package com.aggregatoricapaci.logistic.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.aggregatoricapaci.logistic.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.luck.picture.lib.engine.ImageEngine;
import com.luck.picture.lib.listener.OnImageCompleteCallback;
import com.luck.picture.lib.tools.MediaUtils;
import com.luck.picture.lib.widget.longimage.ImageSource;
import com.luck.picture.lib.widget.longimage.ImageViewState;
import com.luck.picture.lib.widget.longimage.SubsamplingScaleImageView;

public class GlideEngine implements ImageEngine {
    private static GlideEngine instance;

    public void loadImage(@NonNull Context context, @NonNull String url, @NonNull ImageView imageView) {
        Glide.with(context).load(url).into(imageView);
    }

    public void loadImage(@NonNull Context context, @NonNull String url, @NonNull final ImageView imageView, final SubsamplingScaleImageView longImageView, final OnImageCompleteCallback callback) {
        Glide.with(context).asBitmap().load(url).into(new ImageViewTarget<Bitmap>(imageView) {
            public void onLoadStarted(@Nullable Drawable placeholder) {
                super.onLoadStarted(placeholder);
                if (callback != null) {
                    callback.onShowLoading();
                }

            }

            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                if (callback != null) {
                    callback.onHideLoading();
                }

            }

            protected void setResource(@Nullable Bitmap resource) {
                if (callback != null) {
                    callback.onHideLoading();
                }

                if (resource != null) {
                    boolean eqLongImage = MediaUtils.isLongImg(resource.getWidth(), resource.getHeight());
                    longImageView.setVisibility(eqLongImage ? View.GONE : View.VISIBLE);
                    imageView.setVisibility(eqLongImage ? View.GONE : View.VISIBLE);
                    if (eqLongImage) {
                        longImageView.setQuickScaleEnabled(true);
                        longImageView.setZoomEnabled(true);
                        longImageView.setPanEnabled(true);
                        longImageView.setDoubleTapZoomDuration(100);
                        longImageView.setMinimumScaleType(2);
                        longImageView.setDoubleTapZoomDpi(2);
                        longImageView.setImage(ImageSource.bitmap(resource), new ImageViewState(0.0F, new PointF(0.0F, 0.0F), 0));
                    } else {
                        imageView.setImageBitmap(resource);
                    }
                }

            }
        });
    }

    public void loadImage(@NonNull Context context, @NonNull String url, @NonNull final ImageView imageView, final SubsamplingScaleImageView longImageView) {
        Glide.with(context).asBitmap().load(url).into(new ImageViewTarget<Bitmap>(imageView) {
            protected void setResource(@Nullable Bitmap resource) {
                if (resource != null) {
                    boolean eqLongImage = MediaUtils.isLongImg(resource.getWidth(), resource.getHeight());
                    longImageView.setVisibility(eqLongImage ? View.VISIBLE : View.GONE);
                    imageView.setVisibility(eqLongImage ? View.GONE : View.VISIBLE);
                    if (eqLongImage) {
                        longImageView.setQuickScaleEnabled(true);
                        longImageView.setZoomEnabled(true);
                        longImageView.setPanEnabled(true);
                        longImageView.setDoubleTapZoomDuration(100);
                        longImageView.setMinimumScaleType(2);
                        longImageView.setDoubleTapZoomDpi(2);
                        longImageView.setImage(ImageSource.bitmap(resource), new ImageViewState(0.0F, new PointF(0.0F, 0.0F), 0));
                    } else {
                        imageView.setImageBitmap(resource);
                    }
                }

            }
        });
    }

    public void loadFolderImage(@NonNull final Context context, @NonNull String url, @NonNull final ImageView imageView) {
        ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) Glide.with(context).asBitmap().load(url).override(180, 180)).centerCrop()).sizeMultiplier(0.5F)).apply((new RequestOptions()).placeholder(R.drawable.ic_stub)).into(new BitmapImageViewTarget(imageView) {
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCornerRadius(8.0F);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    public void loadAsGifImage(@NonNull Context context, @NonNull String url, @NonNull ImageView imageView) {
        Glide.with(context).asGif().load(url).into(imageView);
    }

    public void loadGridImage(@NonNull Context context, @NonNull String url, @NonNull ImageView imageView) {
        ((RequestBuilder) ((RequestBuilder) Glide.with(context).load(url).override(200, 200)).centerCrop()).apply((new RequestOptions()).placeholder(R.drawable.ic_stub)).into(imageView);
    }

    private GlideEngine() {
    }

    public static GlideEngine createGlideEngine() {
        if (null == instance) {
            Class var0 = GlideEngine.class;
            synchronized (GlideEngine.class) {
                if (null == instance) {
                    instance = new GlideEngine();
                }
            }
        }

        return instance;
    }
}