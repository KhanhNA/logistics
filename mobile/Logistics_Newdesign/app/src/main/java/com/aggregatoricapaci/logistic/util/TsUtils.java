package com.aggregatoricapaci.logistic.util;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class TsUtils {
    static DecimalFormat df;
    public static List safe(List other) {

        return other == null ? Collections.EMPTY_LIST : other;
    }

    public static boolean isNotNull(List other) {
        if (other != null && other.size() > 0) {
            return true;
        }
        return false;
    }

    public static boolean isObjectNotNull(Object other) {
        return other != null;
    }

    public static <T> T getData(Object src, Class<T> clazz, HashMap<String, Field> destFields) throws Exception {
        Field[] fields = src.getClass().getDeclaredFields();
        T dest = clazz.newInstance();
        Field fieldDest;
        if (destFields == null) { //== null nghia la chi dung cho noi tai trong ham
            destFields = new HashMap<>();
        }
        for (Field field : dest.getClass().getDeclaredFields()) {
            destFields.put(field.getName(), field);
        }

        for (Field field : fields) {
            int modifier = field.getModifiers();
            if (Modifier.isStatic(modifier) || Modifier.isFinal(modifier)) {
                continue;
            }

            field.setAccessible(true);
            Object value = field.get(src);


            fieldDest = destFields.get(field.getName());
            if (fieldDest == null || fieldDest.getType() != field.getType()) {
                continue;
            }
            fieldDest.setAccessible(true);
            fieldDest.set(dest, value);


        }
        return dest;
    }

    public static double calculatePercentage(double obtained, double total) {
        return obtained * 100 / total;
    }

    public static BigDecimal calculatePercentage(BigDecimal base, BigDecimal pct) {
        return base.multiply(pct).divide(new BigDecimal(100), RoundingMode.HALF_UP);
    }

    public static @NonNull
    <T> T checkNotNull(final T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }

    public static DecimalFormat getFormatDecimal() {
        if (df == null) {
            df = new DecimalFormat("#.##");
        }
        df.setRoundingMode(RoundingMode.CEILING);
        return df;
    }
    public static void hideSystemUI(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        //| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
    public static void showSystemUI(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public static void hideSoftKeyboard(Activity activity) {
       try{
           InputMethodManager inputMethodManager =
                   (InputMethodManager) activity.getSystemService(
                           Activity.INPUT_METHOD_SERVICE);
           inputMethodManager.hideSoftInputFromWindow(
                   activity.getCurrentFocus().getWindowToken(), 0);
       }
       catch (Exception e){
           e.printStackTrace();
       };
    }
}
