package com.aggregatoricapaci.logistic.base.http;

public class ErrorCode {
    // general
    public static final String INVALID_INPUT = "INVALID_INPUT";
    public static final String INPUT_OVER_MAXIMUM = "INPUT_OVER_MAXIMUM";
    public static final String USER_NOT_EXIST = "USER_NOT_EXIST";
    // product comment
    public static final String PRODUCT_NOT_FOUND = "PRODUCT_NOT_FOUND";
    public static final String DUPLICATE_COMMENT = "DUPLICATE_COMMENT";
    public static final String CONSTRAINT_VIOLATION = "CONSTRAINT_VIOLATION";
    public static final String MAX_COMMENT_IMAGES = "MAXIMUM_2_IMAGES";

    // merchant
    public static final String MAX_GROUP_MERCHANT = "MAXIMUM_1_GROUP";
    public static final String MERCHANT_DOES_NOT_EXIST = "MERCHANT_DOES_NOT_EXIST";

    // Login
    public static final String UNAUTHORIZED = "unauthorized";
    public static final String LOCKED = "Locked";
    public static final int UNAUTHORIZED_CODE = 401;

    // MytelPay
    public static final String PAYMENT_METHOD_MUST_BE_THE_SAME = "PAYMENT_METHOD_MUST_BE_THE_SAME";
    public static final String USER_NAME_MUST_BE_THE_SAME = "USER_NAME_MUST_BE_THE_SAME";
    public static final String ORDER_NOT_FOUND = "ORDER_NOT_FOUND";
    public static final String WRONG_HEADER_SIGNATURE = "WRONG_HEADER_SIGNATURE";
    public static final String PAYMENT_AMOUNT_NOT_MATCH = "PAYMENT_AMOUNT_NOT_MATCH";
    public static final String WRONG_ORDER_STATUS = "WRONG_ORDER_STATUS";
    public static final String TRANSACTION_FAILED = "TRANSACTION_FAILED";
    public static final String USER_DOES_NOT_HAVE_PAYMENT_ACCOUNT = "USER_DOES_NOT_HAVE_PAYMENT_ACCOUNT";

    // Order
    public static final String ORDER_DELIVERED = "ORDER_DELIVERED";
    public static final String COUPON_CODE_NOT_QUALIFIED = "COUPON_CODE_NOT_QUALIFIED";
    public static final String WRONG_USER_NAME = "WRONG_USER_NAME";
    public static final String ORDER_CANCELED = "ORDER_CANCELED";
    public static final String ORDER_RETURNED = "ORDER_RETURNED";
    public static final String ORDER_WAITING_PAYMENT = "ORDER_IS_WAITING_FOR_PAYMENT";
    public static final String ORDER_WAITING_CONFIRMATION = "ORDER_IS_WAITING_FOR_CONFIRMATION";
}
