package com.aggregatoricapaci.logistic.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class LanguageSpinner {
    private int idRes;
    private int  languageName;
    private String languageCode;
}
