package com.aggregatoricapaci.logistic.util;

public class OrderStatusType {
    public static final int PENDING = 0;
    public static final int PROCESSED = 1;
    public static final int DELIVERING = 2;
    public static final int DELIVERED = 3;
    public static final int COMPLETE = 4;
    public static final int CANCELED = 5;
    public static final int RETURN = 6;
    public static final int WAITING_PAY = 7;

}
