package com.aggregatoricapaci.logistic.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.base.AppController;
import com.aggregatoricapaci.logistic.base.http.HttpHelper;
import com.aggregatoricapaci.logistic.base.http.rx.RxSchedulers;
import com.aggregatoricapaci.logistic.base.network.rx.RxSubscriber;
import com.aggregatoricapaci.logistic.model.ApiResponse;
import com.aggregatoricapaci.logistic.model.UserInfo;
import com.aggregatoricapaci.logistic.model.dto.MerchantDto;
import com.aggregatoricapaci.logistic.util.Constants;
import com.aggregatoricapaci.logistic.util.ErrorCode;
import com.aggregatoricapaci.logistic.util.ToastUtils;
import com.google.gson.Gson;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.RetrofitClient;

import java.io.IOException;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;

@Getter
@Setter
public class LoginVM extends BaseViewModel<UserInfo> {
    private static final String TAG = "LoginVM";
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    private SharedPreferences sharedPreferences;
    private UserInfo userInfo;
    private SharedPreferences.Editor editor;

    private String messageInputCodeFail = "";
    private String messageRequiteInviteCode = "";
    public String MSG_ERROR;
    public int smsTimeReset;


    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }

    public LoginVM(@NonNull Application application) {
        super(application);
        userInfo = new UserInfo();
        sharedPreferences = AppController.getInstance().getSharePre();
        userInfo.setIsSave(sharedPreferences.getBoolean("chkSave", false));
        userInfo.setUserName(sharedPreferences.getString(Constants.USER_NAME, ""));
        userInfo.setPassWord(sharedPreferences.getString(Constants.MK, ""));
        model.set(userInfo);

        getConfigUrl();
        new HttpHelper.Builder(AppController.getInstance())
                .initOkHttp()
                .createRetrofit(AppController.MOBILE_SERVICE_URL)
                .build();
    }

    private void getConfigUrl() {
        AppController.BASE_LOGIN_URL = sharedPreferences.getString("U_LOGIN", AppController.BASE_LOGIN_URL);
        AppController.MOBILE_SERVICE_URL = sharedPreferences.getString("U_SERVER", AppController.MOBILE_SERVICE_URL);
    }


    public void requestLogin(String username, String password) {
        if (model.get() != null) {
            // Update user info
            getModelE().setUserName(username);
            getModelE().setPassWord(password);
            getConfigUrl();
            HttpHelper.requestLogin(username, password, this::loginSuccess);
        }
    }

    /**
     * @param call
     * @param response
     * @param o
     * @param throwable
     */

    private void loginSuccess(Call call, Response response, Object o, Throwable throwable) {
        if (o != null && !(o instanceof IOException)) {
            // Login success
            Gson gson = new Gson();
            String json = gson.toJson(getModelE());

            editor = AppController.getInstance().getEditor();
            editor.putString("UserInfo", json);

            MerchantDto dto = new MerchantDto();
            dto.setUserName(getModelE().getUserName());

            if (getModelE().getIsSave() != null && getModelE().getIsSave()) {
                editor.putString(Constants.MK, getModelE().getPassWord());
                editor.putBoolean("chkSave", true);
            } else {
                editor.putBoolean("chkSave", false);
                editor.putString(Constants.MK, "");
            }
            editor.commit();
            getUserInfo();
        } else if (o instanceof IOException) {
            // Not connected
            responseLiveData.postValue(ApiResponse.notConnect(new Throwable()));
            resetTokenWithoutGuestUser();
        } else {
            // "error": "unauthorized"
            if (throwable == null) throwable = new Throwable();
            responseLiveData.postValue(ApiResponse.error(throwable));
            resetTokenWithoutGuestUser();
        }

    }

    private void resetTokenWithoutGuestUser() {
        RetrofitClient.TOKEN = "";
        HttpHelper.TOKEN_DCOM = "";
    }


    private void getUserInfo() {
        callApi(HttpHelper.getInstance().getApi().getMerchantByUserName(getModelE().getUserName())
                .compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<MerchantDto>() {
                    @Override
                    public void onSuccess(MerchantDto merchantDto) {
                        getUserInfoSuccess(merchantDto);
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }
                }));
    }

    private void handleError(Throwable throwable) {
        String message = RxSubscriber.getMessage(throwable);
        if (ErrorCode.USER_NOT_EXIST.equals(message)) {
            responseLiveData.postValue(ApiResponse.error(new Throwable(message)));
        } else {
            responseLiveData.postValue(ApiResponse.notConnect(new Throwable()));
        }
        throwable.printStackTrace();
    }


    private void getUserInfoSuccess(MerchantDto result) {
        if (result != null) {
            if (result.getStatus() != 1 || result.getMerchantTypeId() != null && result.getMerchantTypeId() != 1 && result.getMerchantTypeId() != 2) {
                ToastUtils.showToast(R.string.not_permission);
                responseLiveData.postValue(ApiResponse.error(new Throwable()));
            } else if (result.getMerchantTypeId() != null) {
                SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
                String oldUserName = sharedPreferences.getString(Constants.USER_NAME, "");
                long merchantIdOldUser = sharedPreferences.getLong(Constants.MERCHANT_ID, 0);
                AppController.putCurrentMerchant(result);
                if (!getModelE().getUserName().equals(sharedPreferences.getString(Constants.USER_NAME, ""))) {
                    AppController.getInstance().getSharePre().edit().putString(Constants.MK_WALLET, "").apply();
                    // Delete token firebase
                }
                responseLiveData.postValue(ApiResponse.success(result));
                editor.putLong(Constants.MERCHANT_ID, result.getMerchantId()).apply();
                editor.putString(Constants.USER_NAME, getModelE().getUserName()).apply();
            }
        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
        }
    }


    private void loginGuestError(Throwable e) {
        getModelE().setUserName("");
        responseLiveData.postValue(ApiResponse.error(e));
        RetrofitClient.TOKEN = "";
        HttpHelper.TOKEN_DCOM = "";
    }
}
