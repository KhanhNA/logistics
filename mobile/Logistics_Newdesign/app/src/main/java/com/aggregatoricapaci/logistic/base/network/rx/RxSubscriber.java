package com.aggregatoricapaci.logistic.base.network.rx;

import android.content.Context;

import com.aggregatoricapaci.logistic.R;
import com.aggregatoricapaci.logistic.base.AppController;
import com.aggregatoricapaci.logistic.base.network.ServerException;
import com.aggregatoricapaci.logistic.util.NetworkUtils;
import com.aggregatoricapaci.logistic.util.ToastUtils;
import com.google.gson.JsonParseException;
import com.tsolution.base.dto.EventDTO;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.subscribers.DisposableSubscriber;
import okhttp3.internal.http2.ErrorCode;
import retrofit2.HttpException;

/**
 * @author PhamBien
 */
public abstract class RxSubscriber<T> extends DisposableSubscriber<T> {

    private Context context;

    public RxSubscriber() {
        super();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!NetworkUtils.isNetworkConnected(AppController.getInstance())) {
            showLoading();
            ToastUtils.showToast("No Internet");
            onNoNetWork();
            cancel();
        }
    }


    @Override
    public void onComplete() {

    }

    protected void showLoading() {
        // ToastUtils.showToast("Loading...");


    }

    protected void showProgress() {

    }

    protected void onNoNetWork() {
        onFailure("No internet", 400);
    }

    @Override
    public void onError(Throwable e) {
        String message = "";
        int code = -1;
        e.printStackTrace();
        if (e instanceof UnknownHostException) {
            message = "UnknownHostException";
        } else if (e instanceof HttpException) {
            try {
                JSONObject jsonObject = new JSONObject(((HttpException) e).response().errorBody().string());
                if (jsonObject.has("message")) {
                    message = jsonObject.getString("message");
                }
            } catch (Exception ex) {
                message = "HTTP 500";
            }
            code = ((HttpException) e).code();
        } else if (e instanceof SocketTimeoutException) {
            message = "SocketTimeoutException";
        } else if (e instanceof JsonParseException
                || e instanceof JSONException) {
            message = "JsonParseError: " + e.getMessage();
        } else if (e instanceof ConnectException) {
            message = "Not Connect Server";
        } else if (e instanceof ServerException) {
            message = ((ServerException) e).message;
            code = ((ServerException) e).code;
        } else {
            message = "Unknown";
        }
        if (code == 401) {
            EventBus.getDefault().post(EventDTO.builder()
                    .message(AppController.getInstance().getString(R.string.token_timeout))
                    .code(code)
                    .build());
        } else if (code != 400) {
//             ToastUtils.showToast(message); // Should show Toast outside this function
        }
        onFailure(message, code);
    }

    @Override
    public void onNext(T t) {
        onSuccess(t);
    }

    /**
     * success
     *
     * @param t
     */
    public abstract void onSuccess(T t);

    /**
     * failure
     *
     * @param msg
     */
    public void onFailure(String msg, int code) {
    }

    public static String getMessage(Throwable e) {
        String message = "";
        if (e instanceof UnknownHostException) {
            message = "UnknownHostException";
        } else if (e instanceof HttpException) {
            try {
                JSONObject jsonObject = new JSONObject(((HttpException) e).response().errorBody().string());
                if (jsonObject.has("message")) {
                    message = jsonObject.getString("message");
                }
            } catch (Exception ex) {
                message = "HTTP 500";
            }
        } else if (e instanceof SocketTimeoutException) {
            message = "SocketTimeoutException";
        } else if (e instanceof JsonParseException
                || e instanceof JSONException) {
            message = "JsonParseError: " + e.getMessage();
        } else if (e instanceof ConnectException) {
            message = "Not Connect Server";
        } else if (e instanceof ServerException) {
            message = ((ServerException) e).message;
        } else {
            message = "Unknown";
        }
        return message;
    }
}
