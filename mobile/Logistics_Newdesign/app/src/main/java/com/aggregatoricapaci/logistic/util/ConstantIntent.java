package com.aggregatoricapaci.logistic.util;

import androidx.annotation.StringDef;

@StringDef({ConstantIntent.PRODUCT_ID, ConstantIntent.PRODUCT_NAME, ConstantIntent.IMAGE_URL, ConstantIntent.REVIEW_COUNT})
public @interface ConstantIntent {
    String PRODUCT_ID = "product_id";
    String PRODUCT_NAME = "product_name";
    String IMAGE_URL = "image_url";
    String REVIEW_COUNT = "review_count";
}
