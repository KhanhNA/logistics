package com.aggregatoricapaci.logistic.util;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

public class FontManager {

    public static final String ROOT = "fonts/",
            FONTAWESOME = ROOT + "font_awesome_solid.otf";
    public static final String FONTAWESOME_PRO = ROOT + "font_awesome_pro.otf";

    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }
    @BindingAdapter("font")
    public static void setTypeFace(View v, String font){
        Typeface type = Typeface.createFromAsset(v.getContext().getAssets(), font);
        ((TextView) v).setTypeface(type);
    }
    public static void markAsIconContainer(View v, Typeface typeface) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View child = vg.getChildAt(i);
                markAsIconContainer(child, typeface);
            }
        } else if (v instanceof TextView) {
            ((TextView) v).setTypeface(typeface);
        }
    }

}