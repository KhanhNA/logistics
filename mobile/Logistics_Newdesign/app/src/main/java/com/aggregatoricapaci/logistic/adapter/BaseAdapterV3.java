package com.aggregatoricapaci.logistic.adapter;

import androidx.annotation.LayoutRes;
import androidx.databinding.ViewDataBinding;

import com.aggregatoricapaci.logistic.BR;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class BaseAdapterV3<T> extends BaseQuickAdapter<T, BaseDataBindingHolder> implements LoadMoreModule {
    private AdapterListener listenerAdapter;
    private BaseViewModel baseViewModel;

    public BaseAdapterV3(@LayoutRes int itemLayoutId) {
        super(itemLayoutId);
    }

    public BaseAdapterV3(int layoutResId, AdapterListener listenerAdapter) {
        super(layoutResId);
        this.listenerAdapter = listenerAdapter;
    }

    public BaseAdapterV3(@LayoutRes int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    public BaseAdapterV3(@LayoutRes int layoutResId, BaseViewModel baseViewModel, @Nullable List<T> data) {
        super(layoutResId, data);
        this.baseViewModel = baseViewModel;
    }

    public BaseAdapterV3(@LayoutRes int layoutResId, @Nullable List<T> data, AdapterListener listener) {
        super(layoutResId, data);
        this.listenerAdapter = listener;
    }

    public void setListenerAdapter(AdapterListener listenerAdapter) {
        this.listenerAdapter = listenerAdapter;
    }

    @Override
    protected void convert(@NotNull BaseDataBindingHolder holder, T o) {
        if (o instanceof BaseModel) {
            ((BaseModel) o).index = holder.getAdapterPosition() + 1;
        }
        //  Binding
        ViewDataBinding binding = holder.getDataBinding();
        if (binding != null) {
//            binding.setVariable(BR.viewHolder, o);
            if (baseViewModel != null) {
                binding.setVariable(BR.viewModel, baseViewModel);
            }
//            if (listenerAdapter != null) {
//                binding.setVariable(BR.listenerAdapter, this.listenerAdapter);
//            }
            binding.executePendingBindings();
        }
    }


}
