package com.aggregatoricapaci.logistic.util;

public interface IntentConstants {
    String INTENT_START_MAIN = "startMain";
    String INTENT_INPUT_SUCCES = "inputSucess";
    String INTENT_INPUT_FAIL = "inputFail";
    String INTENT_INPUT_CODE = "inputCode";
    String INTENT_REGISTER_SUCCESS = "registerSuccess";
    String INTENT_PHONE_NUMBER_CONFLICT = "PHONE_NUMBER_CONFLICT";
    String ITEM_ID = "itemId";
    String ADMINISTRATIVE_ID = "ADMINISTRATIVE_ID";
    String ADMINISTRATIVE_ACTION = "ADMINISTRATIVE_ACTION";
    String ADMINISTRATIVE_NAME = "ADMINISTRATIVE_NAME";
    String LIST_COUPON = "LIST_COUPON";
    String REJECT = "REJECT";
    String CATEGORY = "CATEGORY";
    String ORDER_TYPE = "ORDER_TYPE";
    String WRONG_ACTIVE_CODE = "WRONG_ACTIVE_CODE";
    String USED_ACTIVE_CODE = "USED_ACTIVE_CODE";
    String USED_SHARE_POLICY = "USED_SHARE_POLICY";
    String USER_NAME_ACTIVE = "USER_NAME_ACTIVE";
    String LOGIN_CLAIM_SUCCESS = "LOGIN_CLAIM_SUCCESS";
    String LOGIN_CLAIM_FAILED = "LOGIN_CLAIM_FAILED";

}
