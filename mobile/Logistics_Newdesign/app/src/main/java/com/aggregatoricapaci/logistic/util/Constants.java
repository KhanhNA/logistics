package com.aggregatoricapaci.logistic.util;

import static android.provider.ContactsContract.Directory.PACKAGE_NAME;

public class Constants {
    public static final String APP_INFO = "DCOMMERCE";
    public static final String TOKEN_INFO = "TOKEN_INFO";
    public static final String USER_NAME = "USER_NAME";
    public static final String MERCHANT_ID = "MERCHANT_ID";
    public static final String MK = "MK";
    public static final String MK_WALLET = "MK_WALLET";
    public static final String SHIP_COD = "SHIP_COD";
    public static final String MSTORE_PAY = "MSTORE_PAY";
    public static final String GO_STORE = "GO_STORE";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static final String chatAppId = "nextsolutions.chat";
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static int PAYLOAD_QUANTITY = 888;
    public static int PAYLOAD_CHECKBOX = 999;
    public static final String STRING_EMPTY = "";

    public enum CODE {
        SUCCESS(200), NOT_FOUND(404), ERROR_INTERNAL(500), AUTHEN_FAIL(401), USER_DEFIEND(-200), FATAL_ERROR(-1);

        private Integer code;

        CODE(Integer i) {
            code = i;
        }

        public Integer getCode() {
            return code;
        }
    }

    public final static String FIRST_USE = "FIRST_USE";
//    public static final int CODE_SUCCESS =200;
//    public static final int CODE_NOT_FOUND =404;
//    public static final int CODE_ERROR_INTERNAL =500;
//    public static final int CODE_AUTHEN_FAIL =401;

    public final static int METHOD_WALLET = 1;//tra bang vi
    public final static int METHOD_CARD = 2; // tra bang the
    public final static int METHOD_DIRECTORY = 3; //tra truc tiep

    public final static String SPACE = " ";

    public enum ORDER {
        PENDING(0), APPROVE(1), REJECT(2);
        private Integer code;

        ORDER(Integer i) {
            code = i;
        }

        public Integer getCode() {
            return code;
        }
    }

    //    public static final int ORDER_PENDING = 0;
//    public static final int ORDER_APPROVE = 1;
//    public static final int ORDER_REJECT = 2;
//    public static final int LANG_VI = 1;
    public static final int LANG_EN = 2;
    public static final long MERCHANT_ID_FAKE = 1;
    public static final int INCENTIVE = 1;
    public static final int SALE = 0;
    public static final int GET_DATA_MANUFACTURE = 1001;
    public static final int GET_DATA_CATEGORIES = 1002;

    public static final int GET_DATA_PRODUCT = 1003;
    public static final int GET_PRODUCT_DETAIL = 1004;
    public static final int GET_DATA_LIST_ORDER = 1005;
    public static final int GET_DATA_LIST_PRODUCT_ORDER_CART = 1020;
    public static final int INTENT_GET_RECEPTION_ADDRESS = 1021;
    public static final int INTENT_GET_METHOD_PAYMENT = 1022;
    public static final int INTENT_REJECT_SUCCESS = 177;

    public static final int GET_DATE_ORDER_DETAIL = 1006;
    public static final int REGISTER_MERCHANT = 1007;
    public static final int GET_DATA_MERCHANT = 1008;
    public static final int CONNECT_TIMEOUT = 30;
    public static final int READ_TIMEOUT = 30;
    public static final int WRITE_TIMEOUT = 30;

    public static boolean IS_REFRESH_ORDERED = false;
    public static boolean IS_REFRESH_ORDER_PENDING = false;
    public static boolean IS_REFRESH_ORDER_REJECT = false;

    public static final String SAVED_MERCHANT_DTO = "SAVED_MERCHANT_DTO";

    // Notify
    public static final String NUMBER_NOTIFY_UNREAD = "number_notify_unread";

    // VIEW ACTION

    public static final String ACTION_NOTIFY_DATA_SET_CHANGED = "NOTIFY_DATA_SET_CHANGED";
    public static final String ACTION_CART_EXIT_EDIT_MODE = "exitEditMode";
    public static final String ACTION_CONFIRM_ORDER_SUCCESS = "confirmOrderSuccess";
    public static final String ACTION_CONFIRM_ORDER_FAILED = "confirmOrderFail";
    public static final String ACTION_ORDER_CANCELED = "orderIsCanceled";
    public static final String ACTION_ORDER_WAITING_PAYMENT = "orderIsWaitingForPayment";
    public static final String ACTION_ORDER_NOT_CONFIRM = "orderIsNotConfirmed";
    public static final String ACTION_SEND_OTP_SUCCESS = "ACTION_SEND_OTP_SUCCESS";
    public static final String ACTION_SEND_OTP_FAILED = "ACTION_SEND_OTP_FAILED";
    public static final String ACTION_CHANGE_PASS_SUCCESS = "ACTION_CHANGE_PASS_SUCCESS";
    public static final String ACTION_CHANGE_PASS_FAILED = "ACTION_CHANGE_PASS_FAILED";
    public static final String ACTION_GET_DEPOSIT_PARAM_SUCCESS = "DEPOSIT_PARAM_SUCCESS";
    public static final String ACTION_GET_DEPOSIT_PARAM_FAILED = "DEPOSIT_PARAM_FAILED";
    public static final String ACTION_GET_PAYMENT_PARAM_FAILED = "PAYMENT_PARAM_FAILED";
    public static final String ACTION_UPDATE_ACCOUNT_SUCCESS = "UPDATE_ACCOUNT_SUCCESS";
    public static final String ACTION_UPDATE_ACCOUNT_FAILED = "UPDATE_ACCOUNT_FAILED";
    public static final String ACTION_ACTIVE_ACCOUNT_SUCCESS = "ACTION_ACTIVE_ACCOUNT_SUCCESS";
    public static final String ACTION_ACTIVE_ACCOUNT_FAILED = "ACTION_ACTIVE_ACCOUNT_FAILED";
    public static final String NEED_SALE_MAN_UPDATE_INFO = "NEED_SALE_MAN_UPDATE_INFO";
    public static final String WRONG_OTP_CODE = "WRONG_OTP_CODE";
    public static final String WRONG_ACTION = "WRONG_ACTION";
    public static final String USER_NOT_EXIST = "USER_NOT_EXIST";
    public static final String IS_FIRST_TIME_LAUNCH = "IS_FIRST_TIME_LAUNCH";
    public static final String MK_DF = "abc@123";
    //
    public static final String STR_BACK_TO_HOME = "backToHome";

    public static final int TIME_DELAY = 5000;
    public static final int TIME_PERIOD = 5000;
    public static final int CURRENT_SLIDE_DEFAULT = 0;

    public static final int ADMINISTRATIVE_ACTION_PROVINCE = 1;
    public static final int ADMINISTRATIVE_ACTION_DISTRICT = 2;
    public static final int ADMINISTRATIVE_ACTION_TOWNSHIP = 3;
    public static final int ADMINISTRATIVE_ACTION_WARD = 4;
    public static final int ADMINISTRATIVE_ID_PROVINCE = -1;

    // Extra
    public static final String EXTRA_PHONE_NUMBER = "PHONE_NUMBER";
    public static final String EXTRA_PASSWORD = "PASSWORD";
    public static final String EXTRA_TIME = "TIME";

    // Payment type
    public static final String EXTRA_PAYMENT_TYPE = "PAY_TYPE";
    public static final String EXTRA_DEPOSIT_AMOUNT = "DEPOSIT_AMOUNT";
    public static final String EXTRA_WALLET_ID = "WALLET_ID";
    public static final String EXTRA_MSISDN = "MSISDN";
    public static final String TYPE_PAY_ORDER = "PAY_ORDER";
    public static final String TYPE_DEPOSIT_WALLET = "DEPOSIT_WALLET";
    public static final String EXTRA_USERNAME = "EXTRA_USERNAME";
    public static final String EXTRA_WALLET_TYPE = "WALLET_TYPE";
    public static final String CUSTOM_DATA = "CUSTOM_DATA";
    public static final String URL_FILE = "URL_FILE";
    public static final String TITLE = "TITLE";
    public static final String FILE_NAME = "PATH";
    public static final String ACTION_ID = "actionId";
    public static final String SHOW_INPUT_CODE = "actionId";

}
