package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.models.Authority
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class Rule {

    @Test
    fun testRule() {
        val rule1 = Authority().apply { authority = "get/repacking-plannings/detail-repacked/{id}" }
        val rule2 = Authority().apply { authority = "post/repacking-plannings/print/{id}" }
        val rule3 = Authority().apply { authority = "patch/claims/reject/{id}" }
        val rule4 = Authority().apply { authority = "TKFC" }
        // AppUtils.rule.userAuthentication
//        RuleHelper().hasRules()
    }
}