package com.next_solutions.logistics.features.claim.claim_detail

import android.app.Application
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ClaimDetailViewModelTest {


    @Mock
    lateinit var application: Application

    lateinit var viewModel: ClaimDetailViewModel

    @Test
    fun init() {
        viewModel = ClaimDetailViewModel(application)
    }

    @Test
    fun acceptClaim() {
    }

    @Test
    fun rejectClaim() {
    }

    @Test
    fun canAcceptOrReject() {
    }

    @Test
    fun claimHasRejected() {
    }

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }
}