package com.next_solutions.logistics;

import com.google.gson.reflect.TypeToken;
import com.next_solutions.logistics.models.Rule;

public class FakeRemoteDataSource {
    private static TestDataFactory mTestDataFactory = new TestDataFactory();

    public static Rule getClientAccounts() {
        return TestDataFactory.getListTypePojo(new TypeToken<Rule>() {
        }, "");
    }
}
