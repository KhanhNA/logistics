package com.next_solutions.logistics.fake_data;

public class FindPoJsonName {
    public static final String FAKE_FROM_STORE = "features/po/search_po/stores.json";
    public static final String FAKE_FROM_DISTRIBUTOR = "features/po/search_po/distributors.json";
}
