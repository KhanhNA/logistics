package com.next_solutions.logistics.test;

public interface ExampleInterface {
    void function1();

    void function2();

    void otherFunction();

    void refresh();
}
