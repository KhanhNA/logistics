package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.models.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ImportStatementHelperTest {
    @Before
    fun setUp() {

    }

    @Test
    fun groupDetailByExpireDateAndPackingFromExportStatement() {

        val details = arrayListOf<ExportStatementDetail>()

        details.add(ExportStatementDetail().apply {
            expireDate = "2020-03-19 00:00:00.000Z"
            productPacking = ProductPacking().apply {
                id = 1
                code = "1"
            }
            storeProductPackingDetail = StoreProductPackingDetail().apply { code = "1" }
            quantity = 1000
        })

        details.add(ExportStatementDetail().apply {
            expireDate = "2020-03-29 00:00:00.000Z"
            productPacking = ProductPacking().apply {
                id = 2
                code = "2"
            }
            storeProductPackingDetail = StoreProductPackingDetail().apply { code = "2" }
            quantity = 2000
        })

        details.add(ExportStatementDetail().apply {
            expireDate = "2020-03-19 00:00:00.000Z"
            productPacking = ProductPacking().apply {
                id = 1
                code = "1"
            }
            storeProductPackingDetail = StoreProductPackingDetail().apply { code = "3" }
            quantity = 3000
        })

        details.add(ExportStatementDetail().apply {
            expireDate = "2020-03-10 00:00:00.000Z"
            productPacking = ProductPacking().apply {
                id = 4
                code = "4"
            }
            storeProductPackingDetail = StoreProductPackingDetail().apply { code = "4" }
            quantity = 4000
        })
        val importStatement = ImportStatement().apply {
            exportStatement = ExportStatement().apply {
                exportStatementDetails = details
            }
        }


        val importStatementHelper = ImportStatementHelper(importStatement)

        val grouped = importStatementHelper.groupDetailByExpireDateAndPackingFromExportStatement()

        Assert.assertEquals(3, grouped.size)
        Assert.assertEquals(4000L, grouped["119/03/2020"]?.quantity)

    }
}