package com.next_solutions.logistics.view_models;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.features.export_statement.export_statement_release.ExportStatementReleaseViewModel;
import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.repository.HttpHelper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static org.junit.Assert.assertNotNull;

public class ExportStatementReleaseViewModelTest {
    private ExportStatementReleaseViewModel viewModel;

    @Mock
    Application application;

    private String token;

    private List data;

    private String exportStatementCode;

    @Before
    public void setUp() {
        viewModel = new ExportStatementReleaseViewModel(application);
    }

    @Test
    public void setStatementCode() throws InterruptedException {
        login();
        testMaxResult();
        testEmptyResult();
        testSingleResult();
    }

    @Test
    public void search() {

    }

    private void login() throws InterruptedException {
        HttpHelper.requestLogin(new MutableLiveData<>(), "fc_admin", "abc@123", false, 1L, (call, response, o, throwable) -> this.token = (String) o);
        Thread.sleep(2000);
        assertNotNull(token);
    }

    private void testMaxResult() throws InterruptedException {
        viewModel.setStatementCode("2019");
        Thread.sleep(2000);
        data = viewModel.getBaseModelsE();
        exportStatementCode = ((ExportStatement) data.get(0)).getCode();

        Assert.assertNotNull(data);
        Assert.assertEquals(AppUtils.PAGE_SIZE, data.size());
    }

    private void testEmptyResult() throws InterruptedException {
        viewModel.setStatementCode("2018");
        Thread.sleep(2000);
        data = viewModel.getBaseModelsE();
        Assert.assertNotNull(data);
        Assert.assertEquals(0, data.size());
    }

    private void testSingleResult() throws InterruptedException {
        Assert.assertNotNull(exportStatementCode);
        viewModel.setStatementCode(exportStatementCode);
        Thread.sleep(2000);
        data = viewModel.getBaseModelsE();
        Assert.assertNotNull(data);
        Assert.assertEquals(1, data.size());
    }
}