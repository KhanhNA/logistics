package com.next_solutions.logistics.automation_test.features.login;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.next_solutions.logistics.JsonName;
import com.next_solutions.logistics.TestDataFactory;
import com.next_solutions.logistics.automation_test.Step;
import com.next_solutions.logistics.automation_test.util.Helper;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Login {
    private AndroidDriver<MobileElement> driver;
    private List<Step> steps;
    private List<Step> steps2;
    private List<Step> steps3;
    private List<Step> steps4;
    private List<Step> stepsStatus;


    @Before
    public void setUp() throws IOException {
        String step = TestDataFactory.readTestFile(JsonName.AUTOMATION_TEST_LOGIN);
        ObjectMapper objectMapper = new ObjectMapper();
        steps = objectMapper.readValue(step, new TypeReference<List<Step>>() {
        });

        String step2 = TestDataFactory.readTestFile(JsonName.AUTOMATION_TEST_OPEN_NAVIGATION);
        steps2 = objectMapper.readValue(step2, new TypeReference<List<Step>>() {
        });


        String step3 = TestDataFactory.readTestFile(JsonName.AUTOMATION_TEST_UPDATE_PALLET);
        steps3 = objectMapper.readValue(step3, new TypeReference<List<Step>>() {
        });

        String step4 = TestDataFactory.readTestFile(JsonName.AUTOMATION_TEST_UPDATE_PALLET_1);
        steps4 = objectMapper.readValue(step4, new TypeReference<List<Step>>() {
        });


        String step5 = TestDataFactory.readTestFile(JsonName.AUTOMATION_TEST_UPDATE_PALLET_2);
        stepsStatus = objectMapper.readValue(step5, new TypeReference<List<Step>>() {
        });

        String step6 = TestDataFactory.readTestFile(JsonName.AUTOMATION_TEST_DELIVERY_1);
        stepsStatus = objectMapper.readValue(step6, new TypeReference<List<Step>>() {
        });
    }

    @Test
    public void createExportStatement() throws InterruptedException, MalformedURLException {
        driver = Helper.getConnect();
//        HelperAction helperAction = new HelperAction(driver);
//        helperAction.login(steps);
//        helperAction.openNavigationMenu(steps2);
//        helperAction.testDelivery(stepsStatus);

//        ProductPacking productPacking1 = new ProductPacking();
//        productPacking1.id = 1L;
//        productPacking1.setCode("productPacking1");
//        ProductPacking productPacking2 = new ProductPacking();
//        productPacking2.id = 2L;
//        productPacking2.setCode("productPacking2");
//
//        ExportStatement exportStatement = new ExportStatement();
//        List<ExportStatementDetail> details = new ArrayList<>();
//
//        ExportStatementDetail statementDetail1 = new ExportStatementDetail();
//        statementDetail1.setExpireDate("1");
//        statementDetail1.setProductPacking(productPacking1);
//        statementDetail1.setQuantity(1000L);
//
//        ExportStatementDetail statementDetail2 = new ExportStatementDetail();
//        statementDetail2.setExpireDate("1");
//        statementDetail2.setProductPacking(productPacking1);
//        statementDetail2.setQuantity(2000L);
//
//        ExportStatementDetail statementDetail3 = new ExportStatementDetail();
//        statementDetail3.setExpireDate("3");
//        statementDetail3.setProductPacking(productPacking2);
//        statementDetail3.setQuantity(3000L);
//
//        ExportStatementDetail statementDetail4 = new ExportStatementDetail();
//        statementDetail4.setExpireDate("4");
//        statementDetail4.setProductPacking(productPacking2);
//        statementDetail4.setQuantity(4000L);
//
//
//        details.add(statementDetail1);
//        details.add(statementDetail2);
//        details.add(statementDetail3);
//        details.add(statementDetail4);
//        exportStatement.setExportStatementDetails(details);
//        StatementHelper statementHelper = new StatementHelper(exportStatement);
//        List<ExportStatementDetail> exportStatementDetails = statementHelper.groupDetailByExpireDateAndProductPacking();
//        Assert.assertEquals(3, exportStatementDetails.size());
    }
}
