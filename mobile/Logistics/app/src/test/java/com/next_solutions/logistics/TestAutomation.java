package com.next_solutions.logistics;


import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class TestAutomation {

    public static void main(String[] args) throws MalformedURLException, InterruptedException {
        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("deviceName", "SM-G955N");
//        cap.setCapability("deviceName", "Galaxy J5");
//
//        cap.setCapability("udid", "d888e44d");
        cap.setCapability("udid", "127.0.0.1:62025");
        cap.setCapability("platformName", "Android");
//        cap.setCapability("platformVersion", "9");
        cap.setCapability("platformVersion", "5.1.1");
        cap.setCapability("appPackage", "com.next_solutions.logistics");
        cap.setCapability("appActivity", "com.next_solutions.logistics.features.login.SplashActivity");

        URL url = new URL("http://localhost:4723/wd/hub");
        AndroidDriver<MobileElement> driver = new AndroidDriver<>(url, cap);

        MobileElement username = driver.findElementById("txtUsername");
        username.setValue("cvdh");


        MobileElement password = driver.findElementById("txtPassword");
//        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        List<MobileElement> fields = driver.findElementsByClassName("android.widget.TextView");
        fields.get(3).click();
        Thread.sleep(1000);
        //driver.findElementByXPath("//android.widget.ImageButton[@content-desc=\"Open navigation drawer\"]\n");
        List<MobileElement> elementsByClassName = driver.findElementsByClassName("android.widget.ImageButton");
        if (!elementsByClassName.isEmpty()) {
            elementsByClassName.get(0).click();
            Thread.sleep(1000);
            List<MobileElement> lblHeader = driver.findElementsById("lblListHeader");
            if (!lblHeader.isEmpty()) {
                lblHeader.get(0).click();
                Thread.sleep(1000);
                List<MobileElement> lblItem = driver.findElementsById("lblListItem");
                if (!lblItem.isEmpty()) {
                    lblItem.get(0).click();
                    Thread.sleep(1000);
                    List<MobileElement> editTexts = driver.findElementsByClassName("android.widget.EditText");
                    if (!editTexts.isEmpty()) {
                        editTexts.get(0).setValue("Đơn xuất tạo tự động");
                        editTexts.get(1).click();
                        driver.getPageSource();
                        MobileElement table = driver.findElementById("list_packing_product");
                        List<MobileElement> items = table.findElementsByClassName("android.widget.LinearLayout");
                        items.get(0).click();
                        Thread.sleep(1000);
                        editTexts = driver.findElementsByClassName("android.widget.EditText");
                        editTexts.get(5).setValue("1");

                        MobileElement buttonAdd = driver.findElementByClassName("android.widget.Button");
                        buttonAdd.click();

                        Point source = editTexts.get(5).getCenter();
                        PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH, "finger");
                        Sequence dragNDrop = new Sequence(finger, 1);
                        dragNDrop.addAction(finger.createPointerMove(Duration.ofMillis(0),
                                PointerInput.Origin.viewport(),
                                source.x / 2, source.y + 400));
                        dragNDrop.addAction(finger.createPointerDown(PointerInput.MouseButton.MIDDLE.asArg()));
                        dragNDrop.addAction(finger.createPointerMove(Duration.ofMillis(600),
                                PointerInput.Origin.viewport(), source.getX() / 2, source.y / 2));
                        dragNDrop.addAction(finger.createPointerUp(PointerInput.MouseButton.MIDDLE.asArg()));
                        driver.perform(Arrays.asList(dragNDrop));
                        Thread.sleep(1000);


                        MobileElement button = driver.findElementById("changeCountry");
                        if (button != null) {
                            button.click();
                            Thread.sleep(2000);
                            MobileElement alert = driver.findElementById("tvTitle");
                            alert.getText().equals("Tạo đơn thành công");
                        }

                    }
                }
            }
        }
    }
}
