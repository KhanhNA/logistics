package com.next_solutions.logistics;

public class JsonName {

    public static final String STORE_FOR_FC_ADMIN = "features/update_pallet/storeForFCAdmin.json";

    public static final String DISTRIBUTORS = "features/update_pallet/distributors.json";

    public static final String SEARCH_WITH_STORE = "features/update_pallet/searchWithStore.json";

    public static final String SEARCH_WITH_DISTRIBUTOR = "features/update_pallet/searchWithDistributor.json";

    public static final String SEARCH_WITH_STATUS = "features/update_pallet/searchWithStatus.json";

    public static final String SEARCH_WITH_DESCRIPTION = "features/update_pallet/searchWithDescription.json";

    public static final String NO_VALUE_PAGING = "features/update_pallet/noValue.json";

    public static final String ERROR_PROCESS = "features/update_pallet/errorProcess.json";

    public static final String PALLETS = "features/update_pallet/pallets.json";

    public static final String IMPORT_PO_IN_PALLET = "features/update_pallet/importPoInPallet.json";

    public static final String IMPORT_PO_NEW = "features/update_pallet/importPoNew.json";

    public static final String FAKE_IMPORT_PO = "features/update_pallet/fakeImportPo.json";


    public static final String FAKE_REPACKING_PLANNING_1 = "features/fake_model/repacking_planning/fakeRepackingPlanning1.json";
    public static final String FAKE_IMPORT_STATEMENT_1 = "features/fake_model/repacking_planning/fakeRepackingPlanning1.json";

    public static final String FAKE_PRODUCT_1 = "features/fake_model/product/fakeProduct1.json";
    public static final String FAKE_PRODUCT_2 = "features/fake_model/product/fakeProduct2.json";

    public static final String FAKE_PALLET_1 = "features/fake_model/pallet/fakePallet1.json";
    public static final String FAKE_PALLET_2 = "features/fake_model/pallet/fakePallet2.json";
    public static final String FAKE_PALLET_3 = "features/fake_model/pallet/fakePallet3.json";
    public static final String FAKE_PALLET_4 = "features/fake_model/pallet/fakePallet4.json";

    public static final String FAKE_STORE_1 = "features/fake_model/store/fakeStore1.json";
    public static final String FAKE_STORE_11 = "features/fake_model/store/fakeStore11.json";

    public static final String FAKE_DISTRIBUTOR_1 = "features/fake_model/distributor/fakeDistributor1.json";
    public static final String FAKE_DISTRIBUTOR_3 = "features/fake_model/distributor/fakeDistributor3.json";

    public static final String FAKE_API_PALLET_1 = "features/api/pallet/fakeApiPallet1.json";
    public static final String FAKE_API_PALLET_2 = "features/api/pallet/fakeApiPallet2.json";
    public static final String FAKE_API_PALLET_3 = "features/api/pallet/fakeApiPallet3.json";

    public static final String FAKE_API_INVENTORY_1 = "features/api/store/fakeApiInventory1.json";
    public static final String FAKE_API_INVENTORY_2 = "features/api/store/fakeApiInventory2.json";

    public static final String FAKE_API_GET_FROM_STORE_1 = "features/api/store/fakeApiGetFromStore1.json";

    public static final String FAKE_API_GET_DISTRIBUTOR_1 = "features/api/distributor/fakeApiDistributor1.json";

    public static final String FAKE_API_IMPORT_PO_1 = "features/api/import_po/fakeImportPo1.json";

    public static final String FAKE_API_EXPORT_STATEMENT_FROM_DC = "automation_step/action/open_navigation_action.json";
    public static final String FAKE_API_EXPORT_STATEMENT_FROM_FC = "automation_step/action/open_navigation_action.json";

    public static final String AUTOMATION_TEST_LOGIN = "automation_step/action/login_action.json";
    public static final String AUTOMATION_TEST_OPEN_NAVIGATION = "automation_step/action/open_navigation_action.json";
    public static final String AUTOMATION_TEST_UPDATE_PALLET = "automation_step/update_pallet_action.json";
    public static final String AUTOMATION_TEST_UPDATE_PALLET_1 = "automation_step/update_pallet_action_1.json";
    public static final String AUTOMATION_TEST_UPDATE_PALLET_2 = "automation_step/update_pallet_action_2.json";
    public static final String AUTOMATION_TEST_DELIVERY_1 = "automation_step/delivery_1.json";


    public static final String AUTOMATION_TEST_CHOOSE_CURRENT_STATUS = "automation_step/action/choose_current_status_action.json";
    public static final String AUTOMATION_TEST_ENTER_TEXT_SEARCH_ACTION = "automation_step/action/enter_text_search_action.json";
    public static final String AUTOMATION_TEST_CHOOSE_STATUS_ACTION = "automation_step/action/choose_status_action.json";
    public static final String AUTOMATION_TEST_CHOOSE_STORE_ACTION = "automation_step/action/choose_store_action.json";
    public static final String AUTOMATION_TEST_CHOOSE_TO_DISTRIBUTOR = "automation_step/action/choose_distributor_action.json";

    public static final String AUTOMATION_TEST_CHECK_BOX_SEARCH_ADVANCED = "automation_step/action/check_box_search_advanced_action.json";
    public static final String AUTOMATION_TEST_CHOOSE_FROM_DATE = "automation_step/action/choose_from_date.json";
    public static final String AUTOMATION_TEST_CHOOSE_TO_DATE = "automation_step/action/choose_to_date.json";
}
