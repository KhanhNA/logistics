package com.next_solutions.logistics.automation_test.util;

import com.next_solutions.logistics.automation_test.Step;

import java.util.List;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class HelperAction {
    private AndroidDriver<MobileElement> driver;

    public void action(AndroidDriver<MobileElement> driver, List<Step> steps) throws InterruptedException {
        this.driver = driver;
        for (Step step : steps) {
            if (step.checking) {
                if (step.many) {
                    checkingMultiElement(step);
                } else {
                    checkingSingleElement(step);
                }
            } else {
                if (step.many) {
                    actionMultiElement(step);
                } else {
                    actionSingleElement(step);
                }
            }
        }
    }

    private void checkingSingleElement(Step step) {
        MobileElement element = Helper.getElement(this.driver, step);
        assertNotNull(element);
        if (step.validateValue != null) {
            assertEquals(step.validateValue, element.getText());
        }
        if (step.contains != null) {
            assertTrue(element.getText().contains(step.contains));
        }
        if (step.notContains != null) {
            assertFalse(element.getText().contains(step.contains));
        }
    }

    private void checkingMultiElement(Step step) {
        List<MobileElement> elements = Helper.getElements(this.driver, step);
        assertNotNull(elements);
        if (step.validateSize != null) {
            assertEquals(step.validateSize, Long.valueOf(elements.size()));
        }
    }


    private void actionSingleElement(Step step) throws InterruptedException {
        MobileElement element = Helper.getElement(this.driver, step);
        assertNotNull(element);
        if (step.action.equals("click")) {
            element.click();
        } else if (step.action.equals("setText")) {
            element.setValue(step.value);
        }
        if (step.wait != null) {
            Thread.sleep(step.wait);
        }
    }


    private void actionMultiElement(Step step) throws InterruptedException {
        if (step.wait != null) {
            Thread.sleep(step.wait);
        }
    }

}
