package com.next_solutions.logistics.automation_test.util

import com.next_solutions.logistics.automation_test.Step
import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver

class Action(private val driver: AndroidDriver<MobileElement>) {
    @Throws(InterruptedException::class)
    fun action(loginSteps: List<Step?>?) {
        HelperAction().action(driver, loginSteps)
    }
}