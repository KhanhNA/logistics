package com.next_solutions.logistics.models.helper

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.text.DecimalFormat


@RunWith(MockitoJUnitRunner::class)
class FormatNumber {
    @Before
    fun setUp() {

    }

    @Test
    fun testFormatNumber() {
        val formatter = DecimalFormat("#,###")
        val myNumber1 = 1000000
        val formattedNumber1: String = formatter.format(myNumber1)
        Assert.assertEquals("1,000,000", formattedNumber1)
        val myNumber2 = 100000
        val formattedNumber2: String = formatter.format(myNumber2)
        Assert.assertEquals("100,000", formattedNumber2)
        val myNumber3 = 10000
        val formattedNumber3: String = formatter.format(myNumber3)
        Assert.assertEquals("10,000", formattedNumber3)
        val myNumber4 = 100
        val formattedNumber4: String = formatter.format(myNumber4)
        Assert.assertEquals("100", formattedNumber4)
    }
}