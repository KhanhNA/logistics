package com.next_solutions.logistics.base.claim

import android.app.Application
import com.next_solutions.logistics.config.AppUtils
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BaseClaimViewModelTest {

    @Mock
    lateinit var application: Application

    @Before
    fun setUp() {
        AppUtils.IS_TEST_MODE = true
    }

    @Test
    fun onCreate() {

    }

    @Test
    fun updateImages() {

    }


}