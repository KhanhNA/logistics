package com.next_solutions.logistics.automation_test;


public class Step {
    // tim kiem element
    public String xpath;
    public String id;
    public String className;
    public Boolean many;
    public Integer index;

    // hanh dong
    public String action;
    public String value;

    // check
    public String validateValue;
    public Long validateSize;
    public Boolean validateVisible;
    public String contains;
    public String notContains;
    public Boolean isNumber;
    public Boolean checking;


    // thoi gian cho
    public Long wait;
}
