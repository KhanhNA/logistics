package com.next_solutions.logistics;

import android.app.Application;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.databinding.ObservableList;
import androidx.lifecycle.MutableLiveData;

import com.next_solutions.logistics.features.login.LoginViewModel;
import com.next_solutions.logistics.models.AppUser;
import com.next_solutions.logistics.models.Language;
import com.next_solutions.logistics.repository.HttpHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <banner href="http://d.android.com/tools/testing">Testing documentation</banner>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    @Captor
    ArgumentCaptor<Object> loginSuccess;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    Application application;

    private final CountDownLatch latch = new CountDownLatch(1);

    //@Mock
    private LoginViewModel viewModel;

    private String token;

    @Before
    public void setUp() {
//        server = new MockWebServer();
//        server.start(10000);
//        server.enqueue(new MockResponse());
        viewModel = new LoginViewModel(application);


//        viewModel.setProcessing(new MutableLiveData<>());
        viewModel.setAppException(new MutableLiveData<>());
//        when(viewModel.getModelE()).thenReturn();
//        when(viewModel.getStringForTest()).thenReturn(new ObservableField<>());
    }

    @Test
    public void validateEmptyUsername_isCorrect() {
        testEmptyUsernameOrPassword__isCorrect("", "abc@123");
    }

    @Test
    public void validateEmptyPassword_isCorrect() {
        testEmptyUsernameOrPassword__isCorrect("fc_admin", "");
    }

    @Test
    public void testLanguage() {
        try {
            Thread.sleep(2000);
            ObservableList<Language> lstLanguage = viewModel.getLanguages();
            assertNotNull(lstLanguage);
            assertEquals(2, lstLanguage.size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void testEmptyUsernameOrPassword__isCorrect(String username, String password) {
        viewModel.setModelE(createModelUser(username, password));
        viewModel.login();
        assertNotNull(viewModel.getModelE().getLoginStatus());
        assertEquals( R.string.userEmpty, viewModel.getModelE().getLoginStatus().intValue());
    }

    private AppUser createModelUser(String username, String password) {
        AppUser appUser = new AppUser();
        appUser.setUsername(username);
        appUser.setPassword(password);
        return appUser;
    }

    private void testLogin_success(String username, String password) {
        viewModel.setModelE(createModelUser(username, password));
        HttpHelper.requestLogin(new MutableLiveData<>(), username, password, false, 1L, (call, response, o, throwable) -> {
            this.token = (String) o;
            assertNotNull(token);
        });
    }

    @Test
    public void testLoginWithFCAdmin() {
        testLogin_success("fc_admin", "abc@123");
    }

    @Test
    public void testLoginWithDCAdmin() {
        testLogin_success("dc1_admin", "abc@123");
    }
}