package com.next_solutions.logistics.automation_test.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.next_solutions.logistics.TestDataFactory;
import com.next_solutions.logistics.automation_test.Step;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Helper {
    public static AndroidDriver<MobileElement> getConnect() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
//
        cap.setCapability("deviceName", "SM-G955N");
//        cap.setCapability("deviceName", "Galaxy J5");
//        cap.setCapability("udid", "d888e44d");
        cap.setCapability("udid", "127.0.0.1:62025");
        cap.setCapability("platformName", "Android");
//        cap.setCapability("platformVersion", "9");
        cap.setCapability("platformVersion", "5.1.1");
        cap.setCapability("appPackage", "com.next_solutions.logistics");
        cap.setCapability("appActivity", "com.next_solutions.logistics.features.login.SplashActivity");
        cap.setCapability("appWaitDuration", "400000");
        URL url = new URL("http://localhost:4723/wd/hub");
        return new AndroidDriver<>(url, cap);
    }

    private static boolean isNullOrEmpty(String s) {
        return s == null || s.equals("");
    }

    private static boolean isNullOrEmpty(Integer s) {
        return s == null;
    }

    static List<MobileElement> getElements(AndroidDriver<MobileElement> driver, Step step) {
        if (!isNullOrEmpty(step.id)) {
            return driver.findElementsById(step.id);
        } else if (!isNullOrEmpty(step.className)) {
            return driver.findElementsByClassName(step.className);
        }
        return null;

    }

    static MobileElement getElement(AndroidDriver<MobileElement> driver, Step step) {
        if (!isNullOrEmpty(step.index)) {
            if (!isNullOrEmpty(step.xpath)) {
                return getElementByXPathAndIndex(driver, step.xpath, step.index);
            } else if (!isNullOrEmpty(step.id)) {
                return getElementByIdAndIndex(driver, step.id, step.index);
            } else if (!isNullOrEmpty(step.className)) {
                return getElementClassName(driver, step.className, step.index);
            }
        } else {
            if (!isNullOrEmpty(step.xpath)) {
                return driver.findElementByXPath(step.xpath);
            } else if (!isNullOrEmpty(step.id)) {
                return driver.findElementById(step.id);
            } else if (!isNullOrEmpty(step.className)) {
                return driver.findElementByClassName(step.className);
            }
        }
        return null;
    }

    private static MobileElement getElementByIdAndIndex
            (AndroidDriver<MobileElement> driver, String id, Integer index) {
        List<MobileElement> elementsById = driver.findElementsById(id);
        if (elementsById.size() < index) {
            return null;
        }
        return elementsById.get(index);
    }

    private static MobileElement getElementByXPathAndIndex
            (AndroidDriver<MobileElement> driver, String xpath, Integer index) {
        List<MobileElement> elementsById = driver.findElementsByXPath(xpath);
        if (elementsById.size() < index) {
            return null;
        }
        return elementsById.get(index);
    }

    private static MobileElement getElementClassName
            (AndroidDriver<MobileElement> driver, String className, Integer index) {
        List<MobileElement> elementsByClassName = driver.findElementsByClassName(className);
        if (elementsByClassName.size() < index) {
            return null;
        }
        return elementsByClassName.get(index);
    }

    public List<Step> getSteps(String jsonName) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String s = TestDataFactory.readTestFile(jsonName);
        return objectMapper.readValue(s, new TypeReference<List<Step>>() {
        });
    }
}
