package com.next_solutions.logistics.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Test {
    //    public static void main(String[] args) {
//        ExampleInterface proxy = ExampleProxy.newInstance(new Example());
//        proxy.function1();
//        proxy.function2();
//        proxy.otherFunction();
//        proxy.refresh();
//    }
    public static void main(String[] args) {
//        TestKotlin testKotlin = new TestKotlin();
//        testKotlin.test1();

//        String a = "get/export-statements/{id}";
//        a.split("/");
        SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_YEAR, 3);
        cal.set(Calendar.YEAR, 2019);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        System.out.println(sdf.format(cal.getTime()));
    }
}
