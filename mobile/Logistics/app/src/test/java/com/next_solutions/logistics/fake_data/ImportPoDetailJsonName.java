package com.next_solutions.logistics.fake_data;

public class ImportPoDetailJsonName {
    public static final String FAKE_GET_ALL_PALLET = "features/update_pallet/po_detail/get_all_pallet.json";
    public static final String EXIST_CLAIM = "features/update_pallet/po_detail/exist_claim.json";
    public static final String FAKE_IMPORT_PO = "features/update_pallet/po_detail/get_import_po.json";
    public static final String NOT_EXIST_CLAIM = "features/update_pallet/po_detail/not_exist_claim.json";
}
