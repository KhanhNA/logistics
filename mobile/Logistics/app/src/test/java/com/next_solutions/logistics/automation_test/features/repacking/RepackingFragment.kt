package com.next_solutions.logistics.automation_test.features.repacking

import com.next_solutions.logistics.JsonName
import com.next_solutions.logistics.automation_test.Step
import com.next_solutions.logistics.automation_test.util.Action
import com.next_solutions.logistics.automation_test.util.Helper
import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class RepackingFragment {


    private lateinit var driver: AndroidDriver<MobileElement>
    private lateinit var stepsLogin: List<Step>
    private lateinit var stepsOpenNavigation: List<Step>
    private lateinit var stepsChooseStatus: List<Step>
    private lateinit var stepsEnterTextSearch: List<Step>
    private lateinit var stepsChooseCurrentStatus: List<Step>
    private lateinit var stepsChooseStore: List<Step>
    private lateinit var stepsChooseDistributor: List<Step>
    private lateinit var stepsViewDetailPo: List<Step>
    private lateinit var stepsCheckBoxSearchAdvanced: List<Step>
    private lateinit var stepChooseFromDate: List<Step>
    private lateinit var stepChooseToDate: List<Step>
    @Before
    @Throws(IOException::class)
    fun setUp() {
        stepsLogin = Helper().getSteps(JsonName.AUTOMATION_TEST_LOGIN)
        stepsOpenNavigation = Helper().getSteps(JsonName.AUTOMATION_TEST_OPEN_NAVIGATION)

        stepsChooseCurrentStatus = Helper().getSteps(JsonName.AUTOMATION_TEST_CHOOSE_CURRENT_STATUS)
        stepsEnterTextSearch = Helper().getSteps(JsonName.AUTOMATION_TEST_ENTER_TEXT_SEARCH_ACTION)
        stepsChooseStatus = Helper().getSteps(JsonName.AUTOMATION_TEST_CHOOSE_STATUS_ACTION)
        stepsChooseStore = Helper().getSteps(JsonName.AUTOMATION_TEST_CHOOSE_STORE_ACTION)
        stepsChooseDistributor = Helper().getSteps(JsonName.AUTOMATION_TEST_CHOOSE_TO_DISTRIBUTOR)
        stepsCheckBoxSearchAdvanced = Helper().getSteps(JsonName.AUTOMATION_TEST_CHECK_BOX_SEARCH_ADVANCED)
        stepChooseFromDate = Helper().getSteps(JsonName.AUTOMATION_TEST_CHOOSE_FROM_DATE)
        stepChooseToDate = Helper().getSteps(JsonName.AUTOMATION_TEST_CHOOSE_TO_DATE)
    }

    @Test
    fun init() {
        driver = Helper.getConnect()
        Action(driver).action(stepsLogin)
        Action(driver).action(stepsOpenNavigation)
        Action(driver).action(stepsChooseStatus)
        Action(driver).action(stepsChooseCurrentStatus)
        Action(driver).action(stepsChooseStore)
        Action(driver).action(stepsChooseDistributor)
        Action(driver).action(stepChooseFromDate)
        Action(driver).action(stepChooseToDate)
    }

    @Test
    fun searchByCode() {
    }

    @Test
    fun searchByStore() {
    }


    @Test
    fun searchByDistributor() {
    }


    @Test
    fun searchByStatus() {
    }

    private fun hasNoResult() {

    }

    private fun hasResult() {

    }
}