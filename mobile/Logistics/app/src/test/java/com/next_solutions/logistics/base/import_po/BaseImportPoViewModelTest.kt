package com.next_solutions.logistics.base.import_po

import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@Suppress("UNCHECKED_CAST")
@RunWith(MockitoJUnitRunner::class)
class BaseImportPoViewModelTest