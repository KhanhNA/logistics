package com.next_solutions.logistics

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.next_solutions.logistics.features.login.LoginActivity
import com.robotium.solo.Solo
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    private var solo: Solo? = null

    private var activityTestRule = ActivityTestRule<LoginActivity>(LoginActivity::class.java)

    @Test
    fun useAppContext() {
        solo = Solo(InstrumentationRegistry.getInstrumentation(), activityTestRule.activity)
        val context = ApplicationProvider.getApplicationContext<Context>()
        val username = solo!!.getEditText(R.id.txtUsername)
        username.setText("tkfc")
        val password = solo!!.getEditText(R.id.txtPassword)
        password.setText("abc@123")
        val login = solo!!.getEditText(R.id.login)
        login.callOnClick()
    }
}