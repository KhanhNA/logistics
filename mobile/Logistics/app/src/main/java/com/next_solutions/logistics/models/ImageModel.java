package com.next_solutions.logistics.models;

import java.io.Serializable;

public class ImageModel implements Serializable {
    private String path;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isLocalImage() {
        return this.path != null;
    }
}
