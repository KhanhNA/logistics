package com.next_solutions.logistics.features.import_statement.claim_delivery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimFragment
import com.next_solutions.logistics.models.ImportStatement
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING
import com.next_solutions.logistics.utils.Constants.MODEL

class ClaimDeliveryStatementFragment : BaseClaimFragment() {

    private lateinit var claimDeliveryStatementViewModel: ClaimDeliveryStatementViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimDeliveryStatementViewModel = viewModel as ClaimDeliveryStatementViewModel
        getData()
        claimDeliveryStatementViewModel.init()
        return binding.root
    }

    private fun getData() {
        val importStatement = activity?.intent?.extras?.get(MODEL) as ImportStatement
        claimDeliveryStatementViewModel.importStatement = importStatement
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val dialog = ChooseDeliveryStatementDetailDialog()
            dialog.show((activity as FragmentActivity).supportFragmentManager, EMPTY_STRING)
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_claim_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimDeliveryStatementViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_delivery_statement
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}