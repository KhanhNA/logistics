package com.next_solutions.logistics.features.po.search_po

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.Po
import com.next_solutions.logistics.models.helper.ClaimRuleHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants.ERROR_PROCESS

class PoDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {

    var buttonEnable = ObservableField(true)

    var createClaim = ObservableField(false)

    var claimDetails: ArrayList<ClaimDetail>? = null

    var po: Po? = null

    var fromClaimFragment = ObservableField(true)

    override fun init() {
        if (!claimDetails.isNullOrEmpty()) {
            getReferenceClaimSuccess(claimDetails!!)
            getPoDetailSuccess(this.po!!)
        } else {
            getReferenceClaim()
        }
    }

    private fun getPoDetail() {
        if (po!!.poDetails == null) {
            callApi(Service.callPoService()
                    .findPoById(po!!.id)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<Po>(appException, Po::class.java, null) {
                        override fun getDataSuccess(po: Po) {
                            getPoDetailSuccess(po)
                        }

                        override fun onFailure(errorRespone: ErrorRespone?) {
                            Log.e(ERROR_PROCESS, errorRespone!!.message)
                        }
                    }))
        } else {
            getPoDetailSuccess(this.po!!)
        }
    }

    private fun getPoDetailSuccess(po: Po) {
        this.po = po
        if (!Po.PoStatus.NEW.value.equals(this.po!!.status)  && !isRejectedPo()) {
            if (claimDetails == null || (claimDetails!!.isNotEmpty() && claimDetails!![0].claim.status == Claim.ClaimStatus.WAIT_APPROVE.value)) {
                claimNotApproved()
            } else {
                if (claimDetails!!.isNotEmpty()) {
                    claimApproved()
                } else {
                    doNotHaveClaim()
                }
            }
        }
        setData(this.po!!.poDetails as List<BaseModel>?)
    }

    private fun doNotHaveClaim() {
        if (ClaimRuleHelper().canCreateClaimPo()) {
            createClaim.set(true)
            buttonEnable.set(true)
        }
    }

    private fun claimApproved() {
        disableButtonEnable()
    }

    private fun claimNotApproved() {
        disableButtonEnable()
    }

    private fun disableButtonEnable() {
        if (ClaimRuleHelper().canCreateClaimPo()) {
            buttonEnable.set(false)
            createClaim.set(true)
        }
    }

    private fun getReferenceClaim() {
        if (claimDetails == null) {
            callApi(Service.callClaimService()
                    .findClaimByReference(this.po!!.id, Claim.ClaimType.IMPORT_PO.value)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, List::class.java) {
                        override fun getDataSuccess(claimDetails: List<ClaimDetail>) {
                            getReferenceClaimSuccess(claimDetails)
                            getPoDetail()
                        }

                        override fun onFailure(errorRespone: ErrorRespone?) {
                            Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                            getPoDetail()
                        }
                    }))
        } else {
            getReferenceClaimSuccess(this.claimDetails!!)
        }
    }

    private fun getReferenceClaimSuccess(claimDetails: List<ClaimDetail>) {
        this.claimDetails = arrayListOf()
        this.claimDetails!!.addAll(claimDetails)

    }

    fun hideButtonViewClaim() {
        fromClaimFragment.set(false)
    }

    fun isRejectedPo(): Boolean {
        return Po.PoStatus.REJECT.value.equals(po!!.status);
    }
}