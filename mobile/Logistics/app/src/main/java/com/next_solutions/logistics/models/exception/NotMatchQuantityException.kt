package com.next_solutions.logistics.models.exception

class NotMatchQuantityException : Exception()