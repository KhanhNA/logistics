package com.next_solutions.logistics.repository.api.language;

public interface LanguageUrlPath {
    String GET_ALL_LANGUAGES = "/languages";
}
