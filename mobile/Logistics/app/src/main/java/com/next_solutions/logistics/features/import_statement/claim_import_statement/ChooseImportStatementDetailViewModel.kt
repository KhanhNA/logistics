package com.next_solutions.logistics.features.import_statement.claim_import_statement

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.ImportStatementDetail

@Suppress("UNCHECKED_CAST")
class ChooseImportStatementDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var importDetails: List<ImportStatementDetail>
    private lateinit var claimImportStatementViewModel: ClaimImportStatementViewModel
    val searchComponent = SearchComponent()
    fun init(claimImportStatementViewModel: ClaimImportStatementViewModel) {
        this.claimImportStatementViewModel = claimImportStatementViewModel
        this.importDetails = claimImportStatementViewModel.importDetails
        searchComponent.resetData()
        search()
    }

    override fun search() {
        val details = claimImportStatementViewModel.baseModelsE as List<ClaimDetail>
        setData(importDetails
                .asSequence()
                .filter {
                    it.quantity != 0L
                }
                .filter {
                    contain(details, it)
                }
                .filter {
                    it.productPacking.product.name.contains(searchComponent.getCodeSearch(), true)
                            || (it.productPacking.code.contains(searchComponent.getCodeSearch(), true))
                }
                .toList())
    }

    private fun contain(details: List<ClaimDetail>, importStatementDetail: ImportStatementDetail): Boolean {
        return details.asSequence().find { claimDetail ->
            claimDetail.productPacking.id == importStatementDetail.productPacking.id
                    && claimDetail.expireDate == importStatementDetail.expireDate
        } == null
    }

}