package com.next_solutions.logistics.models.dao;

import com.next_solutions.logistics.models.entity.StoreEntity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface StoreDao extends BaseDao<StoreEntity>{
    @Query("SELECT s.* FROM store s LIMIT :limit OFFSET :offset")
    LiveData<List<StoreEntity>> getStoreByPage(int limit, int offset);

    @Query("SELECT s.* FROM store s")
    LiveData<List<StoreEntity>> getAllStore();

    @Query("DELETE FROM store")
    void deleteAll();

}
