package com.next_solutions.logistics.models.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;

@Dao
public interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(T obj);
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(T[] obj);
}
