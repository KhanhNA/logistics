package com.next_solutions.logistics.features.claim.update_claim

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.databinding.FragmentClaimImageItemBinding
import com.next_solutions.logistics.models.Image
import com.next_solutions.logistics.repository.HttpHelper
import com.next_solutions.logistics.utils.RecycleViewCallBack
import com.yanzhenjie.album.Album

class UpdateImageClaimAdapter(private var images: ArrayList<Image>?,
                              private var recycleViewCallBack: RecycleViewCallBack,
                              private var baseClaimViewModel: BaseClaimViewModel) : RecyclerView.Adapter<UpdateImageClaimAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<FragmentClaimImageItemBinding>(LayoutInflater.from(parent.context),
                R.layout.fragment_claim_image_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        if (images == null) return 0
        return images!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        images?.let {
            holder.bind(images!![position], recycleViewCallBack, baseClaimViewModel)
        }
    }

    class ViewHolder(var binding: FragmentClaimImageItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(claimImage: Image, recycleViewCallBack: RecycleViewCallBack, baseClaimViewModel: BaseClaimViewModel) {
            if (claimImage.isLocalImage) {
                Album.getAlbumConfig().albumLoader.load(binding.claimImage, claimImage.albumFile)
            } else {

                val glideUrl = GlideUrl(AppUtils.IMAGE_URL + claimImage.claimImage.url,
                        LazyHeaders.Builder()
                                .addHeader("Authorization", HttpHelper.tokenLogin)
                                .build())
                Glide.with(binding.claimImage.getContext())
                        .load(glideUrl)
                        .into(binding.claimImage)
//                Glide.with(binding.root).load(AppUtils.IMAGE_URL + claimImage.claimImage.url).into(binding.claimImage)
            }
            binding.viewHolder = claimImage
            binding.listener = recycleViewCallBack
            binding.viewModel = baseClaimViewModel
        }
    }

}