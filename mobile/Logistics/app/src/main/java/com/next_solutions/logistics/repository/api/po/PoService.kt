package com.next_solutions.logistics.repository.api.po

import com.next_solutions.logistics.repository.api.po.PoUrlPath.Companion.FIND_PO_DETAIL
import com.next_solutions.logistics.repository.api.po.PoUrlPath.Companion.GET_PO
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PoService {
    @GET(GET_PO)
    fun findPo(@Query("storeId") storeId: Long,
               @Query("distributorId") distributorId: String,
               @Query("text") text: String,
               @Query("fromDate") fromDate: String,
               @Query("toDate") toDate: String,
               @Query("dateType") dateType: String,
               @Query("status") status: String,
               @Query("pageNumber") pageNumber: Int,
               @Query("pageSize") pageSize: Int): Single<Response<ResponseBody>>

    @GET(FIND_PO_DETAIL)
    fun findPoById(@Path("id") id: Long): Single<Response<ResponseBody>>
}