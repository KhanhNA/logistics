package com.next_solutions.logistics.features.claim.claim_pallet_step_one

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.features.claim.update_claim.UpdateClaimViewModel
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.PalletDetail
import com.next_solutions.logistics.models.helper.ClaimHelper
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.SCAN_CODE
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class DialogChoosePalletStepOneFragment() : BaseDialogFragment() {

    private lateinit var dialogViewModel: DialogChoosePalletStepOneViewModel
    private lateinit var claimPalletStepOneViewModel: ClaimPalletStepOneViewModel
    var storeId: Long = 0
    var claimDetail: List<ClaimDetail>? = null
    var updateClaimModel: UpdateClaimViewModel? = null;
    constructor(storeId: Long, claimDetail: List<ClaimDetail>, updClaimModel: UpdateClaimViewModel) : this() {
        this.storeId = storeId
        this.claimDetail = claimDetail
        this.updateClaimModel = updClaimModel;
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        dialog?.let { it.window?.requestFeature(Window.FEATURE_NO_TITLE) }
        setUpViewModel()
        setUpRecycleView()
        setStockId(storeId)
        dialogViewModel.init()
        return binding.root
    }
    fun setStockId(id: Long){
        dialogViewModel.stockId = id
        dialogViewModel.claimViewModel.setBaseModelsE(claimDetail)
        dialogViewModel.updateClaimModel = updateClaimModel
//        claimPalletStepOneViewModel.setBaseModelsE(claimDetail)
//        dialogViewModel.search()
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {

        if (SCAN_CODE == view!!.tag) {
            val intent = Intent(activity, QRCodeScannerActivity::class.java)
            startActivityForResult(intent, Constants.REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.REQUEST_CODE_SCAN) {
            dialogViewModel.searchComponent.setData(data!!.getStringExtra(Constants.RESULT_SCAN))
            dialogViewModel.search()
        }
    }

    private fun setUpViewModel() {
        claimPalletStepOneViewModel = ViewModelProvider(baseActivity,
                BaseViewModelFactory.getInstance<Any>(baseActivity.application, ClaimPalletStepOneViewModel::class.java))
                .get(ClaimPalletStepOneViewModel::class.java)
        dialogViewModel = viewModel as DialogChoosePalletStepOneViewModel
        dialogViewModel.claimViewModel = claimPalletStepOneViewModel
    }

    fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_pallet_step_one_item, viewModel, OwnerView { _, palletDetail ->
            if(updateClaimModel != null) {
                updateClaimModel!!.addClaimDetail(ClaimHelper().convertPalletDetailToClaimDetail(palletDetail as PalletDetail))
            }else {
                claimPalletStepOneViewModel.addDetail(palletDetail as PalletDetail)
            }
            dismiss()
        }, baseActivity)
        recyclerView.apply {
            adapter = baseAdapter
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return DialogChoosePalletStepOneViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_pallet_step_one
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }
}