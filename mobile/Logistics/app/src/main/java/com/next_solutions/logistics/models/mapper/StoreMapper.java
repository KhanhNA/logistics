package com.next_solutions.logistics.models.mapper;

import com.next_solutions.logistics.models.Store;
import com.next_solutions.logistics.models.entity.StoreEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StoreMapper {
    StoreMapper INSTANCE = Mappers.getMapper( StoreMapper.class );

    StoreEntity storeToStoreEntity(Store store);
}
