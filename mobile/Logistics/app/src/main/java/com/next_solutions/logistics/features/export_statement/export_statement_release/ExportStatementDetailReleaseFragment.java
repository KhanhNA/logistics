package com.next_solutions.logistics.features.export_statement.export_statement_release;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.CommonActivity;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment;
import com.next_solutions.logistics.features.export_statement.claim_export_statement.ClaimExportStatementFragment;
import com.next_solutions.logistics.models.Claim;
import com.next_solutions.logistics.models.ClaimDetail;
import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;

import java.util.ArrayList;

import static com.next_solutions.logistics.utils.Constants.CLAIM_ACTIVITY_CODE;
import static com.next_solutions.logistics.utils.Constants.CLAIM_DETAILS;
import static com.next_solutions.logistics.utils.Constants.CREATE_CLAIM_SUCCESS;
import static com.next_solutions.logistics.utils.Constants.FRAGMENT;
import static com.next_solutions.logistics.utils.Constants.MODEL;
import static com.next_solutions.logistics.utils.Constants.MODEL_EXPORT_STATEMENT;
import static com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN;

public class ExportStatementDetailReleaseFragment extends BaseFragment {
    private static final int REQUEST_CODE_SCAN_PALLET = 2222;
    private ExportStatementDetailReleaseViewModel detailViewModel;
    private BaseAdapter baseAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        detailViewModel = (ExportStatementDetailReleaseViewModel) viewModel;
        getData();
        baseAdapter = new BaseAdapter(R.layout.fragment_create_export_statement_release_detail_item, viewModel, (view, baseModel) ->
                baseAdapter.notifyItemChanged(baseModel.index - 1), getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        detailViewModel.init();
        return v;
    }

    private void getData() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Intent intent = activity.getIntent();
            if (intent != null && intent.hasExtra(Constants.MODEL)) {
                ExportStatement exportStatement = (ExportStatement) intent.getSerializableExtra(Constants.MODEL);
                detailViewModel.setExportStatement(exportStatement);
            }

            if (intent != null && intent.hasExtra(CLAIM_DETAILS)) {
                Object[] claimDetails = (Object[]) intent.getSerializableExtra(CLAIM_DETAILS);
                detailViewModel.setClaimDetails(new ArrayList<>());
                for (Object claimDetail : claimDetails) {
                    detailViewModel.getClaimDetails().add((ClaimDetail) claimDetail);
                }
                detailViewModel.hideButtonViewClaim();
            }
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if (action.equals(Constants.CREATE_SUCCESS)) {
            showAlertSuccess(Constants.TIME_AFTER_DELAY, R.string.export_release_success, null);
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (getBaseActivity() != null) {
                    getBaseActivity().onBackPressed();
                }
            }, Constants.TIME_DELAY);
        }
        if (action.equals(Constants.ERROR_INVALID_INPUT)) {
            Integer errorPosition = detailViewModel.getErrorPosition();
            if (errorPosition != null) {
                getLayoutManager().scrollToPositionWithOffset(errorPosition, 20);
                baseAdapter.notifyDataSetChanged();
            }
            showAlertWarning(Constants.TIME_AFTER_DELAY, R.string.error_invalid_input, null);
        }
        if (action.equals(Constants.ERROR_PROCESS)) {
            showAlertWarning(Constants.TIME_AFTER_DELAY, R.string.error_process, null);
        }
        if (action.equals("NOT_IN_PALLET")) {
            showAlertWarning(Constants.TIME_AFTER_DELAY, R.string.not_in_pallet, null);
        }
        if (action.equals("PALLET_DONT_HAVE_PRODUCT")) {
            showAlertWarning(Constants.TIME_AFTER_DELAY, R.string.pallet_dont_have_product, null);
        }

        if (action.equals("CHOOSE_PALLET")) {
            showAlertWarning(Constants.TIME_AFTER_DELAY, R.string.choose_pallet, null);
        }

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.scanProduct:
                Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
                startActivityForResult(qrScan, REQUEST_CODE_SCAN);
                break;
            case R.id.scanPallet:
                Intent intentScanPallet = new Intent(getContext(), QRCodeScannerActivity.class);
                startActivityForResult(intentScanPallet, REQUEST_CODE_SCAN_PALLET);
                break;
            case R.id.createClaim:
                Intent intentCreateClaim = new Intent(getContext(), CommonActivity.class);
                bundle.putSerializable(FRAGMENT, ClaimExportStatementFragment.class);
                bundle.putSerializable(MODEL, detailViewModel.getExportStatement());
                intentCreateClaim.putExtras(bundle);
                startActivityForResult(intentCreateClaim, CLAIM_ACTIVITY_CODE);
                break;
            case R.id.reviewClaim:
                Intent intent = new Intent(getContext(), CommonActivity.class);
                if (detailViewModel.getClaimDetails() != null && !detailViewModel.getClaimDetails().isEmpty()) {
                    Claim claim = detailViewModel.getClaimDetails().get(0).getClaim();
                    claim.setClaimDetails(detailViewModel.getClaimDetails());
                    bundle.putSerializable(FRAGMENT, ClaimDetailFragment.class);
                    bundle.putSerializable(MODEL, claim);
                    bundle.putSerializable(MODEL_EXPORT_STATEMENT, detailViewModel.getExportStatement());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_SCAN) {
                String result = data.getStringExtra(Constants.RESULT_SCAN);
                detailViewModel.checkProduct(result);
            }
            if (requestCode == REQUEST_CODE_SCAN_PALLET) {
                String result = data.getStringExtra(Constants.RESULT_SCAN);
                detailViewModel.checkPallet(result);
            }
            if (requestCode == CLAIM_ACTIVITY_CODE) {
                String result = data.getStringExtra(Constants.CLAIM_ACTIVITY);
                if (result.equals(CREATE_CLAIM_SUCCESS)) {
                    getBaseActivity().onBackPressed();
                }
            }
        }

        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_export_statement_release_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ExportStatementDetailReleaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.lstProducts;
    }
}
