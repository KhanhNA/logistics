package com.next_solutions.logistics.models.mapper;

import com.next_solutions.logistics.models.ExportStatementDetail;
import com.next_solutions.logistics.models.ProductDescription;
import com.next_solutions.logistics.models.entity.ProductDescriptionEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ExportStatementDetailMapper {
    ExportStatementDetailMapper INSTANCE = Mappers.getMapper( ExportStatementDetailMapper.class );

    @Mapping(source = "code", target = "productCode")
    @Mapping(source = "name", target = "productName")
    @Mapping(source = "packingQuantity", target = "packingTypeQuantity")
    @Mapping(source = "productId", target = "productId")
    @Mapping(source = "packingId", target = "packingTypeId")
    @Mapping(source = "quantity", target = "totalQuantity")
    @Mapping(target = "index", ignore = true)
    @Mapping(target = "checked", ignore = true)
    ExportStatementDetail productDescriptionToExportStatementDetail(ProductDescriptionEntity product);



    @Mapping(source = "productCode", target = "code")
    @Mapping(source = "productName", target = "name")
    @Mapping(source = "packingTypeQuantity", target = "packingQuantity")
    @Mapping(source = "productId", target = "productId")
    @Mapping(source = "packingTypeId", target = "packingId")
    @Mapping(source = "totalQuantity", target = "quantity")
    ProductDescriptionEntity statementDetailToProduct(ExportStatementDetail statementDetail);

    @Mapping(source = "productCode", target = "code")
    @Mapping(source = "productName", target = "name")
    @Mapping(source = "packingTypeQuantity", target = "packingQuantity")
    @Mapping(target = "index", ignore = true)
    @Mapping(target = "checked", ignore = true)
    ProductDescription statementDetailToProductDescription(ExportStatementDetail statementDetail);



    @Mapping(source = "code", target = "productCode")
    @Mapping(source = "name", target = "productName")
    @Mapping(source = "packingQuantity", target = "packingTypeQuantity")
    @Mapping(target = "index", ignore = true)
    @Mapping(target = "checked", ignore = true)
    ExportStatementDetail productDescriptionToExportStatementDetail(ProductDescription product);
}
