package com.next_solutions.logistics.base.claim.po

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.BaseFragment
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ItemOffsetDecoration
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.ClaimImageAdapter
import com.next_solutions.logistics.config.AppUtils.*
import com.next_solutions.logistics.features.claim.view_claim_image.ViewClaimImageActivity
import com.next_solutions.logistics.models.Image
import com.next_solutions.logistics.models.ImageModel
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.RecycleViewCallBack
import com.yanzhenjie.album.Album
import com.yanzhenjie.album.AlbumFile

abstract class BaseClaimPoFragment : BaseFragment() {
    private lateinit var claimImageAdapter: ClaimImageAdapter
    lateinit var baseClaimPoViewModel: BaseClaimPoViewModel
    private lateinit var claimAdapter: ClaimAdapterBase

    abstract fun getRecycleViewImage(): Int
    abstract fun getLayoutRecycleViewL1Id(): Int
    abstract fun getLayoutRecycleViewL2Id(): Int
    abstract fun getRecycleViewL2Id(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        baseClaimPoViewModel = viewModel as BaseClaimPoViewModel
        setUpRecycleViewImage()
        setUpRecycleViewData()
        return binding.root
    }

    private fun setUpRecycleViewData() {
        claimAdapter = ClaimAdapterBase(
                getLayoutRecycleViewL1Id(),
                getLayoutRecycleViewL2Id(),
                getRecycleViewL2Id(),
                baseClaimPoViewModel,
                context!!)
        recyclerView.apply {
            adapter = claimAdapter
        }
        val spacing = 1
        recyclerView.addItemDecoration(ItemOffsetDecoration(spacing))
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            NOTIFY_CHANGE -> {
                val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
                recyclerView.layoutAnimation = controller
                claimAdapter.notifyDataSetChanged()
                recyclerView.scheduleLayoutAnimation()
            }
            NOTIFY_CHANGE_ITEM -> {
                baseClaimPoViewModel.notifyPosition.get()?.let { claimAdapter.notifyItemChanged(it) }
            }
            NOTIFY_CHANGE_ITEM_REMOVE -> {
                baseClaimPoViewModel.notifyPosition.get()?.let { claimAdapter.notifyItemRemoved(it) }
            }
            NOTIFY_CHANGE_ITEM_ADD -> {
                baseClaimPoViewModel.notifyPosition.get()?.let { claimAdapter.notifyItemInserted(it) }
            }
            PRODUCT_INFO_ERROR -> {
                scrollToPosition()
                showAlertWarning(TIME_AFTER_DELAY, R.string.invalid_input, null)
            }
            "MISSING_AMENABLE" -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.missing_amenable, null)
            }
            SUCCESS -> {
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        val returnIntent = Intent()
                        returnIntent.putExtra(CLAIM_ACTIVITY, CREATE_CLAIM_SUCCESS)
                        baseActivity.setResult(Activity.RESULT_OK, returnIntent)
                        baseActivity.finish()
                    }
                }, TIME_DELAY.toLong())
                showAlertSuccess(TIME_AFTER_DELAY, R.string.success, null)
            }
            MISSING_DESCRIPTION -> {
                showAlertWarning(R.string.missing_image_description)
            }
            UPDATE_SUCCESS -> {
                showAlertSuccess(TIME_AFTER_DELAY, R.string.update_success, null)
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
            }
            UPLOAD_IMAGE_SUCCESS -> {
                val message = context!!.getString(R.string.upload_image_success)
                showNotification(LOGISTIC, message, NOTIFICATION_ID, NOTIFICATION_NAME, R.drawable.alerter_ic_notifications)
            }
            MISSING_DETAILS -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.missing_details, null)
            }
        }
    }


    private fun scrollToPosition() {
        baseClaimPoViewModel.notifyPosition.get()?.let {
            claimAdapter.notifyItemChanged(it)
            getLayoutManager()!!.scrollToPosition(it)
        }
        baseClaimPoViewModel.lastNotifyPosition?.let {
            claimAdapter.notifyItemChanged(it)
        }
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (view!!.id == R.id.add_image) {
            pickFromGallery()
        }
    }

    open fun doAfterPickImageFromGallery(result: java.util.ArrayList<AlbumFile>) {
        baseClaimPoViewModel.updateImages(result)
        claimImageAdapter.notifyDataSetChanged()
    }

    private fun pickFromGallery() {
        Album.album(this)
                .multipleChoice()
                .columnCount(IMAGE_COLUMN)
                .selectCount(IMAGE_UPLOAD_SIZE)
                .camera(true)
                .checkedList(baseClaimPoViewModel.images.asSequence()
                        .filter { it.albumFile != null }
                        .map { it.albumFile }
                        .toCollection(ArrayList()))
                .onResult { result: ArrayList<AlbumFile> ->
                    doAfterPickImageFromGallery(result)
                }
                .onCancel { Toast.makeText(context, "Cancel", Toast.LENGTH_LONG).show() }
                .start()
    }

    open fun setUpRecycleViewImage() {
        claimImageAdapter = ClaimImageAdapter(baseClaimPoViewModel.images, baseClaimPoViewModel, RecycleViewCallBack { view, image ->
            run {
                if (view.id == R.id.delete_image) {
                    baseClaimPoViewModel.removeImage(image as Image)
                    claimImageAdapter.notifyDataSetChanged()
                } else if (view.id == R.id.claim_image) {
                    showImage(image as Image)
                }
            }
        })

        val linearLayout = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.root.findViewById<RecyclerView>(getRecycleViewImage()).apply {
            adapter = claimImageAdapter
            layoutManager = linearLayout
        }
    }

    private fun showImage(image: Image) {
        val intent = Intent(context, ViewClaimImageActivity::class.java)
        val bundle = Bundle()
        val images = baseClaimPoViewModel.images
                .asSequence()
                .map {
                    if (it.isLocalImage) {
                        ImageModel().apply { path = it.albumFile.path }
                    } else {
                        ImageModel().apply { url = it.claimImage.url }
                    }
                }
                .toList()
                .toTypedArray()
        bundle.putSerializable(MODEL, images)
        bundle.putSerializable(POSITION, baseClaimPoViewModel.images.indexOf(image))
        intent.putExtras(bundle)
        startActivity(intent)
    }

}