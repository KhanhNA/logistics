package com.next_solutions.logistics.features.import_statement.import_statement_release.to_dc

import android.app.Application
import com.next_solutions.logistics.base.import_release.BaseImportReleaseViewModel
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.models.ImportStatementDetailPallet
import com.next_solutions.logistics.models.exception.InvalidDetailException
import com.next_solutions.logistics.utils.Constants.NOT_MATCH_QUANTITY

@Suppress("UNCHECKED_CAST")
class ImportStatementDCViewModel(application: Application) : BaseImportReleaseViewModel(application) {
    override fun prepareStatementDetail(): List<ImportStatementDetailPallet> {
        val details = baseModelsE as List<ImportStatementDetail>
        val importDetailPallet = arrayListOf<ImportStatementDetailPallet>()
        details.forEach { detail ->
            run {
                importDetailPallet.add(ImportStatementDetailPallet().apply {
                    importStatementId = importStatement!!.id
                    importStatementDetailId = detail.id
                    quantity = detail.quantity
                    productPackingId = detail.productPacking.id
                    expireDate = detail.expireDate
                })
            }
        }
        return importDetailPallet
    }

    override fun checkValidate(): Boolean {
        val details = baseModelsE as List<ImportStatementDetail>
        for ((index, detail) in details.withIndex()) {
            try {
                isValidDetail(detail)
            } catch (invalidDetailException: InvalidDetailException) {
                lastErrorPosition = errorPosition
                errorPosition = index
                sendBackAction(NOT_MATCH_QUANTITY)
                return false
            }
        }
        return true
    }

    override fun checkErrorPositionWhenClickCheckBox(position: Int) {
        val details = baseModelsE as List<ImportStatementDetail>
        val detail = details[position]
        try {
            isValidDetail(detail)
            errorPosition = -1
        } catch (invalidDetailException: InvalidDetailException) {

        }
    }

    override fun isValidDetail(importStatementDetail: ImportStatementDetail) {
        if ((importStatementDetail.checked == null) || (!importStatementDetail.checked)) {
            throw InvalidDetailException()
        }
    }
}