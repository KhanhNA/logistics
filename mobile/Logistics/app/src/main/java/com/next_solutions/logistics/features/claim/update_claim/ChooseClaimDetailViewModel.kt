package com.next_solutions.logistics.features.claim.update_claim

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.ClaimDetail

@Suppress("UNCHECKED_CAST")
class ChooseClaimDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {

    lateinit var updateClaimViewModel: UpdateClaimViewModel
    val searchComponent = SearchComponent()

    override fun init() {
        search()
    }

    override fun search() {
        val details = updateClaimViewModel.baseModelsE as List<ClaimDetail>
        setData(updateClaimViewModel.claimDetailsReference
                .asSequence()
                .filter {
                    notContain(details, it)
                }
                .filter {
                    it.productPacking.code.contains(searchComponent.getCodeSearch(), true)
                            || it.productPacking.product.name.contains(searchComponent.getCodeSearch(), true)
                }
                .toList())
    }


    private fun notContain(details: List<ClaimDetail>, claimDetail: ClaimDetail): Boolean {
        return details.asSequence().find { detail ->
            detail.productPacking.id == claimDetail.productPacking.id
                    && detail.expireDate == claimDetail.expireDate
        } == null
    }
}