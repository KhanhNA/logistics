package com.next_solutions.logistics.models.dto

import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.PoDetail

class PoClaimDetail(var poDetail: PoDetail,
                    var claimDetails: ArrayList<ClaimDetail>
)
