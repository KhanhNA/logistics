package com.next_solutions.logistics.features.home;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;
import com.next_solutions.base.BaseActivity;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.logistics.Manifest;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.adapter.ExpandableListAdapter;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.databinding.ActivityHomeBinding;
import com.next_solutions.logistics.databinding.NavHeaderHomeBinding;
import com.next_solutions.logistics.features.change_password.ChangePasswordFragment;
import com.next_solutions.logistics.features.login.LoginActivity;
import com.next_solutions.logistics.models.Role;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.PreferencesHelper;
import com.next_solutions.logistics.utils.ValidateUtils;
import com.next_solutions.logistics.utils.app_model.MenuTitle;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;


public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    private HomeViewModel homeViewModel;
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    Map<MenuTitle, SortedSet<MenuTitle>> mapMenu = new TreeMap<>();
    int backPressCount = 0;
    Integer currentGroupPosition = null;
    Integer currentChildPosition = null;
    Integer previousGroupPosition = null;
    Integer previousChildPosition = null;
    private static final int REQUEST_CAMERA = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityHomeBinding activityHomeBinding = (ActivityHomeBinding) binding;
        NavHeaderHomeBinding headerBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.nav_header_home,
                activityHomeBinding.navView, false);
        homeViewModel = (HomeViewModel) viewModel;
        if (homeViewModel != null) {
            headerBinding.setViewModel(homeViewModel);
            homeViewModel.initMenu(getBaseContext());
        }
        activityHomeBinding.navView.addHeaderView(headerBinding.getRoot());


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.gray_blue_dark));
        expandableListView = findViewById(R.id.expandableListView);
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
            return;
        }

        closeProcess();
        if(AppUtils.getRule().getUserAuthentication().getPrincipal().isMustChangePassword()){
            changeFragment(new ChangePasswordFragment());
        }
    }

    @Override
    public void onBackPressed() {
        backPressCount++;
        new Handler().postDelayed(() -> {
            if (backPressCount == 1) {
                backPressCount = 0;
                if (previousGroupPosition != null && previousChildPosition != null) {
                    changeFragmentByMenu(previousGroupPosition, previousChildPosition);
                }
            } else if (backPressCount == 2) {
                super.onBackPressed();
            }
        }, 500);

    }

    private void closeDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }
    public void logout(){
        AppUtils.setLanguage(null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        PreferencesHelper preferencesHelper = AppUtils.getInstance().getPreferencesHelper();
        if (preferencesHelper != null) {
            preferencesHelper.setRefreshToken("");
            preferencesHelper.setToken("");
        }
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify banner parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_sync:
                return true;
            case R.id.log_out:
                logout();
                return true;
            case R.id.change_password:
                changeFragment(new ChangePasswordFragment());
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void populateExpandableList() {
        try {
            for (Role role : AppUtils.getRule().getUserAuthentication().getPrincipal().getRoles()) {
                if (ValidateUtils.isNullOrEmpty(role.getMenus())) {
                    continue;
                }
                for (com.next_solutions.logistics.models.Menu menu : role.getMobileMenus()) {
                    MenuTitle menuTitle = homeViewModel.findMenuByCode(menu.getCode());
                    if (menuTitle != null) {
                        if (menu.getParentMenu() == null && !mapMenu.containsKey(menuTitle)) {
                            mapMenu.put(menuTitle, new TreeSet<>());
                        }
                        if (menu.getParentMenu() != null) {
                            SortedSet<MenuTitle> menuTitleSet;
                            MenuTitle parentMenuTitle = homeViewModel.findMenuByCode(menu.getParentMenu().getCode());
                            if (mapMenu.containsKey(parentMenuTitle)) {
                                menuTitleSet = mapMenu.get(parentMenuTitle);
                            } else {
                                menuTitleSet = new TreeSet<>();
                            }
                            if (menuTitleSet != null) {
                                menuTitleSet.add(menuTitle);
                                if (parentMenuTitle != null) {
                                    mapMenu.put(parentMenuTitle, menuTitleSet);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(ERROR_PROCESS, e.getMessage());
            showAlertError(Constants.TIME_AFTER_DELAY, R.string.get_rule_error, null);
        }
        expandableListAdapter = new ExpandableListAdapter(this, mapMenu.keySet(), mapMenu);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener((parent, v, groupPosition, id) -> false);

        expandableListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            changeFragmentByMenu(groupPosition, childPosition);
            return false;
        });
    }

    private void changeFragmentByMenu(int groupPosition, int childPosition) {
        Log.e("onChildClick", groupPosition + "" + "    " + childPosition);
        Object[] menuTitles = mapMenu.keySet().toArray();
        if (menuTitles != null) {
            MenuTitle title = (MenuTitle) menuTitles[groupPosition];
            Set<MenuTitle> titles = mapMenu.get(title);
            if (titles != null) {
                MenuTitle menuTitle = titles.toArray(new MenuTitle[0])[childPosition];
                toolbar.setTitle(menuTitle.getTitle());
                if (currentChildPosition != null && currentGroupPosition != null) {
                    previousChildPosition = currentChildPosition;
                    previousGroupPosition = currentGroupPosition;
                }
                // new value
                currentChildPosition = childPosition;
                currentGroupPosition = groupPosition;
                if (!ValidateUtils.isNullOrEmpty(menuTitle.getTag())) {
                    Bundle bundle = new Bundle();
                    bundle.putString("fragment", menuTitle.getTag());
                    menuTitle.getFragment().setArguments(bundle);
                }
                if (menuTitle.getFragment() != null) {

                    Class<?> clazz = null;
                    try {
                        clazz = Class.forName(menuTitle.getFragment().getClass().getName());
                        Fragment fragment = (Fragment) clazz.newInstance();
                        changeFragment(fragment);
                    } catch (ClassNotFoundException e) {
                        Log.e(ERROR_PROCESS, e.getMessage());
                    } catch (IllegalAccessException e) {
                        Log.e(ERROR_PROCESS, e.getMessage());
                    } catch (InstantiationException e) {
                        Log.e(ERROR_PROCESS, e.getMessage());
                    }
                }
            }
            closeDrawer();
        }
    }

    private void changeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.framelayout, fragment).commit();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_home;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return HomeViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
