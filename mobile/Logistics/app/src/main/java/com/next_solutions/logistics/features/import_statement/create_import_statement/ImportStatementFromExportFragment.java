package com.next_solutions.logistics.features.import_statement.create_import_statement;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.CommonActivity;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;

import static com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN;

public class ImportStatementFromExportFragment extends BaseFragment {
    private ImportStatementFromExportViewModel importStatementFromExportViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        importStatementFromExportViewModel = (ImportStatementFromExportViewModel) viewModel;
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_create_import_statement_item, viewModel, (view, baseModel) -> {
            ExportStatement exportStatement = (ExportStatement) baseModel;
            Intent intent = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.MODEL, exportStatement);
            intent.putExtras(bundle);
            startActivity(intent);
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if ("scanCode".equals(view.getTag())) {
            Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
            startActivityForResult(qrScan, REQUEST_CODE_SCAN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            String result = data.getStringExtra(Constants.RESULT_SCAN);
            importStatementFromExportViewModel.searchByCode(result);
        }
        closeProcess();
    }

    @Override
    public void onResume() {
        super.onResume();
        importStatementFromExportViewModel.search();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_import_statement;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportStatementFromExportViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.exportStatements;
    }
}
