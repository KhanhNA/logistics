package com.next_solutions.logistics.features.export_statement.export_statement_delivery

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ExportStatementDeliveryFragment : BaseRecycleViewFragment() {

    private lateinit var exportDeliveryViewModel: ExportStatementDeliveryViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        exportDeliveryViewModel = viewModel as ExportStatementDeliveryViewModel
        exportDeliveryViewModel.init()
        return binding.root
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        when (view!!.tag as String) {
            SCAN_CODE -> {
                val qrScan = Intent(context, QRCodeScannerActivity::class.java)
                startActivityForResult(qrScan, REQUEST_CODE_SCAN)
            }
            else -> {
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_SCAN) {
//            exportDeliveryViewModel.searchComponent.setData(data!!.getStringExtra(RESULT_SCAN))
            exportDeliveryViewModel.searchByCode(data!!.getStringExtra(RESULT_SCAN))
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ExportStatementDeliveryViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_export_statement_delivery
    }

    override fun getRecycleResId(): Int {
        return R.id.export_deliveries
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_export_statement_delivery_item
    }

    override fun getListenerAdapter(): OwnerView {
        return OwnerView { _, exportStatement ->
            run {
                val intent = Intent(activity, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ExportStatementDeliveryDetailFragment::class.java)
                bundle.putSerializable(MODEL, exportStatement)
                bundle.putBoolean(INVISIBLE_REVIEW_CLAIM, true)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }
}