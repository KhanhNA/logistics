package com.next_solutions.logistics.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class NumberFormat {
    private static String formatForm = "#,###.###";

    private NumberFormat() {
    }

    public static String formatNumber(Long number) {
        if(number==null) return "";
        DecimalFormat formatter = new DecimalFormat(formatForm);
        return formatter.format(number);
    }

    public static String formatNumber(BigDecimal number) {
        if(number==null) return "";
        DecimalFormat formatter = new DecimalFormat(formatForm);
        return formatter.format(number);
    }

    public static String formatNumber(Double number) {
        if(number==null) return "";
        DecimalFormat formatter = new DecimalFormat(formatForm);
        return formatter.format(number);
    }

    public static String formatNumber(Integer number) {
        if(number==null) return "";
        DecimalFormat formatter = new DecimalFormat(formatForm);
        return formatter.format(number);
    }
}
