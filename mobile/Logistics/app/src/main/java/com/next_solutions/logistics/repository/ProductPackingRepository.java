package com.next_solutions.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.next_solutions.logistics.config.AppDatabase;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.models.dao.ProductPackingDao;
import com.next_solutions.logistics.models.entity.ProductPackingEntity;
import com.next_solutions.logistics.models.view.ProductDescriptionView;

import java.util.List;


public class ProductPackingRepository {
    private ProductPackingDao productPackingDao;

    private LiveData<List<ProductDescriptionView>> allProductDescription;
    public ProductPackingRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        productPackingDao = db.productPackingDao();
    }
    public void insert(List<ProductPackingEntity> entity) {
        ProductPackingEntity[] array = entity.toArray(new ProductPackingEntity[0]);
        new InsertAsyncTask(productPackingDao).execute(array);
    }
    public void searchAll(List<Long> exceptId, Integer pageNumber){
        allProductDescription = productPackingDao.findProduct(AppUtils.getLanguage(), exceptId, AppUtils.PAGE_SIZE, AppUtils.PAGE_SIZE * (pageNumber - 1));
    }
    private static class InsertAsyncTask extends AsyncTask<ProductPackingEntity, Void, Void> {

        private ProductPackingDao mAsyncTaskDao;

        InsertAsyncTask(ProductPackingDao dao) {
            mAsyncTaskDao = dao;
        }


        @Override
        protected Void doInBackground(ProductPackingEntity... productPackingEntities) {
            mAsyncTaskDao.insert(productPackingEntities);
            return null;
        }
    }
}
