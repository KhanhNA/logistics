package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)

public class ImportStatementDetail extends Base {
    private Long quantity;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date importDate;
    private Long productId;
    private Product product;
    private String productCode;
    private Long packingTypeId;
    private Long productPackingId;
    private PackingType packingType;
    private ProductPacking productPacking;
    private String packingTypeCode;
    private String productPackingCode;
    private String productPackingName;
    private Long packingTypeQuantity;
    private BigDecimal price;
    private String productName;
    private ProductPackingPrice productPackingPrice;
    private StoreProductPackingDetail storeProductPackingDetail;
    private String expireDate;
    private Long totalQuantity;
    private Pallet pallet;
    private Long palletId;
    private String uom;
    private Long originalQuantity;
    private Long claimQuantityDelivery;
    private Long claimQuantityImport;
    private Long claimQuantity;
    private List<ImportStatementDetailPallet> importStatementDetailPallets;

    public ImportStatementDetail() {
    }

    public String getExpireDateStr() {
        return expireDate == null ? EMPTY_STRING : this.expireDate;
    }

    public String getExpireDateDisplay() {
        return expireDate == null ? EMPTY_STRING : DateUtils.convertStringDateToDifferentType(this.expireDate, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);
    }

    public ImportStatementDetail(Long id, Date createDate, String createUser, Date updateDate, String updateUser, ProductPacking productPacking) {
        super(id, createDate, createUser, updateDate, updateUser);
        this.productPacking = productPacking;
    }


    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Date getImportDate() {
        return importDate;
    }

    public void setImportDate(Date importDate) {
        this.importDate = importDate;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getPackingTypeId() {
        return packingTypeId;
    }

    public void setPackingTypeId(Long packingTypeId) {
        this.packingTypeId = packingTypeId;
    }

    public Long getProductPackingId() {
        return productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }

    public PackingType getPackingType() {
        return packingType;
    }

    public void setPackingType(PackingType packingType) {
        this.packingType = packingType;
    }

    public String getPackingTypeCode() {
        return packingTypeCode;
    }

    public void setPackingTypeCode(String packingTypeCode) {
        this.packingTypeCode = packingTypeCode;
    }

    public String getProductPackingCode() {
        return productPackingCode;
    }

    public void setProductPackingCode(String productPackingCode) {
        this.productPackingCode = productPackingCode;
    }

    public String getProductPackingName() {
        return productPackingName;
    }

    public void setProductPackingName(String productPackingName) {
        this.productPackingName = productPackingName;
    }

    public Long getPackingTypeQuantity() {
        return packingTypeQuantity;
    }

    public void setPackingTypeQuantity(Long packingTypeQuantity) {
        this.packingTypeQuantity = packingTypeQuantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductPackingPrice getProductPackingPrice() {
        return productPackingPrice;
    }

    public void setProductPackingPrice(ProductPackingPrice productPackingPrice) {
        this.productPackingPrice = productPackingPrice;
    }

    public StoreProductPackingDetail getStoreProductPackingDetail() {
        return storeProductPackingDetail;
    }

    public void setStoreProductPackingDetail(StoreProductPackingDetail storeProductPackingDetail) {
        this.storeProductPackingDetail = storeProductPackingDetail;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Pallet getPallet() {
        return pallet;
    }

    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }

    public Long getPalletId() {
        return palletId;
    }

    public void setPalletId(Long palletId) {
        this.palletId = palletId;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public Long getClaimQuantityDelivery() {
        return claimQuantityDelivery;
    }

    public void setClaimQuantityDelivery(Long claimQuantityDelivery) {
        this.claimQuantityDelivery = claimQuantityDelivery;
    }

    public Long getClaimQuantityImport() {
        return claimQuantityImport;
    }

    public void setClaimQuantityImport(Long claimQuantityImport) {
        this.claimQuantityImport = claimQuantityImport;
    }

    public List<ImportStatementDetailPallet> getImportStatementDetailPallets() {
        return importStatementDetailPallets;
    }

    public void setImportStatementDetailPallets(List<ImportStatementDetailPallet> importStatementDetailPallets) {
        this.importStatementDetailPallets = importStatementDetailPallets;
    }

    public Long getOriginalQuantity() {
        return originalQuantity;
    }

    public void setOriginalQuantity(Long originalQuantity) {
        this.originalQuantity = originalQuantity;
    }

    public Long getClaimQuantity() {
        return claimQuantity == null ? 0L : claimQuantity;
    }

    public void setClaimQuantity(Long claimQuantity) {
        this.claimQuantity = claimQuantity;
    }
}
