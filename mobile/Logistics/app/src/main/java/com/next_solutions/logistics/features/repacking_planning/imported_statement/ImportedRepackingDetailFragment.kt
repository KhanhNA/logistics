package com.next_solutions.logistics.features.repacking_planning.imported_statement

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.*
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.adapter.ParentAdapter
import com.next_solutions.logistics.databinding.FragmentImportedRepackingDetailBinding
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE
import com.next_solutions.logistics.utils.Constants.SCROLL_TO_POSITION
import com.next_solutions.logistics.utils.RecycleViewCallBack
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ImportedRepackingDetailFragment : BaseFragment() {
    private lateinit var importedViewModel: ImportedRepackingDetailViewModel
    private lateinit var viewBinding: FragmentImportedRepackingDetailBinding
    private lateinit var parentAdapter: ParentAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpViewModelAndBinding()
        setUpRecycleView()
        getData()
        importedViewModel.init()
        return binding.root
    }

    private fun setUpViewModelAndBinding() {
        importedViewModel = viewModel as ImportedRepackingDetailViewModel
        viewBinding = binding as FragmentImportedRepackingDetailBinding
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (view!!.id == R.id.btnCamera) {
            val intent = Intent(activity, QRCodeScannerActivity::class.java)
            startActivityForResult(intent, Constants.REQUEST_CODE_SCAN)
        } else if (view.id == R.id.btnViewClaim) {
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            val claim = importedViewModel.claimDetails!![0].claim
            claim.claimDetails = importedViewModel.claimDetails
            bundle.putSerializable(Constants.FRAGMENT, ClaimDetailFragment::class.java)
            bundle.putSerializable(Constants.MODEL, claim)
            bundle.putSerializable(Constants.MODEL_REPACKING_PLANNING, importedViewModel.repackingPlanning)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    private fun setUpRecycleView() {
        val parentCallBack = RecycleViewCallBack { _, _ -> run {} }
        val childCallBack = RecycleViewCallBack { _, _ -> run {} }
        val spacing = 1

        parentAdapter = ParentAdapter(context,
                importedViewModel, importedViewModel.listData,
                R.layout.fragment_imported_repacking_detail_item, parentCallBack,
                R.layout.fragment_imported_repacking_detail_child_item, childCallBack)

        viewBinding.repackingPlanningDetails.apply {
            adapter = parentAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(ItemOffsetDecoration(spacing))
        }
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            SCROLL_TO_POSITION -> {
                importedViewModel.lastNotifyPosition?.let { parentAdapter.notifyItemChanged(it) }
                parentAdapter.notifyItemChanged(importedViewModel.notifyPosition.get()!!)
                layoutManager.scrollToPositionWithOffset(importedViewModel.notifyPosition.get()!!, 20)
            }
            NOTIFY_CHANGE -> {
                parentAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.REQUEST_CODE_SCAN) {
            importedViewModel.searchByCode(data!!.getStringExtra(Constants.RESULT_SCAN))
        }
    }

    private fun getData() {
        val repacking = activity?.intent?.getSerializableExtra(Constants.MODEL)
        importedViewModel.repackingPlanning = repacking as RepackingPlanning
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ImportedRepackingDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_imported_repacking_detail
    }

    override fun getRecycleResId(): Int {
        return 0
    }
}