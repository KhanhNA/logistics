package com.next_solutions.logistics.utils;

import java.util.List;

public class ValidateUtils {
    private ValidateUtils() {
    }

    public static boolean isNullOrEmpty(List list) {
        return list == null || list.isEmpty();
    }

    public static boolean isNullOrEmpty(Object obj) {
        return obj == null;
    }

    public static boolean isNullOrEmpty(Long obj) {
        return obj == null || obj == 0;
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || "".equals(str.trim());
    }

}
