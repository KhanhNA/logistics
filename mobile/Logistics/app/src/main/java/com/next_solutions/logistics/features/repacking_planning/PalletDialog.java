package com.next_solutions.logistics.features.repacking_planning;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModelProvider;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseDialogFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.BaseViewModelFactory;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.features.repacking_planning.repacked_statement.RepackedRepackingPlanningViewModel;
import com.next_solutions.logistics.models.ImportStatementDetail;
import com.next_solutions.logistics.models.Store;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.DialogCallBack;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN;
public class PalletDialog extends BaseDialogFragment {
    private ImportStatementDetail importStatementDetail;
    private DialogCallBack callBack;
    private ObservableField<List<ImportStatementDetail>> importStatementDetails;
    private Store store;
    private PalletDialogViewModel palletVM;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        palletVM = (PalletDialogViewModel) viewModel;
        RepackedRepackingPlanningViewModel repackViewModel = new ViewModelProvider(getBaseActivity(),
                BaseViewModelFactory.getInstance(getBaseActivity()
                        .getApplication(), RepackedRepackingPlanningViewModel.class))
                .get(RepackedRepackingPlanningViewModel.class);
        palletVM.setRepackViewModel(repackViewModel);
        palletVM.init();
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.dialog_search_pallet_item, viewModel, (view, pallet) -> {
            callBack.callBack(pallet);
            dismiss();
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        String tag = (String) view.getTag();
        if (Constants.SCAN_CODE.equals(tag)) {
            Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
            startActivityForResult(qrScan, REQUEST_CODE_SCAN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            String result = data.getStringExtra(Constants.RESULT_SCAN);
            palletVM.getSearchComponent().setData(result);
            palletVM.onClickButtonSearch();
        }
        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_pallet;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PalletDialogViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.pallets;
    }

    public DialogCallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(DialogCallBack callBack) {
        this.callBack = callBack;
    }

    public ImportStatementDetail getImportStatementDetail() {
        return importStatementDetail;
    }

    public void setImportStatementDetail(ImportStatementDetail importStatementDetail) {
        this.importStatementDetail = importStatementDetail;
    }

    public ObservableField<List<ImportStatementDetail>> getImportStatementDetails() {
        return importStatementDetails;
    }

    public void setImportStatementDetails(ObservableField<List<ImportStatementDetail>> importStatementDetails) {
        this.importStatementDetails = importStatementDetails;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
