package com.next_solutions.logistics.features.export_statement.export_statement_delivery

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.component.store.FromStoreComponent
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING

class ExportStatementDeliveryViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    var searchComponent = SearchComponent()
    var fromStoreComponent = FromStoreComponent(this)
    override fun search() {
        callApi(Service.callExportStatementService()
                .findExportStatement(searchComponent.code.get(), EMPTY_STRING, currentPageNumber, AppUtils.PAGE_SIZE
                        , ExportStatement.ExportStatementStatus.PROCESSING.value,
                        fromStoreComponent.stores[fromStoreComponent.selectedStorePosition].id.toString(), "from")
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Pageable<ExportStatement>>(appException,
                        Pageable::class.java, ExportStatement::class.java, null) {
                    override fun getDataSuccess(page: Pageable<ExportStatement>?) {
                        searchSuccess(page)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {

                    }
                }))
    }

    private fun searchSuccess(pageable: Pageable<ExportStatement>?) {
        if (pageable != null) {
            page = pageable
            setData(pageable.content as List<BaseModel>?)
        }
    }

    fun searchByCode(codeScan: String) {
        searchComponent.setData(codeScan)
        search()
    }

}