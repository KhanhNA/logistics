package com.next_solutions.logistics.repository.api.distributor;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;

import static com.next_solutions.logistics.repository.api.distributor.DistributorUrlPath.GET_ALL_DISTRIBUTOR;


public interface DistributorService {
    //    ========================DISTRIBUTOR========================
    @GET(GET_ALL_DISTRIBUTOR)
    Single<Response<ResponseBody>> getAllDistributor();
}
