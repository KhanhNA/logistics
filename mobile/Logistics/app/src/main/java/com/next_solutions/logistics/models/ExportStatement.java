package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExportStatement extends Base {
    public enum ExportStatementStatus {
        // Mới tạo lệnh xuất chưa thực xuất
        REQUESTED(0),
        // Đã tạo lệnh thực xuất chưa có lệnh nhập tương ứng
        RELEASED(1),
        // Đang được thực thi
        // 2.1: Đã tạo lệnh nhập nhưng chưa thực nhập,
        // 2.2: Tạo lệnh từ Merchant_order
        PROCESSING(2),
        // Được hoàn thành thực nhập/thực xuất cho merchant
        COMPLETED(3),
        // Lệnh bị hủy
        CANCELED(4),
        // Lệnh bị trả lại từ phía nhận
        RETURNED(5);

        private int value;

        ExportStatementStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    private Store fromStore;
    private Store toStore;
    private String code;
    private String qRCode;
    private String description;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date importDate;
    private List<ExportStatementDetail> exportStatementDetails;
    private Long toStoreId;
    private String toStoreCode;
    private Long fromStoreId;
    private String fromStoreCode;
    private String exportDate;
    private Integer status;
    private MerchantOrder merchantOrder;
    private String merchantCode;
    private Long merchantOrderId;
    private List<Long> merchantOrderIds;
    private String estimatedTimeOfArrival;

    public List<ExportStatementDetail> getExportStatementDetails() {
        return exportStatementDetails;
    }

    public void setExportStatementDetails(List<ExportStatementDetail> exportStatementDetails) {
        this.exportStatementDetails = exportStatementDetails;
    }

    public Store getFromStore() {
        return fromStore;
    }

    public void setFromStore(Store fromStore) {
        this.fromStore = fromStore;
    }

    public Store getToStore() {
        return toStore;
    }

    public void setToStore(Store toStore) {
        this.toStore = toStore;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getqRCode() {
        return qRCode;
    }

    public void setqRCode(String qRCode) {
        this.qRCode = qRCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getImportDate() {
        return importDate;
    }

    public void setImportDate(Date importDate) {
        this.importDate = importDate;
    }

    public Long getToStoreId() {
        return toStoreId;
    }

    public void setToStoreId(Long toStoreId) {
        this.toStoreId = toStoreId;
    }

    public String getToStoreCode() {
        return toStoreCode;
    }

    public void setToStoreCode(String toStoreCode) {
        this.toStoreCode = toStoreCode;
    }

    public Long getFromStoreId() {
        return fromStoreId;
    }

    public void setFromStoreId(Long fromStoreId) {
        this.fromStoreId = fromStoreId;
    }

    public String getFromStoreCode() {
        return fromStoreCode;
    }

    public void setFromStoreCode(String fromStoreCode) {
        this.fromStoreCode = fromStoreCode;
    }

    public String getExportDate() {
        return exportDate;
    }

    public void setExportDate(String exportDate) {
        this.exportDate = exportDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public MerchantOrder getMerchantOrder() {
        return merchantOrder;
    }

    public void setMerchantOrder(MerchantOrder merchantOrder) {
        this.merchantOrder = merchantOrder;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public Long getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(Long merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public List<Long> getMerchantOrderIds() {
        return merchantOrderIds;
    }

    public void setMerchantOrderIds(List<Long> merchantOrderIds) {
        this.merchantOrderIds = merchantOrderIds;
    }

    public String getEstimatedTimeOfArrival() {
        return estimatedTimeOfArrival;
    }

    public void setEstimatedTimeOfArrival(String estimatedTimeOfArrival) {
        this.estimatedTimeOfArrival = estimatedTimeOfArrival;
    }

    public String getEstimatedTimeOfArrivalDisplay() {
        return estimatedTimeOfArrival == null ? EMPTY_STRING : DateUtils.convertStringDateToDifferentType(estimatedTimeOfArrival, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);
    }
}
