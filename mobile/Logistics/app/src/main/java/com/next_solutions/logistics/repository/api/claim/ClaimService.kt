package com.next_solutions.logistics.repository.api.claim

import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath.Companion.ACCEPT_CLAIM
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath.Companion.CREATE_CLAIM
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath.Companion.FIND_BY_REFERENCE
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath.Companion.FIND_CLAIM
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath.Companion.FIND_CLAIM_BY_ID
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath.Companion.REJECT_CLAIM
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath.Companion.UPDATE_CLAIM
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath.Companion.UPDATE_IMAGE
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

@JvmSuppressWildcards
interface ClaimService {
    @POST(CREATE_CLAIM)
    fun createClaim(@Body claims: List<Claim?>?): Single<Response<ResponseBody>>

    @GET(FIND_CLAIM)
    fun findClaim(@Query("amenable") amenable: String,
                  @Query("code") code: String,
                  @Query("description") description: String,
                  @Query("pageNumber") pageNumber: Int,
                  @Query("pageSize") pageSize: Int,
                  @Query("status") status: Int?,
                  @Query("storeId") storeId: Long,
                  @Query("type") type: String,
                  @Query("fromCreateDate") fromCreateDate: String,
                  @Query("toCreateDate") toCreateDate: String): Single<Response<ResponseBody>>

    @GET(FIND_CLAIM_BY_ID)
    fun findClaimById(@Path("id") id: Long): Single<Response<ResponseBody>>

    @GET(FIND_BY_REFERENCE)
    fun findClaimByReference(@Query("referenceId") referenceId: Long,
                             @Query("type") type: String): Single<Response<ResponseBody>>

    @PATCH(ACCEPT_CLAIM)
    fun acceptClaim(@Path("id") id: Long): Single<Response<ResponseBody>>

    @PATCH(UPDATE_CLAIM)
    fun updateClaim(@Path("id") id: Long,
                    @Body claim: Claim): Single<Response<ResponseBody>>

    @PATCH(REJECT_CLAIM)
    fun rejectClaim(@Path("id") id: Long,
                    @Body claim: Claim): Single<Response<ResponseBody>>

    @Multipart
    @PATCH(UPDATE_IMAGE)
    fun uploadImage(@Part("adds") addImage: RequestBody?,
                    @Part("deletes") deleteImage: RequestBody?,
                    @Part("edits") editImage: RequestBody?,
                    @Part addFiles: ArrayList<MultipartBody.Part>,
                    @Path("id") claimId: Long): Single<Response<ResponseBody>>

}
