package com.next_solutions.logistics.adapter.repacked_repacking

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.RecycledViewPool
import com.next_solutions.base.BR
import com.next_solutions.logistics.R
import com.next_solutions.logistics.databinding.FragmentRepackedRepackingDetailItemBinding
import com.next_solutions.logistics.features.repacking_planning.repacked_statement.RepackedRepackingPlanningViewModel
import com.next_solutions.logistics.models.dto.ImportStatementDetailDTO


class RepackedRepackingAdapter(private val repackingViewModel: RepackedRepackingPlanningViewModel,
                               private val parentFragmentManager: FragmentManager) : RecyclerView.Adapter<RepackedRepackingAdapter.SplitRepackViewHolder>() {
    private val data: List<ImportStatementDetailDTO>?

    init {
        data = repackingViewModel.listData
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SplitRepackViewHolder {
        val binding = DataBindingUtil.inflate<FragmentRepackedRepackingDetailItemBinding>(LayoutInflater.from(parent.context),
                R.layout.fragment_repacked_repacking_detail_item
                , parent, false)
        return SplitRepackViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SplitRepackViewHolder, position: Int) {
        val detailDTO = data!![position]
        val splitRepackedAdapter = SplitRepackedAdapter(detailDTO, parentFragmentManager, repackingViewModel)
        val linearLayoutManager = LinearLayoutManager(holder.recyclerView.context,
                RecyclerView.VERTICAL, false)
        holder.recyclerView.adapter = splitRepackedAdapter
        holder.recyclerView.layoutManager = linearLayoutManager
        holder.recyclerView.setRecycledViewPool(RecycledViewPool())
        holder.bind(detailDTO, position)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    inner class SplitRepackViewHolder(var binding: FragmentRepackedRepackingDetailItemBinding) : RecyclerView.ViewHolder(binding.root) {
        var recyclerView: RecyclerView = binding.children
        fun bind(detailDTO: ImportStatementDetailDTO?, parentPosition: Int?) {
            binding.setVariable(BR.viewHolder, detailDTO)
            binding.setVariable(BR.viewModel, repackingViewModel)
            binding.parentPosition = parentPosition
        }

    }

}
