package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportStatement extends Base {
    public enum ImportStatementStatus {
        // Mới tạo lệnh nhập chưa thực nhập và chưa cộng kho
        NOT_UPDATE_INVENTORY(0),
        // Đã tạo lệnh thực nhập và cộng kho
        UPDATED_INVENTORY(1);

        private int value;

        ImportStatementStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    private Store fromStore;
    private Store toStore;
    private String code;
    private String description;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date importDate;
    private List<ImportStatementDetail> importStatementDetails;
    private List<ImportStatementDetailPallet> importStatementDetailPallets;
    private Long toStoreId;
    private String toStoreCode;
    private Long fromStoreId;
    private String fromStoreCode;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date exportDate;
    private Integer status;
    private ExportStatement exportStatement;
    private Long repackingPlanningId;
    private String estimatedTimeOfArrival;
    private Long totalVolume;
    private Long totalQuantity;

    public List<ImportStatementDetail> getImportStatementDetails() {
        return importStatementDetails;
    }

    public void setImportStatementDetails(List<ImportStatementDetail> importStatementDetails) {
        this.importStatementDetails = importStatementDetails;
    }

    public Store getFromStore() {
        return fromStore;
    }

    public void setFromStore(Store fromStore) {
        this.fromStore = fromStore;
    }

    public Store getToStore() {
        return toStore;
    }

    public void setToStore(Store toStore) {
        this.toStore = toStore;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getImportDate() {
        return importDate;
    }

    public String getImportDateDisplay() {
        return importDate == null ? EMPTY_STRING : DateUtils.clearTimeAndConvertDateToString(importDate, DateUtils.DATE_SHOW_FORMAT);
    }

    public void setImportDate(Date importDate) {
        this.importDate = importDate;
    }

    public Long getToStoreId() {
        return toStoreId;
    }

    public void setToStoreId(Long toStoreId) {
        this.toStoreId = toStoreId;
    }

    public String getToStoreCode() {
        return toStoreCode;
    }

    public void setToStoreCode(String toStoreCode) {
        this.toStoreCode = toStoreCode;
    }

    public Long getFromStoreId() {
        return fromStoreId;
    }

    public void setFromStoreId(Long fromStoreId) {
        this.fromStoreId = fromStoreId;
    }

    public String getFromStoreCode() {
        return fromStoreCode;
    }

    public void setFromStoreCode(String fromStoreCode) {
        this.fromStoreCode = fromStoreCode;
    }

    public Date getExportDate() {
        return exportDate;
    }

    public void setExportDate(Date exportDate) {
        this.exportDate = exportDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ExportStatement getExportStatement() {
        return exportStatement;
    }

    public void setExportStatement(ExportStatement exportStatement) {
        this.exportStatement = exportStatement;
    }

    public Long getRepackingPlanningId() {
        return repackingPlanningId;
    }

    public void setRepackingPlanningId(Long repackingPlanningId) {
        this.repackingPlanningId = repackingPlanningId;
    }

    public String getEstimatedTimeOfArrival() {
        return estimatedTimeOfArrival;
    }

    public void setEstimatedTimeOfArrival(String estimatedTimeOfArrival) {
        this.estimatedTimeOfArrival = estimatedTimeOfArrival;
    }

    public String getEstimatedTimeOfArrivalDisplay() {
        return estimatedTimeOfArrival == null ? EMPTY_STRING : DateUtils.convertStringDateToDifferentType(estimatedTimeOfArrival, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);
    }

    public List<ImportStatementDetailPallet> getImportStatementDetailPallets() {
        return importStatementDetailPallets;
    }

    public void setImportStatementDetailPallets(List<ImportStatementDetailPallet> importStatementDetailPallets) {
        this.importStatementDetailPallets = importStatementDetailPallets;
    }

    public Long getTotalVolume() {
        return totalVolume;
    }

    public Long getTotalVolumeByCm3() {
        return totalVolume / 1000;
    }

    public void setTotalVolume(Long totalVolume) {
        this.totalVolume = totalVolume;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

}
