package com.next_solutions.logistics.features.po.imported_po

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.Po
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.ClaimHelper
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath
import com.next_solutions.logistics.utils.Constants

class ImportedPoDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var po: Po
    lateinit var claimDetails: List<ClaimDetail>
    val claim = ObservableField<Boolean>(false)
    val buttonView = ObservableField<Boolean>(false)
    override fun init() {
        getPo()
    }

    private fun getPo() {
        callApi(Service.callPoService().findPoById(po.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Po>(appException, Po::class.java, null) {
                    override fun getDataSuccess(po: Po) {
                        getPoDetailSuccess(po)
                        getClaimReference()
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                    }
                }))
    }

    private fun getPoDetailSuccess(po: Po) {
        this.po = po
    }

    private fun getClaimReference() {
        callApi(Service.callClaimService().findClaimByReference(po.id, Claim.ClaimType.IMPORT_PO.value)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, List::class.java) {
                    override fun getDataSuccess(claimDetails: List<ClaimDetail>) {
                        getClaimReferenceSuccess(claimDetails)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone!!.message + " " + errorRespone.message)
                    }
                }))
    }

    private fun getClaimReferenceSuccess(claimDetails: List<ClaimDetail>) {
        this.claimDetails = claimDetails
        val claimHelper = ClaimHelper()
        po.importPo.importPoDetails.forEach { detail ->
            run {
                detail.fromClaim = false
                detail.originalQuantity = detail.quantity
            }
        }
        if (claimDetails.isNotEmpty()) {
            claim.set(true)
            buttonView.set(RuleHelper().hasRule(HttpRequest(Method.GET.value, ClaimUrlPath.FIND_CLAIM_BY_ID)))
            po.importPo.importPoDetails.addAll(claimDetails.asSequence()
                    .map { claimHelper.convertClaimDetailToImportPoDetail(it) }
                    .toList())
        } else {
            buttonView.set(false)
        }
        buttonView.notifyChange()
        setData(po.importPo.importPoDetails as List<BaseModel>?)
    }

    fun haveClaim(): ObservableField<Boolean> {
        return claim
    }
}