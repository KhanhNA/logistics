package com.next_solutions.logistics.features.claim.update_claim

import android.app.Application
import android.util.Log
import com.google.gson.Gson
import com.next_solutions.base.BaseModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.config.AppUtils.APPLICATION_JSON
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.helper.ImportStatementHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants.*
import io.reactivex.plugins.RxJavaPlugins
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File


@Suppress("UNCHECKED_CAST")
class UpdateClaimViewModel(application: Application) : BaseClaimViewModel(application) {
    var importPo: ImportPo? = null

    var repackingPlanning: RepackingPlanning? = null

    var exportStatement: ExportStatement? = null

    var importStatement: ImportStatement? = null

    var claim: Claim? = null

    var claimDetailsReference = arrayListOf<ClaimDetail>()

    private var deleteImage = arrayListOf<ClaimImage>()

    override fun init() {
        claim?.let {
            descriptionObservable.set(it.description)
            amenableObservable.set(it.amenable)
            descriptionObservable.notifyChange()
            amenableObservable.notifyChange()
        }
        getClaimDetail()
    }

    private fun getClaimDetail() {
        claim?.let {
            callApi(Service
                    .callClaimService()
                    .findClaimById(it.id)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<Claim>(appException, Claim::class.java, null) {
                        override fun getDataSuccess(claimStatement: Claim?) {
                            getClaimSuccess(claimStatement)
                        }

                        override fun onFailure(errorRespone: ErrorRespone?) {
                            Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                        }
                    }))
        }
    }

    private fun getClaimSuccess(claimStatement: Claim?) {
        claimStatement?.let {
            this.claim = claimStatement
            setData(this.claim!!.claimDetails as List<BaseModel>?)
            getReferenceStatement()
            images.addAll(claimStatement.claimImages.asSequence().map {
                Image().apply {
                    claimImage = ClaimImage().apply {
                        id = it.id
                        claim = Claim().apply { id = it.claim.id }
                        description = it.description
                        url = it.url
                    }
                }
            }.toList())
            sendBackAction(RELOAD_IMAGE)
        }
    }

    private fun getReferenceStatement() {
        claim?.let {
            when (it.type) {

                Claim.ClaimType.IMPORT_PO_UPDATE_PALLET -> {
                    getImportPoDetail()
                }
                Claim.ClaimType.REPACKING_PLANNING -> {
                    getRepackingPlanningDetail()
                }
                Claim.ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING -> {
                    getImportStatementRepackingPlanningDetail()
                }
                Claim.ClaimType.EXPORT_STATEMENT -> {
                    getExportStatementDetail()
                }
                Claim.ClaimType.DELIVERY -> {
                    getDeliveryDetail()
                }
                Claim.ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT -> {
                    getImportStatement()
                }
                Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_1 -> {
                    getOutOfPalletByStep()
                }
                Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_2 -> {
                    getOutOfPalletByStep()
                }
                else -> {
                }
            }
        }
    }

    private fun getImportPoDetail() {
        callApi(Service
                .callImportPoService()
                .findById(claim!!.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportPo>(appException, ImportPo::class.java, null) {

                    override fun getDataSuccess(importPo: ImportPo) {
                        getImportPoDetailSuccess(importPo)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))
    }

    private fun getImportPoDetailSuccess(importPo: ImportPo) {
        this.importPo = importPo
        claimDetailsReference.addAll(importPo.importPoDetails.asSequence().map {
            claimHelper.convertImportPoDetailToClaimDetail(it)
        }.toList())
        updateTotalQuantity()
    }


    private fun getRepackingPlanningDetail() {
        callApi(Service
                .callRepackingPlanningService()
                .findRepackingPlanningById(claim!!.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<RepackingPlanning>(appException, RepackingPlanning::class.java, null) {
                    override fun getDataSuccess(repackingPlanning: RepackingPlanning) {
                        getRepackingPlanningDetailSuccess(repackingPlanning)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))
    }

    private fun getRepackingPlanningDetailSuccess(repackingPlanning: RepackingPlanning) {
        this.repackingPlanning = repackingPlanning
        claimDetailsReference.addAll(repackingPlanning.repackingPlanningDetails.asSequence()
                .map { claimHelper.convertRepackingDetailToClaimDetail(it) }
                .toList())
        updateTotalQuantity()
    }

    private fun getImportStatementRepackingPlanningDetail() {
        callApi(Service
                .callRepackingPlanningService()
                .findRepackingPlanningById(claim!!.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<RepackingPlanning>(appException, RepackingPlanning::class.java, null) {
                    override fun getDataSuccess(repackingPlanning: RepackingPlanning) {
                        getImportStatementRepackingPlanningDetailSuccess(repackingPlanning)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))

    }

    private fun getImportStatementRepackingPlanningDetailSuccess(repackingPlanning: RepackingPlanning) {
        this.repackingPlanning = repackingPlanning
        repackingPlanning.repackingPlanningDetails.forEach { details ->
            run {
                details.repackingPlanningDetailRepackeds.forEach { repacked ->
                    run {
                        if (repacked.quantity != 0L) {
                            claimDetailsReference.add(claimHelper.convertDetailRepackedToClaimDetail(repacked))
                        }
                    }
                }
            }
        }
        updateTotalQuantity()
    }

    private fun getExportStatementDetail() {
        callApi(Service
                .callExportStatementService()
                .findExportStatementById(claim!!.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ExportStatement>(appException, ExportStatement::class.java, null) {
                    override fun getDataSuccess(exportStatement: ExportStatement) {
                        getExportStatementDetailSuccess(exportStatement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))
    }

    private fun getOutOfPalletByStep() {

        claim?.claimDetails?.forEach {
            it.totalQuantity = it.quantity;
            claimDetailsReference.add(it)
        }
        updateTotalQuantity()
    }

    private fun getExportStatementDetailSuccess(exportStatement: ExportStatement) {
        this.exportStatement = exportStatement
        claimDetailsReference.addAll(exportStatement.exportStatementDetails.asSequence().map {
            claimHelper.convertExportDetailToClaimDetail(it)
        }.toList())
        updateTotalQuantity()
    }

    private fun getDeliveryDetail() {
        callApi(Service
                .callImportStatementService()
                .findImportStatementById(claim!!.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportStatement>(appException, ImportStatement::class.java, null) {
                    override fun getDataSuccess(importStatement: ImportStatement) {
                        getImportStatementDetailSuccess(importStatement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))
    }

    private fun getImportStatement() {
        callApi(Service
                .callImportStatementService()
                .findImportStatementById(claim!!.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportStatement>(appException, ImportStatement::class.java, null) {
                    override fun getDataSuccess(importStatement: ImportStatement) {
                        getImportStatementDetailSuccess(importStatement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))
    }

    private fun getImportStatementDetailSuccess(importStatement: ImportStatement) {
        this.importStatement = importStatement
        val importStatementDetails =
                ImportStatementHelper(importStatement).groupDetailByExpireDateAndProductPacking()
        claimDetailsReference.addAll(importStatementDetails.asSequence()
                .map { claimHelper.convertImportDetailToClaimDetail(it) }
                .toList())
        updateTotalQuantity()
    }

    private fun updateTotalQuantity() {
        val minimizeDetail = arrayListOf<ClaimDetail>()
        if (claim!!.type.value != Claim.ClaimType.EXPORT_STATEMENT.value) {
            claim!!.claimDetails.forEach { detail ->
                run {
                    val result = claimDetailsReference
                            .asSequence()
                            .find {
                                it.productPacking.id == detail.productPacking.id && it.expireDate == detail.expireDate
                            }
                    result?.let {
                        detail.totalQuantity = it.totalQuantity
                    }
                }
                minimizeDetail.add(claimHelper.minimizeClaimDetailWithQuantity(detail))
            }
        } else {
            claim!!.claimDetails.forEach { detail ->
                run {
                    val results = claimDetailsReference
                            .asSequence()
                            .filter {
                                it.productPacking.id == detail.productPacking.id
                            }.toList()
                    detail.totalQuantity = 0
                    results.forEach { result ->
                        run {
                            detail.totalQuantity += result.totalQuantity
                        }
                    }
                }
                minimizeDetail.add(claimHelper.minimizeClaimDetailWithQuantity(detail))
            }
        }

        setData(minimizeDetail as List<BaseModel>?)
    }

    fun updateClaim() {
        if (validate()) {
            val claim = prepareClaim()
            if (claim.id != null) {
                callApiUpdateClaim(claim)
            }
        }
    }

    private fun callApiUpdateClaim(claim: Claim) {
        buttonEnable.set(false)
        RxJavaPlugins.setErrorHandler {
            it.printStackTrace()
        }
        callApi(Service
                .callClaimService()
                .updateClaim(claim.id, claim)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(respone: ResponseStatus?) {
                        updateClaimSuccess()
                        uploadImage(claim)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        buttonEnable.set(true)
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                    }

                    override fun onSuccess(response: Response<ResponseBody>) {
                        super.onSuccess(response)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                    }
                }))
    }

    override fun removeImage(image: Image) {
        images.remove(image)
        if (!image.isLocalImage) {
            deleteImage.add(ClaimImage().apply { id = image.claimImage.id })
        }
    }

    override fun uploadImage(claim: Claim) {
        for (image in images) {
            image.claimImage.claim = Claim().apply { id = claim.id }
        }

        val imagePaths = images.asSequence().filter { it.isLocalImage }.map { it.albumFile.path }.toList()

        val jsonAdd = Gson().toJson(images.asSequence().map { it.claimImage }.filter { it.id == null }.toList())
        val bodyJsonAdd = RequestBody.create(MediaType.parse(APPLICATION_JSON), jsonAdd)

        val jsonEdit = Gson().toJson(images.asSequence().map { it.claimImage }.filter { it.id != null }.toList())
        val bodyJsonEdit = RequestBody.create(MediaType.parse(APPLICATION_JSON), jsonEdit)

        val jsonDelete = Gson().toJson(deleteImage)
        val bodyJsonDelete = RequestBody.create(MediaType.parse(APPLICATION_JSON), jsonDelete)

        val parts = arrayListOf<MultipartBody.Part>()
        imagePaths.forEach { path: String ->
            run {
                val file = File(path)
                val fileReqBody = RequestBody.create(MediaType.parse("image/*"), file)
                parts.add(MultipartBody.Part.createFormData("files", file.name, fileReqBody))
            }
        }

        callApi(Service.callClaimService()
                .uploadImage(bodyJsonAdd, bodyJsonDelete, bodyJsonEdit, parts, claim.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(responseStatus: ResponseStatus?) {
                        uploadImageSuccess()
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                    }
                }))


    }

    private fun updateClaimSuccess() {
        sendBackAction(UPDATE_SUCCESS)
    }


    override fun prepareClaim(): Claim {
        val details = baseModelsE as ArrayList<ClaimDetail>
        claim?.let {
            return Claim().apply {
                id = it.id
                type = it.type
                referenceId = it.referenceId
                claimDetails = details
                claimDetails.forEach {
                    it.id = null
                    if (it.claim != null) {
                        val c = Claim()
                        c.setId(it.claim.getId())
                        it.claim = c
                    }
//                    if (it.productPacking != null) {
//                        val p = ProductPacking()
//                        p.setId(it.productPacking.getId())
//                        it.productPacking = p
//                    }
//                    if (it.productPackingPrice != null) {
//                        val pp = ProductPackingPrice();
//                        pp.setId(it.productPackingPrice.getId())
//                        it.productPackingPrice = pp;
//                    }
//                    if (it.palletDetail != null) {
//                        val pd = PalletDetail();
//                        pd.setId(it.palletDetail.getId())
//                        it.palletId = it.palletDetail.pallet.getId()
//                        it.palletDetail = pd
//
//                    }
//                    if (it.storeProductPackingDetail != null) {
//                        val sppd = StoreProductPackingDetail()
//                        sppd.setId(it.storeProductPackingDetail.getId())
//                        it.storeProductPackingDetail = sppd
//                    }
                }
                store = Store().apply {
                    code = it.store.code
                    id = it.store.id
                }
                status = Claim.ClaimStatus.WAIT_APPROVE.value
                description = descriptionObservable.get()
                amenable = amenableObservable.get()
            }
        }
        return Claim()
    }

    fun addClaimDetail(detail: ClaimDetail) {
        val minimizeDetail = claimHelper.minimizeClaimDetailWithoutQuantity(detail)
        val claimDetails = baseModelsE as ArrayList<ClaimDetail>
        notifyPosition.set(claimDetails.size)
        claimDetails.add(minimizeDetail)
        sendBackAction(NOTIFY_CHANGE_ITEM_ADD)
    }
}