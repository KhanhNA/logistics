package com.next_solutions.logistics.repository.api.error

interface ErrorPath {
    companion object {
        const val POST_ERROR = "/fake/monitor-error"
    }

}