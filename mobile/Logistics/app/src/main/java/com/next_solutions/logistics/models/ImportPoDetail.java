package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportPoDetail extends Base {

    private ImportPo importPo;

    private Product product;

    private ProductPacking productPacking;

    private Long quantity;

    private ProductPackingPrice productPackingPrice;

    private String expireDate;

    private List<ImportPoDetailPallet> importPoDetailPallets;

    public Long claimQuantity;
    public Long originalQuantity;
    public Boolean fromClaim;

    public String getExpireDateDisplay() {
        return expireDate == null ? Constants.EMPTY_STRING : DateUtils.convertStringDateToDifferentType(expireDate, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public ImportPo getImportPo() {
        return importPo;
    }

    public void setImportPo(ImportPo importPo) {
        this.importPo = importPo;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public ProductPackingPrice getProductPackingPrice() {
        return productPackingPrice;
    }

    public void setProductPackingPrice(ProductPackingPrice productPackingPrice) {
        this.productPackingPrice = productPackingPrice;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public List<ImportPoDetailPallet> getImportPoDetailPallets() {
        return importPoDetailPallets;
    }

    public void setImportPoDetailPallets(List<ImportPoDetailPallet> importPoDetailPallets) {
        this.importPoDetailPallets = importPoDetailPallets;
    }

    public BigDecimal getTotalPrice() {
        return new BigDecimal(quantity).multiply(productPackingPrice.getPrice()).multiply(new BigDecimal((100.0 + productPacking.getVat()) / 100.0));
    }
}