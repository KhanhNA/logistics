package com.next_solutions.logistics.models.dto;


public class ExpireDateAndProductPacking {
    private String expireDate;
    private Long productPackingId;

    public ExpireDateAndProductPacking() {
    }

    public ExpireDateAndProductPacking(String expireDate, Long productPackingId) {
        this.expireDate = expireDate;
        this.productPackingId = productPackingId;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Long getProductPackingId() {
        return productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }
}
