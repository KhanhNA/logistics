package com.next_solutions.logistics.repository.api.merchant_order;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.next_solutions.logistics.repository.api.merchant_order.MerchantOrderUrlPath.GET_INVENTORY_LEFT;
import static com.next_solutions.logistics.repository.api.merchant_order.MerchantOrderUrlPath.GET_ORDERS;

public interface MerchantOrderService {

    //    ========================MERCHANT ORDER========================
    @GET(GET_ORDERS)
    Call<ResponseBody> getOrders();

    @GET("/merchant-orders/list_po")
    Call<ResponseBody> getStatementDetailFromMerchantOrder(@Query("code") String code);

    @GET(GET_INVENTORY_LEFT)
    Single<Response<ResponseBody>> getInventoryLeft(@Query("productPackingCodes") List<String> productPackingCodes,
                                                    @Query("storeCodes") List<String> storeCodes);
}
