package com.next_solutions.logistics.models.dao;

import com.next_solutions.logistics.models.entity.PackingTypeEntity;

import androidx.room.Dao;

@Dao
public interface PackingTypeDao extends BaseDao<PackingTypeEntity>{


}
