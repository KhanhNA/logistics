package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.models.dto.HttpRequest

class RuleHelper {
    fun hasRules(httpRequests: List<HttpRequest>): Boolean {
        if (AppUtils.rule == null) {
            return false
        }
        httpRequests.forEach { request ->
            run {
                if (!hasRule(request)) {
                    return false
                }
            }
        }
        return true
    }

    fun hasRule(httpRequest: HttpRequest): Boolean {
        if (AppUtils.rule == null) {
            return false
        }
        AppUtils.rule.userAuthentication.authorities.forEach { authority ->
            run {
                if (authority.authority == httpRequest.toString()) {
                    return true
                }
            }
        }
        return false
    }
}
