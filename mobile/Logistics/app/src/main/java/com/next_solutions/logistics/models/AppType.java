package com.next_solutions.logistics.models;

public enum AppType {
    MOBILE("MOBILE"),
    WEB("WEB");
    private String value;

    AppType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
