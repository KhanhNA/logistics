package com.next_solutions.logistics.base.import_po

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.databinding.FragmentImportPoItemBinding
import com.next_solutions.logistics.models.ImportPoDetail

class ImportPoAdapter(private var viewModel: BaseImportPoViewModel,
                      private var context: Context,
                      private var fragmentManager: FragmentManager) : RecyclerView.Adapter<ImportPoAdapter.DetailViewHolder>() {
    private var listData: ArrayList<ImportPoDetail> = viewModel.listData

    init {
        listData = viewModel.listData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        val binding =
                DataBindingUtil.inflate<FragmentImportPoItemBinding>(LayoutInflater.from(parent.context),
                        R.layout.fragment_import_po_item, parent, false)
        return DetailViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        val importStatementDetail = listData[position]
        importStatementDetail.index = position + 1
        val childAdapter = ImportPoAdapterChild(importStatementDetail, fragmentManager, viewModel)

        val recycledViewPool = RecyclerView.RecycledViewPool()

        val manager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val recyclerView = holder.binding.listChild.apply {
            adapter = childAdapter
            layoutManager = manager
        }
        recyclerView.setRecycledViewPool(recycledViewPool)
        holder.bind(importStatementDetail, viewModel)
    }


    class DetailViewHolder(var binding: FragmentImportPoItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(detail: ImportPoDetail, viewModel: BaseImportPoViewModel) {
            binding.viewModel = viewModel
            binding.viewHolder = detail
        }
    }


}