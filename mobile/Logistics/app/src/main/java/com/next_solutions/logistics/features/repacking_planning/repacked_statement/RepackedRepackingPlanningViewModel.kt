package com.next_solutions.logistics.features.repacking_planning.repacked_statement

import android.app.Application
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.R
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.dto.ImportStatementDTO
import com.next_solutions.logistics.models.dto.ImportStatementDetailDTO
import com.next_solutions.logistics.models.dto.SplitRepack
import com.next_solutions.logistics.models.exception.InvalidDetailException
import com.next_solutions.logistics.models.exception.NotMatchQuantityException
import com.next_solutions.logistics.models.helper.ImportStatementDetailHelper
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath
import com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.ValidateUtils
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


open class RepackedRepackingPlanningViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var repackingPlanning: RepackingPlanning
    var searchComponent: SearchComponent = SearchComponent()
    var listData = ArrayList<ImportStatementDetailDTO>()
    var claimDetails: ArrayList<ClaimDetail>? = null
    var currentTotalQuantity = ObservableField<HashMap<String, Long>>(HashMap())
    val descriptionObservable = ObservableField<String>()
    var lastNotifyPosition: Int? = null

    //control display
    val canImportStore = ObservableField(false)
    val buttonEnable = ObservableField(true)
    val canCreateClaim = ObservableField(false)
    val buttonReviewClaim = ObservableField(false)
    val haveClaim = ObservableField(false)
    val fromClaimFragment = ObservableField(false)

    override fun init() {
        if (claimDetails == null) {
            getImportedClaimReference()
        } else {
            getImportClaimReferenceSuccess(this.claimDetails!!)
            getRepackingPlanningDetailSuccess(repackingPlanning)
        }
    }

    private fun getImportedClaimReference() {
        callApi(Service
                .callClaimService()
                .findClaimByReference(repackingPlanning.id, Claim.ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING.value)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, List::class.java) {
                    override fun getDataSuccess(imported: List<ClaimDetail>) {
                        getImportClaimReferenceSuccess(imported)
                        getRepackingPlanningDetail()
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        claimNotApproved()
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.canonicalName)
                        getRepackingPlanningDetail()
                    }
                }))
    }

    private fun getImportClaimReferenceSuccess(importedClaims: List<ClaimDetail>) {
        this.claimDetails = arrayListOf()
        this.claimDetails!!.addAll(importedClaims)
    }

    fun searchByCode(code: String) {
        searchComponent.setData(code)
        search()
    }

    fun reCalculator(addValue: Long, splitRepack: SplitRepack) {
        val total = currentTotalQuantity.get()
        val code = getKeyMapping(splitRepack)
        total?.let {
            val newValue = it[code]!!.plus(addValue)
            it[code] = newValue
        }
        currentTotalQuantity.notifyChange()
    }

    fun onQuantityChanged(s: CharSequence, splitRepack: SplitRepack) {
        try {
            val newQuantity = s.toString().toLong()
            if (splitRepack.quantity != newQuantity) {
                val addValue = newQuantity - splitRepack.quantity
                reCalculator(addValue, splitRepack)
                splitRepack.quantity = newQuantity
            }
        } catch (e: Exception) {
            reCalculator(splitRepack.quantity * -1, splitRepack)
            if (s.toString() == EMPTY_STRING) {
                splitRepack.quantity = 0
            }
            Log.e(ERROR_PROCESS, e.toString() + " " + this.javaClass.name)
        }
    }

    private fun getRepackingPlanningDetail() {
        callApi(Service
                .callRepackingPlanningService()
                .findRepackingPlanningById(repackingPlanning.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<RepackingPlanning>(appException, RepackingPlanning::class.java, null) {
                    override fun getDataSuccess(repackingPlanning: RepackingPlanning) {
                        getRepackingPlanningDetailSuccess(repackingPlanning)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                    }
                })
        )
    }

    private fun getRepackingPlanningDetailSuccess(repackingPlanning: RepackingPlanning) {
        this.repackingPlanning = repackingPlanning
        val hashMap = createMapping(repackingPlanning)
        subQuantityImportPlanning(hashMap)
        if (claimNotApproved()) {
            disableAndHideAllButton()
        } else {
            if (doNotHaveClaim()) {
                enableImportButtonAndCreateButton()
            } else {
                disableCreateClaimAndEnableViewAndImportButton()
            }
        }
        removeProductWithQuantityEqualZero(hashMap)
        sendBackAction(NOTIFY_CHANGE)
    }

    private fun claimNotApproved(): Boolean {
        return claimDetails == null || (claimDetails!!.isNotEmpty()
                && claimDetails!![0].claim.status == Claim.ClaimStatus.WAIT_APPROVE.value)
    }

    private fun disableAndHideAllButton() {
        buttonReviewClaim.set(false)
        haveClaim.set(false)
        canCreateClaim.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
        canImportStore.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ImportStatementUrlPath.CREATE_IMPORT_STATEMENT_FROM_REPACKING_PLANNING)))
        buttonEnable.set(false)
    }

    private fun doNotHaveClaim(): Boolean {
        return claimDetails!!.isEmpty()
    }

    private fun enableImportButtonAndCreateButton() {
        buttonReviewClaim.set(false)
        haveClaim.set(false)
        canCreateClaim.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
        canImportStore.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ImportStatementUrlPath.CREATE_IMPORT_STATEMENT_FROM_REPACKING_PLANNING)))
    }

    private fun disableCreateClaimAndEnableViewAndImportButton() {
        buttonReviewClaim.set(RuleHelper().hasRule(HttpRequest(Method.GET.value, ClaimUrlPath.FIND_CLAIM_BY_ID)))
        haveClaim.set(true)
        canCreateClaim.set(false)
        canCreateClaim.notifyChange()
        canImportStore.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ImportStatementUrlPath.CREATE_IMPORT_STATEMENT_FROM_REPACKING_PLANNING)))
    }

    private fun createMapping(repackingPlanning: RepackingPlanning): HashMap<String, ImportStatementDetailDTO> {
        val hashMap = HashMap<String, ImportStatementDetailDTO>()
        repackingPlanning.repackingPlanningDetails.forEach { detail ->
            run {
                detail.repackingPlanningDetailRepackeds.forEach { repacked ->
                    run {
                        if (repacked.quantity != 0L) {
                            val detailDTO = ImportStatementDetailDTO().apply {
                                expireDate = detail.expireDateStr
                                productPackingId = repacked.productPacking.id
                                productPackingCode = repacked.productPacking.code
                                quantity = repacked.quantity
                                productName = detail.productPacking.product.name
                                uom = detail.productPacking.uom
                                packingQuantity = detail.productPacking.packingType.quantity
                            }
                            val key = detailDTO.expireDate + detailDTO.productPackingCode

                            if (hashMap.containsKey(key)) {
                                val importDetail = hashMap[key]
                                importDetail?.let {
                                    it.quantity = it.quantity + repacked.quantity
                                }
                            } else {
                                detailDTO.splitRepacks = arrayListOf(SplitRepack().apply {
                                    quantity = 0
                                    pallet = Pallet()
                                    productPackingCode = detailDTO.productPackingCode
                                    expireDate = detailDTO.expireDate
                                })
                                hashMap[key] = detailDTO
                            }
                        }
                    }
                }
            }
        }
        return hashMap
    }

    private fun removeProductWithQuantityEqualZero(hashMap: HashMap<String, ImportStatementDetailDTO>) {
        val keys = hashMap.keys
        keys.forEach { key ->
            run {
                val importStatementDetailDTO = hashMap[key]
                importStatementDetailDTO?.let {
                    if (it.quantity != 0L) {
                        listData.add(it)
                        currentTotalQuantity.get()?.set(getKeyMapping(it), 0)
                    }
                }
            }
        }
    }

    fun getKeyMapping(importStatementDetailDTO: ImportStatementDetailDTO): String {
        return importStatementDetailDTO.expireDate + importStatementDetailDTO.productPackingCode
    }

    private fun getKeyMapping(splitRepack: SplitRepack): String {
        return splitRepack.expireDate + splitRepack.productPackingCode
    }

    private fun subQuantityImportPlanning(hashMap: HashMap<String, ImportStatementDetailDTO>) {
        hashMap.keys.forEach { key ->
            val importStatementDetailDTO = hashMap[key]
            run {
                importStatementDetailDTO!!.claimQuantity = 0L
                importStatementDetailDTO.originalQuantity = importStatementDetailDTO.quantity
            }
        }
        if (!claimDetails.isNullOrEmpty() && claimDetails!![0].claim.status == Claim.ClaimStatus.APPROVED.value) {
            claimDetails!!.forEach { detailClaim ->
                run {
                    val key = detailClaim.expireDate + detailClaim.productPacking.code
                    if (hashMap.containsKey(key)) {
                        hashMap[key]?.let {
                            it.quantity -= detailClaim.quantity
                            it.claimQuantity += detailClaim.quantity
                        }
                    }
                }
            }
        }
    }

    fun onCreate() {
        val importDTO = ImportStatementDTO().apply {
            description = descriptionObservable.get()
            fromStoreId = repackingPlanning.store.id
            fromStoreCode = repackingPlanning.store.code
            toStoreCode = repackingPlanning.store.code
            toStoreId = repackingPlanning.store.id
            repackingPlanningId = repackingPlanning.id
        }
        try {
            validateAndSetRepackingPlanningDetail(importDTO)
            buttonEnable.set(false)
            callApi(Service.callImportStatementService()
                    .createImportStatementFromRepackingPlanning(importDTO)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                        override fun getDataSuccess(responseStatus: ResponseStatus) {
                            sendBackAction(SUCCESS)
                        }

                        override fun onFailure(errorRespone: ErrorRespone) {
                            buttonEnable.set(true)
                        }
                    }))

        } catch (notMatchQuantity: NotMatchQuantityException) {
            changeViewAndSaveLastPosition("NOT_MATCH_QUANTITY", false)
        } catch (invalidDetail: InvalidDetailException) {
            changeViewAndSaveLastPosition(NOTIFY_CHANGE_ITEM, false)
        }
    }

    @Throws(NotMatchQuantityException::class, InvalidDetailException::class)
    private fun validateAndSetRepackingPlanningDetail(importStatementDTO: ImportStatementDTO) {
        val converter = ImportStatementDetailHelper()
        val importStatementDetailPallets = arrayListOf<ImportStatementDetailPallet>()
        val importStatementDetailDTOs = arrayListOf<ImportStatementDetailDTO>()
        for ((position, detailDTO) in listData.withIndex()) {
            if (currentTotalQuantity.get()?.get(getKeyMapping(detailDTO)) != detailDTO.quantity) {
                notifyPosition.set(position)
                throw NotMatchQuantityException()
            }

            detailDTO.splitRepacks.forEach {
                if (it.quantity != 0L && it.pallet.id != null) {
                    importStatementDetailPallets.add(converter.convertImportDetailToDetailPallet(it, detailDTO))
                } else {
                    notifyPosition.set(position)
                    throw InvalidDetailException()
                }
            }
            importStatementDetailDTOs.add(detailDTO)
        }
        importStatementDTO.apply {
            this.importStatementDetails = importStatementDetailDTOs
            this.importStatementDetailPallets = importStatementDetailPallets
        }
    }

    private fun changeViewAndSaveLastPosition(message: String, state: Boolean) {
        lastNotifyPosition?.let {
            listData[it].checked = null
        }
        listData[notifyPosition.get()!!].checked = state
        sendBackAction(message)
        lastNotifyPosition = notifyPosition.get()
    }

    override fun search() {
        val textSearch = searchComponent.code.get()
        if (ValidateUtils.isNullOrEmpty(textSearch)) {
            return
        }
        for ((position, detail) in listData.withIndex()) {
            if (detail.productName.toLowerCase(Locale.ROOT).contains(textSearch.toString().toLowerCase(Locale.ROOT))
                            .or(detail.productPackingCode.contains(textSearch.toString(), true))) {
                notifyPosition.set(position)
                changeViewAndSaveLastPosition(SCROLL_TO_POSITION, true)
                return
            }
        }
    }

    fun getColor(importStatementDTO: ImportStatementDetailDTO): Int {
        if (importStatementDTO.checked != null) {
            return if (importStatementDTO.checked) {
                ContextCompat.getColor(getApplication(), R.color.light_orange)
            } else {
                ContextCompat.getColor(getApplication(), R.color.light_red_2)
            }
        }
        return ContextCompat.getColor(getApplication(), R.color.white)
    }

    fun hideButtonViewClaim() {
        fromClaimFragment.set(true)
    }

}