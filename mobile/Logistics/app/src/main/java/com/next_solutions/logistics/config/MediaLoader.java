package com.next_solutions.logistics.config;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.repository.HttpHelper;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.AlbumLoader;

public class MediaLoader implements AlbumLoader {
    @Override
    public void load(ImageView imageView, AlbumFile albumFile) {
        load(imageView, albumFile.getPath());
    }

    @Override
    public void load(ImageView imageView, String url) {

//        GlideUrl glideUrl = new GlideUrl(url,
//                new LazyHeaders.Builder()
//                        .addHeader("Authorization", HttpHelper.tokenLogin)
//                        .build());
//        Glide.with(imageView.getContext())
//                .load(glideUrl)
//                .error(R.drawable.placeholder)
//                .placeholder(R.drawable.placeholder)
//                .into(imageView)
//        ;

        Glide.with(imageView.getContext())
                .load(url)
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(imageView);
    }
}
