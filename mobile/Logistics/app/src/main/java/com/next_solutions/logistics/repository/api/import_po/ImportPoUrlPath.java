package com.next_solutions.logistics.repository.api.import_po;

public interface ImportPoUrlPath {
    String FIND_IMPORT_PO = "/import-pos";
    String FIND_BY_ID = "/import-pos/{id}";
    String UPDATE_PALLET = "/import-pos/update-pallet/{id}";
    String CREATE_IMPORT_PO = "/import-pos";
}
