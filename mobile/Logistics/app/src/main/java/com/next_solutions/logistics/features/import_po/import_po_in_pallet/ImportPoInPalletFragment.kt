package com.next_solutions.logistics.features.import_po.import_po_in_pallet

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.import_po.BaseImportPoFragment
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.utils.Constants

class ImportPoInPalletFragment : BaseImportPoFragment() {

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.reviewClaim) {
            val importPoViewModel = viewModel as ImportPoInPalletViewModel
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            val claim = importPoViewModel.claimDetails?.get(0)?.claim!!
            claim.claimDetails = importPoViewModel.claimDetails
            bundle.putSerializable(Constants.FRAGMENT, ClaimDetailFragment::class.java)
            bundle.putSerializable(Constants.MODEL, claim)
            bundle.putSerializable(Constants.MODEL_IMPORT_PO, importPoViewModel.importPo)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ImportPoInPalletViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_import_po_in_pallet
    }

    override fun getRecycleResId(): Int {
        return R.id.details
    }
}