package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class RepackingPlanningDetailPallet extends Base {

    public Long claimQuantity;
    public Long originalQuantity;
    private RepackingPlanningDetail repackingPlanningDetail;
    private PalletDetail palletDetail;
    private Long quantity;

    public RepackingPlanningDetail getRepackingPlanningDetail() {
        return repackingPlanningDetail;
    }

    public void setRepackingPlanningDetail(RepackingPlanningDetail repackingPlanningDetail) {
        this.repackingPlanningDetail = repackingPlanningDetail;
    }

    public PalletDetail getPalletDetail() {
        return palletDetail;
    }

    public void setPalletDetail(PalletDetail palletDetail) {
        this.palletDetail = palletDetail;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}