package com.next_solutions.logistics.repository.api;

public enum Method {
    POST("POST"), GET("GET"), PATCH("PATCH");
    private String value;

    Method(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
