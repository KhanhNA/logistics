package com.next_solutions.logistics.features.login;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.exceptionHandle.AppException;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.AppUser;
import com.next_solutions.logistics.models.Language;
import com.next_solutions.logistics.models.Rule;
import com.next_solutions.logistics.repository.HttpHelper;
import com.next_solutions.logistics.repository.Service;
import com.next_solutions.logistics.utils.ValidateUtils;

import java.util.List;
import java.util.Locale;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

public class LoginViewModel extends BaseViewModel<AppUser> {
    private ObservableList<Language> languages = new ObservableArrayList<>();
    private ObservableField<String> username = new ObservableField<>();
    private ObservableField<String> password = new ObservableField<>();
    private Boolean saveInfo = false;
    private ObservableField<String> languageCode = new ObservableField<>(Locale.getDefault().getLanguage());

    private int languagePosition = 0;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        try {
            AppUser user = new AppUser();
            model.set(user);
            initLanguage();
            languagePosition = (AppUtils.getInstance().getPreferencesHelper().getCurrentLanguage());
        } catch (Exception e) {
            Log.e(ERROR_PROCESS, e.getMessage());
            if (appException != null) {
                appException.postValue(e);
            }
        }
    }

    public int getLanguagePosition() {
        return languagePosition;
    }

    public void setLanguagePosition(int languagePosition) {
        if (this.languagePosition != languagePosition) {
            this.languagePosition = languagePosition;
            languageCode.set(AppUtils.LANGUAGE_CODE[languagePosition]);
            AppUtils.getInstance().getPreferencesHelper().setCurrentLanguage(languagePosition);
            sendBackAction("CHANGE_LOGIN_TEXT");
        }
    }

    public void login() { //login
        AppUser appUser = model.get();
        if (appUser != null) {
            appUser = new AppUser();
            appUser.setUsername(username.get());
            appUser.setPassword(password.get());
            appUser.setLoginStatus(0);
            if (password.get() != null && !password.get().equals("abc@123")) {
                if (password.get().equals("1")) {
                    AppUtils.BASE_URL = "http://192.168.1.69:8888";
                    AppUtils.BASE_URL_OAUTH = "http://192.168.1.69:9999";
                    AppUtils.IMAGE_URL = "http://192.168.1.69:8888/files/";
                }
                if (password.get().equals("2")) {
                    AppUtils.BASE_URL = "http://logistics.nextsolutions.com.vn:8888";
                    AppUtils.BASE_URL_OAUTH = "http://sso.nextsolutions.com.vn:9999";
                    AppUtils.IMAGE_URL = "http://logistics.nextsolutions.com.vn:8888/files/";
                }
                if (password.get().equals("3")) {
                    AppUtils.BASE_URL = "http://dev.nextsolutions.com.vn:8888";
                    AppUtils.BASE_URL_OAUTH = "http://dev.nextsolutions.com.vn:9999";
                    AppUtils.IMAGE_URL = "http://dev.nextsolutions.com.vn:8888/files/";
                }
                if (password.get().equals("4")) {
                    AppUtils.BASE_URL = "http://192.168.1.21:8888";
                    AppUtils.BASE_URL_OAUTH = "http://192.168.1.69:9999";
                    AppUtils.IMAGE_URL = "http://192.168.1.21:8888/files/";
                }
                if (password.get().equals("5")) {
                    AppUtils.BASE_URL = "http://192.168.1.66:8888";
                    AppUtils.BASE_URL_OAUTH = "http://192.168.1.69:9999";
                    AppUtils.IMAGE_URL = "http://192.168.1.66:8888/files/";
                }
            }
            appUser.setPassword("abc@123");
            if (ValidateUtils.isNullOrEmpty(appUser.getUsername())) {
                appUser.setLoginStatus(R.string.userEmpty);
            }
            if (ValidateUtils.isNullOrEmpty(appUser.getPassword())) {
                appUser.setLoginStatus(R.string.userEmpty);
            }
            if (appUser.getLoginStatus() == R.string.userEmpty) {
                showMessage(appUser.getLoginStatus(), "loginError");
                return;
            }

            for (Language language : languages) {
                if (language.getCode().equals(languageCode.get())) {
                    AppUtils.setLanguage(language.getId());
                }
            }
            if (AppUtils.getLanguage() == null) {
                String langCode = Locale.getDefault().getLanguage();
                for (Language language : languages) {
                    if (language.getCode().equals(langCode)) {
                        AppUtils.setLanguage(language.getId());
                    }
                }
            }
            if (AppUtils.getLanguage() != null) {
                HttpHelper.requestLogin(appException, appUser.getUsername(), appUser.getPassword(), saveInfo, AppUtils.getLanguage(), (call1, response, body, t) -> getRole(body));
            } else {
                sendBackAction("CANT_CONNECT_SERVER");
                initLanguage();
            }
        }
    }

    private void initLanguage() {
        AppUtils.setLanguage(1L);
        callApi(Service.callLanguageService()
                .getAllLanguages()
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<List<Language>>(appException, Language.class, List.class) {
                    @Override
                    public void getDataSuccess(List<Language> languages) {
                        success(languages);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        if (errorRespone.getError() != null) {
                            Log.e(ERROR_PROCESS, errorRespone.getError());
                        }
                    }
                }));
    }

    private void getRole(Object token) {
        if (token != null) {
            callApi(Service.callUserService()
                    .getUser()
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(new RxSingleSubscriber<Rule>(appException, Rule.class, null) {
                        @Override
                        public void getDataSuccess(Rule rule) {
                            getRoleSuccess(rule);
                        }

                        @Override
                        public void onFailure(ErrorRespone errorRespone) {
                            Log.e("ErrorRespone", errorRespone.getError());
                        }
                    }));
        } else {
            appException.postValue(new AppException(R.string.error_username_or_password, "error_username_or_password"));
        }
    }

    private void success(List<Language> languages) {
        this.languages.clear();
        this.languages.addAll(languages);
    }

    private void getRoleSuccess(Rule rule) {
        try {
            AppUtils.setRule(rule);

            view.action("startHome", null, this, null);

        } catch (Exception ex) {
            appException.setValue(ex);
        } catch (AppException e) {
            Log.e("Error", e.getMessage());
        }
    }

    public ObservableList<Language> getLanguages() {
        return languages;
    }

    public ObservableField<String> getUsername() {
        return username;
    }

    public void setUsername(ObservableField<String> username) {
        this.username = username;
    }

    public ObservableField<String> getPassword() {
        return password;
    }

    public void setPassword(ObservableField<String> password) {
        this.password = password;
    }

    public Boolean getSaveInfo() {
        return saveInfo;
    }

    public void setSaveInfo(Boolean saveInfo) {
        this.saveInfo = saveInfo;
    }

    ObservableField<String> getLanguageCode() {
        return languageCode;
    }


}
