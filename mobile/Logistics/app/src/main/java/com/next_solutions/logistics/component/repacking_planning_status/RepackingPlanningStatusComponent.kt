package com.next_solutions.logistics.component.repacking_planning_status

class RepackingPlanningStatusComponent {
    var selectedStatusPosition = 0
        //    --    NEW(0), REPACKED(1), IMPORTED(2), CANCEL(-1);-->
        get() {
            if (field == 3) {
                return -1
            }
            return field
        }

    fun resetData() {
        selectedStatusPosition = 0
    }
}