package com.next_solutions.logistics.models.dao;

import com.next_solutions.logistics.models.entity.ProductDescriptionEntity;

import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface ProductDescriptionDao extends BaseDao<ProductDescriptionEntity>{

    @Query("DELETE FROM product_description")
    void deleteAll();
}
