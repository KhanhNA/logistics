package com.next_solutions.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.next_solutions.logistics.config.AppDatabase;
import com.next_solutions.logistics.models.dao.ProductDescriptionDao;
import com.next_solutions.logistics.models.entity.ProductDescriptionEntity;
import com.next_solutions.logistics.repository.callback.DatabaseCallback;

import java.util.List;


public class ProductDescriptionRepository {
    private ProductDescriptionDao productDescriptionDao;

    public ProductDescriptionRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        productDescriptionDao = db.productDescriptionDao();
    }

    public void insert(ProductDescriptionEntity entity) {
        new ProductDescriptionRepository.insertAsyncTask(productDescriptionDao).execute(entity);
    }
    public void insert(List<ProductDescriptionEntity> entity) {
        ProductDescriptionEntity[] array = entity.toArray(new ProductDescriptionEntity[0]);
        new ProductDescriptionRepository.insertAsyncTask(productDescriptionDao).execute(array);
    }
    public void deleteAll(DatabaseCallback databaseCallback){
       /* Completable.fromAction(() -> productDescriptionDao.deleteAll()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Completable.CompletableSubscriber() {
            @Override
            public void onCompleted() {
                databaseCallback.onSuccess(0);
            }
            @Override
            public void onError(Throwable e) {
                databaseCallback.onError(0, e);
            }

            @Override
            public void onSubscribe(Subscription d) {
            }
        });*/
    }
    private static class insertAsyncTask extends AsyncTask<ProductDescriptionEntity, Void, Void> {

        private ProductDescriptionDao mAsyncTaskDao;

        insertAsyncTask(ProductDescriptionDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ProductDescriptionEntity... params) {
            mAsyncTaskDao.insert(params);
            return null;
        }
    }
}
