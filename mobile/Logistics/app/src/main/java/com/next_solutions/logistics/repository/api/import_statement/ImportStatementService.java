package com.next_solutions.logistics.repository.api.import_statement;

import com.next_solutions.logistics.models.ImportStatement;
import com.next_solutions.logistics.models.Statement;
import com.next_solutions.logistics.models.dto.ImportStatementDTO;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.ACTUALLY_IMPORT;
import static com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.CREATE_FROM_EXPORT_STATEMENT;
import static com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.CREATE_IMPORT_STATEMENT_FROM_REPACKING_PLANNING;
import static com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.FIND_IMPORT_STATEMENT_BY_ID;
import static com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.FIND_IMPORT_STATEMENT_FROM_REPACKING_PLANNING;
import static com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.GET_ALL;
import static com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.UPDATE_IMPORT_STATEMENT;

public interface ImportStatementService {
    //  ========================IMPORT STATEMENT========================
    @POST("/import-statements/import-fc")
    Call<ResponseBody> createImportStatementFC(@Body Statement statement);

    @GET(GET_ALL)
    Single<Response<ResponseBody>> getAll(@Query("code") String code,
                                          @Query("status") Integer status,
                                          @Query("pageNumber") Integer pageNumber,
                                          @Query("pageSize") Integer pageSize,
                                          @Query("type") String type);

    @POST("/import-statements")
    Single<Response<ResponseBody>> createImportStatement(@Body List<ImportStatement> importStatements);

    @POST(CREATE_FROM_EXPORT_STATEMENT)
    Single<Response<ResponseBody>> createFromExportStatement(@Body ImportStatementDTO importStatementDto);

    @GET(FIND_IMPORT_STATEMENT_BY_ID)
    Single<Response<ResponseBody>> findImportStatementById(@Path("id") Long id);

    @POST(CREATE_IMPORT_STATEMENT_FROM_REPACKING_PLANNING)
    Single<Response<ResponseBody>> createImportStatementFromRepackingPlanning(@Body ImportStatementDTO importStatementDTO);

    @GET(FIND_IMPORT_STATEMENT_FROM_REPACKING_PLANNING)
    Single<Response<ResponseBody>> findImportStatementFromRepackingPlanning(@Path("id") Long id);

    @PATCH(UPDATE_IMPORT_STATEMENT)
    Single<Response<ResponseBody>> updateImportStatement(@Path("id") Long id, @Body ImportStatement importStatement);

    @PATCH(ACTUALLY_IMPORT)
    Single<Response<ResponseBody>> actuallyImport(@Path("id") Long id,
                                                  @Body ImportStatement importStatement);

}
