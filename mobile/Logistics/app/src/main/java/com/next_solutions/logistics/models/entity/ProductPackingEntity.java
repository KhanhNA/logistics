package com.next_solutions.logistics.models.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;


@Entity(tableName = "packing_product", foreignKeys = {
        @ForeignKey(entity = ProductEntity.class,
                parentColumns = "id",
                childColumns = "product_id"),
        @ForeignKey(entity = PackingTypeEntity.class,
                parentColumns = "id",
                childColumns = "packing_type_id")})
public class ProductPackingEntity {

    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;

    @ColumnInfo(name = "code")
    private String code;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "vat")
    private Integer vat;

    @ColumnInfo(name = "uom")
    private String uom;

    @ColumnInfo(name = "barcode")
    private String barcode;

    @ColumnInfo(name = "product_id", index = true)
    private Long productId;

    @ColumnInfo(name = "packing_type_id", index = true)
    private Long packingTypeId;

    @ColumnInfo(name = "status")
    private Boolean status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVat() {
        return vat;
    }

    public void setVat(Integer vat) {
        this.vat = vat;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getPackingTypeId() {
        return packingTypeId;
    }

    public void setPackingTypeId(Long packingTypeId) {
        this.packingTypeId = packingTypeId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
