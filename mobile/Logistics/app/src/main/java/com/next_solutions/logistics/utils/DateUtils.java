package com.next_solutions.logistics.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class DateUtils {
    public static final String DATE_SHOW_FORMAT = "dd/MM/yyyy";
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS'Z'";

    private DateUtils() {
    }

    public static String convertDateToString(Date date, String type) {
        if (ValidateUtils.isNullOrEmpty(date)) {
            return "";
        }
        String result;
        SimpleDateFormat formatter = new SimpleDateFormat(type, Locale.US);
        result = formatter.format(date);
        return result;
    }

    public static String clearTimeAndConvertDateToString(Date date, String type) {
        String result;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        date = convertDayMonthYearToDate(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
        SimpleDateFormat formatter = new SimpleDateFormat(type);
        result = formatter.format(date);
        return result;
    }

    public static Date convertStringToDate(String date, String type) {
        DateFormat format = new SimpleDateFormat(type, Locale.US);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            Log.e(Constants.ERROR_PROCESS, e.getMessage());
        }
        return null;
    }

    public static Date convertDayMonthYearToDate(int day, int month, int year) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(year, month, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static String convertDayMonthYearToString(int day, int month, int year, String type) {
        Date date = convertDayMonthYearToDate(day, month, year);
        return convertDateToString(date, type);
    }

    public static String convertStringDateToDifferentType(String date, String currentFormat, String displayFormat) {
        if (ValidateUtils.isNullOrEmpty(date)) {
            return "";
        }
        return DateUtils.convertDateToString(DateUtils.convertStringToDate(date, currentFormat), displayFormat);
    }

    public static Date getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        return DateUtils.convertDayMonthYearToDate(day, month, year);

    }

    public static Date reduceOneMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    }

}
