package com.next_solutions.logistics.repository.api.import_statement;

public interface ImportStatementUrlPath {
    String ACTUALLY_IMPORT = "/import-statements/{id}/import";
    String UPDATE_IMPORT_STATEMENT = "/import-statements/{id}";
    String FIND_IMPORT_STATEMENT_FROM_REPACKING_PLANNING = "/import-statements/repacking-planning/{id}";
    String CREATE_IMPORT_STATEMENT_FROM_REPACKING_PLANNING = "/import-statements/from-repacking-planning";
    String FIND_IMPORT_STATEMENT_BY_ID = "/import-statements/{id}";
    String CREATE_FROM_EXPORT_STATEMENT = "/import-statements/from-export-statement";
    String GET_ALL = "/import-statements/find";
}
