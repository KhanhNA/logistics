package com.next_solutions.logistics.features.test;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.next_solutions.base.BaseFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.logistics.R;


public class ListOrderFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return binding.getRoot();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_list_order;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ListOrderActivityViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


}
