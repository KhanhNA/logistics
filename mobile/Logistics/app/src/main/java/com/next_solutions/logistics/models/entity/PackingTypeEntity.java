package com.next_solutions.logistics.models.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "packing_type")
public class PackingTypeEntity {

    @PrimaryKey
    @ColumnInfo(name="id")
    private Long id;

    @ColumnInfo(name = "code")
    private String code;

    @ColumnInfo(name = "quantity")
    private Integer quantity;

    @ColumnInfo(name = "status")
    private Boolean status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
