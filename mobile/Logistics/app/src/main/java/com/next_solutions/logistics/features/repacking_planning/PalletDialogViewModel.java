package com.next_solutions.logistics.features.repacking_planning;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.Pageable;
import com.next_solutions.logistics.component.search.SearchComponent;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.features.repacking_planning.repacked_statement.RepackedRepackingPlanningViewModel;
import com.next_solutions.logistics.models.Pallet;
import com.next_solutions.logistics.repository.Service;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

public class PalletDialogViewModel extends BaseViewModel {
    private SearchComponent searchComponent = new SearchComponent();
    private RepackedRepackingPlanningViewModel repackViewModel;

    public PalletDialogViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void init() {
        search();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void search() {
        callApi(Service.callPalletService()
                .findPallet(
                        getCurrentPageNumber(),
                        AppUtils.PAGE_SIZE,
                        Pallet.Status.ACTIVE.getValue(),
                        Pallet.PalletStep.IMPORT_STATEMENT_UPDATE_PALLET.getValue(),
                        repackViewModel.getRepackingPlanning().getStore().getId(),
                        searchComponent.getCodeSearch())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<Pageable<Pallet>>(appException, Pageable.class, Pallet.class, null) {
                    @Override
                    public void getDataSuccess(Pageable<Pallet> pageable) {
                        getPalletSuccess(pageable);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    private void getPalletSuccess(Pageable<Pallet> pageable) {
        setPage(pageable);
        setData(pageable.getContent());
    }

    void setRepackViewModel(RepackedRepackingPlanningViewModel repackViewModel) {
        this.repackViewModel = repackViewModel;
    }

    public SearchComponent getSearchComponent() {
        return searchComponent;
    }
}
