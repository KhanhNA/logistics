package com.next_solutions.logistics.features.import_po.import_po_new

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.import_po.BaseImportPoFragment
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.features.import_po.claim_import_po.ClaimImportPoFragment
import com.next_solutions.logistics.utils.Constants.*

class ImportPoNewFragment : BaseImportPoFragment() {

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.createClaim) {
            val importPoViewModel = viewModel as ImportPoNewViewModel
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(MODEL, importPoViewModel.importPo)
            bundle.putSerializable(FRAGMENT, ClaimImportPoFragment::class.java)
            intent.putExtras(bundle)
            startActivityForResult(intent, CLAIM_ACTIVITY_CODE)
        } else if (view.id == R.id.reviewClaim) {
            val importPoViewModel = viewModel as ImportPoNewViewModel
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            val claim = importPoViewModel.claimDetails?.get(0)?.claim!!
            claim.claimDetails = importPoViewModel.claimDetails
            bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
            bundle.putSerializable(MODEL, claim)
            bundle.putSerializable(MODEL_IMPORT_PO, importPoViewModel.importPo)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CLAIM_ACTIVITY_CODE && resultCode == Activity.RESULT_OK && data!!.hasExtra(CLAIM_ACTIVITY)) {
            val result = data.getStringExtra(CLAIM_ACTIVITY)!!
            if (result == CREATE_CLAIM_SUCCESS) {
                baseActivity.onBackPressed()
            }
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ImportPoNewViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_import_po_new
    }

    override fun getRecycleResId(): Int {
        return R.id.details
    }

}