package com.next_solutions.logistics.features.claim.dialog_reject_claim

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.BaseDialogFragment
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.BaseViewModelFactory
import com.next_solutions.logistics.R
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailViewModel
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.ValidateUtils

class DialogRejectClaim(var message: String) : BaseDialogFragment() {
    private lateinit var dialogViewModel: DialogRejectClaimViewModel
    private lateinit var detailViewModel: ClaimDetailViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        prepareViewModel()
        setMessage()
        return binding.root
    }

    private fun setMessage() {
        dialogViewModel.message = message
    }

    private fun prepareViewModel() {
        dialogViewModel = viewModel as DialogRejectClaimViewModel
        detailViewModel = ViewModelProvider(baseActivity,
                BaseViewModelFactory.getInstance<Any>(baseActivity.application, ClaimDetailViewModel::class.java))
                .get(ClaimDetailViewModel::class.java)
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (view!!.id == R.id.cancel_button) {
            dismiss()
        } else {
            dismiss()
            if (!ValidateUtils.isNullOrEmpty(dialogViewModel.reason)) {
                detailViewModel.rejectClaim(dialogViewModel.reason)
            } else {
                showAlertWarning(Constants.TIME_AFTER_DELAY, R.string.missing_reason, null)
            }
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return DialogRejectClaimViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_reject_claim
    }

    override fun getRecycleResId(): Int {
        return 0
    }

}