package com.next_solutions.logistics.features.claim.claim_detail

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.ERROR_PROCESS
import com.next_solutions.logistics.utils.Constants.SUCCESS

class ClaimDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var claim: Claim
    var buttonEnable = ObservableField(true)
    var fromDetailFragment = ObservableField(true)
    var claimImages = arrayListOf<ClaimImage>()
    var po: Po? = null

    var importPo: ImportPo? = null

    var repackingPlanning: RepackingPlanning? = null

    var exportStatement: ExportStatement? = null

    var importStatement: ImportStatement? = null

    override fun init() {
        val claimDetails = claim.claimDetails
        if (claimDetails != null) {
            getClaimDetail()
        } else {
            if ((claim.type != Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_1)
                            .and(claim.type != Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_2)) {
                fromDetailFragment.set(false)
            }
            getReferenceStatement()
            getClaimDetail()
        }
    }

    private fun getClaimDetail() {
        callApi(Service.callClaimService().findClaimById(claim.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Claim>(appException, Claim::class.java, null) {
                    override fun getDataSuccess(claim: Claim) {
                        getClaimDetailSuccess(claim)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone?.message + " " + this.javaClass.name)
                    }
                }))
    }

    private fun getClaimDetailSuccess(claim: Claim) {
        this.claim = claim
        setData(claim.claimDetails as List<BaseModel>?)
        setImageClaims(claim)
    }

    private fun setImageClaims(claim: Claim) {
        claimImages.clear()
        if (claim.claimImages != null) {
            claimImages.addAll(claim.claimImages)
            sendBackAction(Constants.RELOAD_IMAGE)
        }
    }

    private fun getReferenceStatement() {
        when (claim.type) {
            Claim.ClaimType.IMPORT_PO -> {
                getImportPo()
            }
            Claim.ClaimType.IMPORT_PO_UPDATE_PALLET -> {
                getImportPoDetail()
            }
            Claim.ClaimType.REPACKING_PLANNING -> {
                getRepackingPlanningDetail()
            }
            Claim.ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING -> {
                getRepackingPlanningDetail()
            }
            Claim.ClaimType.EXPORT_STATEMENT -> {
                getExportStatementDetail()
            }
            Claim.ClaimType.DELIVERY -> {
                getDeliveryDetail()
            }
            Claim.ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT -> {
                getImportStatementDetail()
            }
            else -> {
            }
        }
    }

    private fun getImportPo() {
        callApi(Service
                .callPoService()
                .findPoById(claim.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Po>(appException, Po::class.java, null) {
                    override fun getDataSuccess(po: Po) {
                        getImportPoSuccess(po)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone?.message + " " + this.javaClass.name)
                    }

                }))
    }

    private fun getImportPoSuccess(po: Po) {
        this.po = po
    }

    private fun getImportPoDetail() {
        callApi(Service
                .callImportPoService()
                .findById(claim.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportPo>(appException, ImportPo::class.java, null) {

                    override fun getDataSuccess(importPo: ImportPo) {
                        getImportPoDetailSuccess(importPo)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }


                }))
    }

    private fun getImportPoDetailSuccess(importPo: ImportPo) {
        this.importPo = importPo
    }

    private fun getRepackingPlanningDetail() {
        callApi(Service
                .callRepackingPlanningService()
                .findRepackingPlanningById(claim.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<RepackingPlanning>(appException, RepackingPlanning::class.java, null) {
                    override fun getDataSuccess(repackingPlanning: RepackingPlanning) {
                        getRepackingPlanningDetailSuccess(repackingPlanning)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))
    }

    private fun getRepackingPlanningDetailSuccess(repackingPlanning: RepackingPlanning) {
        this.repackingPlanning = repackingPlanning
    }

    private fun getExportStatementDetail() {
        callApi(Service
                .callExportStatementService()
                .findExportStatementById(claim.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ExportStatement>(appException, ExportStatement::class.java, null) {
                    override fun getDataSuccess(exportStatement: ExportStatement) {
                        getExportStatementDetailSuccess(exportStatement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))
    }

    private fun getExportStatementDetailSuccess(exportStatement: ExportStatement) {
        this.exportStatement = exportStatement
    }

    private fun getDeliveryDetail() {
        callApi(Service
                .callImportStatementService()
                .findImportStatementById(claim.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportStatement>(appException, ImportStatement::class.java, null) {
                    override fun getDataSuccess(importStatement: ImportStatement) {
                        getDeliveryStatementDetailSuccess(importStatement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this::class.java.name)
                    }
                }))
    }

    private fun getDeliveryStatementDetailSuccess(importStatement: ImportStatement) {
        this.importStatement = importStatement
    }

    private fun getImportStatementDetail() {
        callApi(Service.callImportStatementService()
                .findImportStatementById(this.claim.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportStatement>(appException, ImportStatement::class.java, null) {
                    override fun getDataSuccess(statement: ImportStatement?) {
                        getImportStatementSuccess(statement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone?.message + " " + this.javaClass.name)
                    }
                }))
    }

    private fun getImportStatementSuccess(statement: ImportStatement?) {
        this.importStatement = statement
    }

    fun acceptClaim() {
        buttonEnable.set(false)
        callApi(Service.callClaimService().acceptClaim(claim.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(t: ResponseStatus?) {
                        sendBackAction(SUCCESS)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        buttonEnable.set(true)
                        Log.e(ERROR_PROCESS, errorRespone.message)
                    }
                }))
    }

    fun rejectClaim(reason: String) {
        buttonEnable.set(false)
        callApi(Service.callClaimService().rejectClaim(claim.id, Claim().apply { rejectReason = reason })
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(t: ResponseStatus) {
                        sendBackAction("REJECT_SUCCESS")
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        buttonEnable.set(true)
                        Log.e(ERROR_PROCESS, errorRespone.message)
                    }
                }))
    }

    fun canAccept(): Boolean {
        return claim.status == Claim.ClaimStatus.WAIT_APPROVE.value && RuleHelper().hasRule(HttpRequest(Method.PATCH.value, ClaimUrlPath.ACCEPT_CLAIM))
    }

    fun canReject(): Boolean {
        return claim.status == Claim.ClaimStatus.WAIT_APPROVE.value && RuleHelper().hasRule(HttpRequest(Method.PATCH.value, ClaimUrlPath.REJECT_CLAIM))
    }

    fun claimHasRejected(): Boolean {
        return claim.status == Claim.ClaimStatus.REJECTED.value
    }
}