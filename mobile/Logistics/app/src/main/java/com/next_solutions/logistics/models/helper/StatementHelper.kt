package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.models.ExportStatementDetail

class StatementHelper(private val exportStatement: ExportStatement) {

    fun groupDetailByExpireDateAndProductPacking(): List<ExportStatementDetail> {
        val result = arrayListOf<ExportStatementDetail>()
        val checked = HashMap<String, ExportStatementDetail>()

        exportStatement.exportStatementDetails.forEach { detail ->
            run {
                val key = detail.productPacking.id.toString() + detail.expireDate
                if (checked[key] != null) {
                    checked[key]!!.quantity += detail.quantity
                } else {
                    val exportDetail = ExportStatementDetail().apply {
                        productPacking = detail.productPacking
                        quantity = detail.quantity
                        expireDate = detail.expireDate
                    }
                    result.add(exportDetail)
                    checked[key] = exportDetail
                }
            }
        }
        return result
    }


}