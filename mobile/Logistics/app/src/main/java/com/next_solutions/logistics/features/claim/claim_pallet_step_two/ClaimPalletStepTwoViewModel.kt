package com.next_solutions.logistics.features.claim.claim_pallet_step_two

import android.app.Application
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.component.store.FromStoreComponent
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.PalletDetail
import com.next_solutions.logistics.models.Store
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE_ITEM_ADD
import java.util.*

@Suppress("UNCHECKED_CAST")
class ClaimPalletStepTwoViewModel(application: Application) : BaseClaimViewModel(application) {
    val storeComponent = FromStoreComponent(this)
//    val distributorComponent = DistributorComponent(this, false)

    override fun init() {
        super.init()
        resetData()
    }

    private fun resetData() {
        storeComponent.resetData()
//        distributorComponent.resetData()
        descriptionObservable.set(Constants.EMPTY_STRING)
        amenableObservable.set(Constants.EMPTY_STRING)
        buttonEnable.set(true)
        images.clear()
    }

    override fun doWhenChooseData(obj: Any?) {
        clearDataRecycleView()
    }

    override fun prepareClaim(): Claim {
        val details = baseModelsE as List<ClaimDetail>
        return Claim().apply {
            claimDetails = details
            type = Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_2
            description = descriptionObservable.get()
            claimDetails = details
            store = Store().apply {
                code = storeComponent.getSelectedStore()!!.code
                id = storeComponent.getSelectedStore()!!.id
            }
            status = Claim.ClaimStatus.WAIT_APPROVE.value
            amenable = amenableObservable.get()
        }
    }

    fun addDetail(palletDetail: PalletDetail) {
        if(baseModelsE == null){
            setBaseModelsE(ArrayList<ClaimDetail>())
        }
        val claimDetails = baseModelsE as ArrayList<ClaimDetail>
        notifyPosition.set(claimDetails.size)
        claimDetails.add(claimHelper.convertPalletDetailToClaimDetail(palletDetail))
        sendBackAction(NOTIFY_CHANGE_ITEM_ADD)
    }
}