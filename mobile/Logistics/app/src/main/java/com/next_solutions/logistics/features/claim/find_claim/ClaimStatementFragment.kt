package com.next_solutions.logistics.features.claim.find_claim

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.features.claim.claim_pallet_step_one.ClaimPalletStepOneFragment
import com.next_solutions.logistics.features.claim.update_claim.UpdateClaimFragment
import com.next_solutions.logistics.features.claim.update_claim.po.UpdateClaimPoFragment
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity
import com.next_solutions.logistics.widget.SwipeHelper
import com.next_solutions.logistics.widget.SwipeHelper.UnderlayButtonClickListener
import java.util.*
import java.util.Calendar.*

class ClaimStatementFragment : BaseRecycleViewFragment(), DatePickerDialog.OnDateSetListener {
    private lateinit var claimStatementViewModel: ClaimStatementViewModel
    private var isFromDate: Boolean = false
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimStatementViewModel = viewModel as ClaimStatementViewModel
        claimStatementViewModel.init()
        enableSwipeToUpdate()
        return binding.root
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_search_claim_item
    }

    override fun getListenerAdapter(): OwnerView {
        return OwnerView { _, claim ->
            run {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(MODEL, claim)
                bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }

    private fun enableSwipeToUpdate() {
        val context = context
        if (context != null) {
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            val width = size.x
            val swipeHelper: SwipeHelper = object : SwipeHelper(getContext(), recyclerView, width) {
                override fun instantiateUnderlayButton(viewHolder: RecyclerView.ViewHolder, underlayButtons: MutableList<UnderlayButton>) {
                    if (RuleHelper().hasRule(HttpRequest(Method.PATCH.value, ClaimUrlPath.UPDATE_CLAIM))) {
                        underlayButtons.add(UnderlayButton(
                                getString(R.string.update),
                                UnderlayButtonClickListener { position: Int ->
                                    val intent = Intent(getContext(), CommonActivity::class.java)
                                    val bundle = Bundle()
                                    val claim = claimStatementViewModel.baseModelsE[position]
                                    if (claim is Claim && claim.status != Claim.ClaimStatus.APPROVED.value) {
                                        bundle.putSerializable(MODEL, claimStatementViewModel.baseModelsE[position])
//                                        if (claimStatementViewModel.claimTypesComponent.getSelectedType() == Claim.ClaimType.IMPORT_PO.value) {
                                        if (claim.type.value == Claim.ClaimType.IMPORT_PO.value) {
                                            intent.putExtra(FRAGMENT, UpdateClaimPoFragment::class.java)
                                        }
                                        else {
                                            intent.putExtra(FRAGMENT, UpdateClaimFragment::class.java)
                                        }
                                        intent.putExtras(bundle)
                                        startActivity(intent)
                                    } else {
                                        showAlertWarning(R.string.claim_approved)
                                    }
                                },
                                ContextCompat.getColor(context, R.color.DeepSkyBlue)
                        ))
                    }
                }
            }
            val itemTouchHelper = ItemTouchHelper(swipeHelper)
            itemTouchHelper.attachToRecyclerView(recyclerView)
        }
    }

    private fun showDialogDatePicker(minDate: Long?, maxDate: Long?) {
        val scene = context
        if (scene != null) {
            val calendar = getInstance()
            val year: Int
            val month: Int
            val day: Int
            if (isFromDate) {
                val fromDate: Date = claimStatementViewModel.chooseDate.fromDate.get()!!
                calendar.time = fromDate
                year = calendar[YEAR]
                month = calendar[MONTH]
                day = calendar[DAY_OF_MONTH]
            } else {
                val fromDate: Date = claimStatementViewModel.chooseDate.toDate.get()!!
                calendar.time = fromDate
                year = calendar[YEAR]
                month = calendar[MONTH]
                day = calendar[DAY_OF_MONTH]
            }
            val datePickerDialog = DatePickerDialog(scene, this, year, month, day)
            if (maxDate != null) {
                datePickerDialog.datePicker.maxDate = maxDate
            }
            if (minDate != null) {
                datePickerDialog.datePicker.minDate = minDate
            }
            datePickerDialog.show()
        }
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        val tag = view!!.tag
        if (FROM_DATE == tag) {
            isFromDate = true
            val toDate = claimStatementViewModel.chooseDate.toDate.get()
            showDialogDatePicker(null, toDate!!.time)
        } else if (TO_DATE == tag) {
            isFromDate = false
            val fromDate = claimStatementViewModel.chooseDate.fromDate.get()
            showDialogDatePicker(fromDate!!.time, System.currentTimeMillis() - 1000)
            if (SCAN_CODE == tag) {
                val qrScan = Intent(context, QRCodeScannerActivity::class.java)
                startActivityForResult(qrScan, REQUEST_CODE_SCAN)
            }
        } else if (SCAN_CODE == view.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_SCAN) {
            claimStatementViewModel.searchByCode(data!!.getStringExtra(RESULT_SCAN))
        }
    }

    override fun onResume() {
        super.onResume()
        claimStatementViewModel.onClickButtonSearch()
    }


    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimStatementViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_search_claim
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        claimStatementViewModel.setDate(isFromDate, year, month, dayOfMonth)
    }
}