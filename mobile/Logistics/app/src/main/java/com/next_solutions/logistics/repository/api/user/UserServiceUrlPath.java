package com.next_solutions.logistics.repository.api.user;

public interface UserServiceUrlPath {
    String USER_ME = "/user/me";
    String CHANGE_PASSWORD = "/user/change-password";
}
