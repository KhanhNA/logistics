package com.next_solutions.logistics.config;

import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public interface RxObservableSchedulers {
    RxObservableSchedulers DEFAULT = new RxObservableSchedulers() {
        @Override
        public <T> ObservableTransformer<T, T> applySchedulers() {
            return upstream -> upstream
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    };

    RxObservableSchedulers TEST_SCHEDULER = new RxObservableSchedulers() {
        @Override
        public <T> ObservableTransformer<T, T> applySchedulers() {
            return upstream -> upstream
                    .subscribeOn(Schedulers.trampoline())
                    .observeOn(Schedulers.trampoline());
        }
    };

    <T> ObservableTransformer<T, T> applySchedulers();
}
