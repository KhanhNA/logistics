package com.next_solutions.logistics.features.import_statement.import_statement_release.to_fc

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.logistics.base.import_release.BaseImportReleaseViewModel
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.models.ImportStatementDetailPallet
import com.next_solutions.logistics.models.Pallet
import com.next_solutions.logistics.models.Store
import com.next_solutions.logistics.models.exception.InvalidDetailException
import com.next_solutions.logistics.models.exception.NotMatchQuantityException
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE
import com.next_solutions.logistics.utils.Constants.NOT_MATCH_QUANTITY
import com.next_solutions.logistics.utils.ValidateUtils

class ImportStatementFCViewModel(application: Application) : BaseImportReleaseViewModel(application) {
    var currentTotalQuantity = ObservableField<HashMap<Long, Long>>(HashMap())
    lateinit var storeFC: Store
    var listData = ArrayList<ImportStatementDetail>()

    override fun init() {
        super.init()
        storeFC = importStatement!!.toStore
    }

    override fun prepareStatementDetail(): List<ImportStatementDetailPallet> {
        val importPalletDetail = arrayListOf<ImportStatementDetailPallet>()
        listData.forEach { detail ->
            run {
                importPalletDetail.addAll(groupPallet(detail))
            }
        }
        return importPalletDetail
    }

    private fun groupPallet(importStatementDetail: ImportStatementDetail): List<ImportStatementDetailPallet> {
        val hashMap = HashMap<String, ImportStatementDetailPallet>()
        importStatementDetail.importStatementDetailPallets.forEach { detailPallet ->
            run {
                val key = detailPallet.pallet.code
                if (hashMap.containsKey(key)) {
                    hashMap[key]!!.quantity += detailPallet.quantity
                } else {
                    hashMap[key] = detailPallet
                }
            }
        }
        return hashMap.values.toList()
    }

    override fun checkValidate(): Boolean {
        for ((index, detail) in listData.withIndex()) {
            try {
                isValidDetail(detail)
            } catch (notMatchQuantityException: NotMatchQuantityException) {
                lastErrorPosition = errorPosition
                errorPosition = index
                sendBackAction(NOT_MATCH_QUANTITY)
                return false
            } catch (invalidDetailException: InvalidDetailException) {
                lastErrorPosition = errorPosition
                errorPosition = index
                sendBackAction("DETAIL_MISSING_PALLET_OR_QUANTITY")
                return false
            }
        }
        return true
    }

    override fun checkErrorPositionWhenClickCheckBox(position: Int) {
        val detail = listData[position]
        try {
            isValidDetail(detail)
            errorPosition = -1
        } catch (notMatchQuantityException: NotMatchQuantityException) {
        } catch (invalidDetailException: InvalidDetailException) {
        }
    }

    override fun isValidDetail(importStatementDetail: ImportStatementDetail) {
        val value = currentTotalQuantity.get()!![importStatementDetail.id]!!
        if ((importStatementDetail.quantity != value
                        || (importStatementDetail.checked == null || !importStatementDetail.checked))) {
            throw NotMatchQuantityException()
        }
        val missingDetail = importStatementDetail.importStatementDetailPallets.find {
            ValidateUtils.isNullOrEmpty(it.pallet.id) || (
                    (ValidateUtils.isNullOrEmpty(it.quantity) && value != 0L))
        }
        if (missingDetail != null) {
            throw InvalidDetailException()
        }
    }

    override fun getBaseModelsE(): List<BaseModel> {
        return listData
    }

    override fun setDataForRecycleView(data: List<ImportStatementDetail>) {
        data.forEach { detail ->
            run {
                detail.importStatementDetailPallets = arrayListOf(
                        ImportStatementDetailPallet().apply {
                            importStatementDetailId = detail.id
                            importStatementId = importStatement!!.id
                            productPackingId = detail.productPacking.id
                            expireDate = detail.expireDate
                            quantity = 0
                            pallet = Pallet()
                        })
                currentTotalQuantity.get()?.put(detail.id, 0L)
            }
        }
        listData.addAll(data)
        sendBackAction(NOTIFY_CHANGE)
    }

    fun onQuantityChanged(s: CharSequence, importStatementDetail: ImportStatementDetailPallet) {
        try {
            val newQuantity = s.toString().toLong()
            if (importStatementDetail.quantity != newQuantity) {
                val addValue = newQuantity - importStatementDetail.quantity
                reCalculator(addValue, importStatementDetail)
                importStatementDetail.quantity = newQuantity
            }
        } catch (e: Exception) {
            reCalculator(importStatementDetail.quantity * -1, importStatementDetail)
            if (s.toString() == Constants.EMPTY_STRING) {
                importStatementDetail.quantity = 0
            }
            Log.e(Constants.ERROR_PROCESS, e.toString())
        }
    }

    fun reCalculator(addValue: Long, importPalletDetail: ImportStatementDetailPallet) {
        val total = currentTotalQuantity.get()
        val code = importPalletDetail.importStatementDetailId
        total?.let {
            val newValue = it[code]!!.plus(addValue)
            it[code] = newValue
        }
        currentTotalQuantity.notifyChange()
    }
}