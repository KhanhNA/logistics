package com.next_solutions.logistics.features.claim.claim_pallet_step_one

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimFragment

class ClaimPalletStepOneFragment : BaseClaimFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val claimViewModel = viewModel as ClaimPalletStepOneViewModel
        claimViewModel.init()
        claimImageAdapter.notifyDataSetChanged()
        return binding.root
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val dialog = DialogChoosePalletStepOneFragment()
            dialog.show((activity as FragmentActivity).supportFragmentManager, "")
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_claim_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimPalletStepOneViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_pallet_step_one
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}