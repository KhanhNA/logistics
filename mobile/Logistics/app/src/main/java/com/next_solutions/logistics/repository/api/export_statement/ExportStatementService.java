package com.next_solutions.logistics.repository.api.export_statement;

import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.models.dto.ImportStatementDTO;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.ACTUALLY_EXPORT;
import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.CANCEL_EXPORT_STATEMENT;
import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.CANCEL_EXPORT_STATEMENT_DETAIL;
import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.CREATE_EXPORT_STATEMENT;
import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.FIND_EXPORT_STATEMENT;
import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.FIND_EXPORT_STATEMENT_BY_CODE;
import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.FIND_EXPORT_STATEMENT_BY_ID;
import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.UPDATE_EXPORT_STATEMENT;

public interface ExportStatementService {
    //    ========================EXPORT STATEMENT========================
    @POST(CREATE_EXPORT_STATEMENT)
    Single<Response<ResponseBody>> createExportStatement(@Body List<ExportStatement> exportStatementList);

    @GET("/export-statements/get-by-code")
    Call<ResponseBody> getExportStatement(@Query("code") String code);

    @GET(FIND_EXPORT_STATEMENT_BY_ID)
    Single<Response<ResponseBody>> findExportStatementById(@Path("id") Long id);

    @GET("/export-statements/from-merchant-order")
    Call<ResponseBody> createStatementFromMerChantOrder(@Query("code") String code);

    @GET(FIND_EXPORT_STATEMENT)
    Single<Response<ResponseBody>> findExportStatement(@Query("code") String code,
                                                       @Query("createDate") String createDate,
                                                       @Query("pageNumber") Integer pageNumber,
                                                       @Query("pageSize") Integer pageSize,
                                                       @Query("status") Integer status,
                                                       @Query("toStoreId") String toStoreId,
                                                       @Query("type") String type);

    @GET(FIND_EXPORT_STATEMENT_BY_CODE)
    Single<Response<ResponseBody>> findExportStatementByCode(@Query("code") String code);

    @PATCH(ACTUALLY_EXPORT)
    Single<Response<ResponseBody>> actuallyExport(@Path("id") Long id, @Body ImportStatementDTO importStatementDTO);


    @POST(CANCEL_EXPORT_STATEMENT)
    Single<Response<ResponseBody>> cancelExportStatement(@Path("id") Long id);


    @POST(CANCEL_EXPORT_STATEMENT_DETAIL)
    Single<Response<ResponseBody>> cancelExportStatementDetail(@Path("id") Long id, @Body List<Long> detailIds);


    @PATCH(UPDATE_EXPORT_STATEMENT)
    Single<Response<ResponseBody>> updateExportStatement(@Path("id") Long id, @Body ExportStatement exportStatement);
}
