package com.next_solutions.logistics.models.mapper;

import com.next_solutions.logistics.models.PackingType;
import com.next_solutions.logistics.models.entity.PackingTypeEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PackingTypeMapper {
    PackingTypeMapper INSTANCE = Mappers.getMapper( PackingTypeMapper.class );

    //    @Mapping(target = "statementStatus", ignore = true)
//    @Mapping(target = "checked", ignore = true)
    PackingTypeEntity packingTypeDtoToPackingTypeEntity(PackingType packingType);
}
