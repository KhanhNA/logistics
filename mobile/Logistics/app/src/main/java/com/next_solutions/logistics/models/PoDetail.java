package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class PoDetail extends Base {

    private Po po;

    private Product product;

    private ProductPacking productPacking;

    private ProductPackingPrice productPackingPrice;

    private Long quantity;

    private Integer vat;

    public Long originalQuantity;
    private Long claimQuantity = 0L;


    public BigDecimal getTotal() {
        return new BigDecimal(quantity).multiply(productPackingPrice.getPrice().multiply(new BigDecimal((100 + vat) / 100.0)));
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Po getPo() {
        return po;
    }

    public void setPo(Po po) {
        this.po = po;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductPackingPrice getProductPackingPrice() {
        return productPackingPrice;
    }

    public void setProductPackingPrice(ProductPackingPrice productPackingPrice) {
        this.productPackingPrice = productPackingPrice;
    }

    public Integer getVat() {
        return vat;
    }

    public void setVat(Integer vat) {
        this.vat = vat;
    }

    public Long getClaimQuantity() {
        return claimQuantity;
    }

    public void setClaimQuantity(Long claimQuantity) {
        this.claimQuantity = claimQuantity;
    }


}