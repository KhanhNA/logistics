package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Inventory extends Base {
    private Store store;
    private String expireDate;
    private Long totalQuantity;
    private ProductPacking productPacking;
    private Product product;
    private String className;
    private Long productPackingId;

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Long getProductPackingId() {
        return productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }
}