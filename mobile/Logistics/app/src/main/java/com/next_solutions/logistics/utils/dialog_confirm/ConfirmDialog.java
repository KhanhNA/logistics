package com.next_solutions.logistics.utils.dialog_confirm;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.next_solutions.base.BaseDialogFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.utils.DialogCallBack;

public class ConfirmDialog extends BaseDialogFragment {

    private DialogCallBack dialogCallBack;
    private Object data;
    private String message;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ConfirmDialogViewModel confirmDialogViewModel = (ConfirmDialogViewModel) viewModel;
        confirmDialogViewModel.init(message);
        return binding.getRoot();
    }

    public void init(DialogCallBack dialogCallBack, String message, Object data) {
        this.dialogCallBack = dialogCallBack;
        this.message = message;
        this.data = data;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.confirm_button) {
            dialogCallBack.callBack(data);
        } else if (view.getId() == R.id.cancel_button) {
            dialogCallBack.onCancel();
        }
        dismiss();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_confirm;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ConfirmDialogViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
