package com.next_solutions.logistics.repository.api.config

class ConfigUrlPath {
    companion object {
        const val CONFIGS = "/configs"
    }
}