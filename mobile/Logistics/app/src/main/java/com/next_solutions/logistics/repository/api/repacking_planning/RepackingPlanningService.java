package com.next_solutions.logistics.repository.api.repacking_planning;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.next_solutions.logistics.repository.api.repacking_planning.RepackingPlanningUrlPath.DETAIL_REPACKED;
import static com.next_solutions.logistics.repository.api.repacking_planning.RepackingPlanningUrlPath.FIND_REPACKING_PLANNING;
import static com.next_solutions.logistics.repository.api.repacking_planning.RepackingPlanningUrlPath.FIND_REPACKING_PLANNING_BY_ID;

public interface RepackingPlanningService {
    //    ========================REPACKING PLANNING========================

    @GET(FIND_REPACKING_PLANNING)
    Single<Response<ResponseBody>> findRepackingPlanning(@Query("text") String text,
                                                         @Query("dateType") Integer dateType,
                                                         @Query("fromDate") String fromDate,
                                                         @Query("toDate") String toDate,
                                                         @Query("pageNumber") Integer pageNumber,
                                                         @Query("pageSize") Integer pageSize,
                                                         @Query("storeId") Long storeId,
                                                         @Query("status") Integer status,
                                                         @Query("distributorId") Long distributorId);

    @GET(FIND_REPACKING_PLANNING_BY_ID)
    Single<Response<ResponseBody>> findRepackingPlanningById(@Path("id") Long id);

    @GET(DETAIL_REPACKED)
    Single<Response<ResponseBody>> findDetailRepacked(@Path("id") Long id);
}
