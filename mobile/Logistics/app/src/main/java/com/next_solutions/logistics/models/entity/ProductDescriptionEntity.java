package com.next_solutions.logistics.models.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.next_solutions.logistics.utils.Constants;


@Entity(tableName = "product_description", foreignKeys = {
        @ForeignKey(entity = ProductEntity.class,
                parentColumns = "id",
                childColumns = "product_id")})
public class ProductDescriptionEntity {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;

    @ColumnInfo(name = "product_id")
    private Long productId;

    @ColumnInfo(name = "code")
    private String code;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "lang_id")
    private Long langId;

    @ColumnInfo(name = "packing_quantity")
    private Long packingQuantity;

    @ColumnInfo(name = "quantity")
    private Long quantity;

    @ColumnInfo(name = "packing_id")
    private Long packingId;

    public String getPackingQuantityStr() {
        return packingQuantity == null ? Constants.EMPTY_STRING : packingQuantity.toString();
    }

    public String getQuantityStr() {
        return quantity == null ? Constants.EMPTY_STRING : quantity.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLangId() {
        return langId;
    }

    public void setLangId(Long langId) {
        this.langId = langId;
    }

    public Long getPackingQuantity() {
        return packingQuantity;
    }

    public void setPackingQuantity(Long packingQuantity) {
        this.packingQuantity = packingQuantity;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPackingId() {
        return packingId;
    }

    public void setPackingId(Long packingId) {
        this.packingId = packingId;
    }
}
