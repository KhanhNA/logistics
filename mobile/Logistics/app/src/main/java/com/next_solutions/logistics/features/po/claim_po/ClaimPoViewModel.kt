package com.next_solutions.logistics.features.po.claim_po

import android.app.Application
import com.next_solutions.logistics.base.claim.po.BaseClaimPoViewModel
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.ClaimObject
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE_ITEM

@Suppress("UNCHECKED_CAST")
class ClaimPoViewModel(application: Application) : BaseClaimPoViewModel(application) {
    override fun init() {
        po.poDetails.forEach { detail: PoDetail ->
            run {
                listData.add(claimHelper.convertPoDetailToClaimObject(detail))
                currentTotalQuantity.get()!!.put(detail.productPacking.code, 0L)
            }
        }
        sendBackAction(NOTIFY_CHANGE)
    }

    override fun prepareClaim(): Claim {
        val details = arrayListOf<ClaimDetail>()
        listData.forEach { detail: ClaimObject ->
            run {
                val groupByExpiryDate = HashMap<String, ClaimDetail>()
                detail.claimDetails.forEach { claimDetail ->
                    run {
                        val key = claimDetail.expireDate
                        if (groupByExpiryDate.containsKey(key)) {
                            groupByExpiryDate[key]!!.quantity = groupByExpiryDate[key]!!.quantity + claimDetail.quantity
                        } else {
                            groupByExpiryDate[key] = claimDetail
                        }
                    }
                }
                details.addAll(groupByExpiryDate.values)
            }
        }
        return Claim().apply {
            type = Claim.ClaimType.IMPORT_PO
            description = descriptionObservable.get()
            referenceId = po.id
            claimDetails = details
            store = Store().apply {
                code = po.store.code
                id = po.store.id
            }
            status = Claim.ClaimStatus.WAIT_APPROVE.value
            amenable = amenableObservable.get()
        }
    }

    fun addPoDetail(poDetail: PoDetail) {
        notifyPosition.set(listData.size)
        listData.add(ClaimObject().apply {
            claimDetails = arrayListOf(ClaimDetail().apply {
                quantity = 0
                productPacking = ProductPacking().apply {
                    code = poDetail.productPacking.code
                    id = poDetail.productPacking.id
                }
            })
            productPackingCode = poDetail.productPacking.code
            productPackingName = poDetail.productPacking.product.name
            productPackingUom = poDetail.productPacking.uom
            productPackingType = poDetail.productPacking.packingType.quantity
            quantity = poDetail.quantity
        })
        currentTotalQuantity.get()?.set(poDetail.productPacking.code, 0L)
        sendBackAction(NOTIFY_CHANGE_ITEM)
    }

}