package com.next_solutions.logistics.features.po.claim_po

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.Po

@Suppress("UNCHECKED_CAST")
class ChoosePoDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var po: Po
    val searchComponent = SearchComponent()
    private lateinit var claimPoViewModel: ClaimPoViewModel
    fun init(claimPoViewModel: ClaimPoViewModel) {
        this.claimPoViewModel = claimPoViewModel
        this.po = claimPoViewModel.po
        searchComponent.resetData()
        search()
    }

    override fun search() {
        val entries = claimPoViewModel.currentTotalQuantity.get()!!.keys.toList()
        val codeSearch = searchComponent.code.get().toString()
        setData(claimPoViewModel.po.poDetails
                .asSequence()
                .filter {
                    !entries.contains(it.productPacking.code)
                }
                .filter {
                    it.productPacking.code.contains(codeSearch, true)
                            || (it.productPacking.product.name.contains(codeSearch, true))
                }
                .toList())
    }

}