package com.next_solutions.logistics.utils.app_model;


import androidx.fragment.app.Fragment;


public class MenuTitle implements Comparable<MenuTitle> {
    private String title;
    private Fragment fragment;
    private String tag;
    private Integer icon;

    public MenuTitle(String title, Integer icon, Fragment fragment, String tag) {
        this.title = title;
        this.icon = icon;
        this.fragment = fragment;
        this.tag = tag;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof MenuTitle) {
            MenuTitle menuTitle = (MenuTitle) obj;
            return menuTitle.getTitle().equals(this.getTitle());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public int compareTo(MenuTitle o) {
        if (o == null) {
            return -1;
        }
        return this.getTitle().compareTo(o.getTitle());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }
}
