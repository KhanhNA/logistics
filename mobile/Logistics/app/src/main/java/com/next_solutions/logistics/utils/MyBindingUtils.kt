package com.next_solutions.logistics.utils

//import com.bumptech.glide.Glide
//import com.bumptech.glide.load.model.GlideUrl
//import com.bumptech.glide.load.model.LazyHeaders

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders

import com.next_solutions.logistics.repository.HttpHelper


object MyBindingUtils {

    @JvmStatic
    @BindingAdapter("imgWithAuthen")
    fun loadImageHomeBanner(view: ImageView?, imageUrl: String?) {
        if (view != null && imageUrl != null) {
            val glideUrl = GlideUrl(imageUrl,
                    LazyHeaders.Builder()
                            .addHeader("Authorization", HttpHelper.tokenLogin)
                            .build())
            Glide.with(view.getContext())
                    .load(glideUrl)
                    .into(view)
        }
    }
}