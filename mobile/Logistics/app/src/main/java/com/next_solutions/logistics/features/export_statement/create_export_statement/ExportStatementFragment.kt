package com.next_solutions.logistics.features.export_statement.create_export_statement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.export_statement.BaseExportStatementFragment

class ExportStatementFragment : BaseExportStatementFragment() {
    private lateinit var exportStatementViewModel: ExportStatementViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View? = super.onCreateView(inflater, container, savedInstanceState)
        exportStatementViewModel = viewModel as ExportStatementViewModel
        exportStatementViewModel.init()
        return v
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_create_export_statement
    }

    override fun getVMClass(): Class<out BaseViewModel<*>?>? {
        return ExportStatementViewModel::class.java
    }

    override fun getRecycleResId(): Int {
        return R.id.deltails
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_create_export_statement_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }
}
