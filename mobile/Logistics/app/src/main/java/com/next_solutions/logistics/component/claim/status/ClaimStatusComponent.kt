package com.next_solutions.logistics.component.claim.status

class ClaimStatusComponent {
    var selectedStatusPosition = 0

    fun getSelectedClaimStatus(): Int? {
        if (selectedStatusPosition == 0) {
            return null
        }
        return selectedStatusPosition - 1
    }

    fun resetData() {
        selectedStatusPosition = 0
    }
}