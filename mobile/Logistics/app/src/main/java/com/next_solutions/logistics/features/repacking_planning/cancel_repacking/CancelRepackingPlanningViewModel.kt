package com.next_solutions.logistics.features.repacking_planning.cancel_repacking

import android.app.Application
import android.util.Log
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.models.RepackingPlanningDetail
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants

@Suppress("UNCHECKED_CAST")
class CancelRepackingPlanningViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var repackingPlanning: RepackingPlanning
    val searchComponent = SearchComponent()

    var lastNotifyPosition = -1
    override fun init() {
        callApiGetRepackingDetail()
    }

    private fun callApiGetRepackingDetail() {
        callApi(Service.callRepackingPlanningService()
                .findRepackingPlanningById(repackingPlanning.getId())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<RepackingPlanning>(appException, RepackingPlanning::class.java, null) {
                    override fun getDataSuccess(repackingPlanning: RepackingPlanning) {
                        getCancelRepackSuccess(repackingPlanning)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                    }
                }))
    }

    private fun getCancelRepackSuccess(repackingPlanning: RepackingPlanning) {
        this.repackingPlanning = repackingPlanning
        setData(repackingPlanning.repackingPlanningDetails as List<BaseModel>?)
    }

    override fun search() {
        val data = baseModelsE as List<RepackingPlanningDetail>
        val textSearch = searchComponent.code.get().toString()
        for ((position, detail) in data.withIndex()) {
            if (detail.productPacking.code.contains(textSearch, true)
                    || (detail.productPacking.product.name.contains(textSearch, true))) {
                if (notifyPosition.get() != null) {
                    lastNotifyPosition = notifyPosition.get()!!
                }
                if (lastNotifyPosition != -1) {
                    data[lastNotifyPosition].checked = null
                }

                notifyPosition.set(position)
                detail.checked = true
                sendBackAction(Constants.SCROLL_TO_POSITION)
                return
            }
        }
    }
}