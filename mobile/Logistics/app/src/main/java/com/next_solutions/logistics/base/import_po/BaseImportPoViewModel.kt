package com.next_solutions.logistics.base.import_po

import android.app.Application
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.R
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath
import com.next_solutions.logistics.repository.api.import_po.ImportPoUrlPath
import com.next_solutions.logistics.repository.api.pallet.PalletUrlPath
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE

abstract class BaseImportPoViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var importPo: ImportPo
    var claimDetails: ArrayList<ClaimDetail>? = null
    val searchComponent = SearchComponent()
    var buttonEnable = ObservableField(true)
    var currentTotalQuantity = ObservableField<HashMap<String, Long>>(HashMap())
    var listData: ArrayList<ImportPoDetail> = arrayListOf()
    var lastNotifyPosition: Int? = null
    var fromClaimFragment = ObservableField(false)
    var storePallet = arrayListOf<Pallet>()

    var buttonCreateClaim = ObservableField(false)
    var buttonViewClaim = ObservableField(false)
    var haveClaim = ObservableField(false)
    var buttonUpdatePallet = ObservableField(false)
    lateinit var store: Store

    abstract fun updateTotalQuantity()
    abstract fun avoidRecursiveData(): List<ImportPoDetail>


    override fun init() {
        if (!claimDetails.isNullOrEmpty()) {
            getClaimSuccess(claimDetails!!)
            findImportPoSuccess(importPo)
        } else {
            getClaimReference()
        }
        if (RuleHelper().hasRule(HttpRequest(Method.GET.value, PalletUrlPath.FIND_PALLET))) {
            getAllPallet()
        }

    }

    private fun getImportPoDetail() {
        callApi(Service.callImportPoService()
                .findById(importPo.getId())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportPo>(appException, ImportPo::class.java, null) {
                    override fun getDataSuccess(importPo: ImportPo) {
                        findImportPoSuccess(importPo)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                    }
                }))
    }

    private fun getAllPallet() {
        callApi(Service.callPalletService()
                .findPallet(currentPageNumber, Int.MAX_VALUE,
                        Pallet.Status.ACTIVE.value,
                        Pallet.PalletStep.IMPORT_PO_UPDATE_PALLET.value,
                        store.id, Constants.EMPTY_STRING)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Pageable<Pallet>>(appException, Pageable::class.java, Pallet::class.java, null) {
                    override fun getDataSuccess(page: Pageable<Pallet>?) {
                        storePallet.clear()
                        storePallet.addAll(page!!.content)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                    }

                }))
    }

    override fun search() {
        val textSearch = searchComponent.code.get()!!
        for ((position, detail) in this.listData.withIndex()) {
            if (detail.productPacking.product.name.contains(textSearch, true)
                    || (detail.productPacking.code.contains(textSearch, true))) {
                updateLastNotifyPosition()
                updateNotifyPosition(position, detail, true)
                sendBackAction(Constants.SEARCH)
                return
            }
        }
    }

    fun updateNotifyPosition(position: Int, detail: ImportPoDetail, status: Boolean) {
        notifyPosition.set(position)
        detail.checked = status
    }

    fun updateLastNotifyPosition() {
        lastNotifyPosition = notifyPosition.get()
        lastNotifyPosition?.let { listData[it].checked = null }
    }

    private fun findImportPoSuccess(importPo: ImportPo) {
        this.importPo = importPo
        notifyDisplay()
        updateTotalQuantity()
        val importPoDetails = avoidRecursiveData()
        listData.addAll(importPoDetails)
        sendBackAction(NOTIFY_CHANGE)
    }

    private fun notifyDisplay() {
        reCalculatorQuantity()

        if (claimDetails == null || (claimDetails!!.isNotEmpty() && claimDetails!![0].claim.status == Claim.ClaimStatus.WAIT_APPROVE.value)) {
            claimNotApproved()
        } else {
            if (claimDetails!!.isNotEmpty()) {
                claimApproved()
            } else {
                doNotHaveClaim()
            }
        }
    }

    private fun claimNotApproved() {
        buttonUpdatePallet.set(false)
        buttonCreateClaim.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
        buttonViewClaim.set(false)
        haveClaim.set(false)
        buttonEnable.set(false)
    }

    private fun claimApproved() {
        val ruleHelper = RuleHelper()
        buttonUpdatePallet.set(ruleHelper.hasRule(HttpRequest(Method.POST.value, ImportPoUrlPath.UPDATE_PALLET)))
        buttonViewClaim.set(ruleHelper.hasRule(HttpRequest(Method.GET.value, ClaimUrlPath.FIND_CLAIM_BY_ID)))
        haveClaim.set(true)
        buttonCreateClaim.set(false)
    }

    private fun doNotHaveClaim() {
        val ruleHelper = RuleHelper()
        buttonUpdatePallet.set(ruleHelper.hasRule(HttpRequest(Method.POST.value, ImportPoUrlPath.UPDATE_PALLET)))
        buttonViewClaim.set(false)
        haveClaim.set(false)
        buttonCreateClaim.set(ruleHelper.hasRule(HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun getClaimReference() {
        callApi(Service.callClaimService()
                .findClaimByReference(importPo.getId(), Claim.ClaimType.IMPORT_PO_UPDATE_PALLET.value)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, MutableList::class.java) {
                    override fun getDataSuccess(claimDetails: List<ClaimDetail>) {
                        getClaimSuccess(claimDetails)
                        getImportPoDetail()
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                        getImportPoDetail()
                    }
                }))
    }

    private fun getClaimSuccess(claimDetails: List<ClaimDetail>) {
        this.claimDetails = arrayListOf()
        this.claimDetails!!.addAll(claimDetails)
    }

    private fun reCalculatorQuantity() {
        for (importPoDetail in importPo.importPoDetails) {
            importPoDetail.claimQuantity = 0
            importPoDetail.originalQuantity = importPoDetail.quantity
        }
        if (claimDetails != null && (claimDetails!!.isNotEmpty() && claimDetails!![0].claim.status == Claim.ClaimStatus.APPROVED.value)) {
            for (importPoDetail in importPo.importPoDetails) {
                for (claimDetail in claimDetails!!) {
                    if (claimDetail.productPacking.getId() == importPoDetail.productPacking.getId() &&
                            claimDetail.expireDate == importPoDetail.expireDate) {
                        importPoDetail.claimQuantity += claimDetail.quantity
                        importPoDetail.quantity -= claimDetail.quantity
                    }
                }
            }
        }
    }

    open fun getKeyMapping(detail: ImportPoDetail): String {
        return detail.productPacking.code + detail.expireDate
    }

    fun reCalculator(addValue: Long, detailPalletPallet: ImportPoDetailPallet) {
        val total = currentTotalQuantity.get()
        val code = getKeyMapping(detailPalletPallet.importPoDetail)
        total?.let {
            val newValue = it[code]!!.plus(addValue)
            it[code] = newValue
        }
        currentTotalQuantity.notifyChange()
    }

    fun onQuantityChanged(s: CharSequence, importPoDetailPallet: ImportPoDetailPallet) {
        try {
            val newQuantity = s.toString().toLong()
            if (importPoDetailPallet.quantity != newQuantity) {
                val addValue = newQuantity - importPoDetailPallet.quantity
                reCalculator(addValue, importPoDetailPallet)
                importPoDetailPallet.quantity = newQuantity
            }
        } catch (e: Exception) {
            reCalculator(importPoDetailPallet.quantity * -1, importPoDetailPallet)
            if (s.toString() == Constants.EMPTY_STRING) {
                importPoDetailPallet.quantity = 0
            }
            Log.e(Constants.ERROR_PROCESS, e.toString())
        }
    }

    fun prepareDataForApi(): List<ImportPoDetailPallet> {
        val data = arrayListOf<ImportPoDetailPallet>()
        this.listData.forEach { importPoDetail ->
            run {
                val mappingPallet = HashMap<String, ImportPoDetailPallet>()
                importPoDetail.importPoDetailPallets.forEach { detailsPallet ->
                    run {
                        val key = detailsPallet.pallet.id.toString()
                        if (mappingPallet.containsKey(key)) {
                            mappingPallet[key]!!.quantity += detailsPallet.quantity
                        } else {
                            mappingPallet[key] = detailsPallet
                        }
                    }
                }
                data.addAll(mappingPallet.values.toList())
            }
        }
        return data
    }

    fun getColor(importPoDetail: ImportPoDetail): Int {
        if (importPoDetail.checked != null) {
            return if (importPoDetail.checked) {
                ContextCompat.getColor(getApplication(), R.color.light_orange)
            } else {
                ContextCompat.getColor(getApplication(), R.color.light_red_2)
            }
        }
        return ContextCompat.getColor(getApplication(), R.color.white)
    }

    fun hideButtonViewClaim() {
        fromClaimFragment.set(false)
    }
}