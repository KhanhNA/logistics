package com.next_solutions.logistics.features.import_po.claim_import_po

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.ImportPo
import com.next_solutions.logistics.models.ImportPoDetail

@Suppress("UNCHECKED_CAST")
class ChooseImportPoDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {

    var importPo: ImportPo? = null
    var searchComponent = SearchComponent()
    private lateinit var claimImportPoViewModel: ClaimImportPoViewModel

    fun init(claimImportPoViewModel: ClaimImportPoViewModel) {
        this.claimImportPoViewModel = claimImportPoViewModel
        this.importPo = claimImportPoViewModel.importPo
        searchComponent.resetData()
        search()
    }

    override fun search() {
        val details = claimImportPoViewModel.baseModelsE as List<ClaimDetail>
        setData(claimImportPoViewModel.importPo.importPoDetails
                .asSequence()
                .filter {
                    contain(details, it)
                }
                .filter {
                    it.product.name.contains(searchComponent.getCodeSearch(), true)
                            || (it.productPacking.code.contains(searchComponent.getCodeSearch(), true))
                }
                .toList())
    }

    private fun contain(details: List<ClaimDetail>, importPoDetail: ImportPoDetail): Boolean {
        return details.asSequence().find { claimDetail ->
            claimDetail.productPacking.id == importPoDetail.productPacking.id
                    && claimDetail.expireDate == importPoDetail.expireDate
        } == null
    }


}