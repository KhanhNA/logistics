package com.next_solutions.logistics.models.dao;

import com.next_solutions.logistics.models.entity.ProductEntity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface ProductDao extends BaseDao<ProductEntity> {
    @Query("SELECT p.* from product p WHERE p.id NOT IN(:exceptId) LIMIT :limit OFFSET :offset")
    LiveData<List<ProductEntity>> getAllProduct(long[] exceptId, int limit, int offset);

    @Query("DELETE FROM product")
    void deleteAll();
}
