package com.next_solutions.logistics.component.store

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import com.next_solutions.logistics.models.Store

abstract class BaseStoreComponent {
    abstract fun callApiGetFromStore()

    var stores: ObservableList<Store> = ObservableArrayList()
        private set
    var selectedStorePosition = 0

    fun getSelectedStore(): Store? {
        if (selectedStorePosition >= stores.size) {
            return null
        }
        return stores[selectedStorePosition]
    }

    fun resetData() {
        selectedStorePosition = 0
    }
}