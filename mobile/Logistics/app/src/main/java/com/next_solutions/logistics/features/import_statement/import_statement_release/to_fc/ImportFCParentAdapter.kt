package com.next_solutions.logistics.features.import_statement.import_statement_release.to_fc

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.databinding.FragmentImportStatementFcItemBinding
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.utils.RecycleViewCallBack

class ImportFCParentAdapter(private var viewModel: ImportStatementFCViewModel,
                            private var context: Context,
                            private var fragmentManager: FragmentManager,
                            private var adapterListener: RecycleViewCallBack) : RecyclerView.Adapter<ImportFCParentAdapter.Item>() {
    private var listData: ArrayList<ImportStatementDetail> = viewModel.listData

    init {
        listData = viewModel.listData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        val binding =
                DataBindingUtil.inflate<FragmentImportStatementFcItemBinding>(LayoutInflater.from(parent.context),
                        R.layout.fragment_import_statement_fc_item, parent, false)
        return Item(binding)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: Item, position: Int) {
        val importStatementDetail = listData[position]
        importStatementDetail.index = position + 1
        val childAdapter = ImportFCChildAdapter(importStatementDetail, fragmentManager, viewModel)

        val recycledViewPool = RecyclerView.RecycledViewPool()

        val manager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val recyclerView = holder.binding.listChild.apply {
            adapter = childAdapter
            layoutManager = manager
        }
        recyclerView.setRecycledViewPool(recycledViewPool)
        holder.bind(importStatementDetail, viewModel, adapterListener)
    }

    class Item(var binding: FragmentImportStatementFcItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(detail: ImportStatementDetail, viewModel: ImportStatementFCViewModel, adapterListener: RecycleViewCallBack) {
            binding.viewModel = viewModel
            binding.viewHolder = detail
            binding.listener = adapterListener
        }
    }


}