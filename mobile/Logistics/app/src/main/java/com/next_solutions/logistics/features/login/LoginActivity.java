package com.next_solutions.logistics.features.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.next_solutions.base.BaseActivity;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.features.home.HomeActivity;

import java.util.Locale;

@SuppressLint("SetTextI18n")
public class LoginActivity extends BaseActivity {
    LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginViewModel = (LoginViewModel) viewModel;
        if (AppUtils.getInstance().getPreferencesHelper().getCurrentLanguage() != -1) {
            loginViewModel.getLanguageCode().set(AppUtils.LANGUAGE_CODE[AppUtils.getInstance().getPreferencesHelper().getCurrentLanguage()]);
        }
        binding.setLifecycleOwner(this);
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "startHome":
                Intent home = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(home);
                finish();
                break;
            case "CHANGE_LOGIN_TEXT":
                String s = loginViewModel.getLanguageCode().get();
                setLocale(s);
                break;
            case "CANT_CONNECT_SERVER":
                showAlertWarning(R.string.network_error);
                break;
            default:
                break;
        }
    }

    public void setLocale(String lang) {
        Log.e("=====================", "Đổi ngôn ngữ " + lang);
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, LoginActivity.class);
        finish();
        startActivity(refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginViewModel.class;
    }
}
