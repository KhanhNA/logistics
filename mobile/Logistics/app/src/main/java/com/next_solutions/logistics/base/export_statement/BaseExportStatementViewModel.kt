package com.next_solutions.logistics.base.export_statement

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Config
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.models.ExportStatementDetail
import com.next_solutions.logistics.models.exception.InvalidDetailException
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.DialogCallBack
import java.math.BigDecimal

@Suppress("UNCHECKED_CAST")
abstract class BaseExportStatementViewModel(application: Application) : BaseViewModel<BaseModel>(application), DialogCallBack {
    var exceptId: ArrayList<Long>? = arrayListOf()
    var buttonEnable = ObservableField(true)
    val descriptionObservable = ObservableField(EMPTY_STRING)
    var fromStoreId: Long? = null
    var config: Config? = null
    var exportStatementId: Long? = null

    // data for api when create or update
    var exportStatements: List<ExportStatement> = arrayListOf()

    abstract fun resetData()
    abstract fun prepareStatement(): List<ExportStatement>
    abstract fun createOrUpdateExportStatement(exportStatements: List<ExportStatement>)
    abstract fun statementSuccess()
    open fun addNewStatementDetail(detail: ExportStatementDetail) {
        val size = baseModelsE.size
        baseModelsE.add(detail)
        exceptId!!.add(detail.productPackingId)
        notifyPosition.set(size)
        sendBackAction(NOTIFY_CHANGE_ITEM_ADD)
    }

    override fun init() {
        exceptId = arrayListOf()
        getConfig()
    }

    private fun getConfig() {
        callApi(
                Service.callConfigService()
                        .getConfig("EXPORT_STATEMENT_WARNING")
                        .compose(Service.getRxSingleSchedulers().applySchedulers())
                        .subscribeWith(object : RxSingleSubscriber<Config>(appException, Config::class.java, null) {
                            override fun getDataSuccess(config: Config) {
                                getConfigSuccess(config)
                            }

                            override fun onFailure(errorRespone: ErrorRespone?) {
                                Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                            }
                        }))
    }

    private fun getConfigSuccess(config: Config) {
        this.config = config
    }

    open fun deleteItem(exportStatementDetail: ExportStatementDetail) {
        val selectPosition = exceptId!!.indexOf(exportStatementDetail.productPackingId)
        if (selectPosition != -1) {
            if (baseModelsE != null) {
                baseModelsE.removeAt(selectPosition)
                notifyPosition.set(selectPosition)
                sendBackAction(NOTIFY_CHANGE_ITEM_REMOVE)
            }
            exceptId!!.removeAt(selectPosition)
        }
    }

    open fun onCreate() {
        if (validate()) {
            exportStatements = prepareStatement()
            try {
                if (greaterThanMaxValue(exportStatements)) {
                    sendBackAction(CONFIRM)
                } else {
                    createOrUpdateExportStatement(exportStatements)
                }
            } catch (invalidDetailException: InvalidDetailException) {
                sendBackAction("MISSING_PRICE")
            }
        }
    }

    private fun greaterThanMaxValue(exportStatements: List<ExportStatement>): Boolean {
        if (config != null) {
            run {
                val currencyType = config!!.currency
                val maxValue = config!!.maxValue
                var sumValue = BigDecimal(0)
                for ((position, detail) in exportStatements[0].exportStatementDetails.withIndex()) {
                    val packingPrice = detail.productPacking.productPackingPrices
                            .find { price -> price.currency.code == currencyType }
                    if (packingPrice != null) {
                        sumValue = sumValue.add(packingPrice.price * BigDecimal(detail.productPacking.packingType.quantity * detail.quantity))
                    } else {
                        notifyPosition.set(position)
                        throw InvalidDetailException()
                    }
                }
                return sumValue.compareTo(BigDecimal(maxValue)) != -1
            }
        }
        return false
    }


    private fun validate(): Boolean {
        val details: List<ExportStatementDetail> = baseModelsE as List<ExportStatementDetail>
        if (details.isEmpty()) {
            sendBackAction(MISSING_DETAILS)
            return false
        }
        for ((position, detail) in details.withIndex()) {
            if ((detail.quantity > detail.totalQuantity) || (detail.quantity <= 0)) {
                notifyPosition.set(position)
                sendBackAction(ERROR_INVALID_INPUT)
                return false
            }
        }

        return true
    }

    override fun onCancel() {
    }

    override fun callBack(obj: Any?) {
        if (obj is ExportStatementDetail) {
            addNewStatementDetail(obj)
        } else if (obj is List<*>) {
            createOrUpdateExportStatement(obj as List<ExportStatement>)
        }
    }
}