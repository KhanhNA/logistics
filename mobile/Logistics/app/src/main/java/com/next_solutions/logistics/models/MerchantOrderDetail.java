package com.next_solutions.logistics.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MerchantOrderDetail extends Base {


    /**
     *
     */
    private static final long serialVersionUID = -7080433788939135359L;

    private MerchantOrder merchantOrder;

    private ProductPacking productPacking;

	private Long quantity;

	private BigDecimal price;

    /**
     * 0: Hàng bán, 1: Hàng khuyến mại
     */
    private Integer type;

	/**
	 * 0: from_store không đủ hàng, 1: from_store đủ hàng
     */
    private Integer status;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public MerchantOrder getMerchantOrder() {
        return merchantOrder;
    }

    public void setMerchantOrder(MerchantOrder merchantOrder) {
        this.merchantOrder = merchantOrder;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}