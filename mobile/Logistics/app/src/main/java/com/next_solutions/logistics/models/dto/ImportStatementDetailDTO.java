package com.next_solutions.logistics.models.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.models.Base;
import com.next_solutions.logistics.models.Pallet;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportStatementDetailDTO extends Base {
    private Long productPackingId;
    private String productPackingCode;
    private Long quantity;
    private String expireDate;
    private List<SplitRepack> splitRepacks;

    private String productPackingName;
    private String productName;
    private String uom;
    private Long packingQuantity;
    public Long claimQuantity;
    public Long originalQuantity;
    public String packingUom;
    public Pallet pallet;
    public Boolean fromClaim;
    public String storeProductPackingDetailCode;
    public int state;
    //
    public Long getProductPackingId() {
        return this.productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }

    public String getProductPackingCode() {
        return this.productPackingCode;
    }

    public void setProductPackingCode(String productPackingCode) {
        this.productPackingCode = productPackingCode;
    }

    public Long getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public String getExpireDateDisplay() {
        return expireDate == null ? EMPTY_STRING : DateUtils.convertStringDateToDifferentType(this.expireDate, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);

    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public List<SplitRepack> getSplitRepacks() {
        return splitRepacks;
    }

    public void setSplitRepacks(List<SplitRepack> splitRepacks) {
        this.splitRepacks = splitRepacks;
    }

    public String getProductPackingName() {
        return productPackingName;
    }

    public void setProductPackingName(String productPackingName) {
        this.productPackingName = productPackingName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public Long getPackingQuantity() {
        return packingQuantity;
    }

    public void setPackingQuantity(Long packingQuantity) {
        this.packingQuantity = packingQuantity;
    }
}
