package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.config.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Claim extends Base {
    public enum ClaimStatus {
        WAIT_APPROVE(0), APPROVED(1), REJECTED(2);

        Integer value;

        ClaimStatus(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return this.value;
        }
    }

    public enum ClaimType {
        IMPORT_PO("IMPORT_PO"),
        IMPORT_PO_UPDATE_PALLET("IMPORT_PO_UPDATE_PALLET"),
        REPACKING_PLANNING("REPACKING_PLANNING"),
        IMPORT_STATEMENT_REPACKING_PLANNING("IMPORT_STATEMENT_REPACKING_PLANNING"),
        IMPORT_STATEMENT_EXPORT_STATEMENT("IMPORT_STATEMENT_EXPORT_STATEMENT"),
        EXPORT_STATEMENT("EXPORT_STATEMENT"),
        DELIVERY("DELIVERY"),
        OUT_OF_DATE_PALLET_STEP_1("OUT_OF_DATE_PALLET_STEP_1"),
        OUT_OF_DATE_PALLET_STEP_2("OUT_OF_DATE_PALLET_STEP_2"),
        OTHER("OTHER");

        String value;

        ClaimType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public String getValueDisplay() {
            if (value.equals(ClaimType.IMPORT_PO.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_import_po);
            } else if (value.equals(ClaimType.IMPORT_PO_UPDATE_PALLET.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_import_po_update_pallet);
            } else if (value.equals(ClaimType.REPACKING_PLANNING.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_repacking_planning);
            } else if (value.equals(ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_import_statement_repacking_planning);
            } else if (value.equals(ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_import_statement_export_statement);
            } else if (value.equals(ClaimType.EXPORT_STATEMENT.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_export_statement);
            } else if (value.equals(ClaimType.DELIVERY.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_delivery);
            } else if (value.equals(ClaimType.OUT_OF_DATE_PALLET_STEP_1.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_out_of_date_pallet_step_1);
            } else if (value.equals(ClaimType.OUT_OF_DATE_PALLET_STEP_2.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_out_of_date_pallet_step_2);
            } else if (value.equals(ClaimType.OTHER.value)) {
                return AppUtils.getInstance().getString(R.string.claim_type_other);
            }
            return EMPTY_STRING;
        }
    }

    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public String getStatusDisplay() {
        switch (status) {
            case 0:
                return AppUtils.getInstance().getString(R.string.wait_approve);
            case 1:
                return AppUtils.getInstance().getString(R.string.approved);
            case 2:
                return AppUtils.getInstance().getString(R.string.rejected);
        }
        return EMPTY_STRING;
    }

    private String code;
    private String rejectReason;
    private Store store;
    private ClaimType type;
    private Long referenceId;
    private String amenable;
    private String description;

    public void setStatus(Integer status) {
        this.status = status;
    }

    private List<ClaimDetail> claimDetails;
    private List<ClaimImage> claimImages;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Store getStore() {
        return this.store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public ClaimType getType() {
        return this.type;
    }

    public void setType(ClaimType type) {
        this.type = type;
    }

    public Long getReferenceId() {
        return this.referenceId;
    }

    public void setReferenceId(Long referenceId) {
        this.referenceId = referenceId;
    }

    public String getAmenable() {
        return this.amenable;
    }

    public void setAmenable(String amenable) {
        this.amenable = amenable;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public List<ClaimDetail> getClaimDetails() {
        return this.claimDetails;
    }

    public void setClaimDetails(List<ClaimDetail> claimDetails) {
        this.claimDetails = claimDetails;
    }

    public List<ClaimImage> getClaimImages() {
        return claimImages;
    }

    public void setClaimImages(List<ClaimImage> claimImages) {
        this.claimImages = claimImages;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }
}