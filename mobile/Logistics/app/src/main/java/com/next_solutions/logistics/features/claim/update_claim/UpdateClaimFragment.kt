package com.next_solutions.logistics.features.claim.update_claim

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimFragment
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.databinding.FragmentUpdateClaimBinding
import com.next_solutions.logistics.features.claim.claim_pallet_step_one.DialogChoosePalletStepOneFragment
import com.next_solutions.logistics.features.claim.claim_pallet_step_two.DialogChoosePalletStepTwoFragment
import com.next_solutions.logistics.features.claim.view_claim_image.ViewClaimImageActivity
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.Image
import com.next_solutions.logistics.models.ImageModel
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.MODEL
import com.next_solutions.logistics.utils.RecycleViewCallBack
import com.yanzhenjie.album.AlbumFile
import java.util.*

class UpdateClaimFragment : BaseClaimFragment() {

    private lateinit var updateClaimViewModel: UpdateClaimViewModel
    private lateinit var bindingFragment: FragmentUpdateClaimBinding
    private lateinit var imageClaimAdapter: UpdateImageClaimAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpBindingAndViewModel()
        getData()
        updateClaimViewModel.init()
        return binding.root
    }

    override fun setUpRecycleViewImage() {
        imageClaimAdapter = UpdateImageClaimAdapter(baseClaimViewModel.images, RecycleViewCallBack { view, image ->
            run {
                if (view!!.id == R.id.delete_image) {
                    updateClaimViewModel.removeImage(image as Image)
                    imageClaimAdapter.notifyDataSetChanged()
                } else if (view.id == R.id.claim_image) {
                    showImage(image as Image)
                }
            }
        }, baseClaimViewModel)
        val linearLayout = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.root.findViewById<RecyclerView>(getRecycleViewImage()).apply {
            adapter = imageClaimAdapter
            layoutManager = linearLayout
        }
    }

    private fun showImage(image: Image) {
        val intent = Intent(context, ViewClaimImageActivity::class.java)
        val bundle = Bundle()
        val images = updateClaimViewModel.images
                .asSequence()
                .map {
                    if (it.isLocalImage) {
                        ImageModel().apply { path = it.albumFile.path }
                    } else {
                        ImageModel().apply { url = it.claimImage.url }
                    }
                }
                .toList()
                .toTypedArray()
        bundle.putSerializable(MODEL, images)
        bundle.putSerializable(Constants.POSITION, updateClaimViewModel.images.indexOf(image))
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun setUpBindingAndViewModel() {
        updateClaimViewModel = viewModel as UpdateClaimViewModel
        bindingFragment = binding as FragmentUpdateClaimBinding
    }

    private fun getData() {
        val claimStatement = activity?.intent?.extras?.get(MODEL) as Claim?
        claimStatement?.let { updateClaimViewModel.claim = claimStatement }
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        super.processFromVM(action, view, viewModel, t)
        if (action == Constants.RELOAD_IMAGE) {
            imageClaimAdapter.notifyDataSetChanged()
        } else if (action == Constants.UPLOAD_IMAGE_CLAIM_SUCCESS) {
            val message = context!!.getString(R.string.upload_image_success)
            showNotification(AppUtils.LOGISTIC, message, AppUtils.NOTIFICATION_ID, AppUtils.NOTIFICATION_NAME, R.drawable.alert_upload)
        }
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)

        if (view!!.id == R.id.add_product) {
            if(this.updateClaimViewModel.claim?.type?.equals(Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_1)!! ){
                val dialog = DialogChoosePalletStepOneFragment(updateClaimViewModel.claim!!.store.id, updateClaimViewModel.claim!!.claimDetails, updateClaimViewModel)

                dialog.show((activity as FragmentActivity).supportFragmentManager, "")
            }
            else if(this.updateClaimViewModel.claim?.type?.equals(Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_2)!! ){
                val dialog = DialogChoosePalletStepTwoFragment(updateClaimViewModel.claim!!.store.id, updateClaimViewModel.claim!!.claimDetails, updateClaimViewModel)

                dialog.show((activity as FragmentActivity).supportFragmentManager, "")
            }
            else {
                val dialog = ChooseClaimDetailDialog()
                dialog.show((activity as FragmentActivity).supportFragmentManager, "")
            }
        }
    }

    override fun doAfterPickImageFromGallery(result: ArrayList<AlbumFile>) {
        baseClaimViewModel.updateImages(result)
        imageClaimAdapter.notifyDataSetChanged()
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_update_claim_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return UpdateClaimViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_update_claim
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}