package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Statement extends Base {
    private Store fromStore;
    private Store toStore;
    private String code;
    private String qRCode;
    private String description;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date importDate;
    private List<StatementDetail> importStatementDetails;
    private List<StatementDetail> exportStatementDetails;
    private Long toStoreId;
    private String toStoreCode;
    private Long fromStoreId;
    private String fromStoreCode;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date exportDate;
    private Integer status;

    public Store getFromStore() {
        return fromStore;
    }

    public void setFromStore(Store fromStore) {
        this.fromStore = fromStore;
    }

    public Store getToStore() {
        return toStore;
    }

    public void setToStore(Store toStore) {
        this.toStore = toStore;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getqRCode() {
        return qRCode;
    }

    public void setqRCode(String qRCode) {
        this.qRCode = qRCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getImportDate() {
        return importDate;
    }

    public void setImportDate(Date importDate) {
        this.importDate = importDate;
    }

    public List<StatementDetail> getImportStatementDetails() {
        return importStatementDetails;
    }

    public void setImportStatementDetails(List<StatementDetail> importStatementDetails) {
        this.importStatementDetails = importStatementDetails;
    }

    public List<StatementDetail> getExportStatementDetails() {
        return exportStatementDetails;
    }

    public void setExportStatementDetails(List<StatementDetail> exportStatementDetails) {
        this.exportStatementDetails = exportStatementDetails;
    }

    public Long getToStoreId() {
        return toStoreId;
    }

    public void setToStoreId(Long toStoreId) {
        this.toStoreId = toStoreId;
    }

    public String getToStoreCode() {
        return toStoreCode;
    }

    public void setToStoreCode(String toStoreCode) {
        this.toStoreCode = toStoreCode;
    }

    public Long getFromStoreId() {
        return fromStoreId;
    }

    public void setFromStoreId(Long fromStoreId) {
        this.fromStoreId = fromStoreId;
    }

    public String getFromStoreCode() {
        return fromStoreCode;
    }

    public void setFromStoreCode(String fromStoreCode) {
        this.fromStoreCode = fromStoreCode;
    }

    public Date getExportDate() {
        return exportDate;
    }

    public void setExportDate(Date exportDate) {
        this.exportDate = exportDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
