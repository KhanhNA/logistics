package com.next_solutions.logistics.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.next_solutions.logistics.models.Rule;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.PreferencesHelper;
import com.next_solutions.logistics.utils.handler.CrashHandler;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;

import io.reactivex.plugins.RxJavaPlugins;

public class AppUtils extends MultiDexApplication {
    public static final String[] LANGUAGE_CODE = {"vi", "en", "my"};
    public static boolean IS_TEST_MODE = false;
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS'Z'";
    public static final int IMAGE_COLUMN = 2;
    public static final int IMAGE_UPLOAD_SIZE = 10;
    public static final String NOTIFICATION_ID = "Logistic";
    public static final String LOGISTIC = "LOGISTIC";
    public static final String NOTIFICATION_NAME = "Logistic";
    public static final String APP_INFO = "A31b4c24l3kj35d4AKJQ";
    public static final String CLIENT_ID = "Logistics";
    public static final String CLIENT_SECRET = "XY7kmzoNzl100";
    public static final int PAGE_SIZE = 20;
    public static final int FIRST_PAGE = 1;
    public static String BASE_URL = "http://192.168.1.69:8888";
    public static String BASE_URL_OAUTH = "http://192.168.1.69:9999";
    public static String IMAGE_URL = "http://192.168.1.69:8888/files/";
    //    public static String BASE_URL = "http://cloud.nextsolutions.com.vn:8888";
//    public static String BASE_URL_OAUTH = "http://cloud.nextsolutions.com.vn:9999";
//    public static String IMAGE_URL = "http://cloud.nextsolutions.com.vn:8888/files/";
    private static AppUtils mInstance;
    private PreferencesHelper preferencesHelper;
    private static Long language = 1L;
    public static String BASE_URL_TEST = "http://localhost:";
    public static String APPLICATION_JSON = "application/json";
    public static Rule rule;

    @Override
    public void onCreate() {
        super.onCreate();

        RxJavaPlugins.setErrorHandler(throwable -> Log.e("RxJavaPlugins", throwable.getMessage()));
        Album.initialize(AlbumConfig.newBuilder(this)
                .setAlbumLoader(new MediaLoader())
                .build());
        Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(getApplicationContext()));
        mInstance = this;
    }

    public static AppUtils getInstance() {
        return mInstance;
    }

    public static Long getLanguage() {
        return language;
    }


    public static Rule getRule() {
        return rule;
    }

    public static void setRule(Rule rule) {
        AppUtils.rule = rule;
    }

    public static void setLanguage(Long language) {
        AppUtils.language = language;
    }


    public PreferencesHelper getPreferencesHelper() {
        if (preferencesHelper == null) {
            preferencesHelper = new PreferencesHelper();
            SharedPreferences sharedPreferences = getSharedPreferences(APP_INFO, Context.MODE_PRIVATE);
            preferencesHelper.setSharedPreferences(sharedPreferences);
        }
        return preferencesHelper;
    }


    public static String changeFormCode(String code) {
        StringBuilder result = new StringBuilder();
        String[] s = code.replaceAll("\\r", Constants.EMPTY_STRING)
                .replaceAll("\\n", Constants.EMPTY_STRING)
                .split("\\s+");
        for (String str : s) {
            result.append(str).append(" ");
        }
        return result.toString().trim();
    }
}
