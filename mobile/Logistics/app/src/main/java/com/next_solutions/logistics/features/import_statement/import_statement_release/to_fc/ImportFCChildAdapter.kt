package com.next_solutions.logistics.features.import_statement.import_statement_release.to_fc

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.databinding.FragmentImportStatementFcItem2Binding
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.models.ImportStatementDetailPallet
import com.next_solutions.logistics.models.Pallet
import com.next_solutions.logistics.utils.DialogCallBack
import com.next_solutions.logistics.utils.RecycleViewCallBack

class ImportFCChildAdapter(private var importStatementDetail: ImportStatementDetail,
                           private var fragmentManager: FragmentManager,
                           private var viewModel: ImportStatementFCViewModel) : RecyclerView.Adapter<ImportFCChildAdapter.ViewHolder>(), DialogCallBack {
    private var notifyPosition = 0
    private lateinit var chooseDetail: ImportStatementDetailPallet
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
                DataBindingUtil.inflate<FragmentImportStatementFcItem2Binding>(LayoutInflater.from(parent.context),
                        R.layout.fragment_import_statement_fc_item2, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        if (importStatementDetail.importStatementDetailPallets == null) {
            return 0
        }
        return importStatementDetail.importStatementDetailPallets.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val palletDetail = importStatementDetail.importStatementDetailPallets[position]

        holder.bind(palletDetail, viewModel,
                RecycleViewCallBack { view, obj ->
                    run {
                        val detail = obj as ImportStatementDetailPallet
                        val eventPosition = importStatementDetail.importStatementDetailPallets.indexOf(detail)

                        when {
                            view!!.id == R.id.choose_pallet -> {
                                notifyPosition = eventPosition
                                chooseDetail = detail
                                showPalletPicker()
                            }
                            view.id == R.id.delete_detail -> {
                                notifyPosition = eventPosition + 1
                                removeRow(eventPosition, detail)
                            }
                            view.id == R.id.add_detail -> {
                                notifyPosition = eventPosition
                                addRow(eventPosition)
                            }
                        }
                    }
                })
    }

    private fun addRow(eventPosition: Int) {
        val addPosition = eventPosition + 1
        importStatementDetail.importStatementDetailPallets
                .add(addPosition, ImportStatementDetailPallet().apply {
                    importStatementDetailId = importStatementDetail.id
                    importStatementId = viewModel.importStatement!!.id
                    productPackingId = importStatementDetail.productPacking.id
                    expireDate = importStatementDetail.expireDate
                    quantity = 0
                    pallet = Pallet()
                })
        notifyItemInserted(addPosition)
    }

    private fun removeRow(removePosition: Int, palletDetail: ImportStatementDetailPallet) {
        if (removePosition == 0) {
            return
        }
        notifyItemRemoved(removePosition)
        importStatementDetail.importStatementDetailPallets.removeAt(removePosition)
        viewModel.reCalculator(palletDetail.quantity * -1, palletDetail)
    }

    private fun showPalletPicker() {
        val dialog = ChoosePalletForImportFcDialog()
        dialog.setDialogCallBack(this)
        dialog.show(fragmentManager, "")
    }

    class ViewHolder(var binding: FragmentImportStatementFcItem2Binding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(palletDetail: ImportStatementDetailPallet, viewModel: ImportStatementFCViewModel,
                 adapterListener: RecycleViewCallBack) {
            binding.viewHolder = palletDetail
            binding.viewModel = viewModel
            binding.listener = adapterListener
        }
    }

    override fun onCancel() {

    }

    override fun callBack(obj: Any?) {
        val pallet = obj as Pallet
        chooseDetail.pallet = Pallet().apply {
            id = pallet.id
            displayName = pallet.displayName
            code = pallet.code
            step = pallet.step
        }
        notifyItemChanged(notifyPosition)
    }


}