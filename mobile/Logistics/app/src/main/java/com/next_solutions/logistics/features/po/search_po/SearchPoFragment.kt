package com.next_solutions.logistics.features.po.search_po

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.Po
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.FragmentHelper
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.po.PoUrlPath
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity
import java.util.*

class SearchPoFragment : BaseFragment(), DatePickerDialog.OnDateSetListener {

    private var searchPoViewModel: SearchPoViewModel? = null
    private var isFromDate: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpRecycleView()
        searchPoViewModel = viewModel as SearchPoViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchPoViewModel!!.init()
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        when (view!!.tag) {
            FROM_DATE -> {
                isFromDate = true
                val toDate = searchPoViewModel!!.chooseDate.toDate.get()
                showDialogDatePicker(null, toDate!!.time)
            }
            TO_DATE -> {
                isFromDate = false
                val fromDate = searchPoViewModel!!.chooseDate.fromDate.get()
                showDialogDatePicker(fromDate!!.time, System.currentTimeMillis() - 1000)
            }
            SCAN_CODE -> {
                val qrScan = Intent(context, QRCodeScannerActivity::class.java)
                startActivityForResult(qrScan, REQUEST_CODE_SCAN)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_SCAN) {
            searchPoViewModel?.searchComponent?.setData(data!!.getStringExtra(RESULT_SCAN))
            searchPoViewModel?.onClickButtonSearch()
        }
    }

    private fun showDialogDatePicker(minDate: Long?, maxDate: Long?) {
        val scene = context
        if (scene != null) {
            val calendar = Calendar.getInstance()
            val year: Int
            val month: Int
            val day: Int
            if (isFromDate) {
                val fromDate: Date = searchPoViewModel!!.chooseDate.fromDate.get()!!
                calendar.time = fromDate
                year = calendar[Calendar.YEAR]
                month = calendar[Calendar.MONTH]
                day = calendar[Calendar.DAY_OF_MONTH]
            } else {
                val fromDate: Date = searchPoViewModel!!.chooseDate.toDate.get()!!
                calendar.time = fromDate
                year = calendar[Calendar.YEAR]
                month = calendar[Calendar.MONTH]
                day = calendar[Calendar.DAY_OF_MONTH]
            }
            val datePickerDialog = DatePickerDialog(scene, this, year, month, day)
            if (maxDate != null) {
                datePickerDialog.datePicker.maxDate = maxDate
            }
            if (minDate != null) {
                datePickerDialog.datePicker.minDate = minDate
            }
            datePickerDialog.show()
        }
    }

    private fun setUpRecycleView() {
        val adapter = BaseAdapter(R.layout.fragment_search_po_item, viewModel, OwnerView { _, po ->
            run {
                if (RuleHelper().hasRule(HttpRequest(Method.GET.value, PoUrlPath.FIND_PO_DETAIL))) {
                    val intent = Intent(context, CommonActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable(MODEL, po as Po)
                    bundle.putSerializable(FRAGMENT, FragmentHelper().getFragmentType(po))
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }
        }, baseActivity)
        recyclerView?.adapter = adapter
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_search_po_v2
    }

    override fun getRecycleResId(): Int {
        return R.id.list_po
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return SearchPoViewModel::class.java
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        searchPoViewModel!!.setDate(isFromDate, year, month, dayOfMonth)
    }
}