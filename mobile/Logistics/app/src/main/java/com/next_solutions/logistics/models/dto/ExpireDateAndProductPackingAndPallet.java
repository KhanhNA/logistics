package com.next_solutions.logistics.models.dto;

public class ExpireDateAndProductPackingAndPallet {
    private String expireDate;
    private Long productPackingId;
    private String palletCode;

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Long getProductPackingId() {
        return productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }

    public String getPalletCode() {
        return palletCode;
    }

    public void setPalletCode(String palletCode) {
        this.palletCode = palletCode;
    }
}
