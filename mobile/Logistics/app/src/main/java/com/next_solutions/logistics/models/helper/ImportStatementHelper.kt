package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.models.ExportStatementDetail
import com.next_solutions.logistics.models.ImportStatement
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.models.StoreProductPackingDetail

class ImportStatementHelper(private val importStatement: ImportStatement) {

    fun groupDetailByExpireDateAndProductPacking(): List<ImportStatementDetail> {
        val result = arrayListOf<ImportStatementDetail>()
        val checked = HashMap<String, ImportStatementDetail>()

        importStatement.importStatementDetails.forEach { detail ->
            run {
                val key = detail.productPacking.id.toString() + detail.expireDateDisplay
                if (checked[key] != null) {
                    checked[key]!!.quantity += detail.quantity
                } else {
                    val exportDetail = ImportStatementDetail().apply {
                        id = detail.id
                        productPacking = detail.productPacking
                        quantity = detail.quantity
                        expireDate = detail.expireDate
                    }
                    result.add(exportDetail)
                    checked[key] = exportDetail
                }
            }
        }
        return result
    }

    fun groupDetailByExpireDateAndPackingFromExportStatement(): HashMap<String, ExportStatementDetail> {
        val result = arrayListOf<ExportStatementDetail>()
        val checked = HashMap<String, ExportStatementDetail>()

        importStatement.exportStatement.exportStatementDetails.forEach { detail ->
            run {
                val key = detail.productPacking.id.toString() + detail.expireDateDisplay
                if (checked[key] != null) {
                    checked[key]!!.quantity += detail.quantity
                } else {
                    val exportDetail = ExportStatementDetail().apply {
                        productPacking = detail.productPacking
                        quantity = detail.quantity
                        expireDate = detail.expireDate
                        storeProductPackingDetail = StoreProductPackingDetail().apply { code = detail.storeProductPackingDetail.code }
                    }
                    result.add(exportDetail)
                    checked[key] = exportDetail
                }
            }
        }
        return checked
    }
}