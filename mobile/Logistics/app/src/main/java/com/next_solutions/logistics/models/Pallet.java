package com.next_solutions.logistics.models;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.utils.ValidateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Pallet extends Base {
    public enum Status {
        ACTIVE(true), INACTIVE(false);

        private boolean value;

        Status(boolean value) {
            this.value = value;
        }

        public boolean getValue() {
            return this.value;
        }
    }

    public enum PalletStep {
        // Mới tạo lệnh nhập chưa thực nhập và chưa cộng kho
        IMPORT_PO_UPDATE_PALLET(1),
        // Đã tạo lệnh thực nhập và cộng kho
        IMPORT_STATEMENT_UPDATE_PALLET(2);

        private int value;

        PalletStep(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    private Store store;

    private String code;

    private String qRCode;

    private String name;

    private Integer step;

    private Pallet parentPallet;

    private List<Pallet> childPallets;

    private List<PalletDetail> palletDetails;

    private String displayName;

    public String getCode() {
        if (ValidateUtils.isNullOrEmpty(code)) {
            return EMPTY_STRING;
        }
        return code;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getqRCode() {
        return qRCode;
    }

    public void setqRCode(String qRCode) {
        this.qRCode = qRCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public Pallet getParentPallet() {
        return parentPallet;
    }

    public void setParentPallet(Pallet parentPallet) {
        this.parentPallet = parentPallet;
    }

    public List<Pallet> getChildPallets() {
        return childPallets;
    }

    public void setChildPallets(List<Pallet> childPallets) {
        this.childPallets = childPallets;
    }

    public List<PalletDetail> getPalletDetails() {
        return palletDetails;
    }

    public void setPalletDetails(List<PalletDetail> palletDetails) {
        this.palletDetails = palletDetails;
    }

    public String getDisplayName() {
        return displayName == null ? EMPTY_STRING : displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @NonNull
    @Override
    public String toString() {
        if (code != null && name != null) {
            return code + " - " + name;
        }
        if (code == null && name != null) {
            return name;
        }
        if (code != null) {
            return code;
        }
        return EMPTY_STRING;
    }
}