package com.next_solutions.logistics.base.claim

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.features.home.HomeActivity
import com.next_solutions.logistics.models.Image
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.RecycleViewCallBack
import com.yanzhenjie.album.Album
import com.yanzhenjie.album.AlbumFile
import java.util.*

abstract class BaseClaimFragment : BaseRecycleViewFragment() {
    abstract fun getRecycleViewImage(): Int
    lateinit var claimImageAdapter: ClaimImageAdapter
    lateinit var baseClaimViewModel: BaseClaimViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        baseClaimViewModel = viewModel as BaseClaimViewModel
        setUpRecycleViewImage()
        return binding.root
    }

    open fun setUpRecycleViewImage() {
        claimImageAdapter = ClaimImageAdapter(baseClaimViewModel.images, baseClaimViewModel, RecycleViewCallBack { view, image ->
            run {
                if (view.id == R.id.delete_image) {
                    baseClaimViewModel.removeImage(image as Image)
                    claimImageAdapter.notifyDataSetChanged()
                }
            }
        })

        val linearLayout = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.root.findViewById<RecyclerView>(getRecycleViewImage()).apply {
            adapter = claimImageAdapter
            layoutManager = linearLayout
        }
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            NOTIFY_CHANGE -> {
                baseAdapter!!.notifyDataSetChanged()
            }
            NOTIFY_CHANGE_ITEM -> {
                baseClaimViewModel.notifyPosition.get()?.let { baseAdapter!!.notifyItemChanged(it) }
            }
            NOTIFY_CHANGE_ITEM_REMOVE -> {
                baseClaimViewModel.notifyPosition.get()?.let { baseAdapter!!.notifyItemRemoved(it) }
            }
            NOTIFY_CHANGE_ITEM_ADD -> {
                baseClaimViewModel.notifyPosition.get()?.let { baseAdapter!!.notifyItemInserted(it) }
            }
            SUCCESS -> {
                showAlertSuccess(TIME_AFTER_DELAY, R.string.success, null)
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null && baseActivity !is HomeActivity) {
                        val returnIntent = Intent()
                        returnIntent.putExtra(CLAIM_ACTIVITY, CREATE_CLAIM_SUCCESS)
                        baseActivity.setResult(Activity.RESULT_OK, returnIntent)
                        baseActivity.finish()
                    }
                }, TIME_DELAY.toLong())
            }
            PRODUCT_INFO_ERROR -> {
                scrollToPosition()
                showAlertWarning(TIME_AFTER_DELAY, R.string.invalid_input, null)
            }
            "MISSING_AMENABLE" -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.missing_amenable, null)
            }
            UPLOAD_IMAGE_SUCCESS -> {
                val message = context!!.getString(R.string.upload_image_success)
                claimImageAdapter.notifyDataSetChanged()
                showNotification(AppUtils.LOGISTIC, message, AppUtils.NOTIFICATION_ID, AppUtils.NOTIFICATION_NAME, R.drawable.alert_upload)
            }
            MISSING_DESCRIPTION -> {
                showAlertWarning(R.string.missing_image_description)
            }
            UPDATE_SUCCESS -> {
                showAlertSuccess(TIME_AFTER_DELAY, R.string.update_success, null)
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
            }
            MISSING_DETAILS -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.missing_details, null)
            }
        }
    }

    private fun scrollToPosition() {
        baseClaimViewModel.notifyPosition.get()?.let {
            baseAdapter!!.notifyItemChanged(it)
            getLayoutManager()!!.scrollToPosition(it)
        }
        baseClaimViewModel.lastNotifyPosition?.let {
            baseAdapter!!.notifyItemChanged(it)
        }
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (view!!.id == R.id.add_image) {
            pickFromGallery()
        }
    }

    open fun doAfterPickImageFromGallery(result: ArrayList<AlbumFile>) {
        baseClaimViewModel.updateImages(result)
        claimImageAdapter.notifyDataSetChanged()
    }

    private fun pickFromGallery() {
        Album.album(this)
                .multipleChoice()
                .columnCount(AppUtils.IMAGE_COLUMN)
                .selectCount(AppUtils.IMAGE_UPLOAD_SIZE)
                .camera(true)
                .checkedList(baseClaimViewModel.images
                        .filter { it.albumFile != null }
                        .map { it.albumFile }
                        .toCollection(ArrayList()))
                .onResult { result: ArrayList<AlbumFile> ->
                    doAfterPickImageFromGallery(result)
                }
                .onCancel { Toast.makeText(context, "Cancel", Toast.LENGTH_LONG).show() }
                .start()
    }
}