package com.next_solutions.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.next_solutions.logistics.config.AppDatabase;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.models.dao.ProductDao;
import com.next_solutions.logistics.models.entity.ProductEntity;
import com.next_solutions.logistics.repository.callback.DatabaseCallback;

import java.util.List;


public class ProductRepository {
    private ProductDao mProductDao;
    private LiveData<List<ProductEntity>> allProduct;
    public ProductRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        mProductDao = db.productDao();
    }
    public void insert(ProductEntity product) {
        new InsertAsyncTask(mProductDao).execute(product);
    }
    public void insert(List<ProductEntity> product) {
        ProductEntity[] array = product.toArray(new ProductEntity[0]);
        new InsertAsyncTask(mProductDao).execute(array);
    }
    public void search(List<Long> exceptId, int page){
        long[] ids = new long[exceptId.size()];
        for(int i  = 0; i < exceptId.size(); i++){
            ids[i] = exceptId.get(i);
        }
        allProduct = mProductDao.getAllProduct(ids, AppUtils.PAGE_SIZE, AppUtils.PAGE_SIZE * (page - 1));
    }
    public void deleteAll(DatabaseCallback databaseCallback){
    }
    private static class InsertAsyncTask extends AsyncTask<ProductEntity, Void, Void> {

        private ProductDao mAsyncTaskDao;

        InsertAsyncTask(ProductDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ProductEntity... params) {
            mAsyncTaskDao.insert(params);
            return null;
        }
    }

}
