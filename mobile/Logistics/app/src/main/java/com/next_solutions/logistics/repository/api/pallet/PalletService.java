package com.next_solutions.logistics.repository.api.pallet;

import java.util.Map;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

import static com.next_solutions.logistics.repository.api.pallet.PalletUrlPath.FIND_ALL_PALLET;
import static com.next_solutions.logistics.repository.api.pallet.PalletUrlPath.FIND_INVENTORY_PALLET;
import static com.next_solutions.logistics.repository.api.pallet.PalletUrlPath.FIND_PALLET;

public interface PalletService {

    //    ========================PALLET========================

    @GET(FIND_PALLET)
    Single<Response<ResponseBody>> findPallet(@Query("pageNumber") Integer pageNumber,
                                              @Query("pageSize") Integer pageSize,
                                              @Query("status") Boolean status,
                                              @Query("step") Integer step,
                                              @Query("storeId") Long storeId,
                                              @Query("text") String text);

    @GET(FIND_ALL_PALLET)
    Call<ResponseBody> findAllPallet();


    @GET(FIND_INVENTORY_PALLET)
    Single<Response<ResponseBody>> findInventory(@Query("storeId") Long storeId,
                                                 @Query("distributorId") Long distributorId,
                                                 @Query("palletSteps") Integer palletSteps,
                                                 @Query("pageSize") Integer pageSize,
                                                 @Query("pageNumber") Integer pageNumber,
                                                 @Query("text") String text,
                                                 @QueryMap Map<String, String> exceptIds);
}
