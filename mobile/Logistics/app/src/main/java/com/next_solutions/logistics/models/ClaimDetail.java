package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class ClaimDetail extends Base {

    private Claim claim;

    private ProductPacking productPacking;

    private String expireDate;

    private Long quantity;

    private Long totalQuantity;

    private Long currentQuantity;

    private ProductPackingPrice productPackingPrice;

    private PalletDetail palletDetail;
    private Long palletId;
    private StoreProductPackingDetail storeProductPackingDetail;

    public Claim getClaim() {
        return this.claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public ProductPacking getProductPacking() {
        return this.productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Long getQuantity() {
        return this.quantity == null ? 0 : this.quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public ProductPackingPrice getProductPackingPrice() {
        return this.productPackingPrice;
    }

    public void setProductPackingPrice(ProductPackingPrice productPackingPrice) {
        this.productPackingPrice = productPackingPrice;
    }

    public PalletDetail getPalletDetail() {
        return this.palletDetail;
    }

    public void setPalletDetail(PalletDetail palletDetail) {
        this.palletDetail = palletDetail;
    }

    public StoreProductPackingDetail getStoreProductPackingDetail() {
        return this.storeProductPackingDetail;
    }

    public void setStoreProductPackingDetail(StoreProductPackingDetail storeProductPackingDetail) {
        this.storeProductPackingDetail = storeProductPackingDetail;
    }

    public Long getCurrentQuantity() {
        return currentQuantity;
    }

    public void setCurrentQuantity(Long currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    public String getExpireDateDisplay() {
        return DateUtils.convertStringDateToDifferentType(this.expireDate, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);
    }

    public Long getPalletId() {
        return palletId;
    }

    public void setPalletId(Long palletId) {
        this.palletId = palletId;
    }

    public String getPalletName() {
        return palletDetail == null || palletDetail.getPallet() == null ||
                palletDetail.getPallet().getDisplayName() == null ? null :
                palletDetail.getPallet().getDisplayName();
    }
}