package com.next_solutions.logistics.features.import_statement.claim_delivery

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.helper.ImportStatementHelper
import com.next_solutions.logistics.utils.Constants

@Suppress("UNCHECKED_CAST")
class ClaimDeliveryStatementViewModel(application: Application) : BaseClaimViewModel(application) {
    lateinit var importStatement: ImportStatement
    lateinit var importDetails: List<ImportStatementDetail>
    override fun init() {
        val claimDetails = arrayListOf<ClaimDetail>()
        importDetails = ImportStatementHelper(importStatement).groupDetailByExpireDateAndProductPacking()
        importDetails.forEach { details ->
            run {
                claimDetails.add(claimHelper.convertImportDetailToClaimDetail(details))
            }
        }
        setData(claimDetails as List<BaseModel>?)
    }

    override fun prepareClaim(): Claim {
        val details = baseModelsE as List<ClaimDetail>
        return Claim().apply {
            claimDetails = details
            type = Claim.ClaimType.DELIVERY
            description = descriptionObservable.get()
            referenceId = importStatement.id
            claimDetails = details
            store = Store().apply {
                code = importStatement.toStore.code
                id = importStatement.toStore.id
            }
            status = Claim.ClaimStatus.WAIT_APPROVE.value
            amenable = amenableObservable.get()
        }
    }

    fun addDetail(importStatementDetail: ImportStatementDetail) {
        val claimDetails = baseModelsE as ArrayList<ClaimDetail>
        notifyPosition.set(claimDetails.size)
        claimDetails.add(claimHelper.convertImportDetailToClaimDetail(importStatementDetail))
        sendBackAction(Constants.NOTIFY_CHANGE_ITEM_ADD)
    }
}