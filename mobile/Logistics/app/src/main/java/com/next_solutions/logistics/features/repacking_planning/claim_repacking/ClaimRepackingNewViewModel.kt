package com.next_solutions.logistics.features.repacking_planning.claim_repacking

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.utils.Constants

@Suppress("UNCHECKED_CAST")
class ClaimRepackingNewViewModel(application: Application) : BaseClaimViewModel(application) {
    lateinit var repackingPlanning: RepackingPlanning
    override fun prepareClaim(): Claim {
        val details = baseModelsE as List<ClaimDetail>
        return Claim().apply {
            claimDetails = details
            type = Claim.ClaimType.REPACKING_PLANNING
            description = descriptionObservable.get()
            referenceId = repackingPlanning.id
            claimDetails = details
            store = Store().apply {
                code = repackingPlanning.store.code
                id = repackingPlanning.store.id
            }
            status = Claim.ClaimStatus.WAIT_APPROVE.value
            amenable = amenableObservable.get()
        }
    }

    override fun init() {
        val claimDetails = arrayListOf<ClaimDetail>()
        repackingPlanning.repackingPlanningDetails.forEach { details ->
            run {
                claimDetails.add(claimHelper.convertRepackingDetailToClaimDetail(details))
            }
        }
        setData(claimDetails as List<BaseModel>?)
    }

    fun addDetail(repackingDetail: RepackingPlanningDetail) {
        val claimDetails = baseModelsE as ArrayList<ClaimDetail>
        notifyPosition.set(claimDetails.size)
        claimDetails.add(claimHelper.convertRepackingDetailToClaimDetail(repackingDetail))
        sendBackAction(Constants.NOTIFY_CHANGE_ITEM_ADD)
    }

}