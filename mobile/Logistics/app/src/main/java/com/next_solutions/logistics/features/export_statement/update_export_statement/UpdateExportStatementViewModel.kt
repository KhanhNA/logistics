package com.next_solutions.logistics.features.export_statement.update_export_statement

import android.app.Application
import android.util.Log
import com.next_solutions.base.BaseModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.base.export_statement.BaseExportStatementViewModel
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.models.ExportStatementDetail
import com.next_solutions.logistics.models.StoreProductPacking
import com.next_solutions.logistics.models.helper.ExportStatementDetailHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.UPDATE_SUCCESS
import java.util.*

@Suppress("UNCHECKED_CAST")
class UpdateExportStatementViewModel(application: Application) : BaseExportStatementViewModel(application) {
    lateinit var exportStatement: ExportStatement

    override fun init() {
        super.init()
        exceptId = arrayListOf()
        getStatementDetail()
        exportStatementId = exportStatement.id
        descriptionObservable.set(exportStatement.description)
    }

    override fun resetData() {
    }

    override fun prepareStatement(): List<ExportStatement> {
        val statement = ExportStatement().apply {
            id = exportStatement.id
            fromStoreId = exportStatement.fromStore.id
            fromStoreCode = exportStatement.fromStore.code
            toStoreId = exportStatement.toStore.id
            toStoreCode = exportStatement.toStore.code
            status = ExportStatement.ExportStatementStatus.REQUESTED.value
            exportStatementDetails = baseModelsE as MutableList<ExportStatementDetail>?
            description = descriptionObservable.get()
            merchantOrderIds = ArrayList()
        }
        val exportStatements: MutableList<ExportStatement> = ArrayList()
        exportStatements.add(statement)
        return exportStatements
    }

    override fun createOrUpdateExportStatement(exportStatements: List<ExportStatement>) {
        callApi(Service.callExportStatementService()
                .updateExportStatement(exportStatements[0].id, exportStatements[0])
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(t: ResponseStatus?) {
                        statementSuccess()
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        buttonEnable.set(true)
                    }
                }))
    }

    override fun statementSuccess() {
        buttonEnable.set(true)
        sendBackAction(UPDATE_SUCCESS)
    }

    private fun getStatementDetail() {
        callApi(Service.callExportStatementService()
                .findExportStatementById(exportStatement.getId())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ExportStatement>(appException, ExportStatement::class.java, null) {
                    override fun getDataSuccess(exportStatement: ExportStatement) {
                        getExportStatementDetailSuccess(exportStatement)
                        getProductInStore()
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message)
                    }
                }))
    }

    private fun getProductInStore() {
        val storeCodes: MutableList<String> = ArrayList()
        storeCodes.add(exportStatement.fromStore.code)
        val productPackingCodes: MutableList<String> = ArrayList()
        if (!exportStatement.exportStatementDetails.isNullOrEmpty()) {
            for (detail in exportStatement.exportStatementDetails) {
                productPackingCodes.add(detail.productPacking.code)
            }
            callApi(Service
                    .callMerchantOrderService()
                    .getInventoryLeft(productPackingCodes, storeCodes)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<List<StoreProductPacking>>(appException, StoreProductPacking::class.java, MutableList::class.java) {
                        override fun getDataSuccess(storeProductPacks: List<StoreProductPacking>) {
                            recalculationTotalQuantity(storeProductPacks)
                        }

                        override fun onFailure(errorRespone: ErrorRespone) {
                            Log.e(Constants.ERROR_PROCESS, errorRespone.message)
                        }
                    }))
        }
    }

    private fun recalculationTotalQuantity(storeProductPacks: List<StoreProductPacking>) {
        for (inventory in storeProductPacks) {
            for (exportStatementDetail in exportStatement.exportStatementDetails) {
                if (exportStatementDetail.productPackingId == inventory.productPacking.getId()) {
                    exportStatementDetail.totalQuantity = exportStatementDetail.quantity + inventory.totalQuantity
                }
            }
        }

        setData(exportStatement.exportStatementDetails as List<BaseModel>?)

    }

    private fun getExportStatementDetailSuccess(exportStatement: ExportStatement) {
        this.exportStatement = exportStatement
        val helper = ExportStatementDetailHelper()
        val details = exportStatement
                .exportStatementDetails
                .asSequence()
                .map {
                    helper.minimizeDetail(it)
                }.toList()
        this.exportStatement.exportStatementDetails = ExportStatementDetailHelper().groupExportDetails(details)

        fromStoreId = this.exportStatement.fromStore.id

        exceptId!!.addAll(this.exportStatement.exportStatementDetails.map { it.productPackingId })
    }
}