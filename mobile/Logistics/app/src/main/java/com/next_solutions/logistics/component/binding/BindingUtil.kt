package com.next_solutions.logistics.component.binding

import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.utils.DateUtils
import com.next_solutions.logistics.utils.NumberFormat
import java.math.BigDecimal
import java.util.*


@BindingAdapter("textNumber")
fun TextView.setTextNumber(value: Long?) {
    if (value == null) return
    this.text = NumberFormat.formatNumber(value)
}

@BindingAdapter("textNumber")
fun TextView.setTextNumber(value: BigDecimal?) {
    if (value == null) return
    this.text = NumberFormat.formatNumber(value)
}


@BindingAdapter("textDate")
fun TextView.setTextDate(date: Date?) {
    if (date == null) return
    this.text = DateUtils.clearTimeAndConvertDateToString(date, DateUtils.DATE_SHOW_FORMAT)
}

@BindingAdapter("android:layout_marginBottom")
fun setBottomMargin(view: View, bottomMargin: Float) {
    val layoutParams = view.layoutParams as MarginLayoutParams
    layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin,
            layoutParams.rightMargin, Math.round(bottomMargin))
    view.layoutParams = layoutParams
}

@BindingAdapter("method", "url")
fun bindAuthentication(cardView: CardView, method: Method, url: String) {
    cardView.visibility = if (RuleHelper().hasRule(HttpRequest(method.value, url))) View.VISIBLE else View.GONE
}