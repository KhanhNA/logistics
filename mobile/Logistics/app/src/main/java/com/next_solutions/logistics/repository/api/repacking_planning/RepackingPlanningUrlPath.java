package com.next_solutions.logistics.repository.api.repacking_planning;

public interface RepackingPlanningUrlPath {
    String FIND_REPACKING_PLANNING = "/repacking-plannings";
    String REPACK = "/repacking-plannings/repack/{id}";
    String FIND_REPACKING_PLANNING_BY_ID = "/repacking-plannings/{id}";
    String DETAIL_REPACKED = "/repacking-plannings/detail-repacked/{id}";

}
