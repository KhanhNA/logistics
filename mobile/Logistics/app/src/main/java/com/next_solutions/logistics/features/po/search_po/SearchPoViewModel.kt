package com.next_solutions.logistics.features.po.search_po

import android.app.Application
import android.util.Log
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.component.date.ChooseDateComponent
import com.next_solutions.logistics.component.distributor.DistributorComponent
import com.next_solutions.logistics.component.import_po_status.CurrentPoStatusComponent
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.component.store.FromFCComponent
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Po
import com.next_solutions.logistics.models.Store
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.ERROR_PROCESS
import com.next_solutions.logistics.utils.DateUtils
import com.next_solutions.logistics.utils.DateUtils.DATE_FORMAT
import java.util.*

class SearchPoViewModel(application: Application) : BaseViewModel<BaseModel>(application) {

    val searchComponent = SearchComponent()
    val storeComponent = FromFCComponent(this)
    val distributorComponent = DistributorComponent(this, true)
    val currentPoStatusComponent = CurrentPoStatusComponent()
    val statusComponent = CurrentPoStatusComponent()
    var chooseDate = ChooseDateComponent()


    override fun init() {
        clearDataRecycleView()
        clearDataFormSearch()
        search()
    }

    private fun clearDataFormSearch() {
        storeComponent.resetData()
        distributorComponent.resetData()
        statusComponent.resetData()
        currentPoStatusComponent.resetData()
        chooseDate.fromDate.set(DateUtils.reduceOneMonth(Calendar.getInstance().time))
        chooseDate.toDate.set(Calendar.getInstance().time)
        searchComponent.resetData()
    }

    override fun search() {

        val from = DateUtils.clearTimeAndConvertDateToString(chooseDate.fromDate.get(), DATE_FORMAT)
        val to = DateUtils.clearTimeAndConvertDateToString(chooseDate.toDate.get(), DATE_FORMAT)

        val status: String = Po.PoStatus.getValue(statusComponent.selectedStatusPosition);

        val currentStatus: String = Po.PoStatus.getValue(currentPoStatusComponent.selectedStatusPosition);
        var distributorId = Constants.EMPTY_STRING
        val disId = distributorComponent.getSelectedDistributor()?.id
        if (disId != null) {
            distributorId = disId.toString()
        }
        findPo(storeComponent.getSelectedStore(), distributorId, from, to, status, currentStatus)
    }

    private fun findPo(storeSelected: Store?, distributorId: String, from: String, to: String, status: String, currentStatus: String) {
        if (storeSelected != null) {
            callApi(Service.callPoService()
                    .findPo(storeSelected.getId(), distributorId, searchComponent.code.get()!!, from, to, status, currentStatus,
                            currentPageNumber, AppUtils.PAGE_SIZE)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<Pageable<Po>>(appException, Pageable::class.java,
                            Po::class.java, null) {
                        override fun getDataSuccess(page: Pageable<Po>?) {
                            setPage(page)
                            setData(page!!.content as List<Po>?)
                        }

                        override fun onFailure(errorRespone: ErrorRespone?) {
                            Log.e(ERROR_PROCESS, errorRespone!!.message)
                        }
                    }))
        }
    }

    fun searchByCode(code: String) {
        searchComponent.setData(code)
        search()
    }

    fun setDate(isFromDate: Boolean?, year: Int, month: Int, dayOfMonth: Int) {
        if (isFromDate!!) {
            chooseDate.fromDate.set(DateUtils.convertDayMonthYearToDate(dayOfMonth, month, year))
        } else {
            chooseDate.toDate.set(DateUtils.convertDayMonthYearToDate(dayOfMonth, month, year))
        }

    }
}