package com.next_solutions.logistics.models.dao;

import com.next_solutions.logistics.models.entity.ProductPackingEntity;
import com.next_solutions.logistics.models.view.ProductDescriptionView;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface ProductPackingDao extends BaseDao<ProductPackingEntity> {
    @Query("SELECT pp.id AS packingProductId, pd.code AS productCode, pd.name AS productName  " +
            " FROM packing_product pp" +
            " JOIN product p ON p.id = pp.product_id" +
            " JOIN product_description pd ON pd.product_id" +
            " JOIN packing_type pt ON pt.id = pp.packing_type_id" +
            " WHERE pd.lang_id = :langId AND p.id NOT IN (:exceptId)" +
            " LIMIT :limit OFFSET :offset")
    LiveData<List<ProductDescriptionView>> findProduct(Long langId, List<Long> exceptId, Integer limit, Integer offset);
}
