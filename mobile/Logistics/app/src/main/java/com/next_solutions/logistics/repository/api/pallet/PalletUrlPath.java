package com.next_solutions.logistics.repository.api.pallet;

public interface PalletUrlPath {
    String FIND_PALLET = "/pallets";
    String FIND_ALL_PALLET = "/pallets/all";
    String FIND_INVENTORY_PALLET = "/pallets/group-by-pallet-product-packing-expire-date";
}
