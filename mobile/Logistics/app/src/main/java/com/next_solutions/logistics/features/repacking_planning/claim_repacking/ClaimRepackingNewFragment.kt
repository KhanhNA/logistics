package com.next_solutions.logistics.features.repacking_planning.claim_repacking

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimFragment
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.utils.Constants.MODEL

class ClaimRepackingNewFragment : BaseClaimFragment() {

    private lateinit var claimRepackingNewViewModel: ClaimRepackingNewViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimRepackingNewViewModel = viewModel as ClaimRepackingNewViewModel
        getData()
        claimRepackingNewViewModel.init()
        return binding.root
    }

    private fun getData() {
        val repackingPlanning = activity?.intent?.getSerializableExtra(MODEL)
        claimRepackingNewViewModel.repackingPlanning = repackingPlanning as RepackingPlanning
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val dialog = ChooseRepackingDetailDialog()
            dialog.show((activity as FragmentActivity).supportFragmentManager, "")
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_claim_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimRepackingNewViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_repacking
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}