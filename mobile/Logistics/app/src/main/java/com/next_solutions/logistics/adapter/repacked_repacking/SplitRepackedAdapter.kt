package com.next_solutions.logistics.adapter.repacked_repacking

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.databinding.SplitRepackItemBinding
import com.next_solutions.logistics.features.repacking_planning.PalletDialog
import com.next_solutions.logistics.features.repacking_planning.repacked_statement.RepackedRepackingPlanningViewModel
import com.next_solutions.logistics.models.Pallet
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.dto.ImportStatementDetailDTO
import com.next_solutions.logistics.models.dto.SplitRepack
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath
import com.next_solutions.logistics.utils.DialogCallBack
import com.next_solutions.logistics.utils.RecycleViewCallBack


class SplitRepackedAdapter internal constructor(private val detailDTO: ImportStatementDetailDTO,
                                                private val parentFragmentManager: FragmentManager,
                                                private val viewModel: RepackedRepackingPlanningViewModel) : RecyclerView.Adapter<SplitRepackedAdapter.ImportDetailViewHolder>(), RecycleViewCallBack, DialogCallBack {

    private var notifyPosition = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImportDetailViewHolder {
        val binding = DataBindingUtil.inflate<SplitRepackItemBinding>(LayoutInflater.from(parent.context),
                R.layout.split_repack_item
                , parent, false)
        return ImportDetailViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ImportDetailViewHolder, position: Int) {
        val importStatementDetail = detailDTO.splitRepacks[position]
        holder.bind(importStatementDetail, viewModel, this)
    }

    override fun getItemCount(): Int {
        return detailDTO.splitRepacks.size
    }

    override fun onItemClick(view: View, obj: Any) {
        val detail = obj as SplitRepack
        val eventPosition = detailDTO.splitRepacks.indexOf(detail)
        when (view.id) {
            R.id.pallet -> {
                notifyPosition = detailDTO.splitRepacks.indexOf(detail)
                showDialogPalletPicker()
            }
            R.id.delete_detail -> {
                removeRow(eventPosition, detailDTO.splitRepacks.size, obj)
            }
            R.id.add_detail -> {
                notifyPosition = eventPosition
                addRow(eventPosition)
            }
        }
    }

    private fun addRow(eventPosition: Int) {
        val addPosition = eventPosition + 1
        detailDTO.splitRepacks
                .add(addPosition, SplitRepack().apply {
                    quantity = 0
                    pallet = Pallet()
                    productPackingCode = detailDTO.productPackingCode
                    expireDate = detailDTO.expireDate
                })
        notifyItemInserted(addPosition)
    }

    private fun removeRow(removePosition: Int, size: Int, splitRepack: SplitRepack) {
        if (removePosition == 0 && size == 1) {
            return
        }
        notifyItemRemoved(removePosition)
        detailDTO.splitRepacks.removeAt(removePosition)
        viewModel.reCalculator(splitRepack.quantity * -1, splitRepack)
    }

    private fun showDialogPalletPicker() {
        if (RuleHelper().hasRule(HttpRequest(Method.POST.value, ImportStatementUrlPath.CREATE_IMPORT_STATEMENT_FROM_REPACKING_PLANNING))) {
            val dialog = PalletDialog()
            dialog.callBack = this
            dialog.show(parentFragmentManager, "")
        }
    }

    inner class ImportDetailViewHolder(var binding: SplitRepackItemBinding) : RecyclerView.ViewHolder(binding.root) {
        var quantity: EditText = binding.root.findViewById(R.id.quantity)
        fun bind(splitRepack: SplitRepack, viewModel: RepackedRepackingPlanningViewModel, viewCallBack: RecycleViewCallBack?) {
            binding.viewHolder = splitRepack
            binding.viewModel = viewModel
            binding.listener = viewCallBack
        }

    }

    override fun onCancel() {
    }

    override fun callBack(obj: Any) {
        val pallet = obj as Pallet
        detailDTO.splitRepacks[notifyPosition].pallet = Pallet().apply {
            id = pallet.id
            code = pallet.code
            displayName = pallet.displayName
        }
        notifyItemChanged(notifyPosition)
    }

}