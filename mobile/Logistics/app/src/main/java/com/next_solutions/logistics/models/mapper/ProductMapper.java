package com.next_solutions.logistics.models.mapper;

import com.next_solutions.logistics.models.Product;
import com.next_solutions.logistics.models.entity.ProductEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );
    ProductEntity productDtoToProduct(Product product);
}
