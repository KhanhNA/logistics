package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportPoDetailPallet extends Base {


    private ImportPo importPo;
    private ImportPoDetail importPoDetail;
    private Pallet pallet;
    private PalletDetail palletDetail;
    private Long quantity;

    public ImportPoDetailPallet() {
    }

    public ImportPoDetailPallet(Long quantity, Pallet pallet, ImportPo importPo, ImportPoDetail importPoDetail) {
        this.quantity = quantity;
        this.pallet = pallet;
        this.importPo = importPo;
        this.importPoDetail = importPoDetail;
    }

    public ImportPo getImportPo() {
        return importPo;
    }

    public void setImportPo(ImportPo importPo) {
        this.importPo = importPo;
    }

    public ImportPoDetail getImportPoDetail() {
        return importPoDetail;
    }

    public void setImportPoDetail(ImportPoDetail importPoDetail) {
        this.importPoDetail = importPoDetail;
    }

    public Pallet getPallet() {
        return pallet;
    }

    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }

    public PalletDetail getPalletDetail() {
        return palletDetail;
    }

    public void setPalletDetail(PalletDetail palletDetail) {
        this.palletDetail = palletDetail;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}