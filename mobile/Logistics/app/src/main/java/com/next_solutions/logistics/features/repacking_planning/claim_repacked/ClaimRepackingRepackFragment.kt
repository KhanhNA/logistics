package com.next_solutions.logistics.features.repacking_planning.claim_repacked

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimFragment
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.utils.Constants

class ClaimRepackingRepackFragment : BaseClaimFragment() {

    private lateinit var claimRepackingRepackViewModel: ClaimRepackingRepackViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimRepackingRepackViewModel = viewModel as ClaimRepackingRepackViewModel
        getData()
        claimRepackingRepackViewModel.init()
        return binding.root
    }

    private fun getData() {
        val repackingPlanning = activity?.intent?.getSerializableExtra(Constants.MODEL)
        claimRepackingRepackViewModel.repackingPlanning = repackingPlanning as RepackingPlanning
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val dialog = ChooseRepackedDetailDialog()
            dialog.show((activity as FragmentActivity).supportFragmentManager, "")
        }
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_claim_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimRepackingRepackViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_repacking_repack
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}