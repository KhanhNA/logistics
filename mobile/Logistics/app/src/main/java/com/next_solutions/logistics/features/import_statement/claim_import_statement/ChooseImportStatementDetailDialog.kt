package com.next_solutions.logistics.features.import_statement.claim_import_statement

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ChooseImportStatementDetailDialog : BaseDialogFragment() {

    private lateinit var chooseImportStatementDetailViewModel: ChooseImportStatementDetailViewModel
    private lateinit var claimImportStatementViewModel: ClaimImportStatementViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        claimImportStatementViewModel = ViewModelProvider(baseActivity,
                BaseViewModelFactory.getInstance<Any>(baseActivity.application, ClaimImportStatementViewModel::class.java))
                .get(ClaimImportStatementViewModel::class.java)
        chooseImportStatementDetailViewModel = viewModel as ChooseImportStatementDetailViewModel
        setUpRecycleView()
        chooseImportStatementDetailViewModel.init(claimImportStatementViewModel)
        return binding.root
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (Constants.SCAN_CODE == view!!.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            val result = data!!.getStringExtra(Constants.RESULT_SCAN)
            chooseImportStatementDetailViewModel.searchComponent.setData(result)
            chooseImportStatementDetailViewModel.search()
        }
        closeProcess()
    }

    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_import_statement_detail_item,
                chooseImportStatementDetailViewModel, OwnerView { _, detail ->
            run {
                claimImportStatementViewModel.addDetail(detail as ImportStatementDetail)
                dismiss()
            }
        }, baseActivity)
        recyclerView.adapter = baseAdapter
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChooseImportStatementDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_import_statement_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.details
    }

}