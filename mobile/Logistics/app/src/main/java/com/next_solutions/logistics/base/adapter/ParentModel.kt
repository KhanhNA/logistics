package com.next_solutions.logistics.base.adapter

import com.next_solutions.base.BaseModel
import com.next_solutions.logistics.models.Product
import com.next_solutions.logistics.models.ProductPacking
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING
import com.next_solutions.logistics.utils.DateUtils
import com.next_solutions.logistics.utils.DateUtils.DATE_FORMAT
import com.next_solutions.logistics.utils.DateUtils.DATE_SHOW_FORMAT

class ParentModel : BaseModel() {
    var data: List<BaseModel>? = null
    var productPacking: ProductPacking? = null
    var quantity: Long? = null
    var expireDate: String? = null
    var product: Product? = null
    var claimQuantity: Long? = null

    fun getExpireDateDisplay(): String {
        return if (expireDate == null) EMPTY_STRING else DateUtils.convertStringDateToDifferentType(expireDate, DATE_FORMAT, DATE_SHOW_FORMAT)
    }
}