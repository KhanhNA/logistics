package com.next_solutions.logistics.features.test;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.next_solutions.base.BaseViewModel;


public class ListOrderActivityViewModel extends BaseViewModel {
    private ObservableField<Integer> errorPosition = new ObservableField<>();

    public ListOrderActivityViewModel(@NonNull Application application) {
        super(application);
    }

}
