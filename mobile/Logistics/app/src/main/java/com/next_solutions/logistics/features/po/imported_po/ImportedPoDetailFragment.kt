package com.next_solutions.logistics.features.po.imported_po

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.models.Po
import com.next_solutions.logistics.utils.Constants.*

class ImportedPoDetailFragment : BaseRecycleViewFragment() {
    private lateinit var importedPoViewModel: ImportedPoDetailViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        importedPoViewModel = viewModel as ImportedPoDetailViewModel
        getData()
        importedPoViewModel.init()
        return binding.root
    }

    private fun getData() {
        if (activity != null && activity!!.intent.hasExtra(MODEL)) {
            val po = activity!!.intent.getSerializableExtra(MODEL) as Po
            importedPoViewModel.po = po
        }
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.btnViewClaim) {
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            val claim = importedPoViewModel.claimDetails[0].claim
            claim.claimDetails = importedPoViewModel.claimDetails
            bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
            bundle.putSerializable(MODEL, claim)
            bundle.putSerializable(MODEL_PO, importedPoViewModel.po)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_imported_po_detail_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> run {} }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ImportedPoDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_imported_po_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}