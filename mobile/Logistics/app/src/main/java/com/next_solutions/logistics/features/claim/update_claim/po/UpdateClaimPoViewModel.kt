package com.next_solutions.logistics.features.claim.update_claim.po

import android.app.Application
import android.util.Log
import com.google.gson.Gson
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.base.claim.po.BaseClaimPoViewModel
import com.next_solutions.logistics.config.AppUtils.APPLICATION_JSON
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.ClaimObject
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class UpdateClaimPoViewModel(application: Application) : BaseClaimPoViewModel(application) {
    var claim: Claim? = null

    private var deleteImage = arrayListOf<ClaimImage>()

    override fun init() {
        claim?.let {
            descriptionObservable.set(it.description)
            amenableObservable.set(it.amenable)
            descriptionObservable.notifyChange()
            amenableObservable.notifyChange()
        }
        getClaimDetail()
    }

    private fun getClaimDetail() {
        claim?.let {
            callApi(Service
                    .callClaimService()
                    .findClaimById(it.id)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<Claim>(appException, Claim::class.java, null) {
                        override fun getDataSuccess(claimStatement: Claim?) {
                            getClaimSuccess(claimStatement)
                        }

                        override fun onFailure(errorRespone: ErrorRespone?) {
                            Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                        }
                    }))
        }
    }

    private fun getClaimSuccess(claimStatement: Claim?) {
        claimStatement?.let {
            this.claim = claimStatement
            getReferencePoStatement()
            images.addAll(claimStatement.claimImages.asSequence().map {
                Image().apply {
                    claimImage = ClaimImage().apply {
                        id = it.id
                        claim = Claim().apply { id = it.claim.id }
                        description = it.description
                        url = it.url
                    }
                }
            }.toList())
            sendBackAction(RELOAD_IMAGE)
        }
    }

    private fun getReferencePoStatement() {
        callApi(Service
                .callPoService()
                .findPoById(claim!!.referenceId)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Po>(appException, Po::class.java, null) {
                    override fun getDataSuccess(poStatement: Po) {
                        getReferencePoStatementSuccess(poStatement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                    }

                }))
    }

    private fun getReferencePoStatementSuccess(poStatement: Po) {
        this.po = poStatement
        groupDetailByProductPacking()
    }

    private fun groupDetailByProductPacking() {
        val totalQuantity = currentTotalQuantity.get()
        val group = HashMap<String, ClaimObject>()
        claim!!.claimDetails.forEach {
            val productPacking = it.productPacking
            val key = productPacking.id.toString()
            val claimDetail = claimHelper.minimizeClaimDetailWithQuantity(it)
            if (group.containsKey(key)) {
                val claimDetails = group[key]!!.claimDetails
                claimDetails.add(claimDetail)
                val quantity = totalQuantity?.get(getCodeMapping(claimDetail))?.plus(claimDetail.quantity)
                quantity?.let {
                    totalQuantity[getCodeMapping(claimDetail)] = quantity
                }
            } else {
                val claimObject = claimHelper.convertClaimDetailToClaimObject(it)

                val maxQuantity = getMaxQuantityDetail(claimObject)

                claimObject.quantity = maxQuantity
                claimObject.claimDetails.add(claimDetail)
                group[key] = claimObject

                totalQuantity!![getCodeMapping(claimObject)] = claimDetail.quantity
            }
        }
        listData.addAll(group.values)
        sendBackAction(NOTIFY_CHANGE)
    }

    private fun getMaxQuantityDetail(claimObject: ClaimObject): Long {
        val poDetailMapping = po.poDetails.asSequence().find { it.productPacking.id == claimObject.productPackingId }
        poDetailMapping?.let {
            return poDetailMapping.quantity
        }
        return 0
    }

    override fun prepareClaim(): Claim {
        val details = arrayListOf<ClaimDetail>()
        listData.forEach { detail: ClaimObject ->
            run {
                val groupByExpiryDate = HashMap<String, ClaimDetail>()
                detail.claimDetails.forEach { claimDetail ->
                    run {
                        val key = claimDetail.expireDate
                        if (groupByExpiryDate.containsKey(key)) {
                            groupByExpiryDate[key]!!.quantity = groupByExpiryDate[key]!!.quantity + claimDetail.quantity
                        } else {
                            groupByExpiryDate[key] = claimDetail
                        }
                    }
                }
                details.addAll(groupByExpiryDate.values)
            }
        }
        return Claim().apply {
            id = claim!!.id
            type = Claim.ClaimType.IMPORT_PO
            description = descriptionObservable.get()
            referenceId = po.id
            claimDetails = details
            store = Store().apply {
                code = po.store.code
                id = po.store.id
            }
            status = Claim.ClaimStatus.WAIT_APPROVE.value
            amenable = amenableObservable.get()
        }
    }

    fun addClaimDetail(poDetail: PoDetail) {
        val minimizeDetail = claimHelper.convertPoDetailToClaimObject(poDetail)
        notifyPosition.set(listData.size)
        listData.add(minimizeDetail)
        currentTotalQuantity.get()?.let {
            currentTotalQuantity.get()!![getCodeMapping(minimizeDetail)] = 0
        }
        sendBackAction(NOTIFY_CHANGE_ITEM_ADD)
    }

    fun updateClaim() {
        if (validate()) {
            val claim = prepareClaim()
            if (claim.id != null) {
                callApiUpdateClaim(claim)

            }

        }
    }

    private fun callApiUpdateClaim(claim: Claim) {
        callApi(Service.callClaimService().updateClaim(claim.id, claim)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(t: ResponseStatus?) {
                        sendBackAction(UPDATE_SUCCESS)
                        uploadImage(claim)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message)
                    }

                }))
    }


    override fun uploadImage(claim: Claim) {
        for (image in images) {
            image.claimImage.claim = Claim().apply { id = claim.id }
        }

        val imagePaths = images.asSequence().filter { it.isLocalImage }.map { it.albumFile.path }.toList()

        val jsonAdd = Gson().toJson(images.asSequence().map { it.claimImage }.filter { it.id == null }.toList())
        val bodyJsonAdd = RequestBody.create(MediaType.parse(APPLICATION_JSON), jsonAdd)

        val jsonEdit = Gson().toJson(images.asSequence().map { it.claimImage }.filter { it.id != null }.toList())
        val bodyJsonEdit = RequestBody.create(MediaType.parse(APPLICATION_JSON), jsonEdit)

        val jsonDelete = Gson().toJson(deleteImage)
        val bodyJsonDelete = RequestBody.create(MediaType.parse(APPLICATION_JSON), jsonDelete)

        val parts = arrayListOf<MultipartBody.Part>()
        imagePaths.forEach { path: String ->
            run {
                val file = File(path)
                val fileReqBody = RequestBody.create(MediaType.parse("image/*"), file)
                parts.add(MultipartBody.Part.createFormData("files", file.name, fileReqBody))
            }
        }

        callApi(Service.callClaimService()
                .uploadImage(bodyJsonAdd, bodyJsonDelete, bodyJsonEdit, parts, claim.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(responseStatus: ResponseStatus?) {
                        uploadImageSuccess()
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                    }
                }))
    }

}