package com.next_solutions.logistics.features.inventory;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.Pageable;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.component.distributor.DistributorComponent;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.Distributor;
import com.next_solutions.logistics.models.Inventory;
import com.next_solutions.logistics.models.Rule;
import com.next_solutions.logistics.models.Store;
import com.next_solutions.logistics.repository.Service;
import com.next_solutions.logistics.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

@SuppressWarnings("unchecked")
public class InventoryViewModel extends BaseViewModel {
    public final ObservableField<String> code = new ObservableField<>("");
    private ObservableList<Store> stores = new ObservableArrayList<>();
    private Inventory inventory = new Inventory();
    private Integer selectedFromStorePosition = 0;
    private DistributorComponent distributorComponent = new DistributorComponent(this, true);

    public DistributorComponent getDistributorComponent() {
        return distributorComponent;
    }

    public void setDistributorComponent(DistributorComponent distributorComponent) {
        this.distributorComponent = distributorComponent;
    }

    public InventoryViewModel(@NonNull Application application) {
        super(application);
    }

    public int setBackGround(Inventory inventory) {
        if (inventory.getClassName() == null) {
            return AppUtils.getInstance().getResources().getColor(R.color.white);
        } else {
            switch (inventory.getClassName()) {
                case Constants.YELLOW:
                    return AppUtils.getInstance().getResources().getColor(R.color.light_orange);
                case Constants.ORANGE:
                    return AppUtils.getInstance().getResources().getColor(R.color.LemonChiffon);
                case Constants.RED:
                    return AppUtils.getInstance().getResources().getColor(R.color.light_red_2);
                default:
                    return AppUtils.getInstance().getResources().getColor(R.color.white);
            }
        }

    }

    @Override
    public void init() {
        selectedFromStorePosition = 0;
        clearDataRecycleView();
        distributorComponent.resetData();
        getFromStore();
    }

    @Override
    public void search() {
        if (stores != null && !stores.isEmpty() && !distributorComponent.getDistributors().isEmpty()) {
            Store store = stores.get(selectedFromStorePosition);
            Distributor distributor = distributorComponent.getSelectedDistributor();
            Integer page = getCurrentPageNumber();
            ArrayList<Long> arrExceptionProductPackingId = new ArrayList<>();
            if (store != null) {
                // add mặc định exceptionId = -1
                long exceptionId = -1L;
                arrExceptionProductPackingId.add(exceptionId);
                if (distributor != null) {
                    getInventoryFromAPI(distributor, page, arrExceptionProductPackingId, store);
                }
            }
        }
    }

    private void getFromStore() {
        ArrayList<Long> exceptIds = new ArrayList<>();
        exceptIds.add(-1L);
        callApi(Service.callStoreService()
                .getFromStore(exceptIds, false, AppUtils.FIRST_PAGE, Integer.MAX_VALUE, true, "")
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<Pageable<Store>>(appException, Pageable.class, Store.class, null) {
                    @Override
                    public void getDataSuccess(Pageable<Store> storePageable) {
                        getStoreSuccess(storePageable);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    private void getStoreSuccess(Object body) {
        if (body != null) {
            Pageable<Store> pageable = (Pageable<Store>) body;
            this.stores.clear();
            this.stores.addAll(pageable.getContent());

            if (canViewDCInventory()) {
                addDCToListStore();
            }
        } else {
            sendBackAction(Constants.ERROR_PROCESS);
        }
    }

    private void addDCToListStore() {
        List<Long> storeIds = getStoreFCIds();
        callApi(Service.callStoreService().getStoreDC(storeIds)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<List<Store>>(appException, Store.class, List.class) {
                    @Override
                    public void getDataSuccess(List<Store> stores) {
                        getStoreDCSuccess(stores);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    private List<Long> getStoreFCIds() {
        ArrayList<Long> storeIds = new ArrayList<>();
        for (Store store : stores) {
            storeIds.add(store.getId());
        }
        return storeIds;
    }

    private void getStoreDCSuccess(List<Store> stores) {
        this.stores.addAll(stores);
        search();
    }

    private void getInventoryFromAPI(Distributor distributor, Integer page, ArrayList<Long> exceptIds, Store store) {
        callApi(Service.callInventoryService()
                .getInventory(distributor.getId(), page, AppUtils.PAGE_SIZE, exceptIds, store.getId(), code.get())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<Pageable<Inventory>>(appException, Pageable.class, Inventory.class, null) {
                    @Override
                    public void getDataSuccess(Pageable<Inventory> pageable) {
                        searchSuccess(pageable);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    private void searchSuccess(Pageable<Inventory> pageable) {
        if (pageable != null) {
            setPage(pageable);
            setData(pageable.getContent());
        } else {
            sendBackAction(ERROR_PROCESS);
        }
    }

    private boolean canViewDCInventory() {
        int size = AppUtils.rule.getUserAuthentication().getPrincipal().getRoles().size();
        for (int i = 0; i < size; i++) {
            String roleName = AppUtils.rule.getUserAuthentication().getPrincipal().getRoles().get(i).getRoleName();
            if (roleName.equals(Rule.RuleType.TKFC.getValue())) {
                return true;
            }
        }
        return false;
    }

    public ObservableField<String> getCode() {
        return code;
    }

    public ObservableList<Store> getStores() {
        return stores;
    }

    public void setStores(ObservableList<Store> stores) {
        this.stores = stores;
    }

    public Inventory getInventory() {
        return inventory;
    }

    void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Integer getSelectedFromStorePosition() {
        return selectedFromStorePosition;
    }

    public void setSelectedFromStorePosition(Integer selectedFromStorePosition) {
        this.selectedFromStorePosition = selectedFromStorePosition;
        search();
    }

    Long getSelectedStoreId() {
        return stores.get(selectedFromStorePosition).getId();

    }

    Store getSelectedStore() {
        return stores.get(selectedFromStorePosition);
    }
}
