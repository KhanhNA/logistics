package com.next_solutions.logistics.models.dao;

import com.next_solutions.logistics.models.entity.PalletEntity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface PalletDao extends BaseDao<PalletEntity>{
    @Query("SELECT p.* from pallet p LIMIT :limit OFFSET :offset")
    LiveData<List<PalletEntity>> getPalletByPage(int limit, int offset);

    @Query("SELECT p.* from pallet p")
    LiveData<List<PalletEntity>> getAllPallet();

    @Query("DELETE FROM pallet")
    void deleteAll();
}
