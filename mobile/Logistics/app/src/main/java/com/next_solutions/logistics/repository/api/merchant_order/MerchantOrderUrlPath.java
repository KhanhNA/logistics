package com.next_solutions.logistics.repository.api.merchant_order;

public interface MerchantOrderUrlPath {
    String GET_ORDERS = "/merchant-orders/list_po";

    String GET_INVENTORY_LEFT = "/merchant-orders/get-inventory";
}
