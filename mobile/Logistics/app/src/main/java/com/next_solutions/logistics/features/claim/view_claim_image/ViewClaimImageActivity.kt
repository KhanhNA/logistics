package com.next_solutions.logistics.features.claim.view_claim_image

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.ImageModel
import com.next_solutions.logistics.utils.Constants


@Suppress("UNCHECKED_CAST")
class ViewClaimImageActivity : AppCompatActivity() {
    var images: Array<ImageModel>? = null
    private var currentPosition: Int? = 0

    private lateinit var viewPager: ViewPager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_claim_image)
        getData()
        setUpViewPager()
        setUpCallBackUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpCallBackUp() {
        val myToolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(myToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun setUpViewPager() {
        viewPager = findViewById(R.id.view_pager)
        val fullSizeAdapter = FullSizeAdapter(this, images!!)
        viewPager.adapter = fullSizeAdapter
        currentPosition?.let {
            viewPager.setCurrentItem(currentPosition!!, true)
        }
    }

    private fun getData() {
        images = intent?.extras?.get(Constants.MODEL) as Array<ImageModel>
        currentPosition = intent?.extras?.get(Constants.POSITION) as Int?
    }
}