package com.next_solutions.logistics.models.dto;

import androidx.annotation.NonNull;

public class HttpRequest {
    public String url;
    public String method;

    public HttpRequest(String method, String url) {
        this.url = url;
        this.method = method;
    }

    @NonNull
    @Override
    public String toString() {
        return method.toLowerCase() + url.toLowerCase();
    }
}
