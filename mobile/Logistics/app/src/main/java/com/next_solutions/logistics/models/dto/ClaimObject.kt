package com.next_solutions.logistics.models.dto

import com.next_solutions.base.BaseModel
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.utils.DateUtils

class ClaimObject : BaseModel() {
    lateinit var claimDetails: ArrayList<ClaimDetail>
    lateinit var productPackingName: String
    lateinit var productPackingCode: String
    var productPackingId: Long = 0
    lateinit var productPackingUom: String
    var quantity: Long = 0
    var productPackingType: Long = 0
    var expireDate: String = ""
    fun getExpireDateDisplay(): String {
        return DateUtils.convertStringDateToDifferentType(this.expireDate, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT)
    }
}
