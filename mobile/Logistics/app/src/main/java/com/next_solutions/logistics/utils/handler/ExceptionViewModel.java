package com.next_solutions.logistics.utils.handler;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.next_solutions.base.ErrorRespone;
import com.next_solutions.logistics.config.ResponseStatus;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.repository.Service;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

public class ExceptionViewModel extends AndroidViewModel {

    protected MutableLiveData<Throwable> appException;
    String errorMessage = EMPTY_STRING;
    private CompositeDisposable mCompositeDisposable;

    ExceptionViewModel(@NonNull Application application) {
        super(application);
    }

    public void callApi(Disposable disposable) {
        if (this.mCompositeDisposable == null) {
            this.mCompositeDisposable = new CompositeDisposable();
        }

        this.mCompositeDisposable.add(disposable);
    }

    public void sendErrorMessage() {
        callApi(Service.callErrorService()
                .sendError(errorMessage)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    @Override
                    public void getDataSuccess(ResponseStatus responseStatus) {

                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {

                    }
                }));
    }
}