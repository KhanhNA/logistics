package com.next_solutions.logistics.config;


public class ResponseStatus {
    public ResponseStatus(Integer code) {
        this.code = code;
    }

    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
