package com.next_solutions.logistics.features.import_statement.claim_import_statement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimFragment
import com.next_solutions.logistics.models.ImportStatement
import com.next_solutions.logistics.utils.Constants.MODEL

class ClaimImportStatementFragment : BaseClaimFragment() {
    private lateinit var claimImportStatementViewModel: ClaimImportStatementViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimImportStatementViewModel = viewModel as ClaimImportStatementViewModel
        getData()
        claimImportStatementViewModel.init()
        return binding.root
    }

    private fun getData() {
        val importStatement = activity?.intent?.getSerializableExtra(MODEL)
        claimImportStatementViewModel.importStatement = importStatement as ImportStatement
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val dialog = ChooseImportStatementDetailDialog()
            dialog.show((activity as FragmentActivity).supportFragmentManager, "")
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_claim_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimImportStatementViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_import_statement
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }
}