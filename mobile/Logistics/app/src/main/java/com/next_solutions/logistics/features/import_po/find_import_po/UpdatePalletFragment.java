package com.next_solutions.logistics.features.import_po.find_import_po;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.CommonActivity;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.models.ImportPo;
import com.next_solutions.logistics.models.dto.HttpRequest;
import com.next_solutions.logistics.models.helper.FragmentHelper;
import com.next_solutions.logistics.models.helper.RuleHelper;
import com.next_solutions.logistics.repository.api.Method;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;

import static com.next_solutions.logistics.repository.api.import_po.ImportPoUrlPath.FIND_BY_ID;
import static com.next_solutions.logistics.utils.Constants.FRAGMENT;
import static com.next_solutions.logistics.utils.Constants.MODEL;
import static com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN;
import static com.next_solutions.logistics.utils.Constants.SCAN_CODE;

public class UpdatePalletFragment extends BaseFragment {
    private UpdatePalletViewModel updatePalletViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        updatePalletViewModel = (UpdatePalletViewModel) viewModel;
        setUpRecycleView();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updatePalletViewModel.init();
    }

    private void setUpRecycleView() {
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_update_pallet_item, viewModel, (view, baseModel) -> {
            if (new RuleHelper().hasRule(new HttpRequest(Method.GET.getValue(), FIND_BY_ID))) {
                ImportPo importPo = (ImportPo) baseModel;
                Intent intent = new Intent(getContext(), CommonActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(MODEL, importPo);
                bundle.putSerializable(FRAGMENT, new FragmentHelper().getFragmentType(importPo));
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }, getBaseActivity());
        if (recyclerView != null) {
            recyclerView.setAdapter(baseAdapter);
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        String tag = (String) view.getTag();
        if (SCAN_CODE.equals(tag)) {
            Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
            startActivityForResult(qrScan, REQUEST_CODE_SCAN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (isScanCodeAndResultOk(requestCode, resultCode) &&
                data != null && data.hasExtra(Constants.RESULT_SCAN)) {
            String result = data.getStringExtra(Constants.RESULT_SCAN);
            updatePalletViewModel.getSearchComponent().setData(result);
            updatePalletViewModel.onClickButtonSearch();
        }
        closeProcess();
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK;
    }

    @Override
    public void onResume() {
        super.onResume();
        updatePalletViewModel.onClickButtonSearch();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_update_pallet;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return UpdatePalletViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.importPos;
    }
}
