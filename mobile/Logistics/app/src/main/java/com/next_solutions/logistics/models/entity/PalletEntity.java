package com.next_solutions.logistics.models.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;


@Entity(tableName = "pallet", foreignKeys = {@ForeignKey(entity = StoreEntity.class,
        parentColumns = "id",
        childColumns = "store_id"), @ForeignKey(entity = PalletEntity.class,
        parentColumns = "id",
        childColumns = "parent_pallet_id")})
public class PalletEntity {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;

    @ColumnInfo(name = "store_id")
    private Long storeId;

    @ColumnInfo(name = "code")
    private String code;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "step")
    private Integer step;

    @ColumnInfo(name = "parent_pallet_id", index = true)
    private Long parentPalletId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public Long getParentPalletId() {
        return parentPalletId;
    }

    public void setParentPalletId(Long parentPalletId) {
        this.parentPalletId = parentPalletId;
    }
}
