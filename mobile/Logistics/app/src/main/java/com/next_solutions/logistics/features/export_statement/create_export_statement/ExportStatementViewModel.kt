package com.next_solutions.logistics.features.export_statement.create_export_statement

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.base.export_statement.BaseExportStatementViewModel
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.models.ExportStatementDetail
import com.next_solutions.logistics.models.Store
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants
import java.util.*

@Suppress("UNCHECKED_CAST")
class ExportStatementViewModel(application: Application) : BaseExportStatementViewModel(application) {

    var listFromStore: ObservableList<Store> = ObservableArrayList()
    var listToStore: ObservableList<Store> = ObservableArrayList()

    var selectedFromStorePosition = 0
        set(value) {
            field = value
            clearDataRecycleView()
            exceptId = arrayListOf()
            if (!listFromStore.isNullOrEmpty()) {
                fromStoreId = listFromStore[selectedFromStorePosition].id
                getToStore()
            }
        }
    var selectedToStorePosition = 0

    override fun init() {
        super.init()
        resetData()
    }

    override fun resetData() {
        descriptionObservable.set(Constants.EMPTY_STRING)
        selectedFromStorePosition = 0
        selectedToStorePosition = 0
        setData(ArrayList())
        getFromStores()
    }

    override fun prepareStatement(): List<ExportStatement> {
        val exportStatement = ExportStatement()
        setStoreForStatement(exportStatement)
        exportStatement.status = ExportStatement.ExportStatementStatus.REQUESTED.value
        exportStatement.exportStatementDetails = baseModelsE as MutableList<ExportStatementDetail>?
        exportStatement.description = descriptionObservable.get()
        exportStatement.merchantOrderIds = ArrayList()

        val exportStatements: MutableList<ExportStatement> = ArrayList()
        exportStatements.add(exportStatement)
        return exportStatements
    }

    override fun createOrUpdateExportStatement(exportStatements: List<ExportStatement>) {
        buttonEnable.set(false)
        callApi(Service.callExportStatementService()
                .createExportStatement(exportStatements)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ExportStatement>>(appException, ExportStatement::class.java, MutableList::class.java) {
                    override fun getDataSuccess(exportStatements: List<ExportStatement>) {
                        statementSuccess()
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        buttonEnable.set(true)
                    }
                }))
    }

    override fun statementSuccess() {
        buttonEnable.set(true)
        sendBackAction(Constants.CREATE_SUCCESS)
        resetData()
    }

    private fun setStoreForStatement(exportStatement: ExportStatement?) {
        val fromStore = listFromStore[selectedFromStorePosition]
        if (exportStatement != null && fromStore != null) {
            exportStatement.fromStoreId = fromStore.getId()
            exportStatement.fromStoreCode = fromStore.code
            exportStatement.toStoreId = listToStore[selectedToStorePosition].getId()
            exportStatement.toStoreCode = listToStore[selectedToStorePosition].code
        }
    }

    private fun getFromStores() {
        val exceptIds = ArrayList<Long>()
        exceptIds.add(-1L)
        callApi(Service.callStoreService()
                .getFromStore(exceptIds,
                        false,
                        AppUtils.FIRST_PAGE, Int.MAX_VALUE, true,
                        Constants.EMPTY_STRING)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Pageable<Store>>(appException, Pageable::class.java, Store::class.java, null) {
                    override fun getDataSuccess(storePageable: Pageable<Store>) {
                        successFromStore(storePageable)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message)
                    }
                }))
    }

    private fun successFromStore(storePageable: Pageable<Store>) {
        listFromStore.clear()
        listFromStore.addAll(storePageable.content)
    }

    private fun getToStore() {
        if (fromStoreId != null) {
            val exceptIds = ArrayList<Long>()
            exceptIds.add(-1L)
            exceptIds.add(fromStoreId!!)
            callApi(Service.callStoreService()
                    .getToStore(exceptIds, true, AppUtils.FIRST_PAGE, Int.MAX_VALUE, true, Constants.EMPTY_STRING)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<Pageable<Store>>(appException, Pageable::class.java, Store::class.java, null) {
                        override fun getDataSuccess(storePageable: Pageable<Store>) {
                            successToStore(storePageable)
                        }

                        override fun onFailure(errorRespone: ErrorRespone) {
                            Log.e(Constants.ERROR_PROCESS, errorRespone.message)
                        }
                    }))
        }
    }

    private fun successToStore(storePageable: Pageable<Store>) {
        listToStore.clear()
        listToStore.addAll(storePageable.content)
    }
}