package com.next_solutions.logistics.component.claim.type

import androidx.databinding.ObservableArrayList
import com.next_solutions.logistics.R
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.helper.ClaimRuleHelper
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING

class ClaimTypeComponent {
    private var types = ObservableArrayList<Claim.ClaimType>()
    var typesDisplay = ObservableArrayList<String>()
    var selectedTypePosition = 0

    init {
        prepareListType()
    }

    private fun prepareListType() {
        clearAllType()
        types.addAll(ClaimRuleHelper().getListTypeCanAccess())
        typesDisplay.add(AppUtils.getInstance().getString(R.string.all))
        types.forEach { type: Claim.ClaimType -> typesDisplay.add(type.valueDisplay) }
    }

    private fun clearAllType() {
        types.clear()
    }

    fun getSelectedType(): String {
        if (selectedTypePosition == 0) {
            return EMPTY_STRING
        }
        return types[selectedTypePosition - 1].value
    }

    fun resetData() {
        selectedTypePosition = 0
    }
}