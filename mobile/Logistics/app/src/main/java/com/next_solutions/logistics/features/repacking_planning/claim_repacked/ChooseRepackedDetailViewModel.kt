package com.next_solutions.logistics.features.repacking_planning.claim_repacked

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.models.RepackingPlanningDetailRepacked

@Suppress("UNCHECKED_CAST")
class ChooseRepackedDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    private lateinit var claimRepackViewModel: ClaimRepackingRepackViewModel
    private lateinit var repackingPlanning: RepackingPlanning
    var searchComponent = SearchComponent()

    fun init(claimRepackingNewViewModel: ClaimRepackingRepackViewModel) {
        this.claimRepackViewModel = claimRepackingNewViewModel
        this.repackingPlanning = claimRepackingNewViewModel.repackingPlanning
        searchComponent.resetData()
        search()
    }

    override fun search() {
        val claimDetails = claimRepackViewModel.baseModelsE as List<ClaimDetail>
        val searchResult = arrayListOf<RepackingPlanningDetailRepacked>()
        val codeSearch = searchComponent.getCodeSearch()
        claimRepackViewModel.repackingPlanning.repackingPlanningDetails.forEach { detail ->
            run {
                searchResult.addAll(
                        detail.repackingPlanningDetailRepackeds
                                .asSequence()
                                .filter { it.quantity != 0L && contain(claimDetails, it) }
                                .filter {
                                    it.productPacking.product.name.contains(codeSearch, true)
                                            || (it.productPacking.code.contains(codeSearch, true))
                                }
                                .toList())
            }
        }
        setData(searchResult as List<BaseModel>?)
    }

    private fun contain(details: List<ClaimDetail>, repacked: RepackingPlanningDetailRepacked): Boolean {
        return details.asSequence().find { claimDetail ->
            claimDetail.productPacking.id == repacked.productPacking.id
                    && claimDetail.expireDate == repacked.repackingPlanningDetail.expireDateStr

        } == null
    }
}