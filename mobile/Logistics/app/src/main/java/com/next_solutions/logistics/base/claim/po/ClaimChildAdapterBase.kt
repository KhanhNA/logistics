package com.next_solutions.logistics.base.claim.po

import android.app.DatePickerDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.logistics.BR
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.dto.ClaimObject
import com.next_solutions.logistics.utils.DateUtils
import com.next_solutions.logistics.utils.RecycleViewCallBack
import java.util.*

class ClaimChildAdapterBase(private var claimObject: ClaimObject,
                            private var layoutRecycleViewL2Id: Int,
                            private var context: Context,
                            private var poViewModel: BaseClaimPoViewModel) :
        RecyclerView.Adapter<ClaimChildAdapterBase.ClaimPoChildViewHolder>(), DatePickerDialog.OnDateSetListener {

    private var notifyPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClaimPoChildViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context),
                layoutRecycleViewL2Id, parent, false)
        return ClaimPoChildViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return claimObject.claimDetails.size
    }

    override fun onBindViewHolder(holder: ClaimPoChildViewHolder, position: Int) {

        val claimDetail = claimObject.claimDetails[position]
        holder.bind(claimDetail, poViewModel,
                RecycleViewCallBack { view, obj ->
                    run {
                        val detail = obj as ClaimDetail
                        when {
                            view!!.id == R.id.expire_date -> {
                                notifyPosition = claimObject.claimDetails.indexOf(detail)
                                showDialogDatePicker()
                            }
                            view.id == R.id.delete_detail -> {
                                removeRow(detail)
                            }
                            view.id == R.id.add_detail -> {
                                addRow(detail)
                            }
                        }
                    }
                })
    }

    private fun addRow(claimDetail: ClaimDetail) {
        val addPosition = claimObject.claimDetails.indexOf(claimDetail) + 1
        poViewModel.addRow(addPosition, claimObject)
        notifyItemInserted(addPosition)
    }

    private fun removeRow(claimDetail: ClaimDetail) {
        if (!claimObject.claimDetails.isNullOrEmpty() && claimObject.claimDetails.size > 1) {
            val removePosition = claimObject.claimDetails.indexOf(claimDetail)
            claimObject.claimDetails.removeAt(removePosition)
            notifyItemRemoved(removePosition)
            poViewModel.reCalculator(claimDetail.quantity * -1, claimDetail)
        }
    }

    private fun showDialogDatePicker() {
        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(context, this, year, month, day)
        datePickerDialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        claimObject.claimDetails[notifyPosition].expireDate =
                DateUtils.convertDayMonthYearToString(day, month, year, DateUtils.DATE_FORMAT)
        notifyItemChanged(notifyPosition)
    }

    class ClaimPoChildViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(claimDetail: ClaimDetail, poViewModel: BaseClaimPoViewModel,
                 adapterListener: RecycleViewCallBack) {
            binding.setVariable(BR.viewHolder, claimDetail)
            binding.setVariable(BR.listener, adapterListener)
            binding.setVariable(BR.viewModel, poViewModel)
        }

    }

}