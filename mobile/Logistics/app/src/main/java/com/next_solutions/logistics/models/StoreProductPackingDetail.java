package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class StoreProductPackingDetail extends Base{
    private StoreProductPacking storeProductPacking;
    private ProductPackingPrice productPackingPrice;
    private Long quantity;
    private Long originalQuantity;
    private String code;
    private String qRCode;

    public StoreProductPacking getStoreProductPacking() {
        return storeProductPacking;
    }

    public void setStoreProductPacking(StoreProductPacking storeProductPacking) {
        this.storeProductPacking = storeProductPacking;
    }

    public ProductPackingPrice getProductPackingPrice() {
        return productPackingPrice;
    }

    public void setProductPackingPrice(ProductPackingPrice productPackingPrice) {
        this.productPackingPrice = productPackingPrice;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getOriginalQuantity() {
        return originalQuantity;
    }

    public void setOriginalQuantity(Long originalQuantity) {
        this.originalQuantity = originalQuantity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getqRCode() {
        return qRCode;
    }

    public void setqRCode(String qRCode) {
        this.qRCode = qRCode;
    }
}
