package com.next_solutions.logistics.features.import_po.claim_import_po

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.ImportPoDetail
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ChooseImportPoDetailDialog : BaseDialogFragment() {
    private lateinit var chooseImportPoDetailViewModel: ChooseImportPoDetailViewModel
    private lateinit var claimImportPoViewModel: ClaimImportPoViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        claimImportPoViewModel = ViewModelProvider(baseActivity,
                BaseViewModelFactory.getInstance<Any>(baseActivity.application, ClaimImportPoViewModel::class.java))
                .get(ClaimImportPoViewModel::class.java)
        chooseImportPoDetailViewModel = viewModel as ChooseImportPoDetailViewModel
        setUpRecycleView()
        chooseImportPoDetailViewModel.init(claimImportPoViewModel)
        return binding.root
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (Constants.SCAN_CODE == view!!.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            val result = data!!.getStringExtra(Constants.RESULT_SCAN)
            chooseImportPoDetailViewModel.searchComponent.setData(result)
            chooseImportPoDetailViewModel.search()
        }
        closeProcess()
    }

    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_import_po_detail_item, chooseImportPoDetailViewModel,
                OwnerView { _, importPoDetail ->
                    run {
                        claimImportPoViewModel.addDetail(importPoDetail as ImportPoDetail)
                        dismiss()
                    }
                }, baseActivity)
        recyclerView.adapter = baseAdapter
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChooseImportPoDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_import_po_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.po_details
    }

}