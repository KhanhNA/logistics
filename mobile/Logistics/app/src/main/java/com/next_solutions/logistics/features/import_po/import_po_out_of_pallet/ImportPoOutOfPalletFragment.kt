package com.next_solutions.logistics.features.import_po.import_po_out_of_pallet

import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.import_po.BaseImportPoFragment

class ImportPoOutOfPalletFragment : BaseImportPoFragment() {
    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ImportPoOutOfPalletViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_import_po_out_of_pallet
    }

    override fun getRecycleResId(): Int {
        return R.id.details
    }

}