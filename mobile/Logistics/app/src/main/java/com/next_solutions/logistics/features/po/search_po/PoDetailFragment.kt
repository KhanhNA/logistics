package com.next_solutions.logistics.features.po.search_po

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.features.po.claim_po.ClaimPoFragment
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.Po
import com.next_solutions.logistics.utils.Constants.*

@Suppress("UNCHECKED_CAST")
class PoDetailFragment : BaseFragment() {
    private lateinit var poDetailViewModel: PoDetailViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        poDetailViewModel = viewModel as PoDetailViewModel
        getData()
        poDetailViewModel.init()
        setUpRecyclerView()
        return binding.root
    }

    private fun setUpRecyclerView() {
        val adapter = BaseAdapter(R.layout.fragment_po_detail_item, viewModel, OwnerView { _, _ -> }, baseActivity)
        recyclerView.adapter = adapter
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        run {
            when (view!!.id) {
                R.id.btnCreateClaim -> {
                    val intent = Intent(context, CommonActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable(MODEL, poDetailViewModel.po)
                    bundle.putSerializable(FRAGMENT, ClaimPoFragment::class.java)
                    intent.putExtras(bundle)
                    startActivityForResult(intent, CLAIM_ACTIVITY_CODE)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CLAIM_ACTIVITY_CODE && resultCode == Activity.RESULT_OK && data!!.hasExtra(CLAIM_ACTIVITY)) {
            val result = data.getStringExtra(CLAIM_ACTIVITY)!!
            if (result == CREATE_CLAIM_SUCCESS) {
                baseActivity.onBackPressed()
            }
        }
    }

    private fun getData() {
        if (activity != null) {
            if (activity!!.intent.hasExtra(MODEL)) {
                val po = activity!!.intent.getSerializableExtra(MODEL) as Po
                poDetailViewModel.po = po
            }
            if (activity!!.intent.hasExtra(CLAIM_DETAILS)) {
                val claimDetails = activity!!.intent.getSerializableExtra(CLAIM_DETAILS) as Array<Any>
                poDetailViewModel.claimDetails = arrayListOf()
                for (obj in claimDetails) {
                    poDetailViewModel.claimDetails!!.add(obj as ClaimDetail)
                }
                poDetailViewModel.hideButtonViewClaim()
            }
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return PoDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_po_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}