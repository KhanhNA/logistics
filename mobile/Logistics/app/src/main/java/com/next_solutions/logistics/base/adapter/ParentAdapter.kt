package com.next_solutions.logistics.base.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.BR
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.R
import com.next_solutions.logistics.utils.RecycleViewCallBack

class ParentAdapter(private var context: Context?,
                    private var viewModel: BaseViewModel<BaseModel>,
                    private var listData: List<ParentModel?>?,
                    private var parentLayoutId: Int,
                    private var parentCallBack: RecycleViewCallBack,
                    private var childLayoutId: Int,
                    private var childCallBack: RecycleViewCallBack) : RecyclerView.Adapter<ParentAdapter.ParentViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context),
                parentLayoutId, parent, false)
        return ParentViewHolder(binding)
    }

    override fun getItemCount(): Int {
        if (listData == null) {
            return 0
        }
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ParentViewHolder, position: Int) {
        if (listData != null && position < listData!!.size) {
            val dataParent = listData!![position]
            if (dataParent != null) {
                val childData = dataParent.data
                if (childData != null) {
                    val childAdapter = ChildAdapter(childData, childLayoutId, viewModel, childCallBack)

                    val recycledViewPool = RecyclerView.RecycledViewPool()

                    val manager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    val recyclerView = holder.binding.root.findViewById<RecyclerView>(R.id.children).apply {
                        adapter = childAdapter
                        layoutManager = manager
                    }
                    recyclerView.setRecycledViewPool(recycledViewPool)
                    holder.bind(dataParent, viewModel, parentCallBack)
                }
            }
        }
    }

    class ParentViewHolder(var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(detail: ParentModel?, baseViewModel: BaseViewModel<BaseModel>, callBack: RecycleViewCallBack) {
            binding.setVariable(BR.viewHolder, detail)
            binding.setVariable(BR.listener, callBack)
            binding.setVariable(BR.viewModel, baseViewModel)
        }
    }


}