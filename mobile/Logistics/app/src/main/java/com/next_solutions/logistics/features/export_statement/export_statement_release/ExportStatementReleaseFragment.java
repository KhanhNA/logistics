package com.next_solutions.logistics.features.export_statement.export_statement_release;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.CommonActivity;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.features.export_statement.update_export_statement.UpdateExportStatementFragment;
import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.models.dto.HttpRequest;
import com.next_solutions.logistics.models.helper.RuleHelper;
import com.next_solutions.logistics.repository.api.Method;
import com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath;
import com.next_solutions.logistics.utils.dialog_confirm.ConfirmDialog;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;
import com.next_solutions.logistics.widget.SwipeHelper;

import java.util.List;

import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.CANCEL_EXPORT_STATEMENT;
import static com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath.UPDATE_EXPORT_STATEMENT;
import static com.next_solutions.logistics.utils.Constants.FRAGMENT;
import static com.next_solutions.logistics.utils.Constants.MODEL;
import static com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN;
import static com.next_solutions.logistics.utils.Constants.RESULT_SCAN;
import static com.next_solutions.logistics.utils.Constants.SCAN_CODE;
import static com.next_solutions.logistics.utils.Constants.SUCCESS;
import static com.next_solutions.logistics.utils.Constants.TIME_AFTER_DELAY;

public class ExportStatementReleaseFragment extends BaseFragment {
    private ExportStatementReleaseViewModel releaseViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        releaseViewModel = (ExportStatementReleaseViewModel) viewModel;
        setUpRecycleView();
        enableSwipeToUpdate();
        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        releaseViewModel.init();
    }

    private void setUpRecycleView() {
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_create_export_statement_release_item, viewModel, (view, baseModel) -> {
            ExportStatement exportStatement = (ExportStatement) baseModel;
            Intent intent = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(MODEL, exportStatement);
            intent.putExtras(bundle);
            intent.putExtra(FRAGMENT, ExportStatementDetailReleaseFragment.class);
            startActivity(intent);
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
    }

    private void enableSwipeToUpdate() {
        Context context = getContext();
        if (context != null) {
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            SwipeHelper swipeHelper = new SwipeHelper(getContext(), recyclerView, width) {
                @Override
                public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                    if (new RuleHelper().hasRule(new HttpRequest(Method.PATCH.getValue(), ExportUrlPath.UPDATE_EXPORT_STATEMENT))) {
                        underlayButtons.add(new SwipeHelper.UnderlayButton(
                                getString(R.string.update),
                                position -> {
                                    if (releaseViewModel.isExportStatement(position) &&
                                            new RuleHelper().hasRule(new HttpRequest(Method.PATCH.getValue(), UPDATE_EXPORT_STATEMENT))) {
                                        Intent intent = new Intent(getContext(), CommonActivity.class);
                                        Bundle bundle = new Bundle();
                                        ExportStatement exportStatement = (ExportStatement) releaseViewModel.getBaseModelsE().get(position);
                                        bundle.putSerializable(MODEL, exportStatement);
                                        intent.putExtras(bundle);
                                        intent.putExtra(FRAGMENT, UpdateExportStatementFragment.class);
                                        startActivity(intent);
                                    } else {
                                        showAlertWarning(R.string.order_merchant_cannot_update_or_cancel);
                                    }
                                },
                                AppUtils.getInstance().getResources().getColor(R.color.DeepSkyBlue)
                        ));
                    }
                    if (new RuleHelper().hasRule(new HttpRequest(Method.POST.getValue(), ExportUrlPath.CANCEL_EXPORT_STATEMENT))) {
                        underlayButtons.add(new SwipeHelper.UnderlayButton(
                                getString(R.string.cancel),
                                position -> {
                                    if (releaseViewModel.isExportStatement(position) &&
                                            new RuleHelper().hasRule(new HttpRequest(Method.POST.getValue(), CANCEL_EXPORT_STATEMENT))) {
                                        ExportStatement exportStatement = (ExportStatement) releaseViewModel.getBaseModelsE().get(position);
                                        ConfirmDialog confirmDialog = new ConfirmDialog();
                                        String message = context.getString(R.string.cancel_export_statement);
                                        confirmDialog.init(releaseViewModel, message, exportStatement);
                                        confirmDialog.show(getParentFragmentManager(), "confirmDialogCallBack");
                                    } else {
                                        showAlertWarning(R.string.order_merchant_cannot_update_or_cancel);
                                    }
                                },
                                AppUtils.getInstance().getResources().getColor(R.color.LightCoral)
                        ));
                    }
                }
            };
            ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeHelper);
            itemTouchhelper.attachToRecyclerView(recyclerView);
        }
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if (action.equals(SUCCESS)) {
            showAlertSuccess(TIME_AFTER_DELAY, R.string.cancel_success, null);
            releaseViewModel.search();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        releaseViewModel.search();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        String tag = (String) view.getTag();
        if (SCAN_CODE.equals(tag)) {
            Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
            startActivityForResult(qrScan, REQUEST_CODE_SCAN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            String result = data.getStringExtra(RESULT_SCAN);
            releaseViewModel.setStatementCode(result);
        }
        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_export_statement_release;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ExportStatementReleaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.lstImportStatementRequest;
    }
}
