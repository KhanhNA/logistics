package com.next_solutions.logistics.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MerchantOrder extends Base {
    private String code;

    private String qRCode;

    private String merchantCode;

    private String merchantName;

    private String phone;

    private Long amount;

    private Long orderId;

    private String orderCode;

    private String orderDate;

    private Store fromStore;

    /**
     * 0: Đơn hàng Store thiếu hàng, 1: Đơn hàng Store đủ hàng, 2: Đơn hàng trả hàng
     * thành công, 3: Bị hủy, 4: Bị trả
     */
    private Integer status;

    private List<MerchantOrderDetail> merchantOrderDetails;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getqRCode() {
        return qRCode;
    }

    public void setqRCode(String qRCode) {
        this.qRCode = qRCode;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Store getFromStore() {
        return fromStore;
    }

    public void setFromStore(Store fromStore) {
        this.fromStore = fromStore;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<MerchantOrderDetail> getMerchantOrderDetails() {
        return merchantOrderDetails;
    }

    public void setMerchantOrderDetails(List<MerchantOrderDetail> merchantOrderDetails) {
        this.merchantOrderDetails = merchantOrderDetails;
    }
}