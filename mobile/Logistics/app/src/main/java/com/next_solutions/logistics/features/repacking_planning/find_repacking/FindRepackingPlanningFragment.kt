package com.next_solutions.logistics.features.repacking_planning.find_repacking

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.FragmentHelper
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.FIND_IMPORT_STATEMENT_FROM_REPACKING_PLANNING
import com.next_solutions.logistics.repository.api.repacking_planning.RepackingPlanningUrlPath
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity
import java.util.*

class FindRepackingPlanningFragment : BaseRecycleViewFragment(), OnDateSetListener {
    private var isFromDate: Boolean = false
    private lateinit var mViewModel: FindRepackingPlanningViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mViewModel = viewModel as FindRepackingPlanningViewModel
        mViewModel.init()
        return binding.root
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, baseModel ->
            run {
                if (RuleHelper().hasRule(HttpRequest(Method.GET.value, RepackingPlanningUrlPath.FIND_REPACKING_PLANNING_BY_ID)) || (
                                RuleHelper().hasRule(HttpRequest(Method.GET.value, FIND_IMPORT_STATEMENT_FROM_REPACKING_PLANNING)))) {
                    val repackingPlanning = baseModel as RepackingPlanning
                    val intent = Intent(context, CommonActivity::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable(MODEL, repackingPlanning)
                    bundle.putSerializable(FRAGMENT, FragmentHelper().getFragmentType(repackingPlanning))
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_repacked_repacking_item
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.init()
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        when (view!!.tag) {
            FROM_DATE -> {
                val toDate: Date = mViewModel.chooseDate.toDate.get()!!
                isFromDate = true
                showDialogDatePicker(null, toDate.time)
            }
            TO_DATE -> {
                val fromDate: Date = mViewModel.chooseDate.fromDate.get()!!
                isFromDate = false
                showDialogDatePicker(fromDate.time, System.currentTimeMillis() - 1000)
            }
            SCAN_CODE -> {
                val qrScan = Intent(context, QRCodeScannerActivity::class.java)
                startActivityForResult(qrScan, REQUEST_CODE_SCAN)
            }
            else -> {
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mViewModel.onClickButtonSearch()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_SCAN) {
            mViewModel.searchByCode(data!!.getStringExtra(RESULT_SCAN))
        }
    }


    private fun showDialogDatePicker(minDate: Long?, maxDate: Long?) {
        if (activity != null) {
            val calendar = Calendar.getInstance()
            val year: Int
            val month: Int
            val day: Int
            if (isFromDate) {
                val fromDate: Date = mViewModel.chooseDate.fromDate.get()!!
                calendar.time = fromDate
                year = calendar[Calendar.YEAR]
                month = calendar[Calendar.MONTH]
                day = calendar[Calendar.DAY_OF_MONTH]
            } else {
                val fromDate: Date = mViewModel.chooseDate.toDate.get()!!
                calendar.time = fromDate
                year = calendar[Calendar.YEAR]
                month = calendar[Calendar.MONTH]
                day = calendar[Calendar.DAY_OF_MONTH]
            }
            val datePickerDialog = DatePickerDialog(requireContext(), this, year, month, day)
            if (maxDate != null) {
                datePickerDialog.datePicker.maxDate = maxDate
            }
            if (minDate != null) {
                datePickerDialog.datePicker.minDate = minDate
            }
            datePickerDialog.show()
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return FindRepackingPlanningViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_repacked_repacking
    }

    override fun getRecycleResId(): Int {
        return R.id.repackingPlannings
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        mViewModel.setDate(isFromDate, year, month, dayOfMonth)
    }

}