package com.next_solutions.logistics.features.import_statement.import_statement_release.to_fc

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.*
import com.next_solutions.logistics.R
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.features.import_statement.claim_delivery.ClaimDeliveryStatementFragment
import com.next_solutions.logistics.features.import_statement.claim_import_statement.ClaimImportStatementFragment
import com.next_solutions.logistics.models.ImportStatement
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.RecycleViewCallBack
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ImportStatementFCFragment : BaseFragment() {
    private lateinit var importFCViewModel: ImportStatementFCViewModel
    private lateinit var recycleViewAdapter: ImportFCParentAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        importFCViewModel = viewModel as ImportStatementFCViewModel
        setUpRecycleView()
        getData()
        importFCViewModel.init()
        return binding.root
    }


    private fun getData() {
        val importStatement = activity?.intent?.extras?.get(MODEL) as ImportStatement
        importFCViewModel.importStatement = importStatement
    }

    private fun setUpRecycleView() {
        recycleViewAdapter = ImportFCParentAdapter(importFCViewModel, context!!, (activity as FragmentActivity).supportFragmentManager, RecycleViewCallBack { _, obj ->
            run {
                obj as ImportStatementDetail
                importFCViewModel.checkErrorPositionWhenClickCheckBox(obj.index - 1)
                recycleViewAdapter.notifyItemChanged(obj.index - 1)
            }
        })
        recyclerView.apply {
            adapter = recycleViewAdapter
        }

        val spacing = 1
        recyclerView.addItemDecoration(ItemOffsetDecoration(spacing))
    }


    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            CREATE_SUCCESS -> {
                showAlertSuccess(TIME_AFTER_DELAY, R.string.import_release_success, null)
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
            }
            NOTIFY_CHANGE -> {
                val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
                recyclerView.layoutAnimation = controller
                recycleViewAdapter.notifyDataSetChanged()
                recyclerView.scheduleLayoutAnimation()
            }
            NOT_MATCH_QUANTITY -> {
                scrollToErrorPosition()
                showAlertWarning(TIME_AFTER_DELAY, R.string.not_match_quantity, null)
            }
            "DETAIL_MISSING_PALLET_OR_QUANTITY" -> {
                scrollToErrorPosition()
                showAlertWarning(TIME_AFTER_DELAY, R.string.detail_missing_pallet_or_quantity, null)
            }
            MISSING_DETAILS -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.missing_details, null)
            }
        }
    }

    private fun scrollToErrorPosition() {
        recycleViewAdapter.notifyItemChanged(importFCViewModel.errorPosition)
        recycleViewAdapter.notifyItemChanged(importFCViewModel.lastErrorPosition)
        layoutManager.scrollToPositionWithOffset(importFCViewModel.errorPosition, 20)
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        when {
            view!!.id == R.id.scanProduct -> {
                val qrScan = Intent(context, QRCodeScannerActivity::class.java)
                startActivityForResult(qrScan, REQUEST_CODE_SCAN)
            }
            view.id == R.id.createImportClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimImportStatementFragment::class.java)
                bundle.putSerializable(MODEL, importFCViewModel.importStatement)
                intent.putExtras(bundle)
                startActivity(intent)
            }
            view.id == R.id.viewImportClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
                if (importFCViewModel.claimImportDetails!!.isNotEmpty()) {
                    val claim = importFCViewModel.claimImportDetails!![0].claim
                    claim.claimDetails = importFCViewModel.claimImportDetails!!
                    bundle.putSerializable(MODEL, claim)
                    bundle.putSerializable(MODEL_IMPORT_STATEMENT, importFCViewModel.importStatement)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }
            view.id == R.id.createDeliveryClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimDeliveryStatementFragment::class.java)
                bundle.putSerializable(MODEL, importFCViewModel.importStatement)
                intent.putExtras(bundle)
                startActivity(intent)
            }
            view.id == R.id.viewDeliveryClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
                if (importFCViewModel.claimDeliveryDetails!!.isNotEmpty()) {
                    val claim = importFCViewModel.claimDeliveryDetails!![0].claim
                    claim.claimDetails = importFCViewModel.claimDeliveryDetails!!
                    bundle.putSerializable(MODEL, claim)
                    bundle.putSerializable(MODEL_IMPORT_STATEMENT, importFCViewModel.importStatement)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_SCAN) {
                importFCViewModel.scanProductWithCode(data!!.getStringExtra(RESULT_SCAN))
            }
            if (requestCode == CLAIM_ACTIVITY_CODE) {
                val result = data!!.getStringExtra(CLAIM_ACTIVITY)!!
                if (result == CREATE_CLAIM_SUCCESS) {
                    baseActivity.onBackPressed()
                }
            }

        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ImportStatementFCViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_import_statement_fc
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }
}