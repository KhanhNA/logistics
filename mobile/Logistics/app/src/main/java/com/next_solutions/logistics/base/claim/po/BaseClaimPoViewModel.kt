package com.next_solutions.logistics.base.claim.po

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.Po
import com.next_solutions.logistics.models.ProductPacking
import com.next_solutions.logistics.models.dto.ClaimObject
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.ValidateUtils

abstract class BaseClaimPoViewModel(application: Application) : BaseClaimViewModel(application) {
    lateinit var po: Po
    var listData = ArrayList<ClaimObject>()
    var currentTotalQuantity = ObservableField<HashMap<String, Long>>(HashMap())

    fun onQuantityChanged(s: CharSequence, claimDetail: ClaimDetail) {
        try {
            val newQuantity = s.toString().toLong()
            if (claimDetail.quantity != newQuantity) {
                val addValue = newQuantity - claimDetail.quantity
                reCalculator(addValue, claimDetail)
                claimDetail.quantity = newQuantity
            }
        } catch (e: Exception) {
            reCalculator(claimDetail.quantity * -1, claimDetail)
            if (s.toString() == EMPTY_STRING) {
                claimDetail.quantity = 0
            }
            Log.e(ERROR_PROCESS, e.toString())
        }
    }

    fun reCalculator(addValue: Long, claimDetail: ClaimDetail) {
        val total = currentTotalQuantity.get()
        val code = getCodeMapping(claimDetail)
        total?.let {
            val newValue = it[code]!!.plus(addValue)
            it[code] = newValue
        }
        currentTotalQuantity.notifyChange()
    }

    fun getCodeMapping(claimDetail: ClaimDetail): String {
        return claimDetail.productPacking.code
    }

    fun getCodeMapping(claimObject: ClaimObject): String {
        return claimObject.productPackingCode
    }

    fun addRow(addPosition: Int, claimObject: ClaimObject) {
        claimObject.claimDetails.add(addPosition, ClaimDetail().apply {
            quantity = 0
            productPacking = ProductPacking().apply {
                id = claimObject.productPackingId
                code = claimObject.productPackingCode
            }
        })
    }

    override fun validate(): Boolean {
        if (ValidateUtils.isNullOrEmpty(po)) {
            return false
        }
//        if (ValidateUtils.isNullOrEmpty(amenableObservable.get())) {
//            sendBackAction("MISSING_AMENABLE")
//            return false
//        }
        val totalQuantity = currentTotalQuantity.get()!!
        var position = 0
        listData.forEach { claimObject: ClaimObject ->
            run {
                val currentQuantity = totalQuantity[getCodeMapping(claimObject)]!!
                if (ValidateUtils.isNullOrEmpty(currentQuantity) || (currentQuantity > claimObject.quantity)) {
                    notifyPosition.set(position)
                    changeViewAndSaveLastPosition()
                    return false
                }
                val detailMissingInfo = claimObject.claimDetails
                        .asSequence()
                        .find {
                            ValidateUtils.isNullOrEmpty(it.quantity)
                                    || (ValidateUtils.isNullOrEmpty(it.expireDate))
                        }
                if (detailMissingInfo != null) {
                    notifyPosition.set(position)
                    changeViewAndSaveLastPosition()
                    return false
                }
                position++
            }
        }
        val addImage = images.asSequence().map { it.claimImage }.filter { it.id == null }.toList()
        val missingDescriptionImage = addImage.find { ValidateUtils.isNullOrEmpty(it.description) }
        missingDescriptionImage?.let {
            sendBackAction(MISSING_DESCRIPTION)
            return false
        }

        return true
    }

    private fun changeViewAndSaveLastPosition() {
        lastNotifyPosition?.let {
            listData[it].checked = null
        }
        listData[notifyPosition.get()!!].checked = false
        sendBackAction(PRODUCT_INFO_ERROR)
        lastNotifyPosition = notifyPosition.get()
    }

    fun delete(claimObject: ClaimObject) {
        val notifyRemovePosition = listData.indexOf(claimObject)
        listData.removeAt(notifyRemovePosition)
        currentTotalQuantity.get()?.remove(getCodeMapping(claimObject))
        notifyPosition.set(notifyRemovePosition)
        sendBackAction(NOTIFY_CHANGE_ITEM_REMOVE)
    }
}