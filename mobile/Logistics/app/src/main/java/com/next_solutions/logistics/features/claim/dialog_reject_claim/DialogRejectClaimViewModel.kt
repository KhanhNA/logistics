package com.next_solutions.logistics.features.claim.dialog_reject_claim

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING

class DialogRejectClaimViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    var reason: String = EMPTY_STRING
    lateinit var message: String

}