package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.features.export_statement.export_statement_delivery.ExportStatementDeliveryDetailFragment
import com.next_solutions.logistics.features.export_statement.export_statement_release.ExportStatementDetailReleaseFragment
import com.next_solutions.logistics.features.import_po.import_po_in_pallet.ImportPoInPalletFragment
import com.next_solutions.logistics.features.import_po.import_po_new.ImportPoNewFragment
import com.next_solutions.logistics.features.import_po.import_po_out_of_pallet.ImportPoOutOfPalletFragment
import com.next_solutions.logistics.features.import_statement.import_statement_release.to_dc.ImportStatementDCFragment
import com.next_solutions.logistics.features.import_statement.import_statement_release.to_fc.ImportStatementFCFragment
import com.next_solutions.logistics.features.po.imported_po.ImportedPoDetailFragment
import com.next_solutions.logistics.features.po.search_po.PoDetailFragment
import com.next_solutions.logistics.features.repacking_planning.cancel_repacking.CancelRepackingPlanningFragment
import com.next_solutions.logistics.features.repacking_planning.imported_statement.ImportedRepackingDetailFragment
import com.next_solutions.logistics.features.repacking_planning.new_repacking.NewRepackingPlanningFragment
import com.next_solutions.logistics.features.repacking_planning.repacked_statement.RepackedRepackingPlanningFragment
import com.next_solutions.logistics.models.*
import java.io.Serializable

class FragmentHelper {
    fun getFragmentType(po: Po): Serializable {
        return if (Po.PoStatus.IMPORTED.value.equals(po.status) ) {
            ImportedPoDetailFragment::class.java
        } else {
            PoDetailFragment::class.java
        }
    }

    fun getFragmentType(importPo: ImportPo): Serializable {
        return when (importPo.status) {
            ImportPo.ImportPoStatus.NEW.value -> {
                ImportPoNewFragment::class.java
            }
            ImportPo.ImportPoStatus.IN_PALLET.value -> {
                ImportPoInPalletFragment::class.java
            }
            else -> return ImportPoOutOfPalletFragment::class.java
        }

    }

    fun getFragmentType(claim: Claim): Serializable {
        return when (claim.type) {
            Claim.ClaimType.DELIVERY -> {
                ExportStatementDeliveryDetailFragment::class.java
            }
            Claim.ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT -> {
                ImportStatementFCFragment::class.java
            }
            else -> {
                ExportStatementDetailReleaseFragment::class.java
            }

        }

    }

    fun getFragmentType(repackingPlanning: RepackingPlanning): Serializable {
        return when (repackingPlanning.status) {
            RepackingPlanning.RepackingPlanningStatus.IMPORTED.value -> {
                ImportedRepackingDetailFragment::class.java
            }
            RepackingPlanning.RepackingPlanningStatus.REPACKED.value -> {
                RepackedRepackingPlanningFragment::class.java
            }
            RepackingPlanning.RepackingPlanningStatus.NEW.value -> {
                NewRepackingPlanningFragment::class.java
            }
            else -> {
                CancelRepackingPlanningFragment::class.java
            }
        }
    }

    fun getFragmentType(exportStatement: ExportStatement): Serializable? {
        if (exportStatement.status == ExportStatement.ExportStatementStatus.REQUESTED.value) {
            return ExportStatementDetailReleaseFragment::class.java
        }
        return null
    }

    fun getFragmentType(importStatement: ImportStatement): Serializable? {
        if (importStatement.status == ImportStatement.ImportStatementStatus.UPDATED_INVENTORY.value) {
            return null
        }
        return if (importStatement.toStore.isFC) {
            ImportStatementFCFragment::class.java
        } else {
            ImportStatementDCFragment::class.java
        }
    }
}