package com.next_solutions.logistics.repository.api.error

import com.next_solutions.logistics.repository.api.error.ErrorPath.Companion.POST_ERROR
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ErrorService {
    @POST(POST_ERROR)
    fun sendError(@Body errorMessages: String): Single<Response<ResponseBody>>
}