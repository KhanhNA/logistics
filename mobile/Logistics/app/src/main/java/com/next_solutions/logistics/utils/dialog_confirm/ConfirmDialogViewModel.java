package com.next_solutions.logistics.utils.dialog_confirm;

import android.app.Application;

import androidx.annotation.NonNull;

import com.next_solutions.base.BaseViewModel;

public class ConfirmDialogViewModel extends BaseViewModel {

    private String message;

    public ConfirmDialogViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
