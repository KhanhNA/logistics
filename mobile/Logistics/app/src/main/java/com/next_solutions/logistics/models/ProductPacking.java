package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class ProductPacking extends Base {

    private String code;

    private String name;

    private Integer vat;

    private String uom;

    private String barcode;

    private Product product;

    private PackingType packingType;

    private Boolean status;

    private Distributor distributor;

    private List<ProductPackingPrice> productPackingPrices;

    private List<PalletDetail> palletDetails;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVat() {
        return vat;
    }

    public void setVat(Integer vat) {
        this.vat = vat;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public PackingType getPackingType() {
        return packingType;
    }

    public void setPackingType(PackingType packingType) {
        this.packingType = packingType;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<ProductPackingPrice> getProductPackingPrices() {
        return productPackingPrices;
    }

    public void setProductPackingPrices(List<ProductPackingPrice> productPackingPrices) {
        this.productPackingPrices = productPackingPrices;
    }

    public List<PalletDetail> getPalletDetails() {
        return palletDetails;
    }

    public void setPalletDetails(List<PalletDetail> palletDetails) {
        this.palletDetails = palletDetails;
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }
}
