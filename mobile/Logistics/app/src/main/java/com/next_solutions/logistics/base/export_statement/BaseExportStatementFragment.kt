package com.next_solutions.logistics.base.export_statement

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.features.export_statement.create_export_statement.PackingProductDialog
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.dialog_confirm.ConfirmDialog

abstract class BaseExportStatementFragment : BaseRecycleViewFragment() {
    private lateinit var exportStatementViewModel: BaseExportStatementViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpViewModel()
        return binding.root
    }

    private fun setUpViewModel() {
        exportStatementViewModel = viewModel as BaseExportStatementViewModel
    }

    override fun action(view: View, baseViewModel: BaseViewModel<*>?) {
        val packingProductDialog = PackingProductDialog()
        packingProductDialog.exceptId = exportStatementViewModel.exceptId!!
        packingProductDialog.fromStoreId = exportStatementViewModel.fromStoreId!!
        packingProductDialog.dialogCallBack = exportStatementViewModel
        packingProductDialog.exportStatementId = exportStatementViewModel.exportStatementId
        packingProductDialog.show((activity as FragmentActivity).supportFragmentManager, EMPTY_STRING)
    }

    override fun processFromVM(action: String, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        super.processFromVM(action, view, viewModel, t)
        when (action) {
            CREATE_SUCCESS -> {
                showAlertSuccess(TIME_AFTER_DELAY, R.string.success, null)
            }
            ERROR_PROCESS -> {
                showAlertError(TIME_AFTER_DELAY, R.string.error_process, null)
            }
            ERROR_INVALID_INPUT -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.error_invalid_input, null)
                val notifyPosition = exportStatementViewModel.notifyPosition.get()
                if (notifyPosition != null) {
                    layoutManager.scrollToPosition(notifyPosition)
                }
            }
            NOTIFY_CHANGE_ITEM -> {
                val notifyPosition = exportStatementViewModel.notifyPosition.get()
                if (notifyPosition != null) {
                    baseAdapter!!.notifyItemChanged(notifyPosition)
                }
            }
            NOTIFY_CHANGE_ITEM_ADD -> {
                val notifyPosition = exportStatementViewModel.notifyPosition.get()
                if (notifyPosition != null) {
                    baseAdapter!!.notifyItemInserted(notifyPosition)
                }
            }
            NOTIFY_CHANGE_ITEM_REMOVE -> {
                val notifyPosition = exportStatementViewModel.notifyPosition.get()
                if (notifyPosition != null) {
                    baseAdapter!!.notifyItemRemoved(notifyPosition)
                }
            }
            "MISSING_PRICE" -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.detail_missing_price, null)
                val notifyPosition = exportStatementViewModel.notifyPosition.get()
                if (notifyPosition != null) {
                    layoutManager.scrollToPosition(notifyPosition)
                }
            }
            CONFIRM -> {
                val confirmDialog = ConfirmDialog()
                val message = context!!.getString(R.string.export_statement_greate_than) + " " + exportStatementViewModel.config!!.maxValue + " " + exportStatementViewModel.config!!.currency
                confirmDialog.init(exportStatementViewModel, message, exportStatementViewModel.exportStatements)
                confirmDialog.show((activity as FragmentActivity).supportFragmentManager, "confirmDialogCallBack")
            }
            UPDATE_SUCCESS -> {
                showAlertSuccess(TIME_AFTER_DELAY, R.string.update_success, null)
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
            }
        }
    }
}