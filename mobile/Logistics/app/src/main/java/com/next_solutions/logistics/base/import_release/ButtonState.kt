package com.next_solutions.logistics.base.import_release

import androidx.databinding.ObservableField

class ButtonState {

    var buttonImportRelease = ObservableField(true)

    var buttonCreateClaimImport = ObservableField(false)
    var buttonViewClaimImport = ObservableField(false)

    var buttonCreateClaimDelivery = ObservableField(false)
    var buttonViewClaimDelivery = ObservableField(false)

    fun enableButtonImportRelease() {
        buttonImportRelease.set(true)
        buttonImportRelease.notifyChange()
    }

    fun disableButtonImportRelease() {
        buttonImportRelease.set(false)
        buttonImportRelease.notifyChange()
    }

    fun enableButtonCreateClaimImport() {
        buttonCreateClaimImport.set(true)
        buttonCreateClaimImport.notifyChange()
    }

    fun disableButtonCreateClaimImport() {
        buttonCreateClaimImport.set(false)
        buttonCreateClaimImport.notifyChange()
    }

    fun enableButtonViewClaimImport() {
        buttonViewClaimImport.set(true)
        buttonViewClaimImport.notifyChange()
    }

    fun disableButtonViewClaimImport() {
        buttonViewClaimImport.set(false)
        buttonViewClaimImport.notifyChange()
    }

    fun enableButtonCreateClaimDelivery() {
        buttonCreateClaimDelivery.set(true)
        buttonCreateClaimDelivery.notifyChange()
    }

    fun disableButtonCreateClaimDelivery() {
        buttonCreateClaimDelivery.set(false)
        buttonCreateClaimDelivery.notifyChange()
    }


    fun enableButtonViewClaimDelivery() {

        buttonViewClaimDelivery.set(true)
        buttonViewClaimDelivery.notifyChange()
    }

    fun disableButtonViewClaimDelivery() {
        buttonViewClaimDelivery.set(false)
        buttonViewClaimDelivery.notifyChange()
    }

    fun disableAllButton() {
        disableButtonImportRelease()
        disableButtonCreateClaimImport()
        disableButtonViewClaimImport()
        disableButtonCreateClaimDelivery()
        disableButtonViewClaimDelivery()

    }
}