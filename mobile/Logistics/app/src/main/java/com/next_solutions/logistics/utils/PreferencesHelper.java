package com.next_solutions.logistics.utils;

import android.content.SharedPreferences;


public class PreferencesHelper {
    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String LANGUAGE = "language";
    private static final String TOKEN = "token";
    private static final String CURRENT_LANGUAGE = "CURRENT_LANGUAGE";
    private SharedPreferences sharedPreferences;


    public Long getLanguage() {
        return sharedPreferences.getLong(PreferencesHelper.LANGUAGE, -1L);
    }

    public void setLanguage(Long language) {
        sharedPreferences.edit().putLong(PreferencesHelper.LANGUAGE, language).apply();
    }

    public String getRefreshToken() {
        return sharedPreferences.getString(PreferencesHelper.REFRESH_TOKEN, "");
    }

    public void setRefreshToken(String refreshToken) {
        sharedPreferences.edit().putString(PreferencesHelper.REFRESH_TOKEN, refreshToken).apply();
    }

    public void setToken(String token) {
        sharedPreferences.edit().putString(PreferencesHelper.TOKEN, token).apply();
    }

    public String getToken() {
        return sharedPreferences.getString(PreferencesHelper.TOKEN, "");
    }

    public String getString(String preferenceKey, String preferenceDefaultValue) {
        return sharedPreferences.getString(preferenceKey, preferenceDefaultValue);
    }

    public int getCurrentLanguage() {
        return sharedPreferences.getInt(PreferencesHelper.CURRENT_LANGUAGE, 0);
    }

    public void setCurrentLanguage(Integer position) {
        sharedPreferences.edit().putInt(PreferencesHelper.CURRENT_LANGUAGE, position).commit();
    }


    public void setSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }
}
