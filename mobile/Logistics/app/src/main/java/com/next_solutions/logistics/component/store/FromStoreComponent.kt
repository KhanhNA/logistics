package com.next_solutions.logistics.component.store

import android.util.Log
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Store
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants
import java.util.*

class FromStoreComponent(private val baseViewModel: BaseViewModel<BaseModel>) : BaseStoreComponent() {
    init {
        callApiGetFromStore()
    }

    override fun callApiGetFromStore() {
        val exceptIds = ArrayList<Long>()
        exceptIds.add(-1L)
        baseViewModel.callApi(Service.callStoreService()
                .getFromStore(exceptIds, false, AppUtils.FIRST_PAGE, Int.MAX_VALUE, Store.StoreStatus.ACTIVE.value, Constants.EMPTY_STRING)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Pageable<Store>>(baseViewModel.appException, Pageable::class.java, Store::class.java, null) {
                    override fun getDataSuccess(storePageable: Pageable<Store>) {
                        stores.clear()
                        stores.addAll(storePageable.content)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                    }
                }))
    }
}