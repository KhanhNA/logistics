package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.DateUtils;
import com.next_solutions.logistics.utils.ValidateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.Date;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class StatementDetail extends Base {
    private Statement statement;
    private ProductPacking productPacking;
    private ProductPackingPrice productPackingPrice;
    private Long quantity;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date importDate;
    private Long productId;
    private String productCode;
    private Long packingTypeId;
    private PackingType packingType;
    private String packingTypeCode;
    private Integer packingTypeQuantity;
    private BigDecimal price;
    private String productName;
    private StoreProductPackingDetail storeProductPackingDetail;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date expireDate;
    private String expireDateStr;
    private Long totalQuantity;

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public ProductPackingPrice getProductPackingPrice() {
        return productPackingPrice;
    }

    public void setProductPackingPrice(ProductPackingPrice productPackingPrice) {
        this.productPackingPrice = productPackingPrice;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Date getImportDate() {
        return importDate;
    }

    public void setImportDate(Date importDate) {
        this.importDate = importDate;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getPackingTypeId() {
        return packingTypeId;
    }

    public void setPackingTypeId(Long packingTypeId) {
        this.packingTypeId = packingTypeId;
    }

    public PackingType getPackingType() {
        return packingType;
    }

    public void setPackingType(PackingType packingType) {
        this.packingType = packingType;
    }

    public String getPackingTypeCode() {
        return packingTypeCode;
    }

    public void setPackingTypeCode(String packingTypeCode) {
        this.packingTypeCode = packingTypeCode;
    }

    public void setPackingTypeQuantity(Integer packingTypeQuantity) {
        this.packingTypeQuantity = packingTypeQuantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public StoreProductPackingDetail getStoreProductPackingDetail() {
        return storeProductPackingDetail;
    }

    public void setStoreProductPackingDetail(StoreProductPackingDetail storeProductPackingDetail) {
        this.storeProductPackingDetail = storeProductPackingDetail;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public String getExpireDateStr() {
        return expireDateStr;
    }

    public void setExpireDateStr(String expireDateStr) {
        this.expireDateStr = expireDateStr;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public StatementDetail(Long productId, String productCode, String productName) {
        this.productId = productId;
        this.productCode = productCode;
        this.productName = productName;
    }

    public String getQuantityStr() {
        return String.valueOf(quantity == null ? "" : quantity);
    }

    public void setQuantityStr(String str) {
        if (ValidateUtils.isNullOrEmpty(str)) {
            quantity = 0L;
        } else {
            quantity = Long.parseLong(str);
        }
    }

    public void setExpireDate(Date expireDate) {
        if (expireDate == null) {
            expireDateStr = "";
        } else {
            expireDateStr = DateUtils.convertDateToString(expireDate, DateUtils.DATE_SHOW_FORMAT);
        }
        this.expireDate = expireDate;
    }

    public Long getTotalQuantity() {
        return totalQuantity == null ? 0 : totalQuantity;
    }

    public Integer getPackingTypeQuantity() {
        return packingTypeQuantity == null ? 0 : packingTypeQuantity;
    }
}
