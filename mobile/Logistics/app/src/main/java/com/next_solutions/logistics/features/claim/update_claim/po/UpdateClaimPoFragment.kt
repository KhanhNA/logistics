package com.next_solutions.logistics.features.claim.update_claim.po

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.po.BaseClaimPoFragment
import com.next_solutions.logistics.features.claim.update_claim.UpdateImageClaimAdapter
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.Image
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.RELOAD_IMAGE
import com.next_solutions.logistics.utils.RecycleViewCallBack
import com.yanzhenjie.album.AlbumFile
import java.util.*

class UpdateClaimPoFragment : BaseClaimPoFragment() {
    private lateinit var updateClaimPoViewModel: UpdateClaimPoViewModel
    private lateinit var imageClaimAdapter: UpdateImageClaimAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        updateClaimPoViewModel = viewModel as UpdateClaimPoViewModel
        getData()
        updateClaimPoViewModel.init()
        return binding.root
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val dialog = ChooseClaimPoDetailDialog()
            dialog.show((activity as FragmentActivity).supportFragmentManager, "")
        }
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        super.processFromVM(action, view, viewModel, t)
        if (action == RELOAD_IMAGE) {
            imageClaimAdapter.notifyDataSetChanged()
        }
    }

    override fun setUpRecycleViewImage() {
        imageClaimAdapter = UpdateImageClaimAdapter(baseClaimPoViewModel.images, RecycleViewCallBack { view, image ->
            run {
                if (view!!.id == R.id.delete_image) {
                    baseClaimPoViewModel.removeImage(image as Image)
                    imageClaimAdapter.notifyDataSetChanged()
                }
            }
        }, baseClaimPoViewModel)
        val linearLayout = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.root.findViewById<RecyclerView>(getRecycleViewImage()).apply {
            adapter = imageClaimAdapter
            layoutManager = linearLayout
        }
    }

    override fun doAfterPickImageFromGallery(result: ArrayList<AlbumFile>) {
        baseClaimPoViewModel.updateImages(result)
        imageClaimAdapter.notifyDataSetChanged()
    }

    private fun getData() {
        val claimStatement = activity?.intent?.extras?.get(Constants.MODEL) as Claim?
        claimStatement?.let { updateClaimPoViewModel.claim = claimStatement }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return UpdateClaimPoViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_update_claim_po
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getLayoutRecycleViewL1Id(): Int {
        return R.layout.fragment_claim_po_item
    }

    override fun getLayoutRecycleViewL2Id(): Int {
        return R.layout.fragment_claim_po_item2
    }

    override fun getRecycleViewL2Id(): Int {
        return R.id.claim_details
    }
}