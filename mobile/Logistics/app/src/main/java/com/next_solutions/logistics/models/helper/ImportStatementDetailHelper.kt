package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.base.adapter.ParentModel
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.models.ImportStatementDetailPallet
import com.next_solutions.logistics.models.dto.ImportStatementDetailDTO
import com.next_solutions.logistics.models.dto.SplitRepack

class ImportStatementDetailHelper {
    fun convertImportDetailToDetailPallet(splitRepack: SplitRepack, details: ImportStatementDetailDTO): ImportStatementDetailPallet {
        return ImportStatementDetailPallet().apply {
            importStatementDetailId = details.id
            pallet = splitRepack.pallet
            productPackingId = details.productPackingId
            expireDate = details.expireDate
            quantity = splitRepack.quantity
        }
    }

    fun minimizeDetailToDTO(detail: ImportStatementDetail): ImportStatementDetailDTO {
        return ImportStatementDetailDTO().apply {
            productPackingId = detail.productPackingId
            productPackingCode = detail.productPackingCode
            quantity = detail.quantity
            expireDate = detail.expireDate
        }
    }

    fun convertImportStatementDetailToParentModel(importDetail: ImportStatementDetail): ParentModel {
        return ParentModel().apply {
            quantity = importDetail.quantity + importDetail.claimQuantity
            data = importDetail.importStatementDetailPallets
            productPacking = importDetail.productPacking
            expireDate = importDetail.expireDate
            claimQuantity = importDetail.claimQuantity
        }
    }
}