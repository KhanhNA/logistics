package com.next_solutions.logistics.features.login;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.SingleLiveEvent;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.Rule;
import com.next_solutions.logistics.repository.HttpHelper;
import com.next_solutions.logistics.repository.Service;
import com.next_solutions.logistics.utils.Constants;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

@SuppressWarnings("unchecked")
public class SplashViewModel extends BaseViewModel {

    private SingleLiveEvent<Boolean> openHomeActivity = new SingleLiveEvent<>();
    private String refreshToken;

    public SplashViewModel(@NonNull Application application) {
        super(application);
    }

    void checkTokenExpired(String refreshToken) {
        this.refreshToken = refreshToken;
        HttpHelper.init(AppUtils.getInstance().getPreferencesHelper().getToken());
        callApi(Service.callUserService()
                .getUser()
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribe(this::success, this::error));
    }

    private void error(Throwable throwable) {
        showMessage(R.string.error_process, throwable.getMessage());
    }

    private void success(Response<ResponseBody> response) {
        if (response.code() != Constants.CODE_SUCCESS) {
            getNewToken();
        } else {
            ResponseBody responseBody = response.body();
            if (responseBody != null) {
                try {
                    String s = responseBody.string();
                    ObjectMapper objectMapper = new ObjectMapper();
                    objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
                    Rule rule = objectMapper.readValue(s, Rule.class);
                    getRoleSuccess(rule);
                } catch (IOException e) {
                    Log.e(ERROR_PROCESS, e.getMessage());
                }
            }
        }
    }

    private void getRoleSuccess(Rule rule) {
        AppUtils.setRule(rule);
        AppUtils.setLanguage(AppUtils.getInstance().getPreferencesHelper().getLanguage());
        openHomeActivity.postValue(true);
    }

    private void getNewToken() {
        HttpHelper.getNewToken(this.refreshToken, (ca, res, token, throwable) -> {
            if (token != null) {
                callApi(Service.callUserService()
                        .getUser()
                        .compose(Service.getRxSingleSchedulers().applySchedulers())
                        .subscribeWith(new RxSingleSubscriber<Rule>(appException, Rule.class, null) {
                            @Override
                            public void getDataSuccess(Rule rule) {
                                getRoleSuccess(rule);
                            }

                            @Override
                            public void onFailure(ErrorRespone errorRespone) {
                                Log.e(ERROR_PROCESS, errorRespone.getMessage());
                            }
                        }));
            } else {
                openHomeActivity.postValue(false);
            }
        });
    }

    SingleLiveEvent<Boolean> getOpenHomeActivity() {
        return openHomeActivity;
    }
}
