package com.next_solutions.logistics.repository;

import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxObservableSchedulers;
import com.next_solutions.logistics.config.RxSingleSchedulers;
import com.next_solutions.logistics.repository.api.claim.ClaimService;
import com.next_solutions.logistics.repository.api.config.ConfigService;
import com.next_solutions.logistics.repository.api.distributor.DistributorService;
import com.next_solutions.logistics.repository.api.error.ErrorService;
import com.next_solutions.logistics.repository.api.export_statement.ExportStatementService;
import com.next_solutions.logistics.repository.api.import_po.ImportPoService;
import com.next_solutions.logistics.repository.api.import_statement.ImportStatementService;
import com.next_solutions.logistics.repository.api.inventory.InventoryService;
import com.next_solutions.logistics.repository.api.language.LanguageService;
import com.next_solutions.logistics.repository.api.merchant_order.MerchantOrderService;
import com.next_solutions.logistics.repository.api.pallet.PalletService;
import com.next_solutions.logistics.repository.api.po.PoService;
import com.next_solutions.logistics.repository.api.repacking_planning.RepackingPlanningService;
import com.next_solutions.logistics.repository.api.store.StoreService;
import com.next_solutions.logistics.repository.api.user.UserService;

public class Service {
    private static PalletService palletService;
    private static UserService userService;
    private static ImportPoService importPoService;
    private static DistributorService distributorService;
    private static ExportStatementService exportStatementService;
    private static LanguageService languageService;
    private static RepackingPlanningService repackingPlanningService;
    private static StoreService storeService;
    private static ImportStatementService importStatementService;
    private static MerchantOrderService merchantOrderService;
    private static InventoryService inventoryService;
    private static PoService poService;
    private static ClaimService claimService;
    private static ConfigService configService;
    private static ErrorService errorService;

    private Service() {
    }

    public static RxSingleSchedulers getRxSingleSchedulers() {
        if (AppUtils.IS_TEST_MODE) {
            return RxSingleSchedulers.TEST_SCHEDULER;
        }
        return RxSingleSchedulers.DEFAULT;
    }

    public static RxObservableSchedulers getRxObservableSchedulers() {
        if (AppUtils.IS_TEST_MODE) {
            return RxObservableSchedulers.TEST_SCHEDULER;
        }
        return RxObservableSchedulers.DEFAULT;
    }

    //========================================================================================================================
    public static PalletService callPalletService() {
        if (AppUtils.IS_TEST_MODE) {
            return palletService;
        }
        return HttpHelper.getClient().create(PalletService.class);
    }

    public static void setPalletService(PalletService palletService) {
        Service.palletService = palletService;
    }

    //========================================================================================================================
    public static UserService callUserService() {
        if (AppUtils.IS_TEST_MODE) {
            return userService;
        }
        return HttpHelper.getAuthClient().create(UserService.class);
    }

    public static void setUserService(UserService userService) {
        Service.userService = userService;
    }

    //========================================================================================================================
    public static void setImportPoService(ImportPoService importPoService) {
        Service.importPoService = importPoService;
    }

    public static ImportPoService callImportPoService() {
        if (AppUtils.IS_TEST_MODE) {
            return importPoService;
        }
        return HttpHelper.getClient().create(ImportPoService.class);
    }

    //========================================================================================================================
    public static void setDistributorService(DistributorService distributorService) {
        Service.distributorService = distributorService;
    }

    public static DistributorService callDistributorService() {
        if (AppUtils.IS_TEST_MODE) {
            return distributorService;
        }
        return HttpHelper.getClient().create(DistributorService.class);

    }

    //========================================================================================================================

    public static void setExportStatementService(ExportStatementService exportStatementService) {
        Service.exportStatementService = exportStatementService;
    }

    public static ExportStatementService callExportStatementService() {
        if (AppUtils.IS_TEST_MODE) {
            return exportStatementService;
        }
        return HttpHelper.getClient().create(ExportStatementService.class);
    }

    //========================================================================================================================
    public static void setLanguageService(LanguageService languageService) {
        Service.languageService = languageService;
    }

    public static LanguageService callLanguageService() {
        if (AppUtils.IS_TEST_MODE) {
            return languageService;
        }
        return HttpHelper.getClient().create(LanguageService.class);
    }

    //========================================================================================================================
    public static void setRepackingPlanningService(RepackingPlanningService repackingPlanningService) {
        Service.repackingPlanningService = repackingPlanningService;
    }

    public static RepackingPlanningService callRepackingPlanningService() {
        if (AppUtils.IS_TEST_MODE) {
            return repackingPlanningService;
        }
        return HttpHelper.getClient().create(RepackingPlanningService.class);
    }

    //========================================================================================================================
    public static void setStoreService(StoreService storeService) {
        Service.storeService = storeService;
    }

    public static StoreService callStoreService() {
        if (AppUtils.IS_TEST_MODE) {
            return storeService;
        }
        return HttpHelper.getClient().create(StoreService.class);
    }

    //========================================================================================================================
    public static void setImportStatementService(ImportStatementService importStatementService) {
        Service.importStatementService = importStatementService;
    }

    public static ImportStatementService callImportStatementService() {
        if (AppUtils.IS_TEST_MODE) {
            return importStatementService;
        }
        return HttpHelper.getClient().create(ImportStatementService.class);
    }

    //========================================================================================================================
    public static void setMerchantOrderService(MerchantOrderService merchantOrderService) {
        Service.merchantOrderService = merchantOrderService;
    }

    public static MerchantOrderService callMerchantOrderService() {
        if (AppUtils.IS_TEST_MODE) {
            return merchantOrderService;
        }
        return HttpHelper.getClient().create(MerchantOrderService.class);
    }

    //========================================================================================================================
    public static void setInventoryService(InventoryService inventoryService) {
        Service.inventoryService = inventoryService;
    }

    public static InventoryService callInventoryService() {
        if (AppUtils.IS_TEST_MODE) {
            return inventoryService;
        }
        return HttpHelper.getClient().create(InventoryService.class);
    }

    //========================================================================================================================
    public static void setPoService(PoService poService) {
        Service.poService = poService;
    }

    public static PoService callPoService() {
        if (AppUtils.IS_TEST_MODE) {
            return poService;
        }
        return HttpHelper.getClient().create(PoService.class);
    }

    //========================================================================================================================
    public static void setClaimService(ClaimService claimService) {
        Service.claimService = claimService;
    }

    public static ClaimService callClaimService() {
        if (AppUtils.IS_TEST_MODE) {
            return claimService;
        }
        return HttpHelper.getClient().create(ClaimService.class);
    }

    //========================================================================================================================
    public static void setConfigService(ConfigService configService) {
        Service.configService = configService;
    }

    public static ConfigService callConfigService() {
        if (AppUtils.IS_TEST_MODE) {
            return configService;
        }
        return HttpHelper.getClient().create(ConfigService.class);
    }

    //========================================================================================================================
    public static void setErrorService(ConfigService configService) {
        Service.configService = configService;
    }

    public static ErrorService callErrorService() {
        if (AppUtils.IS_TEST_MODE) {
            return errorService;
        }
        return HttpHelper.getClient().create(ErrorService.class);
    }
}
