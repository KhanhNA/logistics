package com.next_solutions.logistics.features.claim.update_claim

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.ClaimDetail

class ChooseClaimDetailDialog : BaseDialogFragment() {
    private lateinit var claimDetailViewModel: ChooseClaimDetailViewModel
    private lateinit var updateClaimViewModel: UpdateClaimViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpViewModel()
        setUpRecycleView()
        claimDetailViewModel.init()
        return binding.root
    }

    private fun setUpViewModel() {
        claimDetailViewModel = viewModel as ChooseClaimDetailViewModel
        updateClaimViewModel = ViewModelProvider(this.baseActivity,
                BaseViewModelFactory.getInstance<Any>(this.baseActivity.application, UpdateClaimViewModel::class.java))
                .get<UpdateClaimViewModel>(UpdateClaimViewModel::class.java)
        claimDetailViewModel.updateClaimViewModel = updateClaimViewModel
    }

    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_claim_detail_item, claimDetailViewModel, OwnerView { _, detail ->
            run {
                updateClaimViewModel.addClaimDetail(detail as ClaimDetail)
                dismiss()
            }
        }, baseActivity)
        recyclerView.apply {
            adapter = baseAdapter
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChooseClaimDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_claim_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}