package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.models.*

class ImportPoHelper {
    fun convertDetailPalletAvoidRecursive(importPoDetail: ImportPoDetail, importPo: ImportPo): ImportPoDetail {
        val newEntity = convertImportPoDetailAvoidRecursive(importPoDetail, importPo)
        newEntity.importPoDetailPallets = arrayListOf()
        importPoDetail.importPoDetailPallets.forEach { detail ->
            run {
                newEntity.importPoDetailPallets.add(convertDetailPallet(importPo, newEntity, detail))
            }
        }
        return newEntity
    }

    private fun convertDetailPallet(importPo: ImportPo, importPoDetail: ImportPoDetail, importPoDetailPallet: ImportPoDetailPallet): ImportPoDetailPallet {
        return ImportPoDetailPallet(
                importPoDetailPallet.quantity,
                Pallet().apply {
                    displayName = importPoDetailPallet.pallet.displayName
                    code = importPoDetailPallet.pallet.code
                    id = importPoDetailPallet.pallet.id
                },
                importPo,
                ImportPoDetail().apply {
                    id = importPoDetail.id
                    expireDate = importPoDetail.expireDate
                    this.productPacking = importPoDetail.productPacking
                    this.importPo = importPo
                }
        )
    }


    private fun convertImportPoDetailAvoidRecursive(importPoDetail: ImportPoDetail, importPo: ImportPo): ImportPoDetail {
        val product = Product().apply { name = importPoDetail.product.name }
        val productPacking = ProductPacking().apply {
            id = importPoDetail.productPacking.id
            code = importPoDetail.productPacking.code
            this.product = product
            packingType = PackingType().apply { quantity = importPoDetail.productPacking.packingType.quantity }
            uom = importPoDetail.productPacking.uom
        }
        return ImportPoDetail().apply {
            id = importPoDetail.id
            this.importPo = importPo
            expireDate = importPoDetail.expireDate
            quantity = importPoDetail.quantity
            claimQuantity = importPoDetail.claimQuantity
            originalQuantity = importPoDetail.originalQuantity
            this.productPacking = productPacking
        }
    }

    fun createNewEntityAvoidRecursive(importPoDetail: ImportPoDetail, importPo: ImportPo): ImportPoDetail {
        val newEntity = convertImportPoDetailAvoidRecursive(importPoDetail, importPo)
        return newEntity.apply {
            importPoDetailPallets = arrayListOf(ImportPoDetailPallet(0, Pallet(), importPo,
                    ImportPoDetail().apply {
                        id = importPoDetail.id
                        expireDate = importPoDetail.expireDate
                        this.productPacking = newEntity.productPacking
                        this.importPo = importPo
                    }))
        }
    }
}