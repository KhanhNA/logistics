package com.next_solutions.logistics.features.po.claim_po

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.po.BaseClaimPoFragment
import com.next_solutions.logistics.models.Po
import com.next_solutions.logistics.utils.Constants


class ClaimPoFragment : BaseClaimPoFragment() {
    private lateinit var claimPoViewModel: ClaimPoViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimPoViewModel = viewModel as ClaimPoViewModel
        getData()
        claimPoViewModel.init()
        return binding.root
    }

    private fun getData() {
        if (activity != null && activity!!.intent.hasExtra(Constants.MODEL)) {
            val po = activity!!.intent.getSerializableExtra(Constants.MODEL) as Po
            claimPoViewModel.po = po
        }
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val choosePoDetailDialog = ChoosePoDetailDialog()
            choosePoDetailDialog.show((activity as FragmentActivity).supportFragmentManager, "")
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimPoViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_po
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getLayoutRecycleViewL1Id(): Int {
        return R.layout.fragment_claim_po_item
    }

    override fun getLayoutRecycleViewL2Id(): Int {
        return R.layout.fragment_claim_po_item2
    }

    override fun getRecycleViewL2Id(): Int {
        return R.id.claim_details
    }
}