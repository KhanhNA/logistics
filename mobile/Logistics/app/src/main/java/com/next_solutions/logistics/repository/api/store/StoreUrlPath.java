package com.next_solutions.logistics.repository.api.store;

public interface StoreUrlPath {
    String GET_STORE = "/stores";
    String GET_INVENTORY_IGNORE_EXPIRE_DATE = "/stores/inventory-ignore-expire-date";
    String GET_STORES_DC = "/stores/by-provide-store";
}
