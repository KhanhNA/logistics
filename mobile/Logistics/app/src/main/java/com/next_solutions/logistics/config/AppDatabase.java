package com.next_solutions.logistics.config;

import android.content.Context;

import com.next_solutions.logistics.models.dao.PackingTypeDao;
import com.next_solutions.logistics.models.dao.PalletDao;
import com.next_solutions.logistics.models.dao.ProductDao;
import com.next_solutions.logistics.models.dao.ProductDescriptionDao;
import com.next_solutions.logistics.models.dao.ProductPackingDao;
import com.next_solutions.logistics.models.dao.StoreDao;
import com.next_solutions.logistics.models.entity.PackingTypeEntity;
import com.next_solutions.logistics.models.entity.PalletEntity;
import com.next_solutions.logistics.models.entity.ProductDescriptionEntity;
import com.next_solutions.logistics.models.entity.ProductEntity;
import com.next_solutions.logistics.models.entity.ProductPackingEntity;
import com.next_solutions.logistics.models.entity.StoreEntity;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


@Database(entities = {
        ProductEntity.class,
        ProductDescriptionEntity.class,
        PalletEntity.class,
        StoreEntity.class,
        PackingTypeEntity.class,
        ProductPackingEntity.class}, version = 12, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ProductDao productDao();
    public abstract ProductDescriptionDao productDescriptionDao();
    public abstract PalletDao palletDao();
    public abstract StoreDao storeDao();
    public abstract PackingTypeDao packingTypeDao();
    public abstract ProductPackingDao productPackingDao();
    private static  AppDatabase instance;

    public static AppDatabase getDatabase(final Context context) {
        if (instance == null) {
            synchronized (AppDatabase.class) {
                if (instance == null) {
                    instance =
                            Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "Logistics")
                                    .fallbackToDestructiveMigration()
                                    .build();
                }
            }
        }
        return instance;
    }


}
