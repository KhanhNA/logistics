package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Po extends Base {
    private Distributor distributor;

    private Manufacturer manufacturer;
    private String description;

    private String code;

    private String qRCode;

    private Store store;
    private String deliveryAddress;
    private String deliveryDate;
    private String approveDate;

    private String status;
    private String approveUser;
    private String importDate;
    private String importUser;
    private List<PoDetail> poDetails;
    private ImportPo importPo;
    private BigDecimal total;
    public String rejectReason;
    public User importUserObj;
    public User approveUserObj;
    public User createUserObj;

    public String createDateString;

    public List<PoDetail> getPoDetails() {
        return poDetails;
    }

    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date printedDate;


    public enum PoStatus {
        //chu y: thu tu phai tuong dong voi status trong @array/po_status
        NEW("NEW"),
        APPROVED("APPROVED"),
        ARRIVED_VIETNAM_PORT("ARRIVED_VIETNAM_PORT"),
        ARRIVED_MYANMAR_PORT("ARRIVED_MYANMAR_PORT"),
        IMPORTED("IMPORTED"),
        REJECT("REJECT");





//        NEW(0),
//        APPROVE(1),
//        IMPORTED(2),
//        CANCEL(-1);
        private String value;

        PoStatus(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
        public static String getValue(int index){
            switch (index){
                case 0:
                    return "NEW";
                case 1:
                    return "APPROVED";
                case 2:
                    return "ARRIVED_VIETNAM_PORT";
                case 3:
                    return "ARRIVED_MYANMAR_PORT";
                case 4:
                    return "IMPORTED";
                case 5:
                    return "REJECT";
            }
            return "";
        }
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getqRCode() {
        return qRCode;
    }

    public void setqRCode(String qRCode) {
        this.qRCode = qRCode;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(String approveDate) {
        this.approveDate = approveDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApproveUser() {
        return approveUser;
    }

    public void setApproveUser(String approveUser) {
        this.approveUser = approveUser;
    }

    public String getImportDate() {
        return importDate;
    }

    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }

    public String getImportUser() {
        return importUser;
    }

    public void setImportUser(String importUser) {
        this.importUser = importUser;
    }

    public void setPoDetails(List<PoDetail> poDetails) {
        this.poDetails = poDetails;
    }

    public ImportPo getImportPo() {
        return importPo;
    }

    public void setImportPo(ImportPo importPo) {
        this.importPo = importPo;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Date getPrintedDate() {
        return printedDate;
    }

    public void setPrintedDate(Date printedDate) {
        this.printedDate = printedDate;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public User getImportUserObj() {
        return importUserObj;
    }

    public void setImportUserObj(User importUserObj) {
        this.importUserObj = importUserObj;
    }

    public User getApproveUserObj() {
        return approveUserObj;
    }

    public void setApproveUserObj(User approveUserObj) {
        this.approveUserObj = approveUserObj;
    }

    public User getCreateUserObj() {
        return createUserObj;
    }

    public void setCreateUserObj(User createUserObj) {
        this.createUserObj = createUserObj;
    }
}