package com.next_solutions.logistics.features.import_statement.claim_import_statement

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.helper.ImportStatementHelper
import com.next_solutions.logistics.utils.Constants

@Suppress("UNCHECKED_CAST")
class ClaimImportStatementViewModel(application: Application) : BaseClaimViewModel(application) {
    lateinit var importStatement: ImportStatement
    lateinit var importDetails: List<ImportStatementDetail>

    override fun prepareClaim(): Claim {
        val details = baseModelsE as ArrayList<ClaimDetail>
        return Claim().apply {
            type = Claim.ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT
            referenceId = importStatement.id
            claimDetails = details
            store = Store().apply {
                code = importStatement.toStore.code
                id = importStatement.toStore.id
            }
            status = Claim.ClaimStatus.WAIT_APPROVE.value
            description = descriptionObservable.get()
            amenable = amenableObservable.get()
        }
    }

    override fun init() {
        val claimDetails = arrayListOf<ClaimDetail>()
        importDetails = ImportStatementHelper(importStatement).groupDetailByExpireDateAndProductPacking()
        importDetails.forEach { detail ->
            run {
                claimDetails.add(claimHelper.convertImportDetailToClaimDetail(detail))
            }
        }
        setData(claimDetails as List<BaseModel>?)
    }

    fun addDetail(detail: ImportStatementDetail) {
        val claimDetails = baseModelsE as ArrayList<ClaimDetail>
        notifyPosition.set(claimDetails.size)
        claimDetails.add(claimHelper.convertImportDetailToClaimDetail(detail))
        sendBackAction(Constants.NOTIFY_CHANGE_ITEM_ADD)
    }

}