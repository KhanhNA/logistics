package com.next_solutions.logistics.features.import_statement.import_statement_release.to_fc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseDialogFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.BaseViewModelFactory;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.DialogCallBack;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;

import static com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN;

public class ChoosePalletForImportFcDialog extends BaseDialogFragment {
    private ChoosePalletForImportFCViewModel palletVM;
    private DialogCallBack dialogCallBack;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setUpViewModel();
        setUpRecycleView();
        return binding.getRoot();
    }

    private void setUpViewModel() {
        palletVM = (ChoosePalletForImportFCViewModel) viewModel;
        ImportStatementFCViewModel importReleaseViewModel = new ViewModelProvider(getBaseActivity(), BaseViewModelFactory.getInstance(getBaseActivity()
                .getApplication(), ImportStatementFCViewModel.class))
                .get(ImportStatementFCViewModel.class);
        palletVM.setImportReleaseViewModel(importReleaseViewModel);
        palletVM.init();
    }

    private void setUpRecycleView() {
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.dialog_choose_pallet_for_import_fc_item, viewModel, (view, pallet) -> {
            dialogCallBack.callBack(pallet);
            dismiss();
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.scanCode) {
            Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
            startActivityForResult(qrScan, REQUEST_CODE_SCAN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            String result = data.getStringExtra(Constants.RESULT_SCAN);
            palletVM.getTextSearch().set(result);
            palletVM.search();
        }
        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_choose_pallet_for_import_fc;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ChoosePalletForImportFCViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.pallets;
    }

    public void setDialogCallBack(DialogCallBack dialogCallBack) {
        this.dialogCallBack = dialogCallBack;
    }
}
