package com.next_solutions.logistics.component.store

import android.util.Log
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Rule
import com.next_solutions.logistics.models.Store
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING
import java.util.*

class FCAndDCRelateComponent(private val baseViewModel: BaseViewModel<BaseModel>) : BaseStoreComponent() {
    init {
        callApiGetFromStore()

    }

    override fun callApiGetFromStore() {
        val exceptIds = ArrayList<Long>()
        exceptIds.add(-1L)
        baseViewModel.callApi(Service.callStoreService()
                .getFromStore(exceptIds, false, AppUtils.FIRST_PAGE, Int.MAX_VALUE, true, EMPTY_STRING)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Pageable<Store>>(baseViewModel.appException, Pageable::class.java, Store::class.java, null) {
                    override fun getDataSuccess(storePageable: Pageable<Store>) {
                        getStoreSuccess(storePageable)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message)
                    }
                }))
    }


    private fun getStoreSuccess(page: Pageable<Store>) {
        stores.clear()
        stores.addAll(page.content)
        if (canViewDCInventory()) {
            addDCToListStore()
        }else{
            baseViewModel.search()
        }
    }


    private fun canViewDCInventory(): Boolean {
        val size = AppUtils.rule.userAuthentication.principal.roles.size
        for (i in 0 until size) {
            val roleName = AppUtils.rule.userAuthentication.principal.roles[i].roleName
            if (roleName == Rule.RuleType.TKFC.value) {
                return true
            }
        }
        return false
    }

    private fun addDCToListStore() {
        val storeIds: List<Long> = getStoreFCIds()
        baseViewModel.callApi(Service.callStoreService().getStoreDC(storeIds)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<Store>>(baseViewModel.appException, Store::class.java, MutableList::class.java) {
                    override fun getDataSuccess(stores: List<Store>) {
                        getStoreDCSuccess(stores)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message)
                    }
                }))
    }

    private fun getStoreFCIds(): List<Long> {
        val storeIds = ArrayList<Long>()
        for (store in stores) {
            storeIds.add(store.getId())
        }
        return storeIds
    }

    private fun getStoreDCSuccess(stores: List<Store>) {
        this.stores.addAll(stores)
        baseViewModel.search()
    }


}