package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class RepackingPlanningDetailRepacked extends Base {

    private String code;

    private String qRCode;

    private RepackingPlanningDetail repackingPlanningDetail;

    private ProductPacking productPacking;

    private Long quantity;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getqRCode() {
        return qRCode;
    }

    public void setqRCode(String qRCode) {
        this.qRCode = qRCode;
    }

    public RepackingPlanningDetail getRepackingPlanningDetail() {
        return repackingPlanningDetail;
    }

    public void setRepackingPlanningDetail(RepackingPlanningDetail repackingPlanningDetail) {
        this.repackingPlanningDetail = repackingPlanningDetail;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}