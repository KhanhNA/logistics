package com.next_solutions.logistics.features.import_statement.import_statement_release.to_fc;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.Pageable;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.Pallet;
import com.next_solutions.logistics.repository.Service;
import com.next_solutions.logistics.utils.Constants;

@SuppressWarnings("unchecked")
public class ChoosePalletForImportFCViewModel extends BaseViewModel {
    private ImportStatementFCViewModel importReleaseViewModel;
    private ObservableField<String> textSearch = new ObservableField<>(Constants.EMPTY_STRING);

    public ChoosePalletForImportFCViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void init() {
        search();
    }


    @Override
    public void search() {
        callApi(Service.callPalletService()
                .findPallet(
                        getCurrentPageNumber(),
                        AppUtils.PAGE_SIZE,
                        Pallet.Status.ACTIVE.getValue(),
                        Pallet.PalletStep.IMPORT_STATEMENT_UPDATE_PALLET.getValue(),
                        importReleaseViewModel.getStoreFC().getId(),
                        textSearch.get())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<Pageable<Pallet>>(appException, Pageable.class, Pallet.class, null) {
                    @Override
                    public void getDataSuccess(Pageable<Pallet> pageable) {
                        getPalletSuccess(pageable);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    private void getPalletSuccess(Pageable<Pallet> pageable) {
        setPage(pageable);
        setData(pageable.getContent());
    }


    void setImportReleaseViewModel(ImportStatementFCViewModel importReleaseViewModel) {
        this.importReleaseViewModel = importReleaseViewModel;
    }

    public ObservableField<String> getTextSearch() {
        return textSearch;
    }

    public void setTextSearch(ObservableField<String> textSearch) {
        this.textSearch = textSearch;
    }
}
