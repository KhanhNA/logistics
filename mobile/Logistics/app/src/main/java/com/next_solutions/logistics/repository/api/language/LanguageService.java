package com.next_solutions.logistics.repository.api.language;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;

import static com.next_solutions.logistics.repository.api.language.LanguageUrlPath.GET_ALL_LANGUAGES;

public interface LanguageService {
    //  ========================LANGUAGES========================
    @GET(GET_ALL_LANGUAGES)
    Single<Response<ResponseBody>> getAllLanguages();
}
