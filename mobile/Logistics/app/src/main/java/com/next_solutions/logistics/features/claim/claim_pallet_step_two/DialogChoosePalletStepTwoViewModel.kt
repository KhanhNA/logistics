package com.next_solutions.logistics.features.claim.claim_pallet_step_two

import android.app.Application
import android.util.Log
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.features.claim.update_claim.UpdateClaimViewModel
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.Pallet
import com.next_solutions.logistics.models.PalletDetail
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants

@Suppress("UNCHECKED_CAST")
class DialogChoosePalletStepTwoViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var claimViewModel: ClaimPalletStepTwoViewModel
    val searchComponent = SearchComponent()
    var stockId: Long = 0
    var updateClaimModel: UpdateClaimViewModel? = null;
    override fun init() {
        clearDataRecycleView()
        searchComponent.resetData()
        search()
    }

    override fun search() {
        if((stockId == 0L) && (claimViewModel == null || claimViewModel.storeComponent == null || claimViewModel.storeComponent.getSelectedStore() == null)){
            return;
        }
        val exceptIds = HashMap<String, String>()
        var details: List<ClaimDetail>? = null
        if(updateClaimModel != null){
            details = updateClaimModel!!.baseModelsE as List<ClaimDetail>
        }else if(claimViewModel != null && claimViewModel.baseModelsE != null){
            details = claimViewModel.baseModelsE as List<ClaimDetail>
        }

//        val details = claimViewModel.baseModelsE as List<ClaimDetail>
        var storeId = stockId
        if(stockId == 0L){
            storeId = claimViewModel.storeComponent.getSelectedStore()?.id!!
        }
        val distributorId = null //claimViewModel.distributorComponent.getSelectedDistributor()!!.id
        details?.forEach { claimDetail: ClaimDetail ->
            run {
                exceptIds.put(claimDetail.productPacking.id.toString(), claimDetail.expireDate)
            }
        }
        if (storeId != null) {
            findInventory(distributorId, exceptIds, storeId)
        }
    }

    private fun findInventory(distributorId: Long?, exceptIds: HashMap<String, String>, storeId: Long) {
        callApi(Service.callPalletService()
                .findInventory(storeId, distributorId,
                        Pallet.PalletStep.IMPORT_STATEMENT_UPDATE_PALLET.value, AppUtils.PAGE_SIZE, currentPageNumber, searchComponent.code.get().toString(), exceptIds)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Pageable<PalletDetail>>(appException, Pageable::class.java, PalletDetail::class.java, null) {
                    override fun getDataSuccess(page: Pageable<PalletDetail>?) {
                        setPage(page)
                        setData(page!!.content as List<BaseModel>?)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone!!.message)
                    }
                }))
    }

}