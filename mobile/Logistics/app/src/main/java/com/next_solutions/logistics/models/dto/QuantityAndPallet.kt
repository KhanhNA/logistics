package com.next_solutions.logistics.models.dto

import com.next_solutions.logistics.models.Pallet

class QuantityAndPallet {
    lateinit var pallet: Pallet
    var quantity: Long = 0
}