package com.next_solutions.logistics.base.import_po

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseFragment
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ItemOffsetDecoration
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.ImportPo
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

@Suppress("UNCHECKED_CAST")
abstract class BaseImportPoFragment : BaseFragment() {
    private lateinit var baseViewModel: BaseImportPoViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        baseViewModel = viewModel as BaseImportPoViewModel
        getData()
        setUpRecycleView()
        baseViewModel.init()
        return binding.root
    }

    private fun setUpRecycleView() {
        recyclerView.apply {
            adapter = ImportPoAdapter(baseViewModel, context, (activity as FragmentActivity).supportFragmentManager)
        }
        val spacing = 1
        recyclerView.addItemDecoration(ItemOffsetDecoration(spacing))
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (SCAN_CODE == view!!.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_SCAN) {
            baseViewModel.searchComponent.setData(data!!.getStringExtra(RESULT_SCAN))
            baseViewModel.search()
        }
    }

    private fun getData() {
        if (activity != null) {
            if (activity!!.intent.hasExtra(MODEL)) {
                val importPo = activity!!.intent.getSerializableExtra(MODEL) as ImportPo
                baseViewModel.importPo = importPo
                baseViewModel.store = importPo.store
            }
            if (activity!!.intent.hasExtra(CLAIM_DETAILS)) {
                val claimDetails = activity!!.intent.getSerializableExtra(CLAIM_DETAILS) as Array<Any>
                baseViewModel.claimDetails = arrayListOf()
                for (obj in claimDetails) {
                    baseViewModel.claimDetails!!.add(obj as ClaimDetail)
                }
                baseViewModel.hideButtonViewClaim()
            }
        }
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            NOTIFY_CHANGE -> {
                val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
                recyclerView.layoutAnimation = controller
                recyclerView.adapter!!.notifyDataSetChanged()
                recyclerView.scheduleLayoutAnimation()
            }
            SCROLL_TO_POSITION -> {
                scrollToPosition()
                showAlertWarning(R.string.invalid_input)
            }
            SUCCESS -> {
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
                showAlertSuccess(TIME_AFTER_DELAY, R.string.update_success, null)
            }
            SEARCH -> {
                scrollToPosition()
            }
        }
    }

    private fun scrollToPosition() {
        baseViewModel.lastNotifyPosition?.let { recyclerView.adapter!!.notifyItemChanged(it) }
        recyclerView.adapter!!.notifyItemChanged(baseViewModel.notifyPosition.get()!!)
        layoutManager.scrollToPositionWithOffset(baseViewModel.notifyPosition.get()!!, 20)
    }

}