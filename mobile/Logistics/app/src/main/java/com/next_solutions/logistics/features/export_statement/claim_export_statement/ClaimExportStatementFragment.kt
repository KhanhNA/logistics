package com.next_solutions.logistics.features.export_statement.claim_export_statement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimFragment
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING
import com.next_solutions.logistics.utils.Constants.MODEL

class ClaimExportStatementFragment : BaseClaimFragment() {
    private lateinit var claimExportStatementViewModel: ClaimExportStatementViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimExportStatementViewModel = viewModel as ClaimExportStatementViewModel
        getData()
        claimExportStatementViewModel.init()
        return binding.root
    }

    private fun getData() {
        val exportStatement = activity?.intent?.getSerializableExtra(MODEL)
        claimExportStatementViewModel.exportStatement = exportStatement as ExportStatement
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val dialog = DialogChooseExportStatementDetail()
            dialog.show((activity as FragmentActivity).supportFragmentManager, EMPTY_STRING)
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_claim_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimExportStatementViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_export_statement
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}