package com.next_solutions.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.next_solutions.base.SingleLiveEvent;
import com.next_solutions.logistics.config.AppDatabase;
import com.next_solutions.logistics.models.dao.PalletDao;
import com.next_solutions.logistics.models.entity.PalletEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;


public class PalletRepository {
    private PalletDao palletDao;
    private LiveData<List<PalletEntity>> allPallet;
    private SingleLiveEvent<List<PalletEntity>> allPalletV2 = new SingleLiveEvent<>();
    public PalletRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        palletDao = db.palletDao();
    }

    public void getAllPalletFromLocal() {
        allPallet = palletDao.getAllPallet();
    }
    public void insert(PalletEntity entity) {
        new InsertAsyncTask(palletDao).execute(entity);
    }
    public void deleteAll(){
        Completable.fromAction(() -> palletDao.deleteAll()).subscribeOn(Schedulers.io()).subscribe();

    }
    private static class InsertAsyncTask extends AsyncTask<PalletEntity, Void, Void> {

        private PalletDao mAsyncTaskDao;

        InsertAsyncTask(PalletDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final PalletEntity... params) {
            mAsyncTaskDao.insert(params);
            return null;
        }
    }
}
