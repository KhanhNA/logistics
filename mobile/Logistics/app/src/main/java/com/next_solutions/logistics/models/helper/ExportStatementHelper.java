package com.next_solutions.logistics.models.helper;

import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.models.ExportStatementDetail;
import com.next_solutions.logistics.models.Pallet;
import com.next_solutions.logistics.models.ProductPacking;
import com.next_solutions.logistics.models.StoreProductPacking;
import com.next_solutions.logistics.models.StoreProductPackingDetail;
import com.next_solutions.logistics.models.dto.ExpireDateAndProductPacking;
import com.next_solutions.logistics.models.dto.ImportStatementDetailDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExportStatementHelper {
    private final ExportStatement exportStatement;

    public ExportStatementHelper(ExportStatement exportStatement) {
        this.exportStatement = exportStatement;
    }

    public Map<ExpireDateAndProductPacking, ExportStatementDetail> groupDetailByExpireDateAndProductPacking() {
        HashMap<ExpireDateAndProductPacking, ExportStatementDetail> mappingExportDetail = new HashMap<>();
        if (this.exportStatement != null) {
            List<ExportStatementDetail> exportStatementDetails = this.exportStatement.getExportStatementDetails();

            for (ExportStatementDetail detail : exportStatementDetails) {
                //get data
                StoreProductPackingDetail storeProductPackingDetail = detail.getStoreProductPackingDetail();
                StoreProductPacking storeProductPacking = storeProductPackingDetail.getStoreProductPacking();
                String expireDate = storeProductPacking.getExpireDate();
                ProductPacking productPacking = storeProductPacking.getProductPacking();

                //convert
                ExpireDateAndProductPacking key = new ExpireDateAndProductPacking(expireDate, productPacking.getId());
                if (!mappingExportDetail.containsKey(key)) {
                    mappingExportDetail.put(key, detail);
                } else {
                    ExportStatementDetail exportStatementDetail = mappingExportDetail.get(key);
                    if (exportStatementDetail != null) {
                        exportStatementDetail.setQuantity(exportStatementDetail.getQuantity() + detail.getQuantity());
                        mappingExportDetail.put(key, exportStatementDetail);
                    }
                }
            }
        }
        return mappingExportDetail;
    }

    private Map<String, ExportStatementDetail> groupDetailByExpireDateAndProductPackingAndPallet() {
        HashMap<String, ExportStatementDetail> mappingExportDetail = new HashMap<>();
        if (this.exportStatement != null) {
            List<ExportStatementDetail> exportStatementDetails = this.exportStatement.getExportStatementDetails();
            if (exportStatementDetails != null) {
                for (ExportStatementDetail detail : exportStatementDetails) {
                    //get data
                    StoreProductPackingDetail storeProductPackingDetail = detail.getStoreProductPackingDetail();
                    StoreProductPacking storeProductPacking = storeProductPackingDetail.getStoreProductPacking();
                    String expireDate = storeProductPacking.getExpireDate();
                    ProductPacking productPacking = storeProductPacking.getProductPacking();
                    String palletCode = detail.getPalletDetail().getPallet().getCode();

                    //convert
                    String key = expireDate + productPacking.getId() + palletCode;
                    if (!mappingExportDetail.containsKey(key)) {
                        mappingExportDetail.put(key, minimizeExportStatementDetail(detail));
                    } else {
                        ExportStatementDetail exportStatementDetail = mappingExportDetail.get(key);
                        if (exportStatementDetail != null) {
                            exportStatementDetail.setQuantity(exportStatementDetail.getQuantity() + detail.getQuantity());
                            mappingExportDetail.put(key, exportStatementDetail);
                        }
                    }
                }
            }
        }
        return mappingExportDetail;
    }

    private ExportStatementDetail minimizeExportStatementDetail(ExportStatementDetail exportStatementDetail) {
        ExportStatementDetail result = new ExportStatementDetail();
        result.setProductPacking(exportStatementDetail.getProductPacking());
        result.setStoreProductPackingDetail(exportStatementDetail.getStoreProductPackingDetail());
        result.setExpireDate(exportStatementDetail.getExpireDate());
        result.setPalletDetail(exportStatementDetail.getPalletDetail());
        result.setQuantity(exportStatementDetail.getQuantity());
        return result;
    }


    public List<ImportStatementDetailDTO> groupExportDetailFromDC() {
        List<ImportStatementDetailDTO> result = new ArrayList<>();
        Map<ExpireDateAndProductPacking, ExportStatementDetail> mapping = groupDetailByExpireDateAndProductPacking();
        for (ExpireDateAndProductPacking key : mapping.keySet()) {
            ExportStatementDetail statementDetail = mapping.get(key);
            if (statementDetail != null) {
                result.add(convertExportDetailToImportStatementDTO(statementDetail));
            }
        }
        return result;
    }

    public List<ImportStatementDetailDTO> groupExportDetailFromFC() {
        List<ImportStatementDetailDTO> result = new ArrayList<>();
        Map<String, ExportStatementDetail> mapping = groupDetailByExpireDateAndProductPackingAndPallet();
        for (String key : mapping.keySet()) {
            ExportStatementDetail statementDetail = mapping.get(key);
            if (statementDetail != null) {
                result.add(convertExportDetailToImportStatementDTO(statementDetail));
            }
        }
        return result;
    }

    private ImportStatementDetailDTO convertExportDetailToImportStatementDTO(ExportStatementDetail exportDetail) {
        Pallet exportDetailPallet = null;
        if (exportDetail.getPalletDetail() != null) {
            exportDetailPallet = exportDetail.getPalletDetail().getPallet();
        }
        ProductPacking productPacking = exportDetail.getProductPacking();
        ImportStatementDetailDTO result = new ImportStatementDetailDTO();

        result.setProductName(productPacking.getProduct().getName());
        result.setProductPackingCode(productPacking.getCode());
        result.setProductPackingId(productPacking.getId());
        result.setExpireDate(exportDetail.getExpireDate());
        result.originalQuantity = exportDetail.getQuantity();
        result.setQuantity(0L);
        result.packingUom = productPacking.getUom();
        result.setPackingQuantity(productPacking.getPackingType().getQuantity());
        if (exportDetailPallet != null) {
            Pallet pallet = new Pallet();
            pallet.setDisplayName(exportDetailPallet.getDisplayName());
            pallet.setCode(exportDetailPallet.getCode());
            result.pallet = pallet;
        }
        result.storeProductPackingDetailCode = exportDetail.getStoreProductPackingDetail().getCode();
        result.fromClaim = false;

        return result;
    }


}
