package com.next_solutions.logistics.features.claim.find_claim

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.component.claim.status.ClaimStatusComponent
import com.next_solutions.logistics.component.claim.type.ClaimTypeComponent
import com.next_solutions.logistics.component.date.ChooseDateComponent
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.component.store.FCAndDCRelateComponent
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING
import com.next_solutions.logistics.utils.Constants.ERROR_PROCESS
import com.next_solutions.logistics.utils.DateUtils

class ClaimStatementViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    var amenableObservable = ObservableField(EMPTY_STRING)

    val claimStatusComponent = ClaimStatusComponent()
    val description = ObservableField(EMPTY_STRING)

    val claimTypesComponent = ClaimTypeComponent()

    var fcAndDCRelateComponent = FCAndDCRelateComponent(this)
    var searchComponent = SearchComponent()
    val chooseDate = ChooseDateComponent()

    override fun init() {
        clearDataFormSearch()
        search()
    }

    private fun clearDataFormSearch() {
        claimStatusComponent.resetData()
        fcAndDCRelateComponent.resetData()
        claimTypesComponent.resetData()
        description.set(EMPTY_STRING)
        searchComponent.resetData()
        amenableObservable.set(EMPTY_STRING)
        amenableObservable.notifyChange()
    }

    fun searchByCode(code: String) {
        searchComponent.setData(code)
        search()
    }

    override fun search() {
        val from = DateUtils.clearTimeAndConvertDateToString(chooseDate.fromDate.get(), DateUtils.DATE_FORMAT)
        val to = DateUtils.clearTimeAndConvertDateToString(chooseDate.toDate.get(), DateUtils.DATE_FORMAT)
        val storeId = fcAndDCRelateComponent.getSelectedStore()?.id
        val selectedClaimStatus = claimStatusComponent.getSelectedClaimStatus()
        if (storeId != null) {
            callApiSearch(selectedClaimStatus, storeId, from, to)
        }
    }

    private fun callApiSearch(selectedClaimStatus: Int?, storeId: Long, from: String, to: String) {
        callApi(Service.callClaimService()
                .findClaim(amenableObservable.get()!!,
                        searchComponent.code.get().toString(),
                        description.get()!!, currentPageNumber, AppUtils.PAGE_SIZE,
                        selectedClaimStatus, storeId,
                        claimTypesComponent.getSelectedType(), from, to)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<Pageable<Claim>>(appException, Pageable::class.java, Claim::class.java, null) {
                    override fun getDataSuccess(page: Pageable<Claim>?) {
                        setPage(page)
                        setData(page!!.content as List<BaseModel>?)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone?.message)
                    }

                }))
    }

    fun setDate(isFromDate: Boolean?, year: Int, month: Int, dayOfMonth: Int) {
        if (isFromDate!!) {
            chooseDate.fromDate.set(DateUtils.convertDayMonthYearToDate(dayOfMonth, month, year))
        } else {
            chooseDate.toDate.set(DateUtils.convertDayMonthYearToDate(dayOfMonth, month, year))
        }

    }
}