package com.next_solutions.logistics.features.import_po.import_po_in_pallet

import android.app.Application
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.base.import_po.BaseImportPoViewModel
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.ImportPo
import com.next_solutions.logistics.models.ImportPoDetail
import com.next_solutions.logistics.models.ImportPoDetailPallet
import com.next_solutions.logistics.models.helper.ImportPoHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ImportPoInPalletViewModel(application: Application) : BaseImportPoViewModel(application) {

    override fun updateTotalQuantity() {
        this.importPo.importPoDetails.forEach { detail ->
            run {
                val key = getKeyMapping(detail)
                detail.importPoDetailPallets.forEach { detailPallet ->
                    run {
                        if (currentTotalQuantity.get()!![key] == null) {
                            currentTotalQuantity.get()!![key] = 0
                        }
                        currentTotalQuantity.get()!![key] =
                                currentTotalQuantity.get()!![key]!! + detailPallet.quantity
                    }
                }
            }
        }
    }

    override fun avoidRecursiveData(): List<ImportPoDetail> {
        val importPoId = this.importPo.id
        val result = arrayListOf<ImportPoDetail>()
        val importPo = ImportPo().apply { id = importPoId }

        this.importPo.importPoDetails.forEach { importPoDetail: ImportPoDetail ->
            run {
                result.add(ImportPoHelper().convertDetailPalletAvoidRecursive(importPoDetail, importPo))
            }
        }
        return result
    }

    fun onCreate() {
        if (validateInput()) {
            val importPoDetailPallets = prepareDataForApi()
            val importPoId = importPo.getId()
            buttonEnable.set(false)
            callApiUpdatePallet(importPoId, importPoDetailPallets)
        } else {
            sendBackAction(Constants.SCROLL_TO_POSITION)
        }
    }

    private fun callApiUpdatePallet(importPoId: Long?, importPoDetailPallets: List<ImportPoDetailPallet>) {
        callApi(Service.callImportPoService()
                .updatePallet(importPoId, importPoDetailPallets)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(respone: ResponseStatus) {
                        if (respone.code == Constants.CODE_SUCCESS) {
                            sendBackAction(Constants.SUCCESS)
                        }
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        buttonEnable.set(true)
                    }
                }))
    }

    private fun validateInput(): Boolean {
        var position = 0
        this.listData.forEach { detail ->
            run {
                if (detail.quantity != currentTotalQuantity.get()!![getKeyMapping(detail)]!!) {
                    updateLastNotifyPosition()
                    updateNotifyPosition(position, detail, false)
                    return false
                }
                if (detail.importPoDetailPallets.asSequence().find { (it.pallet.id == null) || (it.quantity == 0L) } != null) {
                    updateLastNotifyPosition()
                    updateNotifyPosition(position, detail, false)
                    return false
                }
                position++
            }
        }
        return true
    }


}