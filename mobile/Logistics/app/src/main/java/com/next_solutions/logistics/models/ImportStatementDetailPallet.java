package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class ImportStatementDetailPallet extends Base {

    private Long importStatementId;

    private Long importStatementDetailId;

    private Long productPackingId;

    private String expireDate;

    private Long quantity;

    private Long storeProductPackingDetailId;
    private StoreProductPackingDetail storeProductPackingDetail;
    private Pallet pallet;

    private PalletDetail palletDetail;

    public Long getImportStatementId() {
        return importStatementId;
    }

    public void setImportStatementId(Long importStatementId) {
        this.importStatementId = importStatementId;
    }

    public Long getImportStatementDetailId() {
        return importStatementDetailId;
    }

    public void setImportStatementDetailId(Long importStatementDetailId) {
        this.importStatementDetailId = importStatementDetailId;
    }

    public Long getProductPackingId() {
        return productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getStoreProductPackingDetailId() {
        return storeProductPackingDetailId;
    }

    public void setStoreProductPackingDetailId(Long storeProductPackingDetailId) {
        this.storeProductPackingDetailId = storeProductPackingDetailId;
    }

    public Pallet getPallet() {
        return pallet;
    }

    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }

    public PalletDetail getPalletDetail() {
        return palletDetail;
    }

    public void setPalletDetail(PalletDetail palletDetail) {
        this.palletDetail = palletDetail;
    }

    public StoreProductPackingDetail getStoreProductPackingDetail() {
        return storeProductPackingDetail;
    }

    public void setStoreProductPackingDetail(StoreProductPackingDetail storeProductPackingDetail) {
        this.storeProductPackingDetail = storeProductPackingDetail;
    }
}
