package com.next_solutions.logistics.models;

import com.yanzhenjie.album.AlbumFile;

import java.io.Serializable;

public class Image implements Serializable {
    private AlbumFile albumFile;
    private ClaimImage claimImage;

    public boolean isLocalImage() {
        return albumFile != null;
    }

    public AlbumFile getAlbumFile() {
        return albumFile;
    }

    public void setAlbumFile(AlbumFile albumFile) {
        this.albumFile = albumFile;
    }

    public ClaimImage getClaimImage() {
        return claimImage;
    }

    public void setClaimImage(ClaimImage claimImage) {
        this.claimImage = claimImage;
    }
}
