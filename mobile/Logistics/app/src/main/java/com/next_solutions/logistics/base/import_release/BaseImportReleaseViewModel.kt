package com.next_solutions.logistics.base.import_release

import android.app.Application
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.R
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.ImportStatementHelper
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath
import com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.DateUtils

@Suppress("UNCHECKED_CAST")
abstract class BaseImportReleaseViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    var importStatement: ImportStatement? = null
    var descriptionObservable = ObservableField(EMPTY_STRING)
    var errorPosition = -1
    var lastErrorPosition = -1
    private val qrCodeMapping = HashMap<String, Int>()
    var claimDeliveryDetails: List<ClaimDetail>? = null
    var claimImportDetails: List<ClaimDetail>? = null


    var buttonEnable = ObservableField(true)
    var buttonReleaseEnable = ObservableField(true)
    var buttonRelease = ObservableField(true)


    var buttonCreateClaimDelivery = ObservableField(false)
    var buttonViewClaimDelivery = ObservableField(false)
    var claimDelivery = ObservableField(false)

    var buttonCreateClaimImport = ObservableField(false)
    var buttonViewClaimImport = ObservableField(false)
    var claimImport = ObservableField(false)


    abstract fun prepareStatementDetail(): List<ImportStatementDetailPallet>
    abstract fun checkValidate(): Boolean
    abstract fun checkErrorPositionWhenClickCheckBox(position: Int)
    abstract fun isValidDetail(importStatementDetail: ImportStatementDetail)
    override fun init() {
        this.importStatement?.let {
            run {
                getClaimDeliveryReference()
            }
        }
    }

    private fun getClaimDeliveryReference() {
        callApi(Service.callClaimService()
                .findClaimByReference(importStatement!!.id, Claim.ClaimType.DELIVERY.value)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, List::class.java) {
                    override fun getDataSuccess(claimDeliveryDetails: List<ClaimDetail>) {
                        getClaimDeliverySuccess(claimDeliveryDetails)
                        getClaimImportReference()
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                        getClaimImportReference()
                    }
                }))
    }

    fun getClaimDeliverySuccess(claimDeliveryDetails: List<ClaimDetail>) {
        this.claimDeliveryDetails = claimDeliveryDetails
    }

    private fun getClaimImportReference() {
        callApi(Service.callClaimService()
                .findClaimByReference(importStatement!!.id, Claim.ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT.value)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, List::class.java) {
                    override fun getDataSuccess(claimImportDetails: List<ClaimDetail>) {
                        getClaimImportSuccess(claimImportDetails)
                        findImportStatementById()
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                        findImportStatementById()
                    }
                }))
    }

    private fun getClaimImportSuccess(claimImportDetails: List<ClaimDetail>) {
        this.claimImportDetails = claimImportDetails
    }

    private fun findImportStatementById() {
        callApi(Service.callImportStatementService()
                .findImportStatementById(this.importStatement!!.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportStatement>(appException, ImportStatement::class.java, null) {
                    override fun getDataSuccess(statement: ImportStatement?) {
                        getImportStatementSuccess(statement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone?.message + " " + this.javaClass.name)
                    }
                }))
    }

    private fun getImportStatementSuccess(statement: ImportStatement?) {
        this.importStatement = statement
        val importStatementDetails =
                ImportStatementHelper(importStatement!!).groupDetailByExpireDateAndProductPacking()
        checkButtonState()
        updateTotalQuantityImportDetail(importStatementDetails)
        createQRCodeMapping(importStatementDetails)
        subQuantityClaim(importStatementDetails)
        setDataForRecycleView(importStatementDetails)
    }

    private fun subQuantityClaim(importStatementDetails: List<ImportStatementDetail>) {
        if (!claimImportDetails.isNullOrEmpty()) {
            subQuantityByImportClaim(importStatementDetails)
        }
        if (!claimDeliveryDetails.isNullOrEmpty()) {
            subQuantityByDeliveryClaim(importStatementDetails)
        }
    }

    private fun createQRCodeMapping(importStatementDetails: List<ImportStatementDetail>) {
        importStatementDetails
                .asSequence()
                .forEachIndexed { index, importStatementDetail ->
                    run {
                        val qrCode = generateQRCodeFromImportStatementDetail(importStatementDetail)
                        qrCodeMapping[qrCode] = index
                    }
                }
    }

    private fun generateQRCodeFromImportStatementDetail(importStatementDetail: ImportStatementDetail): String {
        return importStatementDetail.productPacking.product.code + " " +
                importStatementDetail.productPacking.packingType.code + " " +
                DateUtils.convertStringDateToDifferentType(importStatementDetail.expireDate, DateUtils.DATE_FORMAT, "yyyyMMdd")
    }

    private fun checkButtonState() {
        if (claimDeliveryDetails == null) {
            claimDeliveryNotApproved()
        } else {
            if (claimDeliveryDetails!!.isNotEmpty()) {
                claimDeliveryApproved()
            } else {
                doNotHaveClaimDelivery()
            }
        }
        if (claimImportDetails == null) {
            claimNotApproved()
        } else {
            if (claimImportDetails!!.isNotEmpty()) {
                claimImportApproved()
            } else {
                doNotHaveClaimImport()
            }
        }
        if ((claimImportDetails == null) || (claimDeliveryDetails == null)) {
            buttonRelease.set(RuleHelper().hasRule(HttpRequest(Method.PATCH.value, ImportStatementUrlPath.ACTUALLY_IMPORT)))
            buttonReleaseEnable.set(false)
        } else {
            buttonRelease.set(RuleHelper().hasRule(HttpRequest(Method.PATCH.value, ImportStatementUrlPath.ACTUALLY_IMPORT)))
            buttonReleaseEnable.set(true)
        }
    }

    private fun claimDeliveryApproved() {
        buttonCreateClaimDelivery.set(false)
        buttonViewClaimDelivery.set(RuleHelper().hasRule(HttpRequest(Method.GET.value, ClaimUrlPath.FIND_CLAIM_BY_ID)))
        claimDelivery.set(true)
    }

    private fun claimDeliveryNotApproved() {
        buttonCreateClaimDelivery.set(false)
        buttonViewClaimDelivery.set(false)
        claimDelivery.set(false)
    }

    private fun doNotHaveClaimDelivery() {
        buttonCreateClaimDelivery.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
        buttonViewClaimDelivery.set(false)
        claimDelivery.set(false)
    }

    private fun claimImportApproved() {
        buttonCreateClaimImport.set(false)
        buttonViewClaimImport.set(RuleHelper().hasRule(HttpRequest(Method.GET.value, ClaimUrlPath.FIND_CLAIM_BY_ID)))
        claimImport.set(true)
    }

    private fun claimNotApproved() {
        buttonCreateClaimImport.set(false)
        buttonViewClaimImport.set(false)
        claimImport.set(false)
    }

    private fun doNotHaveClaimImport() {
        buttonCreateClaimImport.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
        buttonViewClaimImport.set(false)
        claimImport.set(false)
    }

    private fun subQuantityByImportClaim(importStatementDetails: List<ImportStatementDetail>) {
        claimImportDetails!!.forEach { claimDetail ->
            run {
                val findDetail = importStatementDetails.asSequence().find {
                    (it.productPacking.code == claimDetail.productPacking.code)
                            .and(it.expireDate == claimDetail.expireDate)
                }
                findDetail?.let {
                    findDetail.quantity -= claimDetail.quantity
                    findDetail.claimQuantityImport += claimDetail.quantity
                }
            }
        }
    }

    private fun subQuantityByDeliveryClaim(importStatementDetails: List<ImportStatementDetail>) {
        claimDeliveryDetails!!.forEach { claimDetail ->
            run {
                val findDetail = importStatementDetails.asSequence().find {
                    (it.productPacking.code == claimDetail.productPacking.code)
                            .and(it.expireDate == claimDetail.expireDate)
                }
                findDetail?.let {
                    findDetail.quantity -= claimDetail.quantity
                    findDetail.claimQuantityDelivery += claimDetail.quantity
                }
            }
        }
    }

    open fun setDataForRecycleView(data: List<ImportStatementDetail>) {
        setData(data as List<BaseModel>?)
    }

    private fun updateTotalQuantityImportDetail(importStatementDetails: List<ImportStatementDetail>) {
        for ((position, detail) in importStatementDetails.withIndex()) {
            if (detail.quantity != 0L) {
                detail.claimQuantityDelivery = 0
                detail.claimQuantityImport = 0
                detail.originalQuantity = detail.quantity
                val code = generateQRCodeFromImportStatementDetail(detail)
                qrCodeMapping[code] = position
            }
        }
    }

    fun releaseStatement() {
        if (checkValidate()) {
            buttonReleaseEnable.set(false)
            buttonEnable.set(false)
            val statementDetails = prepareStatementDetail()
            val importStatement = ImportStatement().apply {
                importStatementDetailPallets = statementDetails
                description = descriptionObservable.get()
            }
            callApi(Service.callImportStatementService()
                    .actuallyImport(this.importStatement!!.id, importStatement)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<ResponseStatus?>(appException, null, null, null) {
                        override fun getDataSuccess(responseStatus: ResponseStatus?) {
                            sendBackAction(CREATE_SUCCESS)
                        }

                        override fun onFailure(errorRespone: ErrorRespone) {
                            buttonEnable.set(true)
                            buttonReleaseEnable.set(true)
                        }
                    }))
        }
    }

    fun scanProductWithCode(code: String?) {
        code?.let {
            val str = AppUtils.changeFormCode(code)
            val position = qrCodeMapping[str]
            if (position != null) {
                baseModelsE[position].checked = true
                notifyChangeItem(position)
                sendBackAction(NOTIFY_CHANGE)
            }
        }
    }


    fun getColor(importStatementDetail: ImportStatementDetail): Int {
        val listData = baseModelsE
        if (listData.indexOf(importStatementDetail) == errorPosition) {
            return ContextCompat.getColor(getApplication(), R.color.light_red_2)
        }
        if (importStatementDetail.checked == null) {
            return ContextCompat.getColor(getApplication(), R.color.white)
        }
        return try {
            isValidDetail(importStatementDetail)
            ContextCompat.getColor(getApplication(), R.color.light_green)
        } catch (exception: Exception) {
            ContextCompat.getColor(getApplication(), R.color.light_red_2)
        }
    }


}