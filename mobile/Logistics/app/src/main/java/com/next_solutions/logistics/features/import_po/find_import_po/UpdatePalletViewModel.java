package com.next_solutions.logistics.features.import_po.find_import_po;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.Pageable;
import com.next_solutions.logistics.component.distributor.DistributorComponent;
import com.next_solutions.logistics.component.import_po_status.ImportPoStatusComponent;
import com.next_solutions.logistics.component.search.SearchComponent;
import com.next_solutions.logistics.component.store.FromFCComponent;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.Distributor;
import com.next_solutions.logistics.models.ImportPo;
import com.next_solutions.logistics.models.Store;
import com.next_solutions.logistics.repository.Service;

import java.util.ArrayList;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

@SuppressWarnings("unchecked")
public class UpdatePalletViewModel extends BaseViewModel {

    private FromFCComponent fromFCComponent = new FromFCComponent(this);
//    private DistributorComponent distributorComponent = new DistributorComponent(this, true);
    private SearchComponent searchComponent = new SearchComponent();
    private ImportPoStatusComponent importPoStatusComponent = new ImportPoStatusComponent();

    public UpdatePalletViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void doWhenChooseData(Object obj) {
        setData(new ArrayList<>());
    }

    @Override
    public void init() {
        resetDataSearch();
    }

    private void resetDataSearch() {
        clearDataRecycleView();
        searchComponent.resetData();
//        distributorComponent.resetData();
        fromFCComponent.resetData();
        importPoStatusComponent.resetData();
    }

    @Override
    public void search() {
        fromFCComponent.getStores();
        if (!fromFCComponent.getStores().isEmpty() ) {
            Store store = fromFCComponent.getSelectedStore();
            Distributor distributor = new Distributor(); //distributorComponent.getSelectedDistributor();
            Integer page = getCurrentPageNumber();
            callApiSearch(distributor, store, page);
        }
    }

    private void callApiSearch(Distributor distributor, Store store, Integer page) {
        if (distributor != null && store != null) {
            callApi(Service.callImportPoService()
                    .findImportPo(distributor.getId(), page, AppUtils.PAGE_SIZE,
                            importPoStatusComponent.getSelectedStatusPosition(), store.getId(), searchComponent.getCode().get())
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(new RxSingleSubscriber<Pageable<ImportPo>>(appException, Pageable.class, ImportPo.class, null) {
                        @Override
                        public void getDataSuccess(Pageable<ImportPo> importPoPageable) {
                            searchSuccess(importPoPageable);
                        }

                        @Override
                        public void onFailure(ErrorRespone errorRespone) {
                            Log.e(ERROR_PROCESS, errorRespone.getMessage());
                        }
                    }));
        }
    }

    private void searchSuccess(Pageable<ImportPo> pageable) {
        if (pageable != null) {
            setPage(pageable);
            setData(pageable.getContent());
        } else {
            sendBackAction(ERROR_PROCESS);
        }
    }

//    public DistributorComponent getDistributorComponent() {
//        return distributorComponent;
//    }

    public FromFCComponent getFromFCComponent() {
        return fromFCComponent;
    }

    public SearchComponent getSearchComponent() {
        return searchComponent;
    }

    public ImportPoStatusComponent getImportPoStatusComponent() {
        return importPoStatusComponent;
    }
}
