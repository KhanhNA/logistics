package com.next_solutions.logistics.features.claim.update_claim.po

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.PoDetail
import com.next_solutions.logistics.models.dto.ClaimObject

@Suppress("UNCHECKED_CAST")
class ChooseClaimPoDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {

    lateinit var updateClaimViewModel: UpdateClaimPoViewModel
    val searchComponent = SearchComponent()
    override fun init() {
        search()
    }

    override fun search() {
        val textSearch = searchComponent.getCodeSearch()
        setData(updateClaimViewModel.po.poDetails.asSequence()
                .filter { notContain(updateClaimViewModel.listData, it) }
                .filter {
                    it.product.name.contains(textSearch, true)
                            || (it.product.code.contains(textSearch, true))
                }
                .toList()
        )
    }

    private fun notContain(details: List<ClaimObject>, poDetail: PoDetail): Boolean {
        return details.asSequence().find { detail ->
            detail.productPackingId == poDetail.productPacking.id
        } == null
    }
}