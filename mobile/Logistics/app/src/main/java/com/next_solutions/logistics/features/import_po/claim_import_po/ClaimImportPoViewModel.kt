package com.next_solutions.logistics.features.import_po.claim_import_po

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE_ITEM_ADD

@Suppress("UNCHECKED_CAST")
class ClaimImportPoViewModel(application: Application) : BaseClaimViewModel(application) {
    lateinit var importPo: ImportPo
    override fun prepareClaim(): Claim {
        val details = baseModelsE as List<ClaimDetail>
        return Claim().apply {
            claimDetails = details
            type = Claim.ClaimType.IMPORT_PO_UPDATE_PALLET
            description = descriptionObservable.get()
            referenceId = importPo.id
            claimDetails = details
            store = Store().apply {
                code = importPo.store.code
                id = importPo.store.id
            }
            status = Claim.ClaimStatus.WAIT_APPROVE.value
            amenable = amenableObservable.get()
        }
    }

    override fun init() {
        val claimDetails = arrayListOf<ClaimDetail>()
        importPo.importPoDetails.forEach { details ->
            run {
                claimDetails.add(claimHelper.convertImportPoDetailToClaimDetail(details))
            }
        }
        setData(claimDetails as List<BaseModel>?)
    }

    fun addDetail(importPoDetail: ImportPoDetail) {
        val claimDetails = baseModelsE as ArrayList<ClaimDetail>
        notifyPosition.set(claimDetails.size)
        claimDetails.add(claimHelper.convertImportPoDetailToClaimDetail(importPoDetail))
        sendBackAction(NOTIFY_CHANGE_ITEM_ADD)
    }
}