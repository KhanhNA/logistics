package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.ClaimObject
import com.next_solutions.logistics.models.dto.ImportStatementDetailDTO
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING
import org.jetbrains.annotations.NotNull

class ClaimHelper {

    fun convertClaimDetailToClaimObject(claimDetail: ClaimDetail): ClaimObject {
        val productPacking = claimDetail.productPacking
        return ClaimObject().apply {
            claimDetails = arrayListOf()
            productPackingName = productPacking.product.name
            productPackingCode = productPacking.code
            productPackingId = productPacking.id
            productPackingUom = productPacking.uom
            quantity = 0
            productPackingType = productPacking.packingType.quantity
        }
    }


    fun convertPoDetailToClaimObject(detail: PoDetail): ClaimObject {
        val packing = detail.productPacking
        return ClaimObject().apply {
            claimDetails = arrayListOf(ClaimDetail().apply {
                quantity = 0
                productPacking = ProductPacking().apply {
                    code = packing.code
                    id = packing.id
                }
            })
            productPackingId = packing.id
            productPackingCode = packing.code
            productPackingName = packing.product.name
            productPackingUom = packing.uom
            productPackingType = packing.packingType.quantity
            quantity = detail.quantity
        }
    }

    fun convertImportPoDetailToClaimDetail(importPoDetail: ImportPoDetail): ClaimDetail {
        val packing = importPoDetail.productPacking
        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = packing.id
                code = packing.code
                product = Product().apply { name = packing.product.name }
                packingType = PackingType().apply { quantity = packing.packingType.quantity }
                uom = packing.uom
            }
            totalQuantity = importPoDetail.quantity
            quantity = 0
            expireDate = importPoDetail.expireDate
        }
    }

    fun convertRepackingDetailToClaimDetail(repackingDetail: RepackingPlanningDetail): ClaimDetail {
        val packing = repackingDetail.productPacking
        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = packing.id
                code = packing.code
                product = Product().apply { name = packing.product.name }
                packingType = PackingType().apply { quantity = packing.packingType.quantity }
                uom = packing.uom
            }
            totalQuantity = repackingDetail.quantity
            quantity = 0
            expireDate = repackingDetail.expireDateStr
        }
    }

    fun convertDetailRepackedToClaimDetail(repackingDetailRepacked: RepackingPlanningDetailRepacked): ClaimDetail {
        val packing = repackingDetailRepacked.productPacking
        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = packing.id
                code = packing.code
                product = Product().apply { name = packing.product.name }
                packingType = PackingType().apply { quantity = packing.packingType.quantity }
                uom = packing.uom
            }
            totalQuantity = repackingDetailRepacked.quantity
            quantity = 0
            expireDate = repackingDetailRepacked.repackingPlanningDetail.expireDateStr
        }
    }

    fun convertExportDetailToClaimDetail(exportDetail: ExportStatementDetail): ClaimDetail {
        val packing = exportDetail.productPacking
        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = packing.id
                code = packing.code
                product = Product().apply { name = packing.product.name }
                packingType = PackingType().apply { quantity = packing.packingType.quantity }
                uom = packing.uom
            }
            totalQuantity = exportDetail.quantity
            quantity = 0
            expireDate = exportDetail.expireDate
        }
    }

    fun convertImportDetailToClaimDetail(importStatementDetail: ImportStatementDetail): ClaimDetail {
        val packing = importStatementDetail.productPacking
        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = packing.id
                code = packing.code
                product = Product().apply { name = packing.product.name }
                packingType = PackingType().apply { quantity = packing.packingType.quantity }
                uom = packing.uom
            }
            totalQuantity = importStatementDetail.quantity
            quantity = 0
            expireDate = importStatementDetail.expireDate
        }
    }

    fun convertPalletDetailToClaimDetail(paldetail: PalletDetail): ClaimDetail {
        val packing = paldetail.productPacking
        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = packing.id
                code = packing.code
                product = Product().apply { name = packing.product.name }
                packingType = PackingType().apply { quantity = packing.packingType.quantity }
                uom = packing.uom
            }
            totalQuantity = paldetail.quantity
            quantity = 0
            expireDate = paldetail.expireDate
            palletId = paldetail.pallet.id


            if(paldetail.productPackingPrice != null) {
                val pp = ProductPackingPrice();
                pp.setId(paldetail.productPackingPrice.getId())
                productPackingPrice = pp;
            }
            if(paldetail != null){
                val pd = PalletDetail();
                pd.setId(paldetail.getId())
                if(paldetail.pallet != null){
                    val pl = Pallet();
                    pl.setId(paldetail.pallet.getId())
                    palletId = paldetail.pallet.getId()
                    pd.pallet = pl
                }

                this.palletDetail = pd

            }
            if(paldetail.storeProductPackingDetail != null){
                val sppd = StoreProductPackingDetail()
                sppd.setId(paldetail.storeProductPackingDetail.getId())
                storeProductPackingDetail = sppd
            }

        }
    }

    fun convertInventoryToClaimDetail(inventory: Inventory): ClaimDetail {
        val packing = inventory.productPacking
        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = packing.id
                code = packing.code
                product = Product().apply { name = packing.product.name }
                packingType = PackingType().apply { quantity = packing.packingType.quantity }
                uom = packing.uom
            }
            totalQuantity = palletDetail.quantity
            quantity = 0
            expireDate = palletDetail.expireDate



        }
    }

    fun minimizeClaimDetailWithoutQuantity(detail: ClaimDetail): ClaimDetail {
        val type = PackingType().apply { quantity = detail.productPacking.packingType.quantity }

        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = detail.productPacking.id
                code = detail.productPacking.code
                packingType = type
                uom = detail.productPacking.uom
                product = Product().apply { name = detail.productPacking.product.name }
            }
            expireDate = detail.expireDate
            quantity = 0
            totalQuantity = detail.totalQuantity


            if(detail.productPackingPrice != null) {
                val pp = ProductPackingPrice();
                pp.setId(detail.productPackingPrice.getId())
                productPackingPrice = pp;
            }
            if(detail.palletDetail != null){
                val pd = PalletDetail();
                pd.setId(detail.getId())
                if(detail.palletDetail.pallet != null) {
                    palletId = detail.palletDetail.pallet.getId()
                }
                this.palletDetail = pd

            }
            if(detail.storeProductPackingDetail != null){
                val sppd = StoreProductPackingDetail()
                sppd.setId(detail.storeProductPackingDetail.getId())
                storeProductPackingDetail = sppd
            }



        }
    }

    fun minimizeClaimDetailWithQuantity(detail: ClaimDetail): ClaimDetail {
        return ClaimDetail().apply {
            productPacking = ProductPacking().apply {
                id = detail.productPacking.id
                code = detail.productPacking.code
                packingType = PackingType().apply {
                    quantity = detail.productPacking.packingType.quantity
                    product = Product().apply { name = detail.productPacking.product.name }
                }
                uom = detail.productPacking.uom
            }
            expireDate = detail.expireDate
            quantity = detail.quantity
            totalQuantity = detail.totalQuantity

            if(detail.productPackingPrice != null) {
                val pp = ProductPackingPrice();
                pp.setId(detail.productPackingPrice.getId())
                productPackingPrice = pp;
            }
            if(detail.palletDetail != null){
                val pd = PalletDetail();
                pd.setId(detail.palletDetail.getId())
                palletId = detail.palletDetail.pallet.getId()
                palletDetail = pd

            }
            if(detail.storeProductPackingDetail != null){
                val sppd = StoreProductPackingDetail()
                sppd.setId(detail.storeProductPackingDetail.getId())
                storeProductPackingDetail = sppd
            }





        }
    }

    fun convertClaimDetailToImportPoDetail(detail: ClaimDetail): ImportPoDetail {
        val packing = detail.productPacking
        return ImportPoDetail().apply {
            productPacking = ProductPacking().apply {
                id = packing.id
                code = packing.code
                packingType = PackingType().apply {
                    quantity = packing.packingType.quantity
                }
                productPackingPrice = detail.productPackingPrice
                uom = packing.uom
                vat = packing.vat
                product = packing.product
            }
            expireDate = detail.expireDate
            quantity = 0
            originalQuantity = detail.quantity
            fromClaim = true
        }
    }

    fun convertClaimDetailToImportDetailDTO(claimDetail: @NotNull ClaimDetail): ImportStatementDetailDTO? {
        val packing = claimDetail.productPacking
        var palletClaim: Pallet? = null
        if (claimDetail.palletDetail != null) {
            palletClaim = claimDetail.palletDetail.pallet
        }
        return ImportStatementDetailDTO().apply {
            productName = packing.product.name
            productPackingCode = packing.code
            productPackingId = packing.id
            packingQuantity = packing.packingType.quantity
            packingUom = packing.uom
            originalQuantity = claimDetail.quantity
            quantity = claimDetail.quantity
            expireDate = claimDetail.expireDate
            if (palletClaim != null) {
                pallet = Pallet().apply {
                    displayName = palletClaim.displayName
                    code = palletClaim.code
                }
                storeProductPackingDetailCode = claimDetail.palletDetail.storeProductPackingDetail.code
            }
            fromClaim = true
        }
    }

    fun convertClaimDetailToImportStatementDetailPallet(claimDetail: ClaimDetail): ImportStatementDetailPallet {
        return ImportStatementDetailPallet().apply {
            pallet = Pallet().apply {
                code = EMPTY_STRING
                name = EMPTY_STRING
            }
            palletDetail = PalletDetail().apply {
                expireDate = claimDetail.expireDate
                productPacking = claimDetail.productPacking
            }
            quantity = claimDetail.quantity
        }
    }
}