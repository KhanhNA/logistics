package com.next_solutions.logistics.features.repacking_planning.new_repacking

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.models.RepackingPlanningDetail
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath
import com.next_solutions.logistics.utils.Constants

@Suppress("UNCHECKED_CAST")
class NewRepackingPlanningViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var repackingPlanning: RepackingPlanning
    val searchComponent = SearchComponent()
    var claimDetails: ArrayList<ClaimDetail>? = null
    var lastNotifyPosition = -1
    val buttonCreateClaim = ObservableField(false)
    val buttonReviewClaim = ObservableField(false)
    val haveClaim = ObservableField(false)
    val fromClaimFragment = ObservableField(false)
    override fun init() {
        if (claimDetails == null) {
            getClaimReference()
        } else {
            getClaimDetailSuccess(this.claimDetails!!)
            getNewRepackSuccess(repackingPlanning)
        }
    }

    private fun getClaimReference() {
        callApi(Service.callClaimService()
                .findClaimByReference(repackingPlanning.id, Claim.ClaimType.REPACKING_PLANNING.value)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, List::class.java) {
                    override fun getDataSuccess(details: List<ClaimDetail>) {
                        getClaimDetailSuccess(details)
                        callApiGetRepackingDetail()
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        callApiGetRepackingDetail()
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                    }
                })
        )
    }

    private fun getClaimDetailSuccess(details: List<ClaimDetail>) {
        claimDetails = arrayListOf()
        claimDetails!!.addAll(details)
    }


    private fun callApiGetRepackingDetail() {
        callApi(Service.callRepackingPlanningService()
                .findRepackingPlanningById(repackingPlanning.getId())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<RepackingPlanning>(appException, RepackingPlanning::class.java, null) {
                    override fun getDataSuccess(repackingPlanning: RepackingPlanning) {
                        getNewRepackSuccess(repackingPlanning)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                    }
                }))
    }

    private fun getNewRepackSuccess(repackingPlanning: RepackingPlanning) {
        this.repackingPlanning = repackingPlanning
        notifyDisplay()
        reCalculatorQuantity(repackingPlanning.repackingPlanningDetails)
        setData(repackingPlanning.repackingPlanningDetails as List<BaseModel>?)
    }

    private fun notifyDisplay() {
        if (claimNotApproved()) {
            disableViewButtonAndCreateButton()
        } else {
            if (doNotHaveClaim()) {
                enableCreateButtonAndDisableViewButton()
            } else {
                disableCreateButtonAndEnableViewButton()
            }
        }
    }

    private fun claimNotApproved(): Boolean {
        return claimDetails == null ||
                (claimDetails!!.isNotEmpty() && claimDetails!![0].claim.status == Claim.ClaimStatus.WAIT_APPROVE.value)
    }

    private fun disableViewButtonAndCreateButton() {
        buttonCreateClaim.set(false)
        buttonReviewClaim.set(false)
        haveClaim.set(false)
    }

    private fun doNotHaveClaim(): Boolean {
        return claimDetails!!.isEmpty()
    }

    private fun enableCreateButtonAndDisableViewButton() {
        buttonCreateClaim.set(RuleHelper().hasRule(HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
        buttonReviewClaim.set(false)
        haveClaim.set(false)
    }

    private fun disableCreateButtonAndEnableViewButton() {
        buttonCreateClaim.set(false)
        buttonCreateClaim.notifyChange()
        buttonReviewClaim.set(RuleHelper().hasRule(HttpRequest(Method.GET.value, ClaimUrlPath.FIND_CLAIM_BY_ID)))
        haveClaim.set(true)
    }


    private fun reCalculatorQuantity(detailRepacks: MutableList<RepackingPlanningDetail>) {
        if (!claimDetails.isNullOrEmpty() && claimDetails!![0].claim.status == Claim.ClaimStatus.APPROVED.value) {
            detailRepacks.forEach { detailRepack ->
                run {
                    if (detailRepack.claimQuantity == null) {
                        detailRepack.claimQuantity = 0L
                        detailRepack.originalQuantity = detailRepack.quantity
                    }
                    val matchClaimDetail = claimDetails!!.find {
                        detailRepack.productPacking.id == it.productPacking.id &&
                                detailRepack.expireDateStr == it.expireDate
                    }
                    matchClaimDetail?.let {
                        detailRepack.claimQuantity += it.quantity
                        detailRepack.quantity -= it.quantity
                    }
                }
            }
        }

    }

    fun searchByCode(code: String) {
        searchComponent.setData(code)
        search()
    }

    override fun search() {
        val data = baseModelsE as List<RepackingPlanningDetail>
        val textSearch = searchComponent.code.get().toString()
        for ((position, detail) in data.withIndex()) {
            if (detail.productPacking.code.contains(textSearch, true).or(detail.productPacking.product.name.contains(textSearch, true))) {
                if (notifyPosition.get() != null) {
                    lastNotifyPosition = notifyPosition.get()!!
                }
                if (lastNotifyPosition != -1) {
                    data[lastNotifyPosition].checked = null
                }

                notifyPosition.set(position)
                detail.checked = true
                sendBackAction(Constants.SCROLL_TO_POSITION)
                return
            }
        }
    }

    fun hideButtonViewClaim() {
        fromClaimFragment.set(true)
    }
}