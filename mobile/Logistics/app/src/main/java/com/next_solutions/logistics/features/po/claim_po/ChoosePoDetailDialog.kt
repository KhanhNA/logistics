package com.next_solutions.logistics.features.po.claim_po

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.PoDetail
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN
import com.next_solutions.logistics.utils.Constants.SCAN_CODE
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ChoosePoDetailDialog : BaseDialogFragment() {
    private lateinit var choosePoDetailViewModel: ChoosePoDetailViewModel
    private lateinit var claimPoViewModel: ClaimPoViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        claimPoViewModel = ViewModelProvider(baseActivity,
                BaseViewModelFactory.getInstance<Any>(baseActivity.application, ClaimPoViewModel::class.java))
                .get(ClaimPoViewModel::class.java)

        choosePoDetailViewModel = viewModel as ChoosePoDetailViewModel

        choosePoDetailViewModel.init(claimPoViewModel)
        setUpRecycleView()
        return binding.root
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (SCAN_CODE == view!!.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            val result = data!!.getStringExtra(Constants.RESULT_SCAN)
            choosePoDetailViewModel.searchComponent.setData(result)
            choosePoDetailViewModel.search()
        }
        closeProcess()
    }

    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_po_detail_item, choosePoDetailViewModel, OwnerView { _, poDetail ->
            run {
                claimPoViewModel.addPoDetail(poDetail as PoDetail)
                dismiss()
            }
        }, baseActivity)
        recyclerView.adapter = baseAdapter
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChoosePoDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_po_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.po_details
    }

}