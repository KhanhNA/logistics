package com.next_solutions.logistics.component.date

import androidx.databinding.ObservableField
import com.next_solutions.logistics.utils.DateUtils
import java.util.*

class ChooseDateComponent {
    var fromDate = ObservableField(DateUtils.reduceOneMonth(Calendar.getInstance().time))
    var toDate = ObservableField(Calendar.getInstance().time)

    fun resetData() {
        fromDate.set(DateUtils.reduceOneMonth(Calendar.getInstance().time))
        toDate.set(Calendar.getInstance().time)
        fromDate.notifyChange()
        toDate.notifyChange()
    }

}