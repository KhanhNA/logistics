package com.next_solutions.logistics.features.inventory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.models.Inventory;
import com.next_solutions.logistics.models.dto.HttpRequest;
import com.next_solutions.logistics.models.helper.RuleHelper;
import com.next_solutions.logistics.repository.api.Method;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;

import static com.next_solutions.logistics.repository.api.inventory.InventoryUrlPath.GET_DETAIL_INVENTORY;
import static com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN;
import static com.next_solutions.logistics.utils.Constants.RESULT_SCAN;

public class InventoryFragment extends BaseFragment {
    private InventoryViewModel inventoryViewModel;
    private DetailInventoryDialog inventoryDialog = new DetailInventoryDialog();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        inventoryViewModel = (InventoryViewModel) viewModel;
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.item_recycle_inventory, viewModel, (view, baseModel) -> {
            Inventory inventory = (Inventory) baseModel;
            if (inventoryViewModel.getSelectedStore().isFC()
                    && new RuleHelper().hasRule(new HttpRequest(Method.GET.getValue(), GET_DETAIL_INVENTORY))) {
                inventoryViewModel.setInventory(inventory);
                inventoryDialog.show(getParentFragmentManager(), "inventory_dialog");
            }
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        inventoryViewModel.init();
        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        String tag = (String) view.getTag();
        if ("scanCode".equals(tag)) {
            Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
            startActivityForResult(qrScan, REQUEST_CODE_SCAN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (isScanCodeAndResultOk(requestCode, resultCode) && data != null && data.hasExtra(RESULT_SCAN)) {
            String result = data.getStringExtra(RESULT_SCAN);
            inventoryViewModel.code.set(result);
            inventoryViewModel.onClickButtonSearch();
        }
        closeProcess();
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK;
    }

    @Override
    public void onResume() {
        super.onResume();
        inventoryViewModel.search();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_inventory;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return InventoryViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcInventory;
    }
}
