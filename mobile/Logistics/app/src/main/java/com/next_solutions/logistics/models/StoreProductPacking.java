package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class StoreProductPacking extends Base {
    private Store store;
    private ProductPacking productPacking;

    private Product product;

    private Long totalQuantity;
    private String expireDate;
    private List<StoreProductPackingDetail> storeProductPackingDetails;

    public String getExpireDateStr() {
        return expireDate == null ? Constants.EMPTY_STRING : DateUtils.convertStringDateToDifferentType(expireDate, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public List<StoreProductPackingDetail> getStoreProductPackingDetails() {
        return storeProductPackingDetails;
    }

    public void setStoreProductPackingDetails(List<StoreProductPackingDetail> storeProductPackingDetails) {
        this.storeProductPackingDetails = storeProductPackingDetails;
    }
}
