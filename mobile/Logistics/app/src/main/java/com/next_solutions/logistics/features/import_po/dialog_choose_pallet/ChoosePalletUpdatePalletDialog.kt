package com.next_solutions.logistics.features.import_po.dialog_choose_pallet

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseAdapter
import com.next_solutions.base.BaseDialogFragment
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.Pallet
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.SCAN_CODE
import com.next_solutions.logistics.utils.DialogCallBack
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ChoosePalletUpdatePalletDialog : BaseDialogFragment() {
    private lateinit var dialogViewModel: ChoosePalletUpdatePalletViewModel
    lateinit var dialogCallBack: DialogCallBack
    lateinit var pallets: List<Pallet>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpViewModel()
        setUpRecycleView()
        return binding.root
    }

    private fun setUpViewModel() {
        dialogViewModel = viewModel as ChoosePalletUpdatePalletViewModel
        dialogViewModel.storePallet = pallets
        dialogViewModel.init()

    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (SCAN_CODE == view!!.tag) {
            val intent = Intent(activity, QRCodeScannerActivity::class.java)
            startActivityForResult(intent, Constants.REQUEST_CODE_SCAN)
        }
    }

    private fun setUpRecycleView() {
        recyclerView.adapter = BaseAdapter(R.layout.dialog_choose_pallet_update_pallet_item, dialogViewModel, OwnerView { _, pallet ->
            run {
                dialogCallBack.callBack(pallet)
                dismiss()
            }
        }, baseActivity)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            val result = data!!.getStringExtra(Constants.RESULT_SCAN)
            dialogViewModel.searchComponent.setData(result)
            dialogViewModel.onClickButtonSearch()
        }
        closeProcess()
    }


    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChoosePalletUpdatePalletViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_pallet_update_pallet
    }

    override fun getRecycleResId(): Int {
        return R.id.pallets
    }

}