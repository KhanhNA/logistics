package com.next_solutions.logistics.models;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Manufacturer extends Base {

    public List<ManufacturerDescription> manufacturerDescriptions;
    private String code;
    private String name;
    private String address;
    private String tel;
    private String email;
    private Boolean status;

    @NonNull
    @Override
    public String toString() {
        return code + " - " + getName();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        for (ManufacturerDescription manufacturerDescription : manufacturerDescriptions) {
            if (manufacturerDescription.language.id.equals(AppUtils.getLanguage())) {
                return manufacturerDescription.name;
            }
        }
        return EMPTY_STRING;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }


}