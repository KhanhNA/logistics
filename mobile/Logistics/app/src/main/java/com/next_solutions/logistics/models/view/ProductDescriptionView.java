package com.next_solutions.logistics.models.view;

import androidx.room.ColumnInfo;
import androidx.room.Ignore;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.base.BaseModel;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDescriptionView extends BaseModel {
    @ColumnInfo(name = "packingProductId")
    private Long packingProductId;

    @ColumnInfo(name = "productCode")
    private String productCode;

    @ColumnInfo(name = "productName")
    private String productName;
    @Ignore
    private Integer packingQuantity;
    @Ignore
    private Long packingTypeId;
    @Ignore
    private Long productPackingId;
    @Ignore
    private Long productPackingCode;

    public Long getPackingProductId() {
        return packingProductId;
    }

    public void setPackingProductId(Long packingProductId) {
        this.packingProductId = packingProductId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getPackingQuantity() {
        return packingQuantity;
    }

    public void setPackingQuantity(Integer packingQuantity) {
        this.packingQuantity = packingQuantity;
    }

    public Long getPackingTypeId() {
        return packingTypeId;
    }

    public void setPackingTypeId(Long packingTypeId) {
        this.packingTypeId = packingTypeId;
    }

    public Long getProductPackingId() {
        return productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }

    public Long getProductPackingCode() {
        return productPackingCode;
    }

    public void setProductPackingCode(Long productPackingCode) {
        this.productPackingCode = productPackingCode;
    }
}
