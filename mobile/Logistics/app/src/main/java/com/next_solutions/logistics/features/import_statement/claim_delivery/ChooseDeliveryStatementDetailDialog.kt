package com.next_solutions.logistics.features.import_statement.claim_delivery

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.ImportStatementDetail
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ChooseDeliveryStatementDetailDialog : BaseDialogFragment() {
    private lateinit var chooseExportStatementDetailViewModel: ChooseDeliveryStatementDetailViewModel
    private lateinit var claimExportStatementViewModel: ClaimDeliveryStatementViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        claimExportStatementViewModel = ViewModelProvider(baseActivity,
                BaseViewModelFactory.getInstance<Any>(baseActivity.application, ClaimDeliveryStatementViewModel::class.java))
                .get(ClaimDeliveryStatementViewModel::class.java)
        chooseExportStatementDetailViewModel = viewModel as ChooseDeliveryStatementDetailViewModel
        setUpRecycleView()
        chooseExportStatementDetailViewModel.init(claimExportStatementViewModel)
        return binding.root
    }

    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_delivery_detail_item, chooseExportStatementDetailViewModel, OwnerView { _, detail ->
            run {
                claimExportStatementViewModel.addDetail(detail as ImportStatementDetail)
                dismiss()
            }
        }, baseActivity)
        recyclerView.adapter = baseAdapter
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (Constants.SCAN_CODE == view!!.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            val result = data!!.getStringExtra(Constants.RESULT_SCAN)
            chooseExportStatementDetailViewModel.searchComponent.setData(result)
            chooseExportStatementDetailViewModel.search()
        }
        closeProcess()
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChooseDeliveryStatementDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_delivery_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}