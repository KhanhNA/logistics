package com.next_solutions.logistics.features.export_statement.claim_export_statement

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.logistics.base.claim.BaseClaimViewModel
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.helper.StatementHelper
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE_ITEM_ADD

@Suppress("UNCHECKED_CAST")
class ClaimExportStatementViewModel(application: Application) : BaseClaimViewModel(application) {
    lateinit var exportStatement: ExportStatement
    override fun prepareClaim(): Claim {
        val details = baseModelsE as List<ClaimDetail>
        return Claim().apply {
            type = Claim.ClaimType.EXPORT_STATEMENT
            referenceId = exportStatement.id
            claimDetails = details
            store = Store().apply {
                code = exportStatement.fromStore.code
                id = exportStatement.fromStore.id
            }
            status = Claim.ClaimStatus.WAIT_APPROVE.value
            description = descriptionObservable.get()
            amenable = amenableObservable.get()
        }
    }

    override fun init() {
        val groupDetails = StatementHelper(exportStatement).groupDetailByExpireDateAndProductPacking()
        exportStatement.exportStatementDetails = groupDetails
        val claimDetails = arrayListOf<ClaimDetail>()
        exportStatement.exportStatementDetails.forEach { detail ->
            run {
                claimDetails.add(claimHelper.convertExportDetailToClaimDetail(detail))
            }
        }
        setData(claimDetails as List<BaseModel>?)
    }

    fun addDetail(detail: ExportStatementDetail) {
        val claimDetails = baseModelsE as ArrayList<ClaimDetail>
        notifyPosition.set(claimDetails.size)
        claimDetails.add(claimHelper.convertExportDetailToClaimDetail(detail))
        sendBackAction(NOTIFY_CHANGE_ITEM_ADD)
    }


}