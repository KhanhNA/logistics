package com.next_solutions.logistics.models.mapper;

import com.next_solutions.logistics.models.ProductDescription;
import com.next_solutions.logistics.models.entity.ProductDescriptionEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductDescriptionMapper {
    ProductDescriptionMapper INSTANCE = Mappers.getMapper( ProductDescriptionMapper.class );

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "language.id", target = "langId")
    @Mapping(source = "packingTypeId", target = "packingId")
    @Mapping(target = "quantity", ignore = true)
    ProductDescriptionEntity productDescriptionDtoToProductDescription(ProductDescription product);
}
