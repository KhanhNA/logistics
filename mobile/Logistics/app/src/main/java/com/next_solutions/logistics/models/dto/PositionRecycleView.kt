package com.next_solutions.logistics.models.dto

class PositionRecycleView(var parentPosition: Int,
                          var position: Int,
                          var size: Int)