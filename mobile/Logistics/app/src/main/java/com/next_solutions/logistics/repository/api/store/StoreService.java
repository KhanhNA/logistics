package com.next_solutions.logistics.repository.api.store;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.next_solutions.logistics.repository.api.store.StoreUrlPath.GET_INVENTORY_IGNORE_EXPIRE_DATE;
import static com.next_solutions.logistics.repository.api.store.StoreUrlPath.GET_STORE;
import static com.next_solutions.logistics.repository.api.store.StoreUrlPath.GET_STORES_DC;

public interface StoreService {

    //  ========================STORES========================
    @GET("/stores/fc")
    Call<ResponseBody> getStoreFC();

    @GET("/stores/{id}")
    Call<ResponseBody> findStoreById(@Path("id") Long id);

    @GET(GET_STORE)
    Single<Response<ResponseBody>> getFromStore(@Query("exceptIds") ArrayList<Long> exceptIds,
                                                @Query("ignoreCheckPermission") Boolean ignoreCheckPermission,
                                                @Query("pageNumber") Integer pageNumber,
                                                @Query("pageSize") Integer pageSize,
                                                @Query("status") Boolean status,
                                                @Query("text") String text);

    @GET(GET_STORE)
    Single<Response<ResponseBody>> getToStore(@Query("exceptIds") ArrayList<Long> exceptIds,
                                              @Query("ignoreCheckPermission") Boolean ignoreCheckPermission,
                                              @Query("pageNumber") Integer pageNumber,
                                              @Query("pageSize") Integer pageSize,
                                              @Query("status") Boolean status,
                                              @Query("text") String text);


    @GET(GET_STORE)
    Single<Response<ResponseBody>> getAllStoreFC(@Query("exceptIds") ArrayList<Long> exceptIds,
                                                 @Query("ignoreCheckPermission") Boolean ignoreCheckPermission,
                                                 @Query("is_DC") Boolean isDC,
                                                 @Query("pageNumber") Integer pageNumber,
                                                 @Query("pageSize") Integer pageSize,
                                                 @Query("status") Boolean status,
                                                 @Query("text") String text);

    @GET("/stores/all")
    Call<ResponseBody> getAllStore();

    @GET(GET_INVENTORY_IGNORE_EXPIRE_DATE)
    Single<Response<ResponseBody>> getInventory(@Query("text") String text,
                                                @Query("storeId") Long storeId,
                                                @Query("pageNumber") Integer pageNumber,
                                                @Query("pageSize") Integer pageSize,
                                                @Query("exportStatementId") Long exportStatementId,
                                                @Query("exceptProductPackingIds") List<Long> exceptIds);

    @GET(GET_STORES_DC)
    Single<Response<ResponseBody>> getStoreDC(@Query("storeIds") List<Long> storeIds);
}
