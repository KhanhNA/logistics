package com.next_solutions.logistics.features.import_statement.search;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.Pageable;
import com.next_solutions.logistics.component.search.SearchComponent;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.ImportStatement;
import com.next_solutions.logistics.repository.Service;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

@SuppressWarnings("unchecked")
public class ImportStatementViewModel extends BaseViewModel {
    private SearchComponent searchComponent = new SearchComponent();

    public ImportStatementViewModel(@NonNull Application application) {
        super(application);
    }

    void setStatementCode(String sCode) {
        searchComponent.setData(sCode);
        onClickButtonSearch();
    }

    @Override
    public void search() {
        Integer page = getCurrentPageNumber();
        callApi(Service.callImportStatementService()
                .getAll(searchComponent.getCode().get(),
                        ImportStatement.ImportStatementStatus.NOT_UPDATE_INVENTORY.getValue(),
                        page, AppUtils.PAGE_SIZE, "to")
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<Pageable<ImportStatement>>(appException, Pageable.class, ImportStatement.class, null) {
                    @Override
                    public void getDataSuccess(Pageable<ImportStatement> pageable) {
                        setPage(pageable);
                        List<ImportStatement> importStatements = pageable.getContent();
                        setData(importStatements);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    public SearchComponent getSearchComponent() {
        return searchComponent;
    }
}
