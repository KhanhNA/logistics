package com.next_solutions.logistics.models.mapper;

import com.next_solutions.logistics.models.ProductPacking;
import com.next_solutions.logistics.models.entity.ProductPackingEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductPackingMapper {
    ProductPackingMapper INSTANCE = Mappers.getMapper( ProductPackingMapper.class );

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "packingType.id", target = "packingTypeId")
    ProductPackingEntity productPackingDtoToProductPackingEntity(ProductPacking productPacking);
}
