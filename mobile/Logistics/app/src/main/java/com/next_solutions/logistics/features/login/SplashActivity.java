package com.next_solutions.logistics.features.login;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.next_solutions.base.BaseActivity;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.features.home.HomeActivity;
import com.next_solutions.logistics.utils.PreferencesHelper;
import com.next_solutions.logistics.utils.ValidateUtils;

import java.util.Locale;

public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SplashViewModel splashViewModel = (SplashViewModel) viewModel;
        PreferencesHelper preferencesHelper = AppUtils.getInstance().getPreferencesHelper();
        if (isNetworkConnected()) {
            if (preferencesHelper != null) {
                String refreshToken = preferencesHelper.getRefreshToken();
                if (ValidateUtils.isNullOrEmpty(refreshToken)) {
                    startLogin();
                } else {
                    splashViewModel.checkTokenExpired(refreshToken);
                }
            } else {
                startLogin();
            }
            splashViewModel.getOpenHomeActivity().observe(this, open -> {
                if (Boolean.TRUE.equals(open)) {
                    startHome();
                } else {
                    startLogin();
                }
            });
        } else {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void startHome() {
        changeLanguage();
        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void changeLanguage() {
        String lang = "vi";
        if (AppUtils.getInstance().getPreferencesHelper().getCurrentLanguage() != -1) {
            lang = AppUtils.LANGUAGE_CODE[AppUtils.getInstance().getPreferencesHelper().getCurrentLanguage()];
        }
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    private void startLogin() {
        changeLanguage();
        Intent refresh = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(refresh);
        finish();


    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return SplashViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
