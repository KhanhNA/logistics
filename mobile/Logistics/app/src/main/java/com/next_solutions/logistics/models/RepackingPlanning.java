package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class RepackingPlanning extends Base {
    public enum RepackingPlanningStatus {
        NEW(0), REPACKED(1), IMPORTED(2), CANCEL(-1);
        private Integer value;

        RepackingPlanningStatus(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    private String code;

    private String description;

    private Integer status;

    private Store store;

    private Distributor distributor;

    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date repackedDate;

    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date planningDate;

    private User createUserObj;

    private List<RepackingPlanningDetail> repackingPlanningDetails;

    public String getRepackedDateStr() {
        return repackedDate == null ? Constants.EMPTY_STRING : DateUtils.convertDateToString(repackedDate, DateUtils.DATE_SHOW_FORMAT);
    }

    public String getPlanningDateStr() {
        return planningDate == null ? Constants.EMPTY_STRING : DateUtils.convertDateToString(planningDate, DateUtils.DATE_SHOW_FORMAT);
    }

    public List<RepackingPlanningDetail> getRepackingPlanningDetails() {
        return repackingPlanningDetails;
    }

    public void setRepackingPlanningDetails(List<RepackingPlanningDetail> repackingPlanningDetails) {
        this.repackingPlanningDetails = repackingPlanningDetails;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public Date getRepackedDate() {
        return repackedDate;
    }

    public void setRepackedDate(Date repackedDate) {
        this.repackedDate = repackedDate;
    }

    public Date getPlanningDate() {
        return planningDate;
    }

    public void setPlanningDate(Date planningDate) {
        this.planningDate = planningDate;
    }

    public User getCreateUserObj() {
        return createUserObj;
    }

    public void setCreateUserObj(User createUserObj) {
        this.createUserObj = createUserObj;
    }

}
