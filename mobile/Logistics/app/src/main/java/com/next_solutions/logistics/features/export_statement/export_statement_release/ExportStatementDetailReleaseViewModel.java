package com.next_solutions.logistics.features.export_statement.export_statement_release;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.ResponseStatus;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.Claim;
import com.next_solutions.logistics.models.ClaimDetail;
import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.models.dto.HttpRequest;
import com.next_solutions.logistics.models.dto.ImportStatementDTO;
import com.next_solutions.logistics.models.dto.ImportStatementDetailDTO;
import com.next_solutions.logistics.models.helper.ClaimHelper;
import com.next_solutions.logistics.models.helper.ExportStatementDetailHelper;
import com.next_solutions.logistics.models.helper.ExportStatementHelper;
import com.next_solutions.logistics.models.helper.RuleHelper;
import com.next_solutions.logistics.repository.Service;
import com.next_solutions.logistics.repository.api.Method;
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath;
import com.next_solutions.logistics.repository.api.export_statement.ExportUrlPath;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.ValidateUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;
import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

@SuppressWarnings("unchecked")
public class ExportStatementDetailReleaseViewModel extends BaseViewModel {
    private ObservableField<ExportStatement> exportStatement = new ObservableField<>();

    private List<ClaimDetail> claimDetails = null;
    private ObservableField<Boolean> orderFromDC = new ObservableField<>(false);
    private ObservableField<Boolean> fromClaimFragment = new ObservableField<>(false);
    private HashMap<String, List<Integer>> palletCodes = new HashMap<>();
    private HashMap<String, List<Integer>> qrCodeMapping = new HashMap<>();
    private Integer errorPosition;
    private ObservableField<String> currentPalletCode = new ObservableField<>(EMPTY_STRING);

    private ObservableField<Boolean> buttonEnable = new ObservableField<>(true);
    private ObservableField<Boolean> canExportRelease = new ObservableField<>(true);
    private ObservableField<Boolean> canCreateClaim = new ObservableField<>(true);
    private ObservableField<Boolean> buttonReviewClaim = new ObservableField<>(false);
    private ObservableField<Boolean> claim = new ObservableField<>(false);

    public ExportStatementDetailReleaseViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void init() {
        if (claimDetails == null) {
            RuleHelper ruleHelper = new RuleHelper();
            if (ruleHelper.hasRule(new HttpRequest(Method.GET.getValue(), ClaimUrlPath.FIND_CLAIM_BY_ID))) {
                getClaimReference();
            } else {
                this.claimDetails = new ArrayList<>();
                findExportStatementById();
            }
        } else {
            getClaimReferenceSuccess(claimDetails);
            getExportStatementSuccess(exportStatement.get());
        }
    }

    void checkProduct(String result) {
        Boolean isFromDC = orderFromDC.get();
        if (ValidateUtils.isNullOrEmpty(currentPalletCode.get()) && isFromDC != null && !isFromDC) {
            sendBackAction("CHOOSE_PALLET");
            return;
        }
        result = AppUtils.changeFormCode(result);
        List<Integer> positions;
        if (isFromDC != null && !isFromDC) {
            positions = qrCodeMapping.get(result + " " + currentPalletCode.get());
        } else {
            positions = qrCodeMapping.get(result);
        }
        if (positions != null) {
            List list = getBaseModelsE();
            if (list != null) {
                for (Integer position : positions) {
                    ImportStatementDetailDTO sd = (ImportStatementDetailDTO) list.get(position);
                    if (isFromDC != null && !isFromDC) {
                        if (sd.pallet.getCode().equals(currentPalletCode.get())) {
                            sd.checked = true;
                        } else {
                            sendBackAction("NOT_IN_PALLET");
                        }
                    } else {
                        sd.checked = true;
                        list.set(position, sd);

                    }
                }
            }
            baseModels.notifyChange();
        } else {
            sendBackAction("NOT_IN_PALLET");
        }
    }

    private void getClaimReference() {
        ExportStatement ex = exportStatement.get();
        if (ex != null) {
            callApi(Service.callClaimService()
                    .findClaimByReference(ex.id, Claim.ClaimType.EXPORT_STATEMENT.getValue())
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(new RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail.class, List.class) {
                        @Override
                        public void getDataSuccess(List<ClaimDetail> claimDetails) {
                            getClaimReferenceSuccess(claimDetails);
                            findExportStatementById();
                        }

                        @Override
                        public void onFailure(ErrorRespone errorRespone) {
                            findExportStatementById();
                        }
                    }));
        }
    }

    private void getClaimReferenceSuccess(List<ClaimDetail> claimDetails) {
        this.claimDetails = claimDetails;
    }

    private void findExportStatementById() {
        ExportStatement statement = exportStatement.get();
        if (statement != null) {
            callApi(Service.callExportStatementService()
                    .findExportStatementById(statement.id)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(new RxSingleSubscriber<ExportStatement>(appException, ExportStatement.class, null) {
                        @Override
                        public void getDataSuccess(ExportStatement exportStatement) {
                            getExportStatementSuccess(exportStatement);
                        }

                        @Override
                        public void onFailure(ErrorRespone errorRespone) {
                            Log.e(ERROR_PROCESS, errorRespone.getError());
                        }
                    }));
        }
    }

    private void getExportStatementSuccess(ExportStatement statement) {
        exportStatement.set(statement);
        orderFromDC.set(statement.getFromStore().isDC());
        List<ImportStatementDetailDTO> statementDetails;
        ExportStatementHelper exportHelper = new ExportStatementHelper(statement);
        if (Boolean.TRUE.equals(orderFromDC.get())) {
            statementDetails = exportHelper.groupExportDetailFromDC();
        } else {
            statementDetails = exportHelper.groupExportDetailFromFC();
        }

        notifyButtonDisplay(statementDetails);
        setUpQRCodeMapping(statementDetails);
        if (Boolean.FALSE.equals(orderFromDC.get())) {
            setUpPalletMapping(statementDetails);
        }
        setData(statementDetails);
    }

    private void notifyButtonDisplay(List<ImportStatementDetailDTO> statementDetails) {
        if (claimNotApproved()) {
            disableAndHideAllButton();
        } else {
            if (doNotHaveClaim()) {
                enableExportAndCreateButton();
            } else {
                claimApproved();
                addClaimDetail(statementDetails);
            }
        }
    }

    private Boolean claimNotApproved() {
        return claimDetails == null ||
                (!claimDetails.isEmpty() &&
                        claimDetails.get(0).getClaim().getStatus().equals(Claim.ClaimStatus.WAIT_APPROVE.getValue()));
    }

    private void disableAndHideAllButton() {
        buttonReviewClaim.set(false);
        claim.set(false);
        canExportRelease.set(new RuleHelper().hasRule(new HttpRequest(Method.PATCH.getValue(), ExportUrlPath.ACTUALLY_EXPORT)));
        buttonEnable.set(false);
        canCreateClaim.set(new RuleHelper().hasRule(new HttpRequest(Method.POST.getValue(), ClaimUrlPath.CREATE_CLAIM)));
    }

    private Boolean doNotHaveClaim() {
        return claimDetails.isEmpty();
    }

    private void enableExportAndCreateButton() {
        canExportRelease.set(new RuleHelper().hasRule(new HttpRequest(Method.PATCH.getValue(), ExportUrlPath.ACTUALLY_EXPORT)));
        canCreateClaim.set(new RuleHelper().hasRule(new HttpRequest(Method.POST.getValue(), ClaimUrlPath.CREATE_CLAIM)));
        buttonReviewClaim.set(false);
        claim.set(false);
    }

    private void claimApproved() {
        canExportRelease.set(new RuleHelper().hasRule(new HttpRequest(Method.PATCH.getValue(), ExportUrlPath.ACTUALLY_EXPORT)));
        canCreateClaim.set(false);
        buttonReviewClaim.set(new RuleHelper().hasRule(new HttpRequest(Method.GET.getValue(), ClaimUrlPath.FIND_CLAIM_BY_ID)));
        claim.set(true);
    }

    private void addClaimDetail(List<ImportStatementDetailDTO> statementDetails) {
        ClaimHelper claimHelper = new ClaimHelper();
        if (claimDetails != null) {
            for (ClaimDetail claimDetail : claimDetails) {
                statementDetails.add(claimHelper.convertClaimDetailToImportDetailDTO(claimDetail));
            }
        }
    }

    private void setUpQRCodeMapping(List<ImportStatementDetailDTO> statementDetails) {
        int index = 0;
        for (ImportStatementDetailDTO statementDetail : statementDetails) {
            String key;
            if (Boolean.FALSE.equals(orderFromDC.get())) {
                key = AppUtils.changeFormCode(statementDetail.storeProductPackingDetailCode + " " + statementDetail.pallet.getCode());
            } else {
                key = AppUtils.changeFormCode(statementDetail.storeProductPackingDetailCode + " ");
            }
            if (this.qrCodeMapping.containsKey(key)) {
                List<Integer> positions = this.qrCodeMapping.get(key);
                if (positions != null) {
                    positions.add(index);
                }
            } else {
                List<Integer> positions = new ArrayList<>();
                positions.add(index);
                this.qrCodeMapping.put(key, positions);
            }

            index++;
        }
    }

    private void setUpPalletMapping(List<ImportStatementDetailDTO> statementDetails) {
        int index = 0;
        for (ImportStatementDetailDTO statementDetail : statementDetails) {
            String key = statementDetail.pallet.getCode();
            if (palletCodes.containsKey(key)) {
                List<Integer> listPositions = palletCodes.get(key);
                if (listPositions != null) {
                    listPositions.add(index);
                }
            } else {
                ArrayList positions = new ArrayList();
                positions.add(index);
                palletCodes.put(key, positions);
            }
            index++;
        }
    }


    public void onCreate() {
        if (validate()) {
            ExportStatement ex = exportStatement.get();
            if (ex != null) {
                List<ImportStatementDetailDTO> data = new ArrayList<>();
                for (Object obj : getBaseModelsE()) {
                    ImportStatementDetailDTO detailDTO = (ImportStatementDetailDTO) obj;
                    if (detailDTO.getQuantity() != 0) {
                        data.add(detailDTO);
                    }
                }
                List<ImportStatementDetailDTO> importStatementDetails =
                        new ExportStatementDetailHelper().groupDetailByExpireDateAndProductPacking(data);
                ImportStatementDTO importStatementDTO = prepareImportStatementDTO(importStatementDetails);
                buttonEnable.set(false);
                callApi(Service.callExportStatementService()
                        .actuallyExport(ex.id, importStatementDTO)
                        .compose(Service.getRxSingleSchedulers().applySchedulers())
                        .subscribeWith(new RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                            @Override
                            public void getDataSuccess(ResponseStatus responseStatus) {
                                statementSuccess(responseStatus);
                            }

                            @Override
                            public void onFailure(ErrorRespone errorRespone) {
                                buttonEnable.set(true);
                                Log.e(ERROR_PROCESS, errorRespone.getMessage() + " " + this.getClass().getName());
                            }
                        }));
            }
        } else {
            sendBackAction(Constants.ERROR_INVALID_INPUT);
        }
    }

    private ImportStatementDTO prepareImportStatementDTO(List<ImportStatementDetailDTO> data) {
        ExportStatement statement = exportStatement.get();
        ImportStatementDTO importStatementDTO = new ImportStatementDTO();
        if (statement != null) {
            importStatementDTO.setImportStatementDetails(data);
            if (statement.getToStore() != null) {
                importStatementDTO.setToStoreCode(statement.getToStore().getCode());
                importStatementDTO.setToStoreId(statement.getToStore().getId());
            }
            importStatementDTO.setFromStoreId(statement.getFromStore().getId());
            importStatementDTO.setFromStoreCode(statement.getFromStore().getCode());
            importStatementDTO.setExportStatementId(statement.getId());
            importStatementDTO.setEstimatedTimeOfArrival(statement.getEstimatedTimeOfArrival());
        }
        return importStatementDTO;
    }

    private void statementSuccess(ResponseStatus responseStatus) {
        if (responseStatus.getCode() == Constants.CODE_SUCCESS) {
            sendBackAction(Constants.CREATE_SUCCESS);
        }
    }

    private boolean validate() {
        errorPosition = null;
        if (exportStatement == null) {
            return false;
        }

        List<ImportStatementDetailDTO> importStatementDetailDTOS = getBaseModelsE();

        int stt = 0;
        for (ImportStatementDetailDTO detailDTO : importStatementDetailDTOS) {
            if (detailDTO.checked == null || !detailDTO.checked || (detailDTO.getQuantity() > detailDTO.originalQuantity)) {
                errorPosition = stt;
                return false;
            }
            stt++;
        }
        return true;
    }

    void checkPallet(String code) {
        List<Integer> positions = palletCodes.get(code);
        List<ImportStatementDetailDTO> details = getBaseModelsE();
        if (positions != null) {
            currentPalletCode.set(code);
            for (ImportStatementDetailDTO statementDetail : details) {
                statementDetail.state = 0;
            }
            for (Integer index : positions) {
                ImportStatementDetailDTO statementDetail = details.get(index);
                statementDetail.state = 1;
            }
            notifyChangeAll();
        } else {
            sendBackAction("PALLET_DONT_HAVE_PRODUCT");
        }
    }

    public int getColor(ImportStatementDetailDTO detail) {
        if (errorPosition != null && getBaseModelsE().indexOf(detail) == errorPosition) {
            return AppUtils.getInstance().getResources().getColor(R.color.light_red_2);
        }
        if (detail.checked == null && detail.state == 0) {
            return AppUtils.getInstance().getResources().getColor(R.color.white);
        }
        if (detail.checked != null && detail.checked) {
            return AppUtils.getInstance().getResources().getColor(R.color.light_green);
        }
        if (detail.state == 1) {
            return AppUtils.getInstance().getResources().getColor(R.color.light_orange);
        }
        if (detail.checked != null) {
            return AppUtils.getInstance().getResources().getColor(R.color.light_red_2);
        }
        return AppUtils.getInstance().getResources().getColor(R.color.white);
    }

    public ExportStatement getExportStatement() {
        return exportStatement.get();
    }

    public void setExportStatement(ExportStatement exportStatement) {
        this.exportStatement.set(exportStatement);
    }

    public ObservableField<Boolean> getCanExportRelease() {
        return canExportRelease;
    }

    public ObservableField<Boolean> getCanCreateClaim() {
        return canCreateClaim;
    }

    Integer getErrorPosition() {
        return errorPosition;
    }

    public ObservableField<Boolean> getButtonEnable() {
        return buttonEnable;
    }

    public ObservableField<Boolean> getOrderFromDC() {
        return orderFromDC;
    }

    public void hideButtonViewClaim() {
        fromClaimFragment.set(true);
    }

    public ObservableField<String> getCurrentPalletCode() {
        return currentPalletCode;
    }

    public ObservableField<Boolean> getClaim() {
        return claim;
    }

    public ObservableField<Boolean> getButtonReviewClaim() {
        return buttonReviewClaim;
    }

    public ObservableField<Boolean> getFromClaimFragment() {
        return fromClaimFragment;
    }

    public List<ClaimDetail> getClaimDetails() {
        return claimDetails;
    }

    public void setClaimDetails(List<ClaimDetail> claimDetails) {
        this.claimDetails = claimDetails;
    }
}
