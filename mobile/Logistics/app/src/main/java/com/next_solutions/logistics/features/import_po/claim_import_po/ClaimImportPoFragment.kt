package com.next_solutions.logistics.features.import_po.claim_import_po

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.claim.BaseClaimFragment
import com.next_solutions.logistics.models.ImportPo
import com.next_solutions.logistics.utils.Constants.MODEL

class ClaimImportPoFragment : BaseClaimFragment() {
    private lateinit var claimImportPoViewModel: ClaimImportPoViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimImportPoViewModel = viewModel as ClaimImportPoViewModel
        getData()
        claimImportPoViewModel.init()
        return binding.root
    }

    private fun getData() {
        val importPo = activity?.intent?.getSerializableExtra(MODEL) as ImportPo
        claimImportPoViewModel.importPo = importPo
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        super.action(view, baseViewModel)
        if (view!!.id == R.id.add_product) {
            val dialog = ChooseImportPoDetailDialog()
            dialog.show((activity as FragmentActivity).supportFragmentManager, "")
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_claim_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getRecycleViewImage(): Int {
        return R.id.images
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimImportPoViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_import_po
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }
}