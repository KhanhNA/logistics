package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.R
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.claim.ClaimUrlPath

class ClaimRuleHelper {

    fun canCreateClaim(): Boolean {
        return RuleHelper().hasRule(HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM))
    }

    private val types = arrayListOf<Claim.ClaimType>()
    fun getListTypeCanAccess(): List<Claim.ClaimType> {
        addTypeClaimPoIfUserHasRule()
        addTypeImportPOUpdatePalletIfUserHasRule()
        addTypeRepackingPlanningIfUserHasRule()
        addTypeImportStatementRepackingPlanningIfUserHasRule()
        addTypeExportStatementIfUserHasRule()
        addTypeDeliveryIfUserHasRule()
        addTypeImportStatementIfUserHasRule()
        addTypeOutOfDateOneIfUserHasRule()
        addTypeOutOfDateTwoIfUserHasRule()
        addTypeOtherIfUserHasRule()
        return types
    }

    // IMPORT_PO
    // =================================================
    private fun addTypeClaimPoIfUserHasRule() {
        //  if (canCreateClaimPo()) {
        addTypeImportPo()
        //     }
    }

    fun canCreateClaimPo(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                //HttpRequest(Method.POST.value, ImportPoUrlPath.CREATE_IMPORT_PO),
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeImportPo() {
        AppUtils.getInstance().getString(R.string.accept_claim)
        types.add(Claim.ClaimType.IMPORT_PO)
    }

    // IMPORT_PO_UPDATE_PALLET
    // =================================================
    private fun addTypeImportPOUpdatePalletIfUserHasRule() {
        //   if (canCreateClaimImportPoUpdatePallet()) {
        addTypeUpdatePalletImportPo()
        //   }
    }

    fun canCreateClaimImportPoUpdatePallet(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                // HttpRequest(Method.POST.value, ImportPoUrlPath.UPDATE_PALLET),
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeUpdatePalletImportPo() {
        types.add(Claim.ClaimType.IMPORT_PO_UPDATE_PALLET)
    }

    // REPACKING_PLANNING
    // =================================================
    private fun addTypeRepackingPlanningIfUserHasRule() {
        //   if (canCreateClaimRepackingPlanning()) {
        addTypeRepackingPlanning()
        // }
    }

    fun canCreateClaimRepackingPlanning(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                // HttpRequest(Method.POST.value, RepackingPlanningUrlPath.REPACK),
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeRepackingPlanning() {
        types.add(Claim.ClaimType.REPACKING_PLANNING)
    }

    // IMPORT_STATEMENT_REPACKING_PLANNING
    // =================================================
    private fun addTypeImportStatementRepackingPlanningIfUserHasRule() {
        //    if (canCreateClaimImportStatementRepackingPlanning()) {
        addTypeImportStatementRepackingPlanning()
        //   }
    }

    fun canCreateClaimImportStatementRepackingPlanning(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                //   HttpRequest(Method.POST.value, ImportStatementUrlPath.CREATE_IMPORT_STATEMENT_FROM_REPACKING_PLANNING),
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeImportStatementRepackingPlanning() {
        types.add(Claim.ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING)
    }

    // IMPORT_STATEMENT_EXPORT_STATEMENT
    // =================================================
    private fun addTypeImportStatementIfUserHasRule() {
        //    if (canCreateClaimImportStatement()) {
        addTypeImportStatement()
        //   }
    }

    fun canCreateClaimImportStatement(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                //   HttpRequest(Method.PATCH.value, ImportStatementUrlPath.ACTUALLY_IMPORT),
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeImportStatement() {
        types.add(Claim.ClaimType.IMPORT_STATEMENT_EXPORT_STATEMENT)
    }

    // EXPORT_STATEMENT
    // =================================================
    private fun addTypeExportStatementIfUserHasRule() {
        //      if (canCreateClaimExportStatement()) {
        addTypeExportStatement()
    }
    //  }

    fun canCreateClaimExportStatement(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                // HttpRequest(Method.PATCH.value, ExportUrlPath.ACTUALLY_EXPORT),
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeExportStatement() {
        types.add(Claim.ClaimType.EXPORT_STATEMENT)
    }


    // DELIVERY
    // =================================================
    private fun addTypeDeliveryIfUserHasRule() {
        //       if (canCreateClaimDelivery()) {
        addTypeDelivery()
        //     }
    }

    private fun canCreateClaimDelivery(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                // HttpRequest(Method.PATCH.value, ExportUrlPath.ACTUALLY_EXPORT),
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeDelivery() {
        types.add(Claim.ClaimType.DELIVERY)
    }


    // OUT_OF_DATE_PALLET_STEP_1
    // =================================================
    private fun addTypeOutOfDateOneIfUserHasRule() {
//        if (canCreateClaimOutOfDatePalletStep1()) {
        addTypeClaimOutOfDatePalletStep1()
        //     }
    }

    private fun canCreateClaimOutOfDatePalletStep1(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeClaimOutOfDatePalletStep1() {
        types.add(Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_1)
    }


    // OUT_OF_DATE_PALLET_STEP_2
    // =================================================
    private fun addTypeOutOfDateTwoIfUserHasRule() {
        //     if (canCreateClaimOutOfDatePalletStep2()) {
        addTypeClaimOutOfDatePalletStep2()
        //     }
    }

    private fun canCreateClaimOutOfDatePalletStep2(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeClaimOutOfDatePalletStep2() {
        types.add(Claim.ClaimType.OUT_OF_DATE_PALLET_STEP_2)
    }


    // OTHER
    // =================================================
    private fun addTypeOtherIfUserHasRule() {
        //     if (canCreateClaimOther()) {
        addTypeOther()
        //      }
    }

    private fun canCreateClaimOther(): Boolean {
        return RuleHelper().hasRules(arrayListOf(
                HttpRequest(Method.POST.value, ClaimUrlPath.CREATE_CLAIM)))
    }

    private fun addTypeOther() {
        types.add(Claim.ClaimType.OTHER)
    }


}