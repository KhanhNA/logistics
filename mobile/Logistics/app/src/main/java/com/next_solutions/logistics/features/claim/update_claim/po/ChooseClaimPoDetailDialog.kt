package com.next_solutions.logistics.features.claim.update_claim.po

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.PoDetail

class ChooseClaimPoDetailDialog : BaseDialogFragment() {
    private lateinit var claimDetailViewModel: ChooseClaimPoDetailViewModel
    private lateinit var updateClaimViewModel: UpdateClaimPoViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpViewModel()
        setUpRecycleView()
        claimDetailViewModel.init()
        return binding.root
    }

    private fun setUpViewModel() {
        claimDetailViewModel = viewModel as ChooseClaimPoDetailViewModel
        updateClaimViewModel = ViewModelProvider(this.baseActivity,
                BaseViewModelFactory.getInstance<Any>(this.baseActivity.application, UpdateClaimPoViewModel::class.java))
                .get<UpdateClaimPoViewModel>(UpdateClaimPoViewModel::class.java)
        claimDetailViewModel.updateClaimViewModel = updateClaimViewModel
    }

    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_claim_po_detail_item, claimDetailViewModel, OwnerView { _, detail ->
            run {
                updateClaimViewModel.addClaimDetail(detail as PoDetail)
                dismiss()
            }
        }, baseActivity)
        recyclerView.apply {
            adapter = baseAdapter
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChooseClaimPoDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_claim_po_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}