package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class RepackingPlanningDetail extends Base {
    private RepackingPlanning repackingPlanning;

    private ProductPacking productPacking;

    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date expireDate;

    private Long quantity;

    private List<RepackingPlanningDetailPallet> repackingPlanningDetailPallets;

    private List<RepackingPlanningDetailRepacked> repackingPlanningDetailRepackeds;

    private Long inventory;
    public Long claimQuantity;
    public Long originalQuantity;

    public RepackingPlanning getRepackingPlanning() {
        return repackingPlanning;
    }

    public void setRepackingPlanning(RepackingPlanning repackingPlanning) {
        this.repackingPlanning = repackingPlanning;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public List<RepackingPlanningDetailPallet> getRepackingPlanningDetailPallets() {
        return repackingPlanningDetailPallets;
    }

    public void setRepackingPlanningDetailPallets(List<RepackingPlanningDetailPallet> repackingPlanningDetailPallets) {
        this.repackingPlanningDetailPallets = repackingPlanningDetailPallets;
    }

    public List<RepackingPlanningDetailRepacked> getRepackingPlanningDetailRepackeds() {
        return repackingPlanningDetailRepackeds;
    }

    public void setRepackingPlanningDetailRepackeds(List<RepackingPlanningDetailRepacked> repackingPlanningDetailRepackeds) {
        this.repackingPlanningDetailRepackeds = repackingPlanningDetailRepackeds;
    }

    public Long getInventory() {
        return inventory;
    }

    public void setInventory(Long inventory) {
        this.inventory = inventory;
    }

    public String getExpireDateStr() {
        return expireDate == null ? EMPTY_STRING : DateUtils.clearTimeAndConvertDateToString(expireDate, DateUtils.DATE_FORMAT);
    }

    public String getExpireDateDisplay() {
        return expireDate == null ? EMPTY_STRING : DateUtils.clearTimeAndConvertDateToString(expireDate, DateUtils.DATE_SHOW_FORMAT);
    }
}