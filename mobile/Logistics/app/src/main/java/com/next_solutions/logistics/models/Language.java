package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Language extends Base {
    private Integer sORTORDER;
    private String code;
    @Override
    public String toString() {
        return code;
    }

    public Integer getsORTORDER() {
        return sORTORDER;
    }

    public void setsORTORDER(Integer sORTORDER) {
        this.sORTORDER = sORTORDER;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
