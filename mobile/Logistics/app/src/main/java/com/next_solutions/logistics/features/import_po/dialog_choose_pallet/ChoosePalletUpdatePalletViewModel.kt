package com.next_solutions.logistics.features.import_po.dialog_choose_pallet

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.Pallet

class ChoosePalletUpdatePalletViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    val searchComponent = SearchComponent()
    lateinit var storePallet: List<Pallet>
    override fun init() {
        clearDataRecycleView()
        search()
    }

    override fun search() {
        val textSearch = searchComponent.code.get()!!
        setData(this.storePallet.asSequence().filter {
            it.displayName.contains(textSearch, true) || (it.code.contains(textSearch, true))
        }.toList())
    }
}