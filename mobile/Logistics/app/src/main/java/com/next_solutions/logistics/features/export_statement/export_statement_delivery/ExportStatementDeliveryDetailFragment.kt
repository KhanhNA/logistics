package com.next_solutions.logistics.features.export_statement.export_statement_delivery

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.features.import_statement.claim_delivery.ClaimDeliveryStatementFragment
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.utils.Constants.*

class ExportStatementDeliveryDetailFragment : BaseRecycleViewFragment() {

    private lateinit var deliveryViewModel: ExportStatementDeliveryDetailViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        deliveryViewModel = viewModel as ExportStatementDeliveryDetailViewModel
        getData()
        deliveryViewModel.init()
        return binding.root
    }

    private fun getData() {
        val exportStatement = activity?.intent?.extras?.getSerializable(MODEL)
        val invisibleReviewClaim = activity?.intent?.extras?.getBoolean(INVISIBLE_REVIEW_CLAIM, true)
        if (invisibleReviewClaim != null) {
            deliveryViewModel.invisibleReviewClaim.set(invisibleReviewClaim)
        }
        deliveryViewModel.exportStatement = exportStatement as ExportStatement
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_export_statement_delivery_detail_item
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (view?.id == R.id.createClaim) {
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(FRAGMENT, ClaimDeliveryStatementFragment::class.java)
            bundle.putSerializable(MODEL, deliveryViewModel.exportStatement)
            intent.putExtras(bundle)
            startActivity(intent)
        } else if (view?.id == R.id.reviewClaim) {
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
            bundle.putBoolean(FROM_DETAIL_FRAGMENT, false)
            bundle.putSerializable(MODEL, deliveryViewModel.claim)
            bundle.putSerializable(MODEL_EXPORT_STATEMENT, deliveryViewModel.exportStatement)
            intent.putExtras(bundle)
            startActivity(intent)
        }

    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ExportStatementDeliveryDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_export_statement_delivery_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.export_details
    }

}