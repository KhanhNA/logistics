package com.next_solutions.logistics.base.claim

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.databinding.FragmentClaimImageItemBinding
import com.next_solutions.logistics.models.Image
import com.next_solutions.logistics.utils.RecycleViewCallBack
import com.yanzhenjie.album.Album

class ClaimImageAdapter(private var images: ArrayList<Image>,
                        private var viewModel: BaseClaimViewModel,
                        private var adapter: RecycleViewCallBack) : RecyclerView.Adapter<ClaimImageAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<FragmentClaimImageItemBinding>(LayoutInflater.from(parent.context),
                R.layout.fragment_claim_image_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        images.let { return it.size }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        images.let {
            holder.bind(it[position], adapter, viewModel)
        }
    }

    class ViewHolder(var binding: FragmentClaimImageItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(image: Image, adapter: RecycleViewCallBack, viewModel: BaseClaimViewModel) {
            Album.getAlbumConfig().albumLoader.load(binding.claimImage, image.albumFile)
            binding.listener = adapter
            binding.viewHolder = image
            binding.viewModel = viewModel
        }
    }


}