package com.next_solutions.logistics.models.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.models.Base;
import com.next_solutions.logistics.models.ImportStatementDetailPallet;
import com.next_solutions.logistics.models.ProductPacking;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportStatementDTO extends Base {
    private String code;
    private Long fromStoreId;
    private String fromStoreCode;
    private Long toStoreId;
    private String toStoreCode;
    private Long exportStatementId;
    private Long repackingPlanningId;
    private ProductPacking productPacking;
    private String description;
    private String estimatedTimeOfArrival;
    private List<ImportStatementDetailDTO> importStatementDetails;
    private List<ImportStatementDetailPallet> importStatementDetailPallets;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getFromStoreId() {
        return fromStoreId;
    }

    public void setFromStoreId(Long fromStoreId) {
        this.fromStoreId = fromStoreId;
    }

    public String getFromStoreCode() {
        return fromStoreCode;
    }

    public void setFromStoreCode(String fromStoreCode) {
        this.fromStoreCode = fromStoreCode;
    }

    public Long getToStoreId() {
        return toStoreId;
    }

    public void setToStoreId(Long toStoreId) {
        this.toStoreId = toStoreId;
    }

    public String getToStoreCode() {
        return toStoreCode;
    }

    public void setToStoreCode(String toStoreCode) {
        this.toStoreCode = toStoreCode;
    }

    public Long getExportStatementId() {
        return exportStatementId;
    }

    public void setExportStatementId(Long exportStatementId) {
        this.exportStatementId = exportStatementId;
    }

    public Long getRepackingPlanningId() {
        return repackingPlanningId;
    }

    public void setRepackingPlanningId(Long repackingPlanningId) {
        this.repackingPlanningId = repackingPlanningId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEstimatedTimeOfArrival() {
        return estimatedTimeOfArrival;
    }

    public void setEstimatedTimeOfArrival(String estimatedTimeOfArrival) {
        this.estimatedTimeOfArrival = estimatedTimeOfArrival;
    }

    public List<ImportStatementDetailDTO> getImportStatementDetails() {
        return importStatementDetails;
    }

    public void setImportStatementDetails(List<ImportStatementDetailDTO> importStatementDetails) {
        this.importStatementDetails = importStatementDetails;
    }

    public List<ImportStatementDetailPallet> getImportStatementDetailPallets() {
        return importStatementDetailPallets;
    }

    public void setImportStatementDetailPallets(List<ImportStatementDetailPallet> importStatementDetailPallets) {
        this.importStatementDetailPallets = importStatementDetailPallets;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }
}
