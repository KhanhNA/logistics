package com.next_solutions.logistics.repository.api.po

interface PoUrlPath {
    companion object {
        const val GET_PO = "/pos"
        const val CREATE_PO = "/pos"
        const val FIND_PO_DETAIL = "/pos/{id}"
    }

}