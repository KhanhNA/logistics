package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.DateUtils;
import com.next_solutions.logistics.utils.ValidateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.Date;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExportStatementDetail extends Base {
    private ExportStatement exportStatement;
    private ProductPacking productPacking;
    private Double productPackingPrice;
    private Long quantity;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date importDate;
    private Long productId;
    private String productCode;
    private Long packingTypeId;
    private PackingType packingType;
    private String packingTypeCode;
    private Long packingTypeQuantity;
    private BigDecimal price;
    private String productName;
    private StoreProductPackingDetail storeProductPackingDetail;
    private String expireDate;
    private String expireDateStr;
    private Long totalQuantity;
    private Long productPackingId;
    private String productPackingCode;
    private Long storeProductPackingId;
    private String uom;
    private PalletDetail palletDetail;
    private int state;
    public Long claimQuantity;

    public ExportStatementDetail() {
    }

    public Long getQuantity() {
        return quantity == null ? 0 : quantity;
    }

    public void setQuantityStr(String str) {
        if (ValidateUtils.isNullOrEmpty(str)) {
            quantity = 0L;
        } else {
            quantity = Long.parseLong(str);
        }
    }

    public String getQuantityStr() {
        return String.valueOf(quantity == null ? EMPTY_STRING : quantity);
    }

    public ExportStatementDetail(Long id, ExportStatement exportStatement, Long quantity,
                                 String productCode, Long packingTypeQuantity, String productName,
                                 Long totalQuantity, Long productPackingId, String productPackingCode, String uom) {
        this.id = id;
        this.exportStatement = exportStatement;
        this.quantity = quantity;
        this.productCode = productCode;
        this.packingTypeQuantity = packingTypeQuantity;
        this.productName = productName;
        this.totalQuantity = totalQuantity;
        this.productPackingId = productPackingId;
        this.productPackingCode = productPackingCode;
        this.uom = uom;
    }

    public Long getTotalQuantity() {
        return totalQuantity == null ? 0 : totalQuantity;
    }

    public Long getPackingTypeQuantity() {
        return packingTypeQuantity == null ? 0L : packingTypeQuantity;
    }

    public StoreProductPacking getStoreProductPacking() {
        return this.getStoreProductPackingDetail().getStoreProductPacking();
    }

    public ExportStatement getExportStatement() {
        return exportStatement;
    }

    public void setExportStatement(ExportStatement exportStatement) {
        this.exportStatement = exportStatement;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public Double getProductPackingPrice() {
        return productPackingPrice;
    }

    public void setProductPackingPrice(Double productPackingPrice) {
        this.productPackingPrice = productPackingPrice;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Date getImportDate() {
        return importDate;
    }

    public void setImportDate(Date importDate) {
        this.importDate = importDate;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getPackingTypeId() {
        return packingTypeId;
    }

    public void setPackingTypeId(Long packingTypeId) {
        this.packingTypeId = packingTypeId;
    }

    public PackingType getPackingType() {
        return packingType;
    }

    public void setPackingType(PackingType packingType) {
        this.packingType = packingType;
    }

    public String getPackingTypeCode() {
        return packingTypeCode;
    }

    public void setPackingTypeCode(String packingTypeCode) {
        this.packingTypeCode = packingTypeCode;
    }

    public void setPackingTypeQuantity(Long packingTypeQuantity) {
        this.packingTypeQuantity = packingTypeQuantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public StoreProductPackingDetail getStoreProductPackingDetail() {
        return storeProductPackingDetail;
    }

    public void setStoreProductPackingDetail(StoreProductPackingDetail storeProductPackingDetail) {
        this.storeProductPackingDetail = storeProductPackingDetail;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public void setExpireDateStr(String expireDateStr) {
        this.expireDateStr = expireDateStr;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Long getProductPackingId() {
        return productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }

    public String getProductPackingCode() {
        return productPackingCode;
    }

    public void setProductPackingCode(String productPackingCode) {
        this.productPackingCode = productPackingCode;
    }

    public Long getStoreProductPackingId() {
        return storeProductPackingId;
    }

    public void setStoreProductPackingId(Long storeProductPackingId) {
        this.storeProductPackingId = storeProductPackingId;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public PalletDetail getPalletDetail() {
        return palletDetail;
    }

    public void setPalletDetail(PalletDetail palletDetail) {
        this.palletDetail = palletDetail;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getExpireDateDisplay() {
        return expireDate == null ? EMPTY_STRING : DateUtils.convertStringDateToDifferentType(expireDate, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);
    }
}
