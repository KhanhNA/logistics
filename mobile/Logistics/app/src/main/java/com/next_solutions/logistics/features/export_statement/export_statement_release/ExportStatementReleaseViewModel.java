package com.next_solutions.logistics.features.export_statement.export_statement_release;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.Pageable;
import com.next_solutions.logistics.component.search.SearchComponent;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.ResponseStatus;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.repository.Service;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.DialogCallBack;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@SuppressWarnings("unchecked")
public class ExportStatementReleaseViewModel extends BaseViewModel implements DialogCallBack {

    private SearchComponent searchComponent;

    public ExportStatementReleaseViewModel(@NonNull Application application) {
        super(application);
    }

    private List<ExportStatement> exportStatements;

    @Override
    public void init() {
        searchComponent = new SearchComponent();
    }

    public void setStatementCode(String sCode) {
        searchComponent.setData(sCode);
        onClickButtonSearch();
    }

    @Override
    public void search() {
        Integer page = getCurrentPageNumber();
        callApi(Service
                .callExportStatementService()
                .findExportStatement(searchComponent.getCode().get(),
                        EMPTY_STRING,
                        page, AppUtils.PAGE_SIZE,
                        ExportStatement.ExportStatementStatus.REQUESTED.getValue(),
                        EMPTY_STRING, "from")
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<Pageable<ExportStatement>>(appException, Pageable.class, ExportStatement.class, null) {
                    @Override
                    public void getDataSuccess(Pageable<ExportStatement> exportStatementPageable) {
                        searchSuccess(exportStatementPageable);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    private void searchSuccess(Pageable<ExportStatement> pageable) {
        if (pageable != null) {
            setPage(pageable);
            exportStatements = pageable.getContent();
            setData(exportStatements);
        }
    }

    @Override
    public void callBack(Object object) {
        ExportStatement exportStatement = (ExportStatement) object;
        callApi(Service.callExportStatementService()
                .cancelExportStatement(exportStatement.getId())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    @Override
                    public void getDataSuccess(ResponseStatus responseStatus) {
                        sendBackAction(Constants.SUCCESS);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.getMessage());
                    }
                })
        );
    }

    boolean isExportStatement(Integer position) {
        return exportStatements.get(position).getMerchantOrder() == null;
    }

    @Override
    public void onCancel() {

    }

    public SearchComponent getSearchComponent() {
        return searchComponent;
    }
}
