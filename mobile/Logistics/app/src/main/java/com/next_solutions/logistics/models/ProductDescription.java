package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.DateUtils;
import com.next_solutions.logistics.utils.ValidateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class ProductDescription extends Base {
    private Product product;
    private Language language;
    private String code;
    private String name;
    private Long totalQuantity;
    private Long packingQuantity;
    private Long packingTypeId;
    private Long productPackingId;
    private String productPackingCode;
    private Long storeProductPackingId;
    private String uom;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date expireDate;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof StatementDetail) {
            StatementDetail statementDetail = (StatementDetail) obj;
            return statementDetail.getProductId().equals(this.product.getId());
        }
        return super.equals(obj);
    }

    public ProductDescription(String code, String name, Long totalQuantity, Long packingQuantity, Long productPackingId, String productPackingCode, String uom) {
        this.code = code;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.packingQuantity = packingQuantity;
        this.productPackingId = productPackingId;
        this.productPackingCode = productPackingCode;
        this.uom = uom;
    }

    public ProductDescription() {

    }

    public Long getTotalQuantity() {
        return totalQuantity == null ? 0 : totalQuantity;
    }

    public Long getPackingQuantity() {
        return packingQuantity == null ? 0 : packingQuantity;
    }

    public String getTotalQuantityStr() {
        return totalQuantity == null ? Constants.EMPTY_STRING : totalQuantity.toString();
    }

    public String getPackingQuantityStr() {
        return packingQuantity == null ? Constants.EMPTY_STRING : packingQuantity.toString();
    }

    public String getExpireDateStr() {
        return expireDate == null ? Constants.EMPTY_STRING : DateUtils.convertDateToString(expireDate, DateUtils.DATE_SHOW_FORMAT);
    }

    public String getUom() {
        return ValidateUtils.isNullOrEmpty(uom) ? "" : uom;
    }

    public String getPackingTypeStr() {
        return uom == null ? null : packingQuantity + " " + uom;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public void setPackingQuantity(Long packingQuantity) {
        this.packingQuantity = packingQuantity;
    }

    public Long getPackingTypeId() {
        return packingTypeId;
    }

    public void setPackingTypeId(Long packingTypeId) {
        this.packingTypeId = packingTypeId;
    }

    public Long getProductPackingId() {
        return productPackingId;
    }

    public void setProductPackingId(Long productPackingId) {
        this.productPackingId = productPackingId;
    }

    public String getProductPackingCode() {
        return productPackingCode;
    }

    public void setProductPackingCode(String productPackingCode) {
        this.productPackingCode = productPackingCode;
    }

    public Long getStoreProductPackingId() {
        return storeProductPackingId;
    }

    public void setStoreProductPackingId(Long storeProductPackingId) {
        this.storeProductPackingId = storeProductPackingId;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

}
