package com.next_solutions.logistics.features.import_statement.create_import_statement;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.Pageable;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.ExportStatement;
import com.next_solutions.logistics.repository.Service;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

@SuppressWarnings("unchecked")
public class ImportStatementFromExportViewModel extends BaseViewModel {
    private ObservableField<String> exportStatementCode = new ObservableField<>("");

    public ImportStatementFromExportViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void init() {
        search();
    }

    private void searchSuccess(Pageable<ExportStatement> pageable) {
        setPage(pageable);
        List<ExportStatement> exportStatements = pageable.getContent();
        setData(exportStatements);
    }

    @Override
    public void search() {
        Integer page = getCurrentPageNumber();
        callApi(Service.callExportStatementService()
                .findExportStatement(exportStatementCode.get(),
                        null,
                        page, AppUtils.PAGE_SIZE,
                        ExportStatement.ExportStatementStatus.RELEASED.getValue(), null, "to")
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<Pageable<ExportStatement>>(appException, Pageable.class, ExportStatement.class, null) {
                    @Override
                    public void getDataSuccess(Pageable<ExportStatement> exportStatementPageable) {
                        searchSuccess(exportStatementPageable);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    void searchByCode(String code) {
        exportStatementCode.set(code);
        onClickButtonSearch();
    }

    public ObservableField<String> getExportStatementCode() {
        return exportStatementCode;
    }
}
