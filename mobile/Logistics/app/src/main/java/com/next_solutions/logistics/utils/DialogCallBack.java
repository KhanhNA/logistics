package com.next_solutions.logistics.utils;

public interface DialogCallBack {
    void callBack(Object object);

    void onCancel();
}
