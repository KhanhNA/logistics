package com.next_solutions.logistics.features.export_statement.claim_export_statement

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.ExportStatementDetail
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class DialogChooseExportStatementDetail : BaseDialogFragment() {

    private lateinit var dialogChooseExportStatementDetailViewModel: DialogChooseExportStatementDetailViewModel
    private lateinit var claimExportStatementViewModel: ClaimExportStatementViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        claimExportStatementViewModel = ViewModelProvider(baseActivity,
                BaseViewModelFactory.getInstance<Any>(baseActivity.application, ClaimExportStatementViewModel::class.java))
                .get(ClaimExportStatementViewModel::class.java)
        dialogChooseExportStatementDetailViewModel = viewModel as DialogChooseExportStatementDetailViewModel
        setUpRecycleView()
        dialogChooseExportStatementDetailViewModel.init(claimExportStatementViewModel)
        return binding.root
    }

    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_export_statement_detail_item, dialogChooseExportStatementDetailViewModel, OwnerView { _, detail ->
            run {
                claimExportStatementViewModel.addDetail(detail as ExportStatementDetail)
                dismiss()
            }
        }, baseActivity)
        recyclerView.adapter = baseAdapter
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (Constants.SCAN_CODE == view!!.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            val result = data!!.getStringExtra(Constants.RESULT_SCAN)
            dialogChooseExportStatementDetailViewModel.searchComponent.setData(result)
            dialogChooseExportStatementDetailViewModel.search()
        }
        closeProcess()
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return DialogChooseExportStatementDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_export_statement_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.details
    }

}