package com.next_solutions.logistics.models.dto;

import com.next_solutions.logistics.models.Pallet;

public class SplitRepack {
    private Long quantity;
    private Pallet pallet;
    private String expireDate;
    private String productPackingCode;

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getProductPackingCode() {
        return productPackingCode;
    }

    public void setProductPackingCode(String productPackingCode) {
        this.productPackingCode = productPackingCode;
    }

    public Long getQuantity() {
        return quantity == null ? 0L : quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Pallet getPallet() {
        return pallet;
    }

    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }
}
