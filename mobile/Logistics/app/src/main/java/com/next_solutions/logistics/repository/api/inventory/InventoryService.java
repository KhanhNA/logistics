package com.next_solutions.logistics.repository.api.inventory;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.next_solutions.logistics.repository.api.inventory.InventoryUrlPath.GET_DETAIL_INVENTORY;
import static com.next_solutions.logistics.repository.api.inventory.InventoryUrlPath.GET_INVENTORY;


public interface InventoryService {
    //    ========================INVENTORY========================

    @GET(GET_INVENTORY)
    Single<Response<ResponseBody>> getInventory(@Query("distributorId") Long distributorId,
                                                @Query("pageNumber") Integer pageNumber,
                                                @Query("pageSize") Integer pageSize,
                                                @Query("exceptProductPackingIds") List<Long> exceptProductPackingIds,
                                                @Query("storeId") Long storeId,
                                                @Query("text") String text);

    @GET(GET_DETAIL_INVENTORY)
    Single<Response<ResponseBody>> getDetailInventory(@Query("expireDate") String expireDate,
                                                      @Query("productPackingId") Long productPackingId,
                                                      @Query("storeId") Long storeId);

}
