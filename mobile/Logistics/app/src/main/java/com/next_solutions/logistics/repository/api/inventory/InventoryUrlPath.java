package com.next_solutions.logistics.repository.api.inventory;

public interface InventoryUrlPath {
    String GET_INVENTORY = "/stores/inventory";
    String GET_DETAIL_INVENTORY = "/stores/inventory/pallet-details";
}
