package com.next_solutions.logistics.features.repacking_planning.imported_statement

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.base.adapter.ParentModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.helper.ImportStatementDetailHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.NOTIFY_CHANGE

@Suppress("UNCHECKED_CAST")
class ImportedRepackingDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    lateinit var repackingPlanning: RepackingPlanning
    val searchComponent = SearchComponent()
    var importStatement = ObservableField(ImportStatement())
    var listData = arrayListOf<ParentModel?>()
    var lastNotifyPosition: Int? = null
    var claimDetails: List<ClaimDetail>? = null
    var claim: ObservableField<Boolean> = ObservableField(false)
    override fun init() {
        getImportReferenceRepacking()
    }

    private fun getImportReferenceRepacking() {
        callApi(Service.callImportStatementService()
                .findImportStatementFromRepackingPlanning(repackingPlanning.getId())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ImportStatement>(appException, ImportStatement::class.java, null) {
                    override fun getDataSuccess(statement: ImportStatement) {
                        getImportStatementSuccess(statement)
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message)
                    }
                }))
    }

    override fun search() {
        val data = baseModelsE as List<ImportStatementDetailPallet>
        val textSearch = searchComponent.code.get().toString()
        for ((position, detail) in data.withIndex()) {
            if (detail.palletDetail.productPacking.code.contains(textSearch, true)
                    || (detail.palletDetail.productPacking.product.name.contains(textSearch, true))) {
                updateLastNotifyPosition()
                updateNotifyPosition(position, detail)
                sendBackAction(Constants.SCROLL_TO_POSITION)
            }
        }
    }

    private fun updateNotifyPosition(position: Int, detail: ImportStatementDetailPallet) {
        notifyPosition.set(position)
        detail.checked = true
    }

    private fun updateLastNotifyPosition() {
        val data = baseModelsE as List<ImportStatementDetailPallet>
        lastNotifyPosition = notifyPosition.get()
        lastNotifyPosition?.let { data[it].checked = null }
    }

    private fun getImportStatementSuccess(statement: ImportStatement) {
        this.importStatement.set(statement)
        this.importStatement.notifyChange()
        getClaimReference()

    }

    private fun getClaimReference() {
        callApi(Service.callClaimService()
                .findClaimByReference(repackingPlanning.id, Claim.ClaimType.IMPORT_STATEMENT_REPACKING_PLANNING.value)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, List::class.java) {
                    override fun getDataSuccess(claimDetails: List<ClaimDetail>) {
                        getClaimReferenceSuccess(claimDetails)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {

                    }
                }))
    }

    private fun getClaimReferenceSuccess(claimDetails: List<ClaimDetail>) {
        this.claimDetails = claimDetails
        if (!claimDetails.isNullOrEmpty()) {
            claim.set(true)
        } else {
            claim.set(false)
        }
        claim.notifyChange()
        groupProductPacking()
    }

    private fun groupProductPacking() {
        val statement = importStatement.get()!!
        val convertHelper = ImportStatementDetailHelper()
        statement.importStatementDetails
                .asSequence().forEach { importDetail ->
                    val claimDetailMatch = claimDetails!!
                            .find {
                                it.expireDate == importDetail.expireDate &&
                                        it.productPacking.code == importDetail.productPacking.code
                            }
                    if (claimDetailMatch != null) {
                        importDetail.claimQuantity = claimDetailMatch.quantity
                    }

                    val palletDetails = statement.importStatementDetailPallets.filter {
                        it.importStatementDetailId == importDetail.id
                    }
                    importDetail.importStatementDetailPallets = palletDetails
                    listData.add(convertHelper.convertImportStatementDetailToParentModel(importDetail))
                }
        sendBackAction(NOTIFY_CHANGE)
    }

    fun searchByCode(scanCode: String) {
        searchComponent.setData(scanCode)
        onClickButtonSearch()
    }
}