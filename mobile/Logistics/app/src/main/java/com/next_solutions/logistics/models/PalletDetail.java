package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.utils.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class PalletDetail extends Base {

    private Pallet pallet;

    private Product product;

    private ProductPacking productPacking;

    private ProductPackingPrice productPackingPrice;

    private Long quantity;

    private Long orgQuantity;

    private String expireDate;

    private ImportPoDetailPallet importPoDetailPallet;

    private Long inventory;

    private StoreProductPackingDetail storeProductPackingDetail;

    public Pallet getPallet() {
        return pallet;
    }

    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductPacking getProductPacking() {
        return productPacking;
    }

    public void setProductPacking(ProductPacking productPacking) {
        this.productPacking = productPacking;
    }

    public ProductPackingPrice getProductPackingPrice() {
        return productPackingPrice;
    }

    public void setProductPackingPrice(ProductPackingPrice productPackingPrice) {
        this.productPackingPrice = productPackingPrice;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getOrgQuantity() {
        return orgQuantity;
    }

    public void setOrgQuantity(Long orgQuantity) {
        this.orgQuantity = orgQuantity;
    }


    public ImportPoDetailPallet getImportPoDetailPallet() {
        return importPoDetailPallet;
    }

    public void setImportPoDetailPallet(ImportPoDetailPallet importPoDetailPallet) {
        this.importPoDetailPallet = importPoDetailPallet;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public Long getInventory() {
        return inventory;
    }

    public void setInventory(Long inventory) {
        this.inventory = inventory;
    }

    public String getExpireDateDisplay() {
        return expireDate == null ? EMPTY_STRING : DateUtils.convertStringDateToDifferentType(this.expireDate, DateUtils.DATE_FORMAT, DateUtils.DATE_SHOW_FORMAT);
    }

    public StoreProductPackingDetail getStoreProductPackingDetail() {
        return storeProductPackingDetail;
    }

    public void setStoreProductPackingDetail(StoreProductPackingDetail storeProductPackingDetail) {
        this.storeProductPackingDetail = storeProductPackingDetail;
    }
}
