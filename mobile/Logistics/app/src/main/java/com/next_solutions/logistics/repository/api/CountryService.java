package com.next_solutions.logistics.repository.api;

import com.next_solutions.logistics.models.Country;
import com.next_solutions.logistics.models.CountryDescription;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CountryService {
    //  ========================COUNTRIES========================
    @GET("/countries")
    Call<List<CountryDescription>> getAllLanguage();

    @GET("/countries")
    Call<ResponseBody> getAllCountriesDescription();

    @GET("/countries")
    Call<ResponseBody> getAllCountriesDescription(@Query("support") Boolean support,
                                                  @Query("language") Long language);

    @GET("/countries/{id}")
    Call<ResponseBody> getOneCountry(@Path("id") Long id);

    @PATCH("/countries/{id}")
    Call<ResponseBody> updateCountryDescription(@Path("id") Long id, @Body Country source);
}
