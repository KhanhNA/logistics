package com.next_solutions.logistics.repository.api.import_po;

import com.next_solutions.logistics.models.ImportPoDetailPallet;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.next_solutions.logistics.repository.api.import_po.ImportPoUrlPath.FIND_BY_ID;
import static com.next_solutions.logistics.repository.api.import_po.ImportPoUrlPath.FIND_IMPORT_PO;
import static com.next_solutions.logistics.repository.api.import_po.ImportPoUrlPath.UPDATE_PALLET;

public interface ImportPoService {
    //    ========================IMPORT POS========================

    @GET(FIND_IMPORT_PO)
    Single<Response<ResponseBody>> findImportPo(@Query("distributorId") Long distributorId,
                                                @Query("pageNumber") Integer pageNumber,
                                                @Query("pageSize") Integer pageSize,
                                                @Query("status") Integer status,
                                                @Query("storeId") Long storeId,
                                                @Query("text") String text);

    @GET(FIND_BY_ID)
    Single<Response<ResponseBody>> findById(@Path("id") Long id);

    @POST(UPDATE_PALLET)
    Single<Response<ResponseBody>> updatePallet(@Path("id") Long id, @Body List<ImportPoDetailPallet> importPoDetailPallets);

}
