package com.next_solutions.logistics.features.export_statement.create_export_statement

import android.app.Application
import android.util.Log
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.ExportStatementDetail
import com.next_solutions.logistics.models.Inventory
import com.next_solutions.logistics.models.ProductPacking
import com.next_solutions.logistics.models.helper.ExportStatementDetailHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants

class PackingProductDialogViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    private lateinit var exceptId: ArrayList<Long>
    private var fromStoreId: Long = 0
    private var exportStatementId: Long? = null

    var textSearch = Constants.EMPTY_STRING

    fun init(exceptId: ArrayList<Long>, fromStoreId: Long, exportStatementId: Long?) {
        clearDataRecycleView()
        textSearch = Constants.EMPTY_STRING
        this.exceptId = exceptId
        this.fromStoreId = fromStoreId
        this.exportStatementId = exportStatementId
    }

    override fun search() {
        searchByService()
    }


    private fun searchByService() {
        val page = currentPageNumber
        val detailsIds = exceptId
        val exceptId: MutableList<Long> = ArrayList()
        exceptId.addAll(detailsIds)
        exceptId.add(-1L)
        if (page != null && fromStoreId != 0L) {
            callApi(Service.callStoreService()
                    .getInventory(textSearch, fromStoreId, page, AppUtils.PAGE_SIZE, exportStatementId, exceptId)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<Pageable<Inventory>>(appException, Pageable::class.java, Inventory::class.java, null) {
                        override fun getDataSuccess(pageable: Pageable<Inventory>) {
                            searchProductSuccess(pageable)
                        }

                        override fun onFailure(errorRespone: ErrorRespone) {
                            Log.e(Constants.ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                        }
                    }))
        }
    }

    private fun searchProductSuccess(inventoryPageable: Pageable<Inventory>) {
        page = inventoryPageable
        val inventories = inventoryPageable.content
        val data: MutableList<ExportStatementDetail> = ArrayList()
        for (inventory in inventories) {
            val exportStatementDetail = ExportStatementDetail()
            exportStatementDetail.productPacking = ProductPacking()
            val detail = ExportStatementDetailHelper().convertInventoryToExportStatementDetail(inventory)
            data.add(detail)
        }
        setData(data as List<BaseModel>?)
    }

}