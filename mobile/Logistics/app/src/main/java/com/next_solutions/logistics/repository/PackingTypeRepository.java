package com.next_solutions.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.next_solutions.logistics.config.AppDatabase;
import com.next_solutions.logistics.models.dao.PackingTypeDao;
import com.next_solutions.logistics.models.entity.PackingTypeEntity;

import java.util.List;

public class PackingTypeRepository {
    private PackingTypeDao packingTypeDao;
    public PackingTypeRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        packingTypeDao = db.packingTypeDao();
    }

    public void insert(PackingTypeEntity entity) {
        new PackingTypeRepository.insertAsyncTask(packingTypeDao).execute(entity);
    }
    public void insert(List<PackingTypeEntity> entity) {
        PackingTypeEntity[] array = entity.toArray(new PackingTypeEntity[0]);
        new PackingTypeRepository.insertAsyncTask(packingTypeDao).execute(array);
    }
    private static class insertAsyncTask extends AsyncTask<PackingTypeEntity, Void, Void> {

        private PackingTypeDao mAsyncTaskDao;

        insertAsyncTask(PackingTypeDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final PackingTypeEntity... params) {
            mAsyncTaskDao.insert(params);
            return null;
        }
    }
}
