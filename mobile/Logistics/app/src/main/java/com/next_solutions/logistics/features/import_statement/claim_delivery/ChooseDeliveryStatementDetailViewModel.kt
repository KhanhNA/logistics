package com.next_solutions.logistics.features.import_statement.claim_delivery

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.ImportStatementDetail

@Suppress("UNCHECKED_CAST")
class ChooseDeliveryStatementDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    private lateinit var importDetails: List<ImportStatementDetail>
    private lateinit var claimDeliveryStatementViewModel: ClaimDeliveryStatementViewModel
    var searchComponent = SearchComponent()
    fun init(claimExportStatementViewModel: ClaimDeliveryStatementViewModel) {
        this.claimDeliveryStatementViewModel = claimExportStatementViewModel
        this.importDetails = claimExportStatementViewModel.importDetails
        searchComponent.resetData()
        search()
    }

    override fun search() {
        val details = claimDeliveryStatementViewModel.baseModelsE as List<ClaimDetail>
        val codeSearch = searchComponent.getCodeSearch()
        setData(importDetails
                .asSequence()
                .filter {
                    contain(details, it)
                }
                .filter {
                    it.productPacking.product.name.contains(codeSearch, true)
                            || (it.productPacking.code.contains(codeSearch, true))
                }
                .toList())
    }

    private fun contain(details: List<ClaimDetail>, importStatementDetail: ImportStatementDetail): Boolean {
        return details.asSequence().find { claimDetail ->
            claimDetail.productPacking.id == importStatementDetail.productPacking.id
                    && claimDetail.expireDate == importStatementDetail.expireDate
        } == null
    }
}