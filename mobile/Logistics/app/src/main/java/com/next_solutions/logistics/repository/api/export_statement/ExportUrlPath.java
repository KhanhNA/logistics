package com.next_solutions.logistics.repository.api.export_statement;

public interface ExportUrlPath {
    String UPDATE_EXPORT_STATEMENT = "/export-statements/{id}";
    String CREATE_EXPORT_STATEMENT = "/export-statements";
    String FIND_EXPORT_STATEMENT = "/export-statements";
    String FIND_EXPORT_STATEMENT_BY_CODE = "/export-statements/get-by-code";
    String FIND_EXPORT_STATEMENT_BY_ID = "/export-statements/{id}";
    String CANCEL_EXPORT_STATEMENT_DETAIL = "/export-statements/cancel-detail/{id}";
    String CANCEL_EXPORT_STATEMENT = "/export-statements/cancel/{id}";
    String ACTUALLY_EXPORT = "/export-statements/{id}/export";
}
