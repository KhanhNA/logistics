package com.next_solutions.logistics.repository.api;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ProductService {

    //  ========================PRODUCT========================
    @GET("/products")
    Call<ResponseBody> searchProduct(@Query("text") String text,
                                     @Query("language") Long language,
                                     @Query("exceptId") List<Long> exceptId,
                                     @Query("PageNumber") Integer pageNumber,
                                     @Query("PageSize") Integer pageSize);

    @GET("/products/packing-type")
    Call<ResponseBody> getPackingType(@Query("status") Boolean status);

}
