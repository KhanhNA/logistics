package com.next_solutions.logistics.features.home;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.features.claim.claim_pallet_step_one.ClaimPalletStepOneFragment;
import com.next_solutions.logistics.features.claim.claim_pallet_step_two.ClaimPalletStepTwoFragment;
import com.next_solutions.logistics.features.claim.find_claim.ClaimStatementFragment;
import com.next_solutions.logistics.features.export_statement.create_export_statement.ExportStatementFragment;
import com.next_solutions.logistics.features.export_statement.export_statement_release.ExportStatementReleaseFragment;
import com.next_solutions.logistics.features.import_po.find_import_po.UpdatePalletFragment;
import com.next_solutions.logistics.features.import_statement.create_import_statement.ImportStatementFromExportFragment;
import com.next_solutions.logistics.features.import_statement.search.ImportStatementFragment;
import com.next_solutions.logistics.features.inventory.InventoryFragment;
import com.next_solutions.logistics.features.po.search_po.SearchPoFragment;
import com.next_solutions.logistics.features.repacking_planning.find_repacking.FindRepackingPlanningFragment;
import com.next_solutions.logistics.features.test.ListOrderFragment;
import com.next_solutions.logistics.models.Principal;
import com.next_solutions.logistics.models.entity.PalletEntity;
import com.next_solutions.logistics.models.entity.StoreEntity;
import com.next_solutions.logistics.utils.ValidateUtils;
import com.next_solutions.logistics.utils.app_model.MenuTitle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeViewModel extends BaseViewModel {
    private String fullName;
    private String role;
    private LiveData<List<PalletEntity>> pallets;
    private LiveData<List<StoreEntity>> stores;


    public HomeViewModel(@NonNull Application application) {
        super(application);
        Principal principal = AppUtils.getRule().getUserAuthentication().getPrincipal();
        fullName = principal.getFirstName() + " " + principal.getLastName();
        role = principal.getRoles().get(0).getRoleName();
    }

    private Map<String, MenuTitle> menuTitleMap = new HashMap<String, MenuTitle>() {
    };

    MenuTitle findMenuByCode(String title) {
        if (ValidateUtils.isNullOrEmpty(title) || !menuTitleMap.containsKey(title)) {
            return null;
        }
        return menuTitleMap.get(title);
    }

    void initMenu(Context baseContext) {
        menuTitleMap.put("CATEGORY", new MenuTitle(baseContext.getString(R.string.category), R.drawable.category, null, null));
        menuTitleMap.put("INVENTORY", new MenuTitle(baseContext.getString(R.string.inventory_header), R.drawable.manage_fc, new InventoryFragment(), null));

        menuTitleMap.put("MERCHANT_ORDERs", new MenuTitle(baseContext.getResources().getString(R.string.order_processing), R.drawable.merchant_order, null, null));
        menuTitleMap.put("MERCHANT_ORDERs_LIST_MERCHANT_ORDER", new MenuTitle(baseContext.getString(R.string.check_orders_of_merchant), R.drawable.list_merchant_order, new ListOrderFragment(), null));

        menuTitleMap.put("IMPORT_STATEMENTs", new MenuTitle(baseContext.getString(R.string.import_statement), R.drawable.import_statement, null, null));
        menuTitleMap.put("IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT", new MenuTitle(baseContext.getString(R.string.create_import_statement_from_export_statement), R.drawable.create_import_statement, new ImportStatementFromExportFragment(), null));
        menuTitleMap.put("IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT_RELEASES", new MenuTitle(baseContext.getString(R.string.create_import_statement_from_export_statement_release), R.drawable.import_release, new ImportStatementFragment(), null));

        menuTitleMap.put("EXPORT_STATEMENTs", new MenuTitle(baseContext.getString(R.string.export_statement), R.drawable.export_statement, null, null));
        menuTitleMap.put("EXPORT_STATEMENTs_EXPORT_STATEMENT", new MenuTitle(baseContext.getString(R.string.create_an_export_statement), R.drawable.create_export_statement, new ExportStatementFragment(), null));
        menuTitleMap.put("EXPORT_STATEMENTs_RELEASES", new MenuTitle(baseContext.getString(R.string.export_release), R.drawable.export_release, new ExportStatementReleaseFragment(), null));

        menuTitleMap.put("MANAGE_FC", new MenuTitle(baseContext.getString(R.string.manage_fc), R.drawable.manage_fc, new ImportStatementFragment(), null));
        menuTitleMap.put("MANAGE_FC_UPDATE_PALLET", new MenuTitle(baseContext.getString(R.string.update_pallet), R.drawable.update_pallet, new UpdatePalletFragment(), null));
        menuTitleMap.put("MANAGE_FC_IMPORT_STATEMENTs_FROM_REPACKING_PLANNING", new MenuTitle(baseContext.getString(R.string.create_import_statement_from_repacking_planning), R.drawable.import_fc_from_repack, new FindRepackingPlanningFragment(), null));

        menuTitleMap.put("PUCHASEORDER", new MenuTitle(baseContext.getString(R.string.po), R.drawable.po, null, null));
        menuTitleMap.put("PUCHASEORDER_LIST", new MenuTitle(baseContext.getString(R.string.list_po), R.drawable.list_po, new SearchPoFragment(), null));

        menuTitleMap.put("CLAIM", new MenuTitle(baseContext.getString(R.string.claim_pallet), R.drawable.claim_pallet, null, null));
        menuTitleMap.put("CLAIM_PALLET_STEP_ONE", new MenuTitle(baseContext.getString(R.string.claim_pallet_step_one), R.drawable.crack_one, new ClaimPalletStepOneFragment(), null));
        menuTitleMap.put("CLAIM_PALLET_STEP_TWO", new MenuTitle(baseContext.getString(R.string.claim_pallet_step_two), R.drawable.crack_two, new ClaimPalletStepTwoFragment(), null));

        menuTitleMap.put("CLAIM_FIND", new MenuTitle(baseContext.getString(R.string.search_claim), R.drawable.search_claim, new ClaimStatementFragment(), null));
    }

    public String getFullName() {
        return fullName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public LiveData<List<PalletEntity>> getPallets() {
        return pallets;
    }

    public void setPallets(LiveData<List<PalletEntity>> pallets) {
        this.pallets = pallets;
    }

    public LiveData<List<StoreEntity>> getStores() {
        return stores;
    }

    public void setStores(LiveData<List<StoreEntity>> stores) {
        this.stores = stores;
    }

}
