package com.next_solutions.logistics.features.import_po.import_po_out_of_pallet

import android.app.Application
import com.next_solutions.logistics.base.import_po.BaseImportPoViewModel
import com.next_solutions.logistics.models.ImportPoDetail

class ImportPoOutOfPalletViewModel(application: Application) : BaseImportPoViewModel(application) {
    override fun updateTotalQuantity() {
        this.importPo.importPoDetails.forEach { detail ->
            run {
                val key = getKeyMapping(detail)
                detail.importPoDetailPallets.forEach { detailPallet ->
                    run {
                        if (currentTotalQuantity.get()!![key] == null) {
                            currentTotalQuantity.get()!![key] = 0
                        }

                        currentTotalQuantity.get()!![key] =
                                currentTotalQuantity.get()!![key]!! + detailPallet.quantity
                    }
                }
            }
        }
    }

    override fun getKeyMapping(detail: ImportPoDetail): String {
        return detail.productPacking.code + detail.expireDate
    }

    override fun avoidRecursiveData(): List<ImportPoDetail> {
        return this.importPo.importPoDetails
    }
}