package com.next_solutions.logistics.features.change_password

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants

class ChangePasswordViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    var oldPassword: String? = null
    var newPassword: String? = null
    var newConfirmPassword: String? = null
    var buttonEnable = ObservableField(true)
    private fun validate(): Boolean {
        if (oldPassword == null) {
            sendBackAction("EMPTY_OLD_PASSWORD")
            return false
        }
        if (newConfirmPassword == null) {
            sendBackAction("EMPTY_NEW_PASSWORD")
            return false
        }
        if (!newPassword.equals(newConfirmPassword)) {
            sendBackAction("NEW_PASSWORD_NOT_EQUAL_NEW_CONFIRM_PASSWORD")
            return false
        }
        return true
    }

    fun onCreate() {
        if (validate()) {
            buttonEnable.set(false)
            callApi(Service.callUserService().changePassword(newConfirmPassword, newPassword, oldPassword)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                        override fun getDataSuccess(respone: ResponseStatus?) {
                            sendBackAction(Constants.SUCCESS)
                        }

                        override fun onFailure(errorRespone: ErrorRespone?) {
                            buttonEnable.set(true)
                            Log.e(Constants.ERROR_PROCESS, errorRespone!!.message)
                        }
                    }))
        }
    }

}