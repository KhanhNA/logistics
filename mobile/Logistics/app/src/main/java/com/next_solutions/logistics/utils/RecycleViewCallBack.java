package com.next_solutions.logistics.utils;

import android.view.View;

public interface RecycleViewCallBack {
    void onItemClick(View v, Object o);
}
