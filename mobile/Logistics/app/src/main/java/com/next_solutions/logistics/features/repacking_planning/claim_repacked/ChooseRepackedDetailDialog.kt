package com.next_solutions.logistics.features.repacking_planning.claim_repacked

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.next_solutions.base.*
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.models.RepackingPlanningDetailRepacked
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ChooseRepackedDetailDialog : BaseDialogFragment() {

    private lateinit var chooseDetailViewModel: ChooseRepackedDetailViewModel
    private lateinit var claimRepackViewModel: ClaimRepackingRepackViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        claimRepackViewModel = ViewModelProvider(baseActivity,
                BaseViewModelFactory.getInstance<Any>(baseActivity.application, ClaimRepackingRepackViewModel::class.java))
                .get(ClaimRepackingRepackViewModel::class.java)

        chooseDetailViewModel = viewModel as ChooseRepackedDetailViewModel
        setUpRecycleView()
        chooseDetailViewModel.init(claimRepackViewModel)
        return binding.root
    }

    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_choose_repacked_detail_item, chooseDetailViewModel,
                OwnerView { _, detail ->
                    run {
                        claimRepackViewModel.addDetail(detail as RepackingPlanningDetailRepacked)
                        dismiss()
                    }
                }, baseActivity)
        recyclerView.adapter = baseAdapter
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        if (Constants.SCAN_CODE == view!!.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            val result = data!!.getStringExtra(Constants.RESULT_SCAN)
            chooseDetailViewModel.searchComponent.setData(result)
            chooseDetailViewModel.search()
        }
        closeProcess()
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChooseRepackedDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_choose_repacked_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.repacking_details
    }

}