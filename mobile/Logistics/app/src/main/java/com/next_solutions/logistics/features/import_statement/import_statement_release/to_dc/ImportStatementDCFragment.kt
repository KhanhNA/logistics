package com.next_solutions.logistics.features.import_statement.import_statement_release.to_dc

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.features.import_statement.claim_delivery.ClaimDeliveryStatementFragment
import com.next_solutions.logistics.features.import_statement.claim_import_statement.ClaimImportStatementFragment
import com.next_solutions.logistics.models.ImportStatement
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class ImportStatementDCFragment : BaseRecycleViewFragment() {
    private lateinit var importStatementDCViewModel: ImportStatementDCViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        importStatementDCViewModel = viewModel as ImportStatementDCViewModel
        getData()
        return binding.root
    }

    private fun getData() {
        val importStatement = activity?.intent?.extras?.get(MODEL) as ImportStatement
        importStatementDCViewModel.importStatement = importStatement
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        importStatementDCViewModel.init()
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_import_statement_dc_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { view, baseModel ->
            when (view.id) {
                R.id.check_box -> {
                    baseAdapter?.notifyItemChanged(baseModel.index - 1)
                }
            }
        }
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        when (view!!.id) {
            R.id.scanProduct -> {
                val qrScan = Intent(context, QRCodeScannerActivity::class.java)
                startActivityForResult(qrScan, REQUEST_CODE_SCAN)
            }
            R.id.createImportClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimImportStatementFragment::class.java)
                bundle.putSerializable(MODEL, importStatementDCViewModel.importStatement)
                intent.putExtras(bundle)
                startActivityForResult(intent, CLAIM_ACTIVITY_CODE)
            }
            R.id.viewImportClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
                if (importStatementDCViewModel.claimImportDetails!!.isNotEmpty()) {
                    val claim = importStatementDCViewModel.claimImportDetails!![0].claim
                    claim.claimDetails = importStatementDCViewModel.claimImportDetails!!
                    bundle.putSerializable(MODEL, claim)
                    bundle.putSerializable(MODEL_IMPORT_STATEMENT, importStatementDCViewModel.importStatement)
                    intent.putExtras(bundle)
                }
            }
            R.id.createDeliveryClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimDeliveryStatementFragment::class.java)
                bundle.putSerializable(MODEL, importStatementDCViewModel.importStatement)
                intent.putExtras(bundle)
                startActivityForResult(intent, CLAIM_ACTIVITY_CODE)
            }
            R.id.viewDeliveryClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
                if (importStatementDCViewModel.claimDeliveryDetails!!.isNotEmpty()) {
                    val claim = importStatementDCViewModel.claimDeliveryDetails!![0].claim
                    claim.claimDetails = importStatementDCViewModel.claimDeliveryDetails!!
                    bundle.putSerializable(MODEL, claim)
                    bundle.putSerializable(MODEL_IMPORT_STATEMENT, importStatementDCViewModel.importStatement)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }
        }
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            CREATE_SUCCESS -> {
                showAlertSuccess(TIME_AFTER_DELAY, R.string.import_release_success, null)
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
            }
            NOTIFY_CHANGE -> {
                baseAdapter?.notifyDataSetChanged()
            }
            NOT_MATCH_QUANTITY -> {
                scrollToErrorPosition()
                showAlertWarning(TIME_AFTER_DELAY, R.string.not_match_quantity, null)
            }
        }
    }

    private fun scrollToErrorPosition() {
        baseAdapter!!.notifyItemChanged(importStatementDCViewModel.errorPosition)
        baseAdapter!!.notifyItemChanged(importStatementDCViewModel.lastErrorPosition)
        layoutManager.scrollToPositionWithOffset(importStatementDCViewModel.errorPosition, 20)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_SCAN) {
                importStatementDCViewModel.scanProductWithCode(data!!.getStringExtra(RESULT_SCAN))
            }
            if (requestCode == CLAIM_ACTIVITY_CODE) {
                val result = data!!.getStringExtra(CLAIM_ACTIVITY)!!
                if (result == CREATE_CLAIM_SUCCESS) {
                    baseActivity.onBackPressed()
                }
            }
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ImportStatementDCViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_import_statement_dc
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}