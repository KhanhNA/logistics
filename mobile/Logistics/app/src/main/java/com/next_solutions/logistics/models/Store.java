package com.next_solutions.logistics.models;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.utils.ValidateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.math.BigDecimal;
import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Store extends Base {

    public enum StoreStatus {
        ACTIVE(true),
        IN_ACTIVE(false);
        private boolean value;

        StoreStatus(boolean value) {
            this.value = value;
        }

        public boolean getValue() {
            return this.value;
        }
    }

    private Country country;
    private Language language;
    private Currency currency;
    private String code;
    private String name;
    private String domainName;
    private String email;
    private String logo;
    private String address;
    private String city;
    private String phone;
    private String postalCode;
    private Boolean dc;
    private Boolean status;
    private BigDecimal lat;
    private BigDecimal lng;
    private Store provideStore;
    private List<StoreProductPacking> storeProductPackings;
    private User user;

    public Boolean isDC() {
        return dc == null ? Boolean.FALSE : dc;
    }

    public Boolean isFC() {
        return dc == null ? Boolean.FALSE : !dc;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Boolean getDc() {
        return dc;
    }

    public void setDc(Boolean dc) {
        this.dc = dc;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLng() {
        return lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }

    public Store getProvideStore() {
        return provideStore;
    }

    public void setProvideStore(Store provideStore) {
        this.provideStore = provideStore;
    }

    public List<StoreProductPacking> getStoreProductPackings() {
        return storeProductPackings;
    }

    public void setStoreProductPackings(List<StoreProductPacking> storeProductPackings) {
        this.storeProductPackings = storeProductPackings;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @NonNull
    @Override
    public String toString() {
        if (ValidateUtils.isNullOrEmpty(code)) {
            return name;
        }
        return code + " - " + name;

    }
}
