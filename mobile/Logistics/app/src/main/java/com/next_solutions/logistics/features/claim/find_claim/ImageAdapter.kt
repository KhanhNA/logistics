package com.next_solutions.logistics.features.claim.find_claim

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.next_solutions.logistics.R
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.databinding.FragmentImageItemBinding
import com.next_solutions.logistics.models.ClaimImage
import com.next_solutions.logistics.repository.HttpHelper
import com.next_solutions.logistics.utils.RecycleViewCallBack

class ImageAdapter(private var images: List<ClaimImage>?,
                   private var recycleViewCallBack: RecycleViewCallBack) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<FragmentImageItemBinding>(LayoutInflater.from(parent.context),
                R.layout.fragment_image_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        if (images == null) return 0
        return images!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        images?.let {
            holder.bind(images!![position], recycleViewCallBack)
        }
    }


    class ViewHolder(var binding: FragmentImageItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(claimImage: ClaimImage, recycleViewCallBack: RecycleViewCallBack) {
            if (binding.claimImage != null && claimImage.url != null) {
                val glideUrl = GlideUrl(AppUtils.IMAGE_URL + claimImage.url,
                        LazyHeaders.Builder()
                                .addHeader("Authorization", HttpHelper.tokenLogin)
                                .build())
                Glide.with(binding.claimImage.getContext())
                        .load(glideUrl)
                        .into(binding.claimImage)
            }
            binding.viewHolder = claimImage
            binding.listener = recycleViewCallBack
        }
    }

}