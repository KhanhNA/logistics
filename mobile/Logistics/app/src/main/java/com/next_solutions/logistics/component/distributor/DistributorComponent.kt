package com.next_solutions.logistics.component.distributor

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.R
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Distributor
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants

class DistributorComponent(private val baseViewModel: BaseViewModel<BaseModel>, private val addAll: Boolean) {
    init {
        resetData()
        callApiGetDistributor()
    }

    var distributors: ObservableList<Distributor> = ObservableArrayList()
        private set
    var selectedDistributorPosition = 0
        set(value) {
            baseViewModel.doWhenChooseData(this)
            field = value
        }

    fun getSelectedDistributor(): Distributor? {
        if (selectedDistributorPosition >= distributors.size) {
            return null
        }
        return distributors[selectedDistributorPosition]
    }

    private fun callApiGetDistributor() {
        val exceptIds = ArrayList<Long>()
        exceptIds.add(-1L)
        baseViewModel.callApi(Service.callDistributorService().allDistributor
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<Distributor?>?>(baseViewModel.appException, Distributor::class.java, List::class.java) {
                    override fun getDataSuccess(page: List<Distributor?>?) {
                        page?.let {
                            distributors.clear()
                            if (addAll) {
                                val distributor = Distributor()
                                distributor.name = AppUtils.getInstance().getString(R.string.all)
                                distributors.add(0, distributor)
                            }
                            distributors.addAll(page)
                        }
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.message + " " + this.javaClass.name)
                    }
                }))

    }

    fun resetData() {
        selectedDistributorPosition = 0
    }
}