package com.next_solutions.logistics.features.change_password

import android.content.Intent
import android.view.View
import com.next_solutions.base.BaseFragment
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.R
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.features.login.LoginActivity
import com.next_solutions.logistics.utils.Constants.SUCCESS
import com.next_solutions.logistics.utils.Constants.TIME_AFTER_DELAY

class ChangePasswordFragment : BaseFragment() {

    fun logout() {
        AppUtils.setLanguage(null)
        val intent = Intent(this.activity, LoginActivity::class.java)
        startActivity(intent)
        val preferencesHelper = AppUtils.getInstance().preferencesHelper
        if (preferencesHelper != null) {
            preferencesHelper.refreshToken = ""
            preferencesHelper.token = ""
        }
        this.activity?.finish()
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            SUCCESS -> {

                showAlertSuccess(TIME_AFTER_DELAY, R.string.update_password_success, null);
                logout();
            }
            "EMPTY_OLD_PASSWORD" -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.empty_old_password, null)
            }
            "EMPTY_NEW_PASSWORD" -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.empty_new_password, null)
            }
            "NEW_PASSWORD_NOT_EQUAL_NEW_CONFIRM_PASSWORD" -> {
                showAlertWarning(TIME_AFTER_DELAY, R.string.new_password_not_equal_new_confirm_password, null)
            }
        }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ChangePasswordViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_change_password
    }

    override fun getRecycleResId(): Int {
        return 0
    }

}

