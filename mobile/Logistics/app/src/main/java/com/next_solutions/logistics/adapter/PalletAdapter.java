package com.next_solutions.logistics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.next_solutions.logistics.R;
import com.next_solutions.logistics.models.Pallet;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

public class PalletAdapter extends ArrayAdapter<Pallet> {

    private Context context;
    private List<Pallet> items, tempItems, suggestions;

    public PalletAdapter(Context context, int resource, int textViewResourceId, List<Pallet> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.items = items;
        tempItems = new ArrayList<>(items); // this makes the difference.
        suggestions = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.auto_comple_row, parent, false);
        }
        Pallet pallet = items.get(position);
        if (pallet != null) {
            TextView lblName = view.findViewById(R.id.lbl_name);
            if (lblName != null)
                lblName.setText(pallet.getDisplayName());
        }
        return view;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Pallet) resultValue).getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Pallet pallet : tempItems) {
                    if (pallet.getDisplayName().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            pallet.getCode().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(pallet);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Pallet> filterList = (ArrayList<Pallet>) results.values;
            if (results.count > 0) {
                clear();
                for (Pallet pallet : filterList) {
                    add(pallet);
                    notifyDataSetChanged();
                }
            }
        }
    };
}