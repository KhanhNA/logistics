package com.next_solutions.logistics.base.claim.po

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.logistics.BR
import com.next_solutions.logistics.models.dto.ClaimObject

class ClaimAdapterBase(
        private var layoutRecycleViewL1Id: Int,
        private var layoutRecycleViewL2Id: Int,
        private var recycleViewL2Id: Int,
        private var poViewModel: BaseClaimPoViewModel,
        private var context: Context) : RecyclerView.Adapter<ClaimAdapterBase.ClaimViewHolderL1>() {
    private var listData: ArrayList<ClaimObject> = poViewModel.listData

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClaimViewHolderL1 {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context),
                layoutRecycleViewL1Id, parent, false)
        return ClaimViewHolderL1(binding)
    }

    override fun onBindViewHolder(holder: ClaimViewHolderL1, position: Int) {
        val poClaimDetail = listData[position]
        val childAdapter = ClaimChildAdapterBase(poClaimDetail, layoutRecycleViewL2Id, context, poViewModel)

        val recycledViewPool = RecyclerView.RecycledViewPool()

        val manager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val recyclerView = holder.binding.root.findViewById<RecyclerView>(recycleViewL2Id)
        recyclerView.apply {
            adapter = childAdapter
            layoutManager = manager
        }
        recyclerView.setRecycledViewPool(recycledViewPool)
        holder.bind(poClaimDetail, poViewModel)
    }


    override fun getItemCount(): Int {
        return listData.size
    }

    class ClaimViewHolderL1(var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(claimObject: ClaimObject, poViewModel: BaseClaimPoViewModel) {
            binding.setVariable(BR.viewModel, poViewModel)
            binding.setVariable(BR.viewHolder, claimObject)
        }
    }


}