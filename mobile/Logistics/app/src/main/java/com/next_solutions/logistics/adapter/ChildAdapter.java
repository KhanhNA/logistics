package com.next_solutions.logistics.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.next_solutions.logistics.R;
import com.next_solutions.logistics.models.ImportPoDetailPallet;

import java.util.List;


public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ChildViewHolder> {
    private List<ImportPoDetailPallet> children;
    ChildAdapter(List<ImportPoDetailPallet> children) {
        this.children = children;
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.child, parent, false);
        return new ChildViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildViewHolder holder, int position) {
        ImportPoDetailPallet importPoDetailPallet = children.get(position);
        holder.quantityTV.setText(importPoDetailPallet.getQuantity().toString());
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    class ChildViewHolder extends RecyclerView.ViewHolder {
        TextView quantityTV;
        ChildViewHolder(@NonNull View itemView) {
            super(itemView);
            this.quantityTV = itemView.findViewById(R.id.quantity);
        }
    }
}