package com.next_solutions.logistics.features.repacking_planning.claim_repacking

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.models.RepackingPlanningDetail

@Suppress("UNCHECKED_CAST")
class ChooseRepackingDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    private lateinit var claimRepackingNewViewModel: ClaimRepackingNewViewModel
    private lateinit var repackingPlanning: RepackingPlanning
    var searchComponent = SearchComponent()

    fun init(claimRepackingNewViewModel: ClaimRepackingNewViewModel) {
        this.claimRepackingNewViewModel = claimRepackingNewViewModel
        this.repackingPlanning = claimRepackingNewViewModel.repackingPlanning
        searchComponent.resetData()
        search()
    }

    override fun search() {
        val details = claimRepackingNewViewModel.baseModelsE as List<ClaimDetail>
        setData(claimRepackingNewViewModel.repackingPlanning.repackingPlanningDetails
                .asSequence()
                .filter {
                    contain(details, it)
                }
                .filter {
                    it.productPacking.code.contains(searchComponent.getCodeSearch(), true) || (
                            it.productPacking.product.name.contains(searchComponent.getCodeSearch(), true))
                }
                .toList())
    }

    private fun contain(details: List<ClaimDetail>, repackingDetail: RepackingPlanningDetail): Boolean {
        return details.asSequence().find { claimDetail ->
            claimDetail.productPacking.id == repackingDetail.productPacking.id
                    && claimDetail.expireDate == repackingDetail.expireDateStr
        } == null
    }
}