package com.next_solutions.logistics.models.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PalletMapper {
    PalletMapper INSTANCE = Mappers.getMapper(PalletMapper.class);
}
