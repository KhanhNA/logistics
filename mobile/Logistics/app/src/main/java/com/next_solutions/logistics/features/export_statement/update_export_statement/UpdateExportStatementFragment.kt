package com.next_solutions.logistics.features.export_statement.update_export_statement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.export_statement.BaseExportStatementFragment
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.utils.Constants

class UpdateExportStatementFragment : BaseExportStatementFragment() {
    private lateinit var exportStatementViewModel: UpdateExportStatementViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpViewModel()
        getData()
        return binding.root
    }

    private fun setUpViewModel() {
        exportStatementViewModel = viewModel as UpdateExportStatementViewModel
    }

    private fun getData() {
        val activity = activity
        if (activity != null) {
            val intent = activity.intent
            if (intent != null && intent.hasExtra(Constants.MODEL)) {
                val exportStatement = intent.getSerializableExtra(Constants.MODEL) as ExportStatement
                exportStatementViewModel.exportStatement = exportStatement
                exportStatementViewModel.init()
            }
        }
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_update_export_statement
    }

    override fun getVMClass(): Class<out BaseViewModel<*>?> {
        return UpdateExportStatementViewModel::class.java
    }

    override fun getRecycleResId(): Int {
        return R.id.details
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_update_export_statement_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }
}
