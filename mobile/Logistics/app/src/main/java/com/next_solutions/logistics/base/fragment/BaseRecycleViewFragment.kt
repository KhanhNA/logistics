package com.next_solutions.logistics.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseAdapter
import com.next_solutions.base.BaseFragment
import com.next_solutions.base.listener.OwnerView

abstract class BaseRecycleViewFragment : BaseFragment() {
    var baseAdapter: BaseAdapter? = null
    abstract fun getLayoutItemRes(): Int
    abstract fun getListenerAdapter(): OwnerView?
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpRecycleView()
        return binding.root
    }

    private fun setUpRecycleView() {
        if (recyclerView != null) {
            baseAdapter = BaseAdapter(getLayoutItemRes(), viewModel, getListenerAdapter(), baseActivity)
            recyclerView.adapter = baseAdapter
        }
    }
}