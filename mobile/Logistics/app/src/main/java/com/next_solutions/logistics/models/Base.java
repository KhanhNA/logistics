package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.base.BaseModel;
import com.next_solutions.logistics.config.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;



@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)

public class Base extends BaseModel {
    public Base() {
    }

    public Long id;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    protected Date createDate;
    protected String createUser;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    protected Date updateDate;
    protected String updateUser;

    public Base(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public Base(Long id, Date createDate, String createUser, Date updateDate, String updateUser) {
        this.id = id;
        this.createDate = createDate;
        this.createUser = createUser;
        this.updateDate = updateDate;
        this.updateUser = updateUser;
    }
}
