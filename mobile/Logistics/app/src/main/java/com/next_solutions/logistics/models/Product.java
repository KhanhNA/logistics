package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.next_solutions.logistics.config.AppUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.Date;
import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Product extends Base {
    private ProductType productType;
    private String code;
    private String name;
    private Boolean available;
    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    private Date availableDate;
    private Integer quantityOrdered;
    private Integer sortOrder;
    private Boolean hot;
    private Long totalQuantity;
    private Boolean popular;
    private Integer packingQuantity;
    private Long packingId;
    private String packingCode;
    private Long quantity;
    private Long lifecycle;
    private List<ProductDescription> productDescriptions;
    private Manufacturer manufacturer;
    private Distributor distributor;
    private List<ProductPacking> productPackings;
    private List<ProductPackingPrice> productPackingPrices;

    public String getName() {

        String result = name;
        if (productDescriptions != null) {
            for (ProductDescription productDescription :
                    productDescriptions) {
                if (AppUtils.getLanguage().equals(productDescription.getLanguage().getId())) {
                    result = productDescription.getName();
                }
            }
        }
        return result;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Date getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(Date availableDate) {
        this.availableDate = availableDate;
    }

    public Integer getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(Integer quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean getHot() {
        return hot;
    }

    public void setHot(Boolean hot) {
        this.hot = hot;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Boolean getPopular() {
        return popular;
    }

    public void setPopular(Boolean popular) {
        this.popular = popular;
    }

    public Integer getPackingQuantity() {
        return packingQuantity;
    }

    public void setPackingQuantity(Integer packingQuantity) {
        this.packingQuantity = packingQuantity;
    }

    public Long getPackingId() {
        return packingId;
    }

    public void setPackingId(Long packingId) {
        this.packingId = packingId;
    }

    public String getPackingCode() {
        return packingCode;
    }

    public void setPackingCode(String packingCode) {
        this.packingCode = packingCode;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public List<ProductDescription> getProductDescriptions() {
        return productDescriptions;
    }

    public void setProductDescriptions(List<ProductDescription> productDescriptions) {
        this.productDescriptions = productDescriptions;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public List<ProductPacking> getProductPackings() {
        return productPackings;
    }

    public void setProductPackings(List<ProductPacking> productPackings) {
        this.productPackings = productPackings;
    }

    public List<ProductPackingPrice> getProductPackingPrices() {
        return productPackingPrices;
    }

    public void setProductPackingPrices(List<ProductPackingPrice> productPackingPrices) {
        this.productPackingPrices = productPackingPrices;
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public Long getLifecycle() {
        return lifecycle;
    }

    public void setLifecycle(Long lifecycle) {
        this.lifecycle = lifecycle;
    }
}
