package com.next_solutions.logistics.features.inventory;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.logistics.config.RxSingleSubscriber;
import com.next_solutions.logistics.models.dto.InventoryDetailPalletDTO;
import com.next_solutions.logistics.repository.Service;
import com.next_solutions.logistics.utils.Constants;

import java.util.List;

public class DetailInventoryDialogViewModel extends BaseViewModel {
    private ObservableField<String> textSearch = new ObservableField<>(Constants.EMPTY_STRING);
    private InventoryViewModel inventoryViewModel;

    public DetailInventoryDialogViewModel(@NonNull Application application) {
        super(application);
    }


    @Override
    public void init() {
        clearDataRecycleView();
        search();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void search() {
        callApi(Service.callInventoryService()
                .getDetailInventory(inventoryViewModel.getInventory().getExpireDate(),
                        inventoryViewModel.getInventory().getProductPacking().getId(),
                        inventoryViewModel.getSelectedStoreId())
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(new RxSingleSubscriber<List<InventoryDetailPalletDTO>>(appException, InventoryDetailPalletDTO.class, List.class) {

                    @Override
                    public void getDataSuccess(List<InventoryDetailPalletDTO> pageable) {
                        getPalletSuccess(pageable);
                    }

                    @Override
                    public void onFailure(ErrorRespone errorRespone) {
                        Log.e(Constants.ERROR_PROCESS, errorRespone.getMessage());
                    }
                }));
    }

    private void getPalletSuccess(List<InventoryDetailPalletDTO> pageable) {
        setData(pageable);
    }

    public ObservableField<String> getTextSearch() {
        return textSearch;
    }

    public void setTextSearch(ObservableField<String> textSearch) {
        this.textSearch = textSearch;
    }

    public InventoryViewModel getInventoryViewModel() {
        return inventoryViewModel;
    }

    void setInventoryViewModel(InventoryViewModel inventoryViewModel) {
        this.inventoryViewModel = inventoryViewModel;
    }

}
