package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Config extends Base {

    public String code;
    public String value;

    public String getMaxValue() {
        return value.split("\\|")[1];
    }

    public String getCurrency() {
        return value.split("\\|")[0];
    }


    public enum ConfigType {
        EXPORT_STATEMENT_WARNING("EXPORT_STATEMENT_WARNING");

        private String value;

        ConfigType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }
}
