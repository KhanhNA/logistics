package com.next_solutions.logistics.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.next_solutions.base.RetrofitClient;
import com.next_solutions.base.listener.ResponseResult;
import com.next_solutions.logistics.config.AppUtils;
import com.next_solutions.logistics.utils.PreferencesHelper;
import com.next_solutions.logistics.utils.ValidateUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.next_solutions.logistics.config.AppUtils.DATE_FORMAT;

@SuppressWarnings("unchecked")
public class HttpHelper {
    private static Retrofit retrofit;
    public static String tokenLogin = "";
    private static String authorization = "Authorization";

    private HttpHelper() {

    }

    static Retrofit getClient() {
        if (AppUtils.IS_TEST_MODE) {
            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

            GsonBuilder gsonBuilder = new GsonBuilder();

            okHttpClientBuilder.addInterceptor(chain -> {
                Request request = chain.request();
                okhttp3.Request.Builder newRequest = request.newBuilder();
                return chain.proceed(newRequest.build());
            });

            retrofit = (new retrofit2.Retrofit.Builder())
                    .baseUrl(AppUtils.BASE_URL_TEST)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClientBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .build();
            return retrofit;
        }

        if (retrofit == null) {
            init(tokenLogin);
        }
        return retrofit;
    }

    static Retrofit getAuthClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.addInterceptor(chain -> {
            Request request = chain.request();
            Request.Builder newRequest = request.newBuilder()
                    .addHeader(authorization, HttpHelper.tokenLogin)
                    .addHeader("Accept-Encoding", "identity")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Accept-Language", AppUtils.LANGUAGE_CODE[AppUtils.getLanguage().intValue() - 1]);
            return chain.proceed(newRequest.build());
        });
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(DATE_FORMAT);
        return (new Retrofit.Builder())
                .client(okHttpClientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(AppUtils.BASE_URL_OAUTH)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create())).build();
    }


    public static void init(String accessToken) {
        if (retrofit == null || !ValidateUtils.isNullOrEmpty(accessToken)) {
            HttpHelper.tokenLogin = accessToken;
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder().addInterceptor(interceptor);
            okHttpClientBuilder.addInterceptor(chain -> {
                Request request = chain.request();
                okhttp3.Request.Builder newRequest = request.newBuilder()
                        .addHeader(authorization, accessToken)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept-Encoding", "identity")
                        .addHeader("Accept-Language", AppUtils.LANGUAGE_CODE[AppUtils.getLanguage().intValue() - 1]);
                return chain.proceed(newRequest.build());
            });
            GsonBuilder gsonBuilder = new GsonBuilder();
            retrofit = (new retrofit2.Retrofit.Builder())
                    .baseUrl(AppUtils.BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClientBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create())).build();
        }
    }


    public static void getNewToken(String refreshToken, ResponseResult result) {

        String x = Credentials.basic(AppUtils.CLIENT_ID, AppUtils.CLIENT_SECRET);

        FormBody.Builder builder = new FormBody.Builder();
        builder.add("grant_type", "refresh_token");
        builder.add("refresh_token", refreshToken);

        final Request request = new Request.Builder()
                .url(AppUtils.BASE_URL_OAUTH + "/oauth/token")
                .addHeader(authorization, x)
                .addHeader("Accept-Encoding", "identity")
                .addHeader("Content-type", "application/x-www-form-urlencoded")
                .post(builder.build())
                .build();

        OkHttpClient.Builder builder1 = new OkHttpClient.Builder();

        builder1.build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    ResponseBody body = response.body();
                    if (body != null) {
                        String token = body.string();
                        Type type = new TypeToken<Map<String, String>>() {
                        }.getType();
                        Gson gson = new Gson();
                        Map<String, String> myMap = gson.fromJson(token, type);
                        tokenLogin = myMap.get("token_type") + " " + myMap.get("access_token");
                        PreferencesHelper preferencesHelper = AppUtils.getInstance().getPreferencesHelper();
                        preferencesHelper.setRefreshToken(myMap.get("refresh_token"));
                        preferencesHelper.setToken(HttpHelper.tokenLogin);
                        HttpHelper.init(HttpHelper.tokenLogin);
                        result.onResponse(null, null, token, null);
                    }
                } else {
                    result.onResponse(null, null, null, null);
                }

            }
        });
    }

    public static void requestLogin(final MutableLiveData<Throwable> appException,
                                    final String userName, String password,
                                    final Boolean save,
                                    final Long language,
                                    final ResponseResult result) {
        String secret = Credentials.basic(AppUtils.CLIENT_ID, AppUtils.CLIENT_SECRET);
        okhttp3.FormBody.Builder builder = new okhttp3.FormBody.Builder();
        builder.add("grant_type", "password");
        builder.add("username", userName.trim());
        builder.add("password", password);
        Request request = (new okhttp3.Request.Builder())
                .url(AppUtils.BASE_URL_OAUTH + "/oauth/token")
                .addHeader("origin", "abc")
                .addHeader(authorization, secret)
                .addHeader("Accept-Encoding", "identity")
                .addHeader("Accept", "application/json, text/plain, */*")
                .addHeader("Content-type", "application/x-www-form-urlencoded")
                .addHeader("Accept-Language", language.toString())
                .post(builder.build()).build();
        OkHttpClient.Builder builder1 = new OkHttpClient.Builder();
        builder1.build().newCall(request).enqueue(new Callback() {
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                appException.postValue(e);
            }

            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    ResponseBody body = response.body();
                    if (body != null) {
                        String token = body.string();
                        Type type = (new TypeToken<Map<String, String>>() {
                        }).getType();
                        Gson gson = new Gson();
                        Map<String, String> myMap = gson.fromJson(token, type);
                        HttpHelper.tokenLogin = myMap.get("token_type") + " " + myMap.get("access_token");
                        RetrofitClient.USER_NAME = userName;
                        if (Boolean.TRUE.equals(save)) {
                            PreferencesHelper preferencesHelper = AppUtils.getInstance().getPreferencesHelper();
                            preferencesHelper.setRefreshToken(myMap.get("refresh_token"));
                            preferencesHelper.setToken(HttpHelper.tokenLogin);
                            preferencesHelper.setLanguage(language);
                        }
                        HttpHelper.init(HttpHelper.tokenLogin);
                        result.onResponse(null, null, token, null);
                    }
                } else {
                    result.onResponse(null, null, null, null);
                }
            }
        });
    }
}
