package com.next_solutions.logistics.features.repacking_planning.cancel_repacking

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.features.repacking_planning.claim_repacking.ClaimRepackingNewFragment
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.Constants.SCROLL_TO_POSITION
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

class CancelRepackingPlanningFragment : BaseRecycleViewFragment() {
    private lateinit var repackingViewModel: CancelRepackingPlanningViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        repackingViewModel = viewModel as CancelRepackingPlanningViewModel
        getData()
        repackingViewModel.init()
        return binding.root
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        if (action == SCROLL_TO_POSITION) {
            if (repackingViewModel.lastNotifyPosition != -1) {
                baseAdapter!!.notifyItemChanged(repackingViewModel.lastNotifyPosition)
            }
            baseAdapter!!.notifyItemChanged(repackingViewModel.notifyPosition.get()!!)
            layoutManager.scrollToPosition(repackingViewModel.notifyPosition.get()!!)
        }
    }

    private fun getData() {
        val repacking = activity?.intent?.getSerializableExtra(Constants.MODEL)
        repackingViewModel.repackingPlanning = repacking as RepackingPlanning
    }

    override fun action(view: View, baseViewModel: BaseViewModel<*>?) {
        if (view.id == R.id.createClaim) {
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(Constants.FRAGMENT, ClaimRepackingNewFragment::class.java)
            bundle.putSerializable(Constants.MODEL, repackingViewModel.repackingPlanning)
            intent.putExtras(bundle)
            startActivity(intent)
        }
        if (view.id == R.id.btnCamera) {
            val intent = Intent(activity, QRCodeScannerActivity::class.java)
            startActivityForResult(intent, Constants.REQUEST_CODE_SCAN)
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_cancel_repacking_planning_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return CancelRepackingPlanningViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_cancel_repacking_planning
    }

    override fun getRecycleResId(): Int {
        return R.id.repacking_planning_details
    }
}