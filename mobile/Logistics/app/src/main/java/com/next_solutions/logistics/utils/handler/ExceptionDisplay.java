package com.next_solutions.logistics.utils.handler;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.next_solutions.logistics.R;
import com.next_solutions.logistics.databinding.ExceptionDisplayBinding;
import com.next_solutions.logistics.features.login.LoginActivity;
import com.next_solutions.logistics.utils.DialogCallBack;

import static com.next_solutions.logistics.utils.Constants.ERROR_PROCESS;

public class ExceptionDisplay extends AppCompatActivity implements DialogCallBack {
    private ExceptionViewModel exceptionViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exception_display);
        exceptionViewModel = new ExceptionViewModel(getApplication());
        ExceptionDisplayBinding binding = DataBindingUtil.setContentView(this, R.layout.exception_display);
        binding.setViewModel(exceptionViewModel);
        getErrorMessage();
    }

    private void getErrorMessage() {
        if (getIntent().getExtras() != null) {
            exceptionViewModel.errorMessage = getIntent().getExtras().getString(ERROR_PROCESS);
        }
    }

    @Override
    public void onBackPressed() {
        intentData();
    }

    public void intentData() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
    }

    @Override
    public void callBack(Object object) {
        onBackPressed();
    }

    @Override
    public void onCancel() {

    }
}

