package com.next_solutions.logistics.models;


import androidx.annotation.StringRes;

import com.next_solutions.base.BaseModel;

public class AppUser extends BaseModel {
    private Integer id;
    private String username;
    private String firstName;
    private String lastName;
    private String name;
    private Integer loginStatus;
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLoginStatus() {
        return loginStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLoginStatus(@StringRes Integer loginStatus) {
        this.loginStatus = loginStatus;
    }
}
