package com.next_solutions.logistics.features.inventory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModelProvider;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseDialogFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.BaseViewModelFactory;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.models.ImportStatementDetail;
import com.next_solutions.logistics.models.Store;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;

import java.util.List;

import static com.next_solutions.logistics.utils.Constants.SCAN_CODE;

public class DetailInventoryDialog extends BaseDialogFragment {
    private ImportStatementDetail importStatementDetail;
    private ObservableField<List<ImportStatementDetail>> importStatementDetails;
    private Store store;
    private BaseAdapter baseAdapter;
    private final int scanCode = 1;
    private DetailInventoryDialogViewModel dialogViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        dialogViewModel = (DetailInventoryDialogViewModel) viewModel;
        InventoryViewModel inventoryViewModel = new ViewModelProvider(getBaseActivity(), BaseViewModelFactory.getInstance(getBaseActivity()
                .getApplication(), InventoryViewModel.class))
                .get(InventoryViewModel.class);
        dialogViewModel.setInventoryViewModel(inventoryViewModel);
        dialogViewModel.init();
        baseAdapter = new BaseAdapter(R.layout.item_detail_pallet_inventory, viewModel, (view, baseModel) -> {
        }, getBaseActivity());
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        String tag = (String) view.getTag();
        if (SCAN_CODE.equals(tag)) {
            Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
            startActivityForResult(qrScan, scanCode);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == scanCode && resultCode == Activity.RESULT_OK) {
            String result = data.getStringExtra(Constants.RESULT_SCAN);
            dialogViewModel.getTextSearch().set(result);
            dialogViewModel.search();
        }
        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_detail_pallet_inventory;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DetailInventoryDialogViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rc_detail_pallets;
    }

    public ImportStatementDetail getImportStatementDetail() {
        return importStatementDetail;
    }

    public void setImportStatementDetail(ImportStatementDetail importStatementDetail) {
        this.importStatementDetail = importStatementDetail;
    }

    public ObservableField<List<ImportStatementDetail>> getImportStatementDetails() {
        return importStatementDetails;
    }

    public void setImportStatementDetails(ObservableField<List<ImportStatementDetail>> importStatementDetails) {
        this.importStatementDetails = importStatementDetails;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public BaseAdapter getBaseAdapter() {
        return baseAdapter;
    }

    public void setBaseAdapter(BaseAdapter baseAdapter) {
        this.baseAdapter = baseAdapter;
    }

    public int getScanCode() {
        return scanCode;
    }
}
