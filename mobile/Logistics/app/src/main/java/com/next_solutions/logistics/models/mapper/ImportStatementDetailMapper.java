package com.next_solutions.logistics.models.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ImportStatementDetailMapper {
    ImportStatementDetailMapper INSTANCE = Mappers.getMapper( ImportStatementDetailMapper.class );
}
