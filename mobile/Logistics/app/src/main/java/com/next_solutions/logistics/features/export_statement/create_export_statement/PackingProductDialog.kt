package com.next_solutions.logistics.features.export_statement.create_export_statement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseAdapter
import com.next_solutions.base.BaseDialogFragment
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.utils.DialogCallBack

class PackingProductDialog : BaseDialogFragment() {

    private lateinit var dialogViewModel: PackingProductDialogViewModel
    var exceptId: ArrayList<Long> = arrayListOf()
    var fromStoreId: Long = 0
    var exportStatementId: Long? = null
    lateinit var dialogCallBack: DialogCallBack
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setUpViewModel()
        setUpRecycleView()
        dialogViewModel.search()
        return binding.root
    }


    private fun setUpRecycleView() {
        val baseAdapter = BaseAdapter(R.layout.dialog_packing_product_item, viewModel, OwnerView { _, obj ->
            dialogCallBack.callBack(obj)
            dismiss()
        }, baseActivity)
        recyclerView.adapter = baseAdapter
    }

    private fun setUpViewModel() {
        dialogViewModel = viewModel as PackingProductDialogViewModel
        dialogViewModel.init(exceptId, fromStoreId, exportStatementId)
    }


    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return PackingProductDialogViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.dialog_packing_product
    }

    override fun getRecycleResId(): Int {
        return R.id.list_packing_product
    }

}