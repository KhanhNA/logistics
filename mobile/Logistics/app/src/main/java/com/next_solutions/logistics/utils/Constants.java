package com.next_solutions.logistics.utils;

public class Constants {

    public static final String CREATE_SUCCESS = "CREATE_SUCCESS";
    public static final String CREATE_ERROR = "CREATE_ERROR";
    public static final String PRODUCT_INFO_ERROR = "PRODUCT_INFO_ERROR";
    public static final String STEP_PALLET_ERROR = "STEP_PALLET_ERROR";
    public static final String ERROR_INVALID_INPUT = "ERROR_INVALID_INPUT";
    public static final String NO_PALLET_FOUND = "NO_PALLET_FOUND";
    public static final String ERROR_STEP_PALLET = "ERROR_STEP_PALLET";
    public static final String NOTIFY_CHANGE = "NOTIFY_CHANGE";
    public static final String PALLET = "PALLET";
    public static final String FRAGMENT = "FRAGMENT";
    public static final String RESET_DATA = "RESET_DATA";
    public static final String RESULT_SCAN = "RESULT_SCAN";
    public static final String ERROR_PROCESS = "ERROR_PROCESS";
    public static final String MODEL = "MODEL";
    public static final String MODEL_PO = "MODEL_PO";
    public static final String MODEL_IMPORT_PO = "MODEL_IMPORT_PO";
    public static final String MODEL_REPACKING_PLANNING = "MODEL_REPACKING_PLANNING";
    public static final String MODEL_EXPORT_STATEMENT = "MODEL_EXPORT_STATEMENT";
    public static final String MODEL_IMPORT_STATEMENT = "MODEL_IMPORT_STATEMENT";
    public static final String INVISIBLE_REVIEW_CLAIM = "INVISIBLE_REVIEW_CLAIM";
    public static final String FROM_DETAIL_FRAGMENT = "FROM_DETAIL_FRAGMENT";
    public static final String PO = "PO";
    public static final String STORE = "STORE";
    public static final String SUCCESS = "SUCCESS";
    public static final String UPLOAD_IMAGE_SUCCESS = "UPLOAD_IMAGE_SUCCESS";
    public static final String PRODUCT_IS_EDITING = "PRODUCT_IS_EDITING";
    public static final String EMPTY_STRING = "";
    public static final String ZERO = "0";
    public static final String SHOW_DIALOG = "SHOW_DIALOG";
    public static final String NOTIFY_CHANGE_ITEM = "NOTIFY_CHANGE_ITEM";
    public static final String NOTIFY_CHANGE_ITEM_ADD = "NOTIFY_CHANGE_ITEM_ADD";
    public static final String CANCEL = "CANCEL";
    public static final String EVENT = "EVENT";
    public static final String LIST_RESULT = "LIST_RESULT";
    public static final String NOTIFY_CHANGE_ITEM_REMOVE = "NOTIFY_CHANGE_ITEM_REMOVE";
    public static final String SCROLL_TO_POSITION = "SCROLL_TO_POSITION";
    public static final String FROM_DATE = "FROM_DATE";
    public static final String TO_DATE = "TO_DATE";
    public static final String YELLOW = "mat-row-yellow";
    public static final String ORANGE = "mat-row-orange";
    public static final String RED = "mat-row-red";
    public static final String SCAN_CODE = "SCAN_CODE";
    public static final int TIME_DELAY = 3000;
    public static final int TIME_AFTER_DELAY = 2000;
    public static final int CODE_SUCCESS = 200;
    public static final int REQUEST_CODE_SCAN = 1111;
    public static final String NOT_MATCH_QUANTITY = "NOT_MATCH_QUANTITY";
    public static final String RELOAD_IMAGE = "RELOAD_IMAGE";
    public static final String POSITION = "POSITION";
    public static final int CLAIM_ACTIVITY_CODE = 4444;
    public static final String CLAIM_DETAILS = "CLAIM_DETAILS";
    public static final String CLAIM_ACTIVITY = "CLAIM_ACTIVITY";
    public static final String CREATE_CLAIM_SUCCESS = "CREATE_CLAIM_SUCCESS";
    public static final String MISSING_DETAILS = "MISSING_DETAILS";
    public static final String MISSING_DESCRIPTION = "MISSING_DESCRIPTION";
    public static final String CONFIRM = "CONFIRM";
    public static final String UPDATE_SUCCESS = "UPDATE_SUCCESS";
    public static final String UPLOAD_IMAGE_CLAIM_SUCCESS = "UPLOAD_IMAGE_CLAIM_SUCCESS";
    public static final String SEARCH = "SEARCH";

    private Constants() {

    }
}
