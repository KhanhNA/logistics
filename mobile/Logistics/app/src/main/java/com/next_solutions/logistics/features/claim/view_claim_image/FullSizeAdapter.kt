package com.next_solutions.logistics.features.claim.view_claim_image

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.next_solutions.logistics.R
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.models.ImageModel
import com.yanzhenjie.album.Album
import com.yanzhenjie.album.AlbumFile

class FullSizeAdapter(private var context: Context,
                      private val images: Array<ImageModel>) : PagerAdapter() {
    lateinit var inflater: LayoutInflater
    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.full_size_image, null)

        val imageView = view.findViewById<ImageView>(R.id.image)
        val image = images[position]
        if (image.isLocalImage) {
            Album.getAlbumConfig().albumLoader.load(imageView, AlbumFile().apply { path = image.path })
        } else {
            Glide.with(context).load(AppUtils.IMAGE_URL + image.url).into(imageView!!)
        }

        val viewPager = container as ViewPager
        viewPager.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        val viewPager = container as ViewPager
        val view = obj as View
        viewPager.removeView(view)
    }

    override fun getCount(): Int {
        return images.size
    }

}