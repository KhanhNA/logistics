package com.next_solutions.logistics.repository.callback;

public interface DatabaseCallback {
    void onSuccess(int code);
    void onError(int code, Throwable e);
}
