package com.next_solutions.logistics.features.claim.claim_detail

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.databinding.FragmentClaimDetailBinding
import com.next_solutions.logistics.features.claim.dialog_reject_claim.DialogRejectClaim
import com.next_solutions.logistics.features.claim.find_claim.ImageAdapter
import com.next_solutions.logistics.features.claim.view_claim_image.ViewClaimImageActivity
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.helper.FragmentHelper
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.RecycleViewCallBack

class ClaimDetailFragment : BaseRecycleViewFragment() {
    private lateinit var viewBinding: FragmentClaimDetailBinding
    private lateinit var claimDetailViewModel: ClaimDetailViewModel
    private lateinit var imageAdapter: ImageAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        claimDetailViewModel = viewModel as ClaimDetailViewModel
        viewBinding = binding as FragmentClaimDetailBinding
        getData()
        setUpImageRecycleView()
        claimDetailViewModel.init()
        return binding.root
    }

    private fun setUpImageRecycleView() {
        val linearLayout = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        imageAdapter = ImageAdapter(claimDetailViewModel.claimImages, RecycleViewCallBack { _, claimImage ->
            run {
                val intent = Intent(context, ViewClaimImageActivity::class.java)
                val bundle = Bundle()
                val images = claimDetailViewModel.claimImages
                        .asSequence()
                        .map {
                            ImageModel().apply { this.url = it.url }
                        }
                        .toList()
                        .toTypedArray()
                bundle.putSerializable(MODEL, images)
                bundle.putSerializable(POSITION, claimDetailViewModel.claimImages.indexOf(claimImage))
                intent.putExtras(bundle)
                startActivity(intent)
            }
        })
        viewBinding.images.apply {
            adapter = imageAdapter
            layoutManager = linearLayout
        }
    }

    override fun action(view: View?, baseViewModel: BaseViewModel<*>?) {
        when {
            view!!.id == R.id.rejectClaim -> {
                val message = resources.getString(R.string.confirm_reject_claim)
                val dialogReject = DialogRejectClaim(message)
                dialogReject.show((activity as FragmentActivity).supportFragmentManager, EMPTY_STRING)
            }
            claimDetailViewModel.po != null -> {
                startPoActivity()
            }
            claimDetailViewModel.importPo != null -> {
                startImportPoActivity()
            }
            claimDetailViewModel.repackingPlanning != null -> {
                startRepackingPlanningActivity()
            }
            claimDetailViewModel.exportStatement != null -> {
                startExportReleaseActivity()
            }
            claimDetailViewModel.importStatement != null -> {
                startImportReleaseActivity()
            }
            else -> {
            }
        }
    }

    private fun startPoActivity() {
        val po = claimDetailViewModel.po!!
        val intent = Intent(context, CommonActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable(MODEL, po)
        bundle.putSerializable(CLAIM_DETAILS, claimDetailViewModel.claim.claimDetails.toTypedArray())
        bundle.putSerializable(FRAGMENT, FragmentHelper().getFragmentType(po))
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun startImportPoActivity() {
        val importPo = claimDetailViewModel.importPo!!
        val intent = Intent(context, CommonActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable(MODEL, importPo)
        bundle.putSerializable(CLAIM_DETAILS, claimDetailViewModel.claim.claimDetails.toTypedArray())
        bundle.putSerializable(FRAGMENT, FragmentHelper().getFragmentType(importPo))
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun startRepackingPlanningActivity() {
        val repackingPlanning = claimDetailViewModel.repackingPlanning!!
        val intent = Intent(context, CommonActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable(MODEL, repackingPlanning)
        bundle.putSerializable(CLAIM_DETAILS, claimDetailViewModel.claim.claimDetails.toTypedArray())
        bundle.putSerializable(FRAGMENT, FragmentHelper().getFragmentType(repackingPlanning))
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun startExportReleaseActivity() {
        val exportStatement = claimDetailViewModel.exportStatement!!
        val fragmentType = FragmentHelper().getFragmentType(exportStatement)
        if (fragmentType != null) {
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(CLAIM_DETAILS, claimDetailViewModel.claim.claimDetails.toTypedArray())
            bundle.putSerializable(MODEL, exportStatement)
            bundle.putSerializable(FRAGMENT, fragmentType)
            intent.putExtras(bundle)
            startActivity(intent)
        } else {
            showAlertWarning(R.string.export_statement_released)
        }
    }

    private fun startImportReleaseActivity() {
        val importStatement = claimDetailViewModel.importStatement!!
        val intent = Intent(context, CommonActivity::class.java)
        val bundle = Bundle()
        val fragmentType = FragmentHelper().getFragmentType(importStatement)
        if (fragmentType != null) {
            bundle.putSerializable(MODEL, importStatement)
            bundle.putSerializable(FRAGMENT, fragmentType)
            bundle.putSerializable(CLAIM_DETAILS, claimDetailViewModel.claim.claimDetails.toTypedArray())
            intent.putExtras(bundle)
            startActivity(intent)
        } else {
            showAlertWarning(R.string.import_statement_released)
        }

    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            SUCCESS -> {
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
                showAlertSuccess(TIME_AFTER_DELAY, R.string.accept_claim_success, null)
            }
            RELOAD_IMAGE -> {
                imageAdapter.notifyDataSetChanged()
            }
            "REJECT_SUCCESS" -> {
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
                showAlertSuccess(TIME_AFTER_DELAY, R.string.reject_claim_success, null)
            }
        }
    }

    private fun getData() {
        activity?.intent?.let {
            val claim = it.getSerializableExtra(MODEL)
            if (claim != null) {
                claimDetailViewModel.claim = claim as Claim
            }
            getStatementReference()
        }
    }

    private fun getStatementReference() {
        getPoReference()
        getImportPOReference()
        getRepackingPlanningReference()
        getExportStatementReference()
        getImportStatementReference()
    }

    private fun getPoReference() {
        val po = activity?.intent?.getSerializableExtra(MODEL_PO)
        if (po != null) {
            claimDetailViewModel.po = po as Po
        }
    }

    private fun getImportPOReference() {
        val importPo = activity?.intent?.getSerializableExtra(MODEL_IMPORT_PO)
        if (importPo != null) {
            claimDetailViewModel.importPo = importPo as ImportPo
        }
    }

    private fun getRepackingPlanningReference() {
        val repackingPlanning = activity?.intent?.getSerializableExtra(MODEL_REPACKING_PLANNING)
        if (repackingPlanning != null) {
            claimDetailViewModel.repackingPlanning = repackingPlanning as RepackingPlanning
        }
    }

    private fun getExportStatementReference() {
        val exportStatement = activity?.intent?.getSerializableExtra(MODEL_EXPORT_STATEMENT)
        if (exportStatement != null) {
            claimDetailViewModel.exportStatement = exportStatement as ExportStatement
        }
    }

    private fun getImportStatementReference() {
        val importStatement = activity?.intent?.getSerializableExtra(MODEL_IMPORT_STATEMENT)
        if (importStatement != null) {
            claimDetailViewModel.importStatement = importStatement as ImportStatement
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_claim_detail_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return ClaimDetailViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_claim_detail
    }

    override fun getRecycleResId(): Int {
        return R.id.list_detail
    }

}