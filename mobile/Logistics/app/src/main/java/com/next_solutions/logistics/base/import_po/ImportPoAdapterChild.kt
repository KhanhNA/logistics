package com.next_solutions.logistics.base.import_po

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.databinding.FragmentImportPoItem2Binding
import com.next_solutions.logistics.features.import_po.dialog_choose_pallet.ChoosePalletUpdatePalletDialog
import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.HttpRequest
import com.next_solutions.logistics.models.helper.RuleHelper
import com.next_solutions.logistics.repository.api.Method
import com.next_solutions.logistics.repository.api.import_po.ImportPoUrlPath
import com.next_solutions.logistics.utils.DialogCallBack
import com.next_solutions.logistics.utils.RecycleViewCallBack

class ImportPoAdapterChild(private var importPoDetail: ImportPoDetail,
                           private var fragmentManager: FragmentManager,
                           private var viewModel: BaseImportPoViewModel) :
        RecyclerView.Adapter<ImportPoAdapterChild.SplitViewHolder>(), DialogCallBack {
    private var notifyPosition = 0
    private lateinit var chooseDetail: ImportPoDetailPallet
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SplitViewHolder {
        val binding =
                DataBindingUtil.inflate<FragmentImportPoItem2Binding>(LayoutInflater.from(parent.context),
                        R.layout.fragment_import_po_item2, parent, false)
        return SplitViewHolder(binding)
    }

    override fun getItemCount(): Int {
        if (importPoDetail.importPoDetailPallets == null) {
            return 0
        }
        return importPoDetail.importPoDetailPallets.size
    }

    override fun onBindViewHolder(holder: SplitViewHolder, position: Int) {
        val palletDetail = importPoDetail.importPoDetailPallets[position]

        holder.bind(palletDetail, viewModel,
                RecycleViewCallBack { view, obj ->
                    run {
                        if (viewModel.importPo.status != ImportPo.ImportPoStatus.OUT_OF_PALLET.value) {
                            val detail = obj as ImportPoDetailPallet
                            val eventPosition = importPoDetail.importPoDetailPallets.indexOf(detail)
                            when {
                                view!!.id == R.id.choose_pallet -> {
                                    notifyPosition = eventPosition
                                    chooseDetail = detail
                                    showPalletPicker()

                                }
                                view.id == R.id.delete_detail -> {
                                    notifyPosition = eventPosition + 1
                                    removeRow(eventPosition, detail, importPoDetail.importPoDetailPallets.size)
                                }
                                view.id == R.id.add_detail -> {
                                    notifyPosition = eventPosition
                                    addRow(eventPosition)
                                }
                            }
                        }
                    }
                })
    }

    private fun addRow(eventPosition: Int) {
        val addPosition = eventPosition + 1
        val detail = importPoDetail
        val importPoId = importPoDetail.importPo.id
        detail.importPoDetailPallets
                .add(addPosition, ImportPoDetailPallet().apply {
                    quantity = 0
                    pallet = Pallet()
                    importPoDetail = ImportPoDetail().apply {
                        id = detail.id
                        expireDate = detail.expireDate
                        productPacking = ProductPacking().apply { code = detail.productPacking.code }
                        importPo = ImportPo().apply { id = importPoId }
                    }
                    importPo = ImportPo().apply { id = importPoId }
                })
        notifyItemInserted(addPosition)
    }

    private fun removeRow(removePosition: Int, palletDetail: ImportPoDetailPallet, size: Int) {
        if (removePosition == 0 && size == 1) {
            return
        }
        notifyItemRemoved(removePosition)
        importPoDetail.importPoDetailPallets.removeAt(removePosition)
        viewModel.reCalculator(palletDetail.quantity * -1, palletDetail)
    }

    private fun showPalletPicker() {
        if (RuleHelper().hasRule(HttpRequest(Method.POST.value, ImportPoUrlPath.UPDATE_PALLET))) {
            val dialog = ChoosePalletUpdatePalletDialog()
            dialog.dialogCallBack = this
            dialog.pallets = viewModel.storePallet
            dialog.show(fragmentManager, "")
        }
    }

    class SplitViewHolder(var binding: FragmentImportPoItem2Binding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(palletDetail: ImportPoDetailPallet, viewModel: BaseImportPoViewModel,
                 adapterListener: RecycleViewCallBack) {
            binding.viewHolder = palletDetail
            binding.viewModel = viewModel
            binding.listener = adapterListener
        }
    }

    override fun onCancel() {

    }

    override fun callBack(obj: Any?) {
        val pallet = obj as Pallet
        chooseDetail.pallet = Pallet().apply {
            id = pallet.id
            displayName = pallet.displayName
            code = pallet.code
            step = pallet.step
        }
        notifyItemChanged(notifyPosition)
    }


}