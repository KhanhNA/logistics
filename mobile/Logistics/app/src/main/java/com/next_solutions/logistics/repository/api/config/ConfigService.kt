package com.next_solutions.logistics.repository.api.config

import com.next_solutions.logistics.repository.api.config.ConfigUrlPath.Companion.CONFIGS
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ConfigService {

    @GET(CONFIGS)
    fun getConfig(@Query("code") code: String): Single<Response<ResponseBody>>
}