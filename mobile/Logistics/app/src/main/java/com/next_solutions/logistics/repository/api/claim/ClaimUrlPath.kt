package com.next_solutions.logistics.repository.api.claim

interface ClaimUrlPath {
    companion object {
        const val CREATE_CLAIM = "/claims"
        const val FIND_CLAIM = "/claims"
        const val FIND_CLAIM_BY_ID = "/claims/{id}"
        const val FIND_BY_REFERENCE = "/claims/by-reference"
        const val ACCEPT_CLAIM = "/claims/accept/{id}"
        const val REJECT_CLAIM = "/claims/reject/{id}"
        const val UPDATE_IMAGE = "/claims/{id}/update-image"
        const val UPDATE_CLAIM = "/claims/{id}"
    }
}