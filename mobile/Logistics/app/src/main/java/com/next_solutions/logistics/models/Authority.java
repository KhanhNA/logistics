package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public
class Authority {
    private String authority;

    public String getMethod() {
        return authority.split("/")[0];
    }

    public String getUrl() {
        return authority.substring(getMethod().length());
    }

    public String getAuthority() {
        return authority.toLowerCase();
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
