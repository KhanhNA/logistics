package com.next_solutions.logistics.features.repacking_planning.new_repacking

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.CommonActivity
import com.next_solutions.base.listener.OwnerView
import com.next_solutions.logistics.R
import com.next_solutions.logistics.base.fragment.BaseRecycleViewFragment
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.features.repacking_planning.claim_repacking.ClaimRepackingNewFragment
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

@Suppress("UNCHECKED_CAST")
class NewRepackingPlanningFragment : BaseRecycleViewFragment() {
    private lateinit var repackingViewModel: NewRepackingPlanningViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        repackingViewModel = viewModel as NewRepackingPlanningViewModel
        getData()
        repackingViewModel.init()
        return binding.root
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        if (action == SCROLL_TO_POSITION) {
            if (repackingViewModel.lastNotifyPosition != -1) {
                baseAdapter!!.notifyItemChanged(repackingViewModel.lastNotifyPosition)
            }
            baseAdapter!!.notifyItemChanged(repackingViewModel.notifyPosition.get()!!)
            layoutManager.scrollToPositionWithOffset(repackingViewModel.notifyPosition.get()!!, 20)
        }
    }

    private fun getData() {
        if (activity != null) {
            if (activity!!.intent.hasExtra(MODEL)) {
                val repacking = activity!!.intent.getSerializableExtra(MODEL) as RepackingPlanning
                repackingViewModel.repackingPlanning = repacking
            }
            if (activity!!.intent.hasExtra(CLAIM_DETAILS)) {
                val claimDetails = activity!!.intent.getSerializableExtra(CLAIM_DETAILS) as Array<Any>
                repackingViewModel.claimDetails = arrayListOf()
                for (obj in claimDetails) {
                    repackingViewModel.claimDetails!!.add(obj as ClaimDetail)
                }
                repackingViewModel.hideButtonViewClaim()
            }
        }
    }

    override fun action(view: View, baseViewModel: BaseViewModel<*>?) {
        when (view.id) {
            R.id.createClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable(FRAGMENT, ClaimRepackingNewFragment::class.java)
                bundle.putSerializable(MODEL, repackingViewModel.repackingPlanning)
                intent.putExtras(bundle)
                startActivityForResult(intent, CLAIM_ACTIVITY_CODE)
            }
            R.id.btnCamera -> {
                val intent = Intent(activity, QRCodeScannerActivity::class.java)
                startActivityForResult(intent, REQUEST_CODE_SCAN)
            }
            R.id.reviewClaim -> {
                val intent = Intent(context, CommonActivity::class.java)
                val bundle = Bundle()
                val claim = repackingViewModel.claimDetails!![0].claim
                claim.claimDetails = repackingViewModel.claimDetails
                bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
                bundle.putSerializable(MODEL, claim)
                bundle.putSerializable(MODEL_REPACKING_PLANNING, repackingViewModel.repackingPlanning)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_SCAN) {
                repackingViewModel.searchByCode(data!!.getStringExtra(RESULT_SCAN))
            }
            if (requestCode == CLAIM_ACTIVITY_CODE && data!!.hasExtra(CLAIM_ACTIVITY)) {
                val result = data.getStringExtra(CLAIM_ACTIVITY)!!
                if (result == CREATE_CLAIM_SUCCESS) {
                    baseActivity.onBackPressed()
                }
            }
        }
    }

    override fun getLayoutItemRes(): Int {
        return R.layout.fragment_new_repacking_planning_item
    }

    override fun getListenerAdapter(): OwnerView? {
        return OwnerView { _, _ -> }
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return NewRepackingPlanningViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_new_repacking_planning
    }

    override fun getRecycleResId(): Int {
        return R.id.repacking_planning_details
    }
}