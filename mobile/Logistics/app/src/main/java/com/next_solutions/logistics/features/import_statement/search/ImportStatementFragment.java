package com.next_solutions.logistics.features.import_statement.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.next_solutions.base.BaseAdapter;
import com.next_solutions.base.BaseFragment;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.CommonActivity;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.models.ImportStatement;
import com.next_solutions.logistics.models.dto.HttpRequest;
import com.next_solutions.logistics.models.helper.FragmentHelper;
import com.next_solutions.logistics.models.helper.RuleHelper;
import com.next_solutions.logistics.repository.api.Method;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity;

import static com.next_solutions.logistics.repository.api.import_statement.ImportStatementUrlPath.FIND_IMPORT_STATEMENT_BY_ID;
import static com.next_solutions.logistics.utils.Constants.REQUEST_CODE_SCAN;
import static com.next_solutions.logistics.utils.Constants.SCAN_CODE;

public class ImportStatementFragment extends BaseFragment {
    private ImportStatementViewModel mViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mViewModel = (ImportStatementViewModel) viewModel;
        BaseAdapter baseAdapter = new BaseAdapter(R.layout.fragment_import_statement_item, viewModel, (view, baseModel) -> {
            if (new RuleHelper().hasRule(new HttpRequest(Method.GET.getValue(), FIND_IMPORT_STATEMENT_BY_ID))) {
                ImportStatement importStatement = (ImportStatement) baseModel;
                Intent intent = new Intent(getContext(), CommonActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.MODEL, importStatement);
                intent.putExtras(bundle);
                intent.putExtra(Constants.FRAGMENT, new FragmentHelper().getFragmentType(importStatement));
                startActivity(intent);
            }
        }, getBaseActivity());
        mViewModel.init();
        recyclerView.setAdapter(baseAdapter);
        return binding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
        mViewModel.search();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (SCAN_CODE.equals(view.getTag())) {
            Intent qrScan = new Intent(getContext(), QRCodeScannerActivity.class);
            startActivityForResult(qrScan, REQUEST_CODE_SCAN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            String result = data.getStringExtra(Constants.RESULT_SCAN);
            mViewModel.setStatementCode(result);
        }
        closeProcess();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_import_statement;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ImportStatementViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.lstImportStatementRequest;
    }
}
