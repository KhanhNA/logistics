package com.next_solutions.logistics.features.export_statement.claim_export_statement

import android.app.Application
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.models.ExportStatementDetail

@Suppress("UNCHECKED_CAST")
class DialogChooseExportStatementDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    private lateinit var exportStatement: ExportStatement
    private lateinit var claimExportStatementViewModel: ClaimExportStatementViewModel
    var searchComponent = SearchComponent()
    fun init(claimExportStatementViewModel: ClaimExportStatementViewModel) {
        this.claimExportStatementViewModel = claimExportStatementViewModel
        this.exportStatement = claimExportStatementViewModel.exportStatement
        searchComponent.resetData()
        search()
    }

    override fun search() {
        val details = claimExportStatementViewModel.baseModelsE as List<ClaimDetail>
        val codeSearch = searchComponent.getCodeSearch()
        setData(claimExportStatementViewModel.exportStatement.exportStatementDetails
                .asSequence()
                .filter {
                    contain(details, it)
                }
                .filter {
                    it.productPacking.product.name.contains(codeSearch, true)
                            || (it.productPacking.code.contains(codeSearch, true))
                }
                .toList())
    }

    private fun contain(details: List<ClaimDetail>, detail: ExportStatementDetail): Boolean {
        return details.asSequence().find { claimDetail ->
            claimDetail.productPacking.id == detail.productPacking.id
                    && claimDetail.expireDate == detail.expireDate
        } == null
    }

}