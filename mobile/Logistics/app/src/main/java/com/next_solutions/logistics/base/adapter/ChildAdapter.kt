package com.next_solutions.logistics.base.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.BR
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.logistics.utils.RecycleViewCallBack

class ChildAdapter(private var data: List<BaseModel>?,
                   private var childLayoutId: Int,
                   private var viewModel: BaseViewModel<BaseModel>,
                   private var childCallBack: RecycleViewCallBack) : RecyclerView.Adapter<ChildAdapter.ChildViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context),
                childLayoutId, parent, false)
        return ChildViewHolder(binding)
    }

    override fun getItemCount(): Int {
        if (data == null) {
            return 0
        }
        return data!!.size
    }

    override fun onBindViewHolder(holder: ChildViewHolder, position: Int) {
        if (data != null) {
            holder.bind(data!![position], viewModel, childCallBack)
        }
    }

    class ChildViewHolder(var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(detail: BaseModel, baseViewModel: BaseViewModel<BaseModel>, callBack: RecycleViewCallBack) {
            binding.setVariable(BR.viewHolder, detail)
            binding.setVariable(BR.listener, callBack)
            binding.setVariable(BR.viewModel, baseViewModel)
        }
    }

}