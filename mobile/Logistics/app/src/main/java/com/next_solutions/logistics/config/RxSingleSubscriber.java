package com.next_solutions.logistics.config;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.next_solutions.base.ErrorRespone;
import com.next_solutions.base.exceptionHandle.AppException;
import com.next_solutions.logistics.R;
import com.next_solutions.logistics.utils.Constants;
import com.next_solutions.logistics.utils.ValidateUtils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Properties;

import io.reactivex.observers.DisposableSingleObserver;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.next_solutions.logistics.utils.Constants.EMPTY_STRING;

/**
 * @author NMQ
 */
@SuppressWarnings("unchecked")
public abstract class RxSingleSubscriber<T> extends DisposableSingleObserver<Response<ResponseBody>> {
    private Class<?> listClass;
    private JavaType pageableClass;
    private Class type;
    private MutableLiveData<Throwable> appException;

    protected RxSingleSubscriber(MutableLiveData<Throwable> appException) {
        super();
        this.appException = appException;
    }

    protected RxSingleSubscriber(MutableLiveData<Throwable> appException, Class<?> pClass, Class<?> cClass, Class<?> type) {
        super();
        this.appException = appException;
        ObjectMapper objectMapper = new ObjectMapper();
        if (pClass != null && cClass != null) {
            this.pageableClass = objectMapper.getTypeFactory().constructParametricType(pClass, cClass);
        } else {
            this.pageableClass = null;
        }
        this.listClass = null;
        this.type = type;
    }

    protected RxSingleSubscriber(MutableLiveData<Throwable> appException, Class<?> elem, Class<?> type) {
        super();
        this.listClass = elem;
        this.appException = appException;
        this.type = type;
        this.pageableClass = null;
    }

    @Override
    public void onSuccess(Response<ResponseBody> response) {
        Object dataCallback;
        if (response.isSuccessful()) {
            try {
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    String s = responseBody.string();
                    if (pageableClass == null && listClass == null && type == null) {
                        getDataSuccess((T) new ResponseStatus(Constants.CODE_SUCCESS));
                        return;
                    }
                    if (listClass == null && pageableClass == null) {
                        dataCallback = new Gson().fromJson(s, Properties.class);
                    } else {
                        ObjectMapper objectMapper = new ObjectMapper()
                                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                                .configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
                        if (type == null) {
                            if (pageableClass != null) {
                                dataCallback = objectMapper.readValue(s, pageableClass);
                            } else {
                                dataCallback = objectMapper.readValue(s, listClass);
                            }
                        } else {
                            dataCallback = objectMapper.readValue(s, objectMapper.getTypeFactory().constructCollectionType(type, listClass));
                        }
                    }
                    getDataSuccess((T) dataCallback);
                }
            } catch (Exception e) {
                sendErrorDefault(e.getMessage() + " " + this.getClass().getName());
            }
        } else {
            try {
                String s = response.errorBody().string();
                ObjectMapper objectMapper = new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
                ErrorRespone errorRespone;
                if (!ValidateUtils.isNullOrEmpty(s)) {
                    errorRespone = objectMapper.readValue(s, ErrorRespone.class);
                    if (this.appException != null) {
                        if (ValidateUtils.isNullOrEmpty(errorRespone.getMessage())) {
                            errorRespone.setMessage(AppUtils.getInstance().getResources().getString(R.string.error_process));
                        }
                        this.appException.postValue(new AppException(R.string.error_process, errorRespone.getMessage()));
                    }
                    onFailure(errorRespone);
                } else {
                    sendErrorDefault(EMPTY_STRING);
                }
            } catch (IOException e) {
                Log.e(Constants.ERROR_PROCESS, e.getMessage() + " " + this.getClass().getName());
                sendErrorDefault(e.getMessage());
            }
        }
    }

    private void sendErrorDefault(String mess) {
        ErrorRespone errorRespone = new ErrorRespone();
        if (mess == null || mess.equals(EMPTY_STRING)) {
            errorRespone.setMessage(AppUtils.getInstance().getResources().getString(R.string.error_process));
        } else {
            errorRespone.setMessage(mess);
        }
        if (this.appException != null) {
            this.appException.postValue(new AppException(R.string.error_process, errorRespone.getMessage()));
        }
        Log.e(Constants.ERROR_PROCESS, mess + " " + this.getClass().getName());
        onFailure(errorRespone);
    }

    @Override
    public void onError(Throwable e) {
        ErrorRespone errorRespone = new ErrorRespone();
        errorRespone.setMessage(AppUtils.getInstance().getResources().getString(R.string.error_process));
        if (this.appException != null && e != null) {
            if (e instanceof SocketTimeoutException) {
                this.appException.postValue(new AppException(R.string.network_error, e.getMessage()));
            } else {
                this.appException.postValue(new AppException(R.string.network_error, e.getMessage()));
            }
        }
        String message = e == null ? "" : e.getMessage();
        Log.e(Constants.ERROR_PROCESS, message + " " + this.getClass().getName());
        onFailure(errorRespone);
    }

    public abstract void getDataSuccess(T t);

    public abstract void onFailure(ErrorRespone errorRespone);
}
