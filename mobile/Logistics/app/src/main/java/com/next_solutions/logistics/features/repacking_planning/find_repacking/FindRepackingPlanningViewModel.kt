package com.next_solutions.logistics.features.repacking_planning.find_repacking

import android.app.Application
import android.util.Log
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.base.Pageable
import com.next_solutions.logistics.component.date.ChooseDateComponent
import com.next_solutions.logistics.component.distributor.DistributorComponent
import com.next_solutions.logistics.component.repacking_planning_status.RepackingPlanningStatusComponent
import com.next_solutions.logistics.component.search.SearchComponent
import com.next_solutions.logistics.component.store.FromFCComponent
import com.next_solutions.logistics.config.AppUtils
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants
import com.next_solutions.logistics.utils.DateUtils

class FindRepackingPlanningViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    var searchComponent = SearchComponent()
    var fromFCComponent = FromFCComponent(this)
//    var distributorComponent = DistributorComponent(this, true)
    var currentStatusComponent = RepackingPlanningStatusComponent()
    var statusComponent = RepackingPlanningStatusComponent()
    var chooseDate = ChooseDateComponent()
    override fun init() {
        clearData()
    }

    private fun clearData() {
        clearDataRecycleView()
//        distributorComponent.resetData()
        fromFCComponent.resetData()
        chooseDate.resetData()
        currentStatusComponent.resetData()
        statusComponent.resetData()
        searchComponent.resetData()
    }

    override fun search() {
        val fDate = chooseDate.fromDate.get()
        val tDate = chooseDate.toDate.get()
        val page = currentPageNumber
        if (fDate != null && tDate != null && !fromFCComponent.stores.isEmpty()
                ) {
            val from = DateUtils.convertDateToString(fDate, AppUtils.DATE_FORMAT)
            val to = DateUtils.convertDateToString(tDate, AppUtils.DATE_FORMAT)
            val currentStatementOrder = currentStatusComponent.selectedStatusPosition
            val status = statusComponent.selectedStatusPosition
            val storeId = fromFCComponent.getSelectedStore()!!.id
            val distributorId = null //distributorComponent.getSelectedDistributor()!!.id
            val searchCode = searchComponent.code.get()
            callApi(Service.callRepackingPlanningService()
                    .findRepackingPlanning(searchCode, status, from, to, page,
                            AppUtils.PAGE_SIZE, storeId, currentStatementOrder, distributorId)
                    .compose(Service.getRxSingleSchedulers().applySchedulers())
                    .subscribeWith(object : RxSingleSubscriber<Pageable<RepackingPlanning>>(appException, Pageable::class.java, RepackingPlanning::class.java, null) {
                        override fun getDataSuccess(pageable: Pageable<RepackingPlanning>) {
                            setPage(pageable)
                            setData(pageable.content as List<BaseModel>)
                        }

                        override fun onFailure(errorRespone: ErrorRespone) {
                            Log.e(Constants.ERROR_PROCESS, errorRespone.error)
                        }
                    }))
        }
    }

    fun setDate(isFromDate: Boolean, year: Int, month: Int, day: Int) {
        val date = DateUtils.convertDayMonthYearToDate(day, month, year)
        if (isFromDate) {
            chooseDate.fromDate.set(date)
        } else {
            chooseDate.toDate.set(date)
        }
    }

    fun searchByCode(code: String) {
        searchComponent.setData(code)
        search()
    }
}