package com.next_solutions.logistics.repository.api.user;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import static com.next_solutions.logistics.repository.api.user.UserServiceUrlPath.CHANGE_PASSWORD;
import static com.next_solutions.logistics.repository.api.user.UserServiceUrlPath.USER_ME;

public interface UserService {
    //  ========================USER========================
    @GET(USER_ME)
    Single<Response<ResponseBody>> getUser();

    @POST(CHANGE_PASSWORD)
    Single<Response<ResponseBody>> changePassword(@Query("newConfirmPassword") String newConfirmPassword,
                                                  @Query("newPassword") String newPassword,
                                                  @Query("oldPassword") String oldPassword);
}
