package com.next_solutions.logistics.base.claim

import android.app.Application
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.R
import com.next_solutions.logistics.config.ResponseStatus
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.ClaimImage
import com.next_solutions.logistics.models.Image
import com.next_solutions.logistics.models.dto.ClaimObject
import com.next_solutions.logistics.models.helper.ClaimHelper
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.ValidateUtils
import com.yanzhenjie.album.AlbumFile
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


@Suppress("UNCHECKED_CAST")
abstract class BaseClaimViewModel(application: Application) : BaseViewModel<BaseModel>(application) {
    var images = arrayListOf<Image>()
    val claimHelper = ClaimHelper()
    var descriptionObservable = ObservableField(EMPTY_STRING)
    var amenableObservable = ObservableField(EMPTY_STRING)
    var buttonEnable = ObservableField(true)
    var lastNotifyPosition: Int? = null

    abstract fun prepareClaim(): Claim

    fun updateImages(result: java.util.ArrayList<AlbumFile>) {
        removeImageExit(result)
        addNewImage(result)
    }

    private fun removeImageExit(result: java.util.ArrayList<AlbumFile>) {
        val removeImages = images.asSequence()
                .filter { image ->
                    image.albumFile != null && !result.asSequence()
                            .map { it.path }
                            .contains(image.albumFile.path)
                }.toList()
        images.removeAll(removeImages)
    }

    private fun addNewImage(result: java.util.ArrayList<AlbumFile>) {
        val addImages = result.asSequence()
                .filter { albumFile ->
                    !images.asSequence()
                            .filter { it.albumFile != null }
                            .map { it.albumFile }
                            .map { it.path }
                            .contains(albumFile.path)
                }.toList()

        addImages.forEach { albumFile: AlbumFile? ->
            run {
                albumFile?.let {
                    images.add(Image().apply {
                        this.albumFile = it
                        val lastIndex = it.path.lastIndexOf('/')
                        val url = it.path.substring(lastIndex + 1)
                        claimImage = ClaimImage().apply {
                            this.url = url
                        }
                    })
                }
            }
        }
    }

    open fun removeImage(image: Image) {
        images.remove(image)
    }

    fun delete(claimDetail: ClaimDetail) {
        val details = baseModelsE as ArrayList<ClaimDetail>
        val removePosition = details.indexOf(claimDetail)
        if (removePosition != -1) {
            notifyPosition.set(removePosition)
            details.removeAt(removePosition)
            sendBackAction(NOTIFY_CHANGE_ITEM_REMOVE)
        }
    }

    open fun validate(): Boolean {
//        if (ValidateUtils.isNullOrEmpty(amenableObservable.get())) {
//            sendBackAction("MISSING_AMENABLE")
//            return false
//        }
        val data = baseModelsE as ArrayList<ClaimDetail>
        if (data.isEmpty()) {
            sendBackAction(MISSING_DETAILS)
            return false
        }
        for ((position, detail) in data.withIndex()) {
            if (ValidateUtils.isNullOrEmpty(detail.quantity) || (!ValidateUtils.isNullOrEmpty(detail.totalQuantity) && detail.quantity > detail.totalQuantity)) {
                notifyPosition.set(position)
                changeViewAndSaveLastPosition()
                return false
            }
        }
        val addImage = images.asSequence().map { it.claimImage }.filter { it.id == null }.toList()
        val missingDescriptionImage = addImage.find { ValidateUtils.isNullOrEmpty(it.description) }
        missingDescriptionImage?.let {
            sendBackAction(MISSING_DESCRIPTION)
            return false
        }
        return true
    }

    private fun changeViewAndSaveLastPosition() {
        lastNotifyPosition?.let {
            baseModelsE[it].checked = null
        }
        baseModelsE[notifyPosition.get()!!].checked = false
        sendBackAction(PRODUCT_INFO_ERROR)
        lastNotifyPosition = notifyPosition.get()
    }

    // start create claim
    fun onCreate() {
        if (validate()) {
            val claim = prepareClaim()
            buttonEnable.set(false)
            createClaim(claim)
        }
    }

    private fun createClaim(claim: Claim) {
        callApi(Service.callClaimService().createClaim(arrayListOf(claim))
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<Claim>>(appException, Claim::class.java, List::class.java) {
                    override fun getDataSuccess(claims: List<Claim>) {
                        sendBackAction(SUCCESS)
                        clearData()
                        if (images.isNotEmpty()) {
                            uploadImage(claims[0])
                        }
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        buttonEnable.set(true)
                    }
                }))
    }

    open fun uploadImage(claim: Claim) {
        for (image in images) {
            image.claimImage.claim = Claim().apply { id = claim.id }
        }

        val imagePaths = images.asSequence().filter { it.isLocalImage }.map { it.albumFile.path }.toList()
        val parts = arrayListOf<MultipartBody.Part>()
        imagePaths.forEach { path: String ->
            run {
                val file = File(path)
                val fileReqBody = RequestBody.create(MediaType.parse("image/*"), file)
                parts.add(MultipartBody.Part.createFormData("files", file.name, fileReqBody))
            }
        }
        val jsonAdd = Gson().toJson(images.asSequence().map { it.claimImage }.filter { it.id == null }.toList())
        val bodyJsonAdd = RequestBody.create(MediaType.parse("application/json"), jsonAdd)
        callApi(Service.callClaimService()
                .uploadImage(bodyJsonAdd, null, null, parts, claim.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ResponseStatus>(appException, null, null, null) {
                    override fun getDataSuccess(responseStatus: ResponseStatus?) {
                        clearImage()
                        uploadImageSuccess()
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone!!.message + " " + this.javaClass.name)
                    }
                }))
    }

    fun uploadImageSuccess() {
        sendBackAction(UPLOAD_IMAGE_CLAIM_SUCCESS)
    }

    private fun clearImage() {
        images.clear()
    }

    private fun clearData() {
        setData(arrayListOf())
        sendBackAction(NOTIFY_CHANGE)
        buttonEnable.set(true)
        amenableObservable.set(EMPTY_STRING)
        descriptionObservable.set(EMPTY_STRING)
    }

    fun getColor(claimDetail: ClaimDetail): Int {
        if (claimDetail.checked != null) {
            return if (claimDetail.checked) {
                ContextCompat.getColor(getApplication(), R.color.light_orange)
            } else {
                ContextCompat.getColor(getApplication(), R.color.light_red_2)
            }
        }
        return ContextCompat.getColor(getApplication(), R.color.white)
    }

    fun getColor(claimObject: ClaimObject): Int {
        if (claimObject.checked != null) {
            return if (claimObject.checked) {
                ContextCompat.getColor(getApplication(), R.color.light_orange)
            } else {
                ContextCompat.getColor(getApplication(), R.color.light_red_2)
            }
        }
        return ContextCompat.getColor(getApplication(), R.color.white)
    }
}