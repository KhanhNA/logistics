package com.next_solutions.logistics.component.search

import androidx.databinding.ObservableField
import com.next_solutions.logistics.utils.Constants.EMPTY_STRING

class SearchComponent {
    var code = ObservableField(EMPTY_STRING)
    fun resetData() {
        code.set(EMPTY_STRING)
        code.notifyChange()
    }


    fun setData(scanCode: String) {
        code.set(scanCode)
        code.notifyChange()
    }

    fun getCodeSearch(): String {
        val textSearch = code.get()
        textSearch?.let {
            return textSearch
        }
        return EMPTY_STRING
    }
}