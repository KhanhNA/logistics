package com.next_solutions.logistics.features.repacking_planning.repacked_statement

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.next_solutions.base.*
import com.next_solutions.logistics.R
import com.next_solutions.logistics.adapter.repacked_repacking.RepackedRepackingAdapter
import com.next_solutions.logistics.databinding.FragmentRepackedRepackingDetailBinding
import com.next_solutions.logistics.features.claim.claim_detail.ClaimDetailFragment
import com.next_solutions.logistics.features.repacking_planning.claim_repacked.ClaimRepackingRepackFragment
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.RepackingPlanning
import com.next_solutions.logistics.utils.Constants.*
import com.next_solutions.logistics.utils.qr_code_utils.QRCodeScannerActivity

@Suppress("UNCHECKED_CAST")
class RepackedRepackingPlanningFragment : BaseFragment() {
    private lateinit var repackingViewModel: RepackedRepackingPlanningViewModel
    private var adapterRepacking: RepackedRepackingAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private lateinit var bindingView: FragmentRepackedRepackingDetailBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        repackingViewModel = viewModel as RepackedRepackingPlanningViewModel
        bindingView = binding as FragmentRepackedRepackingDetailBinding
        getData()
        setUpRecycleView()
        repackingViewModel.init()
        return binding.root
    }

    private fun getData() {
        if (activity != null) {
            if (activity!!.intent.hasExtra(MODEL)) {
                val repacking = activity!!.intent.getSerializableExtra(MODEL) as RepackingPlanning
                repackingViewModel.repackingPlanning = repacking
            }
            if (activity!!.intent.hasExtra(CLAIM_DETAILS)) {
                val claimDetails = activity!!.intent.getSerializableExtra(CLAIM_DETAILS) as Array<Any>
                repackingViewModel.claimDetails = arrayListOf()
                for (obj in claimDetails) {
                    repackingViewModel.claimDetails!!.add(obj as ClaimDetail)
                }
                repackingViewModel.hideButtonViewClaim()
            }
        }
    }

    private fun setUpRecycleView() {
        val customRecyclerView: RecyclerView = bindingView.repackingPlanningDetails
        adapterRepacking = RepackedRepackingAdapter(repackingViewModel, (activity as FragmentActivity).supportFragmentManager)
        linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        customRecyclerView.adapter = adapterRepacking
        customRecyclerView.layoutManager = linearLayoutManager

        val spacing = 4
        customRecyclerView.addItemDecoration(ItemOffsetDecoration(spacing))
    }

    override fun action(view: View, baseViewModel: BaseViewModel<*>?) {
        if (SCAN_CODE == view.tag) {
            val qrScan = Intent(context, QRCodeScannerActivity::class.java)
            startActivityForResult(qrScan, REQUEST_CODE_SCAN)
        }
        if (R.id.createClaim == view.id) {
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(FRAGMENT, ClaimRepackingRepackFragment::class.java)
            bundle.putSerializable(MODEL, repackingViewModel.repackingPlanning)
            intent.putExtras(bundle)
            startActivityForResult(intent, CLAIM_ACTIVITY_CODE)
        }
        if (view.id == R.id.reviewClaim) {
            val intent = Intent(context, CommonActivity::class.java)
            val bundle = Bundle()
            val claim = repackingViewModel.claimDetails!![0].claim
            claim.claimDetails = repackingViewModel.claimDetails
            bundle.putSerializable(FRAGMENT, ClaimDetailFragment::class.java)
            bundle.putSerializable(MODEL, claim)
            bundle.putSerializable(MODEL_REPACKING_PLANNING, repackingViewModel.repackingPlanning)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_SCAN) {
                repackingViewModel.searchByCode(data!!.getStringExtra(RESULT_SCAN))
            }
            if (requestCode == CLAIM_ACTIVITY_CODE && data!!.hasExtra(CLAIM_ACTIVITY)) {
                val result = data.getStringExtra(CLAIM_ACTIVITY)!!
                if (result == CREATE_CLAIM_SUCCESS) {
                    baseActivity.onBackPressed()
                }
            }
        }
    }

    override fun processFromVM(action: String?, view: View?, viewModel: BaseViewModel<*>?, t: Throwable?) {
        when (action) {
            SUCCESS -> {
                val handler = Handler()
                handler.postDelayed({
                    if (baseActivity != null) {
                        baseActivity.onBackPressed()
                    }
                }, TIME_DELAY.toLong())
                showAlertSuccess(TIME_AFTER_DELAY, R.string.success, null)
            }
            NOTIFY_CHANGE -> {
                val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
                bindingView.repackingPlanningDetails.layoutAnimation = controller
                adapterRepacking!!.notifyDataSetChanged()
                bindingView.repackingPlanningDetails.scheduleLayoutAnimation()
            }
            NOTIFY_CHANGE_ITEM -> {
                showErrorDetail(R.string.error_invalid_input)
            }
            "NOT_MATCH_QUANTITY" -> {
                showErrorDetail(R.string.not_match_repacked_quantity)
            }
            SCROLL_TO_POSITION -> {
                repackingViewModel.notifyPosition.get()?.let {
                    adapterRepacking!!.notifyItemChanged(it)
                    linearLayoutManager!!.scrollToPosition(it)
                }
                repackingViewModel.lastNotifyPosition?.let {
                    adapterRepacking!!.notifyItemChanged(it)
                }
            }
        }
    }


    private fun showErrorDetail(messageId: Int) {
        repackingViewModel.notifyPosition.get()?.let {
            adapterRepacking!!.notifyItemChanged(it)
            linearLayoutManager!!.scrollToPosition(it)
            showAlertWarning(messageId)
        }
        repackingViewModel.lastNotifyPosition?.let {
            adapterRepacking!!.notifyItemChanged(it)
        }
    }

    override fun onResume() {
        super.onResume()
        repackingViewModel.search()
    }

    override fun getVMClass(): Class<out BaseViewModel<BaseModel>> {
        return RepackedRepackingPlanningViewModel::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_repacked_repacking_detail
    }

    override fun getRecycleResId(): Int {
        return 0
    }

}