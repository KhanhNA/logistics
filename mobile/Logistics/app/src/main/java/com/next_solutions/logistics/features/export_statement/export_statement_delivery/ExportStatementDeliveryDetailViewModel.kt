package com.next_solutions.logistics.features.export_statement.export_statement_delivery

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import com.next_solutions.base.BaseModel
import com.next_solutions.base.BaseViewModel
import com.next_solutions.base.ErrorRespone
import com.next_solutions.logistics.config.RxSingleSubscriber
import com.next_solutions.logistics.models.Claim
import com.next_solutions.logistics.models.ClaimDetail
import com.next_solutions.logistics.models.ExportStatement
import com.next_solutions.logistics.repository.Service
import com.next_solutions.logistics.utils.Constants.ERROR_PROCESS

class ExportStatementDeliveryDetailViewModel(application: Application) : BaseViewModel<BaseModel>(application) {

    lateinit var exportStatement: ExportStatement
    val buttonVisible = ObservableField(false)
    val buttonReviewClaim = ObservableField(false)
    val invisibleReviewClaim = ObservableField(true)
    lateinit var claim: Claim

    override fun init() {
        findExportStatement()
    }

    private fun findExportStatement() {
        callApi(Service.callExportStatementService().findExportStatementById(exportStatement.id)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<ExportStatement>(appException, ExportStatement::class.java, null) {
                    override fun getDataSuccess(export: ExportStatement?) {
                        getExportStatementSuccess(export!!)
                    }

                    override fun onFailure(errorRespone: ErrorRespone?) {
                        Log.e(ERROR_PROCESS, errorRespone?.message)
                    }
                }))
    }

    private fun showButton() {
        callApi(Service.callClaimService()
                .findClaimByReference(exportStatement.id, Claim.ClaimType.DELIVERY.value)
                .compose(Service.getRxSingleSchedulers().applySchedulers())
                .subscribeWith(object : RxSingleSubscriber<List<ClaimDetail>>(appException, ClaimDetail::class.java, List::class.java) {
                    override fun getDataSuccess(claimDetails: List<ClaimDetail>) {
                        if (claimDetails.isEmpty()) {
                            setButton(true)
                        } else {
                            invisibleReviewClaim()
                            claim = claimDetails[0].claim
                        }
                    }

                    override fun onFailure(errorRespone: ErrorRespone) {
                        buttonVisible.set(false)
                        buttonReviewClaim.set(false)
                        buttonVisible.notifyChange()
                        buttonReviewClaim.notifyChange()
                    }
                })

        )
    }

    private fun setButton(checked: Boolean) {
        buttonVisible.set(checked)
        buttonVisible.notifyChange()
        buttonReviewClaim.set(!checked)
        buttonReviewClaim.notifyChange()
    }

    private fun invisibleReviewClaim() {
        buttonVisible.set(false)
        buttonVisible.notifyChange()
        if (invisibleReviewClaim.get()!!) {
            buttonReviewClaim.set(true)
            buttonReviewClaim.notifyChange()
        }
    }


    fun onCreate() {

    }

    private fun getExportStatementSuccess(export: ExportStatement) {
        this.exportStatement = export
        showButton()
        setData(export.exportStatementDetails as List<BaseModel>?)
    }

}