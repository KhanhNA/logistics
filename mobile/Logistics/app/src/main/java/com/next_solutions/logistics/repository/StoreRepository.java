package com.next_solutions.logistics.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.next_solutions.logistics.models.dao.StoreDao;
import com.next_solutions.logistics.models.entity.StoreEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;

public class StoreRepository {
    private int CODE_RESPONSE_FROM_STORE = 1000;
    private StoreDao mStoreDao;
    private LiveData<List<StoreEntity>> allStore;

    public StoreRepository(Application application) {
//        AppDatabase db = AppDatabase.getDatabase(application);
//        mStoreDao = db.storeDao();
        //allProduct = mProductDao.getAllProduct();
    }

    public void insert(StoreEntity store) {
        new StoreRepository.insertAsyncTask(mStoreDao).execute(store);
    }

    public void insert(List<StoreEntity> store) {
//        new InsertAsyncTask(mProductDao).execute(product);
    }

    public void getAllStoreFromLocal() {
        allStore = mStoreDao.getAllStore();
    }

    public void deleteAll() {
        Completable.fromAction(() -> mStoreDao.deleteAll()).subscribeOn(Schedulers.io()).subscribe();
    }

    private static class insertAsyncTask extends AsyncTask<StoreEntity, Void, Void> {

        private StoreDao mAsyncTaskDao;

        insertAsyncTask(StoreDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final StoreEntity... params) {
            mAsyncTaskDao.insert(params);
            return null;
        }
    }

}
