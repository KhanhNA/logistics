package com.next_solutions.logistics.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.List;


@JsonIdentityInfo(generator = JSOGGenerator.class)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Country extends Base {
    private String isocode;
    private Boolean support;
    private List<CountryDescription> countryDescriptions;

    public String getIsocode() {
        return isocode;
    }

    public void setIsocode(String isocode) {
        this.isocode = isocode;
    }

    public Boolean getSupport() {
        return support;
    }

    public void setSupport(Boolean support) {
        this.support = support;
    }

    public List<CountryDescription> getCountryDescriptions() {
        return countryDescriptions;
    }

    public void setCountryDescriptions(List<CountryDescription> countryDescriptions) {
        this.countryDescriptions = countryDescriptions;
    }
}
