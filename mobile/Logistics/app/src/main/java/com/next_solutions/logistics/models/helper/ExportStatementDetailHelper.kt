package com.next_solutions.logistics.models.helper

import com.next_solutions.logistics.models.*
import com.next_solutions.logistics.models.dto.ImportStatementDetailDTO

class ExportStatementDetailHelper {
    fun convertInventoryToExportStatementDetail(inventory: Inventory): ExportStatementDetail {
        val inventoryPacking = inventory.productPacking
        val inventoryPrices = inventoryPacking.product.productPackingPrices
        val prices = arrayListOf<ProductPackingPrice>()
        inventoryPrices.forEach {
            prices.add(ProductPackingPrice().apply {
                price = it.price
                currency = Currency().apply { code = it.currency.code }
            })
        }
        return ExportStatementDetail().apply {
            quantity = 0
            productPackingId = inventory.productPackingId
            productPackingCode = inventoryPacking.code
            productPacking = ProductPacking().apply {
                product = Product().apply { name = inventoryPacking.product.name }
                packingType = PackingType().apply { quantity = inventoryPacking.packingType.quantity }
                uom = inventoryPacking.uom
                code = inventoryPacking.code
                productPackingPrices = prices
                totalQuantity = inventory.totalQuantity
            }
        }
    }


    fun minimizeDetail(exportStatementDetail: ExportStatementDetail): ExportStatementDetail {
        val prices = arrayListOf<ProductPackingPrice>()
        val inventoryPrices = exportStatementDetail.productPacking.product.productPackingPrices
        inventoryPrices.forEach {
            prices.add(ProductPackingPrice().apply {
                price = it.price
                currency = Currency().apply { code = it.currency.code }
            })
        }
        return ExportStatementDetail().apply {
            quantity = exportStatementDetail.quantity
            productPackingId = exportStatementDetail.productPacking.id
            productPackingCode = exportStatementDetail.productPacking.code
            productPacking = ProductPacking().apply {
                product = Product().apply { name = exportStatementDetail.productPacking.product.name }
                packingType = PackingType().apply { quantity = exportStatementDetail.productPacking.packingType.quantity }
                uom = exportStatementDetail.productPacking.uom
                code = exportStatementDetail.productPacking.code
                productPackingPrices = prices
            }
        }
    }

    fun groupDetailByExpireDateAndProductPacking(data: List<ImportStatementDetailDTO>): List<ImportStatementDetailDTO> {
        val hashMap = HashMap<String, ImportStatementDetailDTO>()
        data.forEach { detail ->
            run {
                val key = detail.expireDate + " " + detail.productPackingCode
                if (hashMap.containsKey(key)) {
                    val importStatementDetailDTO = hashMap[key]
                    importStatementDetailDTO!!.quantity += detail.quantity
                } else {
                    hashMap.put(key, detail)
                }
            }
        }
        return hashMap.values.toList()
    }

    fun groupExportDetails(details: List<ExportStatementDetail>): List<ExportStatementDetail> {
        val hashMap = HashMap<String, ExportStatementDetail>()
        details.forEach { detail ->
            run {
                val key = detail.expireDate + " " + detail.productPacking.code
                if (hashMap.containsKey(key)) {
                    val importStatementDetailDTO = hashMap[key]
                    importStatementDetailDTO!!.quantity += detail.quantity
                } else {
                    hashMap.put(key, detail)
                }
            }
        }
        return hashMap.values.toList()
    }
}