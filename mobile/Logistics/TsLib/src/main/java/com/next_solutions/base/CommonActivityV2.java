package com.next_solutions.base;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.next_solutions.base.databinding.CommonActivityV2Binding;


public class CommonActivityV2 extends AppCompatActivity {
    protected ProgressDialog pdv2;
    protected BaseViewModelActivity viewModel;
    private BaseFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Class<?> clazz = (Class) getIntent().getSerializableExtra("FRAGMENT");
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.commonFrag, (Fragment) clazz.newInstance());
            ft.commit();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        CommonActivityV2Binding binding = DataBindingUtil.setContentView(this, R.layout.common_activity_v2);
        viewModel = new BaseViewModelActivity(getApplication());
        binding.setViewModel(viewModel);


        viewModel.getShowProcess().observe(this, (show) -> {
            Boolean showProcess = (Boolean) show;
            if (showProcess != null && showProcess) {
                openProcess(viewModel.getProcessTitle(), viewModel.getProcessContent());
            } else if (showProcess != null) {
                closeProcess();
            }
        });
    }

    public void openProcess(int processTitle, int processContent) {
        if (pdv2 == null) {
            pdv2 = new ProgressDialog(this);
            if (!pdv2.isShowing()) {
                pdv2.setTitle(processTitle);
                pdv2.setMessage(getResources().getString(processContent));
                pdv2.show();
            }
        }
    }

    public void closeProcess() {
        if (pdv2 != null) {
            if (pdv2.isShowing()) {
                pdv2.dismiss();
            }
        }
    }
}
