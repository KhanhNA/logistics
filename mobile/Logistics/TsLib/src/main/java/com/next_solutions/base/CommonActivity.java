package com.next_solutions.base;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


public class CommonActivity extends BaseActivity {


    private BaseFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            Class<?> clazz = (Class) getIntent().getSerializableExtra("FRAGMENT");
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.commonFrag, (Fragment) clazz.newInstance());
            ft.commit();
        } catch (Exception ex) {
            processError("error", null, viewModel, ex);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.common_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    public BaseFragment getFragment() {
        return fragment;
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
    }
}
