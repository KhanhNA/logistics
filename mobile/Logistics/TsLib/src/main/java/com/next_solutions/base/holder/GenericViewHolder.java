package com.next_solutions.base.holder;


import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.next_solutions.base.BR;
import com.next_solutions.base.BaseModel;
import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.listener.AdapterActionsListener;


public class GenericViewHolder extends RecyclerView.ViewHolder {
    final ViewDataBinding binding;

    public GenericViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = null;
    }

    public GenericViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;

    }

    public void setBinding(BaseModel obj, BaseViewModel viewModel, AdapterActionsListener listener) {
        binding.setVariable(BR.viewHolder, obj);
        binding.setVariable(BR.viewModel, viewModel);
        binding.setVariable(BR.listener, listener);

//        obj.bindingAction();
        binding.executePendingBindings();
    }


    public void setBinding(BaseModel obj, AdapterActionsListener listener) {
        if (binding == null) {
            return;
        }
        binding.setVariable(BR.viewHolder, obj);
//        binding.setVariable(BR.viewModel, viewModel);
        binding.setVariable(BR.listener, listener);

//        obj.bindingAction();
        binding.executePendingBindings();
    }


}
