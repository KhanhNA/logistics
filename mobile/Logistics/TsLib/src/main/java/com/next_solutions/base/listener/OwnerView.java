package com.next_solutions.base.listener;


import android.view.View;

import com.next_solutions.base.BaseModel;

public interface OwnerView extends BaseListener {
    void onClicked(View view, BaseModel baseModel);
}
