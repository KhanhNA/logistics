package com.next_solutions.base;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.CallSuper;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.next_solutions.base.listener.AdapterActionsListener;
import com.next_solutions.base.listener.DefaultFunctionActivity;
import com.next_solutions.base.listener.ViewActionsListener;
import com.tapadoo.alerter.Alerter;

public abstract class BaseFragment extends Fragment implements DefaultFunctionActivity, AdapterActionsListener, ViewActionsListener {
    protected RecyclerView recyclerView;
    protected ViewDataBinding binding;
    protected BaseViewModel viewModel;
    protected LinearLayoutManager layoutManager;

    @Override
    public BaseActivity getBaseActivity() {
        BaseActivity baseActivity = null;
        try {
            baseActivity = (BaseActivity) getActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseActivity;
    }

    @Nullable
    @CallSuper
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            super.onCreateView(inflater, container, savedInstanceState);
            binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
            if (getVMClass() != null) {
                init(savedInstanceState, getLayoutRes(), getVMClass(), getRecycleResId());
            }
            viewModel.getActionToView().observe(this, action -> {
                processFromVM((String) action, null, null, null);
            });
        } catch (Throwable e) {
            e.printStackTrace();
            if (viewModel != null && viewModel.getAppException() != null) {
                viewModel.getAppException().postValue(e);
            }
        }
        return binding.getRoot();
    }

    public ViewModel getShareViewModel(Class clazz) {
        return new ViewModelProvider(getBaseActivity(), BaseViewModelFactory.getInstance(getBaseActivity()
                .getApplication(), clazz))
                .get(clazz);
    }

    public void init(@Nullable Bundle savedInstanceState, @LayoutRes int layoutId,
                     Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Throwable {
        viewModel = new ViewModelProvider(getBaseActivity(), BaseViewModelFactory.getInstance(getBaseActivity().getApplication(), clazz))
                .get(clazz);
        //ViewModelProviders.of(getActivity()).get(clazz);
        View view = binding.getRoot();
        binding.setVariable(BR.viewModel, viewModel);
        if (viewModel != null) {
            viewModel.setView(this::processFromVM);
            viewModel.setAppException(getBaseActivity().getViewModel().getAppException());
            viewModel.setAlertModel(getBaseActivity().getViewModel().getAlertModel());
        }
        binding.setVariable(BR.listener, this);

        if (recyclerViewId != 0) {
            recyclerView = view.findViewById(recyclerViewId);
            layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);

            int spacing = 1;
            recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));
        }
    }

    @Override
    public void adapterAction(View view, BaseModel baseModel) {

    }

    @Override
    final public void onAdapterClicked(View view, BaseModel bm) {
        AdapterActionsListener.super.onAdapterClicked(view, bm);
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {

    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if ("hideKeyBoard".equals(action)) {
            hideKeyBoard();
        }

    }

    private void hideKeyBoard() {
        View view1 = getBaseActivity().getCurrentFocus();
        if (view1 != null) {
            InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
        }
    }

    public LinearLayoutManager getLayoutManager() {
        return layoutManager;
    }

    public void closeProcess() {
        getBaseActivity().closeProcessV2();
    }

    public void openProcess() {
        getBaseActivity().openProcessV2();
    }

    @Override
    final public void onClicked(View view, BaseViewModel viewModel) {
        ViewActionsListener.super.onClicked(view, viewModel);
    }

    public BaseViewModel getViewModel() {
        return viewModel;
    }

    public void setViewModel(BaseViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void showProcess(int stringTitle, int stringContent) {
        CommonActivityV2 activity = (CommonActivityV2) getActivity();
        if (activity != null) {
            activity.openProcess(stringTitle, stringContent);
        }
    }

    public void showAlertWarning(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());

        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setBackgroundColorRes(R.color.colorWaning);
        alerter.show();
    }

    public void showAlertWarning(Integer title) {
        showAlertWarning(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlertError(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setIcon(R.drawable.cross);
        alerter.setBackgroundColorRes(R.color.colorError);
        alerter.show();
    }

    public void showAlertError(Integer title) {
        showAlertError(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlertSuccess(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setIcon(R.drawable.check);
        alerter.setBackgroundColorRes(R.color.colorSuccess);
        alerter.show();
    }

    public void showAlertSuccess(Integer title) {
        showAlertSuccess(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlert(Integer liveTime, Integer color, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        if (color != null) {
            alerter.setBackgroundColorRes(color);
        }
        alerter.show();
    }

    public void showNotification(String title, String task, String notificationId, String notificationName, Integer iconId) {
        Context context = getContext();
        if (context != null) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(notificationId, notificationName, NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            NotificationCompat.Builder notificationCompat = new NotificationCompat
                    .Builder(context, notificationId)
                    .setContentTitle(title)
                    .setContentText(task)
                    .setSmallIcon(iconId);
            notificationManager.notify(1, notificationCompat.build());
        }
    }

    public void showNotification(String title, String task, String notificationId, String notificationName) {
        Context context = getContext();
        if (context != null) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(notificationId, notificationName, NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            NotificationCompat.Builder notificationCompat = new NotificationCompat
                    .Builder(context, notificationId)
                    .setContentTitle(title)
                    .setContentText(task)
                    .setSmallIcon(R.drawable.alerter_ic_notifications);
            notificationManager.notify(1, notificationCompat.build());
        }
    }
}
