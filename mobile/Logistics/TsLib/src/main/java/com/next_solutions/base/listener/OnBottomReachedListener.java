package com.next_solutions.base.listener;

public interface OnBottomReachedListener {
    void onBottomReached(int position);
}
