package com.next_solutions.base;


import androidx.lifecycle.MutableLiveData;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.next_solutions.base.exceptionHandle.AppException;
import com.next_solutions.base.listener.ResponseResult;

import java.io.IOException;
import java.util.Properties;

import lombok.Data;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Data
public class MyCallBackMapper implements Callback<ResponseBody> {
    private ResponseResult func;
    private Class<?> element;
    private JavaType element1;
    private Class type;
    private MutableLiveData<Throwable> appException;

    public MyCallBackMapper(MutableLiveData<Throwable> ex, ResponseResult result, Class<?> elem, Class<?> type) {
        this.func = result;
        this.element = elem;
        this.type = type;
        this.appException = ex;
        this.element1 = null;
    }

    public MyCallBackMapper(MutableLiveData<Throwable> ex, ResponseResult result, Class<?> pClass, Class<?> cClass, Class<?> type) {
        this.func = result;
        //this.element1 = elem;
        ObjectMapper objectMapper = new ObjectMapper();
        if (pClass != null && cClass != null) {
            this.element1 = objectMapper.getTypeFactory().constructParametricType(pClass, cClass);
        } else {
            this.element1 = null;
        }
        this.element = null;
        this.type = type;
        this.appException = ex;
    }

    public MyCallBackMapper(MutableLiveData<Throwable> ex) {
        this.appException = ex;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {
            try {
                ResponseBody responseBody = response.body();
                Class<?> out;
                if (responseBody != null) {
                    String s = responseBody.string();
                    Object value;
                    if (element == null && element1 == null && type == null && func != null) {
                        func.onResponse(call, response, "", null);
                        return;
                    }
                    if (element == null && element1 == null) {

                        value = new Gson().fromJson(s, Properties.class);
                    } else {
                        ObjectMapper objectMapper = new ObjectMapper()
                                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        if (type == null) {
                            if (element1 != null) {
                                value = objectMapper.readValue(s, element1);
                            } else {
                                value = objectMapper.readValue(s, element);
                            }
                        } else {
                            value = objectMapper.readValue(s, objectMapper.getTypeFactory().constructCollectionType(type, element));
                        }
                    }
//                    Object value = objectMapper.
                    //Method m = paren t.getClass().getMethod(functionName, List.class);


                    //m.invokeFunc(parent, value);
                    if (func != null) {
                        func.onResponse(call, response, value, null);
                    }

                }
            } catch (Throwable e) {
                if (appException != null) {
                    appException.postValue(e);
                } else {
                    if (func != null) {
                        func.onResponse(call, response, null, e);
                    }
                }
            }

        } else {
//                    handleResponseError(serviceName, response.code(), response.body());
            if (appException != null) {
                try {
                    String s = response.errorBody().string();
                    ObjectMapper objectMapper = new ObjectMapper();
                    if (!"".equals(s)) {
                        ErrorRespone errorRespone = objectMapper.readValue(s, ErrorRespone.class);
                        appException.setValue(new AppException(response.code(), errorRespone.getMessage()));
                        func.onResponse(call, response, null, new AppException(response.code(), response.toString()));
                    } else {
                        appException.setValue(new AppException(R.string.error_process, "Error"));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    appException.setValue(new AppException(R.string.error_process, ""));
                }
            } else {
                if (func != null) {
                    func.onResponse(call, response, null, new AppException(response.code(), response.toString()));
                }
            }
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        if (appException != null) {
            appException.postValue(t);
        } else {
            if (func != null) {
                func.onResponse(call, null, null, t);
            }
        }
    }

}
