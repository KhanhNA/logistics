package com.next_solutions.base.listener;

public interface ILoadMore {
    void onLoadMore();
}
