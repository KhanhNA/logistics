package com.next_solutions.base;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


public class BaseViewModelActivity<T extends BaseModel> extends AndroidViewModel {
    private int processTitle;
    private int processContent;
    private MutableLiveData<Boolean> showProcess = new MutableLiveData<>();

    public BaseViewModelActivity(@NonNull Application application) {
        super(application);
    }

    public void showProcess(int processTitle, int processContent) {
        this.processContent = processContent;
        this.processTitle = processTitle;
        showProcess.setValue(true);
    }

    public void closeProcess() {
        showProcess.setValue(false);
    }

    public int getProcessTitle() {
        return processTitle;
    }


    public int getProcessContent() {
        return processContent;
    }


    public MutableLiveData<Boolean> getShowProcess() {
        return showProcess;
    }
}
