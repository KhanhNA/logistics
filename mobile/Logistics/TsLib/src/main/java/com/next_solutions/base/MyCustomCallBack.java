package com.next_solutions.base;


import androidx.annotation.NonNull;

import com.next_solutions.base.exceptionHandle.AppException;
import com.next_solutions.base.listener.ResponseResult;

import lombok.Data;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Data
public class MyCustomCallBack<T> implements Callback<T> {
    private ResponseResult func;

    public MyCustomCallBack(ResponseResult result) {
        this.func = result;

    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (response.isSuccessful()) {
            try {
                if (func != null) {
                    func.onResponse(call, response, response.body(), null);


                }
            } catch (Throwable e) {
                if (func != null) {
                    func.onResponse(call, response, null, e);
                }
            }

        } else {
            if (func != null) {
                func.onResponse(call, response, null, new AppException(response.code(), response.toString()));
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if (func != null) {
            func.onResponse(call, null, null, t);
        }
    }
}
