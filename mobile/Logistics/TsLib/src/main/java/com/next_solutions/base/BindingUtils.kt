package com.next_solutions.base

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener

object BindingUtils {

    @JvmStatic
    @InverseBindingAdapter(attribute = "textChange", event = "textAttrChanged")
    fun getTextChange(view: EditText): String {
        val text = view.text.toString().replace("\\D+".toRegex(), "")
        if (TextUtils.isEmpty(text)) {
            return "0"
        }
        try {
            val number = text.toLong()
            return String.format("%,.0f", number)
        } catch (e: Exception) {
            return "0"
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["textChange", "textAttrChanged"], requireAll = false)
    fun setTextChange(view: EditText, text: String, textAttrChanged: InverseBindingListener) {
        view.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                textAttrChanged.onChange()
                view.setSelection(view.text.length)
            }

            override fun afterTextChanged(editable: Editable) {
            }
        })
    }
}