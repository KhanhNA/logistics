package com.next_solutions.base;

import java.io.Serializable;
import java.util.List;

public class BaseModel implements Serializable {
    public Boolean checked;
    public Integer index;
    public String transactionErrCode;
    public List<BaseModel> child;
    public void bindingAction() {

    }
}
