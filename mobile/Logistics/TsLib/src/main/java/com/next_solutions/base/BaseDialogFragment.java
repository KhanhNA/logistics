package com.next_solutions.base;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.next_solutions.base.listener.DefaultFunctionActivity;
import com.next_solutions.base.listener.ViewActionsListener;
import com.tapadoo.alerter.Alerter;

@SuppressWarnings("unchecked")
public abstract class BaseDialogFragment extends DialogFragment implements ViewActionsListener, DefaultFunctionActivity {
    protected RecyclerView recyclerView;
    protected ViewDataBinding binding;
    protected BaseViewModel viewModel;
    protected ProgressDialog pd;


    @Override
    public BaseActivity getBaseActivity() {
        BaseActivity baseActivity = null;
        try {
            baseActivity = (BaseActivity) getActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseActivity;
    }


    @Override
    @CallSuper
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        try {
            init(inflater, container, savedInstanceState, getLayoutRes(), getVMClass(), getRecycleResId());
        } catch (Throwable e) {
            e.printStackTrace();
            if (viewModel != null && viewModel.getAppException() != null) {
                viewModel.getAppException().postValue(e);
            }
        }
        return v;
    }


    public void init(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, @LayoutRes int layoutId,
                     Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Exception {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false);
        viewModel = new ViewModelProvider(getBaseActivity(), BaseViewModelFactory.getInstance(getBaseActivity().getApplication(), clazz))
                .get(clazz);
        View view = binding.getRoot();
        if (recyclerViewId != 0) {
            recyclerView = view.findViewById(recyclerViewId);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
        }
        binding.setVariable(BR.viewModel, viewModel);
        binding.setVariable(BR.listener, this);
        viewModel.setView(this::processFromVM);
        viewModel.setAppException(getBaseActivity().getViewModel().getAppException());
        viewModel.setAlertModel(getBaseActivity().getViewModel().getAlertModel());
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        showProcessing(getResources().getString(R.string.wait));
    }

    @Override
    final public void onClicked(View view, BaseViewModel viewModel) {
        ViewActionsListener.super.onClicked(view, viewModel);
    }


    public void showAlertWarning(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());

        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setBackgroundColorRes(R.color.colorWaning);
        alerter.show();
    }

    public void showAlertWarning(Integer title) {
        showAlertWarning(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlertError(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setIcon(R.drawable.cross);
        alerter.setBackgroundColorRes(R.color.colorError);
        alerter.show();
    }

    public void showAlertError(Integer title) {
        showAlertError(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlertSuccess(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setIcon(R.drawable.check);
        alerter.setBackgroundColorRes(R.color.colorSuccess);
        alerter.show();
    }

    public void showAlertSuccess(Integer title) {
        showAlertSuccess(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlert(Integer liveTime, Integer color, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        if (color != null) {
            alerter.setBackgroundColorRes(color);
        }
        alerter.show();
    }

    public void showNotification(String title, String task, String notificationId, String notificationName, Integer iconId) {
        Context context = getContext();
        if (context != null) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(notificationId, notificationName, NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            NotificationCompat.Builder notificationCompat = new NotificationCompat
                    .Builder(context, notificationId)
                    .setContentTitle(title)
                    .setContentText(task)
                    .setSmallIcon(iconId);
            notificationManager.notify(1, notificationCompat.build());
        }
    }

    public void showNotification(String title, String task, String notificationId, String notificationName) {
        Context context = getContext();
        if (context != null) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(notificationId, notificationName, NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            NotificationCompat.Builder notificationCompat = new NotificationCompat
                    .Builder(context, notificationId)
                    .setContentTitle(title)
                    .setContentText(task)
                    .setSmallIcon(R.drawable.alerter_ic_notifications);
            notificationManager.notify(1, notificationCompat.build());
        }
    }
}
