package com.next_solutions.base.listener;

import android.view.View;

import com.next_solutions.base.BaseViewModel;
import com.next_solutions.base.exceptionHandle.AppException;

public interface ViewFunction extends BaseListener {
    void process(String action, View view, BaseViewModel viewModel, Throwable t);

    default void action(String action, View view, BaseViewModel viewModel, Throwable t) throws AppException {
        try {
            process(action, view, viewModel, t);
        } catch (Exception e) {
//            throw new AppException(-1000, TsLog.printStackTrace(e));
            e.printStackTrace();
        }
    }
}
