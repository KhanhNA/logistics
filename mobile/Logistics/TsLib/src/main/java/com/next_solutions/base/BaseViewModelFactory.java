package com.next_solutions.base;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("unchecked")
public class BaseViewModelFactory implements ViewModelProvider.Factory {
    private static Class classType;
    private static BaseViewModelFactory myViewModelFactory;
    private Application mApplication;

    private BaseViewModelFactory(Application application) {
        mApplication = application;
    }

    public static <U> BaseViewModelFactory getInstance(Application application, Class classType) {
        if (myViewModelFactory == null) {
            myViewModelFactory = new BaseViewModelFactory(application);
        }
        BaseViewModelFactory.classType = classType;
        return myViewModelFactory;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        try {
            return (T) classType.getConstructor(Application.class).newInstance(mApplication);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return (T) new BaseViewModel<>(mApplication);
    }
}
