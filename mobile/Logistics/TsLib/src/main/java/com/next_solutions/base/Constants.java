package com.next_solutions.base;

public class Constants {
    public static final String APP_INFO = "DCOMMERCE";
    public static final String TOKEN_INFO = "TOKEN_INFO";
    public static final String USER_NAME = "USER_NAME";
    public static final String MK = "MK";
    public static final String CREATE_SUCCESS = "CREATE_SUCCESS";

    public static final int TIME_DELAY = 3000;
    public static final int TIME_AFTER_DELAY = 2000;
    public static final int CODE_SUCCESS = 200;
    public static final int CODE_NOT_FOUND = 404;
    public static final int CODE_ERROR_INTERNAL = 500;
    public static final int CODE_AUTHEN_FAIL = 401;

    public final static int METHOD_WALLET = 1;//tra bang vi
    public final static int METHOD_CARD = 2; // tra bang the
    public final static int METHOD_DIRECTORY = 3; //tra truc tiep

    public static final int ORDER_PENDING = 0;
    public static final int ORDER_APPROVE = 1;
    public static final int ORDER_REJECT = 2;
    public static final int LANG_VI = 1;
    public static final int LANG_EN = 2;
    public static final long MERCHANT_ID_FAKE = 1;
    public static final int INCENTIVE = 1;
    public static final int SALE = 0;
    public static final int GET_DATA_MANUFACTURE = 1001;
    public static final int GET_DATA_CATEGORIES = 1002;

    public static final int GET_DATA_PRODUCT = 1003;
    public static final int GET_PRODUCT_DETAIL = 1004;
    public static final int GET_DATA_LIST_ORDER = 1005;
    public static final int GET_DATA_LIST_PRODUCT_ORDER_CART = 1020;
    public static final int INTENT_GET_RECEPTION_ADDRESS = 1021;
    public static final int INTENT_GET_METHOD_PAYMENT = 1022;

    public static final int GET_DATE_ORDER_DETAIL = 1006;
    public static final int REGISTER_MERCHANT = 1007;
    public static final int GET_DATA_MERCHANT = 1008;
}
