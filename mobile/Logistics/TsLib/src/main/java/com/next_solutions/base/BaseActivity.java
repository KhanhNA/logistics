package com.next_solutions.base;

import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.next_solutions.base.exceptionHandle.AppException;
import com.next_solutions.base.listener.DefaultFunctionActivity;
import com.next_solutions.base.listener.ViewActionsListener;
import com.tapadoo.alerter.Alerter;

import java.lang.reflect.InvocationTargetException;

public abstract class BaseActivity extends AppCompatActivity implements ViewActionsListener, DefaultFunctionActivity {
    protected static ProgressDialog pd;
    protected static ProgressDialog pdv2;
    private static AlertDialog.Builder alertDialog;
    protected ProgressDialog.Builder pdb;
    protected BaseViewModel viewModel;
    protected ViewDataBinding binding;

    public BaseActivity() {

    }

    public ProgressDialog.Builder getPdb() {
        return pdb;
    }

    public void setPdb(ProgressDialog.Builder pdb) {
        this.pdb = pdb;
    }

    public ProgressDialog getPd() {
        return pd;
    }

    public void setPd(ProgressDialog pd) {
        BaseActivity.pd = pd;
    }

    public AlertDialog.Builder getAlertDialog() {
        return alertDialog;
    }

    public void setAlertDialog(AlertDialog.Builder alertDialog) {
        BaseActivity.alertDialog = alertDialog;
    }

    @Override
    public BaseActivity getBaseActivity() {
        return this;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try {
            viewModel = getVMClass().getDeclaredConstructor(Application.class).newInstance(getBaseActivity().getApplication());//ViewModelProviders.of(getActivity()).get(clazz);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        binding = DataBindingUtil.setContentView(this, getLayoutRes());


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (viewModel != null) {
            viewModel.setView(this::processFromVM);
            binding.setVariable(BR.viewModel, viewModel);
            binding.setVariable(BR.listener, this);
            viewModel.initObs();

            viewModel.getAppException().observe(this, ex -> {
//                processError("error", null, viewModel, (Throwable) viewModel.getAppException().getValue());
                Throwable t = (Throwable) viewModel.getAppException().getValue();
                if (t == null) {
                    return;
                }
                String msg;
                if (t instanceof AppException) {
                    AppException exception = (AppException) t;
                    msg = exception.message;
                    try {
                        if (msg == null && exception.code != null) {
                            msg = getBaseActivity().getResources().getString(exception.code, exception.message);
                        }
                    } catch (Resources.NotFoundException e) {
                        Integer i = AppException.errCode.get(exception.code);
                        if (i != null) {
                            try {
                                msg = getBaseActivity().getResources().getString(i, exception.message);
                            } catch (Resources.NotFoundException ex1) {

                            }
                        }
                    }
                } else {
                    msg = t.getMessage();
                }

                showAlertError(msg);
            });

            viewModel.getActionToView().observe(this, action -> {
                processFromVM((String) action, null, null, null);
            });

            viewModel.getAlertModel().observe(this, alert -> {
                AlertModel a = (AlertModel) alert;
                showAlertDialog(a.msg, a.funcPositive);
            });
        }
    }

    public void closeProcessV2() {
        if (pdv2 != null && pdv2.isShowing()) {
            pdv2.dismiss();
        }
    }

    public void openProcessV2() {
        if (pdv2 != null && !pdv2.isShowing()) {
            pdv2.setTitle("Loading");
            pdv2.setMessage("Wait while loading...");
            pdv2.setCancelable(false); // disable dismiss by tapping outside of the dialog
            pdv2.show();
        } else {
            pdv2 = new ProgressDialog(getBaseActivity());
        }
    }


    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        showProcessing(getResources().getString(R.string.wait));
    }


    public BaseViewModel getViewModel() {
        return viewModel;
    }


    public Object getListener() {
        return this;
    }


    @Override
    final public void onClicked(View view, BaseViewModel viewModel) {
        ViewActionsListener.super.onClicked(view, viewModel);
    }

    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    public void replaceFragment(@IdRes int containerViewId,
                                @NonNull Fragment fragment,
                                @NonNull String fragmentTag,
                                @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        finish();
//        closeProcess();
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if ("hideKeyBoard".equals(action)) {
            hideKeyBoard();
        }
    }

    private void hideKeyBoard() {
        View view1 = getBaseActivity().getCurrentFocus();
        if (view1 != null) {
            InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
        }
    }

    public void showAlertWarning(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());

        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setBackgroundColorRes(R.color.colorWaning);
        alerter.show();
    }

    public void showAlertWarning(Integer title) {
        showAlertWarning(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlertError(String messageTitle) {
        showAlertError(Constants.TIME_AFTER_DELAY, messageTitle, null);
    }

    public void showAlertError(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setIcon(R.drawable.cross);
        alerter.setBackgroundColorRes(R.color.colorError);
        alerter.show();
    }

    public void showAlertError(Integer liveTime, String messageTitle, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (messageTitle != null) {
            alerter.setTitle(messageTitle);
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setIcon(R.drawable.cross);
        alerter.setBackgroundColorRes(R.color.colorError);
        alerter.show();
    }

    public void showAlertError(Integer title) {
        showAlertError(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlertSuccess(Integer liveTime, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        alerter.setIcon(R.drawable.check);
        alerter.setBackgroundColorRes(R.color.colorSuccess);
        alerter.show();
    }

    public void showAlertSuccess(Integer title) {
        showAlertSuccess(Constants.TIME_AFTER_DELAY, title, null);
    }

    public void showAlert(Integer liveTime, Integer color, Integer title, Integer text) {
        if (!Alerter.isShowing()) {
            Alerter.hide();
        }
        Alerter alerter = Alerter.create(getBaseActivity());
        if (title != null) {
            alerter.setTitle(getResources().getString(title));
        }
        if (text != null) {
            alerter.setText(getResources().getString(text));
        }
        if (liveTime != null) {
            alerter.setDuration(liveTime);
        }
        if (color != null) {
            alerter.setBackgroundColorRes(color);
        }
        alerter.show();
    }
}
