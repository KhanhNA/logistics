//
//  DashPotPresenter.swift
//  Hunter
//
//  Created by TheLightLove on 10/02/2020.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

protocol DashPotInputProtocol: BaseMVPProtocol {
    func getInfordashPot(_ fromdate:String,_ todate:String,_ parentMerchantId:Int)
}
protocol DashPotOutPutProtocol: class {
    func didFinshDashPot(_ business: dashPotModel)
    func didFinshDashPotError(error: String? )
}
class DashPotPresenter: DashPotInputProtocol {
    func getInfordashPot(_ fromdate:String,_ todate:String,_ parentMerchantId:Int) {
        let para:Parameters = ["parentMerchantId":parentMerchantId,
                    "fromDate":fromdate,
                    "toDate":todate]
        let router = RouterFromdat.dashPot(para: para)
        APIRequest.requestOk(router, requiredToken: true) { [weak self] (responseModel: dashPotModel?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshDashPotError(error: "lỗi ko có data")
                return
            }
            self?.delegate?.didFinshDashPot(resenponseMD)
        }
        
    }
    
    weak var delegate: DashPotOutPutProtocol?
    func attachView(_ view: Any) {
        delegate = view as? DashPotOutPutProtocol
    }
}
