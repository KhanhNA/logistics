//
//  UpdateStatusPresenter.swift
//  Hunter
//
//  Created by TheLightLove on 10/02/2020.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

protocol UpdateStatusInputProtocol: BaseMVPProtocol {
    func getInforAllMerchant(_ search:searchFromModel)
    func getAllMerchant(_ parentMerchantId:Int,_ keyword:String,_ page:Int)
    func getMerchantsFollowAction(_ parentMerchantId:Int,_ keyword:String,_ page:Int,_ status:Int)
}
protocol UpdateStatusOutPutProtocol: class {
    func didFinshInforAllUser(_ inForAllUser: [DetailInforAllUserModel])
    func didFinshInforAllUserError(error: String? )
}
class UpdateStausPresenter: UpdateStatusInputProtocol {
    func getMerchantsFollowAction(_ parentMerchantId: Int, _ keyword: String, _ page: Int, _ status: Int) {
        let para:Parameters = ["keyword":keyword ,
                               "parentMerchantId":parentMerchantId ,
                               "status":status,
                               "page":page]
        let router = RouterJsonData.searchStatus(para: para)
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<DetailInforAllUserModel>?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshInforAllUserError(error: "lỗi ko có data")
                return
            }
            guard let merchantInfor = resenponseMD.content else {
                self?.delegate?.didFinshInforAllUserError(error: "lỗi ko có data")
                return
            }
            self?.delegate?.didFinshInforAllUser(merchantInfor)
        }
    }
    
    func getAllMerchant(_ parentMerchantId: Int, _ keyword: String, _ page: Int) {
        let para:Parameters = ["keyword":keyword ,
                               "parentMerchantId":parentMerchantId ,
                               "page":page]
        let router = RouterJsonData.searchStatus(para: para)
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<DetailInforAllUserModel>?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshInforAllUserError(error: "lỗi ko có data")
                return
            }
            guard let merchantInfor = resenponseMD.content else {
                self?.delegate?.didFinshInforAllUserError(error: "lỗi ko có data")
                return
            }
            self?.delegate?.didFinshInforAllUser(merchantInfor)
            
        }
    }
    
    weak var delegate: UpdateStatusOutPutProtocol?
    func attachView(_ view: Any) {
        delegate = view as? UpdateStatusOutPutProtocol
    }
    func getInforAllMerchant(_ search:searchFromModel) {
        let para:Parameters = ["keyword":search.keyword ?? "",
                               "parentMerchantId":search.parentMerchantId ?? 0,
                               "status":search.status ?? 0,
                               "fromDate":search.fromDate ?? "",
                               "toDate":search.toDate ?? "",
                               "noRevenue":search.noRevenue ?? 0,
                               "page":search.page ?? 0]
        let router = RouterJsonData.searchStatus(para: para)
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<DetailInforAllUserModel>?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshInforAllUserError(error: "lỗi ko có data")
                return
            }
            guard let merchantInfor = resenponseMD.content else {
                self?.delegate?.didFinshInforAllUserError(error: "lỗi ko có data")
                return
            }
            self?.delegate?.didFinshInforAllUser(merchantInfor)
            
        }
    }
}
