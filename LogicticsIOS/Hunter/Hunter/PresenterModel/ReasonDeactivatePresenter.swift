//
//  ReasonDeactivatePresenter.swift
//  Hunter
//
//  Created by TheLightLove on 11/02/2020.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

protocol DeactivateInputProtocol: BaseMVPProtocol {
    func getInfordashPot(_ sendInfor:SendInforDeactivateModel)
}
protocol DeactivateOutPutProtocol: class {
    func didFinshDeactivate(_ deactive: CodeModel)
    func didFinshDeactiveError(error: String? )
}
class DeactivatePresenter: DeactivateInputProtocol {
    func getInfordashPot(_ sendInfor:SendInforDeactivateModel) {
        let body:Parameters = ["lockEndDate":sendInfor.lockEndDate ?? "",
                               "lockStartDate":sendInfor.lockStartDate ?? "",
                               "merchantId":sendInfor.merchantId ?? 0,
                               "parentMarchantId":sendInfor.parentMarchantId ?? 0,
                               "reason":sendInfor.reason ?? "",
                               "status":sendInfor.status ?? 0,
                               "action":sendInfor.action ?? 0]
        let router = RouterFromdat.updateStatus(body: body)
        APIRequest.requestOk(router, requiredToken: true) { [weak self] (responseModel: CodeModel?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshDeactiveError(error: "lỗi ko có data")
                return
            }
            self?.delegate?.didFinshDeactivate(resenponseMD)
        }
        
    }
    
    weak var delegate: DeactivateOutPutProtocol?
    func attachView(_ view: Any) {
        delegate = view as? DeactivateOutPutProtocol
    }
}
