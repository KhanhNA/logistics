//
//  MatrixInforPresenter.swift
//  Hunter
//
//  Created by TheLightLove on 13/02/2020.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

protocol MatrixInforInputProtocol: BaseMVPProtocol {
    func requestMechants(userName:String)
}
protocol MatrixInforOutputProtocol: class {
    func didFinishError(errorMessage: String?)
    func didFishInforMechants(_ Mechants: UserInforModel)
}
class MatrixInforPresenter: MatrixInforInputProtocol {
    weak var delegate: MatrixInforOutputProtocol?
    func attachView(_ view: Any) {
        delegate = view as? MatrixInforOutputProtocol
    }
    
    func requestMechants(userName: String) {
        let para: Parameters = [
            "userName" : userName
        ]
        let router = RouterFromdat.merchantsUser(para: para)
        APIRequest.requestOk(router, requiredToken: true) { [weak self] (responseModel: UserInforModel?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinishError(errorMessage: "lỗi ko có data")
                return
            }
            self?.delegate?.didFishInforMechants(resenponseMD)
        }
    }
    
}
