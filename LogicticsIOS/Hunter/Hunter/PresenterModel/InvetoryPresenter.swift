//
//  InvetoryPresenter.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 3/10/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation

import Alamofire
protocol InventoryInputProtocol: BaseMVPProtocol {
    func getFromStore(exceptIds: [Int], ignoreCheckPermission: Bool, pageNumber:Int,pageSize:Int,status:Bool,text: String)
    func getAllDistributor()
    func getListInventory(distributorId: Int, pageNumber: Int, pageSize:Int, exceptProductPackingIds:[Int],storeId: Int, text: String )
    func getDetailInventory(expireDate: String, productPackingId: Int, storeId: Int)
}

protocol InventoryOutPutProtocol: class {
    func didFishStore( store: [StoreModel], totalPage:Int)
    func didFishGetAllDistributor(distrubutor: [DistributorModel])
    func didFishGetInventory(inventory:[InventoryModel],totalPage:Int)
    func didFishDetailInventory(inventoryDetail:[InventoryDetailPalletModel])
    func didFishStoreError(error: String)
    
    
}

class InvetoryPresenter: InventoryInputProtocol {
    func getDetailInventory(expireDate: String, productPackingId: Int, storeId: Int) {
        let para:Parameters = ["expireDate":expireDate,
                                "productPackingId":productPackingId,
                                "storeId":storeId]
        let router = Router.getDetailInventory(para: para)
        ProgressHUD.showProgress()
        APIRequest.requestPortal(router, requiredToken: true) { [weak self] (responseModel: [InventoryDetailPalletModel]?) in
            ProgressHUD.hideProgress()
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFishStoreError(error: "lỗi ko có data")
                return
            }

            self?.delegate?.didFishDetailInventory(inventoryDetail: resenponseMD)
        }
    }
    
    func getFromStore(exceptIds: [Int], ignoreCheckPermission: Bool, pageNumber:Int,pageSize:Int,status:Bool,text: String) {
        let para = ["exceptIds":-1,
                    "ignoreCheckPermission":ignoreCheckPermission,
                    "pageNumber":pageNumber,
                    "pageSize":pageSize,
                    "status":status,
                    "text":text] as [String : Any]
        let router = RouterJsonData.getFromStore(para: para)
        ProgressHUD.showProgress()
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<StoreModel>?) in
            ProgressHUD.hideProgress()
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFishStoreError(error: "lỗi ko có data")
                return
            }
            guard let store = resenponseMD.content else {
                self?.delegate?.didFishStoreError(error: "lỗi  data")
                return
            }
            guard let totalPage = resenponseMD.totalPages else {
                self?.delegate?.didFishStoreError(error: "lỗi  data")
                return
            }
            self?.delegate?.didFishStore(store: store,totalPage: totalPage)
        }
    }
    
    func getAllDistributor() {
        let router = RouterJsonData.getAllDistributor
        ProgressHUD.showProgress()
        APIRequest.requestPortal(router, requiredToken: true) { [weak self] (responseModel: [DistributorModel]?) in
            ProgressHUD.hideProgress()
            self?.delegate?.didFishGetAllDistributor(distrubutor: responseModel!)
        }
    }
    
    func getListInventory(distributorId: Int, pageNumber: Int, pageSize: Int, exceptProductPackingIds: [Int], storeId: Int, text: String) {
        let distributorIdRequest = Utils.distributorIdGetAll(distributorId: distributorId)
        let para = ["distributorId":distributorIdRequest,
                    "pageNumber":pageNumber,
                    "pageSize":pageSize,
                    "exceptProductPackingIds":-1,
                    "storeId":storeId,
                    "text":text
        ] as [String:Any]
        let router = RouterJsonData.getInventory(para: para)
        ProgressHUD.showProgress()
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<InventoryModel>?) in
            ProgressHUD.hideProgress()
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFishStoreError(error: "lỗi ko có data")
                return
            }
            guard let inventory = resenponseMD.content else {
                self?.delegate?.didFishStoreError(error: "lỗi  data")
                return
            }
            guard let totalPage = resenponseMD.totalPages else {
                self?.delegate?.didFishStoreError(error: "lỗi  data")
                return
            }
            self?.delegate?.didFishGetInventory(inventory: inventory,totalPage:totalPage )
        }
        
    }
    
    weak var delegate: InventoryOutPutProtocol?
    func attachView(_ view: Any) {
        delegate = view as? InventoryOutPutProtocol
    }
    
}
