//
//  DetailUserPresenter.swift
//  Hunter
//
//  Created by TheLightLove on 11/02/2020.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

protocol DetailUserInputProtocol: BaseMVPProtocol {
    func getInfordashPot(_ fromdate:String,_ todate:String,_ merchantId:Int)
    func getRevenueMonth(merchantId: Int, year: Int,type :Int)
}
protocol DetailUserOutPutProtocol: class {
    func didFinshRevenueMonth(_ detail: [revenueMonthModel])
    func didFinshDetail(_ detail: revenueUserModel)
    func didFinshDetailError(error: String? )
}
class DetailUserPresenter: DetailUserInputProtocol {
    func getInfordashPot(_ fromdate:String,_ todate:String,_ merchantId:Int) {
        let para:Parameters = ["merchantId":merchantId,
                               "fromDate":fromdate,
                               "toDate":todate]
        let router = RouterFromdat.merchantsDashBoard(para: para)
        APIRequest.requestOk(router, requiredToken: true) { [weak self] (responseModel: revenueUserModel?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshDetailError(error: "lỗi ko có data")
                return
            }
            self?.delegate?.didFinshDetail(resenponseMD)
        }
        
    }
    
    func getRevenueMonth(merchantId: Int, year: Int,type :Int) {
        let param = ["merchantId":merchantId,
                     "year":year,
                     "type":type]
        let router = RouterFromdat.merchantRevenue(para: param)
        APIRequest.requestPortal(router, requiredToken: true) { [weak self] (responseModel: [revenueMonthModel]?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinshDetailError(error: "lỗi ko có data")
                return
            }
            self?.delegate?.didFinshRevenueMonth(resenponseMD)
        }
    }
    
    weak var delegate: DetailUserOutPutProtocol?
    func attachView(_ view: Any) {
        delegate = view as? DetailUserOutPutProtocol
    }
}
