//
//  StoreAndDistributorPresenter.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 4/8/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
import Alamofire
protocol StoreAndDistributorInputProto: BaseMVPProtocol {
    func requestGetFromStore(exceptIds: [Int], ignoreCheckPermission: Bool, pageNumber:Int,pageSize:Int,status:Bool,text: String)
    func requestGetAllDistributor()
    
    func requestGetListPo(storeId: Int, distributorId:String,text:String, fromDate:String, toDate:String, dateType:Int, status:Int, pageNumber:Int, pageSize:Int )
    func requestDetailPo(poId:Int)
    
}

protocol StoreAndDistributorOutPutProtocol: class {
    func didFishStore( store: [StoreModel], totalPage:Int)
    func didFishGetAllDistributor(distrubutor: [DistributorModel])
    func getListPoSuccess(lstPo: [PoModel], totalPages:Int)
    func getPoDetailSuccess(poDetail:PoModel)
    func didFishStoreError(error: String)
}

class StoreAndDistributorPresenter:StoreAndDistributorInputProto{
    func requestDetailPo(poId: Int) {
        let para = [:] as [String : Any]
        let router = RouterTest.getPoDetail(para: para)
        ProgressHUD.showProgress()
        APIRequest.requestOk(router, requiredToken: true) { [weak self] (responseModel: PoModel?) in
            ProgressHUD.hideProgress()
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFishStoreError(error: "lỗi ko có data")
                return
            }
            self?.delegate?.getPoDetailSuccess(poDetail: resenponseMD)
        }
    }
    
   
    func requestGetFromStore(exceptIds: [Int], ignoreCheckPermission: Bool, pageNumber: Int, pageSize: Int, status: Bool, text: String) {
         let para = ["exceptIds":-1,
                           "ignoreCheckPermission":ignoreCheckPermission,
                           "pageNumber":pageNumber,
                           "pageSize":pageSize,
                           "status":status,
                           "text":text] as [String : Any]
               let router = RouterJsonData.getFromStore(para: para)
               ProgressHUD.showProgress()
               APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<StoreModel>?) in
                   ProgressHUD.hideProgress()
                   guard let resenponseMD = responseModel else {
                       self?.delegate?.didFishStoreError(error: "lỗi ko có data")
                       return
                   }
                   guard let store = resenponseMD.content else {
                       self?.delegate?.didFishStoreError(error: "lỗi  data")
                       return
                   }
                   guard let totalPage = resenponseMD.totalPages else {
                       self?.delegate?.didFishStoreError(error: "lỗi  data")
                       return
                   }
                   self?.delegate?.didFishStore(store: store,totalPage: totalPage)
               }
    }
    
    func requestGetAllDistributor() {
        let router = RouterJsonData.getAllDistributor
        ProgressHUD.showProgress()
        APIRequest.requestPortal(router, requiredToken: true) { [weak self] (responseModel: [DistributorModel]?) in
            ProgressHUD.hideProgress()
            self?.delegate?.didFishGetAllDistributor(distrubutor: responseModel!)
        }
    }
    
    func requestGetListPo(storeId: Int, distributorId: String, text: String, fromDate: String, toDate: String, dateType: Int, status: Int, pageNumber: Int, pageSize: Int) {
           let para = ["storeId":storeId,
                       "distributorId":distributorId,
                       "text":text,
                       "fromDate":fromDate,
                       "toDate":toDate,
                       "dateType":dateType,
                       "status":status,
                       "pageNumber":pageNumber,
                       "pageSize":pageSize] as [String : Any]
           let router = Router.getListPo(para: para)
           ProgressHUD.showProgress()
           APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<PoModel>?) in
               ProgressHUD.hideProgress()
               guard let resenponseMD = responseModel else {
                   self?.delegate?.didFishStoreError(error: "lỗi ko có data")
                   return
               }
               guard let lstPo = resenponseMD.content else {
                   self?.delegate?.didFishStoreError(error: "lỗi  data")
                   return
               }
               guard let totalPages = resenponseMD.totalPages else {
                   self?.delegate?.didFishStoreError(error: "lỗi  data")
                   return
               }
            self?.delegate?.getListPoSuccess(lstPo: lstPo, totalPages: totalPages)
            
        }
       }
    
    weak var delegate: StoreAndDistributorOutPutProtocol?
    func attachView(_ view: Any) {
        delegate = view as? StoreAndDistributorOutPutProtocol
    }
    
    
}
