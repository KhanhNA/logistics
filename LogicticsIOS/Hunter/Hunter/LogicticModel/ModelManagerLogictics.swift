//
//  ModelManagerLogictics.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 3/10/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation

@objcMembers
class UserModel:NSObject,Codable{
    
    var ref:String?
    var idRef:String?
    var username:String?
    var firstName:String?
    var lastName:String?
    var tel:String?
    var email:String?
    var status:Bool?
    var distributorId:Int?
    var distributor:DistributorModel?
    var stores:[StoreModel]?
    var principal: principalModel?
    
    enum CodingKeys: String, CodingKey {
        case idRef = "@id"
        case username,firstName, lastName, tel,email, status, distributorId, distributor, stores, principal
        case ref = "@ref"
    }
}
//@objcMembers
//class principalModel: NSObject, Codable {
//    var roles: [Roles]?
//}
//
//@objcMembers
//class Roles:NSObjet,Codable {
//    var id: Int?
//    var roleName: String?
//    var description: String?
//    
//    enum CodingKeys: String, CodingKey {
//        case id,roleName, description
//    }
//}



@objcMembers
class CountryModel:NSObject,Codable {
    var ref: String?
    var idRef:String?
    var id:Int?
    var isocode:String?
    var support:Bool?
    var countryDescriptions:[CountryDescriptionModel]?
    
    enum CodingKeys: String, CodingKey {
        case idRef = "@id"
        case id,isocode, support, countryDescriptions
        case ref = "@ref"
    }

}

@objcMembers
class CountryDescriptionModel:NSObject,Codable{
    var ref:String?
    var idRef: String?
    var language: LanguageModel?
    var country:CountryModel?
    var name:String?
    var title:String?
    var descrip:String?
    

    enum CodingKeys: String, CodingKey {
        case idRef = "@id"
        case language,country, name, title
        case ref = "@ref"
        case descrip = "description"
    }
}


@objcMembers
class LanguageModel:NSObject,Codable {
    var ref:String?
    var idRef:String?
    var id:Int?
    var sortOrder:Int?
    var code:String?
    
    enum CodingKeys: String, CodingKey{
        case idRef = "@id"
        case id,sortOrder, code
        case ref = "@ref"
    }
}

@objcMembers
class CurrencyModel:NSObject,Codable {
    var ref:String?
    var idRef:String?
    var code:String?
    var name:String?
    var support:Bool?
    enum CodingKeys: String, CodingKey {
        case idRef = "@id"
        case code, name, support
        case ref = "@ref"
    }
}

@objcMembers
class User:NSObject,Codable {
    var ref:String?
    var idRef:String?
    var username:String?
    var firstName:String?
    var lastName:String?
    var tel:String?
    var email:String?
    var status:Bool?
    var distributorId:Int?
    var distributor:DistributorModel?
   
    enum CodingKeys: String, CodingKey {
        case idRef = "@id"
        case username, firstName, lastName, tel,email,status, distributorId, distributor
        case ref = "@ref"
    }
}

@objcMembers
class DistributorModel:NSObject,Codable {
    var ref:String?
    var idRef:String?
    var id:Int?
    var code:String?
    var name:String?
    var phoneNumber:String?
    var email:String?
    var address:String?
    var enabled:Bool?
    var tin:String?
    
    init(ref:String?,idRef:String?, id:Int?,code:String?,name:String?, phoneNumber:String?, email:String?, address:String?, enabled:Bool?) {
        self.ref = ref
        self.idRef = idRef
        self.id = id
        self.code = code
        self.name = name
        self.phoneNumber = phoneNumber
        self.email = email
        self.address = address
        self.enabled = enabled
    }
    
    enum CodingKeys: String, CodingKey {
        case id, code, name, phoneNumber, email, address, enabled, tin
        case idRef  = "@id"
        case ref = "@ref"
    }
}

@objcMembers
class ProductDescriptionModel:NSObject,Codable{
    var ref:String?
    var idRef:String?
    var product:ProductModel?
    var language:LanguageModel?
    var code:String?
    var name:String?
    
    enum CodingKeys: String, CodingKey {
        case product, language,code,name
        case idRef  = "@id"
        case ref = "@ref"
    }
    
    
}

@objcMembers
class PalletModel:NSObject, Codable {
    var ref:String?
    var idRef:String?
    var store:StoreModel?
    var code:String?
    var qRCode:String?
    var name:String?
    var step:Int?
    var parentPallet:PalletModel?
    var childPallets:[PalletModel]?
    var palletDetails:[PalletDetailModel]?
    var displayName:String?
    
    enum CodingKeys: String, CodingKey {
        case store,code,qRCode, name, step, parentPallet, childPallets, palletDetails, displayName
//        case code ,qRCode, name, step, displayName
        case idRef  = "@id"
        case ref = "@ref"
    }
}

@objcMembers
class ManufacturerModel:NSObject,Codable{
    var ref:String?
    var idRef:String?
    var code:String?
    var name:String?
    var address:String?
    var tel:String?
    var email:String?
    var status:Bool?
  
    enum CodingKeys: String, CodingKey {
           case code,name,address, tel, email, status
           case idRef  = "@id"
           case ref = "@ref"
       }
}

@objcMembers
class ImportPoDetailPalletModel:NSObject,Codable {
    var ref:String?
    var idRef:String?
    var importPo:ImportPoModel?
    var importPoDetail:ImportPoModel?
    var pallet:PalletModel?
    var palletDetail:PalletDetailModel?
    var quantity:Int?
    
    enum CodingKeys: String, CodingKey {
           case idRef = "@id"
           case importPo, importPoDetail, pallet,palletDetail,quantity
           case ref = "@ref"
       }
}

@objcMembers
class ImportPoModel:NSObject,Codable{
    var ref:String?
    var idRef:String?
    var manufacturer:ManufacturerModel?
    var code:String?
    var qRCode:String?
    var descriptionImport:String?
//       private Po po;
    var store:StoreModel?
//    var importPoDetails:[Import]
    var status:Int?
   
    enum CodingKeys: String, CodingKey {
        case idRef = "@id"
        case manufacturer, code, qRCode,store,status
        case ref = "@ref"
        case descriptionImport = "description"
    }
}



@objcMembers
class StoreModel:NSObject, Codable {
    var ref: String?
    var idRef:String?
    var id:Int?
    var country:CountryModel?
    var language:LanguageModel?
    var currency:CurrencyModel?
    var code:String?
    var name:String?
    var domainName:String?
    var inBusinessSince:String?
    var outBusinessSince:String?
    var email:String?
    var logo:String?
//    var floorNo:String?
    var address:String?
    var city:String?
    var phone:String?
    var postalCode:String?
    var dc: Bool?
    var status:Bool?
    var lat:Double?
    var lng:Double?
    var township:String?
    var region:String?
    var area: Float?
    var user: User?
    var provideStore:StoreModel?
    var storeProductPackings:[StoreProductPackingModel]?
   
    
    enum CodingKeys: String, CodingKey {
        case idRef = "@id"
        case id,language, currency, country, code,name,domainName,inBusinessSince,outBusinessSince,email,logo,address,city,phone,postalCode,dc,status,lat,lng,township,region,area, provideStore,  user, storeProductPackings
        case ref = "@ref"
    }
}

@objcMembers
class StoreProductPackingModel:NSObject,Codable{
    var ref:String?
    var idRef:String?
    var store:StoreModel?
    var productPacking:ProductPackingModel?
    var product:ProductModel?
    var totalQuantity:Int?
    //    @JsonFormat(pattern = AppUtils.DATE_FORMAT)
    var expireDate:String?
    
    enum CodingKeys: String, CodingKey {
        case idRef = "@id"
        case store,productPacking, product, totalQuantity, expireDate
        case ref = "@ref"
    }
}

@objcMembers
class InventoryModel:NSObject,Codable{
    var idRef:String?
    var ref:String?
    var id:Int?
    var store:StoreModel?
    var productPackingId: Int?
    var productPacking:ProductPackingModel?
    var product:ProductModel?
    var expireDate:String?
    var totalQuantity:Int?
    var colorClassName:String?
    
    enum CodingKeys: String, CodingKey {
        case id, store,productPackingId,productPacking, expireDate, totalQuantity, product
        case idRef  = "@id"
        case ref = "@ref"
        case colorClassName = "className"
        }
}

@objcMembers
class InventoryDetailPalletModel:NSObject, Codable{
    var ref:String?
    var idRef:String?
    var pallet:PalletModel?
    var quantity:Int = 0
    enum CodingKeys: String, CodingKey {
        case pallet, quantity
        case idRef  = "@id"
        case ref = "@ref"
    }
}

@objcMembers
class ProductTypeModel: NSObject, Codable{
    var ref:String?
    var idRef:String?
    var code:String?
    var name:String?
    var status:Bool?
   
    enum CodingKeys: String, CodingKey {
        case code, name,status
        case idRef  = "@id"
        case ref = "@ref"
    }
}

@objcMembers
class ProductModel:NSObject,Codable{

    var ref:String?
    var idRef:String?
    var productType:ProductTypeModel?
    var code:String?
    var name:String?
    var available:Bool?
    var availableDate:String?
    var quantityOrdered:Int?
    var sortOrder:Int?
    var hot:Bool?
    var totalQuantity:Int?
    var popular:Bool?
    var packingQuantity:Int?
    var packingId:Int?
    var packingCode:String?
    var quantity:Int?
    var productPackings:[ProductPackingModel]?
    var productDescriptions:[ProductDescriptionModel]?
    var manufacturer:ManufacturerModel?
    var distributor:DistributorModel?
    
    enum CodingKeys: String, CodingKey {
        case productType,code, name, available, availableDate,quantityOrdered, sortOrder, hot, totalQuantity, popular,packingQuantity, packingId, packingCode, quantity,productPackings, productDescriptions, manufacturer, distributor
        case idRef  = "@id"
        case ref = "@ref"
    }
}

@objcMembers
class PackingTypeModel:NSObject,Codable{
    var ref:String?
    var idRef:String?
    var code:String?
    var name:String?
    var status:Bool?
    var quantity:Int?
    
    enum CodingKeys: String, CodingKey {
        case code, name, status, quantity
        case idRef  = "@id"
        case ref = "@ref"
    }
}

@objcMembers
class ProductPackingModel:NSObject,Codable{
    var ref:String?
    var idRef:String?
    var id:Int?
    var code:String?
    var name:String?
    var vat:Int?
    var uom:String?
    var barcode:String?
    var product:ProductModel?
    var packingType:PackingTypeModel?
//    var distributor:DistributorModel?
    var status:Bool?
    var productPackingPrices:[ProductPackingPriceModel]?
    var palletDetails:[PalletDetailModel]?
    
    
    enum CodingKeys: String, CodingKey {
        case id,code, name, vat, uom, barcode,product, packingType, status, productPackingPrices, palletDetails
        case idRef  = "@id"
        case ref = "@ref"
    }
}

//@objcMembers
//class PalletModel:NSObject, Codable {
//    var ref:String?
//    var idRef:String?
//    var store:StoreModel?
//    var code:String?
//    var qRCode:String?
//    var name:String?
//    var step:Int?
////    var parentPalvar:PalvarModel?
//    var childPalvars:[PalvarModel]?
//    var palvarDetails:[PalvarDetailModel]?
//    var displayName:String?
//
//
//    enum CodingKeys: String, CodingKey {
//        case store, code, qRCode, name, step,childPalvars, palvarDetails, displayName
//        case idRef  = "@id"
//        case ref = "@ref"
//    }
//
//}

@objcMembers
class ProductPackingPriceModel:NSObject,Codable{
    var ref:String?
    var idRef:String?
    var currency:CurrencyModel?
    var productPacking:ProductPackingModel?
    var price:Double?
    var savarype:String?
    var fromDate:String?
    var toDate:String?
    var status:Bool?
    
    enum CodingKeys: String, CodingKey {
           case productPacking, price, savarype, fromDate, toDate,status, currency
           case idRef  = "@id"
           case ref = "@ref"
       }
}


@objcMembers
class PalletDetailModel:NSObject,Codable {
    var ref:String?
    var idRef:String?
    var pallet:PalletModel?
    var product:ProductModel?
    var productPacking:ProductPackingModel?
    var productPackingPrice:ProductPackingPriceModel?
    var quantity:Int?
    var orgQuantity:Int?
    var expireDate:String?
    var importPoDetailPallet:ImportPoDetailPalletModel?
    var inventory:Int?
   
    
    enum CodingKeys: String, CodingKey {
           case pallet, product, productPacking, productPackingPrice, quantity,orgQuantity, expireDate, inventory
           case idRef  = "@id"
           case ref = "@ref"
       }
}

struct MenuItem{
    var code:String?
    var title:String?
    var image:UIImage?
    var uiController:UIViewController?
}

struct CellData {
    var opened: Bool
    var title: String
    var sectionData: [MenuItem]
}

@objcMembers
class PoModel:NSObject,Codable {
    var ref:String?
    var idRef:String?
    var id:Int?
    var createDate:String?
    var createUser:String?
    var distributor:DistributorModel?
    var manufacturer:ManufacturerModel?
    var descrip:String?
    var code:String?
    var qRCode:String?
    var store:StoreModel?
    var deliveryAddress:String?
    var deliveryDate:String?
    var approveDate:String?
    var status:Int?
    var approveUser:String?
    var importDate:String?
    var importUser:String?
    var poDetails:[PoDetailModel]?
    var importPo:ImportPoModel?
    var total:Decimal?
   
    enum CodingKeys: String, CodingKey {
        case id,createUser,distributor, manufacturer, code, qRCode,store, deliveryAddress, deliveryDate, approveDate, status, approveUser, importDate,importUser, importPo, createDate, poDetails, total
        case idRef  = "@id"
        case ref = "@ref"
        case descrip = "description"
    }
}

@objcMembers
class PoDetailModel:NSObject,Codable {
    var ref:String?
    var idRef:String?
    var id:Int?
    var po:PoModel?
    var product:ProductModel?
    var productPacking:ProductPackingModel?
    var productPackingPrice:ProductPackingPriceModel?
    var quantity:Int?
    var vat:Int?
   
    enum CodingKeys: String, CodingKey {
        case id,po, product, productPacking, productPackingPrice,quantity, vat
        case idRef  = "@id"
        case ref = "@ref"
    }
    
}
