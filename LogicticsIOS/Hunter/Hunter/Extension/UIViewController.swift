//
//  UIViewController.swift
//  Hunter
//
//  Created by TheLightLove on 17/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
class BaseViewController:UIViewController{
    
//    var leftMenuViewController = LeftMenuViewController()
    func setRevealViewController(listCellData: [CellData]) {
        let screenWidth = UIScreen.main.bounds.size.width
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.leftViewWidth = (60*screenWidth)/100
        SlideMenuOptions.rightViewWidth = (60*screenWidth)/100
        SlideMenuOptions.panFromBezel = false
        self.setNavMenuItem(title: "inventory".localized())
        //SlideMenuOptions.animationDuration = 0.2
        SlideMenuOptions.contentViewScale = 1.0
        let leftMenuViewController = LeftMenuViewController(tableView: listCellData)
        let mainViewController = UINavigationController(rootViewController: SearchPoViewController())
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftMenuViewController)
        let navigationController = UINavigationController(rootViewController: slideMenuController)
        navigationController.isNavigationBarHidden = true
        UIApplication.shared.keyWindow?.rootViewController = navigationController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
     func setNavigationController() {
            UINavigationBar.appearance().barTintColor = Helper.COLOR_BLUE_APP
            UINavigationBar.appearance().tintColor = .black
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: Helper.COLOR_TITLE, NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)]
            UINavigationBar.appearance().isTranslucent = false
            UINavigationBar.appearance().shadowImage = UIImage()
            UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        }
        
        func setLeftBarButtonItem(navigationItem: UINavigationItem, index: Int, title: String) {
            let titleBarButtonItem = UIBarButtonItem(title: title.capitalized, style: .plain, target: self, action: nil)
            titleBarButtonItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)], for: .normal)
            navigationItem.leftBarButtonItems![index] = titleBarButtonItem
        }
        
        func setNavMenuItem(title: String) {
            self.setNavigationController()
            let btnBackItemBar = UIBarButtonItem(image: UIImage(named: "menu.png"), style: .plain, target: self, action: #selector(self.onMenuAction))
    //        btnBackItemBar.tintColor = .white
            self.navigationItem.setLeftBarButton(btnBackItemBar, animated: true)
            self.navigationItem.title = title
        }
        
        @objc func onMenuAction() {
//            leftMenuViewController.loadView()
            self.slideMenuController()?.openLeft()
        }
        
        func setNavBackItem(title: String) {
            self.setNavigationController()
            self.navigationController?.isNavigationBarHidden = false
            self.navigationItem.hidesBackButton = true
            let btnBackItemBar = UIBarButtonItem(image: UIImage(named: "back-black"), style: .plain, target: self, action: #selector(self.onBackAction))
            self.navigationItem.setLeftBarButton(btnBackItemBar, animated: true)
            self.navigationItem.title = title
        }
        
        func setNavMapBackItem(title: String) {
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.tintColor = .darkGray
            self.navigationController?.isNavigationBarHidden = false
            self.navigationItem.hidesBackButton = true
            let btnBackItemBar = UIBarButtonItem(image: UIImage(named: "menu.png"), style: .plain, target: self, action: #selector(self.onBackAction))
            
            self.navigationItem.setLeftBarButton(btnBackItemBar, animated: true)
            self.navigationItem.title = title
        }
        
        
        @objc func onBackAction() {
            self.navigationController?.popViewController(animated: true)
        }
        
        func onLogout() {
            let navigationController = UINavigationController(rootViewController: RootViewController())
            navigationController.isNavigationBarHidden = true
            UIApplication.shared.keyWindow?.rootViewController = navigationController
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
        }
    
}
//// MARK: SlideMenuControllerSwift
//extension BaseViewController {
//    func setRevealViewController() {
//        let screenWidth = UIScreen.main.bounds.size.width
//        SlideMenuOptions.hideStatusBar = false
//        SlideMenuOptions.leftViewWidth = (60*screenWidth)/100
//        SlideMenuOptions.rightViewWidth = (60*screenWidth)/100
//        SlideMenuOptions.panFromBezel = false
//        self.setNavMenuItem(title: "inventory".localized())
//        //SlideMenuOptions.animationDuration = 0.2
//        SlideMenuOptions.contentViewScale = 1.0
//        var leftMenuViewController = LeftMenuViewController(nibName: "LeftMenuViewController", bundle: nil)
//        let mainViewController = UINavigationController(rootViewController: InventoryViewController())
//        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftMenuViewController)
//        let navigationController = UINavigationController(rootViewController: slideMenuController)
//        navigationController.isNavigationBarHidden = true
//        UIApplication.shared.keyWindow?.rootViewController = navigationController
//        UIApplication.shared.keyWindow?.makeKeyAndVisible()
//    }
//}
//
//extension UIViewController {
//
//}


