//
//  AcountModel.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation

struct AccountModel: Codable {
    var access_token: String?
    var token_type: String?
    var refresh_token: String?
    var expires_in: Int?
    var scope : String?
}

struct merchantModel:Codable {
    var merchantId: Int?
    var userName:String?
    var merchantTypeId:Int?
    var fullName:String?
    var mobilePhone:String?
    var merchantCode:String?
    var parentMarchantId:Int?
}

@objcMembers
class UserInforModel:NSObject,Codable {
    var merchantId:Int?
    var merchantCode:String?
    var merchantName:String?
    var merchantTypeId:Int?
    var userName:String?
    var fullName:String?
    var merchantImgUrl:String?
    var mobilePhone:String?
    var activeDate:String?
    var status:Int?
    var address:String?
    var tokenFirebase:String?
    var lat:Double?
    var longitude:Double?
    var dateBirth:String?
    var province:String?
    var district:String?
    var village:String?
    var identityImgFront:String?
    var identityImgBack:String?
    var action:Int?
    var userModified:String?
    var attValues:[matrixModel]?
    var groupValues:[groupMerchantsModel]?
}

struct matrixModel:Codable {
    var id:Int?
    var attributeId:Int?
    var name:String?
    var languageId:Int?
    var required:Int?
    var lstValue:[MatrixDetailModel]?
}

struct MatrixDetailModel:Codable {
    var id:Int?
    var name:String?
    var isSelect:Int?
}

struct groupMerchantsModel:Codable {
    var id:Int?
    var code:String?
    var name:String?
    var isSelect:Int?
}

struct mapAttValuesModel:Codable {
    var attId:Int?
    var attValueId:Int?
    var isSelect:Int?
}
