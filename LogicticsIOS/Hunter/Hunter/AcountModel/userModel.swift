//
//  userModel.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation

//struct UserModel: Codable {
//    var name: String?
//    // var lastName: String?
//    var principal: principalModel?
//}
struct principalModel: Codable {
    var roles: [Roles]?
}
struct Roles: Codable {
    var id: Int?
    var roleName: String?
    var description: String?
    var menus: [Menu]?
    var permissions: [PermissionModel]?
}

@objcMembers
class Menu:NSObject,Codable{
    var id:Int?
    var clientId:String?
    var appType:String?
    var code:String?
    var url:String?
    var hasChild:Bool?
    var parentMenu:Menu?
}

@objcMembers
class PermissionModel:NSObject,Codable{
    var idRef:String?
    var id:Int?
    var clientId:String?
    var url:String?
    var descriptions:String?
    var ref:String?
    
    enum CodingKeys: String, CodingKey {
        case id, clientId,url
        case idRef  = "@id"
        case ref = "@ref"
        case descriptions = "description"
    }
}
