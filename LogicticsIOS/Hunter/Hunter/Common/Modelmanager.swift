//
//  Modelmanager.swift
//  Hunter
//
//  Created by TheLightLove on 19/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation

struct RegisterMerchants:Codable {
    var merchantId:Int?
    var merchantCode:String?
    var merchantName:String?
    var parentMarchantId:Int?
    var merchantTypeId:Int?
    var userName:String?
    var password:String?
    var fullName:String?
    var mobilePhone:String?
    var birthday:String?
    var status:Int?
    var address:String?
    var tokenFirebase:String?
    var lat:Double?
    var longitude:Double?
    var area:String?
    var electricalInfrastructure:String?
    var displayDevice:String?
    var refrigerationEquipment:String?
    var nationality:String?
    var province:String?
    var district:String?
    var village: String?
//    var errors:[errorsModel]?
    var identityImgFront:String?
    var identityImgBack:String?
    var message:String?
    var acreage:String?
    var identityNumber:String?
    var identityAddressBy:String?
    var gender:Int?
    var identityCreateDate:String?
    var electricalInfrastructureId:Int?
    var displayDeviceId:Int?
    var refrigerationEquipmentId:Int?
    var acreageId:Int?
    var businessModelId:Int?
//    "contractImg": null
//    "defaultStoreId": null,
//    "tax": 10.0,
//    "merchantImgUrl": null,
//    var activeCode: null,
//    "activeDate": null,
//    "deactiveDate": null,
//    "registerDate": null,
//    "parentMerchant": null,
//    "walletClientId": null,
//    "defaultBankAccountId": null,
//    "defaultBankAccountNo": null,
//    "dateBirth": null,
//    "activeStatus": null,
//    "dateCard": null,
//    "placeCard": null,
//    "displayDevice": null,
}


struct errorsModel:Codable {
    var field:String?
    var type:String?
    var message:String?
}

struct UserReisterModel:Codable {
    var name:String?
    var phone:String?
    var sex:Int?
    var numberCMND:String?
    var birthDay:String?
    var Nationality:String?
    var DateIssuedby:String?
    var CreatDateCMND:String?
    var Issuedby:String?
    var Numberhouse:String?
    var township:String?
    var province:String?
    var lat:String?
    var long:String?
    var methodBussines:String?
    var methodAcreage:String?
    var Infrastructure:String?
    var deviceDisplay:String?
    var refrigerationequipment:String?
}

struct listCustomModel:Codable {
    var merchantId:Int?
    var merchantCode:String?
    var merchantName:String?
    var parentMarchantId:Int?
    var merchantTypeId:Int?
    var userName:String?
    var fullName:String?
    var merchantImgUrl:String?
    var mobilePhone:String?
    var activeCode:String?
    var activeDate:String?
    var registerDate:String?
    var activeStatus:Int?
    var status:Int?
    var lat:Float?
    var longitude:Float?
}

struct GroceryProduct: Codable {
    var title:String?
    var img:String?
    var price:Int?
    private enum CodingKeys: String, CodingKey {
        case title = "tile"
        case img = "img"
        case price = "price"
    }
}

struct GroceryRegister: Codable {
    var address:String?
    var area:String?
    var birthday:String?
    var district:String?
    var electricalInfrastructure:String?
    var merchantTypeId:Int?
    var national:String?
    var parentMarchantId:Int?
    var password:String?
    var province:String?
    var refrigerationEquipment:String?
    var village:String?
    var userName:String?
    var mobilePhone:String?
    private enum CodingKeys: String, CodingKey {
        case address = "address"
        case area = "area"
        case birthday = "birthday"
        case district = "district"
        case electricalInfrastructure = "electricalInfrastructure"
        case merchantTypeId = "merchantTypeId"
        case national = "national"
        case parentMarchantId = "parentMarchantId"
        case password = "password"
        case province = "province"
        case refrigerationEquipment = "refrigerationEquipment"
        case village = "village"
        case userName = "userName"
        case mobilePhone = "mobilePhone"
    }
}

struct FileRegister: Codable {
    var files:String?
    private enum CodingKeys: String, CodingKey {
        case files = "files"
    }
}

struct ImageData {
    var image: UIImage?
    var name: String?
    var fileName: String?
    var mimeType: String?
    
}

struct listBusinessModel:Codable {
    var langId:Int?
    var businessModelId:Int?
    var name:String?
}

struct updateImageMechantInfor:Codable {
    var id:Int?
    var merchantId:Int?
    var url:String?
    var type:Int?
    var contractNumber:String?
}

struct ImageObj:Codable {
    var imgId:String?
    var imgName:String?
}

struct WareHouseModel:Codable {
    var id:Int?
//    var store:store?
    var code:String?
    var name:String?
    var domainName:String?
    var inBusinessSince:String?
    var email:String?
    var address:String?
    var city:String?
    var phone:String?
    var postalCode:String?
}

struct ListCusttomWarehouse:Codable {
    var id:Int?
    var merchantId:Int?
    var storeCode:String?
    var storeName:String?
    var storeAddress:String?
    var city:String?
    var phone:String?
}

struct StoreWareHouse: Codable {
    var city: String?
    var phone: String?
    var postalCode: String?
    var storeAddress:String?
    var storeCode:String?
    var storeName:String?
    private enum CodingKeys: String, CodingKey {
        case city = "city"
        case phone = "phone"
        case postalCode = "postalCode"
        case storeAddress = "storeAddress"
        case storeCode = "storeCode"
        case storeName = "storeName"
    }
}

struct store:Codable {
    var code:String?
    var name:String?
    var domainName:String?
    var inBusinessSince:String?
    var email:String?
    var address:String?
    var city:String?
    var phone:String?
    var postalCode:String?
}

struct OptionBusinessModel:Codable {
//    var TD:[optionModel]?
    var TBDL:[optionModel]?
    var LHKD:[optionModel]?
    var HTD:[optionModel]?
    var TBTB:[optionModel]?
    var DT:[optionModel]?
}

struct optionModel:Codable {
    var id:Int?
    var name:String?
    var commonOptionId:Int?
    var langId:Int?
    var code:String?
    var type:Int?
}

struct dashPotModel:Codable {
    var totalActive:Int?
    var totalInactive:Int?
    var totalMerchantInactiveRevenue:Int?
    var totalOrder:Int?
    var totalRevenue:Double?
    var totalPending:Int?
}

struct searchFromModel:Codable {
    var keyword:String?
    var parentMerchantId:Int?
    var status:Int?
    var fromDate:String?
    var toDate:String?
    var noRevenue:Int?
    var page:Int?
}

struct DetailInforAllUserModel:Codable {
    var merchantId:Int?
    var merchantCode:String?
    var parentMarchantId:Int?
    var merchantTypeId:Int?
    var userName:String?
    var fullName:String?
    var address:String?
    var mobilePhone:String?
    var activeCode:String?
    var activeDate:String?
    var createDate:String?
    var status:Int?
    var walletClientId:Int?
    var nationality:String?
    var identityImgFront:String?
    var identityImgBack:String?
    var gender:Int?
    var identityAddressBy:String?
    var identityNumber:String?
    var action:Int?
}

struct SendInforDeactivateModel:Codable {
    var lockEndDate:String?
    var lockStartDate:String?
    var merchantId:Int?
    var parentMarchantId:Int?
    var reason:String?
    var status:Int?
    var action:Int?
}

struct CodeModel:Codable {
    var code:Int?
    var message:String?
}

struct revenueUserModel:Codable {
   var totalOrder:Int?
   var totalRevenue:Double?
}

struct revenueMonthModel:Codable {
    var month:Int?
    var year:Int?
    var amount:Double?
}
