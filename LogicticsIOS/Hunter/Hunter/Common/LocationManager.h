//
//  LocationManager.h
//  Hunter
//
//  Created by TheLightLove on 18/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//


//@import Firebase;
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@protocol LocationManagerDelegate <NSObject>

- (void)didUpdateLocationFrom:(CLLocationCoordinate2D)fromLocation toLocation:(CLLocationCoordinate2D)toLocation;

@end

typedef void(^DidUpdateLocation)(CLLocation *newLocation,CLLocation *oldLocation,NSError *error);

@interface LocationManager : NSObject <CLLocationManagerDelegate>{
    DidUpdateLocation blockDidUpdate;
}

@property (strong, nonatomic) CLLocationManager * anotherLocationManager;
@property (weak, nonatomic) id<LocationManagerDelegate> delegate;

//@property (nonatomic) CLLocation *lastLocation;
@property (nonatomic) BOOL updateOn;

+ (instancetype)sharedManager;

- (void)startMonitoringLocation;
- (void)restartMonitoringLocation;


@end
