//
//  APIKey.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation
typealias CSParamater = [APIKey:Any]

enum APIKey: String {
    case page               = "page"
    case langId             = "langId"
    case merchantId         = "merchantId"
    case orderType          = "orderType"
    case entityName         = "entityName"
    case keyWord            = "keyWord"
    case parentMerchantId   = "parentMerchantId"
    case id
    case code
    case statusCode         = "status_code"
    case message
    case userName           = "username"
    case password           = "password"
    case confirmPassword    = "confirm_password"
    case grantType          = "grant_type"
    case clientId           = "client_id"
    case clientSecret       = "client_secret"
    case deviceToken        = "device_token"
    case operationSystem    = "os"
    case email
    case verificationCode = "verification_code"
    case birthday
    case phoneNumber = "phone_number"
    case firstName = "first_name"
    case lastName = "last_name"
    case gender
    case address
    case parentId = "parent_id"
    case timeRequest = "time_request"
    case newPass = "new_password"
    case city_name = "city_name"
    case userId = "user_id"
    case icon
    case prefecture
    case city
    case street
    case house_number
    case town
    case prefCode
    case notificationId = "notification_id"
    
    //Tweet Request
    case type           = "type"
    case limit          = "limit"
    case status         = "status"
    case offset
    case name           = "created_by"
    case timePost       = "created_at"
    case photoUrls      = "images"
    case categoryId     = "category_id"
    case categoryProductId     = "categoryId"
    case genreId        = "genre"
    case description    = "content"
    // Review
    case advertisement  = "advertisement"
    case advertisementId  = "advertisement_id"
    case latestContent  = "latestContent"
    case text           = "text"
    case rate           = "rate"
    case couponId       = "coupon_id"
}

extension Dictionary where Key == APIKey{
    var asParamater: [String: Any] {
        var param = [String: Any]()
        self.forEach { (key, value) in
            param[key.rawValue] = value
        }
        return param
    }
}

