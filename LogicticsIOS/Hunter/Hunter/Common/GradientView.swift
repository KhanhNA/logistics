//
//  GradientView.swift
//  Hunter
//
//  Created by TheLightLove on 12/02/2020.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor: UIColor = AppColor.Primary {
        didSet {
            updateGradientColor()
        }
    }
    
    @IBInspectable var endColor: UIColor = UIColor(rgb: 0xFF9064) {
        didSet {
            updateGradientColor()
        }
    }
    
    @IBInspectable var angle: Int = 135 {
        didSet {
            if angle > 360 || angle < -360 {
                angle = angle % 360
            }
            
            if angle < 0 {
                angle += 360
            }
            
            updateGradientPoints()
        }
    }
    
    @IBInspectable var colorRatio: Float = 0.5 {
        didSet {
            if colorRatio < 0 {
                colorRatio = 0
            } else if colorRatio > 1 {
                colorRatio = 1
            }
            updateGradientLocation()
        }
    }
    
    @IBInspectable var fadeIntensity: Float = 1.0 {
        didSet {
            if fadeIntensity < 0 {
                fadeIntensity = 0
            } else if fadeIntensity > 1 {
                fadeIntensity = 1
            }
            updateGradientLocation()
        }
    }
    
    @IBInspectable var shadowColor: UIColor? = UIColor(rgb: 0x9E3332) {
        didSet {
            gradientLayer.shadowColor = shadowColor?.cgColor
        }
    }
    
    @IBInspectable var shadowOffsetX: CGFloat = 0 {
        didSet {
            gradientLayer.shadowOffset = CGSize(width: shadowOffsetX, height: shadowOffsetY)
        }
    }
    
    @IBInspectable var shadowOffsetY: CGFloat = 4 {
        didSet {
            gradientLayer.shadowOffset = CGSize(width: shadowOffsetX, height: shadowOffsetY)
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 3 {
        didSet {
            gradientLayer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 1 {
        didSet {
            if shadowOpacity > 1 {
                shadowOpacity = 1
            } else if shadowOpacity < 0 {
                shadowOpacity = 0
            }
            gradientLayer.shadowOpacity = shadowOpacity
        }
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    private var isOpacityGradient: Bool {
        get {
            return startColor.isEqual(UIColor.clear) || endColor.isEqual(UIColor.clear)
        }
    }
    
    private var opacityGradientColor: UIColor {
        get {
            if startColor.isEqual(UIColor.clear) == false {
                return startColor
            } else if endColor.isEqual(UIColor.clear) == false {
                return endColor
            } else {
                return UIColor.white
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        updateGradientColor()
        updateGradientPoints()
        updateGradientLocation()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateGradientColor()
        updateGradientPoints()
        updateGradientLocation()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientColor()
    }
    
    private var gradientLayer: CAGradientLayer {
        return self.layer as! CAGradientLayer
    }
    
    private func updateGradientColor() {
        if isOpacityGradient {
            gradientLayer.colors = nil
            gradientLayer.backgroundColor = opacityGradientColor.cgColor
            
            let maskLayer = CAGradientLayer()
            maskLayer.frame = bounds
            if startColor.isEqual(UIColor.clear) == false {
                maskLayer.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
            } else {
                maskLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
            }
            let locations = self.locations()
            maskLayer.locations = [NSNumber(value: locations.0), NSNumber(value: locations.1)]
            let points = gradientPoints()
            maskLayer.startPoint = points.start
            maskLayer.endPoint = points.end
            gradientLayer.mask = maskLayer
        } else {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        }
    }
    
    private func updateGradientPoints() {
        guard isOpacityGradient == false else {
            return
        }
        let points = gradientPoints()
        gradientLayer.startPoint = points.start
        gradientLayer.endPoint = points.end
    }
    
    private func updateGradientLocation() {
        guard isOpacityGradient == false else {
            return
        }
        let locations = self.locations()
        gradientLayer.locations = [NSNumber(value: locations.0), NSNumber(value: locations.1)]
    }
    
    // MARK: - Helper methods
    
    private func gradientPoints() -> (start: CGPoint, end: CGPoint) {
        var rotateX: CGFloat = 0.0
        var rotateY: CGFloat = 0.0
        
        let rotate = CGFloat(angle) / 90.0
        switch rotate {
        case 0...1:
            rotateY = rotate
        case 1...2:
            rotateY = 1
            rotateX = rotate - 1
        case 2...3:
            rotateX = 1
            rotateY = 1 - (rotate - 2)
        case 3...4:
            rotateX = 1 - (rotate - 3)
        default:
            break
        }
        
        let start = CGPoint(x: 1 - rotateY, y: 0 + rotateX)
        let end = CGPoint(x: 0 + rotateY, y: 1 - rotateX)
        return (start, end)
    }
    
    private func locations() -> (Float, Float) {
        let divider = fadeIntensity / self.divider()
        return (colorRatio - divider, colorRatio + divider)
    }
    
    private func divider() -> Float {
        if colorRatio < 0.5 {
            return 1 / colorRatio
        }
        return 1 / (1 - colorRatio)
    }
    
    func setGradientColor(startColor: UIColor, endColor: UIColor) {
        self.startColor = startColor
        self.endColor = endColor
    }
}

