//
//  Common.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation
import UIKit

class Const {
     static let PROJECT_NAME                 = "TSolution"
        
//    static let BASE_URL                     = "http://192.168.1.69:9999"
//    static let URL_GETDATA                  = "http://192.168.1.69:8888"
//    static let GETTOKEN                     = "http://Logistics:XY7kmzoNzl100@192.168.1.69:9999/oauth/token"
    
//        static let BASE_URL                     = "http://demo.nextsolutions.com.vn:9999"
//        static let URL_GETDATA                  = "http://dev.nextsolutions.com.vn:8888"
//        static let GETTOKEN                     = "http://Logistics:XY7kmzoNzl100@dev.nextsolutions.com.vn:9999/oauth/token"
    
    static let BASE_URL                     = "http://cloud.nextsolutions.com.vn:9999"
    static let URL_GETDATA                  = "http://cloud.nextsolutions.com.vn:8888"
    static let GETTOKEN                     = "http://Logistics:XY7kmzoNzl100@cloud.nextsolutions.com.vn:9999/oauth/token"
        
        //public
    //    static let BASE_URL                     = "http://demo.nextsolutions.com.vn:9999"
    //    static let URL_GETDATA                  = "http://dev.nextsolutions.com.vn:8888"
    //    static let GETTOKEN                     = "http://Logistics:XY7kmzoNzl100@dev.nextsolutions.com.vn:9999/oauth/token"
        static let IMAGEFILES                   = "http://dev.nextsolutions.com.vn:8234/api/v1/files/"
        static let IMAGEFILESHeader             = "http://dev.nextsolutions.com.vn:8234/api/v1/files/"

    
    static let PAGE_SIZE:Int = 20
}

struct AppFormatters {
    static let debugConsoleDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd HH:mm:ss.SSS"
        formatter.timeZone = TimeZone(identifier: "UTC")!
        return formatter
    }()
    
    static let baseDateFormate = "HH:mm:ss"
    static let dateJson                 = "yyyy-MM-dd HH:mm:ss.SSS'Z'"
    static let ISO8601                  = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
    static let newsFormat               = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
    static let mainFormat               = "yyyy-MM-dd'T'HH:mm:ss.SSSSS"
    static let timeSaleFormat           = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    static let JPYMDWTimeFormat         = "yyyy年MM月dd日(EEEEE) HH:mm"
    static let dateTimeSale             = "配信期間yyyy.MM.dd HH:mmまで"
    static let convertSeverDateFormater = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let timeSaleServerFormater   = "yyyy-MM-dd'T'HH:mm:ss"
    static let JPShortWithTimeFormat    = "MM月dd日(EEEEE) HH:mm"
    static let fullFormat               = "dd/MM/yyyy HH:mm:ss"
    static let dateNews                 = "yyyy年MM月dd日 HH:mm"
    static let dateTimeFormat           = "dd/MM/yyyy HH:mm"
    static let dateTimeSaleCoupon       = "yyyy年MM月末口まで"
    static let JPShortYearFormat        = "yyyy年MM月dd日(E)"
    static let JPMDHS                   = "MM月dd日 HH:mm"
    static let dateRegisterFormat       = "yyyy年MM月dd日"
    static let tireImageDateFormat      = "yyyyMMddHHmm"
    static let dateDetailNews           = "MM月dd日昼刊"
    static let JPShortFormat            = "MM月dd日(E)"
    static let nomarlFormat             = "dd/MM/yyyy"
    static let dateSeverFormat          = "yyyy-MM-dd"
    static let shortFormat              = "yyyyMMdd"
    static let JPdateMonth              = "MM月dd日"
    static let dateMonth                = "MM/dd"
    static let hourMinuteFormat         = "HH:mm"
    static let nomarlFormatYear         = "yyyy/MM/dd"
}

enum MessageType: String {
    case warnning = "警報"
    case success  = "成功"
    case error    = "エーラー"
    
    var key: String {
        return String(describing: self)
    }
    
    static func icon(with str: String) -> String {
        switch str {
        case "warnning"   :   return "warn"
        case "success"    :   return "success"
        case "error"      :   return "error"
        default:
            return ""
        }
    }
}


// HTTP status code
enum ResultCode: Int, Codable {
    case httpSuccess = 200
    case httpCreated = 201
    case httpAccepted = 202
    case httpNoContent = 204
    case unknow = 999
    case httpInternalError = 500
    case httpBadRequest = 400
    case httpNotFound = 404
    case httpForbidden = 403
    case httpUnauthorized = 401
    case success = 1
    case error = 2
    case warning = 3
    case notFoundObj = 4
    case bodyInvalid = 5
    case paramInvalid = 6
    case noData = 7
    case codeExist = 20
    case emailExist = 21
    case phoneExist = 22
    case passwordWrong = 23
    case empApprovedOrder = 24
    case empBusy = 25
    
    var message: String {
        switch self {
        case .httpBadRequest, .httpNotFound:
            return "No Connection"
        default:
            return ""
        }
    }
    
    var isApiSuccess: Bool {
        return rawValue >= 200 && rawValue < 300
    }
}

struct Screen {
    static var height: CGFloat {
        return UIScreen.main.bounds.size.height
    }
    static var width: CGFloat {
        return UIScreen.main.bounds.size.width
    }
    static let hIP4: CGFloat = 480
    static let hIP5E: CGFloat = 568
    static let hIP6: CGFloat = 667
    static let hIPPlus: CGFloat = 736
    static let hIPX: CGFloat = 812
    static let hIPXR: CGFloat = 896
    static let hIPXS: CGFloat = 812
    static let hIPXSMax: CGFloat = 896
    static let hIPadMini: CGFloat = 1024
    static let hIPadAir: CGFloat = 1024
    static let hIPadPro105: CGFloat = 1112
    static let hIPadPro129: CGFloat = 1336
    
    static var isIphoneX: Bool {
        return height == hIPX
    }
    
    static var isGreatThanIphoneX: Bool {
        return height >= hIPX && height <= hIPXSMax
    }
    
    static var isIpad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static var isIPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
}

struct ColorHex {
    static let f2a66a                  = "f2a66a"
    static let e76b85                  = "e76b85"
    static let ea7184                  = "ea7184"
    static let ea6b88                  = "ea6b88"
    static let ea9847                  = "ea9847"
    static let cd853f                  = "cd853f"
    static let cc3232                  = "cc3232"
    static let hex32a54a               = "32a54a"
    static let bmi59d3df               = "59d3df"
    static let bmi5cb775               = "5cb775"
    static let bmiebda4e               = "ebda4e"
    static let bmiee5a30               = "ee5a30"
    static let bmieb5684               = "eb5684"
    static let bmic2372d               = "c2372d"
}
struct typeScreen {
    static let nextTarger              = "nextTarger"
    static let perfectTager            = "perfectTager"
}
struct BMIMessage {
    static let bmiMessge1              = "現在の肥満度は"
    static let bmiMessge2              = "目標の肥満度は"
}
struct BMiContent {
    static let bmiType0                = "「低体重」です。"
    static let bmiType1                = "「普通体重」です。"
    static let bmiType2                = "「肥満（1度）」です。"
    static let bmiType3                = "「肥満（2度）」です。"
    static let bmiType4                = "「肥満（3度）」です。"
    static let bmiType5                = "「肥満（4度）」です。"
}
struct BMIType {
    static let bmiType0                = "現在の肥満度は「低体重」です。"
    static let bmiType1                = "現在の肥満度は「普通体重」です。"
    static let bmiType2                = "現在の肥満度は「肥満（1度）」です。"
    static let bmiType3                = "現在の肥満度は「肥満（2度）」です。"
    static let bmiType4                = "現在の肥満度は「肥満（3度）」です。"
    static let bmiType5                = "現在の肥満度は「肥満（4度）」です。"
}

struct MessageError {
    static let sexEmpty                = "性別を選択してください。"
    static let userEmpty               = "名前を入力してください。"
    static let furiganaEmpty           = "フリガナを入力してください。"
    static let characterPassword       = "新パスワードは６文字以上にしてください。"
    static let confirmPassword         = "新パスワード確認が違います。"
    static let agreeTeam               = "「ご利用規約」および「プライバシーポリシー」への同意が必要です。内容をご確認の上、ご同意いただける場合、チェックを入れて、次の画面へお進みください。"
    static let heightEmpty             = "身長を入力してください。"
    static let weightEmpty             = "体重を入力してください。"
}

struct WebViewURL {
    static let privacy                  = "https://tavenal.com/privacy"
    static let tos                      = "https://smart-me.jp/tos"
}

struct Key {
    static let appName                  = "appName"
    static let nameCurrentUser          = "nameCurrentUser"
    static let deviceToken              = "deviceToken"
    static let fcmToken                 = "fcmToken"
    static let sessionId                = "sessionId"
    static let titleHistoryBooking      = "titleHistoryBooking"
    static let roomCurrent              = "roomCurrent"
    static let isFirstLaunchApp         = "isFirstLaunchApp"
    static let currentHashPassword      = "currentHashPassword"
    static let firstRunInstalled        = "1stRunInstalled"
    static let titleHistorySearchAds    = "titleHistorySearchAds"
}

enum DeviceType {
    case iPhone5 // 5, 5s, 5c
    case iPhone8 // 6, 6s, 7, 8
    case iPhone8Plus // 6+, 6s+, 7+, 8+
    case iPhoneX // x, xs
    case iPhoneXR // xr
    case iPhoneXSMax // xs max
    case unknown
    case iPad
}

struct DateFormat  {
    static let birthDay                 =          "yyyy-MM-dd"
}

struct Regex {
    static let emailRegEx    = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,}"
    static let phoneRegEx    = "^[0]{1}[1-9]{1}[0-9]{8,9}$" // VietName PhoneNumber
}

struct DateUtil {
    static func compareDate(dateOne:String?, dateTwo:String?) -> Int{
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = AppFormatters.nomarlFormat
        //        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        let firstDate:Date = dateFormatter.date(from: dateOne!)!
        let seconDate:Date = dateFormatter.date(from: dateTwo!)!
        
        if (firstDate > seconDate) {
            return 1
        } else if (firstDate < seconDate) {
            return -1
        }else {
            return 0
        }
    }
    
    static func convertDateToRequest(_ dateString: String) -> String {
        if dateString.isEmpty {
            return ""
        }else {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = AppFormatters.nomarlFormat
            let date = dateFormatter.date(from: dateString)
            dateFormatter.dateFormat = AppFormatters.dateJson
            return dateFormatter.string(from: date!)
        }
    }
    
    static func convertDateFromJson(_ dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = AppFormatters.dateJson
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = AppFormatters.nomarlFormat
        return dateFormatter.string(from: date!)
    }
}
