//
//  HelperRequest.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation
import UIKit


func appDelegate () -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

// Define log function - start
func DEBUGLog(_ message: String,
              file: String = #file,
              line: Int = #line,
              function: String = #function) {
    #if DEBUG
    let fileURL = NSURL(fileURLWithPath: file)
    let fileName = fileURL.deletingPathExtension?.lastPathComponent ?? ""
    print("DEBUG: \(Date().dblog()) \(fileName)::\(function)[L:\(line)] \(message)")
    #endif
    // Nothing to do if not debugging
}

func ERRORLog(_ message: String,
              file: String = #file,
              line: Int = #line,
              function: String = #function) {
    #if DEBUG
    let fileURL = NSURL(fileURLWithPath: file)
    let fileName = fileURL.deletingPathExtension?.lastPathComponent ?? ""
    print("ERROR: \(Date().dblog()) \(fileName)::\(function)[L:\(line)] \(message)")
    #endif
    // Nothing to do if not debugging
}

public func LS(_ key: String,
               tableName: String? = nil,
               bundle: Bundle = Bundle.main,
               value: String = "",
               comment: String = "") -> String {
    return NSLocalizedString(key, tableName: tableName, bundle: bundle, value: value, comment: value)
}

class SessionManagerCodale {
    static let shared = SessionManagerCodale()
    var accountModel: AccountModel? {
        get {
            return self.loadCurrentUserFromKeyChain()
        }
    }
    
    internal func loadCurrentUserFromKeyChain() -> AccountModel? {
        let keychain = KeyChainService(account: AccountName.token.rawValue)
        guard let dataKeyChain = try? keychain.readKeyChain(),
            let data = dataKeyChain.data(using: String.Encoding.utf8) else { return nil }
        let instance = try? JSONDecoder().decode(AccountModel.self, from: data)
        return instance
    }
    
    internal func saveToUserDefault(accountModel: AccountModel) {
        let keychain = KeyChainService(account: AccountName.token.rawValue)
        guard let data = try? JSONEncoder().encode(accountModel) else { return }
        guard let jsonString = String(data: data, encoding: .utf8) else { return }
        try? keychain.saveKeyChain(jsonString)
    }
}
