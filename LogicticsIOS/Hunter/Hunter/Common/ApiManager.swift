//
//  ApiManager.swift
//  Hunter
//
//  Created by TheLightLove on 23/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit
import Alamofire
import AssetsLibrary
import Photos
import SwiftyJSON

class ApiManager: NSObject {
    static let share = ApiManager()
    func requestData(parameters: Parameters, path: String, listImage: [ImageData], completion: @escaping(_ success: Bool,_  response: [String: Any]?) -> Void) {
        let url = String(format: "%@%@", Const.URL_GETDATA, APIPath.regiterMerchants)
        print(url)
        let urlA = URL(string: url)!
        var urlRequest = URLRequest(url: urlA)
//        let body = NSMutableData();
        let boundary = "---------------------------14737809831466499882746641449"
        let contentType = "multipart/form-data; boundary=\(boundary)"
        urlRequest.addValue(contentType, forHTTPHeaderField: "Content-Type")
        //urlRequest.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")  // the request is JSON
        urlRequest.setValue("*/*", forHTTPHeaderField: "Accept")
        urlRequest.httpMethod = "POST"
        urlRequest.addValue(contentType, forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")  // the request is JSON
        urlRequest.setValue("*/*", forHTTPHeaderField: "Accept")
        urlRequest.httpMethod = "POST"
        
        let header: [String:String] = ["Content-Type": "multipart/form-data,multipart/form-data; boundary=--------------------------210309060261705899119955"]
        AlamofireManager.shared.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName:key )
            }
            
            for imageData in listImage {
                if let image = imageData.image?.jpegData(compressionQuality: 1.0) {
                    multipartFormData.append(image, withName: "files", fileName: String(format: "%@.jpg", imageData.fileName!), mimeType: imageData.mimeType!)
                }
            }
        }, to: url, method: .post, headers: header) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("uploading")
                })
                upload.responseJSON { response in
                    switch(response.result) {
                    case .success(_):
                        if let response = response.result.value as? [String: Any] {
                            completion(true, response)
                        } else {
                            completion(false, nil)
                        }
                        break
                    case .failure(_):
                        print("url.....: failure: \(response.error!)")
                        completion(false, nil)
                        break
                    }
                }
                
            case .failure(let encodingError):
                print("url.....: failure: \(encodingError)")
                completion(false, nil)
            }
        }
    }
    
    func requestParams(parameters: Parameters, path: String, completion: @escaping(_ success: Bool, _ response: [String: Any]?) -> Void) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
//        let url = String(format: "%@%@", Const.URL_GETDATA, APIPath.regiterMerchants)
        
        let urlStr = String(format: "%@%@", Const.URL_GETDATA, APIPath.regiterMerchants)
        let url = URL(string: urlStr)!
        let paraData = try! JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
        let paraJson = NSString(data: paraData, encoding: String.Encoding.utf8.rawValue)
        print("url.....:  \(urlStr) \(paraJson ?? "=> nil")")
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
//        request.addValue(TOKEN_BACKEND, forHTTPHeaderField: "Authorization")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = paraJson!.data(using: String.Encoding.utf8.rawValue)
        
        AlamofireManager.shared.request(request).responseJSON { (response) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch(response.result) {
            case .success(_):
                if let response = response.result.value as? [String: Any] {
                    completion(true, response)
                } else {
                    completion(false, nil)
                }
                break
                
            case .failure(_):
                print("url.....: failure: \(response.error!)")
                completion(false, nil)
                break
            }
        }
    }

    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        let url = String(format: "%@%@", Const.URL_GETDATA, APIPath.regiterMerchants)
        let boundary = "---------------------------14737809831466499882746641449"
        let contentType = "multipart/form-data; boundary=\(boundary)"
        let headers: HTTPHeaders = ["Accept":"*/*",
                                    "Content-Type":contentType]
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key,value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
//            multipartFormData.append(data, withName: "avatar", fileName: fileName!,mimeType: "image/jpeg")
        },
                usingThreshold: UInt64.init(),
                to: url,
                method: .post,headers: headers,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        print(response)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
        
    }
    
    func requestDataPath(user:UserReisterModel , path: String, listImage: [ImageData], completion: @escaping(_ success: Bool,_ response: [String: Any]?) -> Void) {
        let url = String(format: "%@%@", Const.URL_GETDATA, APIPath.regiterMerchants)
        let headers: HTTPHeaders = ["Accept":"*/*",
                                    "Content-Type":"multipart/form-data",
                                    "Content-Disposition":"form-data"]
        let str = """
        {
        "fullName": "\(user.name!)",
        "merchantName": "\(user.name!)",
        "merchantTypeId": 1,
        "national": "\(user.Nationality!)",
        "parentMarchantId":0,
        "userName":"\(user.phone!)",
        "mobilePhone":"\(user.phone!)"
        }
        """
        print("stra:\(str)")
        let postDict: [String: Any] = ["merchant": str]
        AlamofireManager.shared.upload(multipartFormData: { (multipartFormData) in
//            for (key, value) in postDict {
                multipartFormData.append(str.data(using: .utf8)!, withName: "merchant")
//            }
//            for imageData in listImage {
//                if let image = imageData.image?.jpegData(compressionQuality: 1.0) {
//                    multipartFormData.append(image, withName: imageData.name!, fileName: String(format: "%@.jpg", imageData.fileName!), mimeType: imageData.mimeType!)
//                }
//            }
            
        }, to: url,method: .post,headers:headers ) { (result) in
            switch result {
            case .success(let upload,_ ,_ ):
                upload.uploadProgress(closure: { (Progress) in
                    print("uploading")
                })
                upload.responseJSON { response in
                    switch(response.result) {
                    case .success(_):
                        if let response = response.result.value as? [String: Any] {
                            completion(true, response)
                        } else {
                            completion(false, nil)
                        }
                        break
                        
                    case .failure(_):
                        print("url.....: failure: \(response.error!)")
                        completion(false, nil)
                        break
                    }
                }
                
            case .failure(let encodingError):
                print("url.....: failure: \(encodingError)")
                completion(false, nil)
            }
        }
    }

    func requestWithData(user: UserReisterModel, imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = String(format: "%@%@", Const.URL_GETDATA, APIPath.regiterMerchants)

        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        let str:NSDictionary = [
        "fullName": "\(user.name!)",
        "merchantName": "\(user.name!)",
        "merchantTypeId": 1,
        "national": "\(user.Nationality!)",
        "parentMarchantId":0,
        "userName":"\(user.phone!)",
        "mobilePhone":"\(user.phone!)"]
        let postDict: [String: Any] = ["merchant": str]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in postDict {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
}

