//
//  BaseMVPProtocol.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit

protocol BaseMVPProtocol {
    func attachView(_ view: Any)
}

protocol StoryboardBased: class {
    static var storyboard: UIStoryboard { get }
}

extension Storyboardable {
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: String(describing: self), bundle: nil)
    }
}

extension StoryboardBased where Self: BaseViewController {
    static func instantiate() -> Self {
        guard let vc = storyboard.instantiateInitialViewController() as? Self else {
            fatalError("The VC of \(Self.className) is not of class \(self)")
        }
        return vc
    }
}

protocol presenter {
    associatedtype T
    var view: T? { get set }
    
    func attachView(view: T?)
    func detach()
}

protocol Presenter: class {
    associatedtype T
    var view: T? { get set }
    
    func attachView(view: T?)
}

extension Presenter {
    func attachView(view: T?) {
        self.view = view
    }
}

class BasePresenter<K>: Presenter {
    typealias T = K
    var view: K?
    
    func attachView(view: K?) {
        self.view = view
    }
}

protocol BaseVCProtocol: class {
    associatedtype P: Presenter
    var presenter: P { get set }
    
    func createPresenter() -> P
}

protocol Storyboardable {
    static var storyboardName: String { get }
    static var identifier: String { get }
}

extension UIStoryboard {
    static func loadViewController<T: Storyboardable>(_ classType: T.Type) -> T {
        guard let vc = UIStoryboard(name: classType.storyboardName,
                                    bundle: nil).instantiateViewController(withIdentifier: classType.identifier) as? T else {
                                        fatalError("Cannot instantiate view controller: \(classType.identifier)")
        }
        return vc
    }
}

extension Storyboardable where Self: BaseViewController {
    static var identifier: String {
        return String(describing: self)
    }
}

extension NSObject {
    static var className: String {
        return String(describing: self)
    }
    
    // For Instance of class get name
    var className: String {
        return String(describing: type(of: self))
    }
}
