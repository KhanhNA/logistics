//
//  Utils.swift
//  Hunter
//
//  Created by TheLightLove on 28/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation
import PopupDialog

class Utils {
    static func distributorIdGetAll(distributorId:Int) -> String{
        let distributorIdRequest:String
        if distributorId == 0{
            distributorIdRequest = ""
            return distributorIdRequest
        }
        distributorIdRequest = String(distributorId)
        return distributorIdRequest
    }
    
    static func setfileImage(stringUrl:String) -> String{
        var imgStr:String = ""
        imgStr = imgStr + Const.IMAGEFILES + stringUrl
        return imgStr
    }
    
    func statusbar() -> UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    static func convertObjDistributortoString(arr:[DistributorModel]) -> [String] {
        var arrString = [String]()
        for item in arr {
            if "".equal(item.name ?? ""){
                arrString.append("\(item.code!)")
            }else{
                arrString.append("\(item.code!) - \(item.name!)")
            }
            
        }
        return arrString
    }
    
    static func convertStoretoString(arr:[StoreModel]) -> [String] {
        var arrString = [String]()
        for i in arr{
            let nameDC:String = (i.code ?? "") + "-" + (i.name ?? "")
            arrString.append(nameDC)
        }
        return arrString
    }
    
    static func formatCurrency(_ price:Double) -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        return numberFormatter.string(from: price as NSNumber)!
    }
    
   
    
    func getCurrentTimeStampWOMiliseconds(dateToConvert: NSDate) -> String {
        let objDateformat: DateFormatter = DateFormatter()
        objDateformat.dateFormat = "yyyy-MM-dd"
        let strTime: String = objDateformat.string(from: dateToConvert as Date)
        let objUTCDate: NSDate = objDateformat.date(from: strTime)! as NSDate
        let milliseconds: Int64 = Int64(objUTCDate.timeIntervalSince1970)
        let strTimeStamp: String = "\(milliseconds)"
        return strTimeStamp
    }
    
    static func popUpDialog(title:String?, message:String?,uiVC:UIViewController?){
    // Create the dialog
    let popup = PopupDialog(title: title, message: message, image: nil)

//    // Create buttons
    let buttonOne = CancelButton(title: "CANCEL") {
        print("You canceled the car dialog.")
    }
//
//    // This button will not the dismiss the dialog
//    let buttonTwo = DefaultButton(title: "ADMIRE CAR", dismissOnTap: false) {
//        print("What a beauty!")
//    }
//
//    let buttonThree = DefaultButton(title: "BUY CAR", height: 60) {
//        print("Ah, maybe next time :)")
//    }
//
//    // Add buttons to dialog
//    // Alternatively, you can use popup.addButton(buttonOne)
//    // to add a single button
    popup.addButtons([buttonOne])

    // Present dialog
        uiVC?.present(popup, animated: true, completion: nil)
    }
    
}

extension BaseViewController {
    func shadowViewControll(_ shadowView:UIView) -> UIView {
        shadowView.layer.cornerRadius = 0
        shadowView.layer.shadowColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        shadowView.layer.shadowRadius = 1.7
        shadowView.layer.shadowOpacity = 0.45
        return shadowView
    }
}

extension String {
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
}
