//
//  x
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class Connectivity {
    class var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

struct BaseResponseModel<M:Decodable>:Decodable {
    var data:M?
    var content:[M]?
    var userAuthentication: M?
    private enum CodingKeys: String, CodingKey {
        case data
        case userAuthentication
        case content
    }
}

struct ResponseModel<M: Codable>: Codable {
    //    var status: ResultCode?
    var userAuthentication: M?
    var content: [M]?
    var totalPages: Int?
    var data : M?
    var productName : String?
    var productId : Int?
    var size : String?
    
    
    // addnewandress
    private enum CodingKeys: String, CodingKey {
        //    case name
        case userAuthentication
        case content
        case totalPages
        case data = ""
        
        
    }
}

struct ResponsePortalModel<M: Codable>: Codable {
    var message: String?
    var result: M?
    var errorMessage: String? {
        return message
    }
    
    private enum CodingKeys: String, CodingKey {
        case message
        case result
    }
}

struct ResponseError {
    
    var status: ResultCode
}

struct EmptyModel: Codable {
    
}

class APIRequest {
    
    static let baseURLString    = Const.BASE_URL
    static let GetDataURLString = Const.URL_GETDATA
    //    static let GetDataURL8888   = Const.URL_GETDATA8888
    
    var isDictInit: Bool = false
    
    @discardableResult
    // router :
    static func request<M: Codable>(_ router: URLRequestConvertible,
                                    requiredToken: Bool = false,
                                    isSkipError: Bool = false,
                                    failure: ((ResponseError) -> Void)? = nil,
                                    completion: ((ResponseModel<M>?) -> Void)?) -> Request? {
        
        guard Connectivity.isConnectedToInternet else {
            DEBUGLog("--------Disconnect network--------")
            completion?(nil)
            return nil
        }
        // getToken
        let manager = Session.manager
        if requiredToken, let token = SessionManagerCodale.shared.accountModel?.access_token {
            let adapter = CPRequestAdapter(accessToken: token)
            manager.adapter = adapter
        } else {
            let adapter = CPRequestAdapter(accessToken: nil)
            manager.adapter = adapter
        }
        //        check result
        return Session.request(router).responseJSON { (dataResponse) in
            if let error = dataResponse.error {
                if !isSkipError {
                    DEBUGLog(error.localizedDescription)
                    completion?(nil)
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                } else {
                    completion?(nil)
                }
                return
            }
            
            // Request failure
            if let httpStatusCode = dataResponse.response?.statusCode, (httpStatusCode >= 300 || httpStatusCode < 200) {
                // False
                // Process token Expired
                print(httpStatusCode)
                if httpStatusCode == ResultCode.httpUnauthorized.rawValue {
                    Dispatch.async {
                        completion?(nil)
                    }
                    completion?(nil)
                    return
                }
            }else{
                
            }
            // Response no data
            guard let data = dataResponse.data else {
                DEBUGLog("No Data")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
                return
            }
            // Process parse data
            
            /*** Note*/
            do {
                let responseModel = try JSONDecoder().decode(ResponseModel<M>.self, from: data)
//                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
               
                if responseModel.userAuthentication == nil {
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    completion?(nil)
                } else {
                    completion?(responseModel)
                }
                if responseModel.content == nil {
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    completion?(nil)
                } else {
//                    let boole: Bool
                    if responseModel.content is NSArray {
                        findAndReplaceRefId(arrObj: responseModel.content!)
                    }
                    completion?(responseModel)
                }
                if responseModel.data == nil{
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    completion?(nil)
                }else{
                    completion?(responseModel)
                }
                
            }
                /***End*/
            catch {
                DEBUGLog("PARSE ERROR: \(error)")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
            }
        }
       
    }
    
    static func findAndReplaceRefId(arrObj: [Any]){
          var dictionary = Dictionary<String,NSObject>()
          for var obj in arrObj{
              obj = parseObjec(obj: obj as! NSObject, dict: &dictionary)
          }
      }
      
      static func parseObjec(obj:NSObject, dict: inout[String:NSObject]) -> NSObject{
          let mirrored_object = Mirror(reflecting: obj)
          for (_, attr) in mirrored_object.children.enumerated() {
              let property_name = attr.label as String?
              if  property_name == "ref" {
                  if (attr.value as? String) != nil{
                      let property_value = attr.value as! String
                      return dict[property_value] ?? "" as NSObject
                  }
              }
              else if property_name == "idRef"{
                  let property_value = attr.value as? String
                  if property_value != nil && !(property_value?.isEmpty ?? true){
                      dict[property_value ?? ""] = obj
                  }
              }else if (checkTypeObject(object: attr.value ) == 0){
                  let property_value = attr.value as! NSObject
                  let parse = parseObjec(obj:  property_value, dict: &dict)
                  obj.setValue(parse, forKey: property_name ?? "")
              }
              else if checkTypeObject(object: attr.value) == 1{
                  let arrValue = attr.value as! NSArray
                  for var property  in arrValue{
                      property = parseObjec(obj: property as! NSObject, dict: &dict)
                  }
                  obj.setValue(arrValue, forKey: property_name ?? "")
              }
              
          }
          return obj
      }
                        
     static func checkTypeObject(object:Any)->Int{
           // 0: Object, 1:Array, 2: Default String, Int,...
          switch object
          {
          case is InventoryModel,is StoreModel, is User, is  CurrencyModel, is DistributorModel,is ProductPackingModel, is ProductModel, is ProductDescriptionModel, is ProductPackingPriceModel, is ManufacturerModel, is PackingTypeModel,is LanguageModel, is ImportPoModel, is StoreProductPackingModel, is CountryDescriptionModel, is CountryModel, is UserModel, is ProductTypeModel, is PoModel, is PoDetailModel:
              return 0
          case is NSArray:
              return 1
          default:
              return 2
          }
      }
    
}



extension APIRequest {
    @discardableResult
    static func requestPortal<M: Codable>(_ router: URLRequestConvertible, requiredToken: Bool = false,
                                          failure: ((ResponseError) -> Void)? = nil,
                                          completion: (([M]?) -> Void)?) -> Request? {
        guard Connectivity.isConnectedToInternet else {
            DEBUGLog("--------Disconnect network--------")
            completion?(nil)
            return nil
        }
        
        return Session.request(router).responseJSON { (dataResponse) in
            if let error = dataResponse.error {
                DEBUGLog(error.localizedDescription)
                completion?(nil)
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                return
            }
            
            // Request failure
            if let httpStatusCode = dataResponse.response?.statusCode, (httpStatusCode >= 300 || httpStatusCode < 200) {
                // False
                // Process token Expired
                print("httpStatusCode: \(httpStatusCode)")
                if httpStatusCode == ResultCode.httpUnauthorized.rawValue {
                    completion?(nil)
                    return
                }
            }
            
            // Response no data
            guard let data = dataResponse.data else {
                DEBUGLog("No Data")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
                return
            }
            
            // Process parse data
            do {
                let responseModel = try JSONDecoder().decode([M].self, from: data)
                if responseModel == nil {
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    completion?(nil)
                } else {
                    completion?(responseModel)
                }
            }
            catch {
                DEBUGLog("PARSE ERROR: \(error)")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
            }
        }
    }
}

extension APIRequest {
    @discardableResult
    static func requestOk<M: Codable>(_ router: URLRequestConvertible, requiredToken: Bool = false,
                                      failure: ((ResponseError) -> Void)? = nil,
                                      completion: ((M?) -> Void)?) -> Request? {
        guard Connectivity.isConnectedToInternet else {
            DEBUGLog("--------Disconnect network--------")
            completion?(nil)
            return nil
        }
        
        return Session.request(router).responseJSON { (dataResponse) in
            if let error = dataResponse.error {
                DEBUGLog(error.localizedDescription)
                completion?(nil)
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                return
            }
            
            // Request failure
            if let httpStatusCode = dataResponse.response?.statusCode, (httpStatusCode >= 300 || httpStatusCode < 200) {
                // False
                // Process token Expired
                if httpStatusCode == ResultCode.httpUnauthorized.rawValue {
                    completion?(nil)
                    return
                }
            }
            
            // Response no data
            guard let data = dataResponse.data else {
                DEBUGLog("No Data")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
                return
            }
            
            // Process parse data
            do {
                let responseModel = try JSONDecoder().decode(M.self, from: data)
                if responseModel == nil {
                    failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                    completion?(nil)
                } else {
                     var dictionary = Dictionary<String,NSObject>()
                    parseObjec(obj: responseModel as! NSObject, dict: &dictionary)
                    completion?(responseModel)
                }
            }
            catch {
                DEBUGLog("PARSE ERROR: \(error)")
                failure?(ResponseError(status: ResultCode(rawValue: dataResponse.response?.statusCode ?? 999) ?? .unknow))
                completion?(nil)
            }
        }
    }
}



private struct CPRequestAdapter: RequestAdapter  {
    private var accessToken: String?
    private var refreshToken: String?
    
    init(accessToken: String? = nil) {
        self.accessToken = accessToken
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        //  urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        guard let accessToken = accessToken else { return urlRequest }
        urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
        return urlRequest
    }
}

class Dispatch: NSObject {
    static func async(_ closure: @escaping () -> Void) {
        DispatchQueue.main.async {
            closure()
        }
    }
    
    static func asyncAfter(_ timeDelay: Double, _ closure: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + timeDelay, execute: {
            closure()
        })
    }
}
