//
//  Uicolor.swift
//  Hunter
//
//  Created by TheLightLove on 12/02/2020.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int,  alpha: CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            alpha: alpha
        )
    }
    
    public convenience init?(hexColor: String) {
        var red: CGFloat?
        var green: CGFloat?
        var blue: CGFloat?
        var alpha: CGFloat = 1.0
        if hexColor.hasPrefix("#") {
            let index = hexColor.index(hexColor.startIndex, offsetBy: 1)
            let hex = String(hexColor[index...hexColor.endIndex])
            let scanner = Scanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            if scanner.scanHexInt64(&hexValue) {
                switch hex.lengthOfBytes(using: String.Encoding.utf8) {
                case 3:
                    red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                    green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                    blue  = CGFloat(hexValue & 0x00F)              / 15.0
                case 4:
                    red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                    green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                    blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                    alpha = CGFloat(hexValue & 0x000F)             / 15.0
                case 6:
                    red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
                case 8:
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                default:
                    break
                }
            }
        }
        if let r = red, let g = green, let b = blue {
            self.init(red: r, green: g, blue: b, alpha: alpha)
        } else {
            self.init()
            return nil
        }
    }
    convenience init(hex: String) {
        self.init(hex: hex, alpha: 1)
    }
    
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(startTo: 1)
        }
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt: UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        var red: UInt32 = 0xff, green: UInt32 = 0xff, blue: UInt32 = 0xff
        switch hexWithoutSymbol.count {
        case 3: // #RGB
            red = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            green = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            blue = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
        case 6: // #RRGGBB
            red = (hexInt >> 16) & 0xff
            green = (hexInt >> 8) & 0xff
            blue = hexInt & 0xff
        default:
            break
        }
        self.init(
            red: (CGFloat(red)/255),
            green: (CGFloat(green)/255),
            blue: (CGFloat(blue)/255),
            alpha: alpha)
    }
    class func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        let color = UIColor.init(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
        return color
    }
    class func rgba(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        let color = UIColor.init(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
        return color
    }
    class func rgb(by color: CGFloat) -> UIColor {
        let color = UIColor.init(red: color/255, green: color/255, blue: color/255, alpha: 1.0)
        return color
    }
    class func directionButtonColor(isEnable: Bool) -> UIColor {
        return isEnable ? UIColor(hex: "#009688") :  UIColor(hex: "#75c6bf")
    }
    
    
    class func dircetionColorTextButton(isEnable: Bool) -> UIColor {
        return isEnable ? UIColor(hex: "#009688") :  UIColor.lightGray
    }
}

enum ValidateLength: Int {
    case minLength = 0
    case minLength1 = 1
    case minLength8 = 8
    case maxLength16 = 16
    case length19 = 19
    case length20 = 20
    case maxLength64 = 64
    case length4000 = 4000
}

extension String {
    static func className(_ aClass: AnyClass) -> String {
        guard let className = NSStringFromClass(aClass).components(separatedBy: ".").last else {
            fatalError()
        }
        return className
    }
    func substring(startTo: Int) -> String {
        let startIdx = self.index(self.startIndex, offsetBy: startTo)
        let endIdx = self.endIndex
        return String(self[startIdx..<endIdx])
    }
    
    func subStringWith(start: Int, end: Int) -> String {
        let startIdx = self.index(startIndex, offsetBy: start)
        let endIdx = self.index(startIndex, offsetBy: end)
        return String(self[startIdx...endIdx])
    }
    public subscript(integerIndex: Int) -> Character {
        let index = self.index(startIndex, offsetBy: integerIndex)
        return self[index]
    }
    func split(_ seperator: String) -> [String] {
        return self.components(separatedBy: seperator)
    }
    func nsRange(from range: Range<Index>) -> NSRange {
        let from = range.lowerBound
        let to = range.upperBound
        let location = self.distance(from: startIndex, to: from)
        let length = self.distance(from: from, to: to)
        return NSRange(location: location, length: length)
    }
    func isContainNumber() -> Bool {
        let decimalCharacters = NSCharacterSet.decimalDigits
        let decimalRange = self.rangeOfCharacter(from: decimalCharacters, options: .numeric, range: nil)
        if decimalRange == nil {
            return false
        } else {
            return true
        }
    }
    func checkTextSufficientComplexity() -> Bool {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: self)
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: self)
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format: "SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluate(with: self)
        
        return capitalresult || numberresult || specialresult
    }
    func isContainCapital() -> Bool {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format: "SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: self)
        return capitalresult
    }
    func isDate() -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if dateFormatter.date(from: self) != nil {
            return true
        } else {
            return false
        }
    }
    func equal(_ string: String) -> Bool {
        return self == string
    }
    
    func isValidEmail() -> Bool {
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", Regex.emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPhone() -> Bool {
        let phoneTest = NSPredicate(format:"SELF MATCHES[c] %@", Regex.phoneRegEx)
        return phoneTest.evaluate(with: self)
    }
    
    func validLength(minLength: ValidateLength = .minLength, maxLength: ValidateLength = .maxLength64) -> Bool {
        return (self.count >= minLength.rawValue) && (self.count <= maxLength.rawValue)
    }
    
    func isSpaceValue() -> Bool {
        return self.compare(" ") == .orderedSame
    }
    
    func isContainMultipleSpace() -> Bool {
        if self.contains("  ") {
            return true
        }
        return false
    }
    
    func dateRepresentation(format: String, useCurrentTimeZone: Bool = true) -> Date? {
        guard !self.isEmpty, !format.isEmpty else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        let locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = locale
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = useCurrentTimeZone ? TimeZone.current : TimeZone(identifier: "UTC")
        
        return dateFormatter.date(from:self)
    }
    
    func toDateFromUTC() -> Date? {
        Formatter.shared.dateFormatter.dateFormat = AppFormatters.ISO8601
        return Formatter.shared.dateFormatter.date(from: self)
    }
    
    func toDateWithFormat(_ format: String) -> Date? {
        guard !format.isEmpty else { return nil }
        Formatter.shared.dateFormatter.dateFormat = format
        return Formatter.shared.dateFormatter.date(from: self)
    }
    
    func getCurrentDate(_ dateFormat: String) -> String {
        let date = Date()
        let locale = Locale(identifier: "en_US_POSIX")
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone.current// TimeZone(identifier: "UTC")
        return dateFormatter.string(from: date)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func trimLeadingTrailingWhitespaces() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    func trimWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 18)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}
