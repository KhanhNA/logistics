//
//  Global.swift
//  MarketPlace
//
//  Created by HiCom on 2/13/19.
//  Copyright © 2019 HiCom. All rights reserved.
//

import UIKit
import CoreLocation

class Global: NSObject {
    static var isCheckPushViewUpdate:Bool?
    static var langId:Int = 1
    static var user = UserInforModel()
    static var isCheckPushUpdateStatus:Bool?
    static var registerObj = RegisterMerchants()
}
