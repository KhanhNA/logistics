//
//  ApiPath.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import Foundation
// MARK: - APIPath
struct APIPath {
    static let login                        = "/user/me"
    static let mechant                      = "/api/v1/merchant/user"
    static let regiterMerchants             = "/api/v1/merchant/create/info"
    static let commonlist                   = "/api/v1/common/list"
    static let updateMerchants              = "/api/v1/merchants/update"
    static let uploadImage                  = "/api/v1/merchant/image"
    static let wareHouse                    = "/stores/near-here"
    static let optionModel                  = "/api/v1/common/option"
    static let storeWareHouse               = "/api/v1/merchant/save/store"
    static let dashPot                      = "/api/v1/hunter-dashboard"
    static let dashPotSearch                = "/api/v1/merchants/search"
    static let updateStatus                 = "/api/v1/merchants/update-status"
    static let merchantDashboard            = "/api/v1/merchant-dashboard"
    static let revenueMechants              = "/api/v1/merchant-revenue"
    static let stores                       = "/stores"
    static let distributors                 = "/distributors/all"
    static let inventory                    = "/stores/inventory"
    static let detailInventory              = "/stores/inventory/pallet-details"
    static let po                           = "/pos"
}

// MARK: - APIParamKeys
struct APIParamKeys {
    static let phoneNumber                  = "phoneNumber"
    static let userId                       = "userId"
    static let verifyCode                   = "verifyCode"
    static let userName                     = "userName"
    static let email                        = "email"
    static let userPassword                 = "userPassword"
    static let avatarPath                   = "avatarPath"
    static let regionId                     = "regionId"
    static let schoolId                     = "schoolId"
    static let password                     = "password"
    static let searchKey                    = "searchKey"
    static let newPassword                  = "newPassword"
    static let learnClass                   = "class"
    static let unitId                       = "unitId"
    static let lessonId                     = "lessonId"
    static let examTypeId                   = "examTypeId"
    static let examId                       = "examId"
    static let successAnswer                = "successAnswer"
    static let wrongAnswer                  = "wrongAnswer"
    static let type                         = "type"
    static let GOOGLE_MAP_KEY               = "AIzaSyAVS7WvgPDd1Bb3kZJgrmylIAW1ivt4w48"
    static let GOOGLE_API_KEY               = "AIzaSyAVS7WvgPDd1Bb3kZJgrmylIAW1ivt4w48"
}
