//
//  Router.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit
import Alamofire

enum Router: URLRequestConvertible {
    case login(body: Parameters?)
    case registerMerchants(body:Parameters?)
    case commonList(body:Parameters?, para:Parameters?)
    case saveStore(body:Parameters?,para:Parameters?)
    case getDetailInventory(para:Parameters)
    case getListPo(para:Parameters)
    //case getPoDetail(para:Parameters)
    var result: (path: String, method: HTTPMethod, body: Parameters?, params: Parameters?) {
        switch self {
        case .login(let body):
            return (APIPath.login, .get, body, nil)
        case .registerMerchants(let body):
            return (APIPath.regiterMerchants, .post , nil,body)
        case .commonList(let body, let para):
            return (APIPath.commonlist , .post, body, para)
        case .saveStore(let body, let para):
            return (APIPath.storeWareHouse , .post, body,para)
        case .getDetailInventory(let para):
            return (APIPath.detailInventory, .get, nil, para)
        case .getListPo(let para):
            return (APIPath.po,.get, nil, para)
//        case .getPoDetail(let para):
//            return (APIPath.detailPo, .get, para, nil)
        }
    }
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var url: URL = try APIRequest.GetDataURLString.asURL()
        let result = self.result
        if result.path == APIPath.login {
            url = try APIRequest.baseURLString.asURL()
        }
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("1", forHTTPHeaderField: "Accept-Language")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        if let params = result.params {
            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
            return urlEncode
        }
        return urlRequest
    }
}

enum RouterTest: URLRequestConvertible {
    case getPoDetail(para:Parameters)
    var result: (path: String, method: HTTPMethod, body: Parameters?, params: Parameters?) {
        switch self {
        case .getPoDetail(let para):
            return (APIPath.po, .get, nil, nil)
        }
    }
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
//        let urlString = String(format: "%@",Const.URL_GETDATA)
//               var urlRequestSum = "\(urlString)?"
//               for param in result.params ?? [:] {
//                   urlRequestSum = "\(urlRequestSum)\(param.key)=\(param.value)&"
//               }
        
        var url: URL = try APIRequest.GetDataURLString.asURL()
        let result = self.result
        if result.path == APIPath.login {
            url = try APIRequest.baseURLString.asURL()
        }
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path).appendingPathComponent("/24"))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("1", forHTTPHeaderField: "Accept-Language")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        if let params = result.params {
            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
            return urlEncode
        }
        return urlRequest
    }
}

enum RouterJsonData: URLRequestConvertible {
    case common(para:Parameters,body:NSDictionary?)
    case updateMechants(para:Parameters,body:NSDictionary?)
    case searchStatus(para:Parameters)
    case getFromStore(para:Parameters)
    case getAllDistributor
    case getInventory(para:Parameters)
    case getDetailInventory(para:Parameters)
    case getListPo(para:Parameters)
    var result: (path: String, method: HTTPMethod, body: NSDictionary?, params: Parameters?) {
        switch self {
        case .common(para:let para,body: let body):
            return (APIPath.commonlist, .post, body, para)
        case .updateMechants(let para, let body):
            return (APIPath.updateMerchants , .post ,body ,para)
        case .searchStatus(let para):
            return (APIPath.dashPotSearch, .get, nil,para)
        case .getFromStore(let para):
            return (APIPath.stores,.get,nil,para)
        case .getAllDistributor:
            return (APIPath.distributors,.get, nil,nil)
        case .getInventory(let para):
            return (APIPath.inventory,.get, nil, para)
        case .getDetailInventory(let para):
            return (APIPath.detailInventory,.get, nil, para)
        case .getListPo(let para):
            return (APIPath.po,.get, nil, para)
        }
    }
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let urlString = String(format: "%@",Const.URL_GETDATA)
        var urlRequestSum = "\(urlString)?"
        for param in result.params ?? [:] {
            urlRequestSum = "\(urlRequestSum)\(param.key)=\(param.value)&"
        }
        let url: URL = try urlRequestSum.asURL()
        let result = self.result
        if result.path == APIPath.commonlist {
            //            url = try APIRequest.baseURLString.asURL()
        }
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("1", forHTTPHeaderField: "Accept-Language")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
//        if let params = result.params {
//            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
//            return urlEncode
//        }
        return urlRequest
    }
    
    
}

enum RouterFromdat: URLRequestConvertible {
    case login(body: Parameters?)
    case registerMerchants(body:Parameters?)
    case commonList(body:Parameters?, para:Parameters?)
    case optionModel(body:Parameters?)
    case dashPot(para:Parameters?)
    case updateStatus(body:Parameters?)
    case merchantsUser(para:Parameters?)
    case merchantsDashBoard(para:Parameters?)
    case merchantRevenue(para:Parameters?)

    var result: (path: String, method: HTTPMethod, body: Parameters?, params: Parameters?) {
        switch self {
        case .login(let body):
            return (APIPath.login, .get, body, nil)
        case .registerMerchants(let body):
            return (APIPath.regiterMerchants, .post , body,nil)
        case .commonList(let body, let para):
            return (APIPath.commonlist , .post, body, para)
        case .optionModel(let body):
            return (APIPath.optionModel, .post,body,nil)
        case .dashPot(let para):
            return (APIPath.dashPot, .get, nil,para)
        case .updateStatus(let body):
            return (APIPath.updateStatus, .post, body,nil)
        case .merchantsUser(let para):
            return (APIPath.mechant, .get, nil,para)
        case .merchantsDashBoard(let para):
            return (APIPath.merchantDashboard, .get,nil, para)
        case .merchantRevenue(let para):
            return (APIPath.revenueMechants , .get,nil, para)
        }
    }
//     MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var url: URL = try APIRequest.GetDataURLString.asURL()
        let result = self.result
        if result.path == APIPath.login {
            url = try APIRequest.baseURLString.asURL()
        }
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
//        urlRequest.httpMethod = result.method.rawValue
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("1", forHTTPHeaderField: "Accept-Language")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        if let params = result.params {
            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
            return urlEncode
        }
        return urlRequest
    }
}

enum RouterJsonData8888: URLRequestConvertible {
    case wareHouse(para:Parameters)
    var result: (path: String, method: HTTPMethod, body: NSDictionary?, params: Parameters?) {
        switch self {
        case .wareHouse(let para):
            return (APIPath.wareHouse , .get , nil, para)
        }
    }
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var url: URL = try APIRequest.baseURLString.asURL()
        let result = self.result
        if result.path == APIPath.login {
            url = try APIRequest.baseURLString.asURL()
        }
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        //        urlRequest.httpMethod = result.method.rawValue
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("1", forHTTPHeaderField: "Accept-Language")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        if let params = result.params {
            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
            return urlEncode
        }
        return urlRequest
    }
    
    
}

enum RouterJsonOrder: URLRequestConvertible {
    case saveStore(body:Any?,para:Parameters?)

    var result: (path: String, method: HTTPMethod, body: Any?, params: Parameters?) {
        switch self {
        case .saveStore(let body, let para):
            return (APIPath.storeWareHouse, .post, body ,para)
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let urlString = String(format: "%@%@",Const.URL_GETDATA , result.path)
        var urlRequestSum = "\(urlString)?"
        for param in result.params ?? [:] {
            urlRequestSum = "\(urlRequestSum)\(param.key)=\(param.value)&"
        }
        let url: URL = try urlRequestSum.asURL()
        let result = self.result
        var urlRequest = URLRequest(url: url.appendingPathComponent(""))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("1", forHTTPHeaderField: "Accept-Language")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
//        if let params = result.params {
//            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
//            return urlEncode
//        }
        return urlRequest
    }
}

enum RouterJson: URLRequestConvertible {
    case mechant(body:Parameters?)
    var result: (path: String, method: HTTPMethod, body: NSDictionary?, params: Parameters?) {
        switch self {
        case .mechant(body: let body):
            return (APIPath.mechant, .get, nil, body)
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let urlString = String(format: "%@%@",Const.URL_GETDATA , result.path)
        var urlRequestSum = "\(urlString)?"
        for param in result.params ?? [:] {
            urlRequestSum = "\(urlRequestSum)\(param.key)=\(param.value)&"
        }
        let url: URL = try urlRequestSum.asURL()
        let result = self.result
        var urlRequest = URLRequest(url: url.appendingPathComponent(""))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("1", forHTTPHeaderField: "Accept-Language")
        if let body = result.body {
            let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted )
            let json = NSString(data: bodyData, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            urlRequest.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        }
        //        if let params = result.params {
        //            let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
        //            return urlEncode
        //        }
        return urlRequest
    }
    
    
}
