//
//  LeftMenuViewController.swift
//  Hunter
//
//  Created by TheLightLove on 17/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit

class LeftMenuViewController: BaseViewController {
    @IBOutlet weak var tbViewMenu: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var tableViewData: [CellData] = []
    
    let arrImage = [UIImage(named: "Dashpot"),
    UIImage(named: ""),
    UIImage(named: "Register"),
    UIImage(named: "language"),
    UIImage(named: "logout")]
    
    init(tableView: [CellData]) {
        self.tableViewData = tableView
        super.init(nibName: "LeftMenuViewController", bundle: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbViewMenu.delegate = self
        tbViewMenu.dataSource = self
        //
        tbViewMenu.register(UINib(nibName: MenuCell.className, bundle: nil), forCellReuseIdentifier: MenuCell.className)
        imgProfile.layer.cornerRadius = 45
        imgProfile.layer.masksToBounds = true
        lblName.text = Global.user.fullName ?? ""
    }
    
    func changeViewCotrller(rootView : UIViewController) {
        let mainViewController = UINavigationController(rootViewController: rootView)
        self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
    }
}

extension LeftMenuViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[section].opened {
            return tableViewData[section].sectionData.count + 1
        } else {
            return 1
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: MenuCell.className, for: indexPath) as! MenuCell
        if indexPath.row == 0 {
            cell.lblTitle?.text = tableViewData[indexPath.section].title.localized()
            cell.imgMenu.image = UIImage(named: "")
            cell.lblTitle?.textColor = .black
            cell.lblTitle.font = UIFont.boldSystemFont(ofSize: 17)
            return cell
        } else {
            //Cell tuỳ biến, đây là cell nhỏ sau khi mở rộng ra nhé!

            cell.lblTitle?.text = tableViewData[indexPath.section].sectionData[indexPath.row - 1].code!.localized()
            cell.lblTitle?.textColor = .gray
            cell.imgMenu.image = tableViewData[indexPath.section].sectionData[indexPath.row - 1].image
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableViewData[indexPath.section].opened {

            if indexPath.row == 0 {
                if "LOGOUT".equal(tableViewData[indexPath.section].title){
                    self.onLogout()
                }else{
                    tableViewData[indexPath.section].opened = false
                    let sections = IndexSet.init(integer: indexPath.section)
                    tableView.reloadSections(sections, with: .automatic)
                }
            }
            else{
                let codeSection = tableViewData[indexPath.section].sectionData[indexPath.row - 1].code!
                let cotroller:UIViewController = tableViewData[indexPath.section].sectionData[indexPath.row - 1].uiController!
                if "INVENTORY".equal(codeSection){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("MERCHANT_ORDERs_LIST_MERCHANT_ORDER"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT_RELEASES"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("EXPORT_STATEMENTs_EXPORT_STATEMENT"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("EXPORT_STATEMENTs_RELEASES"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("EXPORT_STATEMENTs_DELIVERY"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("MANAGE_FC_UPDATE_PALLET"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("MANAGE_FC_IMPORT_STATEMENTs_FROM_REPACKING_PLANNING"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("PUCHASEORDER_LIST"){
                    changeViewCotrller(rootView: cotroller)
                }
                else if codeSection.equal("CLAIM_PALLET_STEP_ONE"){
                    changeViewCotrller(rootView: cotroller)
                }else if codeSection.equal("CLAIM_PALLET_STEP_TWO"){
                    changeViewCotrller(rootView: cotroller)
                }else if codeSection.equal("CLAIM_FIND"){
                    changeViewCotrller(rootView: cotroller)
                }else if codeSection.equal("CHANGE_LANGUAGE"){
                    changeViewCotrller(rootView: cotroller)
                }
            }

        } else {
            tableViewData[indexPath.section].opened = true
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .automatic)
        }

    }

}

  
