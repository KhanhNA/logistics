//
//  ChangeLanguageVC.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 4/4/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit
import PopupDialog
import Localize_Swift

class ChangeLanguageVC: BaseViewController {

    @IBOutlet var btnChangLanguage: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setText()
    }

    @IBAction func changeLanguage(_ sender: Any) {
        let title = "select_language".localized()
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: nil, image: nil)
        
        //        // Create buttons
        let buttonVietNam = DefaultButton(title: "VietNamese".localized()) {
            Localize.setCurrentLanguage("vi")
            self.reloadView()
        }
        
        let buttonEnglish = DefaultButton(title: "English".localized()) {
            Localize.setCurrentLanguage("en")
            self.reloadView()
        }
        
        let buttonMyanmar = DefaultButton(title: "Myanmar".localized()) {
            Localize.setCurrentLanguage("my")
            self.reloadView()
        }
        
        let buttonThree = CancelButton(title: "Cancel".localized()) {
        }
        
        // to add a single button
        popup.addButtons([buttonVietNam, buttonEnglish,buttonMyanmar, buttonThree])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func setText(){
        setNavMenuItem(title:"language".localized())
        btnChangLanguage.setTitle("language".localized(), for: .normal)
    }
    
    func reloadView(){
        self.loadView()
        setText()
       }

}
