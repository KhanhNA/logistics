//
//  InventoryViewController.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 3/4/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit
import DropDown
import PopupDialog

class InventoryViewController: BaseViewController {
    lazy var presenter: InventoryInputProtocol = InvetoryPresenter()
    let chooseDropDownMethod = DropDown()
    var indexChooseMethod:Int?
    var listStoreString = [String]()
    var listDistributorString = [String]()
    var listDistributor = [DistributorModel]()
    var arrStore = [StoreModel]()
    var arrInventory = [InventoryModel]()
    var arrDetailInvent = [InventoryDetailPalletModel]()
    var indexStore:Int = 0
    var indexDistributor:Int = 0
    var indexInventory:Int = 0
    
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet var btnDropStoreDC: UIButton!
    @IBOutlet var tbView: UITableView!
    @IBOutlet var btnDropDistributor: UIButton!
    @IBOutlet var lblDistributor: UILabel!
    @IBOutlet var lblToStore: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = ""
    var topRefreshTable: UIRefreshControl?
    var start_index: Int = -1
    var page: Int = 0
    
    var pageNumber: Int = 1
    
    @IBOutlet weak var searchTF: UITextField!
    var lastContentOffset: CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        setText()
        uiTableViewdelegate()
        presenter.attachView(self)
        tbView.register(UINib(nibName: InventoryTableViewCell.className, bundle: nil), forCellReuseIdentifier: InventoryTableViewCell.className)
        presenter.getFromStore(exceptIds: [-1], ignoreCheckPermission: false, pageNumber: 1, pageSize: Const.PAGE_SIZE, status: true, text:"")
    }
    
    func setText(){
        setNavMenuItem(title: "INVENTORY".localized())
        lblToStore.text = "toStore".localized()
        lblDistributor.text = "distributor".localized()
    }
    
    func requestInventory(){
        presenter.getListInventory(distributorId: listDistributor[indexDistributor].id ?? 0, pageNumber: pageNumber, pageSize: Const.PAGE_SIZE, exceptProductPackingIds: [-1], storeId: arrStore[indexStore].id ?? 0, text: searchTF.text ?? "")
    }
    
    @IBAction func onChooseFromDC(_ sender: Any) {
        setupChooseDropDownStore()
        chooseDropDownMethod.show()
    }
    
    @IBAction func onChooseDistibutor(_ sender: Any) {
        chooseDropDownDistributor()
        chooseDropDownMethod.show()
    }
    
    @IBAction func searchInventory(_ sender: Any) {
       requestInventory()
    }
    
    
    func setupChooseDropDownStore() {
        chooseDropDownMethod.anchorView = btnDropStoreDC
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnDropStoreDC.bounds.height)
        chooseDropDownMethod.dataSource = listStoreString

        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnDropStoreDC.setTitle(item, for: .normal)
            self?.indexStore = Int(index)
            self?.presenter.getListInventory(distributorId: self?.listDistributor[self?.indexDistributor ?? 0 ].id ?? 0, pageNumber: 1, pageSize: Const.PAGE_SIZE, exceptProductPackingIds: [-1], storeId: self?.arrStore[index].id ?? 0, text: self?.searchTF.text ?? "")
        }
    }
    
    func chooseDropDownDistributor() {
        chooseDropDownMethod.anchorView = btnDropDistributor
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnDropStoreDC.bounds.height)
        chooseDropDownMethod.dataSource = listDistributorString
        
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnDropDistributor.setTitle(item, for: .normal)
//            self?.lblTiltleDistributor.text = item
            self?.indexDistributor = Int(index)
            self?.presenter.getListInventory(distributorId: self?.listDistributor[index].id ?? 0, pageNumber: 1, pageSize: Const.PAGE_SIZE, exceptProductPackingIds: [-1], storeId: self?.arrStore[self?.indexStore ?? 0].id ?? 0, text: self?.searchTF.text ?? "")

        }
    }
}

extension InventoryViewController:InventoryOutPutProtocol {
    func didFishDetailInventory(inventoryDetail: [InventoryDetailPalletModel]) {
        arrDetailInvent = inventoryDetail
        showPopUpDetail()
    }
    
    func showPopUpDetail(){
        let customAlert = InventoryPopUpVC(nibName: nil, bundle: nil)
        customAlert.detailPallet = arrDetailInvent
        customAlert.textLabel = "Chi tiết sản phẩm " + (arrInventory[indexInventory].productPacking?.code ?? "") + " trên pallet"

        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        present(customAlert, animated: true, completion: nil)

    }
   
    func didFishGetInventory(inventory: [InventoryModel], totalPage: Int) {
        arrInventory = inventory
        self.tbView?.reloadData()
    }
    
    func didFishGetAllDistributor(distrubutor: [DistributorModel]) {
        // add getAll distributor vao vi tri dau tien
        let distriAll:DistributorModel = DistributorModel(ref: "", idRef: "", id: nil, code: "ALL".localized(), name: "", phoneNumber: "", email: "", address: "", enabled: true)
        self.listDistributor = distrubutor
        listDistributor.insert(distriAll, at: 0)
        listDistributorString = Utils.convertObjDistributortoString(arr:listDistributor)
        btnDropDistributor.setTitle(listDistributor[0].code, for: .normal)
//        lblTiltleDistributor.text = listDistributor[0].code ?? ""
        presenter.getListInventory(distributorId: listDistributor[0].id ?? 0, pageNumber: 1, pageSize: 20, exceptProductPackingIds: [-1], storeId: arrStore[0].id ?? 0, text: "")
    }
    
    func didFishStore(store: [StoreModel], totalPage: Int) {
        self.arrStore = store
        listStoreString = Utils.convertStoretoString(arr: store)
        btnDropStoreDC.setTitle(listStoreString[0], for: .normal)
//        lblTitleStore.text = listStoreString[0]

        presenter.getAllDistributor()
    }
    
    func didFishStoreError(error: String) {
        
    }
}

extension InventoryViewController:UIScrollViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastContentOffset = scrollView.contentOffset.y
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if lastContentOffset > scrollView.contentOffset.y{ // scroll up
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                self?.viewParent.alpha = 1.0
                self?.viewParent.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
        } else if lastContentOffset < (scrollView.contentOffset.y - viewParent.frame.height){ // scroll down nho hon uiView moi hien thi
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                
                self?.viewParent.alpha = 0
                self?.viewParent.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: nil)
        }
    }
}



extension InventoryViewController:UITableViewDelegate,UITableViewDataSource{
    func uiTableViewdelegate() {
        tbView.delegate = self
        tbView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexInventory = indexPath.row
        presenter.getDetailInventory(expireDate: arrInventory[indexPath.row].expireDate ?? "", productPackingId: arrInventory[indexPath.row].productPacking?.id ?? 0, storeId: arrStore[indexStore].id ?? 0)
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastElement = arrInventory.count - 1
//        if  indexPath.row == lastElement {
//            pageNumber = pageNumber + 1
//            requestInventory()
//        }
//    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewParent.frame.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInventory.count
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbView.dequeueReusableCell(withIdentifier: InventoryTableViewCell.className, for: indexPath) as! InventoryTableViewCell
        
        let inventoryIndex = arrInventory[indexPath.row]
        cell.setDataInventory(inventoryIndex)
        cell.selectionStyle = .none
        return cell
    }

}

//extension InventoryViewController{
//    func initLoadmoreAndPullToRefresh() {
//        isLoading = "0"
//        topRefreshTable = UIRefreshControl()
//
//        topRefreshTable?.attributedTitle = NSAttributedString(string: "txt_Swipe_Down".localized(), attributes: [NSAttributedString.Key.foregroundColor: Helper.COLOR_PRIMARY_DEFAULT_APP] )
//        topRefreshTable?.tintColor = Helper.COLOR_PRIMARY_DEFAULT_APP
//        topRefreshTable?.addTarget(self, action: #selector(self.refreshData), for: .valueChanged)
//        if let aTable = topRefreshTable {
//            tbView.addSubview(aTable)
//        }
//        refreshControl = UIRefreshControl()
//        refreshControl?.attributedTitle = NSAttributedString(string: "txt_Swipe_Up".localized(), attributes: [NSAttributedString.Key.foregroundColor: Helper.COLOR_PRIMARY_DEFAULT_APP])
//        // refreshControl.triggerVerticalOffset = 60;
//        refreshControl?.addTarget(self, action: #selector(self.loadMore), for: .valueChanged)
//        refreshControl?.tintColor = Helper.COLOR_PRIMARY_DEFAULT_APP
//        tbView.bottomRefreshControl = refreshControl
//    }
//
//    @objc func refreshData() {
//        if (isLoading == "1") {
//            return
//        }else {
//            page = 0
//            start_index = -1
//        }
//        isLoading = "1"
//        arrInventory.removeAll()
//        presenter.getOrderHistory(langId: Global.langId, parentMerchantId: Global.parentMarchantId ?? 0, orderType: orderType, orderStatus: orderStatus, page: start_index)
//    }
//
//    @objc func loadMore() {
//        if (isLoading == "1") {
//            return
//        }else{
//            isLoading = "1"
//            if start_index == page {
//                finishLoading()
//                return
//            }
//            start_index += 1
//            presenter.getOrderHistory(langId: Global.langId, parentMerchantId: Global.parentMarchantId ?? 0, orderType: orderType, orderStatus: orderStatus, page: start_index)
//        }
//    }
//
//    func scrollToTopTableView() {
//        let top = IndexPath(row: NSNotFound, section: 0)
//        tbView.scrollToRow(at: top, at: .top, animated: true)
//    }
//
//    func finishLoading() {
//        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
//        topRefreshTable?.endRefreshing()
//        tbView.bottomRefreshControl.endRefreshing()
//        isLoading = "0"
//    }
//}



