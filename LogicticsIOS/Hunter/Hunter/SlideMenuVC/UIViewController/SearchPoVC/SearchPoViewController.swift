//
//  SearchPoViewController.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 3/10/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit
import DropDown

class SearchPoViewController: BaseViewController {
    
    @IBOutlet var btnFromStore: UIButton!
    @IBOutlet var btnDistributor: UIButton!
    @IBOutlet var btnStatusRecent: UIButton!
    @IBOutlet var btnStatusOrder: UIButton!
    @IBOutlet var lblStore: UILabel!
    @IBOutlet var lblDistributor: UILabel!
    @IBOutlet var lblStatusRecent: UILabel!
    @IBOutlet var lblStatusOrder: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var viewHeaderTop: UIView!
    
    
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var toDateTF: UITextField!
    var datePicker = UIDatePicker()
    var dateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    
    var lastContentOffset: CGFloat = 0
    lazy var presenter: StoreAndDistributorInputProto = StoreAndDistributorPresenter()
    let chooseDropDownMethod = DropDown()
    var arrStore = [StoreModel]()
    var listDistributor = [DistributorModel]()
    var listStoreString = [String]()
    var listDistributorString = [String]()
    var listPo = [PoModel]()
    var indexStore:Int = 0
    var indexDistributor:Int = 0
    var indexStatusRecent:Int = 0
    var indexStatusOrder:Int = 0
    
    var arrStatus:[String] = ["New".localized(),"Repacked".localized(),"Imported".localized(),"Cancel".localized()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // khơi tạo view
        initController()
        presenter.attachView(self)
        presenter.requestGetFromStore(exceptIds: [-1], ignoreCheckPermission: false, pageNumber: 1, pageSize: 20, status: true, text:"")
       
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fromDateTF {
            datePicker.datePickerMode = .date
        }
        if textField == toDateTF {
            datePicker.datePickerMode = .date
        }
    }
    
    @objc func doneButtonTapped() {
        dateFormatter.dateFormat = AppFormatters.nomarlFormat
        if fromDateTF.isFirstResponder {
            fromDateTF.text = dateFormatter.string(from: datePicker.date)
        }
        if toDateTF.isFirstResponder {
            toDateTF.text = dateFormatter.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    
    func initController() {
        //set title
        setNavMenuItem(title: "list_po".localized())
        //set label
        lblStore.text = "fromStore".localized()
        lblDistributor.text = "distributor".localized()
        lblStatusRecent.text = "current_status".localized()
        lblStatusOrder.text = "statement_status".localized()
        lblFromDate.text = "fromDate".localized()
        lblToDate.text = "toDate".localized()
        
        self.btnStatusOrder.setTitle(arrStatus[0], for: .normal)
        self.btnStatusRecent.setTitle(arrStatus[0], for: .normal)
        // khoi tao datePicker
        fromDateTF.inputView    = datePicker
        toDateTF.inputView      = datePicker
        
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped))
        toolBar.setItems([doneButton], animated: true)
        fromDateTF.inputAccessoryView   = toolBar
        toDateTF.inputAccessoryView     = toolBar
        
        // registerTableView
        self.tbView.register(UINib(nibName: SearchPoTableViewCell.className, bundle: nil), forCellReuseIdentifier: SearchPoTableViewCell.className)
        uiTableViewdelegate()
        
        // set current date UiTextField
        dateFormatter.dateFormat = AppFormatters.nomarlFormat
        let currentDateTime = Date()
        // get date truoc 1 thang
        let fromDate = Calendar.current.date(byAdding: .month, value: -1, to: currentDateTime)
        
        fromDateTF.text = dateFormatter.string(from: fromDate ?? currentDateTime)
        toDateTF.text = dateFormatter.string(from: currentDateTime)
        
        
    }
    
    @IBAction func chooseStore(_ sender: Any) {
        setupChooseDropDownStore()
        chooseDropDownMethod.show()
    }
    
    @IBAction func chooseDistributor(_ sender: Any) {
        setupChooseDropDownDistributor()
        chooseDropDownMethod.show()
    }
    
    @IBAction func chooseStatusRecent(_ sender: Any) {
        setupChooseDropStatusRecent()
        chooseDropDownMethod.show()
    }
    
    @IBAction func chooseStatusOrder(_ sender: Any) {
        setupChooseStatusOrder()
        chooseDropDownMethod.show()
    }
    @IBAction func searchPo(_ sender: Any) {
        searchPo()
    }
    
    func searchPo(){
        let storeId:Int? = arrStore[indexStore].id
        let distributorId:String?
        if indexDistributor == 0{
            distributorId = ""
        }else {
            distributorId = String(listDistributor[indexDistributor].id ?? 0)
        }
        let txtSearch:String? = txtFieldSearch.text
        
        let statusCurent:Int?
        if indexStatusRecent == 3{
            statusCurent = -1
        }else{
            statusCurent = indexStatusRecent
        }
        
        let statusOrder:Int?
        if indexStatusOrder == 3{
            statusOrder = -1
        }else{
            statusOrder = indexStatusOrder
        }
        let fromDate:String? = DateUtil.convertDateToRequest(fromDateTF.text ?? "")
        let toDate:String? = DateUtil.convertDateToRequest(toDateTF.text ?? "")
        
        presenter.requestGetListPo(storeId: storeId ?? 0, distributorId: distributorId ?? "", text: txtSearch ?? "", fromDate: fromDate ?? "", toDate: toDate ?? "", dateType: statusCurent ?? 0, status: statusOrder ?? 0, pageNumber: 1, pageSize: 20)
    }
    
    
    func setupChooseDropDownStore() {
        chooseDropDownMethod.anchorView = btnFromStore
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnFromStore.bounds.height)
        chooseDropDownMethod.dataSource = self.listStoreString
        
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnFromStore.setTitle(item, for: .normal)
            self?.indexStore = Int(index)
        }
    }
    
    func setupChooseDropDownDistributor() {
        chooseDropDownMethod.anchorView = btnDistributor
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnDistributor.bounds.height)
        chooseDropDownMethod.dataSource = self.listDistributorString
        
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnDistributor.setTitle(item, for: .normal)
            self?.indexDistributor = Int(index)
        }
    }
    
    func setupChooseDropStatusRecent() {
        chooseDropDownMethod.anchorView = btnStatusRecent
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnStatusRecent.bounds.height)
        chooseDropDownMethod.dataSource = arrStatus
        
        
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnStatusRecent.setTitle(item, for: .normal)
            self?.indexStatusRecent = Int(index)
        }
    }
    
    func setupChooseStatusOrder() {
        chooseDropDownMethod.anchorView = btnStatusOrder
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnStatusOrder.bounds.height)
        chooseDropDownMethod.dataSource = arrStatus
        
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnStatusOrder.setTitle(item, for: .normal)
            self?.indexStatusOrder = Int(index)
        }
    }
    
    
}

extension SearchPoViewController:StoreAndDistributorOutPutProtocol{
    func getPoDetailSuccess(poDetail: PoModel) {
       let viewControl = DetailPoViewController()
        viewControl.poDetail = poDetail
        self.navigationController?.pushViewController(viewControl, animated: true)
    }
    
    func getListPoSuccess(lstPo: [PoModel], totalPages: Int) {
        listPo = lstPo
        self.tbView.reloadData()
    }
    
    func didFishStore(store: [StoreModel], totalPage: Int) {
        self.arrStore = store
        listStoreString = Utils.convertStoretoString(arr: store)
        presenter.requestGetAllDistributor()
    }
    
    func didFishGetAllDistributor(distrubutor: [DistributorModel]) {
        // add "Tat ca tại vị trí đầu tiên"
        let distriAll:DistributorModel = DistributorModel(ref: "", idRef: "", id: nil, code: "ALL".localized(), name: "", phoneNumber: "", email: "", address: "", enabled: true)
        self.listDistributor = distrubutor
        listDistributor.insert(distriAll, at: 0)
        listDistributorString = Utils.convertObjDistributortoString(arr:listDistributor)
        
        btnFromStore.setTitle(listStoreString[0], for: .normal)
        btnDistributor.setTitle(listDistributorString[0], for: .normal)
        
        searchPo()
//         presenter.requestGetListPo(storeId: 11, distributorId: "", text: "", fromDate: "2020-03-09 00:00:00.000Z", toDate: "2020-04-09 00:00:00.000Z", dateType: 0, status: 0, pageNumber: 1, pageSize: 20)
        
    }
    
    func didFishStoreError(error: String) {
        
    }
    
}

extension SearchPoViewController:UITableViewDelegate, UITableViewDataSource{
    func uiTableViewdelegate() {
        self.tbView.delegate = self
        self.tbView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 150
       }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.requestDetailPo(poId: 24)//        let indexDetailPo = indexPath.row
//        let vc = DetailPoViewController()
//        self.navigationController?.pushViewController(vc, animated: true)
        

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPo.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewHeaderTop.frame.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: SearchPoTableViewCell.className, for: indexPath) as! SearchPoTableViewCell
        
        let poIndex = listPo[indexPath.row]
        cell.setPo(po: poIndex)
        cell.selectionStyle = .none
        return cell
    }
    
    
    
}

extension SearchPoViewController:UIScrollViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastContentOffset = scrollView.contentOffset.y
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if lastContentOffset > scrollView.contentOffset.y{ // scroll up
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                self?.viewHeaderTop.alpha = 1.0
                self?.viewHeaderTop.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
        } else if lastContentOffset < (scrollView.contentOffset.y - viewHeaderTop.frame.height){ // scroll down nho hon uiView moi hien thi
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                
                self?.viewHeaderTop.alpha = 0
                self?.viewHeaderTop.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: nil)
        }
    }
}
