//
//  DetailPoHeaderTableViewCell.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 4/14/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class DetailPoHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblOrderCode: UILabel!
    @IBOutlet weak var lblOrderCodeValue: UILabel!
    @IBOutlet weak var lblDistributor: UILabel!
    @IBOutlet weak var lblDistributorValue: UILabel!
    @IBOutlet weak var lblSuplier: UILabel!
    @IBOutlet weak var lblSupplierValue: UILabel!
    @IBOutlet weak var lblTotalMoney: UILabel!
    @IBOutlet weak var lblTotalMoneyValue: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDescriptionValue: UILabel!
    @IBOutlet weak var TfCreateUser: SkyFloatingLabelTextField!
    @IBOutlet weak var TfCreateDate: SkyFloatingLabelTextField!
    @IBOutlet weak var TfAppoveUser: SkyFloatingLabelTextField!
    @IBOutlet weak var TfAppoveDate: SkyFloatingLabelTextField!
    @IBOutlet weak var TfReceiver: SkyFloatingLabelTextField!
    @IBOutlet weak var TfReceiveDate: SkyFloatingLabelTextField!
    
    var po:PoModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // set text label
        lblOrderCode.text = "statement_code".localized()
        lblDistributor.text = "manufacturer".localized()
        lblSuplier.text = "distributor".localized()
        lblTotalMoney.text = "total_amount".localized()
        lblDescription.text = "description".localized()
        
        // set TextField
        TfCreateUser.placeholder = "create_user".localized()
        TfCreateDate.placeholder = "create_date".localized()
        TfAppoveUser.placeholder = "approved_user".localized()
        TfAppoveDate.placeholder = "approve_date".localized()
        TfReceiver.placeholder = "receive_user".localized()
        TfReceiveDate.placeholder = "receive_date".localized()
        
        // set color value
        
         // set text label
        lblOrderCodeValue.textColor = Helper.COLOR_TEXT
         lblDistributorValue.textColor = Helper.COLOR_TEXT
         lblSupplierValue.textColor = Helper.COLOR_TEXT
         lblTotalMoneyValue.textColor = Helper.COLOR_TEXT
         lblDescriptionValue.textColor = Helper.COLOR_TEXT
        
         // disable color
        TfCreateUser.isEnabled = false
        TfCreateDate.isEnabled = false
        TfAppoveUser.isEnabled = false
        TfAppoveDate.isEnabled = false
        TfReceiver.isEnabled = false
        TfReceiveDate.isEnabled = false
        
        // set color disable
        TfCreateUser.disabledColor = Helper.COLOR_PRIMARY_TITLE
        TfCreateDate.disabledColor = Helper.COLOR_PRIMARY_TITLE
        TfAppoveUser.disabledColor = Helper.COLOR_PRIMARY_TITLE
        TfAppoveDate.disabledColor = Helper.COLOR_PRIMARY_TITLE
        TfReceiver.disabledColor = Helper.COLOR_PRIMARY_TITLE
        TfReceiveDate.disabledColor = Helper.COLOR_PRIMARY_TITLE
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDataHeader(_ poDetail: PoModel?){
        lblOrderCodeValue.text = poDetail?.code
        lblDistributorValue.text = "\(poDetail?.manufacturer?.code ?? "") - \(poDetail?.manufacturer?.name ?? "")"
        lblSupplierValue.text = "\(poDetail?.distributor?.code ?? "") - \(poDetail?.distributor?.name ?? "")"
//        lblTotalMoneyValue.text = Utils.formatCurrency(Double(poDetail?.total))
        lblDescriptionValue.text = "aaaa"
        TfCreateUser.text = poDetail?.createUser
        TfCreateDate.text = poDetail?.createDate
        TfAppoveUser.text = poDetail?.approveUser
        TfAppoveDate.text = poDetail?.approveDate
        TfReceiver.text = poDetail?.importUser
        TfReceiveDate.text = poDetail?.importDate
        
    }
    
}
