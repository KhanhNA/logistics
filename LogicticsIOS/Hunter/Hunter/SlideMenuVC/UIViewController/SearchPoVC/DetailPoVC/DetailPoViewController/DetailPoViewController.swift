//
//  DetailPoViewController.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 4/14/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit

class DetailPoViewController: UIViewController {

    @IBOutlet weak var tbViewDetailPo: UITableView!
    
    var poDetail:PoModel?
    
    var headerCell:DetailPoHeaderTableViewCell?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbViewDetailPo.register(UINib(nibName: DetailPoTableViewCellVC.className, bundle: nil), forCellReuseIdentifier: DetailPoTableViewCellVC.className)
        
        tbViewDetailPo.register(UINib(nibName: DetailPoHeaderTableViewCell.className, bundle: nil), forCellReuseIdentifier: DetailPoHeaderTableViewCell.className)
        
        uiTableViewdelegate()
        
    }
}

extension DetailPoViewController:UITableViewDelegate, UITableViewDataSource{
    
    func uiTableViewdelegate() {
        tbViewDetailPo.delegate = self
        tbViewDetailPo.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return poDetail?.poDetails?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    // height header with table
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 390
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerCell = tbViewDetailPo.dequeueReusableCell(withIdentifier: DetailPoHeaderTableViewCell.className) as? DetailPoHeaderTableViewCell
        headerCell?.po = poDetail
        headerCell?.setDataHeader(poDetail)
        return headerCell
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbViewDetailPo.dequeueReusableCell(withIdentifier: DetailPoTableViewCellVC.className, for: indexPath) as! DetailPoTableViewCellVC
        let poDetailIndex = poDetail?.poDetails?[indexPath.row]
        cell.setDetailPo(detailPo:poDetailIndex!)
        cell.selectionStyle = .none
        return cell
    }
}


