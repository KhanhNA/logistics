//
//  DetailPoTableViewCellVC.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 4/15/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit

class DetailPoTableViewCellVC: UITableViewCell {

    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductCode: UILabel!
    @IBOutlet weak var lblProductCodeValue: UILabel!
    @IBOutlet weak var lblPackingType: UILabel!
    @IBOutlet weak var lblPackingTypeValue: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblNumberValue: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceValue: UILabel!
    @IBOutlet weak var lblTotalMoney: UILabel!
    @IBOutlet weak var lblTotalMoneyValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // set text label
        lblProductCode.text = "packing_product_code".localized()
        lblPackingType.text = "packing_type".localized()
        lblNumber.text = "number".localized()
        lblPrice.text = "amount".localized()
        lblTotalMoney.text = "total_amount".localized()
        
        //set color label
        lblProductName.textColor = Helper.COLOR_TEXT_HEAD
        lblProductCodeValue.textColor = Helper.COLOR_TEXT
        lblPackingTypeValue.textColor = Helper.COLOR_TEXT
        lblNumberValue.textColor = Helper.COLOR_TEXT
        lblPriceValue.textColor = Helper.COLOR_TEXT
        lblTotalMoneyValue.textColor = Helper.COLOR_TEXT
        
        // border cell
        viewParent.layer.cornerRadius = 10
        viewParent.layer.borderWidth = 1
        viewParent.layer.borderWidth = 1.0
        viewParent.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDetailPo(detailPo:PoDetailModel) {
        lblProductName.text = detailPo.product?.name
        lblProductCodeValue.text = detailPo.productPacking?.code
        lblPackingTypeValue.text = "packing_product".localized() + " \(detailPo.productPacking?.packingType?.quantity ?? 0)" + " " + (detailPo.productPacking?.uom ?? "")
        lblNumberValue.text = "\(detailPo.quantity ?? 0)"
        lblPriceValue.text = Utils.formatCurrency(detailPo.productPackingPrice?.price ?? 0) + " \(detailPo.productPackingPrice?.currency?.name ?? "")"
        //tinh total money
        let temp1:Double = Double(detailPo.quantity ?? 0) * (detailPo.productPackingPrice?.price ?? 0)
        let temp2:Double =  100 + Double((detailPo.vat ?? 0))
        let temp3:Double = temp1 * temp2
        let totalMoney:Double = temp3/100
        lblTotalMoneyValue.text = Utils.formatCurrency(totalMoney)
    }
}
