//
//  SearchPoTableViewHeaderCell.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 4/10/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit
import DropDown


protocol UserHeaderTableViewCellDelegate {
    func didSelectStore(position: Int)
    func didSelectDistributor(position: Int)
    func didSelectStatusRecent(position: Int)
    func didSelectStatusOrder(position: Int)
    func didTextSearch(txtSearch:String)
    func didSelectFromDate(fromDate:String?)
    func didSelectToDate(toDate:String?)
}

class SearchPoTableViewHeaderCell: UITableViewCell {
    
    var delegate : UserHeaderTableViewCellDelegate?
    
    @IBOutlet var btnFromStore: UIButton!
    @IBOutlet var btnDistributor: UIButton!
    @IBOutlet var btnStatusRecent: UIButton!
    @IBOutlet var btnStatusOrder: UIButton!
    @IBOutlet var lblStore: UILabel!
    @IBOutlet var lblDistributor: UILabel!
    @IBOutlet var lblStatusRecent: UILabel!
    @IBOutlet var lblStatusOrder: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtFieldSearch: UITextField!
    
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var toDateTF: UITextField!
    var datePicker = UIDatePicker()
    var dateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    //     var delegate: QRCodeReaderViewControllerDelegate?
    
    
    lazy var presenter: StoreAndDistributorInputProto = StoreAndDistributorPresenter()
    let chooseDropDownMethod = DropDown()
    var arrStore = [StoreModel]()
    var listDistributor = [DistributorModel]()
    var listStoreString = [String]()
    var listDistributorString = [String]()
    var listPo = [PoModel]()
    var indexStore:Int = 0
    var indexDistributor:Int = 0
    var indexStatusRecent:Int = 0
    var indexStatusOrder:Int = 0
    
    
    var arrStatus:[String] = ["New".localized(),"Repacked".localized(),"Imported".localized(),"Cancel".localized()]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initController()
    }
    
    func initController() {
        
        //set label
        lblStore.text = "fromStore".localized()
        lblDistributor.text = "distributor".localized()
        lblStatusRecent.text = "current_status".localized()
        lblStatusOrder.text = "statement_status".localized()
        lblFromDate.text = "fromDate".localized()
        lblToDate.text = "toDate".localized()
        
        // khoi tao datePicker
        fromDateTF.inputView    = datePicker
        toDateTF.inputView      = datePicker
        
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped))
        toolBar.setItems([doneButton], animated: true)
        fromDateTF.inputAccessoryView   = toolBar
        toDateTF.inputAccessoryView     = toolBar
        
        // set current date UiTextField
        dateFormatter.dateFormat = AppFormatters.nomarlFormat
        let currentDateTime = Date()
        // get date truoc 1 thang
        let fromDate = Calendar.current.date(byAdding: .month, value: -1, to: currentDateTime)
        fromDateTF.text = dateFormatter.string(from: fromDate ?? currentDateTime)
        toDateTF.text = dateFormatter.string(from: currentDateTime)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fromDateTF {
            datePicker.datePickerMode = .date
        }
        if textField == toDateTF {
            datePicker.datePickerMode = .date
        }
    }
    
    @objc func doneButtonTapped() {
        dateFormatter.dateFormat = AppFormatters.nomarlFormat
        if fromDateTF.isFirstResponder {
            fromDateTF.text = dateFormatter.string(from: datePicker.date)
            self.delegate?.didSelectFromDate(fromDate: fromDateTF.text)
        }
        if toDateTF.isFirstResponder {
            toDateTF.text = dateFormatter.string(from: datePicker.date)
            self.delegate?.didSelectToDate(toDate: toDateTF.text)
        }
    }
        
    @IBAction func chooseStore(_ sender: Any) {
        setupChooseDropDownStore()
        chooseDropDownMethod.show()
    }
    
    @IBAction func chooseDistributor(_ sender: Any) {
        setupChooseDropDownDistributor()
        chooseDropDownMethod.show()
    }
    
    @IBAction func chooseStatusRecent(_ sender: Any) {
        setupChooseDropStatusRecent()
        chooseDropDownMethod.show()
    }
    
    @IBAction func chooseStatusOrder(_ sender: Any) {
        setupChooseStatusOrder()
        chooseDropDownMethod.show()
    }
    
    @IBAction func searchPo(_ sender: Any) {
        self.delegate?.didTextSearch(txtSearch: txtFieldSearch.text ?? "")
    }
    
    func validDate(){
        let checkDate:Int = DateUtil.compareDate(dateOne: fromDateTF.text, dateTwo: toDateTF.text)
        //        if checkDate == 1 {
        //            Utils.popUpDialog(title: "From date can't less than To date".localized(), message: nil, uiVC: self)
        //        }
    }
    
    
    func setupChooseDropDownStore() {
        chooseDropDownMethod.anchorView = btnFromStore
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnFromStore.bounds.height)
        chooseDropDownMethod.dataSource = self.listStoreString
        
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnFromStore.setTitle(item, for: .normal)
            self?.indexStore = Int(index)
            self?.delegate?.didSelectStore(position: Int(index))
            }
    }
    
    func setupChooseDropDownDistributor() {
        chooseDropDownMethod.anchorView = btnDistributor
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnDistributor.bounds.height)
        chooseDropDownMethod.dataSource = self.listDistributorString
        
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnDistributor.setTitle(item, for: .normal)
            self?.delegate?.didSelectDistributor(position: Int(index))
        }
    }
    
    func setupChooseDropStatusRecent() {
        chooseDropDownMethod.anchorView = btnStatusRecent
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnStatusRecent.bounds.height)
        chooseDropDownMethod.dataSource = arrStatus
                
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnStatusRecent.setTitle(item, for: .normal)
            self?.delegate?.didSelectStatusRecent(position: Int(index))
        }
    }
    
    func setupChooseStatusOrder() {
        chooseDropDownMethod.anchorView = btnStatusOrder
        chooseDropDownMethod.bottomOffset = CGPoint(x: 0, y: btnStatusOrder.bounds.height)
        chooseDropDownMethod.dataSource = arrStatus
        
        chooseDropDownMethod.selectionAction = { [weak self] (index, item) in
            self?.btnStatusOrder.setTitle(item, for: .normal)
            self?.delegate?.didSelectStatusOrder(position: Int(index))
        }
    }
}
