//
//  SearchPoTableViewCell.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 4/9/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit

class SearchPoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblCodeOrder: UILabel!
    @IBOutlet weak var lblCodeOrderValue: UILabel!
    @IBOutlet weak var lblManufactorValue: UILabel!
    @IBOutlet weak var lblManufactor: UILabel!
    @IBOutlet weak var lblDistributor: UILabel!
    @IBOutlet weak var lblDistributorValue: UILabel!
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var lblCreateDateValue: UILabel!
    @IBOutlet weak var lblTotalMoney: UILabel!
    @IBOutlet weak var lblTotalMoneyValue: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDescriptionValue: UILabel!
    @IBOutlet weak var stackViewParent: UIStackView!
    @IBOutlet weak var viewParent: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTextLabel()
        setColorLabelValue()
        viewParent.layer.cornerRadius = 10
        viewParent.layer.borderWidth = 1
        viewParent.layer.borderWidth = 1.0
        viewParent.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }
    
    func setTextLabel(){
        lblCodeOrder.text = "statement_code".localized()
        lblManufactor.text = "manufacturer".localized()
        lblDistributor.text = "distributor".localized()
        lblCreateDate.text = "create_date".localized()
        lblTotalMoney.text  = "total_amount".localized()
        lblDescription.text = "description".localized()
    }
    
    func setColorLabelValue() {
        // LABEL
        lblCodeOrder.textColor = Helper.COLOR_TEXT_V2
        lblManufactor.textColor = Helper.COLOR_TEXT_V2
        lblDistributor.textColor = Helper.COLOR_TEXT_V2
        lblCreateDate.textColor = Helper.COLOR_TEXT_V2
        lblTotalMoney.textColor  = Helper.COLOR_TEXT_V2
        lblDescription.textColor = Helper.COLOR_TEXT_V2
        // COLOR VALUE
        lblCodeOrderValue.textColor = Helper.COLOR_TEXT_HEAD
        lblManufactorValue.textColor = Helper.COLOR_TEXT
        lblDistributorValue.textColor = Helper.COLOR_TEXT
        lblCreateDateValue.textColor = Helper.COLOR_TEXT
        lblTotalMoneyValue.textColor = Helper.COLOR_TEXT
        lblDescriptionValue.textColor = Helper.COLOR_TEXT
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPo(po:PoModel) {
        lblCodeOrderValue.text = po.code
        lblManufactorValue.text = String(format: "%@-%@", (po.manufacturer?.code ?? ""),(po.manufacturer?.name ?? ""))
        lblDistributorValue.text = String(format: "%@-%@", (po.distributor?.code ?? ""),(po.distributor?.name ?? ""))
        lblCreateDateValue.text = DateUtil.convertDateFromJson(po.createDate ?? "" )
        lblTotalMoneyValue.text = "\(String(describing: po.total))"
        lblDescriptionValue.text = po.descrip
    }
    
}
