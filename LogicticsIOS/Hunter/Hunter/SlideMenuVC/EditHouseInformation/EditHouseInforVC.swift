//
//  EditHouseInforVC.swift
//  Hunter
//
//  Created by TheLightLove on 06/12/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit

class EditHouseInforVC: UIViewController {
    @IBOutlet weak var tfLat: UITextField!
    @IBOutlet weak var tfLng: UITextField!
    @IBOutlet weak var txtNumberHouse: UITextField!
    @IBOutlet weak var txtTownship: UITextField!
    @IBOutlet weak var txtProvince: UITextField!
    @IBOutlet weak var btnComplete: UIButton!
    @IBOutlet weak var ChooseMaketing: UIButton!
    @IBOutlet weak var btnChooseGround: UIButton!
    @IBOutlet weak var btnDisplay: UIButton!
    @IBOutlet weak var btnElectrical: UIButton!
    @IBOutlet weak var btnRefrigeration: UIButton!
    @IBOutlet weak var collectionGalleryView: UICollectionView!
    private var arrObjImageGallery = [Any]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
