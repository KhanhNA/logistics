//
//  InventoryTableViewCell.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 3/6/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit

class InventoryTableViewCell: UITableViewCell {

    @IBOutlet var lblCodeProduct: UILabel!
    @IBOutlet var lblValueCodeProduct: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblNameValue: UILabel!
    @IBOutlet var lblPackingType: UILabel!
    @IBOutlet var lblPackingTypeValue: UILabel!
    @IBOutlet var lblExpireDate: UILabel!
    @IBOutlet var lblExpiredateValue: UILabel!
    @IBOutlet var lblNumber: UILabel!
    @IBOutlet var lblNumberValue: UILabel!
    @IBOutlet var viewParent: UIView!

    @IBOutlet var viewProduct: UIView!
    @IBOutlet var viewName: UIView!
    @IBOutlet var viewPacking: UIView!
    @IBOutlet var viewExprire: UIView!
    @IBOutlet var viewNumber: UIView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setText()
        //set Text Color
        lblNameValue.textColor = Helper.COLOR_TEXT
        lblValueCodeProduct.textColor = Helper.COLOR_TEXT
        lblPackingTypeValue.textColor = Helper.COLOR_TEXT
        lblExpiredateValue.textColor = Helper.COLOR_TEXT
        lblNumberValue.textColor = Helper.COLOR_TEXT
        //set layout view parent
        viewParent.layer.cornerRadius = 10
        viewParent.layer.borderWidth = 1
        viewParent.layer.borderWidth = 1.0
        viewParent.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
//
//        layer.masksToBounds = false
//        layer.shadowOffset = CGSize(width: 0, height: 0)
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOpacity = 0.23
      
        //        layer.cornerRadius = 10
//        layer.masksToBounds = true
        layer.masksToBounds = false
        layer.shadowOpacity = 0.23
        layer.shadowRadius = 4
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowColor = UIColor.black.cgColor
    }
    
    func setText(){
        lblCodeProduct.text = "packing_product_code".localized()
        lblName.text = "product_name".localized()
        lblPackingType.text = "packing_type".localized()
        lblExpireDate.text = "expire_date".localized()
        lblNumber.text = "number".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataInventory(_ inventory:InventoryModel){
        lblValueCodeProduct.text = inventory.productPacking?.code ?? ""
        lblNameValue.text = inventory.productPacking?.product?.name ?? ""
        lblPackingTypeValue.text = "\(inventory.productPacking?.name ?? "") \(inventory.productPacking?.uom ?? "")"
        lblExpiredateValue.text = inventory.expireDate ?? ""
        lblNumberValue.text = String(inventory.totalQuantity ?? 0)
        
        //set background color cell
        switch inventory.colorClassName {
        case "mat-row-yellow":
            viewProduct.backgroundColor = Helper.COLOR_LIGHT_ORANGE
            viewName.backgroundColor = Helper.COLOR_LIGHT_ORANGE
            viewPacking.backgroundColor = Helper.COLOR_LIGHT_ORANGE
            viewExprire.backgroundColor = Helper.COLOR_LIGHT_ORANGE
            viewNumber.backgroundColor = Helper.COLOR_LIGHT_ORANGE
            viewParent.backgroundColor = Helper.COLOR_LIGHT_ORANGE
            break
        case "mat-row-orange":
            viewProduct.backgroundColor = Helper.COLOR_LEMON_CHIFFON
            viewName.backgroundColor = Helper.COLOR_LEMON_CHIFFON
            viewPacking.backgroundColor = Helper.COLOR_LEMON_CHIFFON
            viewExprire.backgroundColor = Helper.COLOR_LEMON_CHIFFON
            viewNumber.backgroundColor = Helper.COLOR_LEMON_CHIFFON
            viewParent.backgroundColor = Helper.COLOR_LEMON_CHIFFON
            break
        case "mat-row-red":
            viewProduct.backgroundColor = Helper.COLOR_LIGHT_RED
            viewName.backgroundColor = Helper.COLOR_LIGHT_RED
            viewPacking.backgroundColor = Helper.COLOR_LIGHT_RED
            viewExprire.backgroundColor = Helper.COLOR_LIGHT_RED
            viewNumber.backgroundColor = Helper.COLOR_LIGHT_RED
            viewParent.backgroundColor = Helper.COLOR_LIGHT_RED
            break
        default:
            viewProduct.backgroundColor = Helper.COLOR_WHITE
            viewName.backgroundColor = Helper.COLOR_WHITE
            viewPacking.backgroundColor = Helper.COLOR_WHITE
            viewExprire.backgroundColor = Helper.COLOR_WHITE
            viewNumber.backgroundColor = Helper.COLOR_WHITE
            viewParent.backgroundColor = Helper.COLOR_WHITE
        }
       
    }
    
}
