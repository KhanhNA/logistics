//
//  InventoryTableViewCell.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 3/10/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit

class InventoryTableViewCell: UITableViewCell {

    @IBOutlet var lblCodeProduct: UILabel!
    @IBOutlet var lblValueCodeProduct: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblNameValue: UILabel!
    @IBOutlet var lblPackingType: UILabel!
    @IBOutlet var lblPackingTypeValue: UILabel!
    @IBOutlet var lblExpireDate: UILabel!
    @IBOutlet var lblExpiredateValue: UILabel!
    @IBOutlet var lblNumber: UILabel!
    @IBOutlet var lblNumberValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func setDataInventory(_ inventory:inventoryModel){
//        lblValueCodeProduct.text = inventory.productPacking?.code ?? ""
//        lblNameValue.text = inventory.productPacking?.product?.name ?? ""
//        lblPackingTypeValue.text = "\(inventory.productPacking?.name ?? "") \(inventory.productPacking?.uom ?? "")"
//        lblExpiredateValue.text = inventory.expireDate ?? ""
//        lblNumber.text = String(inventory.totalQuantity ?? 0)
//
//    }
    
    func setDataInventory(){
        lblValueCodeProduct.text = "aaaa"
        lblNameValue.text = "bbb"
        lblPackingTypeValue.text = "ccccc"
        lblExpiredateValue.text = "ddd"
        lblNumber.text = "fffff"
        
    }
    
}
