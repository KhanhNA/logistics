//
//  InventoryPopUpVC.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 3/30/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit
import PopupDialog

class InventoryPopUpVC: BaseViewController
{
  
    @IBOutlet var labelDetail: UILabel!
    @IBOutlet var tbDetailPallet: UITableView!
    @IBAction func handleCloseButton(_ sender: Any) {
         dismiss(animated: true)
    }
    
    @IBOutlet var btnClose: UIButton!
    public var detailPallet = [InventoryDetailPalletModel]()
    public var textLabel:String?
    
    override func viewDidLoad() {
           super.viewDidLoad()
           labelDetail.text = textLabel
           tbDetailPallet.register(UINib(nibName: PopupDetailCellPalletTableViewCell.className, bundle: nil), forCellReuseIdentifier: PopupDetailCellPalletTableViewCell.className)
           self.uiTableViewdelegate()
//          self.tbDetailPallet?.reloadData()
           self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
       }
}
    
extension InventoryPopUpVC:UITableViewDelegate,UITableViewDataSource{
    
    func uiTableViewdelegate() {
           tbDetailPallet.delegate = self
           tbDetailPallet.dataSource = self
       }
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return detailPallet.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbDetailPallet.dequeueReusableCell(withIdentifier: PopupDetailCellPalletTableViewCell.className, for: indexPath) as! PopupDetailCellPalletTableViewCell
        let detailPalletCell = detailPallet[indexPath.row]
        cell.setDataDetailPallet(detailPallet: detailPalletCell)
        cell.selectionStyle = .none
        return cell
    }
    

}





