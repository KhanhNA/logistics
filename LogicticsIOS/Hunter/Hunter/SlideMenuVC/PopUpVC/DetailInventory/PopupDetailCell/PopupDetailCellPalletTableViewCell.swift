//
//  PopupDetailCellPalletTableViewCell.swift
//  Hunter
//
//  Created by Phạm hoàng Dự on 4/1/20.
//  Copyright © 2020 TheLightLove. All rights reserved.
//

import UIKit

class PopupDetailCellPalletTableViewCell: UITableViewCell {
  
    @IBOutlet var CodePallet: UILabel!
    @IBOutlet var CodePalletValue: UILabel!
    @IBOutlet var NamePallet: UILabel!
    @IBOutlet var NamePalletValue: UILabel!
    @IBOutlet var Number: UILabel!
    @IBOutlet var NumberValue: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CodePalletValue.textColor = Helper.COLOR_TEXT
        NamePalletValue.textColor = Helper.COLOR_TEXT
        NumberValue.textColor = Helper.COLOR_TEXT
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setDataDetailPallet(detailPallet: InventoryDetailPalletModel){
        CodePalletValue.text = detailPallet.pallet?.code
        NamePalletValue.text = detailPallet.pallet?.name
        NumberValue.text = "\(detailPallet.quantity)"
    }
}
