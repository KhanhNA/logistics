//
//  Helper.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit

struct AppColor {
    static let Primary: UIColor = UIColor(rgb: 0x000000)
}

extension UIColor{
    func getCustomBlueColor() -> UIColor{
        return UIColor(red:255, green:233 ,blue:0 , alpha:1.00)
    }
}

class Helper: NSObject {
    static let COLOR_PRIMARY_DEFAULT = #colorLiteral(red: 1, green: 0.6, blue: 0.2039215686, alpha: 1)
    static let COLOR_PRIMARY_TITLE = #colorLiteral(red: 0.1764705882, green: 0.1764705882, blue: 0.1764705882, alpha: 1)
    static let COLOR_PRIMARY_INFOR = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)   
    static let COLOR_WHITE = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let COLOR_BLUE_APP = #colorLiteral(red: 0.8196078431, green: 0.8431372549, blue: 0.9254901961, alpha: 1)
    static let COLOR_TITLE = #colorLiteral(red: 0.337254902, green: 0.6470588235, blue: 0.8235294118, alpha: 1)
    static let COLOR_TEXT = #colorLiteral(red: 0.337254902, green: 0.6470588235, blue: 0.8235294118, alpha: 1)
    static let COLOR_TEXT_HEAD = #colorLiteral(red: 0.02745098039, green: 0.3529411765, blue: 0.6352941176, alpha: 1)
    static let COLOR_LIGHT_ORANGE = #colorLiteral(red: 1, green: 0.9725490196, blue: 0.8823529412, alpha: 1)
    static let COLOR_LEMON_CHIFFON = #colorLiteral(red: 1, green: 0.9803921569, blue: 0.8039215686, alpha: 1)
    static let COLOR_LIGHT_RED = #colorLiteral(red: 1, green: 0.8, blue: 0.737254902, alpha: 1)
    static let IMAGE_HODER = UIImage(named: "avatar_default-1.png")
    static let COLOR_GRAY = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    static let COLOR_TEXT_V2 = #colorLiteral(red: 0.1411764706, green: 0.1568627451, blue: 0.1725490196, alpha: 1)
}

