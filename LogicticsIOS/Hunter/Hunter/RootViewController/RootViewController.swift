//
//  RootViewController.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class RootViewController:ButtonBarPagerTabStripViewController {
    let blueInstagramColor = Helper.COLOR_PRIMARY_DEFAULT
    var i = 1
    override func viewDidLoad() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = blueInstagramColor
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 20)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = Helper.COLOR_PRIMARY_TITLE
            newCell?.label.textColor = self?.blueInstagramColor
        }
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = LoginVC(itemInfo: "Đăng nhập")
        return [child_1]
    }

}
