//
//  PresenterLoginModel.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit
import Alamofire

protocol loginInputProtocol: BaseMVPProtocol {
    func requestLogin(username: String, password: String)
    func requestMechants(userName:String)
}
protocol LoginOutputProtocol: class {
    func didFinishLogin(_ userModel: principalModel )
    func didFinishError(errorMessage: String?)
    func didFinishErrorMerchants(errorMessage: String?)
    func didFishMechants(_ Mechants: UserInforModel)
}
class LoginPresenter: loginInputProtocol {
    
    
    var user = AccountModel()
    weak var delegate: LoginOutputProtocol?
    func attachView(_ view: Any) {
        delegate = view as? LoginOutputProtocol
    }
    
    func requestLogin(username: String, password: String) {
        let loginRequest = [
            "grant_type"    : APIKey.password,
            "username"      : username,
            "password"      : password
            ] as [String : Any]
        let serverUrl = Const.GETTOKEN
        Alamofire.request(serverUrl, method: .post, parameters: loginRequest, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .success(let data):
                print("isi: \(data)")
                if let json = response.value as?  [String: Any] {
                    let access_token = json["access_token"] as? String
                    self.user.access_token = access_token
                    DispatchQueue.main.async {
                        self.getDetailLogin()
                        self.requestMechants(userName: username)
                    }
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
            }
            SessionManagerCodale.shared.saveToUserDefault(accountModel: self.user)
        }
    }
    
    func requestMechants(userName: String) {
        let para: Parameters = [
            "userName" : userName
        ]
        let router = RouterFromdat.merchantsUser(para: para)
        APIRequest.requestOk(router, requiredToken: true) { [weak self] (responseModel: UserInforModel?) in
            guard let resenponseMD = responseModel else {
                self?.delegate?.didFinishErrorMerchants(errorMessage: "lỗi ko có data")
                return
            }
            self?.delegate?.didFishMechants(resenponseMD)
        }
    }
    
    
    func getDetailLogin(){
        let router = Router.login(body: nil)
        APIRequest.request(router, requiredToken: true) { [weak self] (responseModel: ResponseModel<UserModel>?) in
            guard let loginResponse = responseModel?.userAuthentication else {
                self?.delegate?.didFinishError(errorMessage: "Data Error")
                return
            }
            guard let user = loginResponse.principal else {
                return
            }
            self?.delegate?.didFinishLogin(user)
        }
    }
    
}
