//
//  LoginVC.swift
//  Hunter
//
//  Created by TheLightLove on 16/11/2019.
//  Copyright © 2019 TheLightLove. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import TransitionButton
import Localize_Swift
import PopupDialog

class LoginVC: BaseViewController,IndicatorInfoProvider {
    lazy var presenter: loginInputProtocol = LoginPresenter()
 
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet var btnChangeLanguage: UIButton!
    var button:TransitionButton?
    var index = 1
    var itemInfo: IndicatorInfo = "View"
    var dictionaryMenu = Dictionary<String, MenuItem>()
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
        
    }
    
    @IBAction func changLanguage(_ sender: Any) {
        let title = "select_language".localized()
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: nil, image: nil)
        
        //        // Create buttons
        let buttonVietNam = DefaultButton(title: "VietNamese".localized()) {
            Localize.setCurrentLanguage("vi")
            self.reloadView()
        }
        
        let buttonEnglish = DefaultButton(title: "English".localized()) {
            Localize.setCurrentLanguage("en")
            self.reloadView()
        }
        
        let buttonMyanmar = DefaultButton(title: "Myanmar".localized()) {
            Localize.setCurrentLanguage("my")
            self.reloadView()
        }
        
        let buttonThree = CancelButton(title: "Cancel".localized()) {
        }
        
        // to add a single button
        popup.addButtons([buttonVietNam, buttonEnglish,buttonMyanmar, buttonThree])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func reloadView(){
        self.loadView()
        self.layout()
    }
            
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        layout()
        initMenuTitle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //Do your thing here
    }
        
    @IBAction func onLogin(_ sender: TransitionButton) {
        button = sender
        guard let username = txtUser.text, !(txtUser.text?.isEmpty)! else { return }
        guard let password = txtPassword.text, !(txtPassword.text?.isEmpty)! else { return }
        presenter.requestLogin(username: username, password: password)
    }
    
    func layout() {
        btnLogin.setTitle("login".localized(), for: .normal)
        btnChangeLanguage.setTitle("change_language".localized(), for: .normal)
        btnLogin.layer.cornerRadius = 25
        btnLogin.layer.masksToBounds = true
        btnLogin.backgroundColor = Helper.COLOR_PRIMARY_DEFAULT
    }
    
    func findMenuItem(code:String) -> MenuItem {
        return dictionaryMenu[code]!
    }
    
    func initMenuTitle(){
        dictionaryMenu["CATEGORY"] = MenuItem(code: "CATEGORY", title: "CATEGORY", image: UIImage(named: "category"), uiController: nil)
        dictionaryMenu["INVENTORY"] = MenuItem(code: "INVENTORY", title: "INVENTORY", image: UIImage(named: "manage_fc"), uiController: InventoryViewController())
        
        dictionaryMenu["MERCHANT_ORDERs"] = MenuItem(code: "MERCHANT_ORDERs", title: "MERCHANT_ORDERs", image: UIImage(named: "merchant_order"), uiController: nil)
        dictionaryMenu["MERCHANT_ORDERs_LIST_MERCHANT_ORDER"] = MenuItem(code: "MERCHANT_ORDERs_LIST_MERCHANT_ORDER", title: "MERCHANT_ORDERs_LIST_MERCHANT_ORDER", image: UIImage(named: "list_merchant_order"), uiController: InventoryViewController())
        
        dictionaryMenu["IMPORT_STATEMENTs"] = MenuItem(code: "IMPORT_STATEMENTs", title: "IMPORT_STATEMENTs", image: UIImage(named: "import_statement"), uiController: nil)
        dictionaryMenu["IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT"] = MenuItem(code: "IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT", title: "IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT", image: UIImage(named: "create_import_statement"), uiController: CreateImportStatementVC())
        dictionaryMenu["IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT_RELEASES"] = MenuItem(code: "IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT_RELEASES", title: "IMPORT_STATEMENTs_IMPORT_FROM_EXPORT_STATEMENT_RELEASES", image: UIImage(named: "import_release"), uiController: InventoryViewController())
        
        dictionaryMenu["EXPORT_STATEMENTs"] = MenuItem(code: "EXPORT_STATEMENTs", title: "EXPORT_STATEMENTs", image: UIImage(named: "export_statement"), uiController: nil)
        dictionaryMenu["EXPORT_STATEMENTs_EXPORT_STATEMENT"] = MenuItem(code: "EXPORT_STATEMENTs_EXPORT_STATEMENT", title: "EXPORT_STATEMENTs_EXPORT_STATEMENT", image: UIImage(named: "create_export_statement"), uiController: InventoryViewController())
        dictionaryMenu["EXPORT_STATEMENTs_RELEASES"] = MenuItem(code: "EXPORT_STATEMENTs_RELEASES", title: "EXPORT_STATEMENTs_RELEASES", image: UIImage(named: "export_release"), uiController: InventoryViewController())
        dictionaryMenu["EXPORT_STATEMENTs_DELIVERY"] = MenuItem(code: "EXPORT_STATEMENTs_DELIVERY", title: "EXPORT_STATEMENTs_DELIVERY", image: UIImage(named: "export_delivery"), uiController: InventoryViewController())
        
        dictionaryMenu["MANAGE_FC"] = MenuItem(code: "MANAGE_FC", title: "MANAGE_FC", image: UIImage(named: "manage_fc"), uiController: nil)
        dictionaryMenu["MANAGE_FC_UPDATE_PALLET"] = MenuItem(code: "MANAGE_FC_UPDATE_PALLET", title: "MANAGE_FC_UPDATE_PALLET", image: UIImage(named: "update_pallet"), uiController: InventoryViewController())
        dictionaryMenu["MANAGE_FC_IMPORT_STATEMENTs_FROM_REPACKING_PLANNING"] = MenuItem(code: "MANAGE_FC_IMPORT_STATEMENTs_FROM_REPACKING_PLANNING", title: "MANAGE_FC_IMPORT_STATEMENTs_FROM_REPACKING_PLANNING", image: UIImage(named: "import_fc_from_repack"), uiController: InventoryViewController())
        
        dictionaryMenu["PUCHASEORDER"] = MenuItem(code: "PUCHASEORDER", title: "PUCHASEORDER", image: UIImage(named: "po"), uiController: nil)
        dictionaryMenu["PUCHASEORDER_LIST"] = MenuItem(code: "PUCHASEORDER_LIST", title: "PUCHASEORDER_LIST", image: UIImage(named: "list_po"), uiController: SearchPoViewController())
        
        dictionaryMenu["CLAIM"] = MenuItem(code: "CLAIM", title: "CLAIM", image: UIImage(named: "claim_pallet"), uiController: nil)
        dictionaryMenu["CLAIM_PALLET_STEP_ONE"] = MenuItem(code: "CLAIM_PALLET_STEP_ONE", title: "CLAIM_PALLET_STEP_ONE", image: UIImage(named: "crack_one"), uiController: InventoryViewController())
        dictionaryMenu["CLAIM_PALLET_STEP_TWO"] = MenuItem(code: "CLAIM_PALLET_STEP_TWO", title: "CLAIM_PALLET_STEP_TWO", image: UIImage(named: "crack_two"), uiController: InventoryViewController())
        dictionaryMenu["CLAIM_FIND"] = MenuItem(code: "CLAIM_FIND", title: "CLAIM_FIND", image: UIImage(named: "search_claim"), uiController: InventoryViewController())
        
        
    }
}

extension LoginVC:LoginOutputProtocol{
    func didFinishErrorMerchants(errorMessage: String?) {
        
    }
    
    func didFishMechants(_ Mechants: UserInforModel) {
        Global.user = Mechants
    }
    
        
    func didFinishLogin(_ userModel: principalModel) {
        guard  userModel.roles != nil else {
            return
        }
        // phân quyền left menu controller
        //String la danh muc chính, menuItem là list danh mục con tương ứng
        var dict = Dictionary<String, [MenuItem]>()
        let listRole:[Roles] = userModel.roles!
        for role in listRole {
            let listMenu:[Menu] = role.menus!
            for menu in listMenu {
                // check type la mobile, WEB bỏ qua
                if "MOBILE".equal(menu.appType!) && menu.parentMenu == nil {
                    var arrSubMenuItem = [MenuItem]()
                    // check parentMenu khac nil, add submenu to key parent
                    for subMenu in role.menus! {
                        if "MOBILE".equal(subMenu.appType!) && subMenu.parentMenu != nil {
                            // nêu code subMenu == parent menu, add to arr
                            if menu.code!.equal(subMenu.parentMenu?.code ?? ""){
                                let menuItem:MenuItem = findMenuItem(code: subMenu.code ?? "")
                                arrSubMenuItem.append(menuItem)
                            }
                        }
                    }
                    if isNotNil(someObject: arrSubMenuItem) {
                        dict[menu.code!] = arrSubMenuItem
                    }
                }
            }
        }
        
        //add obj
        var tableViewData: [CellData] = []
        
        // add thêm mục Change Language
        dict["CHANGE_LANGUAGE"] = [MenuItem(code: "CHANGE_LANGUAGE", title: "language", image: UIImage(named: "language"), uiController: ChangeLanguageVC())]
        // gán dictionary cho mảng
        let sortedKeys = dict.keys.sorted()
        for key in sortedKeys{
            let cellItem:CellData = CellData(opened: false, title: key, sectionData: dict[key]!)
            tableViewData.append(cellItem)
        }
        // sap xep lại mảng
        tableViewData.sort(by: {$0.title < $1.title})
        
        button?.startAnimation()
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            sleep(2) // 3: Do your networking task or background work here.
            DispatchQueue.main.async(execute: { () -> Void in
                self.button?.stopAnimation(animationStyle: .expand, completion: {
                    self.setRevealViewController(listCellData: tableViewData)
                })
            })
        })
    }
    func didFinishError(errorMessage: String?) {
        print("Error")
    }
    
    func isNotNil(someObject: Any?) -> Bool {
           if someObject is String {
               if (someObject as? String) != nil {
                   return true
               }else {
                   return false
               }
           }else if someObject is Array<Any> {
               if (someObject as? Array<Any>) != nil {
                   return true
               }else {
                   return false
               }
           }else if someObject is Dictionary<AnyHashable, Any> {
               if (someObject as? Dictionary<String, Any>) != nil {
                   return true
               }else {
                   return false
               }
           }else if someObject is Data {
               if (someObject as? Data) != nil {
                   return true
               }else {
                   return false
               }
           }else if someObject is NSNumber {
               if (someObject as? NSNumber) != nil{
                   return true
               }else {
                   return false
               }
           }else if someObject is UIImage {
               if (someObject as? UIImage) != nil {
                   return true
               }else {
                   return false
               }
           }
           return false
    }
    
   
}


