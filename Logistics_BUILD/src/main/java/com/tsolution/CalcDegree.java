package com.tsolution;

import java.awt.Graphics;

import javax.swing.JFrame;

public class CalcDegree extends JFrame {

	public CalcDegree() {
		this.setSize(1000, 1000);
		this.setVisible(true);
	}

	private static int calcAngleByHourMinute(int hour, int minute) {
		double angle = Math.abs(0.5 * ((60 * hour) + minute));
//		return (int) (angle > 180 ? 360 - angle : angle);
		return (int) angle;
	}

	private static int calcAngleDelta(int h1, int m1, int h2, int m2) {
		double angle1 = CalcDegree.calcAngleByHourMinute(h1, m1);
		double angle2 = CalcDegree.calcAngleByHourMinute(h2, m2);
		double delta = Math.abs(angle2 - angle1);
		return (int) (delta > 180 ? 360 - delta : delta);
	}

	public static void main(String[] args) {
		new CalcDegree();
		System.out.println(CalcDegree.calcAngleDelta(3, 0, 9, 0));
	}

	@Override
	public void paint(Graphics g) {
		g.fillArc(50, 50, 50, 50, CalcDegree.calcAngleByHourMinute(0, 00), CalcDegree.calcAngleDelta(0, 0, 2, 0));
		g.fillArc(100, 50, 50, 50, CalcDegree.calcAngleByHourMinute(3, 00), CalcDegree.calcAngleDelta(3, 0, 10, 0));
		g.fillArc(150, 50, 50, 50, CalcDegree.calcAngleByHourMinute(9, 00), CalcDegree.calcAngleDelta(9, 0, 14, 0));
		g.fillArc(200, 50, 50, 50, CalcDegree.calcAngleByHourMinute(4, 00), CalcDegree.calcAngleDelta(4, 0, 6, 30));
		g.fillArc(250, 50, 50, 50, CalcDegree.calcAngleByHourMinute(16, 00), 3);
		g.fillArc(300, 50, 50, 50, CalcDegree.calcAngleByHourMinute(10, 00), 3);
	}

}
