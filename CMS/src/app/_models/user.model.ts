import {RoleModel} from './role.model';

export class UserModel {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  roles: RoleModel[];
}
