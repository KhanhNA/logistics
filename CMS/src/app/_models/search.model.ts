export class SearchModel {
  clientId: string;
  text: string;
  pageSize: number;
  pageNumber: number;
  totalElements: number;
}
