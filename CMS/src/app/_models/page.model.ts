export class PageModel {
  content: any[];
  size: number;
  number: number;
  totalElements: number;
}
